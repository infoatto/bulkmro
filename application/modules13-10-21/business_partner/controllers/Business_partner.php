<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Business_partner extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('business_partnermodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersList")){
            redirect('dashboard');
        }
    }

    function index() {
        $data = array();
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('business_partner/index', $data);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersAddEdit")){
            redirect('dashboard');
        }
        //add edit form
        $business_partner_id = "0";
        $edit_datas = array();
        $edit_datas['billingCountry'] = array();
        $edit_datas['billingState'] = array();
        $edit_datas['billingCity'] = array();
        $edit_datas['paymentTermData'] = array();
        $edit_datas['incotermData'] = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $business_partner_id = $url_prams['id'];
            //business partner get data in case of edit
            $result = $this->common->getData("tbl_business_partner", "*", array("business_partner_id" => $business_partner_id));
            if (!empty($result)) {
                $edit_datas['business_partner_details'] = $result[0];
                //billing details get data in case of edit
                $billingDetail = $this->common->getData("tbl_billing_details", "*", array("business_partner_id" => $business_partner_id));
                if (!empty($billingDetail)) {
                    $edit_datas['billing_details'] = $billingDetail;
                }
                //shipping details get data in case of edit
                $shippingDetail = $this->common->getData("tbl_shipping_details", "*", array("business_partner_id" => $business_partner_id));
                if (!empty($shippingDetail)) {
                    $edit_datas['shippingDetail'] = $shippingDetail;
                }
            }
        }
        //country data
        $country = $this->common->getData("tbl_country", "country_id,country_name", "");
        if (!empty($country)) {
            $edit_datas['country'] = $country;
        }
        //state data
        $billingState = $this->common->getData("tbl_state", "state_id,state_name,country_id", "");
        if (!empty($billingState)) {
            $edit_datas['billingState'] = $billingState;
        }
        //city data
        $billingCity = $this->common->getData("tbl_city", "city_id,city_name,state_id", "");
        if (!empty($billingCity)) {
            $edit_datas['billingCity'] = $billingCity;
        }
        //payment term data
        $paymentTermData = $this->common->getData("tbl_payment_term", "payment_term_id,payment_term_value",array("status"=>"Active"));
        if (!empty($paymentTermData)) {
            $edit_datas['paymentTermData'] = $paymentTermData;
        }
        //incoterm data
        $incotermData = $this->common->getData("tbl_incoterms", "incoterms_id,incoterms_value",array("status"=>"Active"));
        if (!empty($incotermData)) {
            $edit_datas['incotermData'] = $incotermData;
        } 
        $edit_datas['view'] = isset($_GET['view'])?$_GET['view']:0; 
        
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('business_partner/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        //get list data
        $get_result = $this->business_partnermodel->getRecords($_GET);
        //echo '<pre>'; print_r($get_result);die;
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->business_name));
                array_push($temp, $get_result['query_result'][$i]->business_type);
                array_push($temp, $get_result['query_result'][$i]->business_category);
                array_push($temp, $get_result['query_result'][$i]->contact_person);
                array_push($temp, $get_result['query_result'][$i]->phone_number);
                array_push($temp, $get_result['query_result'][$i]->email);
                array_push($temp, $get_result['query_result'][$i]->status);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("BusinessPartnersAddEdit")){   
                    $actionCol = '<a href="business_partner/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->business_partner_id), '+/', '-_'), '=') . '" title="Edit" class="text-center"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                } 
                if($this->privilegeduser->hasPrivilege("BusinessPartnersView")){   
                    $actionCol .= '<a href="business_partner/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->business_partner_id), '+/', '-_'), '=') . '&view=1" title="Edit" class="text-center" style="float:right;"><img src="' . base_url() . 'assets/images/view-btn-icon.svg" alt="View" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "business_name = '" . $this->input->post('business_name') . "' ";
            if (!empty($this->input->post("business_partner_id"))) {
                $condition .= " AND business_partner_id <> " . $this->input->post("business_partner_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['business_partner_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Business Name already exist...'));
                exit;
            }

            $data = array();
            $data['business_name'] = $this->input->post('business_name');
            $data['business_type'] = $this->input->post('business_type');
            $data['business_category'] = $this->input->post('business_category');
            $data['contact_person'] = $this->input->post('contact_person');
            $data['email'] = $this->input->post('email');
            $data['phone_number'] = $this->input->post('phone_number');
            $data['alias'] = $this->input->post('alias');
            $data['payment_terms_id'] = $this->input->post('payment_terms_id');
            $data['incoterms_id'] = $this->input->post('incoterms_id');
            $data['status'] = $this->input->post('status');

            if (!empty($this->input->post("business_partner_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_business_partner", $data, array("business_partner_id" => $this->input->post("business_partner_id")));

                $data1['business_partner_id'] = $this->input->post("business_partner_id"); 
                foreach ($this->input->post('billing_person') as $key => $value) {
                    $data1['contact_person'] = $this->input->post('billing_person')[$key];
                    $data1['email'] = $this->input->post('billing_email')[$key];
                    $data1['number'] = $this->input->post('billing_phone')[$key];
                    $data1['address_line_1'] = $this->input->post('billing_line_one')[$key];
                    $data1['address_line_2'] = $this->input->post('billing_line_two')[$key];
                    $data1['country_id'] = $this->input->post('billing_country')[$key];
                    $data1['state_id'] = $this->input->post('billing_state')[$key];
                    $data1['city_id'] = $this->input->post('billing_city')[$key];
                    $data1['zipcode'] = $this->input->post('billing_zipcode')[$key];
                    //check billing details data
                    $billingDetailsData = $this->common->getData("tbl_billing_details", "billing_details_id", array("business_partner_id" => $this->input->post("business_partner_id"),"billing_details_id" => (int)$this->input->post("billing_details_id")[$key]));
                    if (!empty($billingDetailsData)) {
                        //update if exits
                        $result1 = $this->common->updateData('tbl_billing_details', $data1, array("business_partner_id" => $this->input->post("business_partner_id"),"billing_details_id" => (int)$this->input->post("billing_details_id")[$key])); 
                    }else{ 
                        //insert if not exits
                        if(!empty($this->input->post('billing_person')[$key])){
                            $result1 = $this->common->insertData('tbl_billing_details', $data1, '1');
                        } 
                    } 
                } 

                $data2['business_partner_id'] = $this->input->post("business_partner_id");                
                foreach ($this->input->post('shipping_person') as $key => $value) {
                    $data2['contact_person'] = $this->input->post('shipping_person')[$key];
                    $data2['email'] = $this->input->post('shipping_email')[$key];
                    $data2['number'] = $this->input->post('shipping_phone')[$key];
                    $data2['address_line_1'] = $this->input->post('shipping_line_one')[$key];
                    $data2['address_line_2'] = $this->input->post('shipping_line_two')[$key];
                    $data2['country_id'] = $this->input->post('shipping_country')[$key];
                    $data2['state_id'] = $this->input->post('shipping_state')[$key];
                    $data2['city_id'] = $this->input->post('shipping_city')[$key];
                    $data2['zipcode'] = $this->input->post('shipping_zipcode')[$key]; 
                    
                    //check shipping details data
                    $shippingDetailsData = $this->common->getData("tbl_shipping_details", "shipping_details_id", array("business_partner_id" => $this->input->post("business_partner_id"),"shipping_details_id" => (int)$this->input->post("shipping_details_id")[$key]));
                    if (!empty($shippingDetailsData)) {
                        //update if exits
                        $result2 = $this->common->updateData('tbl_shipping_details', $data2, array("business_partner_id" => $this->input->post("business_partner_id"),"shipping_details_id" => (int)$this->input->post("shipping_details_id")[$key])); 
                    }else{ 
                        //insert if not exits
                        if(!empty($this->input->post('shipping_person')[$key])){
                            $result2 = $this->common->insertData('tbl_shipping_details', $data2, '1');
                        } 
                    } 
                } 

                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_business_partner", $data, "1");

                if ($result) {
                    //insert billing details
                    $data1['business_partner_id'] = $result;
                    if (!empty($this->input->post('billing_person'))) {
                        foreach ($this->input->post('billing_person') as $key => $value) {
                            $data1['contact_person'] = $value;
                            $data1['email'] = $this->input->post('billing_email')[$key];
                            $data1['number'] = $this->input->post('billing_phone')[$key];
                            $data1['address_line_1'] = $this->input->post('billing_line_one')[$key];
                            $data1['address_line_2'] = $this->input->post('billing_line_two')[$key];
                            $data1['country_id'] = $this->input->post('billing_country')[$key];
                            $data1['state_id'] = $this->input->post('billing_state')[$key];
                            $data1['city_id'] = $this->input->post('billing_city')[$key];
                            $data1['zipcode'] = $this->input->post('billing_zipcode')[$key];
                            $result1 = $this->common->insertData('tbl_billing_details', $data1, '1');
                        }
                    }
                    //insert shipping details
                    $data2['business_partner_id'] = $result;
                    if (!empty($this->input->post('shipping_person'))) {
                        foreach ($this->input->post('shipping_person') as $key => $value) {
                            $data2['contact_person'] = $value;
                            $data2['email'] = $this->input->post('shipping_email')[$key];
                            $data2['number'] = $this->input->post('shipping_phone')[$key];
                            $data2['address_line_1'] = $this->input->post('shipping_line_one')[$key];
                            $data2['address_line_2'] = $this->input->post('shipping_line_two')[$key];
                            $data2['country_id'] = $this->input->post('shipping_country')[$key];
                            $data2['state_id'] = $this->input->post('shipping_state')[$key];
                            $data2['city_id'] = $this->input->post('shipping_city')[$key];
                            $data2['zipcode'] = $this->input->post('shipping_zipcode')[$key];
                            $result2 = $this->common->insertData('tbl_shipping_details', $data2, '1');
                        }
                    }

                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }
    
    function importBusinessPartner() {
        
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersImport")){
            redirect('dashboard');
        }
        require_once('application/libraries/SimpleXLSX.php');
        
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["excelfile"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["excelfile"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die;
                
                $BP_Type = getBusinessPartnerType(); 
                
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) { 
                        if (trim($data[0]) != "Business Name") {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        if (trim($data[1]) != "Business Partner Type" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[2]) != "Business Partner Category" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[3]) != "Contact Person" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[4]) != "Email Address" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[5]) != "Phone Number" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[6]) != "Business Alias" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[7]) != "Status" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[8]) != "Payment Terms" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[9]) != "Incoterm" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[10]) != "Billing Contact Person" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[11]) != "Contact Phone" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[12]) != "Contact Email" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[13]) != "Address Line 1" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[14]) != "Address Line 2" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[15]) != "Country" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[16]) != "State" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[17]) != "City" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[18]) != "Zip Code" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if (trim($data[19]) != "Shipping Contact Person" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[20]) != "Contact Phone" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[21]) != "Contact Email" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[22]) != "Address Line 1" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[23]) != "Address Line 2" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[24]) != "Country" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[25]) != "State" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[26]) != "City" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[27]) != "Zip Code" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1]) && empty($data[2]) && empty($data[3])){
                            continue;
                        } 
                        
                        if (trim($data[0]) == "") {
                            $error .= "Business Name cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                        $typeValid=1;
                        if (!in_array(trim($data[1]),$BP_Type)) {
                            $error .= "Business Partner Type wrong in row : " . ($key + 1) . "<br>";
                            $valid++;
                            $typeValid==0;
                        } 
                         
                        if (!in_array(trim($data[2]),getBusinessPartnerCategory(trim($data[1]))) && $typeValid==1) {
                            $error .= "Business Partner Category wrong in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                        
                        if (trim($data[6]) == "") {
                            $error .= "Business Alias cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }   
                        
                        if (trim($data[7]) != "Active" && $status != "In-active") {
                            $error .= "Status cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1]) && empty($data[2]) && empty($data[3])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $name = str_replace("'", "&#39;", trim($data[0]));
                        $type = str_replace("'", "&#39;", trim($data[1]));
                        $category = str_replace("'", "&#39;", trim($data[2]));                        
                        $contact_person = str_replace("'", "&#39;", trim($data[3]));
                        $email = str_replace("'", "&#39;", trim($data[4]));
                        $phone_number = str_replace("'", "&#39;", trim($data[5]));
                        $alias = str_replace("'", "&#39;", trim($data[6])); 
                        $status = str_replace("'", "&#39;", trim($data[7]));                        
                        $payment_terms = str_replace("'", "&#39;", trim($data[8]));                        
                        $incoterms = str_replace("'", "&#39;", trim($data[9]));
                        //billing details
                        $billing_contact_person = str_replace("'", "&#39;", trim($data[10]));
                        $billing_number = str_replace("'", "&#39;", trim($data[11]));
                        $billing_email = str_replace("'", "&#39;", trim($data[12]));
                        $billing_address_line_1 = str_replace("'", "&#39;", trim($data[13]));
                        $billing_address_line_2 = str_replace("'", "&#39;", trim($data[14]));
                        $billing_county = str_replace("'", "&#39;", trim($data[15]));
                        $billing_state = str_replace("'", "&#39;", trim($data[16]));
                        $billing_city = str_replace("'", "&#39;", trim($data[17]));
                        $billing_zipcode = str_replace("'", "&#39;", trim($data[18]));                        
                        //shipping details
                        $shipping_contact_person = str_replace("'", "&#39;", trim($data[19]));
                        $shipping_number = str_replace("'", "&#39;", trim($data[20]));
                        $shipping_email = str_replace("'", "&#39;", trim($data[21]));
                        $shipping_address_line_1 = str_replace("'", "&#39;", trim($data[22]));
                        $shipping_address_line_2 = str_replace("'", "&#39;", trim($data[23]));
                        $shipping_county = str_replace("'", "&#39;", trim($data[24]));
                        $shipping_state = str_replace("'", "&#39;", trim($data[25]));
                        $shipping_city = str_replace("'", "&#39;", trim($data[26]));
                        $shipping_zipcode = str_replace("'", "&#39;", trim($data[27])); 
                        
                        $condition = "business_name = '" . $name . "' AND business_type = '" . $type . "' AND business_category = '" . $category . "' AND status='" . $status . "' "; 
                        $chk_client_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $condition);
                        $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");
                        
                        //get payment terms id
                        $condition_payterm = "payment_term_value = '" . $payment_terms . "' "; 
                        $chk_payterm_sql = $this->common->Fetch("tbl_payment_term", "payment_term_id", $condition_payterm);
                        $rs_payterm = $this->common->MySqlFetchRow($chk_payterm_sql, "array");
                        
                        $payment_terms_id = 0;
                        if (!empty($rs_payterm[0]['payment_term_id'])) {
                            $payment_terms_id = $rs_payterm[0]['payment_term_id'];
                        }
                        
                        //get incoterms id
                        $condition_incoterm = "incoterms_value = '" . $incoterms . "' "; 
                        $chk_incoterm_sql = $this->common->Fetch("tbl_incoterms", "incoterms_id", $condition_incoterm);
                        $rs_incoterm = $this->common->MySqlFetchRow($chk_incoterm_sql, "array");
                        
                        $incoterms_id = 0;
                        if (!empty($rs_incoterm[0]['incoterms_id'])) {
                            $incoterms_id = $rs_incoterm[0]['incoterms_id'];
                        }
                        
                        $data = array();
                        $data['business_name'] = $name;
                        $data['business_type'] = $type;
                        $data['business_category'] = $category;  
                        $data['contact_person'] = $contact_person; 
                        $data['email'] = $email; 
                        $data['phone_number'] = $phone_number; 
                        $data['alias'] = $alias; 
                        $data['status'] = $status;                        
                        $data['payment_terms_id'] = $payment_terms_id;
                        $data['incoterms_id'] = $incoterms_id; 
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");  
                        

                        //get billing country id
                        $condition_bill_country = "country_name = '" . $billing_county . "' "; 
                        $chk_bill_country_sql = $this->common->Fetch("tbl_country", "country_id", $condition_bill_country);
                        $rs_bill_country = $this->common->MySqlFetchRow($chk_bill_country_sql, "array");

                        $billing_country_id = 0;
                        if (!empty($rs_bill_country[0]['country_id'])) {
                            $billing_country_id = $rs_bill_country[0]['country_id'];
                        }
                        
                        //get billing state id
                        $condition_bill_state = "state_name = '" . $billing_state . "' "; 
                        $chk_bill_state_sql = $this->common->Fetch("tbl_state", "state_id", $condition_bill_state);
                        $rs_bill_state = $this->common->MySqlFetchRow($chk_bill_state_sql, "array");

                        $billing_state_id = 0;
                        if (!empty($rs_bill_state[0]['state_id'])) {
                            $billing_state_id = $rs_bill_state[0]['state_id'];
                        }
                        
                        //get billing city id
                        $condition_bill_city = "city_name = '" . $billing_city . "' "; 
                        $chk_bill_city_sql = $this->common->Fetch("tbl_city", "city_id", $condition_bill_city);
                        $rs_bill_city = $this->common->MySqlFetchRow($chk_bill_city_sql, "array");

                        $billing_city_id = 0;
                        if (!empty($rs_bill_city[0]['city_id'])) {
                            $billing_city_id = $rs_bill_city[0]['city_id'];
                        }
                        
                        $billing_data = array();                           
                        $billing_data['contact_person'] = $billing_contact_person;
                        $billing_data['email'] = $billing_email;
                        $billing_data['number'] = $billing_number;  
                        $billing_data['address_line_1'] = $billing_address_line_1; 
                        $billing_data['address_line_2'] = $billing_address_line_2;
                        $billing_data['country_id'] = $billing_country_id;
                        $billing_data['state_id'] = $billing_state_id;
                        $billing_data['city_id'] = $billing_city_id;                        
                        $billing_data['zipcode'] = $billing_zipcode;   
                        
                        
                        //get shipping country id
                        $condition_ship_country = "country_name = '" . $shipping_county . "' "; 
                        $chk_ship_country_sql = $this->common->Fetch("tbl_country", "country_id", $condition_ship_country);
                        $rs_ship_country = $this->common->MySqlFetchRow($chk_ship_country_sql, "array");

                        $shipping_country_id = 0;
                        if (!empty($rs_ship_country[0]['country_id'])) {
                            $shipping_country_id = $rs_ship_country[0]['country_id'];
                        }
                        
                        //get shipping state id
                        $condition_ship_state = "state_name = '" . $shipping_state . "' "; 
                        $chk_ship_state_sql = $this->common->Fetch("tbl_state", "state_id", $condition_ship_state);
                        $rs_ship_state = $this->common->MySqlFetchRow($chk_ship_state_sql, "array");

                        $shipping_state_id = 0;
                        if (!empty($rs_ship_state[0]['state_id'])) {
                            $shipping_state_id = $rs_ship_state[0]['state_id'];
                        }
                        
                        //get shipping city id
                        $condition_ship_city = "city_name = '" . $shipping_city . "' "; 
                        $chk_ship_city_sql = $this->common->Fetch("tbl_city", "city_id", $condition_ship_city);
                        $rs_ship_city = $this->common->MySqlFetchRow($chk_ship_city_sql, "array");

                        $shipping_city_id = 0;
                        if (!empty($rs_ship_city[0]['city_id'])) {
                            $shipping_city_id = $rs_ship_city[0]['city_id'];
                        }
                        
                        $shipping_data = array();                           
                        $shipping_data['contact_person'] = $shipping_contact_person;
                        $shipping_data['email'] = $shipping_email;
                        $shipping_data['number'] = $shipping_number;  
                        $shipping_data['address_line_1'] = $shipping_address_line_1; 
                        $shipping_data['address_line_2'] = $shipping_address_line_2;
                        $shipping_data['country_id'] = $shipping_country_id;
                        $shipping_data['state_id'] = $shipping_state_id;
                        $shipping_data['city_id'] = $shipping_city_id;                        
                        $shipping_data['zipcode'] = $shipping_zipcode; 
                        
                         //update-insert bussiness partner data 
                        if (!empty($rs_client[0]['business_partner_id'])) {                            
                            $result = $this->common->updateData("tbl_business_partner", $data, array("business_partner_id" => $rs_client[0]['business_partner_id']));  
                            if ($result) { 
                                //update billing details 
                                $result1 = $this->common->updateData("tbl_billing_details", $billing_data, array("business_partner_id" => $rs_client[0]['business_partner_id']));                               
                                //update shipping details 
                                $result2 = $this->common->updateData("tbl_shipping_details", $shipping_data, array("business_partner_id" => $rs_client[0]['business_partner_id']));  
                                
                                $imported++;
                            } else {
                                $notimported++;
                            }
                        }else{
                             //insert data 
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $result = $this->common->insertData("tbl_business_partner", $data, "1");
                            if ($result) {
                                //insert billing details
                                $billing_data['business_partner_id'] = $result;
                                $result1 = $this->common->insertData("tbl_billing_details", $billing_data, "1");
                                
                                //insert shipping details
                                $shipping_data['business_partner_id'] = $result;
                                $result2 = $this->common->insertData("tbl_shipping_details", $shipping_data, "1");
                                
                                $imported++;
                            } else {
                                $notimported++;
                            } 
                        }  
                    }
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    } 

    function getBusinessPartner() {
        //business partner dropdown at click of business type
        $businessType = $_GET['businessType'];
        $view = $_GET['isView'];
        $dropDownCss = '';
        if($view==1){
            $dropDownCss = "disabled"; 
        }
        
        $str = '';
        $selected = 'selected';
        if ($businessType == 'Customer') {
            $str = "<select name='business_category' id='business_category' onchange='removeErroMsgNew();' class='basic-single select-form-mro' $dropDownCss>"
                    . "<option value=''>Select Business Partner Category</option>"
                    . "<option value='Government'>Government</option>"
                    . "<option value='Corporate'>Corporate</option>"
                    . "<option value='Distributor'>Distributor</option></select>";
        } else {
            $str = "<select name='business_category' id='business_category' onchange='removeErroMsgNew();' class='basic-single select-form-mro' $dropDownCss>"
                    . "<option value=''>Select Business Partner Category</option>"
                    . "<option value='Supplier'>Supplier</option>"
                    . "<option value='Freight Forwarder'>Freight Forwarder</option>"
                    . "<option value='Customs Broker'>Customs Broker</option>"
                    . "<option value='Inspection Agent'>Inspection Agent</option>"
                    . "<option value='Manufacturer'>Manufacturer</option>"
                    . "<option value='Distributor'>Distributor</option>"
                    . "<option value='Commission Agent'>Commission Agent</option></select>";
        }
        echo json_encode(array('dropdown' => $str));
        exit;
    }

    function getState() {
        //get state dropdown
        $country_id = $_GET['country_id'];
        $idname = $_GET['idname'];
        $selected_id = $_GET['selected_id'];
        $cnt = $_GET['cnt'];
        $is_bill = $_GET['is_bill'];
        $stateDetail = array();
        $data = $this->common->getData("tbl_state", "state_id,state_name", array("country_id" => (int) $country_id));
        $statestr = '<select name="' . $idname . '" id="' . $idname . '" class="basic-single select-form-mro"  onchange="getCity(this.value,' . $cnt . ',' . $is_bill . ')" >'
                . '<option value="">Select State</option>';
        if ($country_id > 0) {
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    if (!empty($selected_id) && $selected_id == $value['state_id']) {
                        $srt = "selected";
                    } else {
                        $srt = '';
                    }
                    $statestr = $statestr . '<option value="' . $value['state_id'] . '"  ' . $srt . ' >' . str_replace("'", "", $value['state_name']) . '</option>';
                }
            }
            $statestr = $statestr . '</select>';
        }
        echo json_encode(array('state' => $statestr));
        exit;
    }

    function getCity() {
        //get city dropdown
        $state_id = $_GET['state_id'];
        $idname = $_GET['idname'];
        $selected_id = $_GET['selected_id'];
        $cnt = $_GET['cnt'];
        $stateDetail = array();
        $data = $this->common->getData("tbl_city", "city_id,city_name", array("state_id" => (int) $state_id));
        $statestr = '<select name="' . $idname . '" id="' . $idname . '" class="basic-single select-form-mro">'
                . '<option value="">Select City</option>';
        if ($state_id > 0) {
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    if (!empty($selected_id) && $selected_id == $value['city_id']) {
                        $srt = "selected";
                    } else {
                        $srt = '';
                    }
                    $statestr = $statestr . '<option value="' . $value['city_id'] . '"  ' . $srt . ' >' . str_replace("'", "", $value['city_name']) . '</option>';
                }
            }
            $statestr = $statestr . '</select>';
        }
        echo json_encode(array('city' => $statestr));
        exit;
    }

}

?>
