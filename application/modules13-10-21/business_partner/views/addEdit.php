<?php
//start code of view option    
$dropDownCss = '';
$inputFieldCss = '';
if ($view == 1) {
  $dropDownCss = "disabled";
  $inputFieldCss = "readonly";
}
//end code of view option
?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
          <div class="title-sec-wrapper">
            <div class="title-sec-left">
              <div class="breadcrumb">
                <a href="<?php echo base_url("business_partner"); ?>">Business Partners</a>
                <span>></span>
                <p>Add New Business Partner</p>
              </div>
              <div class="page-title-wrapper">
                <h1 class="page-title">Add New Business Partner</h1>
              </div>
            </div>
            <?php if ($view == 0) { ?>
              <button type="submit" class="btn-primary-mro">Save</button>
            <?php } ?>
          </div>
          <div class="page-content-wrapper">
            <input type="hidden" name="business_partner_id" id="business_partner_id" value="<?php echo (!empty($business_partner_details['business_partner_id'])) ? $business_partner_details['business_partner_id'] : ""; ?>">
            <div class="scroll-content-wrapper">
              <div class="scroll-content-left" id="bp-details">
                <div class="form-row form-row-3" id="section-mro-1">
                  <div class="form-group">
                    <label for="business_name">Business Name<sup>*</sup></label>
                    <input type="text" name="business_name" id="business_name" value="<?php echo (!empty($business_partner_details['business_name'])) ? $business_partner_details['business_name'] : ""; ?>" placeholder="Enter Business Name" class="input-form-mro" <?= $inputFieldCss; ?>>
                  </div>
                  <div class="form-group">
                    <label for="business_type">Business Partner Type<sup>*</sup></label>
                    <select name="business_type" id="business_type" class="basic-single select-form-mro" <?= $dropDownCss; ?> onchange="getCategory(this.value);removeErroMsg('business_type');">
                      <option value="">Select Business Partner Type</option>
                      <option value="Customer" <?php echo (!empty($business_partner_details['business_type']) && $business_partner_details['business_type'] == "Customer") ? "selected" : ""; ?>>Customer</option>
                      <option value="Vendor" <?php echo (!empty($business_partner_details['business_type']) && $business_partner_details['business_type'] == "Vendor") ? "selected" : ""; ?>>Vendor</option>
                    </select>
                    <!-- <label id="business_type-error" class="error" for="business_type"></label> -->
                  </div>
                  <div class="form-group">
                    <label for="business_category">Business Partner Category<sup>*</sup></label>
                    <div id="business_category_div">
                      <select name="business_category" id="business_category" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                        <option value="">Select Business Partner Category</option>
                      </select>
                      <!--  <label id="business_category-error" class="error" for="business_category"></label> -->
                    </div>
                  </div>
                </div>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="contact_person">Contact Person</label>
                    <input type="text" name="contact_person" id="contact_person" value="<?php echo (!empty($business_partner_details['contact_person'])) ? $business_partner_details['contact_person'] : ""; ?>" placeholder="Enter Contact Person Name" class="input-form-mro" <?= $inputFieldCss; ?>>
                  </div>
                  <div class="form-group">
                    <label for="">Email Address</label>
                    <input type="text" name="email" id="email" value="<?php echo (!empty($business_partner_details['email'])) ? $business_partner_details['email'] : ""; ?>" placeholder="Enter Email Address" class="input-form-mro" <?= $inputFieldCss; ?>>
                  </div>
                  <div class="form-group">
                    <label for="">Phone Number</label>
                    <input type="text" name="phone_number" id="phone_number" value="<?php echo (!empty($business_partner_details['phone_number'])) ? $business_partner_details['phone_number'] : ""; ?>" placeholder="Enter Phone Number" class="input-form-mro" <?= $inputFieldCss; ?>>
                  </div>
                </div>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Business Alias<sup>*</sup></label>
                    <input type="text" name="alias" id="alias" value="<?php echo (!empty($business_partner_details['alias'])) ? $business_partner_details['alias'] : ""; ?>" placeholder="Enter Business Alias" class="input-form-mro" <?= $inputFieldCss; ?>>
                  </div>
                  <div class="form-group">
                    <label for="Status">Status</label>
                    <select name="status" id="status" class="select-form-mro">
                      <option value="Active" <?php echo (!empty($business_partner_details['status']) && $business_partner_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                      <option value="In-active" <?php echo (!empty($business_partner_details['status']) && $business_partner_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <hr class="separator mt0" id="section-mro-2">
                  <h3 class="form-group-title">Billing Details</h3>
                  <?php if (empty($billing_details)) { ?>
                    <table class="product_location1 disable_" id="admore_inner_table_0" style="width:100%;">
                      <tbody>
                        <tr tr_inner_count="0" class="active">
                          <td>
                            <div class="form-row form-row-3">
                              <div class="form-group">
                                <label for="">Contact Person</label>
                                <input type="text" name="billing_person[0]" id="billing_person0" placeholder="Enter Contact Person" class="input-form-mro" <?= $inputFieldCss; ?>>
                                <input type="hidden" name="billing_details_id[0]" id="billing_details_id0" value="0">
                              </div>
                              <div class="form-group">
                                <label for="">Contact Phone</label>
                                <input type="text" name="billing_phone[0]" id="billing_phone0" placeholder="Enter Contact Phone" class="input-form-mro" <?= $inputFieldCss; ?>>
                              </div>
                              <div class="form-group">
                                <label for="">Contact Email</label>
                                <input type="text" name="billing_email[0]" id="billing_email0" placeholder="Enter Contact Email" class="input-form-mro" <?= $inputFieldCss; ?>>
                              </div>
                            </div>
                            <div class="form-row form-row-2">
                              <div class="form-group">
                                <label for="">Address Line 1</label>
                                <input type="text" name="billing_line_one[0]" id="billing_line_one0" placeholder="Enter Contact Person" class="input-form-mro" <?= $inputFieldCss; ?>>
                              </div>
                              <div class="form-group">
                                <label for="">Address Line 2</label>
                                <input type="text" name="billing_line_two[0]" id="billing_line_two0" placeholder="Enter Contact Phone" class="input-form-mro" <?= $inputFieldCss; ?>>
                              </div>
                            </div>
                            <div class="form-row form-row-4">
                              <div class="form-group">
                                <label for="">Country</label>
                                <select name="billing_country[0]" id="billing_country0" class="basic-single select-form-mro" onchange="getState(this.value, '0', '1')" <?= $dropDownCss; ?>>
                                  <option value="0">Select Country</option>
                                  <?php
                                  foreach ($country as $countryValue) {
                                  ?>
                                    <option value="<?php echo $countryValue['country_id']; ?>">
                                      <?php echo $countryValue['country_name']; ?>
                                    </option>
                                  <?php
                                  }
                                  ?>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="">State</label>
                                <div id="bill_state_div_0">
                                  <select name="billing_state[0]" id="billing_state0" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                                    <option value="0">Select State</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="">City</label>
                                <div id="bill_city_div_0">
                                  <select name="billing_city[0]" id="billing_city0" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                                    <option value="0">Select City</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="">Zip Code</label>
                                <input type="text" name="billing_zipcode[0]" id="billing_zipcode0" placeholder="Enter Zip Code" class="input-form-mro" <?= $inputFieldCss; ?>>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group">
                                <button class="btn btn-sm add_more_inner " panel-count="0" type="button"><img src="<?php echo base_url(); ?>assets/images/add-more-dark.svg" alt="" class="add-more-img">&nbsp&nbspAdd</button>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  <?php } else { ?>
                    <table class="table  product_location1 table-bordered table-striped disable_" id="admore_inner_table_0">
                      <tbody>
                        <?php foreach ($billing_details as $key => $value) { ?>
                          <tr tr_inner_count="<?= $key ?>" class="active">
                            <td>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">Contact Person</label>
                                  <input type="text" name="billing_person[<?= $key ?>]" id="billing_person<?= $key ?>" value="<?= (!empty($billing_details[$key]['contact_person'])) ? $billing_details[$key]['contact_person'] : '' ?>" placeholder="Enter Contact Person" class="input-form-mro" <?= $inputFieldCss; ?>>
                                  <input type="hidden" name="billing_details_id[<?= $key ?>]" id="billing_details_id<?= $key ?>" value="<?= (!empty($billing_details[$key]['billing_details_id'])) ? $billing_details[$key]['billing_details_id'] : '0' ?>">
                                </div>
                                <div class="form-group">
                                  <label for="">Contact Phone</label>
                                  <input type="text" name="billing_phone[<?= $key ?>]" id="billing_phone<?= $key ?>" value="<?= (!empty($billing_details[$key]['number'])) ? $billing_details[$key]['number'] : '' ?>" placeholder="Enter Contact Phone" class="input-form-mro" <?= $inputFieldCss; ?>>
                                </div>
                                <div class="form-group">
                                  <label for="">Contact Email</label>
                                  <input type="text" name="billing_email[<?= $key ?>]" id="billing_email<?= $key ?>" value="<?= (!empty($billing_details[$key]['email'])) ? $billing_details[$key]['email'] : '' ?>" placeholder="Enter Contact Email" class="input-form-mro" <?= $inputFieldCss; ?>>
                                </div>
                              </div>
                              <div class="form-row form-row-2">
                                <div class="form-group">
                                  <label for="">Address Line 1</label>
                                  <input type="text" name="billing_line_one[<?= $key ?>]" id="billing_line_one<?= $key ?>" value="<?= (!empty($billing_details[$key]['address_line_1'])) ? $billing_details[$key]['address_line_1'] : '' ?>" placeholder="Enter Contact Person" class="input-form-mro" <?= $inputFieldCss; ?>>
                                </div>
                                <div class="form-group">
                                  <label for="">Address Line 2</label>
                                  <input type="text" name="billing_line_two[<?= $key ?>]" id="billing_line_two<?= $key ?>" value="<?= (!empty($billing_details[$key]['address_line_2'])) ? $billing_details[$key]['address_line_2'] : '' ?>" placeholder="Enter Contact Phone" class="input-form-mro" <?= $inputFieldCss; ?>>
                                </div>
                              </div>
                              <div class="form-row form-row-4">
                                <div class="form-group">
                                  <label for="">Country</label>
                                  <select name="billing_country[<?= $key ?>]" id="billing_country<?= $key ?>" onchange="getState(this.value,<?= $key ?>, '1')" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                                    <option value="0">Select Country</option>
                                    <?php
                                    $countryID = (isset($billing_details[$key]['country_id']) ? $billing_details[$key]['country_id'] : 0);
                                    foreach ($country as $countryValue) {
                                    ?>
                                      <option value="<?php echo $countryValue['country_id']; ?>" <?php if ($countryValue['country_id'] == $countryID) {
                                                                                                    echo "selected";
                                                                                                  } ?>>
                                        <?php echo $countryValue['country_name']; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="">State</label>
                                  <div id="bill_state_div_<?= $key ?>">
                                    <select name="billing_state[<?= $key ?>]" id="billing_state<?= $key ?>" onchange="getCity(this.value,<?= $key ?>, 1)" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                                      <option value="0">Select State</option>
                                      <?php
                                      $stateID = (isset($billing_details[$key]['state_id']) ? $billing_details[$key]['state_id'] : 0);
                                      foreach ($billingState as $stateValue) {
                                        if ($countryID != $stateValue['country_id']) {
                                          continue;
                                        }
                                      ?>
                                        <option value="<?php echo $stateValue['state_id']; ?>" <?php if ($stateValue['state_id'] == $stateID) {
                                                                                                  echo "selected";
                                                                                                } ?>>
                                          <?php echo $stateValue['state_name']; ?>
                                        </option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="">City</label>
                                  <div id="bill_city_div_<?= $key ?>">
                                    <select name="billing_city[<?= $key ?>]" id="billing_city<?= $key ?>" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                                      <option value="0">Select City</option>
                                      <?php
                                      foreach ($billingCity as $cityValue) {
                                        if ($stateID != $cityValue['state_id']) {
                                          continue;
                                        }
                                        $cityID = (isset($billing_details[$key]['city_id']) ? $billing_details[$key]['city_id'] : 0);
                                      ?>
                                        <option value="<?php echo $cityValue['city_id']; ?>" <?php if ($cityValue['city_id'] == $cityID) {
                                                                                                echo "selected";
                                                                                              } ?>>
                                          <?php echo $cityValue['city_name']; ?>
                                        </option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="">Zip Code</label>
                                  <input type="text" name="billing_zipcode[<?= $key ?>]" id="billing_zipcode<?= $key ?>" value="<?= (!empty($billing_details[$key]['zipcode'])) ? $billing_details[$key]['zipcode'] : '' ?>" placeholder="Enter Zip Code" class="input-form-mro" <?= $inputFieldCss; ?>>
                                </div>
                              </div>
                              <div class="form-row">
                                <div class="form-group">
                                  <?php
                                  if ($view == 0) {
                                    if ($key == '0') { ?>
                                      <button class="btn btn-sm add_more_inner " panel-count="0" type="button"><img src="<?php echo base_url(); ?>assets/images/add-more-dark.svg" alt="" class="add-more-img">&nbsp&nbspAdd</button>
                                    <?php } else { ?>
                                      <button title="Remove" class="btn btn-sm remove inner_remove" type="button"><img src="<?php echo base_url(); ?>assets/images/remove-dark.svg" alt="" class="add-more-img">&nbsp&nbspRemove</button>
                                  <?php
                                    }
                                  }
                                  ?>
                                </div>
                              </div>
                            </td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  <?php } ?>
                </div>

                <div class="form-group">
                  <hr class="separator mt0" id="section-mro-3">
                  <h3 class="form-group-title">Shipping or Pick Up Details</h3>
                  <?php if (empty($shippingDetail)) { ?>
                    <table class="product_location1 disable_" id="ship_admore_inner_table_0" style="width:100%;">
                      <tbody>
                        <tr tr_inner_count="0" class="active">
                          <td>
                            <div class="form-row form-row-3">
                              <div class="form-group">
                                <label for="">Contact Person</label>
                                <input type="text" name="shipping_person[0]" id="shipping_person0" placeholder="Enter Contact Person" class="input-form-mro" <?= $inputFieldCss; ?>>
                                <input type="hidden" name="shipping_details_id[0]" id="shipping_details_id0" value="0">
                              </div>
                              <div class="form-group">
                                <label for="">Contact Phone</label>
                                <input type="text" name="shipping_phone[0]" id="shipping_phone0" placeholder="Enter Contact Phone" class="input-form-mro" <?= $inputFieldCss; ?>>
                              </div>
                              <div class="form-group">
                                <label for="">Contact Email</label>
                                <input type="text" name="shipping_email[0]" id="shipping_email0" placeholder="Enter Contact Email" class="input-form-mro" <?= $inputFieldCss; ?>>
                              </div>
                            </div>
                            <div class="form-row form-row-2">
                              <div class="form-group">
                                <label for="">Address Line 1</label>
                                <input type="text" name="shipping_line_one[0]" id="shipping_line_one0" placeholder="Enter Contact Person" class="input-form-mro" <?= $inputFieldCss; ?>>
                              </div>
                              <div class="form-group">
                                <label for="">Address Line 2</label>
                                <input type="text" name="shipping_line_two[0]" id="shipping_line_two0" placeholder="Enter Contact Phone" class="input-form-mro" <?= $inputFieldCss; ?>>
                              </div>
                            </div>
                            <div class="form-row form-row-4">
                              <div class="form-group">
                                <label for="">Country</label>
                                <select name="shipping_country[0]" id="shipping_country0" class="basic-single select-form-mro" onchange="getState(this.value, '0', '0')" <?= $dropDownCss; ?>>
                                  <option value="0">Select Country</option>
                                  <?php
                                  foreach ($country as $countryValue) {
                                  ?>
                                    <option value="<?php echo $countryValue['country_id']; ?>">
                                      <?php echo $countryValue['country_name']; ?>
                                    </option>
                                  <?php
                                  }
                                  ?>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="">State</label>
                                <div id="ship_state_div_0">
                                  <select name="shipping_state[0]" id="shipping_state0" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                                    <option value="0">Select State</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="">City</label>
                                <div id="ship_city_div_0">
                                  <select name="shipping_city[0]" id="shipping_city0" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                                    <option value="0">Select City</option>
                                  </select>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="">Zip Code</label>
                                <input type="text" name="shipping_zipcode[0]" id="shipping_zipcode0" placeholder="Enter Zip Code" class="input-form-mro" <?= $inputFieldCss; ?>>
                              </div>
                            </div>
                            <div class="form-row">
                              <div class="form-group">
                                <button class="btn btn-sm ship_add_more_inner " panel-count="0" type="button"><img src="<?php echo base_url(); ?>assets/images/add-more-dark.svg" alt="" class="add-more-img">&nbsp&nbspAdd</button>
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  <?php } else { ?>
                    <table class="table  product_location1 table-bordered table-striped disable_" id="ship_admore_inner_table_0">
                      <tbody>
                        <?php foreach ($shippingDetail as $key => $value) { ?>
                          <tr tr_inner_count="<?= $key ?>" class="active">
                            <td>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">Contact Person</label>
                                  <input type="text" name="shipping_person[<?= $key ?>]" id="shipping_person<?= $key ?>" value="<?= (!empty($shippingDetail[$key]['contact_person'])) ? $shippingDetail[$key]['contact_person'] : '' ?>" placeholder="Enter Contact Person" class="input-form-mro" <?= $inputFieldCss; ?>>
                                  <input type="hidden" name="shipping_details_id[<?= $key ?>]" id="shipping_details_id<?= $key ?>" value="<?= (!empty($shippingDetail[$key]['shipping_details_id'])) ? $shippingDetail[$key]['shipping_details_id'] : '' ?>">
                                </div>
                                <div class="form-group">
                                  <label for="">Contact Phone</label>
                                  <input type="text" name="shipping_phone[<?= $key ?>]" id="shipping_phone<?= $key ?>" value="<?= (!empty($shippingDetail[$key]['number'])) ? $shippingDetail[$key]['number'] : '' ?>" placeholder="Enter Contact Phone" class="input-form-mro" <?= $inputFieldCss; ?>>
                                </div>
                                <div class="form-group">
                                  <label for="">Contact Email</label>
                                  <input type="text" name="shipping_email[<?= $key ?>]" id="shipping_email<?= $key ?>" value="<?= (!empty($shippingDetail[$key]['email'])) ? $shippingDetail[$key]['email'] : '' ?>" placeholder="Enter Contact Email" class="input-form-mro" <?= $inputFieldCss; ?>>
                                </div>
                              </div>
                              <div class="form-row form-row-2">
                                <div class="form-group">
                                  <label for="">Address Line 1</label>
                                  <input type="text" name="shipping_line_one[<?= $key ?>]" id="shipping_line_one<?= $key ?>" value="<?= (!empty($shippingDetail[$key]['address_line_1'])) ? $shippingDetail[$key]['address_line_1'] : '' ?>" placeholder="Enter Contact Person" class="input-form-mro" <?= $inputFieldCss; ?>>
                                </div>
                                <div class="form-group">
                                  <label for="">Address Line 2</label>
                                  <input type="text" name="shipping_line_two[<?= $key ?>]" id="shipping_line_two<?= $key ?>" value="<?= (!empty($shippingDetail[$key]['address_line_2'])) ? $shippingDetail[$key]['address_line_2'] : '' ?>" placeholder="Enter Contact Phone" class="input-form-mro" <?= $inputFieldCss; ?>>
                                </div>
                              </div>
                              <div class="form-row form-row-4">
                                <div class="form-group">
                                  <label for="">Country</label>
                                  <select name="shipping_country[<?= $key ?>]" id="shipping_country<?= $key ?>" onchange="getState(this.value,<?= $key ?>, 0)" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                                    <option value="0">Select Country</option>
                                    <?php
                                    $countryID = (isset($shippingDetail[$key]['country_id']) ? $shippingDetail[$key]['country_id'] : 0);
                                    foreach ($country as $countryValue) {
                                    ?>
                                      <option value="<?php echo $countryValue['country_id']; ?>" <?php if ($countryValue['country_id'] == $countryID) {
                                                                                                    echo "selected";
                                                                                                  } ?>>
                                        <?php echo $countryValue['country_name']; ?>
                                      </option>
                                    <?php
                                    }
                                    ?>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="">State</label>
                                  <div id="ship_state_div_<?= $key ?>">
                                    <select name="shipping_state[<?= $key ?>]" id="shipping_state<?= $key ?>" onchange="getCity(this.value,<?= $key ?>, 0)" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                                      <option value="0">Select State</option>
                                      <?php
                                      $stateID = (isset($shippingDetail[$key]['state_id']) ? $shippingDetail[$key]['state_id'] : 0);
                                      foreach ($billingState as $stateValue) {
                                        if ($countryID != $stateValue['country_id']) {
                                          continue;
                                        }
                                      ?>
                                        <option value="<?php echo $stateValue['state_id']; ?>" <?php if ($stateValue['state_id'] == $stateID) {
                                                                                                  echo "selected";
                                                                                                } ?>>
                                          <?php echo $stateValue['state_name']; ?>
                                        </option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="">City</label>
                                  <div id="ship_city_div_<?= $key ?>">
                                    <select name="shipping_city[<?= $key ?>]" id="shipping_city<?= $key ?>" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                                      <option value="0">Select City</option>
                                      <?php
                                      foreach ($billingCity as $cityValue) {
                                        if ($stateID != $cityValue['state_id']) {
                                          continue;
                                        }
                                        $cityID = (isset($shippingDetail[$key]['city_id']) ? $shippingDetail[$key]['city_id'] : 0);
                                      ?>
                                        <option value="<?php echo $cityValue['city_id']; ?>" <?php if ($cityValue['city_id'] == $cityID) {
                                                                                                echo "selected";
                                                                                              } ?>>
                                          <?php echo $cityValue['city_name']; ?>
                                        </option>
                                      <?php
                                      }
                                      ?>
                                    </select>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="">Zip Code</label>
                                  <input type="text" name="shipping_zipcode[<?= $key ?>]" id="shipping_zipcode<?= $key ?>" value="<?= (!empty($shippingDetail[$key]['zipcode'])) ? $shippingDetail[$key]['zipcode'] : '' ?>" placeholder="Enter Zip Code" class="input-form-mro" <?= $inputFieldCss; ?>>
                                </div>
                              </div>
                              <div class="form-row">
                                <div class="form-group">
                                  <?php
                                  if ($view == 0) {
                                    if ($key == '0') { ?>
                                      <button class="btn btn-sm ship_add_more_inner " panel-count="0" type="button"><img src="<?php echo base_url(); ?>assets/images/add-more-dark.svg" alt="" class="add-more-img">&nbsp&nbspAdd</button>
                                    <?php } else { ?>
                                      <button title="Remove" class="btn btn-sm remove ship_inner_remove" type="button"><img src="<?php echo base_url(); ?>assets/images/remove-dark.svg" alt="" class="add-more-img">&nbsp&nbspRemove</button>
                                  <?php }
                                  }
                                  ?>
                                </div>
                              </div>
                            </td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  <?php } ?>
                </div>
                <div id="section-mro-4"></div>

                <hr class="separator mt0" >
                
                <h3 class="form-group-title">Terms</h3>
                <div class="form-row form-row-2">
                  <div class="form-group">
                    <label for="">Payment Terms</label>
                    <select name="payment_terms_id" id="payment_terms_id" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                      <option value="">Select Payment Terms</option>
                      <?php
                      foreach ($paymentTermData as $value) {
                        $paymentTermsID = (isset($business_partner_details['payment_terms_id']) ? $business_partner_details['payment_terms_id'] : 0);
                      ?>
                        <option value="<?php echo $value['payment_term_id']; ?>" <?php
                                                                                  if ($value['payment_term_id'] == $paymentTermsID) {
                                                                                    echo "selected";
                                                                                  }
                                                                                  ?>>
                          <?php echo $value['payment_term_value']; ?>
                        </option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="">Incoterm</label>
                    <select name="incoterms_id" id="incoterms_id" class="basic-single select-form-mro" <?= $dropDownCss; ?>>
                      <option value="">Select Incoterm</option>
                      <?php
                      foreach ($incotermData as $value) {
                        $incotermsID = (isset($business_partner_details['incoterms_id']) ? $business_partner_details['incoterms_id'] : 0);
                      ?>
                        <option value="<?php echo $value['incoterms_id']; ?>" <?php
                                                                              if ($value['incoterms_id'] == $incotermsID) {
                                                                                echo "selected";
                                                                              }
                                                                              ?>>
                          <?php echo $value['incoterms_value']; ?>
                        </option>
                      <?php
                      }
                      ?>
                    </select>
                  </div>
                </div>
                <span id="section-mro-5"></span>
              </div>
              <div class="scroll-content-right">
                <div class="vertical-scroll-links">
                  <a href="#section-mro-1" class="active">Business Partner Detail</a>
                  <a href="#section-mro-2">Billing Address</a>
                  <a href="#section-mro-3">Shipping / Pick Up Address</a>
                  <a href="#section-mro-4">Terms</a>
                  <a href="#section-mro-5" class="d-none"></a>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
<script>
  //state dropdown code for billing and shipping details
  function getState(country_id = 0, cnt = 0, is_bill = 0, selected_id = '') {
    if (is_bill == 0) {
      var ids = 'shipping_state[' + cnt + ']';
    } else {
      var ids = 'billing_state[' + cnt + ']';
    }
    var act = "<?php echo base_url(); ?>business_partner/getState";
    $.ajax({
      type: 'GET',
      url: act,
      data: {
        country_id: country_id,
        idname: ids,
        selected_id: selected_id,
        cnt: cnt,
        is_bill: is_bill
      },
      dataType: "json",
      success: function(data) {
        if (is_bill == 0) {
          $('#ship_state_div_' + cnt).html(data.state);
        } else {
          $('#bill_state_div_' + cnt).html(data.state);
        }
        $('.basic-single').select2();
      }
    });
    getCity(0, cnt, is_bill, selected_id = '');
  }

  //city dropdown code for billing and shipping details
  function getCity(state_id = 0, cnt = 0, is_bill = 0, selected_id = '') {
    if (is_bill == 0) {
      var ids = 'shipping_city[' + cnt + ']';
    } else {
      var ids = 'billing_city[' + cnt + ']';
    }
    var act = "<?php echo base_url(); ?>business_partner/getCity";
    $.ajax({
      type: 'GET',
      url: act,
      data: {
        state_id: state_id,
        idname: ids,
        selected_id: selected_id,
        cnt: cnt
      },
      dataType: "json",
      success: function(data) {
        if (is_bill == 0) {
          $('#ship_city_div_' + cnt).html(data.city);
        } else {
          $('#bill_city_div_' + cnt).html(data.city);
        }
        $('.basic-single').select2();
      }
    });
  }
</script>
<script>
  //add more for billing details
  $(document).on("click", ".add_more_inner", function() {
    var x = $(this).attr("panel-count");
    // alert(x);
    // return;
    var tx = $("#admore_inner_table_" + x + " tbody tr:last-child").attr("tr_inner_count");
    tx++;
    if (isNaN(tx) || tx == "undefined") {
      tx = 0;
    }

    var inner_tr = '<tr tr_inner_count="' + tx + '" class="active">' +
      '<td>' +
      '<div class="form-row form-row-3">' +
      '<div class="form-group">' +
      '<label for="">Contact Person</label>' +
      '<input type="text" name="billing_person[' + tx + ']" id="billing_person' + tx + '" placeholder="Enter Contact Person" class="input-form-mro">' +
      '<input type="hidden" name="billing_details_id[' + tx + ']" id="billing_details_id' + tx + '" value="0">' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">Contact Phone</label>' +
      '<input type="text" name="billing_phone[' + tx + ']" id="billing_phone' + tx + '" placeholder="Enter Contact Phone" class="input-form-mro">' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">Contact Email</label>' +
      '<input type="text" name="billing_email[' + tx + ']" id="billing_email' + tx + '" placeholder="Enter Contact Email" class="input-form-mro">' +
      '</div>' +
      '</div>' +
      '<div class="form-row form-row-2">' +
      '<div class="form-group">' +
      '<label for="">Address Line 1</label>' +
      '<input type="text" name="billing_line_one[' + tx + ']" id="billing_line_one' + tx + '" placeholder="Enter Contact Person" class="input-form-mro">' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">Address Line 2</label>' +
      '<input type="text" name="billing_line_two[' + tx + ']" id="billing_line_two' + tx + '" placeholder="Enter Contact Phone" class="input-form-mro">' +
      '</div>' +
      '</div>' +
      '<div class="form-row form-row-4">' +
      '<div class="form-group">' +
      '<label for="">Country</label>' +
      '<select name="billing_country[' + tx + ']" id="billing_country' + tx + '" class="basic-single select-form-mro" onchange="getState(this.value,' + tx + ',1)" >' +
      '<option value="0">Select Country</option>' +
      <?php
      foreach ($country as $countryValue) {
      ?> '<option value="<?php echo $countryValue['country_id']; ?>">' +
        '<?php echo str_replace("'", "", $countryValue['country_name']); ?>' +
        '</option>' +
      <?php } ?> '</select>' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">State</label>' +
      '<div id="bill_state_div_' + tx + '"><select name="billing_state[' + tx + ']" id="billing_state' + tx + '" class="basic-single select-form-mro">' +
      '<option value="">Select State</option>' +
      '</select></div>' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">City</label>' +
      '<div id="bill_city_div_' + tx + '"><select name="billing_city[' + tx + ']" id="billing_city' + tx + '" class="basic-single select-form-mro">' +
      '<option value="">Select City</option>' +
      '</select></div>' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">Zip Code</label>' +
      '<input type="text" name="billing_zipcode[' + tx + ']" id="billing_zipcode' + tx + '" placeholder="Enter Zip Code" class="input-form-mro">' +
      '</div>' +
      '</div>' +
      '<div class="form-row">' +
      '<div class="form-group">' +
      '<button title="Remove" class="btn btn-sm remove inner_remove" type="button"><img src="<?php echo base_url(); ?>assets/images/remove-dark.svg" alt="" class="add-more-img">&nbsp&nbspRemove</button>' +
      '</div>' +
      '</div>' +
      '</td>' +
      '</tr>';
    $("#admore_inner_table_" + x + " tbody").append(inner_tr);
    $('.basic-single').select2();
  });
  //remove billing details
  $(document).on("click", '.inner_remove', function() {
    var tr_inner_count = $(this).closest('tr').attr('tr_inner_count');
    if (tr_inner_count != 0) {
      $(this).closest('tr').remove();
    } else {
      swal({
        title: "Alert!",
        text: "<p class='font-bold col-red'>Atleast one option is required..</p>",
        timer: 2000,
        showConfirmButton: false,
        html: true
      });
    }
  });

  //add more for shipping details
  $(document).on("click", ".ship_add_more_inner", function() {
    var x = $(this).attr("panel-count");
    // alert(x);
    // return;
    var tx = $("#ship_admore_inner_table_" + x + " tbody tr:last-child").attr("tr_inner_count");
    tx++;
    if (isNaN(tx) || tx == "undefined") {
      tx = 0;
    }

    var inner_tr = '<tr tr_inner_count="' + tx + '" class="active">' +
      '<td>' +
      '<div class="form-row form-row-3">' +
      '<div class="form-group">' +
      '<label for="">Contact Person</label>' +
      '<input type="text" name="shipping_person[' + tx + ']" id="shipping_person' + tx + '" placeholder="Enter Contact Person" class="input-form-mro">' +
      '<input type="hidden" name="shipping_details_id[' + tx + ']" id="shipping_details_id' + tx + '" value="0">' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">Contact Phone</label>' +
      '<input type="text" name="shipping_phone[' + tx + ']" id="shipping_phone' + tx + '" placeholder="Enter Contact Phone" class="input-form-mro">' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">Contact Email</label>' +
      '<input type="text" name="shipping_email[' + tx + ']" id="shipping_email' + tx + '" placeholder="Enter Contact Email" class="input-form-mro">' +
      '</div>' +
      '</div>' +
      '<div class="form-row form-row-2">' +
      '<div class="form-group">' +
      '<label for="">Address Line 1</label>' +
      '<input type="text" name="shipping_line_one[' + tx + ']" id="shipping_line_one' + tx + '" placeholder="Enter Contact Person" class="input-form-mro">' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">Address Line 2</label>' +
      '<input type="text" name="shipping_line_two[' + tx + ']" id="shipping_line_two' + tx + '" placeholder="Enter Contact Phone" class="input-form-mro">' +
      '</div>' +
      '</div>' +
      '<div class="form-row form-row-4">' +
      '<div class="form-group">' +
      '<label for="">Country</label>' +
      '<select name="shipping_country[' + tx + ']" id="shipping_country' + tx + '" class="basic-single select-form-mro" onchange="getState(this.value,' + tx + ',0)">' +
      '<option value="0">Select Country</option>' +
      <?php
      foreach ($country as $countryValue) {
      ?> '<option value="<?php echo $countryValue['country_id']; ?>">' +
        '<?php echo str_replace("'", "", $countryValue['country_name']); ?>' +
        '</option>' +
      <?php } ?> '</select>' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">State</label>' +
      '<div id="ship_state_div_' + tx + '"><select name="shipping_state[' + tx + ']" id="shipping_state' + tx + '" class="basic-single select-form-mro">' +
      '<option value="">Select State</option>' +
      '</select></div>' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">City</label>' +
      '<div id="ship_city_div_' + tx + '"><select name="shipping_city[' + tx + ']" id="shipping_city' + tx + '" class="basic-single select-form-mro">' +
      '<option value="">Select City</option>' +
      '</select></div>' +
      '</div>' +
      '<div class="form-group">' +
      '<label for="">Zip Code</label>' +
      '<input type="text" name="shipping_zipcode[' + tx + ']" id="shipping_zipcode' + tx + '" placeholder="Enter Zip Code" class="input-form-mro">' +
      '</div>' +
      '</div>' +
      '<div class="form-row">' +
      '<div class="form-group">' +
      '<button title="Remove" class="btn btn-sm remove inner_remove" type="button"><img src="<?php echo base_url(); ?>assets/images/remove-dark.svg" alt="" class="add-more-img">&nbsp&nbspRemove</button>' +
      '</div>' +
      '</div>' +
      '</td>' +
      '</tr>';
    $("#ship_admore_inner_table_" + x + " tbody").append(inner_tr);
    $('.basic-single').select2();
  });
  //remove shipping details
  $(document).on("click", '.ship_inner_remove', function() {
    var tr_inner_count = $(this).closest('tr').attr('tr_inner_count');
    if (tr_inner_count != 0) {
      $(this).closest('tr').remove();
    } else {
      swal({
        title: "Alert!",
        text: "<p class='font-bold col-red'>Atleast one option is required..</p>",
        timer: 2000,
        showConfirmButton: false,
        html: true
      });
    }
  });
</script>
<script>
  //business partner category dropdown at click of business partner type
  function getCategory(businessType, category = '') {
    if (businessType != '') {
      var act = "<?php echo base_url(); ?>business_partner/getBusinessPartner";
      $.ajax({
        type: 'GET',
        url: act,
        data: {
          businessType: businessType,
          isView: <?= $view ?>
        },
        dataType: "json",
        success: function(data) {
          $('#business_category_div').html(data.dropdown);
          $('#business_category').val(category);
          $('.basic-single').select2();
        }
      });
    }
  }

  //business partner category dropdown in case of edit record
  <?php if (!empty($business_partner_details['business_partner_id'])) { ?>
    getCategory('<?php echo (!empty($business_partner_details['business_type'])) ? $business_partner_details['business_type'] : ""; ?>', '<?php echo (!empty($business_partner_details['business_category'])) ? $business_partner_details['business_category'] : ""; ?>');
  <?php } ?>
</script>
<script>
  function removeErroMsgNew(id = 'business_category') {
    if ($('#' + id + '-error').length) {
      if ($('#' + id).val() != '0' && $('#' + id).val() != '') {
        $('#' + id).removeClass('error');
        $('#' + id + '-error').hide();
      } else {
        $('#' + id).addClass('error');
        $('#' + id + '-error').show();
      }
    }
  }

  $(document).ready(function() {

    var vRules = {
      "business_name": {
        required: true
      },
      "business_type": {
        required: true
      },
      "business_category": {
        required: true
      },
      "alias": {
        required: true
      }
    };
    var vMessages = {
      "business_name": {
        required: "Please enter business name."
      },
      "business_type": {
        required: "Please select business type."
      },
      "business_category": {
        required: "Please select business category."
      },
      "alias": {
        required: "Please enter alias."
      }
    };
    //Check and save data
    $("#addEditForm").validate({
      rules: vRules,
      messages: vMessages,
      errorClass: 'error',
      errorElement: 'label',
      errorPlacement: function(error, e) {
        e.parents('.form-group').append(error);
      },
      submitHandler: function(form) {
        var act = "<?php echo base_url(); ?>business_partner/submitForm";
        $("#addEditForm").ajaxSubmit({
          url: act,
          type: 'post',
          dataType: 'json',
          cache: false,
          clearForm: false,
          beforeSubmit: function(arr, $form, options) {
            $(".btn-primary-mro").hide();
          },
          success: function(response) {
            showInsertUpdateMessage(response.msg, response);
            if (response.success) {
              setTimeout(function() {
                window.location = "<?= base_url('business_partner') ?>";
              }, 3000);
            }
            $(".btn-primary-mro").show();
          }
        });
      }
    });
   
  });
</script>