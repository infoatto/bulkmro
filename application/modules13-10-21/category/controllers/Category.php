<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Category extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('categorymodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("CategoryList")){
            redirect('dashboard');
        }
    }

    function index() {
        $categoryData = array();
        $result = $this->common->getData("tbl_category", "*", "");
        if (!empty($result)) {
            $categoryData['category_details'] = $result[0];
        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('category/index', $categoryData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("CategoryAddEdit")){
            redirect('dashboard');
        }
        //add edit form
        $edit_datas = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $category_id = $url_prams['id'];
            $result = $this->common->getData("tbl_category", "*", array("category_id" => $category_id));
            if (!empty($result)) {
                $edit_datas['category_details'] = $result[0];
            }
        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('category/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        //get record at list
        $get_result = $this->categorymodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->category_name));
                array_push($temp, ucfirst($get_result['query_result'][$i]->status));
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("CategoryAddEdit")){    
                    $actionCol = '<a href="category/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->category_id), '+/', '-_'), '=') . '" title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "category_name = '" . $this->input->post('category_name') . "' ";
            if (!empty($this->input->post("category_id"))) {
                $condition .= " AND category_id <> " . $this->input->post("category_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_category", "category_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['category_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Category name already exist...'));
                exit;
            }

            $data = array();
            $data['category_name'] = $this->input->post('category_name');
            $data['status'] = $this->input->post('status');
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            if (!empty($this->input->post("category_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_category", $data, array("category_id" => $this->input->post("category_id")));
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['category_name'] = $this->input->post('category_name');
                $data['status'] = $this->input->post('status');
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_category", $data, "1");
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }
    
    function importCategory() { 
        if(!$this->privilegeduser->hasPrivilege("CategoryImport")){
            redirect('dashboard');
        }
        require_once('application/libraries/SimpleXLSX.php');

        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["excelfile"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["excelfile"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die;
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) {
                        $name = trim($data[0]);
                        $status = trim($data[1]); 

                        if ($name != "Category Name") {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        if ($status != "Status" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1])){
                            continue;
                        }

                        $name = trim($data[0]);
                        $status = trim($data[1]); 

                        if ($name == "") {
                            $error .= "Category Name cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }
                        if ($status != "Active" && $status != "In-active") {
                            $error .= "Status cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) && empty($data[1])){
                            continue;
                        }

                        $name = str_replace("'", "&#39;", trim($data[0]));
                        $status = str_replace("'", "&#39;", trim($data[1]));

                        $condition = "category_name = '" . trim($name) . "' AND status='" . trim($status) . "' "; 
                        $chk_client_sql = $this->common->Fetch("tbl_category", "category_id", $condition);
                        $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

                        $data = array();
                        $data['category_name'] = trim($name);
                        $data['status'] = trim($status);
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");

                        if (!empty($rs_client[0]['category_id'])) {
                             //update data 
                            $result = $this->common->updateData("tbl_category", $data, array("category_id" => $rs_client[0]['category_id']));                             
                            if ($result) {
                                $imported++;
                            } else {
                                $notimported++;
                            }
                        }else{
                             //insert data 
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $result = $this->common->insertData("tbl_category", $data, "1");
                            if ($result) {
                                $imported++;
                            } else {
                                $notimported++;
                            } 
                        }  
                    }
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed';
            print_r(json_encode($statusData));
        }
    }

}

?>
