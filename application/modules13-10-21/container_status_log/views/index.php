<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard") ?>">Home</a>
                            <span>></span>
                            <p>Dashboard</p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title"><a href="<?php echo base_url("container_status_log"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Containers Status Log</h1>
                        </div>
                    </div>
                    <div class="title-sec-right"></div>
                </div>
                <div class="page-content-wrapper1">
                    <div id="serchfilter" class="filter-sec-wrapper container-status-log-filter">
                        <div class="form-group filter-search dataTables_filter searchFilterClass"> 
                            <select name="sSearch_0" id="sSearch_0" class="searchInput basic-single select-mro select-xl">
                                <option value="">Container Number</option>
                                <?php foreach ($container_data as $value) { ?>
                                <option value="<?=$value['container_id']?>"><?=$value['container_number']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        
                        <div class="form-group filter-search dataTables_filter searchFilterClass"> 
                            <select name="sSearch_1" id="sSearch_1" class="searchInput basic-single select-mro select-xl">
                                <option value="">Container Status</option>
                                <?php foreach ($container_status as $valueStatus) { ?>
                                <option value="<?=$valueStatus['container_status_id']?>"><?=$valueStatus['container_status_name']?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group clear-search-filter">
                            <button class="btn-primary-mro" onclick="clearSearchFilters();">Clear Search</button> 
                        </div>
                    </div>
                </div>
                <div class="bp-list-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped  basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Container Number</th>
                                    <th>Container Status</th>
                                    <th>Updated Date</th>
                                    <th>Updated By</th> 
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>