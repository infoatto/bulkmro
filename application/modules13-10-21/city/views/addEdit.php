<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("city"); ?>">City List</a>
                                <span>></span>
                                <p>Add City</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add City</h1>
                            </div>
                        </div>          
                        <button class="btn-primary-mro">Save</button>
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="city_id" id="city_id" value="<?php echo(!empty($city_details['city_id'])) ? $city_details['city_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="city_name">City Name</label>
                                    <input type="text" name="city_name" id="city_name" class="input-form-mro" value="<?php echo(!empty($city_details['city_name'])) ? $city_details['city_name'] : ""; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="country_name">Country Name</label>
                                    <select name="country_id" id="country_id" class="basic-single select-form-mro"  onchange="getState(this.value)">
                                        <option value="">Please Select</option>
                                        <?php
                                        foreach ($countryData as $value) {
                                            $countryID = (isset($city_details['country_id']) ? $city_details['country_id'] : 0);
                                            ?> 
                                            <option value="<?php echo $value['country_id']; ?>" <?php if ($value['country_id'] == $countryID) {
                                            echo "selected";
                                        } ?>>
                                            <?php echo $value['country_name']; ?> 
                                            </option> 
                                            <?php
                                        }
                                        ?>
                                    </select> 
                                </div>    
                                <div class="form-group">
                                    <label for="state_name">State Name</label>
                                    <div id="state_div">
                                        <select name="state_id" id="state_id" class="basic-single select-form-mro">
                                            <option value="">Please Select</option> 
                                        </select>
                                    </div>
                                </div>  
                            </div>  
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     
<script>
    //code for get state dropdown
    function getState(country_id, selected_id = '') {
        var act = "<?php echo base_url(); ?>city/getState";
        $.ajax({
            type: 'GET',
            url: act,
            data: {country_id: country_id, idname: 'state_id', selected_id: selected_id},
            dataType: "json",
            success: function (data) {
                $('#state_div').html(data.state);
                $('.basic-single').select2();
            }
        });
    }
    
    //code for get state in case of edit city
    <?php if (!empty($city_details['country_id'])) { ?>
            getState(<?= $city_details['country_id'] ?>,<?= $city_details['state_id'] ?>);
    <?php } ?>
</script>
<script>

    $(document).ready(function () {
        var vRules = {
            "city_name": {required: true},
            "state_id": {required: true},
            "country_id": {required: true}
        };
        var vMessages = {
            "city_name": {required: "Please enter city name."},
            "state_id": {required: "Please select state name."},
            "country_id": {required: "Please select country name."}
        };
        //check and save city data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function(error, e) {
              e.parents('.form-group').append(error);
            },
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>city/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('city') ?>";
                            }, 3000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });
</script>