<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class City extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('citymodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("CityList")){
            redirect('dashboard');
        }
    }

    function index() {
        $cityData = array(); 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('city/index', $cityData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("CityAddEdit")){
            redirect('dashboard');
        }
        //code for add edit form
        $edit_datas = array();
        $city_id = '';
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $city_id = $url_prams['id'];
            $result = $this->common->getData("tbl_city", "*", array("city_id" => $city_id));
            if (!empty($result)) {
                $edit_datas['city_details'] = $result[0];
            }
        }
        //country
        $countryData = $this->common->getData("tbl_country", "country_id,country_name", "");
        if (!empty($countryData)) {
            $edit_datas['countryData'] = $countryData;
        } 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('city/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        //code for get city record at list
        $get_result = $this->citymodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->city_name));
                array_push($temp, $get_result['query_result'][$i]->country_name);
                array_push($temp, $get_result['query_result'][$i]->state_name);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("CityAddEdit")){      
                    $actionCol = '<a href="city/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->city_id), '+/', '-_'), '=') . '" title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "city_name = '" . $this->input->post('city_name') . "' ";
            if (!empty($this->input->post("city_id"))) {
                $condition .= " AND city_id <> " . $this->input->post("city_id");
            }

            if (!empty($this->input->post("country_id"))) {
                $condition .= " AND country_id <> " . $this->input->post("country_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_city", "city_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['city_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'City name already exist...'));
                exit;
            }

            $data = array();
            $data['city_name'] = $this->input->post('city_name');
            $data['country_id'] = $this->input->post('country_id');
            $data['state_id'] = $this->input->post('state_id');
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            if (!empty($this->input->post("city_id"))) {
                //update city
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_city", $data, array("city_id" => $this->input->post("city_id")));
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert city
                $data['city_name'] = $this->input->post('city_name');
                $data['country_id'] = $this->input->post('country_id');
                $data['state_id'] = $this->input->post('state_id');
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_city", $data, "1");
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }

    function getState() {
        //code for get state dropdown
        $country_id = $_GET['country_id'];
        $idname = $_GET['idname'];
        $selected_id = $_GET['selected_id'];
        $stateDetail = array();
        $stateDetail = $this->common->getData("tbl_state", "state_id,state_name", array("country_id" => (int) $country_id));
        //get state dropdown from core helper
        $statestr = getStateData($country_id, $idname, $stateDetail, $selected_id);
        echo json_encode(array('state' => $statestr));
        exit;
    }

}

?>
