<style> 
  .error{
    color: red;
  } 
</style>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="#/">Home</a>
              <span>></span>
              <p>Dashboard</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title"><a href="#/" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> Containers</h1>
            </div>
          </div>
          <div class="title-sec-right">
            <?php if ($this->privilegeduser->hasPrivilege("DocumentUpload")) { ?>
                <a href="javascript:void(0);" class="btn-primary-mro upload-document" data-fancybox="dialog" data-src="#upload-doc-content"><img src="assets/images/upload-icon-white.svg" alt=""> Upload File</a>
                <!--<a href="javascript:void(0);" class="btn-primary-mro upload-document" onclick="openDocFancybox()"><img src="assets/images/upload-icon-white.svg" alt=""> Upload File</a>-->
            <?php } ?>
          </div>
        </div>
        <?php if (!empty($msg)) { ?>  
            <div class="page-title-wrapper" id="msg-id">
                <h6><?= $msg;?></h6>
            </div> 
        <?php } ?>
      
        <div class="filter-sec-wrapper">
            <div id="serchfilter" class="filter-sec-wrapper">
                <!-- <div class="filter-search dataTables_filter searchFilterClass">  
                    <input type="text" name="sSearch_0" id="sSearch_0"  class="searchInput filter-search-input" placeholder="Search by Container No., Document No....">
                </div>   -->
                <div class="form-group mt20"> 
                    <select name="sSearch_1" id="sSearch_1" class="searchInput basic-single select-large"> 
                        <option value="">Container Number</option>
                        <?php foreach ($containerData as $value) { ?>
                        <option value="<?=$value['container_id']?>"><?=$value['container_number']?></option>
                        <?php } ?>
                    </select>
                </div> 

                <div class="form-group  mt20">
                    <select name="sSearch_2" id="sSearch_2" class="searchInput basic-single select-large"> 
                        <option value="">Status</option>
                        <?php foreach ($containerStatus as $value) { ?>
                        <option value="<?=$value['container_status_id']?>"><?=$value['container_status_name']?></option>
                        <?php } ?>
                    </select>
                </div> 
                
                <div class="form-group  mt20">
                    <select name="sSearch_7" id="sSearch_7" class="searchInput basic-single select-small"> 
                        <option value="">FRI</option>
                        <?php foreach ($FRI as $value) { ?>
                        <option value="<?=$value['uploaded_document_number']?>"><?=$value['uploaded_document_number']?></option>
                        <?php } ?>
                    </select>
                </div> 
                
                <div class="form-group  mt20">
                    <select name="sSearch_8" id="sSearch_8" class="searchInput basic-single select-small"> 
                        <option value="">LS</option>
                        <?php foreach ($LS as $value) { ?>
                        <option value="<?=$value['uploaded_document_number']?>"><?=$value['uploaded_document_number']?></option>
                        <?php } ?>
                    </select>
                </div> 

                <div class="form-group  mt20">
                    <select name="sSearch_3" id="sSearch_3" class="searchInput basic-single select-large"> 
                        <option value="">Product</option>
                        <?php foreach ($productData as $value) { ?>
                        <option value="<?=$value['sku_id']?>"><?=$value['product_name']?></option>
                        <?php } ?>
                    </select>
                </div> 

                <div class="form-group  mt20">
                    <select name="sSearch_4" id="sSearch_4" class="searchInput basic-single select-large"> 
                        <option value="">Brand</option>
                        <?php foreach ($brandData as $value) { ?>
                        <option value="<?=$value['brand_id']?>"><?=$value['brand_name']?></option>
                        <?php } ?>
                    </select>
                </div> 

                <div class="form-group  mt20">
                    <select name="sSearch_5" id="sSearch_5" class="searchInput basic-single select-large"> 
                        <option value="">Vessel </option>
                        <?php foreach ($vesselData as $value) { ?>
                        <option value="<?=$value['vessel_name']?>"><?=$value['vessel_name']?></option>
                        <?php } ?>
                    </select>
                </div> 

                <div class="form-group  mt20">
                    <select name="sSearch_6" id="sSearch_6" class="searchInput basic-single select-large"> 
                        <option value="">Freight Forwarder</option>
                        <?php foreach ($freightForwarder as $value) { ?>
                        <option value="<?=$value['business_partner_id']?>"><?=$value['business_name']?></option>
                        <?php } ?>
                    </select>
                </div> 

                <div class="form-group  mt20">
                  <button type="reset"  onclick="refresh()"  class="select-small btn-primary-mro">
                    Clear
                  </button>
                  
                </div> 
                
            </div>
        </div>
        
        <div class="container-list-wrapper">
          <span id="containerListingDiv">
      
            <?php  if($ListOfContainerData){
                foreach ($ListOfContainerData as $key => $value) { 
                  // echo "<pre>";
                  //   print_r($value['documentType']);
                    ?>
                    <div class="container-single-wrapper">
                        <div class="cs-info">
                        <div class="cs-info-cta">
                            <?php if ($this->privilegeduser->hasPrivilege("ContainersDetailsView")) { ?>
                            <a href="<?= base_url()?>container_details?text='<?= rtrim(strtr(base64_encode("id=".$value['containerData']['container_id']), '+/', '-_'), '=')?>' " target="_blank">
                            <img src="<?php echo base_url(); ?>assets/images/view-btn-icon.svg" alt=""></a>
                            <?php } ?>
                            <?php if ($this->privilegeduser->hasPrivilege("ContainersDetailsAddEdit")) { ?>
                                <!-- <a href="#/"><img src="assets/images/upload-btn-icon.svg" alt=""></a> -->
                            <?php } ?>
                           
                        </div>
                        <div class="cs-status-text">
                            <p class="container-number"><?= $value['containerData']['container_number'] ?></p>
                            <p class="container-status"><?= $value['containerStatus'] ?></p>
                            <p class="last-update-title">Last Updated</p>
                            <p class="last-update-name"><?= $value['updatedBy'] ?></p>
                            <p class="last-update-date"><?= $value['updatedOn'] ?></p>
                        </div>
                        </div>
                        <div class="cs-status-wrapper">
                        <div class="connector"></div>
                            <?php if($value['documentType']){ 
                                foreach ($value['documentType'] as $dockey => $docval) { 
                                
                                    //ravendra commit start
                                    $colorClass = "css-grey";
                                    $containerId = $value['containerData']['container_id'];
                                    if(!empty($DocumentStatusData[$containerId][$docval['document_type_id']])){
                                        if($DocumentStatusData[$containerId][$docval['document_type_id']]["status"] == "Unchecked"){
                                            $colorClass = "css-yellow";
                                        }else if($DocumentStatusData[$containerId][$docval['document_type_id']]["status"] == "NotSet"){
                                            $colorClass = "css-red";
                                        }else{
                                            $colorClass = "css-green";
                                        }
                                    }else if (in_array($docval['document_type_id'],$job_not_completed_document_type_id)){
                                            $colorClass = "css-red";
                                     } ?>
                                    <!-- ravendra commit end -->
                                
                                    
                                  <div class="css-single">
                                      <div class="css-flag <?=$colorClass?>"></div>
                                      <div class="css-single-data">
                                      <p class="css-single-data-title"><?= $docval['document_type_name'] ?></p>
                                      <?php
                                   
                                      if(!empty($getuploaded_Document[$value['containerData']['container_id']][$docval['document_type_id']][0]['document_type_id'])){ 
                                        foreach ($getuploaded_Document[$value['containerData']['container_id']] as $uploadkey => $uploadval) { 
                                          
                                          if($docval['document_type_id'] == $uploadval[0]['document_type_id']){ ?>
                                          
                                            <p class="css-single-data-text"><?=  str_replace(',','<br/>',$uploadval[0]['uploaded_document_number']);?></p>
                                            <p class="css-single-data-date"><?= date('d-M-Y',strtotime($uploadval[0]['created_on']))?></p> 
                                        
                                          <?php } ?>

                                        <?php } ?> 
                                      <?php } ?>
                                    </div>
                                </div>
                                <?php  }
                                //  document type end here 
                            } ?>
                        </div>
                    </div>
                <?php }

            }?>
                
          </span>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Upload Document-->
<div id="upload-doc-content" style="display:none;min-width:400px;max-width:972px;">
  <!-- Select document type -->
  <div class="doc-type-wrapper upload-fri-step-1">
    <h3 class="form-group-title">Upload Document</h3>
    <div class="form-radio-wrapper">
      <div class="form-radio">
        <label class="container-radio">Document
          <input type="radio" checked="checked" name="upload_type" value="upload-document">
          <span class="checkmark"></span>
        </label>
      </div>
      <div class="form-radio">
        <label class="container-radio">Invoice
          <input type="radio" name="upload_type" value="upload-invoice">
          <span class="checkmark"></span>
        </label>
      </div>
    </div>
    <div class="form-cta">
      <button class="btn-primary-mro fri-step-1">Next</button>
    </div>
  </div>
  <!-- Select document type -->

  <!-- Upload FRI/LS Step 2 -->
  <div class="ud-step upload-fri-step-2">
    <div class="upload-doc-title">
      <div class="ud-title-left">
        <a href="#/" class="fri-step-2-back"><img src="assets/images/ud-back.svg" alt="" onclick="(function(){location.reload(); return false;})();return false;"></a>
        <h3>Container Listing</h3>
      </div>
    </div>
    <form action="" id="documentStep2" enctype="multipart/form-data"  method="post">
      <div class="select-doc-wrapper">
        <div class="sd-left">
          <span id="document_type_div">
            <h4 class="sd-subtitle">Document Type</h4>
            <select id="document_type" name="document_type" class="select-doc-type select-form-mro mb20" onchange="getLsListing(this.value)" required>
                <?php if($documentType){ ?>
                    <option value="">Select Document Type</option>
                    <?php foreach ($documentType as $key => $value) { ?>
                        <option value="<?= $value['document_type_id'] ?>"><?= $value['document_type_name']?></option>
                    <?php }

                } ?>
            
              </select>
          </span>
            
          <span id="business_partner_div">
             <h4 class="sd-subtitle">Business Partner</h4>
              <select id="business_partner" name="business_partner" class="select-doc-type select-form-mro mb20" onchange="getLsListing('invoice')" >
                  <?php if($business_partner){ ?>
                      <option value="">Select Business Partner Name</option>
                      <?php foreach ($business_partner as $key => $value) { ?>
                          <option value="<?= $value['business_partner_id'] ?>"><?= $value['business_name']?></option>
                      <?php }
                  } ?>
              </select>
          </span>
        
         
		  
          <div id="Ls-listing"> 
            <!-- this whole div while replace once user choose the document type other than FR LS dynamically via ajax call ls LISTING will appear than -->
            <h4 class="sd-subtitle">Choose Containers</h4>
            <div class="multiselect-dropdown-wrapper">
                <div class="md-value">
                  Search for container 
                </div>
                <div class="md-list-wrapper">
                    <input type="text" placeholder="Search" class="md-search">
                    <div class="md-list-items ud-list">
                      <?php if($ListOfContainerData){
                          foreach ($ListOfContainerData as $key => $value) { ?>
                            <div class="mdli-single ud-list-single">
                              <label class="container-checkbox"> <?= $value['containerData']['container_number'] ?>
                                  <input type="checkbox" id="containerno<?= $value['containerData']['container_id']?>"  value="<?= $value['containerData']['container_id']?>" name="container_ids[]">
                                  <span class="checkmark-checkbox"></span>
                              </label>
                            </div>
                            <?php
                        } }
                        ?> 
                    </div>
                    <div class="md-cta">
                        <p class="btn-primary-mro md-done">Done</p>
                        <p class="btn-secondary-mro md-clear">Clear All</p>
                    </div>
                </div>
            </div>
          </div>
        </div>
        <div class="sd-right" id="containterListingForOtherDocument">
          <p class="select-message">Choose containers to upload documents</p>
        </div>
      <div class="upload-doc-next">
        <button type="submit" class="btn-primary-mro fri-step-2">Next</button>
      </div>
      </div>
    </form>                  
  </div>
  <!-- Upload FRI/LS Step 2 -->
  <!-- Upload FRI/LS Step 3 -->
  <form action=""  id="documentStep3" enctype="multipart/form-data" method="post">
    <div class="ud-step upload-fri-step-3">
      <div class="upload-doc-title">
        <div class="ud-title-left">
          <a href="#/" class="fri-step-3-back"><img src="assets/images/ud-back.svg" alt="" onclick="(function(){location.reload(); return false;})();return false;"></a>
          <h3><span id="choose_container">Choose Containers</span> </h3>
        </div>
        <!-- <div class="ud-title-right">
          <a href="#/" class="btn-primary-mro">Back</a>
        </div> -->
      </div>
      <div class="select-doc-wrapper">
        <div class="sd-left">
          <div>
            <span id="selectedLsName"></span>  
          </div>
          <h4 class="sd-subtitle">
            Selected Containers</h4>
          <div class="ud-list mt20">
            <div id="selectedContainer"></div>     
          </div>
        </div>
        <div class="sd-right" >
          <div id="Lsdocumentupload">
            <h4 class="sd-subtitle mb20">Upload Document</h4>
            <input type="file" name="step3_document" id="step3_document">
          </div>
          <span id="step3_document_upload_error" class="error"></span>
        </div>
      </div>
      <div class="upload-doc-next">
        <button  type="submit"  class="btn-primary-mro fri-step-3">Next</button>
      </div>
    </div>
  </form>
  <!-- Upload FRI/LS Step 3 -->
  <!-- Upload FRI/LS Step 4 -->
  <div class="ud-step upload-fri-step-4">
    <div class="upload-doc-title">
      <div class="ud-title-left">
        <a href="#/" class="fri-step-4-back"><img src="assets/images/ud-back.svg" alt="" onclick="(function(){location.reload(); return false;})();return false;"></a>
        <h3>Upload Document</h3>
      </div>
      <!-- <div class="ud-title-right">
        <a href="#/" class="btn-primary-mro">Back</a>
      </div> -->
    </div>
    <div class="select-doc-wrapper">
      <div class="sd-left">
        <h4 class="sd-subtitle">Containers</h4>
        <div class="ud-list ud-list-doc-names mt20" id="step4_selectedContainerDocument"></div>
      </div>
      <div class="sd-right">
        <h4 class="sd-subtitle">
          <span id="step4_uploadedContainerDocumentName">FRI Title</span></h4>
        <div class="ud-list ud-list-doc-names mt20" id="step4_selectedContainerUploadedDocument">
        </div>
      </div>
    </div>
    <div class="upload-doc-next">
      <button class="btn-primary-mro fri-step-4">Next</button>
    </div>
  </div>
  <!-- Upload FRI/LS Step 4 -->
  <!-- Upload FRI/LS Step 5 -->
  <div class="ud-step upload-fri-step-5">
    <form id="documentStep5" enctype="multipart/form-data" method="post">
      <div class="upload-doc-title">
        <div class="ud-title-left">
          <a href="#/" class="fri-step-5-back"><img src="assets/images/ud-back.svg" alt="" onclick="(function(){location.reload(); return false;})();return false;"></a>
          <h3>Review Document Titles</h3>
        </div>
        <div class="ud-title-right">
          <button class="btn-grey-mro">Clear</button>
          <button class="btn-primary-mro">Save</button>
        </div>
      </div>
      <div class="select-doc-wrapper">
        <div class="sd-left">
          <h4 class="sd-subtitle mb20">Enter Custom Data</h4>
            <div id="custom_data_field"></div>              
        </div>
        <div class="sd-right">
          <h4 class="sd-subtitle">Form View</h4>
           <span id="form-view"></span>             
        </div>
      </div>
      <div class="upload-doc-next">
        <button class="btn-grey-mro">Clear</button>
        <button type="submit"  class="btn-primary-mro">Save</button>
      </div>
    </form>
  </div>
  <!-- Upload FRI/LS Step 5 -->
</div>
<!-- Upload Document-->
</body>

</html>

<script>



    $(document).ready(function () {
        $(document).on("click",".fancybox-close-small",function(){
            alert("called");
        })
//        $("#upload-doc-content").fancybox({
//            'afterClose': function() { 
//                window.location = "<?= base_url('listOfContainer');?>";
//            }
//        })
      // start these functions are for LS n FRI document type  
        var vRules = {
            "document_type": {required: true}
        };
        var vMessages = {
            "document_type": {required: "Please Enter Document Type."}
        };
        $("#documentStep2").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>listOfContainer/submitFormStep2";
                
                $("#documentStep2").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        // $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        console.log(response);
                        if (response.success) {
                            if(response.type == 'LS'){
                                getSelectedContainer(response.type);
                            }else{
                              getSelectedLsContainer(response.type);
                            }
                            $('.upload-fri-step-3').show();
                            $('.upload-fri-step-2').hide();
                        } else {
                            alert(response.msg);
                            return false;
                        }
                    }
                });
            }
        });

        // for step 2 submission 
        var vRules = {
            "step3_document": {required: true}
        };
        var vMessages = {
            "step3_document": {required: "Please Select the document for upload"}
        };
        //check and save country
        $("#documentStep3").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>listOfContainer/submitFormStep3";
                
                $("#documentStep3").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        // $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        console.log(response);
                        if (response.success) {

                            if(response.type == 'LS' || response.type == ''){
                              getSelectedContainerDocument(response.type); 
                              getSelectedCustomField(response.type);
                              $('.upload-fri-step-4').show();
                              $('.upload-fri-step-3').hide();
                            }else{
                              getSelectedCustomField(response.type);
                              if(response.type == 'invoice'){
                                $('.upload-fri-step-5').show();
                                $('.upload-fri-step-3').hide();

                              }
                            }
                            
                        } else {
                            $("#step3_document_upload_error").html(response.msg);
                            return false;
                        }
                    }
                });
            }
        });

        //check and save country
        $("#documentStep5").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>listOfContainer/submitFormStep5";
                $("#documentStep5").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        // $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        console.log(response);
                        if (response.success) {
                            alert(response.msg);
                            window.location = "<?= base_url('listOfContainer?msg=Document successfully uploaded to containers');?>";
                        } else {
                            $("#step3_document_upload_error").html(response.msg);
                            return false;
                        }
                    }
                });
            }
        });
        
        function getSelectedContainer(){
          $.ajax({
            url: "<?= base_url('listOfContainer/getSelectedContainer')?>",
            type: "POST",
            dataType: "json",
            success: function(response){
              if(response.success){
                $("#selectedContainer").html(response.html);
              }else{  
                alert(response.msg)
              }
              
            }
          }); 
        }
        
        function getSelectedLsContainer(type){
          $.ajax({
            url: "<?= base_url('listOfContainer/getSelectedLsContainer')?>",
            type: "POST",
            dataType: "json",
            data:{type},
            success: function(response){
              if(response.success){
                $("#selectedContainer").html(response.html);
                $("#selectedLsName").html(response.selectedLsName);
                $("#choose_container").html("Choose LS");
                $("#Lsdocumentupload").html(response.uploadSectionhtml);
              }else{  
                alert(response.msg)
              }
              
            }
          }); 
        }

        function getSelectedContainerDocument(type){
          $.ajax({
            url: "<?= base_url('listOfContainer/getSelectedContainerDocument')?>",
            type: "POST",
            dataType: "json",
            data :{type},
            success: function(response){
              if(response.success){
                $("#step4_uploadedContainerDocumentName").html(response.document_prefix_name + ' Title '); 
                $("#step4_selectedContainerDocument").html(response.html_container);
                $("#step4_selectedContainerUploadedDocument").html(response.html_uploaded); 
              }else{  
                alert(response.msg)
              }
              
            }
          }); 
        }
        

        function getSelectedCustomField(type){
          $.ajax({
            url: "<?= base_url('listOfContainer/getSelectedCustomField')?>",
            type: "POST",
            dataType: "json",
            data :{type},
            success: function(response){
              if(response.success){
                $("#custom_data_field").html(response.customField);
                $("#form-view").html(response.formView);
              }else{  
                alert(response.msg)
              }
              
            }
          }); 
        }
      // end these functions are for LS n FRI document type  


      //  $(document).on("change",".searchInput",function(){
      $(document).on('change', '.searchInput', function(){  
          // get filter data 
          var container_id = $("#sSearch_1").val();
          var status = $("#sSearch_2").val();
          var product_id = $("#sSearch_3").val();
          var brand_id = $("#sSearch_4").val();
          var vessel_id = $("#sSearch_5").val();
          var freight_forwarder = $("#sSearch_6").val();
          var fri = $("#sSearch_7").val();
          var ls = $("#sSearch_8").val();
        
          $.ajax({
                url: "<?= base_url('listOfContainer/getFilterdata')?>",
                type: "POST",
                dataType: "json",
                data:{container_id,status,product_id,brand_id,vessel_id,freight_forwarder,fri,ls},
                success: function(response){
                  if(response.success){
                    $("#containerListingDiv").html(response.containerListingDiv);
                  }else{  
                    $("#containerListingDiv").html(response.containerListingDiv);
                  } 
                }
              }); 
      });  

    });

    //  start  these functions are for othere document type 
        
    function getLsListing(document_type_id){
      console.log(document_type_id);
        if(document_type_id){
          if(!['2','3'].includes(document_type_id) ||  document_type_id == 'invoice'){
              // to get the LS from tbl for this type of document 
              $.ajax({
                url: "<?= base_url('listOfContainer/getLsListing')?>",
                type: "POST",
                dataType: "json",
                data:{document_type_id},
                success: function(response){
                   console.log(response);
                  if(response.success){
                    $("#Ls-listing").html(response.html);
                    $("#containterListingForOtherDocument").html(response.rightSideMsg);
                  }else{  
                    alert(response.msg)
                    $("#Ls-listing").html(response.html);
                    $("#containterListingForOtherDocument").html(response.rightSideMsg);
                  } 
                }
              }); 
          } else {
            // call for container listing for  fr n LS 
              $.ajax({
                url: "<?= base_url('listOfContainer/getContainerListing')?>",
                type: "POST",
                dataType: "json",
                data:{document_type_id},
                success: function(response){
                  if(response.success){
                    $("#Ls-listing").html(response.html);
                    $("#containterListingForOtherDocument").html(response.rightSideMsg);
                  }else{  
                    alert(response.msg)
                    $("#Ls-listing").html(response.html);
                    $("#containterListingForOtherDocument").html(response.rightSideMsg);
                  } 
                }
              }); 
          }

        }else{
          alert("please select the document type!")
        }
      }
      //  end  these functions are for othere document type 

      function getvalue(val) {
        if (val) {
            $("#ls_number").val(val);
            var test = $("#ls_number" + val).text();
            $('.sd-value').html(test);
            var document_type_id = $("#document_type").val();
            // console.log(document_type_id);
            // return false;
            // call ajax to get the container which is belonging to this LS number 
            $.ajax({
                url: "<?= base_url('listOfContainer/getLSContainerListing')?>",
                type: "POST",
                dataType: "json",
                data: {val,document_type_id},
                success: function(response){
                  if(response.success){
                    $("#containterListingForOtherDocument").html(response.html);
                  }else{  
                    alert(response.msg)
                    $("#containterListingForOtherDocument").html(response.html);
                  } 
                }
              }); 

        } else {
            alert("select the value first");
        }
    }


    var sku = [];
    function getSku(sku_id){
      if(sku_id){
        if(sku.includes(sku_id.toString())){
          alert("data is already added in below list");
          return false;
          }else{
          sku.push(sku_id);
          console.log("new value");
        } 
        console.log(sku);
        if(sku_id){
          $.ajax({
                  url: "<?= base_url('listOfContainer/getsku')?>",
                  type: "POST",
                  dataType: "json",
                  data: {sku_id},
                  success: function(response){
                    if(response.success){
                      $("#sku_div").append(response.htmlsku);
                      // $("#product_desc").html(response.product_desc);
                      // $("#item_size").html(response.item_size);
                    }else{  
                      // alert(response.msg)
                      // $("#").html(response.html);
                    } 
                  }
                }); 
        }else{
          alert("select sku value");
          $("#item_code").html('');
          $("#product_desc").html('');
          $("#item_size").html('');
        }
      }
    }

    function removeSku(sku_id){
      sku = sku.filter(function(item) {
          return item !== sku_id
      })
      $("#singleSkuDiv"+sku_id).remove();
    }

    function refresh(){
      location.reload();
    } 
   
 
</script>