<h4 class="sd-subtitle">Choose Container</h4>
<div class="multiselect-dropdown-wrapper">
    <div class="md-value">
        Search for Container
    </div>
    <div class="md-list-wrapper">
        <input type="text" placeholder="Search" class="md-search">
        <div class="md-list-items ud-list">
            <?php if($containerListing){
                foreach ($containerListing as $key => $value) { ?>
                <div class="mdli-single ud-list-single">
                    <label class="container-checkbox"> <?= $value['container_number'] ?>
                        <input type="checkbox" id="containerno<?= $value['container_id']?>"  value="<?= $value['container_id']?>" name="container_ids[]">
                        <span class="checkmark-checkbox"></span>
                    </label>
                </div>
                <?php
            } }
            ?> 
        </div>
        <div class="md-cta">
            <p class="btn-primary-mro md-done">Done</p>
            <p class="btn-secondary-mro md-clear">Clear All</p>
        </div>
    </div>
</div>