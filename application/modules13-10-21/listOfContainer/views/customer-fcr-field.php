  <?php
    if ($getSelectedCustomField) { ?>
      
      <!--start container sku table-->
      <?php if (!empty($containersSKUData)) { ?>
          <div class="bp-list-wrapper">
              <table class="table table-striped document-sku-table no-footer" cellpadding="0" cellspacing="0">
                  <thead>
                      <tr>
                          <th>Container</th>
                          <th>Name of FF</th>
                          <th>Liner Name </th> 
                      </tr>
                  </thead>
                  <tbody>
                      <?php
                        foreach ($containersSKUData as $containerId => $containerSkuData) {
                            $containerNumberArr = array();
                            $businessNameArr = array();
                            $cnt = 0;
                            foreach ($containerSkuData as $containerSkuId => $value) {
                                if($cnt==0){
                                ?>
                                  <tr>
                                      <td>
                                          <?php
                                            if (!in_array($value['container_number'], $containerNumberArr)) { ?>
                                                <input type="checkbox" checked name="selected_container_id[]" id="container_id<?=$containerId?>" value="<?= $containerId ?>"/>
                                                    <?=$value['container_number'] ?>
                                                
                                            <?php }
                                            ?>
                                      </td>
                                      <td><?= $value['ff_name'] ?></td>
                                      <td>
                                          <input type="hidden" name="tbl_container_id[<?= $containerId ?>]" id="tbl_container_id<?= $containerId ?>" value="<?= (!empty($containerId)) ? $containerId : '' ?>">
                                          <input type="text" name="liner_name[<?= $containerId ?>]" id="liner_name<?= $containerId ?>" value="<?= (!empty($value['liner_name'])) ? $value['liner_name'] : '' ?>" class="input-form-mro" placeholder="Enter Liner Name">
                                          <?php
                                          
                                          $pol = $value['pol'];
                                          $pod = $value['pod'];
                                          $etd =$value['etd'] ;
                                          $revised_etd = $value['revised_etd'];
                                          $transit_day = $value['ett'] ;
                                          ?>
                                      </td> 
                                  </tr>
                              <?php
                                    $containerNumberArr[] = $value['container_number'];                                    
                                } 
                                $cnt++;
                            } ?> 
                      <?php

                        } ?>
                  </tbody>
              </table>
          </div>
          <div class="form-group ">
            <label for="">Port of Loading</label>
            <input type="text" name="pol"  id="pol" placeholder="Enter data" value="<?= $pol ?>" class="input-form-mro">
          </div>
          <div class="form-group ">
            <label for="">Port of Discharge </label>
            <input type="text" name="pod"  id="pod"  value="<?= $pod ?>" placeholder="Enter data" class="input-form-mro">
          </div>
          <div class="form-group ">
            <label for="">ETD </label>
            <input type="date" name="etd"  id="etd"  value="<?php echo(!empty($etd)) ? date('Y-m-d', strtotime($etd)) : date("m/d/Y"); ?>" placeholder="Enter data" class="input-form-mro">
          </div>
          <div class="form-group ">
            <label for="">Revised ETD </label>
            <input type="date" name="revised_etd"  id="revised_etd" value="<?php echo(!empty($revised_etd)) ? date('Y-m-d', strtotime($revised_etd)) : date("m/d/Y"); ?>" placeholder="Enter data" class="input-form-mro">
          </div>
          <div class="form-group ">
            <label for="">Transit Days (Not in HBL) </label>
            <input type="text" name="ett"  id="ett" value="<?= $transit_day ?>" placeholder="Enter data" class="input-form-mro">
          </div>


      <?php } ?>
      <!--end container sku table-->

      <?php foreach ($getSelectedCustomField as $key => $value) {
          ?>
        
        <input type="hidden" name="custom_field_structure_id[]" id="custom_field_structure_id" value="<?= $value['custom_field_structure_id'] ?>">
        <input type="hidden" name="document_type_id" id="document_type_id" value="<?= $value['document_type_id'] ?>">
        <input type="hidden" name="sub_document_type_id" id="sub_document_type_id" value="<?= $value['sub_document_type_id'] ?>">
        <input type="hidden" name="uploaded_document_number" id="uploaded_document_number" value="<?= $value['uploaded_document_number'] ?>">

          <?php if ($value['custom_field_type'] == 'date') { ?>
              <div class="form-group ">
                  <label for=""><?= $value['custom_field_title'] ?></label>
                  <input type="date" name="<?= $value['custom_field_structure_id'] ?>" required id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter date" class="input-form-mro">
              </div>
          <?php  } elseif ($value['custom_field_type'] == 'text') { ?>
              <div class="form-group ">
                  <label for=""><?= $value['custom_field_title'] ?></label>
                  <input type="text" name="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" required id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter data" class="input-form-mro">
              </div>
          <?php } elseif ($value['custom_field_type'] == 'dropdown') { ?>

              <div class="form-group">
                  <div class="form-group">
                      <label for=""><?= $value['custom_field_title'] ?></label>
                      <div class="multiselect-dropdown-wrapper">
                          <div class="md-value">
                              Select value
                          </div>
                          <div class="md-list-wrapper">
                              <input type="text" placeholder="Search" class="md-search">
                              <div class="md-list-items">
                                  <?php if ($ListOfContainerData) {
                                        foreach ($ListOfContainerData as $key => $value) { ?>
                                          <div class="mdli-single ud-list-single">
                                              <label class="container-checkbox"> <?= $value['containerData']['container_number'] ?>
                                                  <input type="checkbox" id="<?= $value['custom_field_structure_id']; ?>" value="<?= $value['containerData']['container_id'] ?>" name="<?= $value['custom_field_structure_id'] ?>">
                                                  <span class="checkmark-checkbox"></span>
                                              </label>
                                          </div>
                                  <?php
                                        }
                                    }
                                    ?>
                              </div>
                              <div class="md-cta">
                                  <!-- <a href="#/" class="btn-primary-mro md-done">Done</a>
                            <a href="#/" class="btn-secondary-mro md-clear">Clear All</a> -->
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

          <?php } elseif ($value['custom_field_title'] == 'number') { ?>

          <?php } ?>
  <?php }
    } ?>