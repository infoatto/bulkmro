<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Sku extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('skumodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("SKUList")){
            redirect('dashboard');
        }
    }

    function index() {
        $data = array(); 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('sku/index', $data);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("SKUAddEdit")){
            redirect('dashboard');
        }
        //add edit form
        $sku_id = "";
        $edit_datas = array();
        $edit_datas['categoryData'] = array();
        $edit_datas['brandData'] = array();
        $edit_datas['businessPartnerData'] = array();
        $edit_datas['sizeData'] = array();
        $edit_datas['uomData'] = array();

        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $sku_id = $url_prams['id'];
            $result = $this->common->getData("tbl_sku", "*", array("sku_id" => $sku_id));
            if (!empty($result)) {
                $edit_datas['sku_details'] = $result[0];
            }
        }
        //get category data
        $categoryData = $this->common->getData("tbl_category", "category_id,category_name", array("status"=>"Active"));
        if (!empty($categoryData)) {
            $edit_datas['categoryData'] = $categoryData;
        }
        //get brand data
        $brandData = $this->common->getData("tbl_brand", "brand_id,brand_name", array("status"=>"Active"));
        if (!empty($brandData)) {
            $edit_datas['brandData'] = $brandData;
        }
        //get business partner data
        $businessPartnerData = $this->common->getData("tbl_business_partner", "business_partner_id,business_name", array("business_type" => "Vendor", "business_category" => "Manufacturer", "status" => "Active"));
        if (!empty($businessPartnerData)) {
            $edit_datas['businessPartnerData'] = $businessPartnerData;
        }
        //get size data
        $sizeData = $this->common->getData("tbl_size", "size_id,size_name", array("status"=>"Active"));
        if (!empty($sizeData)) {
            $edit_datas['sizeData'] = $sizeData;
        }
        //get uom data
        $uomData = $this->common->getData("tbl_uom", "uom_id,uom_name", array("status"=>"Active"));
        if (!empty($uomData)) {
            $edit_datas['uomData'] = $uomData;
        }

        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('sku/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        $get_result = $this->skumodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->sku_number));
                array_push($temp, $get_result['query_result'][$i]->sku_vendor);
                array_push($temp, $get_result['query_result'][$i]->product_name);
                array_push($temp, $get_result['query_result'][$i]->status);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("SKUAddEdit")){    
                    $actionCol = '<a href="sku/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->sku_id), '+/', '-_'), '=') . '" title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "sku_number = '" . $this->input->post('sku_number') . "' ";
            if (!empty($this->input->post("sku_id"))) {
                $condition .= " AND sku_id <> " . $this->input->post("sku_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['sku_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'BM SKU Number already exist...'));
                exit;
            }

            $data = array();
            $data['sku_number'] = $this->input->post('sku_number');
            $data['sku_vendor'] = $this->input->post('sku_vendor');
            $data['category_id'] = $this->input->post('category_id');
            $data['brand_id'] = $this->input->post('brand_id');
            $data['manufacturer_id'] = $this->input->post('manufacturer_id');
            $data['product_name'] = $this->input->post('product_name');
            $data['size_id'] = $this->input->post('size_id');
            $data['uom_id'] = $this->input->post('uom_id');
            $data['status'] = $this->input->post('status');

            if (!empty($this->input->post("sku_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_sku", $data, array("sku_id" => $this->input->post("sku_id")));
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_sku", $data, "1");
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }
    
    function importSKU() {
        
        if(!$this->privilegeduser->hasPrivilege("SKUImport")){
            redirect('dashboard');
        }
        require_once('application/libraries/SimpleXLSX.php');
        
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["excelfile"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["excelfile"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die; 
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) { 
                        if (trim($data[0]) != "BM SKU Number") {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        if (trim($data[1]) != "Vendor SKU" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[2]) != "Status" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[3]) != "Category" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[4]) != "Brand Name" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[5]) != "Manufacturer" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[6]) != "Product Name" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[7]) != "Size" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[8]) != "UoM" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }   
                        
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[2])){
                            continue;
                        } 
                        
                        if (trim($data[0]) == "") {
                            $error .= "BM SKU Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }   
                        
                        if (trim($data[2]) != "Active" && $status != "In-active") {
                            $error .= "Status cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[3]) == "") {
                            $error .= "Category cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[4]) == "") {
                            $error .= "Brand Name cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[5]) == "") {
                            $error .= "Manufacturer cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[6]) == "") {
                            $error .= "Product Name cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }   
                        
                        
                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $sku_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_vendor = str_replace("'", "&#39;", trim($data[1]));
                        $status = str_replace("'", "&#39;", trim($data[2]));                        
                        $category_name = str_replace("'", "&#39;", trim($data[3]));
                        $brand_name = str_replace("'", "&#39;", trim($data[4]));
                        $manufacturer_name = str_replace("'", "&#39;", trim($data[5]));
                        $product_name = str_replace("'", "&#39;", trim($data[6]));
                        $size_name = str_replace("'", "&#39;", trim($data[7])); 
                        $uom_name = str_replace("'", "&#39;", trim($data[8]));  
                        
                        //get category id
                        $condition_category = "category_name = '" . $category_name . "' "; 
                        $chk_category_sql = $this->common->Fetch("tbl_category", "category_id", $condition_category);
                        $rs_category = $this->common->MySqlFetchRow($chk_category_sql, "array");                        
                        $category_id = 0;
                        if (!empty($rs_category[0]['category_id'])) {
                            $category_id = $rs_category[0]['category_id'];
                        } 
                        
                        //get brand id
                        $condition_brand = "brand_name = '" . $brand_name . "' "; 
                        $chk_brand_sql = $this->common->Fetch("tbl_brand", "brand_id", $condition_brand);
                        $rs_brand = $this->common->MySqlFetchRow($chk_brand_sql, "array");                        
                        $brand_id = 0;
                        if (!empty($rs_brand[0]['brand_id'])) {
                            $brand_id = $rs_brand[0]['brand_id'];
                        } 
                        
                        //get brand id
                        $condition_brand = "brand_name = '" . $brand_name . "' "; 
                        $chk_brand_sql = $this->common->Fetch("tbl_brand", "brand_id", $condition_brand);
                        $rs_brand = $this->common->MySqlFetchRow($chk_brand_sql, "array");                        
                        $brand_id = 0;
                        if (!empty($rs_brand[0]['brand_id'])) {
                            $brand_id = $rs_brand[0]['brand_id'];
                        } 
                        
                        //get Manufacturer id
                        $condition_manufacturer = "business_name = '" . $manufacturer_name . "' AND business_type='Vendor' AND business_category ='Manufacturer' "; 
                        $chk_manufacturer_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $condition_manufacturer);
                        $rs_manufacturer = $this->common->MySqlFetchRow($chk_manufacturer_sql, "array");                        
                        $manufacturer_id = 0;
                        if (!empty($rs_manufacturer[0]['business_partner_id'])) {
                            $manufacturer_id = $rs_manufacturer[0]['business_partner_id'];
                        } 
                        
                        
                        //get size id
                        $condition_size = "size_name = '" . $size_name . "' "; 
                        $chk_size_sql = $this->common->Fetch("tbl_size", "size_id", $condition_size);
                        $rs_size = $this->common->MySqlFetchRow($chk_size_sql, "array");                        
                        $size_id = 0;
                        if (!empty($rs_size[0]['size_id'])) {
                            $size_id = $rs_size[0]['size_id'];
                        } 
                        
                        
                        //get uom id
                        $condition_uom = "uom_name = '" . $uom_name . "' "; 
                        $chk_uom_sql = $this->common->Fetch("tbl_uom", "uom_id", $condition_uom);
                        $rs_uom = $this->common->MySqlFetchRow($chk_uom_sql, "array");                        
                        $uom_id = 0;
                        if (!empty($rs_uom[0]['uom_id'])) {
                            $uom_id = $rs_uom[0]['uom_id'];
                        }  
                        
                        $data = array();
                        $data['sku_number'] = $sku_number;
                        $data['sku_vendor'] = $sku_vendor;
                        $data['category_id'] = $category_id;  
                        $data['brand_id'] = $brand_id; 
                        $data['manufacturer_id'] = $manufacturer_id; 
                        $data['size_id'] = $size_id; 
                        $data['uom_id'] = $uom_id; 
                        $data['product_name'] = $product_name;
                        $data['status'] = $status; 
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s"); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' AND status='" . $status . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                         //update-insert bussiness partner data 
                        if (!empty($rs_sku[0]['sku_id'])) {                            
                            $result = $this->common->updateData("tbl_sku", $data, array("sku_id" => $rs_sku[0]['sku_id']));  
                            if ($result) {  
                                $imported++;
                            } else {
                                $notimported++;
                            }
                        }else{
                             //insert data 
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $result = $this->common->insertData("tbl_sku", $data, "1");
                            if ($result) {
                                $imported++;
                            } else {
                                $notimported++;
                            } 
                        }  
                    }
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    } 

}

?>
