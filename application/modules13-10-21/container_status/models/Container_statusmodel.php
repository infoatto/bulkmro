<?PHP
class Container_statusmodel extends CI_Model
{
	function getRecords($get){
            $table = "tbl_container_status"; 
            $default_sort_column = 'c.container_status_id';
            $default_sort_order = 'desc';
            $condition = "1=1 "; 

            $colArray = array('c.container_status_name','c.status');
            $sortArray = array('c.container_status_name','c.status');

            $page = $get['iDisplayStart'];	// iDisplayStart starting offset of limit funciton
            $rows = $get['iDisplayLength'];	// iDisplayLength no of records from the offset

            // sort order by column
            $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
            $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

            for($i=0;$i<count($colArray);$i++){ 
                if(isset($_GET['Searchkey_'.$i]) && $_GET['Searchkey_'.$i] !=""){
                        $condition .= " AND ".$colArray[$i]." LIKE '%".$_GET['Searchkey_'.$i]."%' ";
                }  
            }

            $this -> db -> select('c.*');
            $this -> db -> from("$table as c"); 
            $this->db->where("($condition)");
            $this->db->order_by($sort, $order);
            $this->db->limit($rows,$page); 
            $query = $this -> db -> get();
            //echo $this->db->last_query();exit; 

            $this -> db -> select('c.*');
            $this -> db -> from("$table as c");
            $this->db->where("($condition)");
            $this->db->order_by($sort, $order); 
            $query1 = $this -> db -> get();

            if($query -> num_rows() > 0){
                $totcount = $query1 -> num_rows();
                return array("query_result" => $query->result(), "totalRecords" => $totcount);
            }else{
                return array("totalRecords" => 0);
            }
	}
}
?>
