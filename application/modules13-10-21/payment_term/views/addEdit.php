<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("payment_term"); ?>">Payment Term List</a>
                                <span>></span>
                                <p>Add Payment Term</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add Payment Term</h1>
                            </div>
                        </div>          
                        <button class="btn-primary-mro">Save</button>
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="payment_term_id" id="payment_term_id" value="<?php echo(!empty($payment_term_details['payment_term_id'])) ? $payment_term_details['payment_term_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="SizeName">Payment Term Value</label>
                                    <input type="text" name="payment_term_value" id="payment_term_value" class="input-form-mro" value="<?php echo(!empty($payment_term_details['payment_term_value'])) ? $payment_term_details['payment_term_value'] : ""; ?>">
                                </div> 
                                <div class="form-group">
                                    <label for="Status">Status</label>
                                    <select name="status" id="status" class="select-form-mro">
                                        <option value="Active" <?php echo(!empty($payment_term_details['status']) && $payment_term_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                        <option value="In-active" <?php echo(!empty($payment_term_details['status']) && $payment_term_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                    </select>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {
        var vRules = {
            "payment_term_value": {required: true}
        };
        var vMessages = {
            "payment_term_value": {required: "Please enter payment term value."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>payment_term/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('payment_term') ?>";
                            }, 3000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });
</script>