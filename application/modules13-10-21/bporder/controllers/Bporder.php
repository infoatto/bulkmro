<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Bporder extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('bpordermodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersOrderList")){
            redirect('dashboard');
        }
    }

    function index() {
        $data = array(); 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('bporder/index', $data);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")){
            redirect('dashboard');
        }
        $edit_datas = array();
        $edit_datas['bp_order_details'] = array();
        $edit_datas['bpDetails'] = array();
        $edit_datas['skuDetails'] = array();
        $edit_datas['bp_skudetails'] = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $bp_order_id = $url_prams['id'];
            $result = $this->common->getData("tbl_bp_order", "*", array("bp_order_id" => $bp_order_id));
            if (!empty($result)) {
                $edit_datas['bp_order_details'] = $result[0];
            }
            //order sku details data
            $skuData = $this->bpordermodel->getOrderSkuDetails($bp_order_id);
            if (!empty($skuData)) {
                $edit_datas['bp_skudetails'] = $skuData;
            }
        }
        //business partner data
        $condition = $condition = " business_category in ('Government','Corporate','Distributor','Supplier') AND status = 'Active' ";
        $bpDetails = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias", $condition); 
        if (!empty($bpDetails)) {
            $edit_datas['bpDetails'] = $bpDetails;
        }
        //sku data
        $skuDetails = $this->common->getData("tbl_sku", "sku_id,sku_number,product_name", array("status"=>"Active"));
        if (!empty($skuDetails)) {
            $edit_datas['skuDetails'] = $skuDetails;
        }
        $edit_datas['view'] = isset($_GET['view'])?$_GET['view']:0;
        
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('bporder/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        $get_result = $this->bpordermodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, $get_result['query_result'][$i]->contract_number);
                array_push($temp, date('d-m-Y', strtotime($get_result['query_result'][$i]->shipment_start_date)));
                array_push($temp, $get_result['query_result'][$i]->duration);
                array_push($temp, $get_result['query_result'][$i]->patment_terms);
                array_push($temp, $get_result['query_result'][$i]->incoterms);
                array_push($temp, $get_result['query_result'][$i]->status);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")){   
                    $actionCol = '<a href="bporder/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->bp_order_id), '+/', '-_'), '=') . '" title="Edit" class="text-center"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                 if($this->privilegeduser->hasPrivilege("BusinessPartnersOrderView")){   
                    $actionCol .= '<a href="bporder/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->bp_order_id), '+/', '-_'), '=') . '&view=1" title="Edit" class="text-center" style="float:right;"><img src="' . base_url() . 'assets/images/view-btn-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "contract_number = '" . $this->input->post('contract_number') . "' ";
            if (!empty($this->input->post("bp_order_id"))) {
                $condition .= " AND bp_order_id <> " . $this->input->post("bp_order_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_bp_order", "bp_order_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['bp_order_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Contract Number already exist...'));
                exit;
            }

            $data = array();
            $data['contract_number'] = $this->input->post('contract_number');
            $data['business_partner_id'] = $this->input->post('business_partner_id');
            $data['shipment_start_date'] = !empty($this->input->post('shipment_start_date')) ? date("Y-m-d", strtotime($this->input->post('shipment_start_date'))) : NULL;
            $data['duration'] = $this->input->post('duration');
            $data['billing_details_id'] = $this->input->post('billing_details_id');
            $data['shipping_details_id'] = $this->input->post('shipping_details_id');
            $data['patment_terms'] = $this->input->post('patment_terms');
            $data['incoterms'] = $this->input->post('incoterms');
            $data['status'] = $this->input->post('status');

            if (!empty($this->input->post("bp_order_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_bp_order", $data, array("bp_order_id" => $this->input->post("bp_order_id")));

                $data1['bp_order_id'] = $this->input->post("bp_order_id");
                $resultdel = $this->common->deleteRecord('tbl_bp_order_sku', array("bp_order_id" => $this->input->post("bp_order_id")));
                if ($resultdel) {
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $data1['price'] = $this->input->post('sku_price')[$key];
                            $result1 = $this->common->insertData('tbl_bp_order_sku', $data1, '1');
                        }
                    }
                }

                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_bp_order", $data, "1");
                if ($result) {
                    $data1['bp_order_id'] = $result;
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $data1['price'] = $this->input->post('sku_price')[$key];
                            $result1 = $this->common->insertData('tbl_bp_order_sku', $data1, '1');
                        }
                    }
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }
    
    function importBPorder() {
        echo 'comming soon'; die;
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersOrderImport")){
            redirect('dashboard');
        }
        require_once('application/libraries/SimpleXLSX.php');
        
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["excelfile"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["excelfile"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die; 
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) { 
                        if (trim($data[0]) != "BM SKU Number") {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        if (trim($data[1]) != "Vendor SKU" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[2]) != "Status" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[3]) != "Category" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[4]) != "Brand Name" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[5]) != "Manufacturer" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[6]) != "Product Name" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[7]) != "Size" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[8]) != "UoM" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }   
                        
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[2])){
                            continue;
                        } 
                        
                        if (trim($data[0]) == "") {
                            $error .= "BM SKU Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }   
                        
                        if (trim($data[2]) != "Active" && $status != "In-active") {
                            $error .= "Status cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[3]) == "") {
                            $error .= "Category cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[4]) == "") {
                            $error .= "Brand Name cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[5]) == "") {
                            $error .= "Manufacturer cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[6]) == "") {
                            $error .= "Product Name cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }   
                        
                        
                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $sku_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_vendor = str_replace("'", "&#39;", trim($data[1]));
                        $status = str_replace("'", "&#39;", trim($data[2]));                        
                        $category_name = str_replace("'", "&#39;", trim($data[3]));
                        $brand_name = str_replace("'", "&#39;", trim($data[4]));
                        $manufacturer_name = str_replace("'", "&#39;", trim($data[5]));
                        $product_name = str_replace("'", "&#39;", trim($data[6]));
                        $size_name = str_replace("'", "&#39;", trim($data[7])); 
                        $uom_name = str_replace("'", "&#39;", trim($data[8]));  
                        
                        //get category id
                        $condition_category = "category_name = '" . $category_name . "' "; 
                        $chk_category_sql = $this->common->Fetch("tbl_category", "category_id", $condition_category);
                        $rs_category = $this->common->MySqlFetchRow($chk_category_sql, "array");                        
                        $category_id = 0;
                        if (!empty($rs_category[0]['category_id'])) {
                            $category_id = $rs_category[0]['category_id'];
                        } 
                        
                        //get brand id
                        $condition_brand = "brand_name = '" . $brand_name . "' "; 
                        $chk_brand_sql = $this->common->Fetch("tbl_brand", "brand_id", $condition_brand);
                        $rs_brand = $this->common->MySqlFetchRow($chk_brand_sql, "array");                        
                        $brand_id = 0;
                        if (!empty($rs_brand[0]['brand_id'])) {
                            $brand_id = $rs_brand[0]['brand_id'];
                        } 
                        
                        //get brand id
                        $condition_brand = "brand_name = '" . $brand_name . "' "; 
                        $chk_brand_sql = $this->common->Fetch("tbl_brand", "brand_id", $condition_brand);
                        $rs_brand = $this->common->MySqlFetchRow($chk_brand_sql, "array");                        
                        $brand_id = 0;
                        if (!empty($rs_brand[0]['brand_id'])) {
                            $brand_id = $rs_brand[0]['brand_id'];
                        } 
                        
                        //get Manufacturer id
                        $condition_manufacturer = "business_name = '" . $manufacturer_name . "' AND business_type='Vendor' AND business_category ='Manufacturer' "; 
                        $chk_manufacturer_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $condition_manufacturer);
                        $rs_manufacturer = $this->common->MySqlFetchRow($chk_manufacturer_sql, "array");                        
                        $manufacturer_id = 0;
                        if (!empty($rs_manufacturer[0]['business_partner_id'])) {
                            $manufacturer_id = $rs_manufacturer[0]['business_partner_id'];
                        } 
                        
                        
                        //get size id
                        $condition_size = "size_name = '" . $size_name . "' "; 
                        $chk_size_sql = $this->common->Fetch("tbl_size", "size_id", $condition_size);
                        $rs_size = $this->common->MySqlFetchRow($chk_size_sql, "array");                        
                        $size_id = 0;
                        if (!empty($rs_size[0]['size_id'])) {
                            $size_id = $rs_size[0]['size_id'];
                        } 
                        
                        
                        //get uom id
                        $condition_uom = "uom_name = '" . $uom_name . "' "; 
                        $chk_uom_sql = $this->common->Fetch("tbl_uom", "uom_id", $condition_uom);
                        $rs_uom = $this->common->MySqlFetchRow($chk_uom_sql, "array");                        
                        $uom_id = 0;
                        if (!empty($rs_uom[0]['uom_id'])) {
                            $uom_id = $rs_uom[0]['uom_id'];
                        }  
                        
                        $data = array();
                        $data['sku_number'] = $sku_number;
                        $data['sku_vendor'] = $sku_vendor;
                        $data['category_id'] = $category_id;  
                        $data['brand_id'] = $brand_id; 
                        $data['manufacturer_id'] = $manufacturer_id; 
                        $data['size_id'] = $size_id; 
                        $data['uom_id'] = $uom_id; 
                        $data['product_name'] = $product_name;
                        $data['status'] = $status; 
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s"); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' AND status='" . $status . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                         //update-insert bussiness partner data 
                        if (!empty($rs_sku[0]['sku_id'])) {                            
                            $result = $this->common->updateData("tbl_sku", $data, array("sku_id" => $rs_sku[0]['sku_id']));  
                            if ($result) {  
                                $imported++;
                            } else {
                                $notimported++;
                            }
                        }else{
                             //insert data 
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $result = $this->common->insertData("tbl_sku", $data, "1");
                            if ($result) {
                                $imported++;
                            } else {
                                $notimported++;
                            } 
                        }  
                    }
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    } 

    //business partner billing and shipping details dropdown
    function getBusinessPartner() {
        $bp_id = $_GET['bp_id'];
        $billing_id = $_GET['billing_id'];
        $shipping_id = $_GET['shipping_id'];
        $view = $_GET['isView'];
        $dropDownCss = '';
        if($view==1){
            $dropDownCss = "disabled"; 
        }

        $billingstr = "<select name='billing_details_id' id='billing_details_id' onchange='getBillAddress(this.value)' class='basic-single select-form-mro' $dropDownCss>"
                . "<option value='0'>Select Billing Details</option>";
        $shippingstr = "<select name='shipping_details_id' id='shipping_details_id' onchange='getShipAddress(this.value)' class='basic-single select-form-mro' $dropDownCss>"
                . "<option value='0'>Select Shipping Details</option>";
        $payment = '';
        $incoterm = '';
        if ($bp_id) {
            //billing details
            $billingDetail = $this->common->getData("tbl_billing_details", "*", array("business_partner_id" => $bp_id));
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    $selected = '';
                    $address = $value['contact_person'] . " (" . $value['address_line_1'] . ")";
                    if ($billing_id == $value['billing_details_id']) {
                        $selected = 'selected';
                    }
                    $billingstr = $billingstr . "<option value='" . $value['billing_details_id'] . "' $selected >" . $address . "</option>";
                }
            }
            $billingstr = $billingstr . "</select>";
            //shipping details
            $shippingDetail = $this->common->getData("tbl_shipping_details", "*", array("business_partner_id" => $bp_id));
            if (!empty($shippingDetail)) {
                foreach ($shippingDetail as $key => $value) {
                    $selected = '';
                    $address = $value['contact_person'] . " (" . $value['address_line_1'] . ")";
                    if ($shipping_id == $value['shipping_details_id']) {
                        $selected = 'selected';
                    }
                    $shippingstr = $shippingstr . "<option value='" . $value['shipping_details_id'] . "' $selected >" . $address . "</option>";
                }
            }
            $shippingstr = $shippingstr . "</select>";
            //business partner payment term value
            $bpDetail = $this->common->getData("tbl_business_partner", "payment_terms_id,incoterms_id", array("business_partner_id" => $bp_id));
            if (!empty($bpDetail[0]['payment_terms_id'])) {
                $paymentTermDetail = $this->common->getData("tbl_payment_term", "payment_term_value", array("payment_term_id" => $bpDetail[0]['payment_terms_id']));
                if (!empty($paymentTermDetail)) {
                    $payment = $paymentTermDetail[0]['payment_term_value'];
                }
            }
            //business partner icoterm value
            if (!empty($bpDetail[0]['incoterms_id'])) {
                $incotermDetail = $this->common->getData("tbl_incoterms", "incoterms_value", array("incoterms_id" => $bpDetail[0]['incoterms_id']));
                if (!empty($incotermDetail)) {
                    $incoterm = $incotermDetail[0]['incoterms_value'];
                }
            }
        }

        echo json_encode(array('billing' => $billingstr, 'shipping' => $shippingstr, 'payment' => $payment, 'incoterm' => $incoterm));
        exit;
    }

    //billing address details at click of billing details dropdown
    function getBillingAddress() {
        $bd_id = $_GET['bd_id'];
        $billingstr = "";
        if ($bd_id > 0) {
            $billingDetail = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $bd_id));
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    $countyStateCity = $this->getCountryStateCity($value['country_id'], $value['state_id'], $value['city_id']);
                    $address = $value['address_line_1'] . " " . $value['address_line_2'] . " " . $countyStateCity . " " . $value['zipcode'];
                    $billingstr = $billingstr . "<p class='bas-title'>Billing Address 11</p>
                          <p class='bas-text'>" . $address . "</p>
                          <p class='bas-title'>Billing Contact</p>
                          <p class='bas-text'>" . $value['contact_person'] . "</p> 
                          <p class='bas-title'>Billing Email</p>
                          <p class='bas-text'>" . $value['email'] . "</p>
                          <p class='bas-title'>Billing Phone</p>
                          <p class='bas-text'>" . $value['number'] . "";
                }
            }
        }
        echo json_encode(array('billing' => $billingstr));
        exit;
    }

    //shipping address details at click of shipping details dropdown
    function getShipAddress() {
        $bd_id = $_GET['bd_id'];
        $billingstr = "";
        if ($bd_id > 0) {
            $billingDetail = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $bd_id));
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    $countyStateCity = $this->getCountryStateCity($value['country_id'], $value['state_id'], $value['city_id']);
                    $address = $value['address_line_1'] . " " . $value['address_line_2'] . " " . $countyStateCity . " " . $value['zipcode'];
                    $billingstr = $billingstr . "<p class='bas-title'>Shipping Address</p>
                          <p class='bas-text'>" . $address . "</p>
                          <p class='bas-title'>Shipping Contact</p>
                          <p class='bas-text'>" . $value['contact_person'] . "</p> 
                          <p class='bas-title'>Shipping Email</p>
                          <p class='bas-text'>" . $value['email'] . "</p>
                          <p class='bas-title'>Shipping Phone</p>
                          <p class='bas-text'>" . $value['number'] . "";
                }
            }
        }

        echo json_encode(array('billing' => $billingstr));
        exit;
    }

    //country, state, city name
    function getCountryStateCity($countyId = 0, $stateID = 0, $cityID = 0) {
        $str = ',';
        $cityDetail = $this->common->getData("tbl_city", "city_name", array("city_id" => $cityID));
        if (!empty($cityDetail)) {
            $str = $str . $cityDetail[0]['city_name'] . ",";
        }
        $stateDetail = $this->common->getData("tbl_state", "state_name", array("state_id" => $stateID));
        if (!empty($stateDetail)) {
            $str = $str . " " . $stateDetail[0]['state_name'] . ",";
        }
        $countryDetail = $this->common->getData("tbl_country", "country_name", array("country_id" => $countyId));
        if (!empty($countryDetail)) {
            $str = $str . " " . $countryDetail[0]['country_name'];
        }
        return $str;
    }

    //business partner sku data
    function getskuDetails() {
        $sku_id = $_GET['sku_id'];
        $data = array();
        if ($sku_id > 0) {
            $skuDetail = $this->bpordermodel->getskuData($sku_id);
            if (!empty($skuDetail)) {
                $data = $skuDetail[0];
            }
        }
        echo json_encode(array('skuData' => $data));
        exit;
    }
    //payment term and incoterm dropdown
    function getPaymentIncoterm() {
        $cnt = $_GET['cnt'];
        $billingstr = '';
        $shippingstr = '';
        if ($cnt == 1) {
            $billingstr = "<select name='patment_terms' id='patment_terms' class='basic-single select-form-mro' style='width: 97%;'>"
                    . "<option value=''>Select Payment Terms</option>";
            //payment term data        
            $paymentTermDetails = $this->common->getData("tbl_payment_term", "payment_term_id,payment_term_value", array("status"=>"Active"));
            if (!empty($paymentTermDetails)) {
                foreach ($paymentTermDetails as $key => $value) {
                    $billingstr = $billingstr . "<option value='" . $value['payment_term_value'] . "' >" . $value['payment_term_value'] . "</option>";
                }
            }
            $billingstr = $billingstr . "</select>";
        } else {
            $shippingstr = "<select name='incoterms' id='incoterms' class='basic-single select-form-mro' style='width: 97%;'>"
                    . "<option value=''>Select Incoterm</option>";
            //Incoterm data        
            $incotermDetails = $this->common->getData("tbl_incoterms", "incoterms_id,incoterms_value", array("status"=>"Active"));
            if (!empty($incotermDetails)) {
                foreach ($incotermDetails as $key => $value) {
                    $shippingstr = $shippingstr . "<option value='" . $value['incoterms_value'] . "' >" . $value['incoterms_value'] . "</option>";
                }
            }
            $shippingstr = $shippingstr . "</select>";
        }

        echo json_encode(array('paymentTerm' => $billingstr, 'incoTerm' => $shippingstr));
        exit;
    }

}

?>
