<?PHP

class ListOfDocumentmodel extends CI_Model {

    function getRecords($get) {

        $table = "tbl_document_type";
        $default_sort_column = 'dt.document_type_squence';
        $default_sort_order = 'Asc';
        $condition = "1=1 ";

        $colArray = array('dt.document_type_name', 'dt.status');
        $sortArray = array('dt.document_type_name', 'dt.status');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

        for ($i = 0; $i < count($colArray); $i++) {
            if (isset($_GET['Searchkey_' . $i]) && $_GET['Searchkey_' . $i] != "") {
                $condition .= " AND " . $colArray[$i] . " LIKE '%" . $_GET['Searchkey_' . $i] . "%' ";
            }
        }

        $this->db->select('dt.*,group_concat(sdt.sub_document_type_name) as sub_document_type');
        $this->db->from("$table as dt");
        $this->db->join("tbl_sub_document_type as sdt","sdt.document_type_id = dt.document_type_id","left");
        $this->db->where("($condition)");
        $this->db->group_by('dt.document_type_id');
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page);
        $query = $this->db->get();
        // echo $this->db->last_query();exit; 

        $this->db->select('dt.*,group_concat(sdt.sub_document_type_name) as sub_document_type');
        $this->db->from("$table as dt");
        $this->db->join("tbl_sub_document_type as sdt","sdt.document_type_id = dt.document_type_id","left");
        $this->db->where("($condition)");
        $this->db->group_by('dt.document_type_id');
        $this->db->order_by($sort, $order);
        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    }

}

?>
