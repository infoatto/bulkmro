<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="<?php echo base_url("dashboard") ?>">Home</a>
              <span>></span>
              <p>Dashboard</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title"><a href="<?php echo base_url("listOfDocument"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Document Type</h1>
            </div>
          </div>
          <div class="title-sec-right">
            <?php if ($this->privilegeduser->hasPrivilege("DocumentTypeAddEdit")) { ?>
              <a href="<?php echo base_url("listOfDocument/addEdit"); ?>" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> Add Document Type</a>
            <?php } ?>
          </div>
        </div>
        <div class="page-content-wrapper1">
          <div id="serchfilter" class="filter-sec-wrapper">
            <div class="form-group filter-search dataTables_filter searchFilterClass">
              <input type="text" id="sSearch_0" name="sSearch_0" class="searchInput filter-search-input" placeholder="Document Name">
            </div>
            <div class="form-group clear-search-filter">
              <button class="btn-primary-mro" onclick="clearSearchFilters();">Clear Search</button>
            </div>
          </div>
        </div>

        <!-- <div class="row "> -->

        <!-- </div> -->

        <div class="bp-list-wrapper">
          <div class="card">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-hover basic-datatables dynamicTable text-left" noofrecords=20 cellpadding="0" cellspacing="0">
                  <thead>
                    <tr>
                      <th>SR no</th>
                      <th>Document Name</th>
                      <th>Sub Document Name</th>
                      <th>Status</th>
                      <th class="table-action-cls" style="width:50px !important;">Actions</th>
                    </tr>
                  </thead>
                  <?php if (!empty($this->session->flashdata('message-ordering'))) { ?>
                    <div class="alert alert-success" id="MSG">
                      <?php echo $this->session->flashdata('message-ordering'); ?>
                    <?php } ?>
                    <tbody class="row_position">
                    </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
  // $(".row_position" ).sortable({
  //     // alert("hello");
  //     delay: 100,
  //     stop: function() {
  //         var selectedData = new Array();
  //         $('.row_position>tr>td>span.squence').each(function() {
  //             selectedData.push($(this).attr("tr_row"));
  //         });
  //         updateOrder(selectedData);
  //     }
  // });

  function updateOrder(data) {
    console.log(data);
    // alert(data);
    $.ajax({
      url: "<?php echo base_url('listOfDocument/changeorder'); ?>",
      type: 'post',
      data: {
        position: data
      },
      success: function(result) {
        window.location.reload();
      }

    })
  }

  $(document).ready(function() {
    // Add Row
    $('#MSG').slideUp(2000);


    $('#add-row').DataTable({
      "pageLength": 10,
      //"ordering": false,
      "aaSorting": [],
      columnDefs: [{
        orderable: false,
        targets: [3, 4]
      }]

    });

  });
</script>