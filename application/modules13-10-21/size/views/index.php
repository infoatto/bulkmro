<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard") ?>">Home</a>
                            <span>></span>
                            <p>Dashboard</p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title"><a href="<?php echo base_url("size"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Size</h1>
                        </div>
                    </div>
                    
                    <div class="title-sec-right">
                        <?php if ($this->privilegeduser->hasPrivilege("SizeAddEdit")) { ?>
                            <a href="<?php echo base_url("size/addEdit"); ?>" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> Add Size</a>                            
                        <?php } ?> 
                        <?php if ($this->privilegeduser->hasPrivilege("SizeImport")) { ?>                            
                            <a href="#" class="btn-primary-mro" id="import">Import</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="page-content-wrapper" id="efrom" style="display:none;">
                    <form id="excel_form" name="excel_form" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload Excel</h6> 
                            <input type="file" id="excelfile" name="excelfile" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel()" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample_size.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div> </br>
                <div class="page-content-wrapper1">
                    <div id="serchfilter" class="filter-sec-wrapper">
                        <div class="form-group filter-search dataTables_filter searchFilterClass">
                            <input type="text" id="sSearch_0" name="sSearch_0" class="searchInput filter-search-input" placeholder="Size Name">
                        </div>
                        <div class="form-group clear-search-filter">
                            <button class="btn-primary-mro" onclick="clearSearchFilters();">Clear Search</button> 
                        </div>
                    </div>
                </div>
                <div class="bp-list-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped  basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Size Name</th>
                                    <th>Status</th>
                                    <th class="table-action-cls" style="width:50px !important;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

 function uploadExcel()
 {
    if( document.getElementById("excelfile").files.length == 0 ){ 
        showInsertUpdateMessage("Please Choose a xlsx file.",false);   
    }
    else
    {
        $(".upload-button").hide();
        var data = new FormData($('#excel_form')[0]);
        $.ajax({
            url: '<?php echo base_url('size/importSize'); ?>',
            type: 'POST',
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            data: data, 
            success: function(data)
            { 
                var response = JSON.parse(data); 
                if (response.success) {  
                    showInsertUpdateMessage(response.msg,response);
                    showInsertUpdateMessage("Total entry done: "+response.imported,response); 
                    if(response.notimported > 0){
                        showInsertUpdateMessage("Total entry failed: "+response.notimported,false);   
                    }
                    $('#excel_form')[0].reset();
                    setTimeout(function () {  
                        location.reload();
                    }, 3000);
                }else{
                    showInsertUpdateMessage(response.msg,response,false);
                }
                $(".upload-button").show();
            }
        });
    }
}

$("#import").click(function(){
   $("#efrom").toggle();
});
</script>