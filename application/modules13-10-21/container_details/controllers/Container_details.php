<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Container_details extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('container_detailsmodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("ContainersDetailsView")){
            redirect('dashboard');
        }
    }

    function index() {
        $data['container_details'] = array();
        $data['show_details'] = array();
        $data['customerAlias'] = array();
        $data['supplierAlias'] = array();
        $data['fcr'] = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $container_id = $url_prams['id'];
            //fetch container data
            $containerResult = $this->common->getData("tbl_container", "*", array("container_id" => $container_id));
            if (!empty($containerResult)) {
                $data['container_details'] = $containerResult[0];
                
                //container status  
                $containerStatusData =  $this->container_detailsmodel->getContainerStatus($containerResult[0]['container_id']); 
                if(!empty($containerStatusData)){
                    $data['show_details']['containerStatus'] = $containerStatusData[0]['container_status_name'];
                } 
                
                //last update by and date
                $updateByData = $this->common->getData("tbl_users", "firstname,lastname", array("user_id" => $containerResult[0]['updated_by']));
                if(!empty($updateByData)){
                    $data['show_details']['updateBy'] = $updateByData[0]['firstname'].' '.substr($updateByData[0]['lastname'],0, 1).', '.date('d-M-y', strtotime($containerResult[0]['updated_on']));
                } 
                        
                //fetch CHA
                $freightForwarderData = $this->common->getData("tbl_business_partner", "business_name", array("business_partner_id" => $containerResult[0]['freight_forwarder_id']));
                if(!empty($freightForwarderData)){
                    $data['freightForwarder'] = $freightForwarderData[0]['business_name'];
                }
                
                //fetch freight forwarder
                $brokerData = $this->common->getData("tbl_business_partner", "business_name", array("business_partner_id" => $containerResult[0]['broker_id']));
                if(!empty($brokerData)){
                    $data['CHA'] = $brokerData[0]['business_name'];
                }
                
                 //fetch customer and supplier billing and shipping details data
                $orderData = $this->container_detailsmodel->getAddressDetails($container_id);
                $custBillData = array();
                $custShipData = array(); 
                if (!empty($orderData)) {
                    foreach ($orderData as $value) {  
                        $custBillData = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $value['customer_billing_details_id']));
                        $suppShipData = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $value['supplier_shipping_details_id']));
                    }
                } 
                //fetch customer alias 
                if(!empty($custBillData[0]['business_partner_id'])){
                    $customerAlias = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $custBillData[0]['business_partner_id']));
                    if(!empty($customerAlias)){
                        $data['customerAlias'] = $customerAlias[0]['alias'];
                    }
                } 

                //fetch supplier alias 
                if(!empty($suppShipData[0]['business_partner_id'])){
                    $supplierAlias = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $suppShipData[0]['business_partner_id']));
                    if(!empty($supplierAlias)){
                        $data['supplierAlias'] = $supplierAlias[0]['alias'];
                    }
                }  
                
                //get FCR details
                $fcrData = $this->container_detailsmodel->getFCRDetails($container_id);
                if(!empty($fcrData)){
                    foreach ($fcrData as $value) {  
                        $data['fcr'][$value['custom_field_structure_id']] = $value['custom_field_structure_value'];
                    }
                }                      
                
            }  
            
            
            $data['editURL'] = 'container/addEdit?text='.$_GET['text'];
            
            $this->load->view('template/head.php');
            $this->load->view('template/navigation.php');
            $this->load->view('container_details/index', $data);
            $this->load->view('template/footer.php');
            $this->load->view('template/footer-scripts.php');
        }
    } 
    
    function getEditableForm(){
        $container_id = $_GET['container_id']; 
        $data['container_details'] = array();
        $data['containerStatusId'] = 0;
        $data['freightForwarderId'] = 0;
        $data['CHA_id'] = 0;
        $data['customerAlias'] = array();
        $data['supplierAlias'] = array();
        $data['containerStatusData'] = array();
        $data['freightForwarderData'] = array();
        $data['CHAData'] = array();
        
        $data['fcr'] = array();
        if (!empty($container_id) && $container_id > 0) { 
            //fetch container data
            $containerResult = $this->common->getData("tbl_container", "*", array("container_id" => $container_id));
            if (!empty($containerResult)) {
                $data['container_details'] = $containerResult[0];
                
                //container status  
                $containerStatusData =  $this->container_detailsmodel->getContainerStatus($containerResult[0]['container_id']); 
                if(!empty($containerStatusData)){
                    $data['containerStatusId'] = $containerStatusData[0]['container_status_id'];
                }                      
                        
                //fetch CHA
                $freightForwarderData = $this->common->getData("tbl_business_partner", "business_partner_id", array("business_partner_id" => $containerResult[0]['freight_forwarder_id']));
                if(!empty($freightForwarderData)){
                    $data['freightForwarderId'] = $freightForwarderData[0]['business_partner_id'];
                }
                
                //fetch freight forwarder
                $brokerData = $this->common->getData("tbl_business_partner", "business_partner_id", array("business_partner_id" => $containerResult[0]['broker_id']));
                if(!empty($brokerData)){
                    $data['CHA_id'] = $brokerData[0]['business_partner_id'];
                }
                
                 //fetch customer and supplier billing and shipping details data
                $orderData = $this->container_detailsmodel->getAddressDetails($container_id);
                $custBillData = array();
                $custShipData = array(); 
                if (!empty($orderData)) {
                    foreach ($orderData as $value) {  
                        $custBillData = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $value['customer_billing_details_id']));
                        $suppShipData = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $value['supplier_shipping_details_id']));
                    }
                } 
                //fetch customer alias 
                if(!empty($custBillData[0]['business_partner_id'])){
                    $customerAlias = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $custBillData[0]['business_partner_id']));
                    if(!empty($customerAlias)){
                        $data['customerAlias'] = $customerAlias[0]['alias'];
                    }
                } 

                //fetch supplier alias 
                if(!empty($suppShipData[0]['business_partner_id'])){
                    $supplierAlias = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $suppShipData[0]['business_partner_id']));
                    if(!empty($supplierAlias)){
                        $data['supplierAlias'] = $supplierAlias[0]['alias'];
                    }
                }  
                
                //get FCR details
                $fcrData = $this->container_detailsmodel->getFCRDetails($container_id);
                if(!empty($fcrData)){
                    foreach ($fcrData as $value) {  
                        $data['fcr'][$value['custom_field_structure_id']] = $value['custom_field_structure_value'];
                    }
                }                      
                
            }  
                                        
            $containerStatusData = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", array("status"=>"Active"));
            if(!empty($containerStatusData)){
                $data['containerStatusData'] = $containerStatusData;
            }                          
            
            $freightForwarderData = $this->common->getData("tbl_business_partner", "business_partner_id,business_name", array("business_type" => "Vendor", "business_category" => "Freight Forwarder", "status" => "Active"));
            if (!empty($freightForwarderData)) {
                $data['freightForwarderData'] = $freightForwarderData;
            }

            $CHAData = $this->common->getData("tbl_business_partner", "business_partner_id,business_name", array("business_type" => "Vendor", "business_category" => "Customs Broker", "status" => "Active"));
            if (!empty($CHAData)) {
                $data['CHAData'] = $CHAData;
            } 
            
            
            $html = "";                     
            $html = $this->load->view("index-editable-form",$data,true); 

            echo json_encode(array('html' =>$html));
            exit;  
        }      
       
    }
    
    function editableSubmitForm(){ 
                                        
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "container_number = '" . $this->input->post('container_number') . "' ";
            if (!empty($this->input->post("container_id"))) {
                $condition .= " AND container_id <> " . $this->input->post("container_id");
            } 

            $chk_client_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");
                                        
            if (!empty($rs_client[0]['container_id'])) { 
                echo json_encode(array('success' => false, 'msg' => 'Container Number already exist...'));
                exit;
            }                           

            $data = array();
            $data['container_number'] = $this->input->post('container_number'); 
            $data['broker_id'] = $this->input->post('broker_id'); 
            $data['freight_forwarder_id'] = $this->input->post('freight_forwarder_id');             
            $data['revised_eta'] = !empty($this->input->post('revised_eta')) ? date("Y-m-d", strtotime($this->input->post('revised_eta'))) : NULL;
            $data['revised_etd'] = !empty($this->input->post('revised_etd')) ? date("Y-m-d", strtotime($this->input->post('revised_etd'))) : NULL;             
            $data['ett'] = $this->input->post('ett'); 
            $data['revised_ett'] = $this->input->post('revised_ett');             
            $data['liner_name'] = $this->input->post('liner_name'); 
            $data['pol'] = $this->input->post('pol'); 
            $data['pod'] = $this->input->post('pod'); 
            $data['vessel_name'] = $this->input->post('vessel_name'); 
            $data['flag'] = $this->input->post('flag');
            $data['email_sent_on'] = !empty($this->input->post('email_sent_on')) ? date("Y-m-d", strtotime($this->input->post('email_sent_on'))) : NULL;
            $data['email_subject'] = $this->input->post('email_subject'); 
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            if (!empty($this->input->post("container_id"))) {
                //update container
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_container", $data, array("container_id" => $this->input->post("container_id"))); 
                                        
                if (!empty($this->input->post("container_status_id")) && ($this->input->post("old_container_status_id") != $this->input->post("container_status_id"))) { 
                    $data1['container_id'] = $this->input->post("container_id");
                    $data1['container_status_id'] = $this->input->post("container_status_id");                    
                    $data1['created_on'] = date("Y-m-d H:i:s");          
                    $data1['created_by'] = $_SESSION['mro_session'][0]['user_id']; 
                    $result1 = $this->common->insertData('tbl_container_status_log', $data1, '1'); 
                }                       
                
                if ($result) {
                    
                    //container details show information
                    $container_id = $this->input->post("container_id"); 
                    $data['container_details'] = array();
                    $data['show_details'] = array();
                    $data['customerAlias'] = array();
                    $data['supplierAlias'] = array();
                    $data['fcr'] = array();
                    if (!empty($container_id) && $container_id > 0) { 
                        //fetch container data
                        $containerResult = $this->common->getData("tbl_container", "*", array("container_id" => $container_id));
                        if (!empty($containerResult)) {
                            $data['container_details'] = $containerResult[0];

                            //container status  
                            $containerStatusData =  $this->container_detailsmodel->getContainerStatus($containerResult[0]['container_id']); 
                            if(!empty($containerStatusData)){
                                $data['show_details']['containerStatus'] = $containerStatusData[0]['container_status_name'];
                            } 

                            //last update by and date
                            $updateByData = $this->common->getData("tbl_users", "firstname,lastname", array("user_id" => $containerResult[0]['updated_by']));
                            if(!empty($updateByData)){
                                $data['show_details']['updateBy'] = $updateByData[0]['firstname'].' '.substr($updateByData[0]['lastname'],0, 1).', '.date('d-M-y', strtotime($containerResult[0]['updated_on']));
                            } 

                            //fetch CHA
                            $freightForwarderData = $this->common->getData("tbl_business_partner", "business_name", array("business_partner_id" => $containerResult[0]['freight_forwarder_id']));
                            if(!empty($freightForwarderData)){
                                $data['freightForwarder'] = $freightForwarderData[0]['business_name'];
                            }

                            //fetch freight forwarder
                            $brokerData = $this->common->getData("tbl_business_partner", "business_name", array("business_partner_id" => $containerResult[0]['broker_id']));
                            if(!empty($brokerData)){
                                $data['CHA'] = $brokerData[0]['business_name'];
                            }

                             //fetch customer and supplier billing and shipping details data
                            $orderData = $this->container_detailsmodel->getAddressDetails($container_id);
                            $custBillData = array();
                            $custShipData = array(); 
                            if (!empty($orderData)) {
                                foreach ($orderData as $value) {  
                                    $custBillData = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $value['customer_billing_details_id']));
                                    $suppShipData = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $value['supplier_shipping_details_id']));
                                }
                            } 
                            //fetch customer alias 
                            if(!empty($custBillData[0]['business_partner_id'])){
                                $customerAlias = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $custBillData[0]['business_partner_id']));
                                if(!empty($customerAlias)){
                                    $data['customerAlias'] = $customerAlias[0]['alias'];
                                }
                            } 

                            //fetch supplier alias 
                            if(!empty($suppShipData[0]['business_partner_id'])){
                                $supplierAlias = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $suppShipData[0]['business_partner_id']));
                                if(!empty($supplierAlias)){
                                    $data['supplierAlias'] = $supplierAlias[0]['alias'];
                                }
                            }  

                            //get FCR details
                            $fcrData = $this->container_detailsmodel->getFCRDetails($container_id);
                            if(!empty($fcrData)){
                                foreach ($fcrData as $value) {  
                                    $data['fcr'][$value['custom_field_structure_id']] = $value['custom_field_structure_value'];
                                }
                            }                      

                        }  
                    }                         

                    $html = "";                     
                    $html = $this->load->view("index-without-editable",$data,true);  
                    
                    echo json_encode(array('success' => true, 'msg' => 'Container updated successfully.','html' => $html));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while updating data.'));
                    exit;
                }
            }   
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while updating data.'));
            exit;
        } 
    }

    function getProductDetails() {
        $container_id = $_GET['container_id'];
        $productData = $this->container_detailsmodel->getProductDetails($container_id);
        $strtr = '';
        $strtable = '';
        if (!empty($productData)) {
            $srNo = 0;
            foreach ($productData as $value) {
                $srNo++;
                $strtr .= '<tr>
                        <td>'.$srNo.'</td>
                        <td>'.$value['product_name'].'</td>
                        <td>'.$value['sku_number'].'</td>
                        <td>'.$value['business_name'].'</td>
                        <td>'.$value['sku_vendor'].'</td>
                        <td>'.$value['brand_name'].'</td>
                        <td>'.$value['size_name'].'</td>
                        <td>'.$value['quantity'].'</td>
                        <td>'.$value['uom_name'].'</td>
                    </tr>';
            }
        }
        
        $strtable .= '<table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Sr. No.</th>
                                <th scope="col">Product Description</th>
                                <th scope="col">BM SKU</th>
                                <th scope="col">Manufacturer</th>
                                <th scope="col">Manufacturer SKU</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Size</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">UoM</th>
                            </tr>
                        </thead>
                        <tbody>'; 
        $strtable = $strtable.$strtr.'</tbody></table>';
        echo json_encode(array('productDetails' => $strtable));
        exit;
    }
    
    function getAddressDetails() {
        $container_id = $_GET['container_id'];
        //fetch billing and shipping details id from master contact
        $orderData = $this->container_detailsmodel->getAddressDetails($container_id); 
        $str = ''; 
        $custBillData = array();
        $custShipData = array();
        $suppBillData = array();
        $suppShipData = array();
        $customerContactId = 0;
        $supplierContactId =0;
        $customerContact = ''; 
        $supplierContact = '';
        if (!empty($orderData)) {
            foreach ($orderData as $value) {  
                //fetch customer and supplier billing and shipping details data
                $custBillData = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $value['customer_billing_details_id']));
                $custShipData = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $value['customer_shipping_details_id']));
                $suppBillData = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $value['supplier_billing_details_id']));
                $suppShipData = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $value['supplier_shipping_details_id']));
                
                //fetch customer and vendor contract 
                $customerDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $value['customer_contract_id']));
                if(!empty($customerDetail)){
                    $customerContactId = $customerDetail[0]['bp_order_id']; 
                    $customerContact = $customerDetail[0]['contract_number'];
                }
                $supplierDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $value['supplier_contract_id']));
                if(!empty($supplierDetail)){
                    $supplierContactId = $supplierDetail[0]['bp_order_id'];
                    $supplierContact = $supplierDetail[0]['contract_number'];
                }
            }
        } 
        //fetch customer data
        $customerData = array();
        if(!empty($custBillData[0]['business_partner_id'])){
            $customerData = $this->common->getData("tbl_business_partner", "*", array("business_partner_id" => $custBillData[0]['business_partner_id']));
        } 
        
        //fetch supplier data
        $supplierData = array();
        if(!empty($suppShipData[0]['business_partner_id'])){
            $supplierData = $this->common->getData("tbl_business_partner", "*", array("business_partner_id" => $suppShipData[0]['business_partner_id']));
        } 
        
        //customer address details
        $cust_bill_countyStateCity = $this->getCountryStateCity($custBillData[0]['country_id'], $custBillData[0]['state_id'], $custBillData[0]['city_id']);
        $cust_bill_address = $custBillData[0]['address_line_1'] . " " . $custBillData[0]['address_line_2'] . " " . $cust_bill_countyStateCity . " " . $custBillData[0]['zipcode'];
        $cust_ship_countyStateCity = $this->getCountryStateCity($custShipData[0]['country_id'], $custShipData[0]['state_id'], $custShipData[0]['city_id']);
        $cust_ship_address = $custShipData[0]['address_line_1'] . " " . $custShipData[0]['address_line_2'] . " " . $cust_ship_countyStateCity . " " . $custShipData[0]['zipcode'];
        
        //supplier address details
        $supp_bill_countyStateCity = $this->getCountryStateCity($suppBillData[0]['country_id'], $suppBillData[0]['state_id'], $suppBillData[0]['city_id']);
        $supp_bill_address = $suppBillData[0]['address_line_1'] . " " . $suppBillData[0]['address_line_2'] . " " . $supp_bill_countyStateCity . " " . $suppBillData[0]['zipcode'];
        $supp_ship_countyStateCity = $this->getCountryStateCity($suppShipData[0]['country_id'], $suppShipData[0]['state_id'], $suppShipData[0]['city_id']);
        $supp_ship_address = $suppShipData[0]['address_line_1'] . " " . $suppShipData[0]['address_line_2'] . " " . $supp_ship_countyStateCity . " " . $suppShipData[0]['zipcode'];
         
        $url = "http://bulkmro.webshowcase-india.com/bporder/addEdit?text";
        if($_SERVER['HTTP_HOST']=='localhost'){
            $url = "http://localhost/bulkmro/bporder/addEdit?text";
        }
        
        $customerContractStr = '<p class="sit-single-value">'.$customerContact.'</p>';
        $supplierContractStr = '<p class="sit-single-value">'.$supplierContact.'</p>';
        if($this->privilegeduser->hasPrivilege("BusinessPartnersOrderView")){
            $customerContractStr = '<p class="sit-single-value"><a href="'.$url.'=' . rtrim(strtr(base64_encode("id=" . $customerContactId), '+/', '-_'), '=') . '&view=1" target="_blank"><u>'.$customerContact.'</u></a></p>';
            $supplierContractStr = '<p class="sit-single-value"><a href="'.$url.'=' . rtrim(strtr(base64_encode("id=" . $supplierContactId), '+/', '-_'), '=') . '&view=1" target="_blank"><u>'.$supplierContact.'</u></a></p>';
        }
        
        $str .= '<div class="tab-content-wrapper">
                    <div class="sit-wrapper nat-content">
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Business Alias</p>
                                <p class="sit-single-value">'.$customerData[0]['alias'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Business Partner Type</p>
                                <p class="sit-single-value">'.$customerData[0]['business_type'].', '.$customerData[0]['business_category'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Contact Name</p>
                                <p class="sit-single-value">'.$customerData[0]['contact_person'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Email</p>
                                <p class="sit-single-value">'.$customerData[0]['email'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Phone</p>
                                <p class="sit-single-value">'.$customerData[0]['phone_number'].'</p>
                            </div>
                        </div>
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Billing Address</p>
                                <p class="sit-single-value">'.$cust_bill_address.'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Billing Contact</p>
                                <p class="sit-single-value">'.$custBillData[0]['number'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Address</p>
                                <p class="sit-single-value">'.$cust_ship_address.'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Contact</p>
                                <p class="sit-single-value">'.$custShipData[0]['number'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Customer Contract</p>
                                '.$customerContractStr.'
                            </div>                      
                        </div>                    
                    </div>
                    <hr class="separator">
                    <div class="sit-wrapper nat-content">
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Vendor Alias</p>
                                <p class="sit-single-value">'.$supplierData[0]['alias'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Business Partner Type</p>
                                <p class="sit-single-value">'.$supplierData[0]['business_type'].', '.$supplierData[0]['business_category'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Contact Name</p>
                                <p class="sit-single-value">'.$supplierData[0]['contact_person'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Email</p>
                                <p class="sit-single-value">'.$supplierData[0]['email'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Phone</p>
                                <p class="sit-single-value">'.$supplierData[0]['phone_number'].'</p>
                            </div>
                        </div> 
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Billing Address</p>
                                <p class="sit-single-value">'.$supp_bill_address.'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Billing Contact</p>
                                <p class="sit-single-value">'.$suppBillData[0]['number'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Address</p>
                                <p class="sit-single-value">'.$supp_ship_address.'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Contact</p>
                                <p class="sit-single-value">'.$suppShipData[0]['number'].'</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Vendor Contract</p>
                                '.$supplierContractStr.'
                            </div>                      
                        </div>                    
                    </div>
                </div>';
        echo json_encode(array('productDetails' => $str));
        exit;
    }
    
    //country, state, city name
    function getCountryStateCity($countyId = 0, $stateID = 0, $cityID = 0) {
        $str = ',';
        $cityDetail = $this->common->getData("tbl_city", "city_name", array("city_id" => $cityID));
        if (!empty($cityDetail)) {
            $str = $str . $cityDetail[0]['city_name'] . ",";
        }
        $stateDetail = $this->common->getData("tbl_state", "state_name", array("state_id" => $stateID));
        if (!empty($stateDetail)) {
            $str = $str . " " . $stateDetail[0]['state_name'] . ",";
        }
        $countryDetail = $this->common->getData("tbl_country", "country_name", array("country_id" => $countyId));
        if (!empty($countryDetail)) {
            $str = $str . " " . $countryDetail[0]['country_name'];
        }
        return $str;
    } 
    
    //get invoice details
    function getInvoiceDetails() {
        $container_id = $_GET['container_id']; 
        $invoiceData = $this->container_detailsmodel->getInvoiceDetails($container_id); 
        $str = '<div class="tab-content-wrapper">
                    <h3 class="form-group-title">Uploaded Invoices</h3>
                    <div class="table-responsive">
                        <table class="table list-table uploaded-invoices-table">
                            <tbody>';
                            $strtr = ''; 
                            if (!empty($invoiceData)) { 
                                $cnt = 0;
                                foreach ($invoiceData as $value) { 
                                    $paid = ""; $unpaid = "";  $partial = ""; 
                                    switch ($value['invoice_status']) {
                                        case "paid":
                                            $paid = "selected"; 
                                            break;
                                         case "unpaid":
                                            $unpaid = "selected";  
                                            break; 
                                        default:
                                            $partial = "selected"; 
                                            break;
                                    }
                                    $strtr .= '<tr>
                                    <td style="width:150px">
                                        <div class="form-group ">
                                            <input type="date" name="invoice_date['.$cnt.']" id="invoice_date'.$cnt.'" value="'.date('Y-m-d', strtotime($value['invoice_date'])).'" style="width: 95%" class="input-form-mro valid editable-field" aria-invalid="false">
                                        </div>
                                    </td>
                                    <td style="width:250px">
                                        <div class="form-group">
                                            <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="'.$value['invoice_file_name'].'" readonly>
                                        </div>
                                    </td>
                                    <td style="width:150px">
                                        '.$value['business_name'].'
                                    </td>
                                    <td style="width:200px">
                                        <div class="form-group ">
                                            <select name="invoice_status['.$cnt.']" id="invoice_status'.$cnt.'" class="select-form-mro editable-field select-large">
                                                <option value="paid" '.$paid.'>Paid</option>
                                                <option value="unpaid" '.$unpaid.'>Un-Paid</option>
                                                <!--<option value="partial" '.$partial.'>Partial</option>-->
                                            </select>
                                        </div>
                                    </td>
                                    <td style="width:150px">
                                        <div class="form-group ">
                                            <input type="date" name="paid_date['.$cnt.']" id="paid_date'.$cnt.'" value="'.date('Y-m-d', strtotime($value['paid_date'])).'" style="width: 95%" class="input-form-mro editable-field valid" aria-invalid="false">
                                        </div>
                                    </td>
                                    <td style="width:150px">
                                        <div class="form-group">
                                            <input type="text" name="invoice_value['.$cnt.']" id="invoice_value'.$cnt.'" placeholder="Enter Contract Number" class="input-form-mro editable-field" value="'.$value['invoice_value'].'">
                                        </div>
                                    </td>
                                    <td style="width:170px">';
                                        if($this->privilegeduser->hasPrivilege("DocumentInvoiceUpdate")){
                                            $strtr .= '<img onclick="showUpdateButton('.$cnt.');" src="'.base_url().'assets/images/edit-icon.svg" alt="" class="mr8"> '; 
                                        }
                                        $strtr .= '<button class="btn-grey-mro" onclick="showInvoiceDetails('.$value['custom_field_invoice_data_submission_id'].');">View</button> 
                                    </td>
                                    <td style="width:220px">
                                        <div class="cs-status-text" id="cs-status-text'.$cnt.'" style="display:block">
                                            <p class="last-update-title" id="last-update-title'.$cnt.'">Last Updated</p>
                                            <p class="last-update-name" id="last-update-name'.$cnt.'">'.$value['firstname'].' '.substr($value['lastname'],0, 1).' , '.date('d-M-y', strtotime($value['updated_on'])).'</p>
                                            <p id="success-msg'.$cnt.'" style="display:none;color: #09cc09;"></p>
                                        </div>
                                        <div class="cs-status-text" id="cs-status-update'.$cnt.'" style="display:none">
                                            <button class="btn-primary-mro" onclick="updateInvoiceDetails('.$value['custom_field_invoice_data_submission_id'].','.$cnt.');">Done</button>
                                        </div>
                                    </td>
                                </tr> ';
                                        $cnt++;
                                }
                                
                            }
                                
                        $str =  $str.$strtr.'</tbody>
                        </table>
                    </div>
                </div>'; 
        
        echo json_encode(array('invoiceDetails' => $str));
        exit;
    }
    
    function updateInvoiceDetails(){
        $id = $_GET['id'];
        $invoice_date = $_GET['invoice_date'];
        $invoice_status = $_GET['invoice_status'];
        $paid_date = $_GET['paid_date'];
        $invoice_value = $_GET['invoice_value']; 
        $result = array();
                                        
        if($id > 0 && $this->privilegeduser->hasPrivilege("DocumentInvoiceUpdate")){                   
            $data['invoice_date'] = $invoice_date;
            $data['invoice_status'] = $invoice_status;
            $data['invoice_value'] = $invoice_value;
            $data['paid_date'] = $paid_date;
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            $result = $this->common->updateData("tbl_custom_field_invoice_data_submission", $data, array("custom_field_invoice_data_submission_id" => $id));
        }
        
        if ($result) {
            echo json_encode(array('success' => true, 'msg' => 'Successfully updated.'));
            exit;
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
            exit;
        }
        
    }
    
    function getLinkContainerDetails() {
        $container_id = $_GET['container_id']; 
        $LSFRIArr = array();
        $WithOutLSFRIArr = array();
        $linkContainerData = array();
        $linkContainerLSIds = array();
        $linkContainerWithOutLSIds = array();
        $LSArr = array();
        $WithOutLSArr = array();
        $LSFRIDocumentNumber = $this->common->getData("tbl_document_uploaded_files", "uploaded_document_number", array("container_id" => $container_id));
        $WithOutLSFRIDocumentNumber = $this->common->getData("tbl_document_ls_uploaded_files", "uploaded_document_number", array("container_id" => $container_id));
        
        //LS and FRI number
        if(!empty($LSFRIDocumentNumber)){
            foreach ($LSFRIDocumentNumber as $value) { 
                $LSFRIArr[] = $value['uploaded_document_number'];
            }
        }
        
        if(!empty($LSFRIArr)){
            $documentNumberString = implode("','",$LSFRIArr);
            $documentNumberString = "'".$documentNumberString."'"; 
            $linkContainerLSIds = $this->container_detailsmodel->getLinkContainers($documentNumberString,'LS'); 
        } 
        
        foreach ($linkContainerLSIds as $value){
            $LSArr[] = $value['container_id'];
        }
        
        //without LS and FRI number
        if(!empty($WithOutLSFRIDocumentNumber)){
            foreach ($WithOutLSFRIDocumentNumber as $value) { 
                $WithOutLSFRIArr[] = $value['uploaded_document_number'];
            }
        }
        
        if(!empty($WithOutLSFRIArr)){
            $documentNumberString = implode("','",$WithOutLSFRIArr);
            $documentNumberString = "'".$documentNumberString."'"; 
            $linkContainerWithOutLSIds = $this->container_detailsmodel->getLinkContainers($documentNumberString); 
        }  
        
        foreach ($linkContainerWithOutLSIds as $value){
            $WithOutLSArr[] = $value['container_id'];
        }  
        
        $linkContainerData = array_unique(array_merge($LSArr,$WithOutLSArr), SORT_REGULAR); 
//        print_r($LSArr);
//        print_r($WithOutLSArr);
//        print_r($linkContainerData);die; 
        
        $str = '<div class="table-responsive">
                    <table class="table list-table linked-containers-table">
                        <thead>
                            <tr>
                                <th scope="col">Linked Containers</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>';
                        if(!empty($linkContainerData)){
                            foreach ($linkContainerData as $value) {  
                                $linkContainerDetailsData = $this->container_detailsmodel->getLinkContainerDetails($value);
                                if(!empty($linkContainerDetailsData)){
                                    foreach ($linkContainerDetailsData as $value1) {  
                                        $str .= '<tr>
                                             <td>'.$value1['container_number'].'</td>
                                             <td>
                                                 <div class="cs-status-text">
                                                     <p class="container-status">'.$value1['container_status_name'].'</p>
                                                 </div>
                                             </td>
                                             <td>
                                                 <div class="cs-status-text">
                                                     <p class="last-update-title">Last Updated</p>
                                                     <p class="last-update-name">'.$value1['firstname'].' '.substr($value1['lastname'],0, 1).' , '.date('d-M-y', strtotime($value1['created_on'])).'</p>
                                                 </div>
                                             </td>
                                         </tr>'; 
                                    }
                                }
                            }
                        }
                      $str .=  '</tbody>
                    </table>
                </div>'; 
        
        echo json_encode(array('linkDetails' => $str));
        exit;
    }
    
    
    function getInvoiceDetailsView(){
        $id = $_GET['id'];  
        $invoiceDetails = '';
        $invoicePDF ='';
        $invoiceData = $this->container_detailsmodel->getInvoiceDetailsView($id); 
        $invoiceSKUData = $this->container_detailsmodel->getInvoiceSKUDetails($id); 
        //echo '<pre>'; print_r($invoiceData); die;     
        if($invoiceData){
           $invoiceDetails = '<div class="form-group">
                <label for="">Business Partner Name</label>
                <input type="text"  value="'.$invoiceData[0]['business_name'].'" readonly class="input-form-mro"> 
            </div>
            <div class="form-group">
                <input type="text" value="'.$invoiceData[0]['invoice_file_name'].'" readonly placeholder="Enter data" class="input-form-mro">
                <i class="icon-edit"></i> 
               
            </div>
            <div class="form-group">
                <label for="">Total Invoice Value</label>
                <input type="text" value="'.$invoiceData[0]['invoice_value'].'" readonly placeholder="Enter data" class="input-form-mro">
            </div>
            <div class="form-group">
                <label for="">Invoice Date</label>
                <input type="date" value="'.$invoiceData[0]['invoice_date'].'" readonly placeholder="Enter data" class="input-form-mro">
            </div>
            <div class="form-group">
                <label for="">Invoice Status</label>
                <select class="input-form-mro" readonly> 
                    <option value="">'.$invoiceData[0]['invoice_status'].'</option> 
                </select>
            </div>
            <div class="form-group">
                <label for="">Paid Date</label>
                <input type="date" value="'.$invoiceData[0]['paid_date'].'" readonly placeholder="Enter data" class="input-form-mro">
            </div>';
            if(!empty($invoiceSKUData)){
                foreach ($invoiceSKUData as $value) {
                    $invoiceDetails .= '<div class="form-group">
                                <label for="">SKU</label>
                                <select class="input-form-mro" readonly>
                                    <option value="">'.$value['sku_number'].'</option> 
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Item Code </label>
                                <span>'.$value['sku_number'].'</span>
                            </div>

                            <div class="form-group">
                                <label for="">Product Description</label>
                                <span>'.$value['product_name'].'</span>
                            </div>

                            <div class="form-group">
                                <label for="">Size </label>
                                <span>'.$value['size_name'].'</span>
                            </div> 
                            <div class="form-group">
                                <label for="">Quantity</label>
                                <input type="number" value="'.$value['quantity'].'" readonly placeholder="Enter quantity" class="input-form-mro">
                            </div>

                            <div class="form-group">
                                <label for="">Price Per Unit</label>
                                <input type="number" value="'.$value['price_per_unit'].'" readonly placeholder="Enter price per unit" class="input-form-mro">
                            </div>';
                }
            }                       
           
          $invoicePDF = '<div><iframe src="'.base_url('container_document/').$invoiceData[0]['invoice_file_name'].'" style="width:100%;height:900px;"></iframe></div>';
        }
        echo json_encode(array('invoiceDetails' => $invoiceDetails,'invoicePDF' => $invoicePDF));
        exit;
    }
    
    
    function getDocumentDetails() {
        $container_id = $_GET['container_id'];
        $documentUploadedData = $this->container_detailsmodel->getDocumentUploaded($container_id);
        $documentIsUploadedData = $this->container_detailsmodel->getDocumentIsUploaded($container_id);
        
        $documentUploadedDetails = array(); 
        if(!empty($documentUploadedData)){
            foreach ($documentUploadedData as $value) { 
                $documentUploadedDetails[$value['document_type_id']] = $value;
            }
        }
        
        if(!empty($documentIsUploadedData)){
            foreach ($documentIsUploadedData as $value) { 
                $documentUploadedDetails[$value['document_type_id']] = $value;
            }
        }
        
        $containerSKUData = $this->container_detailsmodel->getContainerSKUData($container_id);
        
        //echo '<pre>'; print_r($documentUploadedDetails); die;
        $cm = "'";
        $str = '';
        $documentStatus = 'Unchecked';
        $str .= '<div class="tab-content-wrapper container-details-doc-tab">';
            if(!empty($documentUploadedDetails)){
                foreach ($documentUploadedDetails as $value) {  
                    
                    $documentStatusData = $this->container_detailsmodel->getDocumentStatus($value['document_type_id'],$container_id); 
                    
                    if(!empty($documentStatusData)){
                        $documentStatus = $documentStatusData[0]['status'];
                        $approvedDate = date('d-M-y', strtotime($documentStatusData[0]['updated_on']));
                        $approvedBy = $documentStatusData[0]['firstname'].' '.$documentStatusData[0]['lastname'];
                    } 
                    $colorClass = "css-grey"; 
                    if($documentStatus == "Unchecked"){
                        $colorClass = "css-yellow";
                    }else{
                        $colorClass = "css-green";
                    }                  
                    
                    $str .= '<div class="cd-doc-row">
                        <div class="cdd-row-left">
                            <div class="doc-flag">
                                <div class="css-flag '.$colorClass.'"></div>
                            </div>
                            <div class="doc-title">
                                <p class="doc-name">'.str_replace(',','<br/>',$value['uploaded_document_number']).'</p>
                                <p class="doc-date">'.date('d-M-y', strtotime($value['updated_on'])).'</p>
                                <div class="cs-status-text">
                                    <p class="last-update-title">Last Updated</p>
                                    <p class="last-update-name">'.$value['firstname'].' '.substr($value['lastname'],0, 1).'</p>
                                    <p class="last-update-date">'.date('d-M-y', strtotime($value['updated_on'])).'</p>
                                </div>
                            </div>
                        </div>
                        <div class="cdd-row-center">
                            <div class="cd-doc-wrapper">
                                <div class="doc-single">
                                    <div class="sit-single">
                                        <p class="sit-single-title">SKU</p>';
                                        if(!empty($containerSKUData)){
                                            foreach ($containerSKUData as $value1) {
                                                $str .=  '<p class="sit-single-value">'.$value1['sku_number'].'</p>';
                                            }
                                        } 
                                        $str .=  '<p class="sit-single-value"><strong>Total</strong></p>
                                    </div>
                                </div>
                                <div class="doc-single">
                                    <div class="sit-single">
                                        <p class="sit-single-title">Quantity</p>';
                                        $qtyTotal = 0;
                                        if(!empty($containerSKUData)){
                                            foreach ($containerSKUData as $value2) {
                                                $str .=  '<p class="sit-single-value">'.$value2['quantity'].'</p>';
                                                $qtyTotal += $value2['quantity'];
                                            }
                                        }  
                                        $str .= '<p class="sit-single-value"><strong>'.$qtyTotal.'</strong></p>
                                    </div>
                                </div>';
                                        
                                $documentsFieldsData = $this->container_detailsmodel->getDocumentsFields($value['document_type_id'],$value['document_number']);   
                                if(!empty($documentsFieldsData)){
                                    foreach ($documentsFieldsData as $value3) {
                                        $str .= '<div class="doc-single">
                                             <div class="sit-single">
                                                 <p class="sit-single-title">'.$value3['custom_field_title'].'</p>
                                                 <p class="sit-single-value">'.$value3['custom_field_structure_value'].'</p>
                                             </div>
                                         </div>';
                                    }
                                }
                               
                           $str .= '</div>
                        </div>
                        <div class="cdd-row-right popup"> 
                            <button class="btn-grey-mro" onclick="showDocumentDetails('.$value['document_type_id'].','.$container_id.','.$cm.$value['document_number'].$cm.','.$cm.$documentStatus.$cm.');" title="Document Approval">View</button> 
                            <a class="btn-grey-mro" onclick="reuploadDocument('.$value['document_type_id'].','.$container_id.','.$cm.$value['document_number'].$cm.','.$cm.$documentStatus.$cm.');" style="cursor: pointer;" title="Reupload Document"><img src="'.base_url().'assets/images/redo.svg" alt=""></a>';
                            if($documentStatus == 'checked'){
                                $str .= '&nbsp<a class="btn-grey-mro" onclick="showApprovedBy('.$value['document_type_id'].');" title="Approved Information"><img class="info" src="'.base_url().'assets/images/info-btn.png" alt=""></a>';
                            }
                        $str .= '<span class="popuptext" id="myPopup'.$value['document_type_id'].'">
                                    <span>'.$approvedBy.'</span></br><span>'.$approvedDate.'</span>
                                </span>
                       </div>
                    </div>';  
                }
            }
            $str .= '</div>';       
        echo json_encode(array('documentDetails' => $str));
        exit;
    }
    
    function getUploadDetailsView(){
        $document_type_id = $_GET['document_type_id']; 
        $container_id = $_GET['container_id']; 
        $document_number = $_GET['document_number'];  
        $status = $_GET['status']; 
        
        $leftsidehtml = "";
        $form_view = ''; 
        $cm = "'";
        
        $result['getSelectedCustomField'] = $this->container_detailsmodel->getDocumentsFields($document_type_id,$document_number); 
        
        $result['containersSKUData'] = $this->getcontainersSKUData($document_type_id,$document_number); 
        
        //echo '<pre>'; print_r($result['getSelectedCustomField']); die;
        $leftsidehtml = $this->load->view("customer-field",$result,true);
        
        $condition = "1=1  AND duf.document_type_id = '".$document_type_id."' AND duf.container_id = ".$container_id." "; 
        if(in_array($document_type_id,array(2,3))){
             $main_table = array("tbl_document_uploaded_files as duf", array("duf.*"));
        } else{
             $main_table = array("tbl_document_ls_uploaded_files as duf", array("duf.*"));
        }   
        $join_tables = array(
            array("left","tbl_sub_document_type as sdt","sdt.document_type_id = duf.document_type_id", array('sdt.sub_document_type_name as document_name')),
        ); 
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,"",'document_type_id,sub_document_type_id');
        
        
        $result1['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
        //echo '<pre>'; print_r($result1['formView']); die;
        
        $form_view = $this->load->view("document-form-view",$result1,true);
        
        $savehtml = '';
        
        if($this->privilegeduser->hasPrivilege("DocumentAddEdit")){ 
            $savehtml .= '<span class="btn-secondary-mro update-record" onclick="showSaveButton();"><img src="'.base_url().'assets/images/update-icon.svg" alt=""> Update</span>  
                    <button class="btn-secondary-mro save-record" style="display: none;">Save</button>';
        }                             
        if($this->privilegeduser->hasPrivilege("DocumentApproval")){
            if($status =='Unchecked'){
                $savehtml .= '<span class="btn-primary-mro checkedClass"><span onclick="documentApprove('.$document_type_id.','.$container_id.','.$cm.$document_number.$cm.');"><img src="'.base_url().'assets/images/check.png" alt=""> Check</span></span> ';
            }else{
                $savehtml .= ' <span class="btn-primary-mro checkedClass" style="cursor: auto;">Checked</span>';
            }
        }
                                            
        echo json_encode(array('custom_field' => $leftsidehtml,'form_view' => $form_view,'savehtml' =>$savehtml));
        exit;
    }
    
    function reuploadDocument(){        
        $document_type_id = $_GET['document_type_id']; 
        $container_id = $_GET['container_id']; 
        $document_number = $_GET['document_number'];  
        $status = $_GET['status']; 
        
        $leftsidehtml = "";
        $form_view = ''; 
        $cm = "'";
        
        $result['getSelectedCustomField'] = $this->container_detailsmodel->getDocumentsFields($document_type_id,$document_number); 
        
        $result['containersSKUData'] = $this->getcontainersSKUData($document_type_id,$document_number); 
        
        //echo '<pre>'; print_r($result['getSelectedCustomField']); die;
        $leftsidehtml = $this->load->view("customer-field-reupload",$result,true);               
    
        $condition = "1=1  AND d.document_type_id = '".$document_type_id."' ";
        $main_table = array("tbl_document_type as d", array('d.document_type_name'));
        $join_tables = array(
            array("left","tbl_sub_document_type as sd","sd.document_type_id = d.document_type_id", array("sd.sub_document_type_name,sd.sub_document_type_id")),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,"",'');
        $getSubDocument = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
        
        $result1['getSubDocument'] = $getSubDocument;  
        $result1['document_type_id'] = $document_type_id; 
        $result1['container_id'] = $container_id;                                        
        //echo '<pre>'; print_r($result1['getSubDocument']); die;
        
        $form_view = $this->load->view("document-reupload-form",$result1,true);
        
        $savehtml = '';
        
        if($this->privilegeduser->hasPrivilege("DocumentAddEdit")){ 
            $savehtml .= '<button class="btn-primary-mro">Save</button>';
        }                                   
        echo json_encode(array('custom_field' => $leftsidehtml,'form_view' => $form_view,'savehtml' =>$savehtml));
        exit;
    }
    
    function documentApprove(){
        $document_type_id = $_GET['document_type_id']; 
        $container_id = $_GET['container_id'];
        $document_number = $_GET['document_number']; 
        
        $table = "tbl_document_ls_uploaded_files";
        if($document_type_id =='2' || $document_type_id == '3'){
            $table = "tbl_document_uploaded_files";
        } 
        $containerIds = $this->common->getData($table, "container_id", array("uploaded_document_number" => $document_number));        
        if(!empty($containerIds)){
            foreach ($containerIds as $value) { 
                $data['status'] = 'Checked'; 
                $data['updated_on'] = date("Y-m-d H:i:s");
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $condition = " container_id = ".$value['container_id']." and document_type_id = ".$document_type_id." "; 
                $result = $this->common->updateData("tbl_container_uploaded_document_status", $data, $condition);                        
            }
        }
        
        if($result){
            echo json_encode(array("success" => true, "msg" => "Document Approved! "));
            exit;
        }else{
            echo json_encode(array("success" => false, "msg" => "Not Approved!"));
            exit;
        }
    } 
    
    public function submitFormStep5(){
        //echo "<pre>"; print_r($_POST); die;   
        //document update code
        if(!empty($_POST['custom_field_structure_id']) && isset($_POST['custom_field_structure_id'])){ 
            $uploaded_document_number = explode(',',$_POST['uploaded_document_number']);
            if(!empty($_POST['sub_document_type_id']) && isset($_POST['sub_document_type_id'])){
                $sub_document_type_cnt = explode(',',$_POST['sub_document_type_id']);
                foreach ($sub_document_type_cnt as $subDocumentkey => $subDocumentval) {
                    foreach ($_POST['custom_field_structure_id'] as $key => $value) {
                        $data['custom_field_structure_id'] = $value;
                        $data['custom_field_structure_value'] =  $_POST[$value];
                        $data['uploaded_document_number'] =  $uploaded_document_number[$subDocumentkey];
                        $data['document_type_id'] =   (!empty($_POST['document_type_id'])?$_POST['document_type_id']:" ");
                        $data['sub_document_type_id'] =  $subDocumentval;
                        $data['current_session_id'] = isset($_SESSION['mro_session'][0]['current_session_id'])?$_SESSION['mro_session'][0]['current_session_id']:'';
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['created_on'] = date("Y-m-d H:i:s"); 
                        $condition = " custom_field_structure_id = ".$value." and document_type_id = ".$_POST['document_type_id']." "; 
                        $result = $this->common->updateData("tbl_custom_field_data_submission", $data, $condition); 
                    }
                }
            }else{
                foreach ($_POST['custom_field_structure_id'] as $key => $value) {
                    $data['custom_field_structure_id'] = $value;
                    $data['uploaded_document_number'] =  $uploaded_document_number[0];
                    $data['custom_field_structure_value'] =  $_POST[$value];
                    $data['document_type_id'] =   (!empty($_POST['document_type_id'])?$_POST['document_type_id']:" ");
                    $data['sub_document_type_id'] =  (!empty($_POST['sub_document_type_id'])?$_POST['sub_document_type_id']:"0");
                    $data['current_session_id'] = isset($_SESSION['mro_session'][0]['current_session_id'])?$_SESSION['mro_session'][0]['current_session_id']:'';
                    $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['updated_on'] = date("Y-m-d H:i:s");
                    $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['created_on'] = date("Y-m-d H:i:s"); 
                    $condition = " custom_field_structure_id = ".$value." and document_type_id = ".$_POST['document_type_id']." "; 
                    $result = $this->common->updateData("tbl_custom_field_data_submission", $data, $condition);
                }
            }
            
            if(!empty($_POST['container_sku_id']) && isset($_POST['container_sku_id'])){
                foreach ($_POST['container_sku_id'] as $key => $value) { 
                    $data1['quantity'] = $_POST['container_sku_qty'][$key];  
                    $condition = " container_sku_id = ".$key." ";
                    $result1 = $this->common->updateData("tbl_container_sku", $data1, $condition);
                }
            } 

            if($result){
                echo json_encode(array("success" => true, "msg" => "Custom field data updated successfully ! " ,"type"=>"document"));
                exit;
            }else{
                echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                exit;
            }
        }else{
            //document reupload code
            if(!empty($_POST['container_id']) && isset($_POST['container_id']) && !empty($_POST['document_type_id']) && isset($_POST['document_type_id'])){ 
                $this->load->library('upload');
                $result = array();
                // this is for fr n ls document type 
                if($_POST['document_type_id'] =='2' || $_POST['document_type_id'] =='3'){
                    if(isset($_FILES) && isset($_FILES["step3_document"]["name"])){                        
                        $this->upload->initialize($this->set_upload_options());
                        if (!$this->upload->do_upload("step3_document")){
                            $image_error = array('error' => $this->upload->display_errors());
                            echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
                            exit;
                        }else{
                            $image_data = array('upload_data' => $this->upload->data());
                            $step3_document = $image_data['upload_data']['file_name']; 
                            $data['uploaded_document_file'] = $step3_document;
                            $upload = true;
                        }	
                    }  
                    
                    if($upload){                     
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $condition = " container_id = ".$_POST['container_id']." AND document_type_id = ".$_POST['document_type_id']." ";
                        $result = $this->common->updateData("tbl_document_uploaded_files", $data, $condition);
                    }
                    
                }else{
                    $files = $_FILES;
                    foreach($files['step3_document']['name'] as $key=>$value){
                        $upload = false;
                        if(!empty($files['step3_document']['name'][$key]) && isset($files['step3_document']['name'][$key])){

                            $_FILES['step3_document']['name']= $files['step3_document']['name'][$key];
                            $_FILES['step3_document']['type']= $files['step3_document']['type'][$key];
                            $_FILES['step3_document']['tmp_name']= $files['step3_document']['tmp_name'][$key];
                            $_FILES['step3_document']['error']= $files['step3_document']['error'][$key];
                            $_FILES['step3_document']['size']= $files['step3_document']['size'][$key];    
                                
                            $this->upload->initialize($this->set_upload_options());
                            if (!$this->upload->do_upload("step3_document")){
                                $image_error = array('error' => $this->upload->display_errors());
                                echo json_encode(array("success"=>false, "msg"=>$image_error['error']));
                                exit;
                            }else{
                                $image_data = array('upload_data' => $this->upload->data());
                                $step3_document = $image_data['upload_data']['file_name']; 
                                if($key == '0'){
                                    $uploaded_document_type_id['document_type_id'] = $key;
                                    $data['uploaded_document_file']  = $step3_document;
                                }else{
                                    $uploaded_document_file_name['file_name'][$key] = $step3_document;
                                    $uploaded_document_type_id['sub_document_type_id'][$key] = $key;
                                }
                                $upload = true;
                            }	
                        }
                    } 
                    
                    if($upload){ 
                        
                        foreach($files['step3_document']['name'] as $key=>$value){                            
                            if($key == 0){ 
                                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['updated_on'] = date("Y-m-d H:i:s");
                                $condition = " container_id = ".$_POST['container_id']." AND document_type_id = ".$_POST['document_type_id']." ";
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $data, $condition);                               
                            }else{     
                                $data['uploaded_document_file'] = $uploaded_document_file_name['file_name'][$key];                                
                                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['updated_on'] = date("Y-m-d H:i:s");
                                $condition = " container_id = ".$_POST['container_id']." AND document_type_id = ".$_POST['document_type_id']." AND sub_document_type_id = ".$uploaded_document_type_id['sub_document_type_id'][$key]." ";
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $data, $condition);
                            }                             
                        }             
                    } 
                } 
                                        
                if($result){
                    echo json_encode(array("success" => true, "msg" => "Reupload document successfully ! " ,"type"=>"reupload"));
                    exit;
                }else{
                    echo json_encode(array("success" => false, "msg" => "Something wrong!","type"=>""));
                    exit;
                }
            }            
        }
    }
    
    
    function getcontainersSKUData($document_type_id=0,$document_number=''){ 
        if($document_type_id == '2' || $document_type_id == '3'){
            $main_table = array("tbl_document_uploaded_files as dluf", array("dluf.document_type_id"));            
        }else{
            $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.document_type_id"));//dluf.container_id,
        } 
        $condition = "1=1  AND dluf.uploaded_document_number = '".$document_number."' ";
        
        $join_tables = array(
            array("","tbl_container_sku as csku","csku.container_id = dluf.container_id", array('csku.*')),
            array("","tbl_sku as sku","sku.sku_id = csku.sku_id", array('sku.sku_number')),
            array("","tbl_container as c","c.container_id = dluf.container_id", array('c.container_number')),
            array("","tbl_order as o","o.order_id = c.order_id", array()),
            array("","tbl_bp_order as bpo","bpo.bp_order_id = o.supplier_contract_id", array()),
            array("","tbl_business_partner as bp","bp.business_partner_id = bpo.business_partner_id",array('bp.business_name')),
        );                           
        
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition); //order by dluf.container_id DESC
        $containerData = $this->common->MySqlFetchRow($rs, "array");          
        //echo $this->db->last_query();exit;
        $resultArr = array();
        foreach ($containerData as $value) {
            if($value['document_type_id'] == '2' || $value['document_type_id'] == '3'){
                $resultArr[$value['container_id']][$value['container_sku_id']] = $value;
            }        
        }
        krsort($resultArr);           
        return $resultArr; 
    }
    
    
    public function set_upload_options($file_name = ""){
        $config = array();
        if(!empty($file_name) &&  $file_name != ""){
                $config['file_name']   = $file_name.time();
        }
        $config['upload_path'] = DOC_ROOT_FRONT."/container_document";
        if(!is_dir($config['upload_path'])){
                mkdir($config['upload_path'],0777, true);		
        }
        $config['allowed_types'] = 'pdf';
        $config['max_size']      = '0';
        $config['overwrite']     = FALSE;
        // $config['min_width']            = 1000;
        // $config['min_height']           = 1000;
        return $config;
    } 
    

}

?>
