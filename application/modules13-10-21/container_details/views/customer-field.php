  
  <?php
    if($getSelectedCustomField){ 
        
        foreach ($getSelectedCustomField as $key => $value) { ?>
          <?php if ($value['custom_field_type'] == 'date' && $value['custom_field_title'] == 'Date of Document') { ?>
              <div class="form-group ">
                  <label for=""><?= $value['custom_field_title'] ?></label>
                  <input type="date" name="<?= $value['custom_field_structure_id'] ?>" value="<?= $value['custom_field_structure_value']?>" required id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" readonly placeholder="Enter date"  class="input-form-mro readonly">
              </div>
          <?php  } ?>
        <?php } ?>

        <!--start container sku table-->
        <?php if (!empty($containersSKUData)) { ?>
            <div class="bp-list-wrapper">
                <table class="table table-striped document-sku-table no-footer" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Container</th>
                            <th>SKU</th>
                            <th>Quantity</th>
                            <th>Supplier Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                          foreach ($containersSKUData as $containerId => $containerSkuData) {
                              $containerNumberArr = array();
                              $businessNameArr = array();
                              foreach ($containerSkuData as $containerSkuId => $value) {
                          ?>
                                <tr>
                                    <td>
                                        <?php
                                          if (!in_array($value['container_number'], $containerNumberArr)) {
                                              echo $value['container_number'];
                                          }
                                          ?>
                                    </td>
                                    <td><?= $value['sku_number'] ?>
                                        <input type="hidden" name="container_sku_id[<?= $containerSkuId ?>]" id="container_sku_id<?= $containerSkuId ?>" value="<?= (!empty($value['container_sku_id'])) ? $value['container_sku_id'] : '0' ?>">
                                    </td>
                                    <td>
                                        <input type="number" name="container_sku_qty[<?= $containerSkuId ?>]" id="container_sku_qty<?= $containerSkuId ?>" value="<?= (!empty($value['quantity'])) ? $value['quantity'] : '0' ?>" class="input-form-mro readonly" required placeholder="Enter quantity" readonly>
                                    </td>
                                    <td>
                                        <?php
                                          if (!in_array($value['business_name'], $businessNameArr)) {
                                              echo $value['business_name'];
                                          }
                                          ?>
                                    </td>
                                </tr>
                            <?php
                                  $containerNumberArr[] = $value['container_number'];
                                  $businessNameArr[] = $value['business_name'];
                              } ?>
                            <!-- <tr style="height: 30px;">
                                <td colspan="4"></td>
                            </tr> -->
                        <?php

                          } ?>
                    </tbody>
                </table>
            </div>
        <?php } ?>
        <!--end container sku table--> 
        
        <?php 
        foreach ($getSelectedCustomField as $key => $value) { ?>
            <input type="hidden" name="custom_field_structure_id[]" id="custom_field_structure_id" value="<?= $value['custom_field_structure_id']?>">
            <input type="hidden" name="document_type_id" id="document_type_id" value="<?= $value['document_type_id']?>">
            <input type="hidden" name="sub_document_type_id" id="sub_document_type_id" value="<?= $value['sub_document_type_id']?>">
            <input type="hidden" name="uploaded_document_number" id="uploaded_document_number" value="<?= $value['uploaded_document_number']?>">

            <?php if($value['custom_field_type'] == 'date' && $value['custom_field_title'] != 'Date of Document'){ ?>
                <div class="form-group ">
                    <label for=""><?= $value['custom_field_title']?></label>
                    
                    <input type="date" name="<?= $value['custom_field_structure_id']?>" value="<?= $value['custom_field_structure_value']?>" required id="<?= str_replace(' ', '_',trim($value['custom_field_structure_id']));?>" placeholder="Enter date" class="input-form-mro readonly" readonly>
                </div>
            <?php  } elseif ($value['custom_field_type'] == 'text') { ?>
                <div class="form-group ">
                    <label for=""><?= $value['custom_field_title']?></label>
                    <input type="text" name="<?= str_replace(' ', '_',trim($value['custom_field_structure_id']));?>" value="<?= $value['custom_field_structure_value']?>"  required id="<?= str_replace(' ', '_',trim($value['custom_field_structure_id']));?>"  placeholder="Enter data" class="input-form-mro readonly" readonly>
                </div>
            <?php } elseif ($value['custom_field_type'] == 'dropdown') { ?>

                <div class="form-group">
                    <div class="form-group">
                        <label for=""><?= $value['custom_field_title']?></label>
                        <div class="multiselect-dropdown-wrapper">
                        <div class="md-value">
                            Select value
                        </div>
                        <div class="md-list-wrapper">
                            <input type="text" placeholder="Search" class="md-search">
                            <div class="md-list-items">
                            <?php if($ListOfContainerData){
                                foreach ($ListOfContainerData as $key => $value) { ?>
                                <div class="mdli-single ud-list-single">
                                    <label class="container-checkbox"> <?= $value['containerData']['container_number'] ?>
                                        <input type="checkbox"  id="<?= $value['custom_field_structure_id']; ?>"  value="<?= $value['custom_field_structure_value']?>"  name="<?= $value['custom_field_structure_id']?>">
                                        <span class="checkmark-checkbox"></span>
                                    </label>
                                </div>
                                <?php
                            } }
                            ?> 
                            </div> 
                        </div>
                        </div>
                    </div>
                </div>
                
            <?php }elseif ($value['custom_field_title'] == 'number'){ ?>
                
            <?php }?>
        <?php }
    }?> 
        
         