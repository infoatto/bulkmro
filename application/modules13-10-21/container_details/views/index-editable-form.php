<form action="" id="editableForm" method="post" enctype="multipart/form-data">
<div class="title-sec-wrapper">
    <div class="title-sec-left">         
        <div class="page-title-wrapper">  
            <div class="form-group">
                <label for="">Container Number<sup>*</sup></label>
                <input type="text" name="container_number" id="container_number" value="<?php echo(!empty($container_details['container_number'])) ? $container_details['container_number'] : ""; ?>" placeholder="Enter Container Number" class="input-form-mro">
                <input type="hidden" name="container_id" id="container_id" value="<?php echo (!empty($container_details['container_id'])) ? $container_details['container_id'] : ""; ?>">
                <input type="hidden" name="old_container_status_id" id="old_container_status_id" value="<?php echo (!empty($containerStatusId)) ? $containerStatusId : "0"; ?>">
            </div> 
            <div class="nav-divider"></div>
            <div>
                <p class="container-status"> 
                    <select name="container_status_id" id="container_status_id" class="basic-single select-form-mro">
                        <option value="">Select Container Status</option>
                        <?php
                        foreach ($containerStatusData as $value) { 
                            ?> 
                            <option value="<?php echo $value['container_status_id']; ?>" <?php
                            if ($value['container_status_id'] == $containerStatusId) {
                                echo "selected";
                            }
                            ?>>
                            <?php echo $value['container_status_name']; ?> 
                            </option> 
                            <?php
                        }
                        ?>
                    </select> 
                </p>
            </div> 
           
        </div>
    </div> 
    <div class="title-sec-right"> 
         <?php
        if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) {
            ?>
            <button type="submit" class="btn-primary-mro">Save</button>
        <?php }
        ?>
    </div>
</div>
<div class="shipping-info-top">
    <h2 class="sec-title">Shipping Information</h2>
    <hr class="separator">
    <div class="sit-wrapper">
        <div class="sit-row">
            <div class="sit-single">
                <p class="sit-single-title">Freight Forwarder</p>
                <p class="sit-single-value"> 
                    <select name="freight_forwarder_id" id="freight_forwarder_id" class="basic-single select-form-mro">
                        <option value="">Select Freight Forwarder</option>
                        <?php
                        foreach ($freightForwarderData as $value) { 
                            ?> 
                            <option value="<?php echo $value['business_partner_id']; ?>" <?php
                            if ($value['business_partner_id'] == $freightForwarderId) {
                                echo "selected";
                            }
                            ?>>
                            <?php echo $value['business_name']; ?> 
                            </option> 
                            <?php
                        }
                        ?>
                    </select> 
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">CHA</p>
                <p class="sit-single-value">
                    <select name="broker_id" id="broker_id" class="basic-single select-form-mro">
                        <option value="">Select CHA</option>
                        <?php
                        foreach ($CHAData as $value) { 
                            ?> 
                            <option value="<?php echo $value['business_partner_id']; ?>" <?php
                            if ($value['business_partner_id'] == $CHA_id) {
                                echo "selected";
                            }
                            ?>>
                            <?php echo $value['business_name']; ?> 
                            </option> 
                            <?php
                        }
                        ?>
                    </select> 
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">POL</p>
                <p class="sit-single-value"> 
                    <input type="text" name="pol" id="pol" value="<?php echo(!empty($container_details['pol'])) ? $container_details['pol'] : ""; ?>" placeholder="Enter POL" class="input-form-mro">
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">POD</p>
                <p class="sit-single-value">
                    <input type="text" name="pod" id="pod" value="<?php echo(!empty($container_details['pod'])) ? $container_details['pod'] : ""; ?>" placeholder="Enter POD" class="input-form-mro">                    
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Liner Name</p>
                <p class="sit-single-value">
                    <input type="text" name="liner_name" id="liner_name" value="<?php echo(!empty($container_details['liner_name'])) ? $container_details['liner_name'] : ""; ?>" placeholder="Enter Liner Name" class="input-form-mro">                    
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Vessel Name</p>
                <p class="sit-single-value">
                    <input type="text" name="vessel_name" id="vessel_name" value="<?php echo(!empty($container_details['vessel_name'])) ? $container_details['vessel_name'] : ""; ?>" placeholder="Enter Vessel Name" class="input-form-mro">  
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Flag</p>
                <p class="sit-single-value">
                    <input type="text" name="flag" id="flag" value="<?php echo(!empty($container_details['flag'])) ? $container_details['flag'] : ""; ?>" placeholder="Enter Flag" class="input-form-mro">
                </p>
            </div>
        </div>
        <div class="sit-row">
            <div class="sit-single">
                <p class="sit-single-title">ETA</p>
                <p class="sit-single-value mt20"><?= date('d-M-y', strtotime($container_details['eta'])); ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">ETD</p>
                <p class="sit-single-value mt20"><?= date('d-M-y', strtotime($container_details['etd'])); ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Revised ETA</p>
                <p class="sit-single-value">
                    <input type="date" name="revised_eta" id="revised_eta" value="<?php echo(!empty($container_details['revised_eta'])) ? date('Y-m-d', strtotime($container_details['revised_eta'])) : date("m/d/Y"); ?>" onchange="fillETT();" class="input-form-mro  valid" aria-invalid="false">                     
                </p>
            </div>            
            <div class="sit-single">
                <p class="sit-single-title">Revised ETD</p>
                <p class="sit-single-value"> 
                    <input type="date" name="revised_etd" id="revised_etd" value="<?php echo(!empty($container_details['revised_etd'])) ? date('Y-m-d', strtotime($container_details['revised_etd'])) : date("m/d/Y"); ?>" onchange="fillETT();" class="input-form-mro  valid" aria-invalid="false">                 
                </p>
            </div>            
            <div class="sit-single">
                <p class="sit-single-title">ETT In Days</p>
                <p class="sit-single-value"> <input type="text" readonly name="ett" id="ett" value="<?php echo(!empty($container_details['ett'])) ? $container_details['ett'] : ""; ?>" placeholder="No ETA or ETD Entered" class="input-form-mro"></p>                    
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Revised ETT In Days</p>
                <p class="sit-single-value"><input type="text" readonly name="revised_ett" id="revised_ett" value="<?php echo(!empty($container_details['revised_ett'])) ? $container_details['revised_ett'] : ""; ?>" placeholder="No ETA or ETD Entered" class="input-form-mro"></p>
            </div>
        </div>
        <div class="sit-row">
            <div class="sit-single">
                <p class="sit-single-title">Customer Alias</p>
                <p class="sit-single-value mt20"><?= (!empty($customerAlias)) ? $customerAlias : "";?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Vendor Alias</p>
                <p class="sit-single-value mt20"><?= (!empty($supplierAlias)) ? $supplierAlias : "";?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Shipping Container</p>
                <p class="sit-single-value mt20"><?= (!empty($fcr['44'])) ? $fcr['44'] : "Waiting for FCR"; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Shipping Container Seal</p>
                <p class="sit-single-value mt20"><?= (!empty($fcr['46'])) ? $fcr['46'] : "Waiting for FCR"; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">FF Seal</p>
                <p class="sit-single-value mt20"><?= (!empty($fcr['45'])) ? $fcr['45'] : "Waiting for FCR"; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Email Sent On</p>
                <p class="sit-single-value">
                    <input type="date" name="email_sent_on" id="email_sent_on" value="<?php echo(!empty($container_details['email_sent_on'])) ? date('Y-m-d', strtotime($container_details['email_sent_on'])) : date("m/d/Y"); ?>" class="input-form-mro valid" aria-invalid="false">                 
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Email Subject</p>
                <p class="sit-single-value">
                    <input type="text" name="email_subject" id="email_subject" value="<?php echo(!empty($container_details['email_subject'])) ? $container_details['email_subject'] : ""; ?>" placeholder="Enter Flag" class="input-form-mro">                   
                </p>
            </div>
        </div>
    </div>
</div>
</form>
<script> 
    $(document).ready(function () {  
        var vRules = {
            "container_number": {required: true} 
        };
        var vMessages = {
            "container_number": {required: "Please enter container number."} 
        }; 
        $("#editableForm").validate({
            rules: vRules,
            messages: vMessages,
            errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function(error, e) {
              e.parents('.form-group').append(error);
            },
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>container_details/editableSubmitForm";
                $("#editableForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) {
                             setTimeout(function () {                                
                                $('#container-editable-div').html(response.html);  
                            }, 2000); 
                        }else{
                            $(".btn-primary-mro").show();
                        } 
                    }
                });
            }
        });
        
    }); 
    
    
    $('.basic-single').select2({
        searchInputPlaceholder: 'Search'
    });
    
     function fillETT() {
        var revised_eta = $('#revised_eta').val();
        var revised_etd = $('#revised_etd').val();
        var act = "<?php echo base_url(); ?>container/getETTDays";
        $.ajax({
            type: 'GET',
            url: act,
            data: {revised_eta: revised_eta, revised_etd: revised_etd},
            dataType: "json",
            success: function (data) {
                $('#ett').val(data.days);
                $('#revised_ett').val(data.days);
            }
        });
    } 
    
//    $("#editableForm").submit(function( event ) {  
//        alert();
//        var act = "<?php echo base_url(); ?>container_details/editableSubmitForm";
//        $("#editableForm").ajaxSubmit({
//            url: act,
//            type: 'post',
//            dataType: 'json',
//            cache: false,
//            clearForm: false,
//            beforeSubmit: function (arr, $form, options) {
//                $(".btn-primary-mro").hide();
//            },
//            success: function (response) { 
//                showInsertUpdateMessage(response.msg,response);                        
//                if (response.success) {                             
//                    $('#container-editable-div').html(response.html);  
//                }   
//            }
//        });
//    });   
</script>