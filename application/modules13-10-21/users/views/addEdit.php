<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("users"); ?>">Users</a>
                                <span>></span>
                                <p>Add User</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add User</h1>
                            </div>
                        </div>          
                        <button class="btn-primary-mro">Save</button>
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="user_id" id="user_id" value="<?php echo(!empty($user_details['user_id'])) ? $user_details['user_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="Role Name">User Role</label>
                                    <select name="role_id" id="role_id" class="select-form-mro">
                                        <option value="">Select Role</option>
                                        <?php if(!empty($roles_details)){  ?>
                                            <?php foreach($roles_details as $key=>$val){ ?>
                                                <option value="<?php echo $val['role_id'];?>" <?php echo(!empty($user_details['role_id']) && $user_details['role_id'] == $val['role_id'])?"selected":"";?>><?php echo $val['role_name']; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="First Name">First Name</label>
                                    <input type="text" name="firstname" id="firstname" class="input-form-mro" value="<?php echo(!empty($user_details['firstname'])) ? $user_details['firstname'] : ""; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="Last Name">Last Name</label>
                                    <input type="text" name="lastname" id="lastname" class="input-form-mro" value="<?php echo(!empty($user_details['lastname'])) ? $user_details['lastname'] : ""; ?>">
                                </div> 
                            </div>
                            <div class="form-row form-row-3"> 
                                <div class="form-group">
                                    <label for="Email Id">Email Id</label>
                                    <input type="text" name="email_id" id="email_id" class="input-form-mro" value="<?php echo(!empty($user_details['email_id'])) ? $user_details['email_id'] : ""; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="Mobile Number">Mobile Number</label>
                                    <input type="text" name="mobile_number" id="mobile_number" class="input-form-mro" value="<?php echo(!empty($user_details['mobile_number'])) ? $user_details['mobile_number'] : ""; ?>">
                                </div>
                                <div class="form-group">
                                    <label for="Gender">Gender</label>
                                    <select name="gender" id="gender" class="select-form-mro">
                                        <option value="Male" <?php echo(!empty($user_details['gender']) && $user_details['gender'] == "Male") ? "selected" : ""; ?>>Male</option>
                                        <option value="Female" <?php echo(!empty($user_details['gender']) && $user_details['gender'] == "Female") ? "selected" : ""; ?>>Female</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row form-row-3">
                                <div class="form-group datepicker">
                                    <label for="Date of Birth">Date of Birth (mm/dd/yyyy)</label>
                                    <input type="date" name="dob" id="dob" class="input-form-mro " value="<?php echo(!empty($user_details['dob'])) ? $user_details['dob'] : date("m/d/Y"); ?>">
                                </div>
                                <div class="form-group">
                                    <label for="Status">Status</label>
                                    <select name="status" id="status" class="select-form-mro">
                                        <option value="Active" <?php echo(!empty($user_details['status']) && $user_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                        <option value="In-active" <?php echo(!empty($user_details['status']) && $user_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="Profile Picture">Profile Picture</label>
                                    <input type="file" name="profile_picture" id="profile_picture" class="input-form-mro" >
                                    <input type="hidden" name="existing_profile_image" id="existing_profile_image" value="<?php echo(!empty($user_details['profile_picture'])) ? $user_details['profile_picture'] : ""; ?>">
                                </div>
                            </div>

                            <!-- user credentials wrapper -->
                            <?php if (empty($user_details['user_id'])) { ?>
                                <div class="form-row form-row-3"> 
                                    <div class="form-group">
                                        <label for="Username">Username</label>
                                        <input type="text" name="user_name" id="user_name" class="input-form-mro" value="<?php echo(!empty($user_details['user_name'])) ? $user_details['user_name'] : ""; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="Password">Password</label>
                                        <input type="password" name="password" id="password" class="input-form-mro" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>                                         
                                    </div>
                                    <div class="form-group">
                                        <label for="Confirm Password">Confirm Password</label>
                                        <input type="password" name="confirm_password" id="confirm_password" class="input-form-mro">
                                    </div>
                                </div>
                            <?php } else { ?>                           
                                <div class="form-row form-row-3">
                                    <div class="form-group">
                                        <label for="Username">Username</label>
                                        <input type="text" name="user_name" id="user_name" class="input-form-mro" value="<?php echo(!empty($user_details['user_name'])) ? $user_details['user_name'] : ""; ?>">
                                    </div>
                                    <div class="form-group">
                                        <label for="Password">Password</label>
                                        <input type="password" name="password" id="password" class="input-form-mro" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" required>                                                 
                                    </div>
                                    <div class="form-group">
                                        <label for="Confirm Password">Confirm Password</label>
                                        <input type="password" name="confirm_password" id="confirm_password" class="input-form-mro">
                                    </div> 
                                </div>
                            <?php } ?>   
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section> 

<script>

    $(document).ready(function () {

        var vRules = {
            "role_id": {required: true},
            "firstname": {required: true},
            "lastname": {required: true},
            "email_id": {required: true, email: true},
            "mobile_number": {required: true},
            "gender": {required: true},
            "dob": {required: true},
            "user_name": {required: true},
            "confirm_password": {required: true, equalTo: "#password"}
        };
        var vMessages = {
            "role_id": {required: "Please Select User Role."},
            "firstname": {required: "Please enter First Name."},
            "lastname": {required: "Please enter Last Name."},
            "email_id": {required: "Please enter Email Id.", email: "Please enter valid Email Id."},
            "mobile_number": {required: "Please enter Mobile Number."},
            "gender": {required: "Please Select Gender."},
            "dob": {required: "Please select Date of Birth."},
            "user_name": {required: "Please enter Username."},
            "confirm_password": {required: "Please enter Confirm Password", equalTo: "Password and Confirm Password should same."}
        };
        //check and save user data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>users/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('users') ?>";
                            }, 3000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });
</script>