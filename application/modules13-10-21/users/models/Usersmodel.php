<?PHP

class Usersmodel extends CI_Model {

    function getRecords($get) {
        $table = "tbl_users";
        $table_id = 'u.user_id';
        $default_sort_column = 'u.user_id';
        $default_sort_order = 'desc';
        $condition = "1=1 "; //AND u.user_id !='".$_SESSION["mro_session"][0]["user_id"]."'
        $colArray = array('u.firstname', 'u.lastname', 'r.role_name', 'u.email_id', 'u.mobile_number', 'u.gender', 'u.status');
        $sortArray = array('u.firstname', 'u.lastname', 'r.role_name', 'u.email_id', 'u.mobile_number', 'u.profile_picture', 'u.gender', 'u.status');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

        for ($i = 0; $i < count($colArray); $i++) {
            if (isset($_GET['Searchkey_' . $i]) && $_GET['Searchkey_' . $i] != "") {
                $condition .= " AND " . $colArray[$i] . " LIKE '%" . $_GET['Searchkey_' . $i] . "%' ";
            }
        } 

        $this->db->select('u.*,r.role_name');
        $this->db->from("$table as u"); 
        $this->db->join("tbl_roles as r", "r.role_id = u.role_id");
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page); 
        $query = $this->db->get();
        //echo $this->db->last_query();exit;


        $this->db->select('u.*,r.role_name');
        $this->db->from("$table as u"); 
        $this->db->join("tbl_roles as r", "r.role_id = u.role_id");
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order); 
        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    }

}

?>
