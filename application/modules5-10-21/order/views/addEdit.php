<?php
//start code of view option    
$dropDownCss = '';
$inputFieldCss = '';
if($view==1){
    $dropDownCss = "disabled";
    $inputFieldCss = "readonly";
}
//end code of view option
?>
<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("order"); ?>">Master Contract</a>
                                <span>></span>
                                <p>New Master Contract</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Master Contract</h1>
                            </div>
                        </div>
                        <?php if($view==0){ ?>
                             <button type="submit" class="btn-primary-mro">Save</button>
                        <?php } ?>
                    </div>
                    <div class="page-content-wrapper master-order-wrapper">
                        <input type="hidden" name="order_id" id="order_id" value="<?php echo(!empty($order_details['order_id'])) ? $order_details['order_id'] : ""; ?>">
                        <div class="scroll-content-wrapper master-order">
                            <div class="scroll-content-left master-order-left" id="contract-details"> 
                                <h3 class="form-group-title">Order Details</h3>
                                <div class="form-row form-row-3">
                                    <div class="form-group">
                                        <label for="">Customer Contract Number<sup>*</sup></label>
                                        <select name="customer_contract_id" id="customer_contract_id" class="basic-single select-form-mro" onchange="getBillingShippingDetails(this.value,'customer')" <?=$dropDownCss;?>> 
                                            <option value="">Select Supplier Contract</option>
                                            <?php
                                            foreach ($customerDetails as $ccValue) {
                                                $ccID = (isset($order_details['customer_contract_id']) ? $order_details['customer_contract_id'] : 0);
                                                ?> 
                                                <option value="<?php echo $ccValue['bp_order_id']; ?>"  <?php
                                                if ($ccValue['bp_order_id'] == $ccID) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                            <?php echo $ccValue['contract_number']; ?> 
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select> 
                                    </div> 
                                    <div class="form-group">
                                        <label for="">Supplier Contract Number <sup>*</sup></label>
                                        <select name="supplier_contract_id" id="supplier_contract_id" class="basic-single select-form-mro" onchange="getBillingShippingDetails(this.value,'supplier')" <?=$dropDownCss;?>> 
                                            <option value="">Select Supplier Contract</option>
                                            <?php
                                            foreach ($supplierDetails as $bpValue) {
                                                $bpID = (isset($order_details['supplier_contract_id']) ? $order_details['supplier_contract_id'] : 0);
                                                ?> 
                                                <option value="<?php echo $bpValue['bp_order_id']; ?>"  <?php
                                                if ($bpValue['bp_order_id'] == $bpID) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                            <?php echo $bpValue['contract_number']; ?> 
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="form-group input-edit">
                                        <label for="">Master Contract Number <sup>*</sup></label>
                                        <input type="text" name="contract_number" id="contract_number" value="<?php echo(!empty($order_details['contract_number'])) ? $order_details['contract_number'] : ""; ?>" placeholder="Customer-Supplier Contract #" class="input-form-mro" <?=$inputFieldCss;?>>                                        
                                    </div>
                                </div>
                                <div class="form-row form-row-3">
                                    <div class="form-group">
                                        <label for="">CHA</label> 
                                        <select name="broker_id" id="broker_id" class="basic-single select-form-mro" <?=$dropDownCss;?>> 
                                            <option value="0">Select CHA</option>
                                            <?php
                                            foreach ($chaDetails as $value) {
                                                $bpID = (isset($order_details['broker_id']) ? $order_details['broker_id'] : 0);
                                                ?> 
                                                <option value="<?php echo $value['business_partner_id']; ?>"  <?php
                                                if ($value['business_partner_id'] == $bpID) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                            <?php echo $value['business_name']; ?> 
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="form-group">
                                        <label for="">Freight Forwarder</label> 
                                        <select name="freight_forwarder_id" id="freight_forwarder_id" class="basic-single select-form-mro" <?=$dropDownCss;?>>  
                                            <option value="0">Select Freight Forwarder</option>
                                            <?php
                                            foreach ($freightForwarder as $value) {
                                                $bpID = (isset($order_details['freight_forwarder_id']) ? $order_details['freight_forwarder_id'] : 0);
                                                ?> 
                                                <option value="<?php echo $value['business_partner_id']; ?>"  <?php
                                                if ($value['business_partner_id'] == $bpID) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                            <?php echo $value['business_name']; ?> 
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                </div>
                                <hr class="separator mt0" id="shipment-dates">
                                <h3 class="form-group-title">Shipment Dates</h3>
                                <div class="form-row form-row-3">
                                    <div class="form-group input-edit">
                                        <label for="">Shipment Start Date<sup>*</sup></label>
                                        <div id="start_data_div" style="float: left;">
                                            <input type="date" name="shipment_start_date" id="shipment_start_date" value="<?php echo(!empty($order_details['shipment_start_date'])) ? date('Y-m-d', strtotime($order_details['shipment_start_date'])) : date("m/d/Y"); ?>" style="width: 95%" class="input-form-mro valid" aria-invalid="false" <?=$inputFieldCss;?>>                                                                
                                        </div> 
                                    </div>
                                    <div class="form-group input-edit">
                                        <label for="">Duration in Weeks<sup>*</sup></label>
                                        <div id="duration_div" style="float: left;width: 80%;">
                                            <input type="number" name="duration" id="duration" value="<?php echo(!empty($order_details['duration'])) ? $order_details['duration'] : ""; ?>" style="width: 95%" placeholder="Enter Duration in Weeks" class="input-form-mro valid" aria-invalid="false" <?=$inputFieldCss;?>>
                                        </div>
                                    </div>
                                </div>
                                <hr class="separator mt0" id="pickup-details">
                                <h3 class="form-group-title">Billing and Shipping Addresses</h3>
                                <div class="form-row form-row-3">
                                    <div class="form-group">
                                        <label for="">Customer Billing Address<sup>*</sup></label>
                                        <div id="customer_billing_details_div">
                                            <select name="customer_billing_details_id" id="customer_billing_details_id" class="basic-single select-form-mro" <?=$dropDownCss;?>>
                                                <option value="">Select Billing Address</option> 
                                            </select>
                                        </div>
                                        <div class="ba-single ba-select" id="customer_billing_details_add_div">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Customer Shipping Address<sup>*</sup></label>
                                        <div id="customer_shipping_details_div">
                                            <select name="customer_shipping_details_id" id="customer_shipping_details_id" class="basic-single select-form-mro" <?=$dropDownCss;?>>
                                                <option value="">Select Shipping Address</option> 
                                            </select>
                                        </div>
                                        <div class="ba-single ba-select" id="customer_shipping_details_add_div">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-row form-row-3">
                                    <div class="form-group">
                                        <label for="">Supplier Billing Address<sup>*</sup></label>
                                        <div id="supplier_billing_details_div">
                                            <select name="supplier_billing_details_id" id="supplier_billing_details_id" class="basic-single select-form-mro" <?=$dropDownCss;?>>
                                                <option value="">Select Billing Address</option> 
                                            </select>
                                        </div>
                                        <div class="ba-single ba-select" id="supplier_billing_details_add_div">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Supplier Shipping Address<sup>*</sup></label>
                                        <div id="supplier_shipping_details_div">
                                            <select name="supplier_shipping_details_id" id="supplier_shipping_details_id" class="basic-single select-form-mro" <?=$dropDownCss;?>>
                                                <option value="">Select Shipping Address</option> 
                                            </select>
                                        </div>
                                        <div class="ba-single ba-select" id="supplier_shipping_details_add_div">

                                        </div>
                                    </div>
                                </div>
                                <hr class="separator mt0" id="deliver-skus">
                                <h3 class="form-group-title">SKUs to Deliver</h3>
                                <div class="form-row form-row-3">
                                    <div class="sku-list-wrapper add-sku-wrapper" id="sku_details_div">
                                        <?php 
                                        foreach ($skudetails as $key => $value) {  
                                        ?>
                                            <div class="sit-row">
                                                <div class="sit-single sku-item-code">
                                                    <p class="sit-single-title">Item Code</p>
                                                    <p class="sit-single-value"><?=$skudetails[$key]['sku_number'];?></p>
                                                    <input type="hidden" name="sku_id[<?= $key ?>]" id="sku_id<?= $key ?>" value="<?= (!empty($skudetails[$key]['sku_id'])) ? $skudetails[$key]['sku_id'] : '' ?>">
                                                    <input type="hidden" name="sku_number[<?= $key ?>]" id="sku_number<?= $key ?>" value="<?= (!empty($skudetails[$key]['sku_number'])) ? $skudetails[$key]['sku_number'] : '' ?>">
                                                </div>
                                                <div class="sit-single sku-pd">
                                                    <p class="sit-single-title">Product Description</p>
                                                    <p class="sit-single-value"><?=$skudetails[$key]['product_name'];?></p>
                                                </div>
                                                <div class="sit-single sku-size">
                                                    <p class="sit-single-title">UoM</p>
                                                    <p class="sit-single-value"><?=$skudetails[$key]['uom_name'];?></p>
                                                </div>
                                                <div class="sit-single sku-size">
                                                    <p class="sit-single-title">Size</p>
                                                    <p class="sit-single-value"><?=$skudetails[$key]['size_name'];?></p>
                                                </div>
                                                <div class="sit-single sku-qty">
                                                    <p class="sit-single-title">Quantity<sup>*</sup></p>
                                                    <input type="text" name="sku_qty[<?= $key ?>]" id="sku_qty<?= $key ?>" value="<?= (!empty($skudetails[$key]['quantity'])) ? $skudetails[$key]['quantity'] : '' ?>" class="input-form-mro" placeholder="Enter quantity" <?=$inputFieldCss;?>>
                                                </div> 
                                            </div> 
                                        <?php } ?> 
                                    </div>
                                </div>
                                <hr class="separator mt0" id="delivery-schedule">
                                <?php if(empty($deliverySchedule)){?>
                                    <div class="load-delivery-schedule">
                                        <h3 class="form-group-title">Delivery Schedule</h3>
                                        <p><strong>Note:</strong> Add All SKUs before Loading Delivery Schedule</p>
                                        <span class="btn-primary-mro load-ds" onclick="loadDeliverySchedule();" style="cursor: pointer;">Load Delivery Schedule </span>
                                    </div>
                                <?php } ?>
                                <div class="delivery-schedule-data" style="display:<?php echo (!empty($deliverySchedule))? 'block':''?>;">
                                    <div class="title-sec-wrapper ds-title-wrapper">
                                        <h3 class="form-group-title">Delivery Schedule</h3>
                                        <?php if($view==0){ ?>
                                            <span class="btn-primary-mro load-ds" onclick="loadDeliverySchedule();" style="cursor: pointer;"><img src="<?php echo base_url(); ?>assets/images/redo-white.svg" alt=""> Reload </span> 
                                        <?php } ?>
                                    </div>
                                    <div id="delivery_schedule">
                                        <?php 
                                        $str = '';
                                        if(!empty($deliverySchedule)){
                                            $skuDateArr = array();
                                            foreach ($deliverySchedule as $key => $value) {  
                                                $skuDateArr[date('M Y', strtotime($value['week_date']))][$value['week_date']][$value['sku_id']]=array('qty'=>$value['sku_qty'],'sku'=>$value['sku_number']);
                                            }
                                            $str = ''; 
                                            $skuNumber_cnt = 0;
                                            foreach ($skuDateArr as $monthYrs => $weekArr) {  
                                                   $str = $str.'<div class="delivery-schedule-wrapper">';
                                                       $str = $str.'<div class="ds-single-wrapper"> 
                                                                   <div class="ds-row ds-row-header">
                                                                       <div class="ds-month">
                                                                           <a href="#/" class="ds-toggle-icon"></a> <input type="text" class="input-form-mro" value="'.$monthYrs.'" '.$inputFieldCss.'>
                                                                       </div> 
                                                                   </div>';

                                                      $skuNumber_cnt = 0;
                                                      $qtyTotalArr = array();
                                                       foreach ($weekArr as $weekDate => $valueSKU) {
                                                           $skuNumber_str = '';
                                                           $skuQty_str = ''; 
                                                           foreach ($valueSKU as $skuID => $skuQtyNumber) { 
                                                               if($skuNumber_cnt == 0){
                                                                   $uom = '';
                                                                   if(!empty($uomDetails[$skuID])){
                                                                       $uom = $uomDetails[$skuID];
                                                                   } 
                                                                   $skuNumber_str .= '<div class="ds-sku"><input type="hidden" name="schedule_skuNumber['.$skuID.']" class="input-form-mro" value="'.$skuQtyNumber['sku'].'">'.$skuQtyNumber['sku'].' ('.$uom.')</div>';  
                                                               }
                                                               $qty = $skuQtyNumber['qty'];
                                                               $skuQty_str .='<div class="ds-sku">
                                                                               <input type="text" name="schedule_skuQty['.$skuID.']" class="input-form-mro" value="'.$qty.'" '.$inputFieldCss.'>
                                                                           </div>';

                                                               $qtyTotalArr[$skuID][] = $qty; 
                                                           } 
                                                           //sku week date, sku number and qty code
                                                           $str .= '<div class="ds-expand-content" >';
                                                                    if($skuNumber_cnt == 0){
                                                                       $str .= '<div class="ds-row ds-row-week ds-row-week-header">
                                                                            <div class="ds-month"></div>
                                                                            <div class="ds-weeks">Weeks</div>
                                                                            '.$skuNumber_str.'
                                                                        </div>';
                                                                    }
                                                                    $str .= '<div class="ds-row ds-row-week">
                                                                         <div class="ds-month"></div>
                                                                         <div class="ds-weeks">
                                                                             <input type="text"  name="schedule_weekdate[]" class="input-form-mro" value="'.$weekDate.'" readonly>
                                                                         </div>
                                                                         '.$skuQty_str.'
                                                                     </div> 
                                                                   </div>';
                                                            //end week date, sku number and qty code
                                                           $skuNumber_cnt++;
                                                       } 
                                                       //code for month wise qty total
                                                       $str .= '<div class="ds-row ds-row-total">
                                                                   <div class="ds-month"></div>
                                                                   <div class="ds-weeks">Recalculated Total</div>'; 
                                                                   foreach ($qtyTotalArr as $skuId => $qty) {
                                                                       $qtyTotalData = 0;
                                                                       foreach ($qty as $key => $qtyTotal) {
                                                                           $qtyTotalData += $qtyTotal;
                                                                       }
                                                                       $str .='<div class="ds-sku">
                                                                       '.$qtyTotalData.'
                                                                       </div>'; 
                                                                   }  
                                                              $str .= '</div>';
                                                        //end total

                                                       $str .='</div></div>'; 
                                           }  
                                           
                                         }
                                         echo $str;
                                         ?>
                                    </div> 
                                </div> 
                            </div>
                            <div class="scroll-content-right master-order-right">
                                <div class="vertical-scroll-links">
                                    <a href="#contract-details" class="active">Contract Details</a>
                                    <a href="#shipment-dates">Shipment Dates</a>
                                    <a href="#pickup-details">Billing and Shipping Addresses</a>
                                    <a href="#deliver-skus">SKUs to Deliver</a>
                                    <a href="#delivery-schedule">Delivery Schedule</a>
                                </div>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>

<script> 
    //get customer and supplier contract and fill in master contract
    function getCustomerContact(isedit=0){
        var customer_id = $('#customer_contract_id').val();
        var supplier_id = $('#supplier_contract_id').val();
        if(customer_id > 0 && supplier_id > 0){
            var act = "<?php echo base_url(); ?>order/getOrderContract";
            $.ajax({
                type: 'GET',
                url: act,
                data: {customer_id: customer_id,supplier_id: supplier_id},
                dataType: "json",
                success: function (data) {
                    if(isedit==0){
                        if(data.contract != ''){
                            $('#contract_number').val(data.contract);
                        }else{
                            $('#contract_number').val(''); 
                            alert("Customer Contract not exist"); 
                        } 
                    }
                }
            });
        }
    }

    //remove sku details
    $(document).on("click", '.inner_remove', function () {
        var tr_inner_count = $(this).closest('tr').attr('tr_inner_count');
        $(this).closest('tr').remove();
    });

</script>
<script>
    //code for get business details and shipping details in dropdown also get payment and incoterm
    function getBillingShippingDetails(bpo_id = 0, type, billing_id = '', shipping_id = '', isedit = 0) {
        //get customer supplier contract number and fill in master contract
        getCustomerContact(isedit);
        var act = "<?php echo base_url(); ?>order/getBusinessPartner";
        $.ajax({
            type: 'GET',
            url: act,
            data: {type: type, bpo_id: bpo_id, billing_id: billing_id, shipping_id: shipping_id,isView:<?=$view?>},
            dataType: "json",
            success: function (data) {
                if(type =='customer'){
                    $('#customer_billing_details_div').html(data.billing);
                    $('#customer_shipping_details_div').html(data.shipping); 
                    if(isedit == 0){
                        $('#sku_details_div').html(data.sku);
                        $('#start_data_div').html(data.date);
                        $('#duration_div').html(data.duration);
                    }
                }else{
                    $('#supplier_billing_details_div').html(data.billing);
                    $('#supplier_shipping_details_div').html(data.shipping); 
                }
            }
        });
        if(type =='customer'){
            getBillAddress(billing_id,0);
            getShipAddress(shipping_id,0);
        }else{
            getBillAddress(billing_id,1);
            getShipAddress(shipping_id,1);
        }
    }
    //code for show billing address details
    function getBillAddress(bd_id = 0,cnt) {
        var act = "<?php echo base_url(); ?>order/getBillingAddress";
        $.ajax({
            type: 'GET',
            url: act,
            data: {bd_id: bd_id},
            dataType: "json",
            success: function (data) {
                if(cnt==0){
                    $('#customer_billing_details_add_div').html(data.billing);
                }else{
                    $('#supplier_billing_details_add_div').html(data.billing);
                }
            }
        });
    }
    //code for show shipping address details
    function getShipAddress(sd_id = 0,cnt) {
        var act = "<?php echo base_url(); ?>order/getShipAddress";
        $.ajax({
            type: 'GET',
            url: act,
            data: {bd_id: sd_id},
            dataType: "json",
            success: function (data) {
                if(cnt==0){
                    $('#customer_shipping_details_add_div').html(data.billing);
                }else{
                    $('#supplier_shipping_details_add_div').html(data.billing);
                }
            }
        });
    }


//code for edit case get business details and shipping details in dropdown also get payment and incoterm
<?php if (!empty($order_details['order_id'])) { ?>
        getBillingShippingDetails('<?php echo(!empty($order_details['customer_contract_id'])) ? $order_details['customer_contract_id'] : "0"; ?>','customer', '<?php echo(!empty($order_details['customer_billing_details_id'])) ? $order_details['customer_billing_details_id'] : "0"; ?>', '<?php echo(!empty($order_details['customer_shipping_details_id'])) ? $order_details['customer_shipping_details_id'] : "0"; ?>', '1');
        getBillingShippingDetails('<?php echo(!empty($order_details['supplier_contract_id'])) ? $order_details['supplier_contract_id'] : "0"; ?>','supplier', '<?php echo(!empty($order_details['supplier_billing_details_id'])) ? $order_details['supplier_billing_details_id'] : "0"; ?>', '<?php echo(!empty($order_details['supplier_shipping_details_id'])) ? $order_details['supplier_shipping_details_id'] : "0"; ?>', '1');
<?php } ?>

</script> 
<script>
    function loadDeliverySchedule(){ 
        var data = $('#addEditForm').serialize();
        var act = "<?php echo base_url(); ?>order/loadDeliverySchedule";
        $.ajax({  
            url: act,
            type: 'POST', 
            data: data,
            dataType: 'json',
            cache: false,
            clearForm: false,
            success: function (data) {  
                $('#delivery_schedule').html(data.str);
                
            }
        });
    }
</script> 
<script>

    $(document).ready(function () {

        var vRules = {
            "contract_number": {required: true},
            "customer_contract_id": {required: true},
            "supplier_contract_id": {required: true},
            "shipment_start_date": {required: true},
            "duration": {required: true},
            "customer_billing_details_id": {required: true},
            "customer_shipping_details_id": {required: true},
            "supplier_billing_details_id": {required: true},
            "supplier_shipping_details_id": {required: true}
        };
        var vMessages = {
            "contract_number": {required: "Please enter master contract number."},
            "customer_contract": {required: "Please enter customer contract number."},
            "supplier_contract_id": {required: "Please select supplier contract number."},
            "shipment_start_date": {required: "Please select shipment start date."},
            "duration": {required: "Please enter duration in weeks."},
            "customer_billing_details_id": {required: "Please select customer billing address."},
            "customer_shipping_details_id": {required: "Please select customer shipping address."},
            "supplier_billing_details_id": {required: "Please select supplier billing address."},
            "supplier_shipping_details_id": {required: "Please select supplier shipping address."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>order/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        //console.log(response);
                        if (response.success) {
                            alert(response.msg);
                            setTimeout(function () {
                                window.location = "<?= base_url('order') ?>";
                            }, 1000);
                        } else {
                            alert(response.msg);
                        }
                    }
                });
            }
        });

    });
</script> 