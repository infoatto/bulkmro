<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Container_status extends CI_Controller
{
	function __construct(){
            parent::__construct();
            $this->load->model('container_statusmodel','',TRUE);
            $this->load->model('common_model/common_model','common',TRUE);
            checklogin();
            if(!$this->privilegeduser->hasPrivilege("ContainerStatusList")){
                redirect('dashboard');
            }
	}

	function index(){
            $container_statusData = array(); 
            $result = $this->common->getData("tbl_container_status","*","");                
            if(!empty($result)){
                $container_statusData['container_status_details'] = $result[0];
            }  
            $this->load->view('template/head.php');
            $this->load->view('template/navigation.php'); 
            $this->load->view('container_status/index',$container_statusData);
            $this->load->view('template/footer.php');                
            $this->load->view('template/footer-scripts.php'); 
	}

	function addEdit(){
            if(!$this->privilegeduser->hasPrivilege("ContainerStatusAddEdit")){
                redirect('dashboard');
            }
            $edit_datas = array();
            if(!empty($_GET['text']) && isset($_GET['text'])){
                $varr=base64_decode(strtr($_GET['text'], '-_', '+/'));
                parse_str($varr,$url_prams);
                $container_status_id = $url_prams['id'];
                $result = $this->common->getData("tbl_container_status","*",array("container_status_id"=>$container_status_id));
                if(!empty($result)){
                    $edit_datas['container_status_details'] = $result[0];
                } 
            }
            
            //document type
            $documentType = $this->common->getData("tbl_document_type", "document_type_id,document_type_name", "");
            if (!empty($documentType)) {
                $edit_datas['documentType'] = $documentType;
            }
            
            $this->load->view('template/head.php');
            $this->load->view('template/navigation.php'); 
            $this->load->view('container_status/addEdit',$edit_datas);
            $this->load->view('template/footer.php');                
            $this->load->view('template/footer-scripts.php'); 
	}

	function fetch(){  
            $get_result = $this->container_statusmodel->getRecords($_GET); 
            $result = array();
            $result["sEcho"]= $_GET['sEcho'];
            $result["iTotalRecords"] = $get_result['totalRecords'];	//iTotalRecords get no of total recors
            $result["iTotalDisplayRecords"]= $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
            $items = array();
            if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0){
                for($i=0;$i<sizeof($get_result['query_result']);$i++){
                    $temp = array();
                    array_push($temp, ucfirst($get_result['query_result'][$i]->container_status_name));
                    array_push($temp, ucfirst($get_result['query_result'][$i]->status)); 
                    $actionCol = '';
                    if($this->privilegeduser->hasPrivilege("ContainerStatusAddEdit")){ 
                        if($get_result['query_result'][$i]->container_status_id != '1'){
                            $actionCol ='<a href="container_status/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->container_status_id), '+/', '-_'), '=').'" title="Edit" class="text-center" style="float:left;width:100%;"><img src="'.base_url().'assets/images/edit-icon.svg" alt="Edit" ></a>';
                        } 
                    }
                    array_push($temp, $actionCol); 
                    array_push($items, $temp);
                }
            }
            $result["aaData"] = $items;
            echo json_encode($result);
            exit;
	} 

	function submitForm(){ 
            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                /*check duplicate entry*/
                $condition = "container_status_name = '".$this->input->post('container_status_name')."' ";
                if(!empty($this->input->post("container_status_id"))){
                    $condition .= " AND container_status_id <> ".$this->input->post("container_status_id");
                }

                $chk_client_sql = $this->common->Fetch("tbl_container_status","container_status_id",$condition);
                $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

                if(!empty($rs_client[0]['container_status_id'])){
                    echo json_encode(array('success'=>false, 'msg'=>'Size name already exist...'));
                    exit;
                }

                $data = array();
                $data['container_status_name'] = $this->input->post('container_status_name');
                $data['status'] = $this->input->post('status'); 
                $data['document_type_id'] = $this->input->post('document_type_id'); 
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                if(!empty($this->input->post("container_status_id"))){
                    $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['updated_on'] = date("Y-m-d H:i:s");
                    $result = $this->common->updateData("tbl_container_status",$data,array("container_status_id"=>$this->input->post("container_status_id")));
                    if($result){
                        echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
                        exit;
                    }else{
                        echo json_encode(array('success'=>false, 'msg'=>'Problem while Updating data.'));
                        exit;
                    }
                }else{
                    $data['container_status_name'] = $this->input->post('container_status_name');
                    $data['status'] = $this->input->post('status');
                    $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['created_on'] = date("Y-m-d H:i:s");
                    $result = $this->common->insertData("tbl_container_status",$data,"1");
                    if($result){
                        echo json_encode(array('success'=>true, 'msg'=>'Record Inserted Successfully.'));
                        exit;
                    }else{
                        echo json_encode(array('success'=>false, 'msg'=>'Problem while Inserting data.'));
                        exit;
                    }
                }  
            }else{
                echo json_encode(array('success'=>false, 'msg'=>'Problem while add/edit data.'));
                exit;
            }
	} 
        
}

?>
