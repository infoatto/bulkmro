<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="<?php echo base_url("dashboard") ?>">Home</a>
              <span>></span>
              <p>Dashboard</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title"><a href="<?php echo base_url("roles"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Roles</h1>
            </div>
          </div>
          <div class="title-sec-right">
              <?php if($this->privilegeduser->hasPrivilege("RolesAddEdit")){ ?>
                <a href="<?php echo base_url("roles/addEdit"); ?>" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> Add Role</a>
              <?php } ?>
          </div>
        </div>
        <div class="page-content-wrapper1">
          <div id="serchfilter" class="filter-sec-wrapper">
            <div class="filter-search dataTables_filter searchFilterClass">
              <input type="text" id="sSearch_0" name="sSearch_0" class="searchInput filter-search-input" placeholder="Role Name">
            </div>
            <div class="form-group clear-search-filter">
              <button class="btn-primary-mro" onclick="clearSearchFilters();">Clear Search</button>
            </div>
          </div>
        </div><br>
        <div class="bp-list-wrapper">
          <div class="table-responsive">
            <table class="table table-striped basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
              <thead>
                <tr>
                  <th>Role Name</th>
                  <th>Status</th>
                  <th class="table-action-cls" style="width:50px !important;">Actions</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>