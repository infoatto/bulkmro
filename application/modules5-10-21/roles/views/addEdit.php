<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("roles"); ?>">Roles List</a>
                                <span>></span>
                                <p>Add Role</p>
                            </div>
                            <div class="page-title-wrapper"> 
                                <h1 class="page-title">Add Role</h1> 
                            </div>
                        </div>          
                        <button class="btn-primary-mro">Save</button>
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="role_id" id="role_id" value="<?php echo(!empty($role_details['role_id'])) ? $role_details['role_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="RoleName">Role Name</label>
                                    <input type="text" name="role_name" id="role_name" class="input-form-mro" value="<?php echo(!empty($role_details['role_name'])) ? $role_details['role_name'] : ""; ?>"  <?php echo(!empty($role_details['role_id'])) ? "readonly" : ""; ?>>
                                </div> 
                                <div class="form-group">
                                    <label for="Status">Status</label>
                                    <select name="status" id="status" class="select-form-mro">
                                        <option value="Active" <?php echo(!empty($role_details['status']) && $role_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                        <option value="In-active" <?php echo(!empty($role_details['status']) && $role_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                    </select>
                                </div>
                            </div>
                            <hr class="separator mt0">
                            <div class="form-row form-row-3">
                                <div class="control-group">
                                    <div class="">
                                        <fieldset id="fieldUserRoles">
                                            <div> 
                                                <h3 class="form-group-title">User Roles</h3>
                                            </div> 
                                            <?php
                                                $selectedCheckArray = array();
                                                if(!empty($permissions) && isset($permissions)){
                                                    for($i=0;$i<sizeof($permissions);$i++){
                                                        if(!empty($selpermissions) && isset($selpermissions)){
                                                            for($j=0;$j<sizeof($selpermissions);$j++){
                                                                $selectedCheckArray[] = $selpermissions[$j]->perm_id;
                                                            }
                                                        }
                                            ?>
                                                        <span style="float:left;width:300px;overflow-wrap: break-word;">
                                                            <input <?php if(in_array($permissions[$i]->perm_id,$selectedCheckArray)){ echo "checked=checked";} ?> class="chk" type="checkbox" id="<?php echo $permissions[$i]->perm_id; ?>" name="perm_id[]" value="<?php echo $permissions[$i]->perm_id; ?>"/>
                                                            <?php echo $permissions[$i]->perm_desc; ?>
                                                        </span>
                                            <?php   } 
                                                } 
                                            ?>
                                            <div class="clearfix" style="height: 10px; width: 100%; float: left; display: inline;">&nbsp;</div>
                                            <button id="chkAll" style="float:left;" class="btn-primary-mro marginR10 checkall" type="button" >Check All</button>
                                            <div class="clearfix" style="height: 10px; width: 100%; float: left; display: inline;">&nbsp;</div>
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                            
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>
    var clicked = false;
    $(".checkall").on("click", function() {
      $(".chk").prop("checked", !clicked);
      clicked = !clicked;
    });

    $(document).ready(function () {
        var vRules = {
            "role_name": {required: true}
        };
        var vMessages = {
            "role_name": {required: "Please enter role name."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>roles/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show(); 
                        if (response.success) {
                            alert(response.msg);
                            setTimeout(function () {
                                window.location = "<?= base_url('roles') ?>";
                            }, 1000);
                        } else {
                            alert(response.msg);
                        }
                    }
                });
            }
        });

    });
</script>