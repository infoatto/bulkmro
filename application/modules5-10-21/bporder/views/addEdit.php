<?php
//start code of view option    
$dropDownCss = '';
$inputFieldCss = '';
if($view==1){
    $dropDownCss = "disabled";
    $inputFieldCss = "readonly";
}
//end code of view option
?>
<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("bporder"); ?>">Business Partners</a>
                                <span>></span>
                                <p>New Business Partner Contract</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Business Partner Contract</h1>
                            </div>
                        </div> 
                        <?php if($view==0){ ?>
                             <button type="submit" class="btn-primary-mro">Save</button>
                        <?php } ?> 
                    </div>
                    <div class="page-content-wrapper">
                        <input type="hidden" name="bp_order_id" id="bp_order_id" value="<?php echo(!empty($bp_order_details['bp_order_id'])) ? $bp_order_details['bp_order_id'] : ""; ?>">
                        <div class="scroll-content-wrapper">
                            <div class="scroll-content-left" id="contract-details"> 
                                <h3 class="form-group-title">Contract Details</h3>
                                <div class="form-row form-row-3">
                                    <div class="form-group">
                                        <label for="">Contract Number<sup>*</sup></label>
                                        <input type="text" name="contract_number" id="contract_number" value="<?php echo(!empty($bp_order_details['contract_number'])) ? $bp_order_details['contract_number'] : ""; ?>" placeholder="Enter Contract Number" class="input-form-mro" <?=$inputFieldCss;?>>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Business Partner<sup>*</sup></label>
                                        <select name="business_partner_id" id="business_partner_id" class="basic-single select-form-mro" onchange="getBillingShippingDetails(this.value)" <?=$dropDownCss;?>> 
                                            <option value="">Select Business Partner</option>
                                            <?php
                                            foreach ($bpDetails as $bpValue) {
                                                $bpID = (isset($bp_order_details['business_partner_id']) ? $bp_order_details['business_partner_id'] : 0);
                                                ?> 
                                                <option value="<?php echo $bpValue['business_partner_id']; ?>"  <?php
                                                if ($bpValue['business_partner_id'] == $bpID) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                            <?php echo $bpValue['business_name']; ?> 
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                </div>
                                <hr class="separator mt0" id="shipment-dates">
                                <h3 class="form-group-title">Shipment Dates</h3>
                                <div class="form-row form-row-3">
                                    <div class="form-group datepicker">
                                        <label for="">Shipment Start Date<sup>*</sup></label>
                                        <input type="date" name="shipment_start_date" id="shipment_start_date" value="<?php echo(!empty($bp_order_details['shipment_start_date'])) ? date('Y-m-d', strtotime($bp_order_details['shipment_start_date'])) : date("m/d/Y"); ?>" class="form-control valid" aria-invalid="false" <?=$inputFieldCss;?>>                    
                                    </div>
                                    <div class="form-group">
                                        <label for="">Duration in Weeks<sup>*</sup></label>
                                        <input type="number" name="duration" id="duration" value="<?php echo(!empty($bp_order_details['duration'])) ? $bp_order_details['duration'] : ""; ?>" placeholder="Enter Duration in Weeks" class="input-form-mro" <?=$inputFieldCss;?>>                    
                                    </div>
                                </div>
                                <hr class="separator mt0" id="pickup-details">
                                <h3 class="form-group-title">Billing and Shipping Addresses</h3>
                                <div class="form-row form-row-3">
                                    <div class="form-group">
                                        <label for="">Billing Address<sup>*</sup></label>
                                        <div id="billing_details_div">
                                            <select name="billing_details_id" id="billing_details_id" class="select-form-mro" <?=$dropDownCss;?>>
                                                <option value="">Select Billing Address</option> 
                                            </select>
                                        </div>
                                        <div class="ba-single ba-select" id="billing_details_add_div">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Ship To or From Address<sup>*</sup></label> 
                                        <div id="shipping_details_div">
                                            <select name="shipping_details_id" id="shipping_details_id" class="select-form-mro" <?=$dropDownCss;?>>
                                                <option value="">Select Shipping Address</option> 
                                            </select>
                                        </div>
                                        <div class="ba-single ba-select" id="shipping_details_add_div">

                                        </div>
                                    </div>
                                </div>
                                <hr class="separator mt0" id="scroll-terms">
                                <h3 class="form-group-title">Terms</h3>
                                <div class="form-row form-row-3">
                                    <div class="form-group input-edit">
                                        <label for="">Payment Terms</label>
                                        <div id="patment_terms_text" style="width: 85%;float: left;">
                                            <input type="text" name="patment_terms" id="patment_terms" value="<?php echo(!empty($bp_order_details['patment_terms'])) ? $bp_order_details['patment_terms'] : ""; ?>" placeholder="Typed Response" style="width: 97%;float: left;" class="input-form-mro" <?=$inputFieldCss;?>>                                            
                                        </div>
                                        <?php if($view==0){ ?>
                                        <img onclick="showHidePaymentDiv(1)" src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt="">
                                        <?php } ?>
                                    </div>
                                    <div class="form-group input-edit">
                                        <label for="">Incoterm</label>
                                        <div id="incoterms_text" style="width: 85%;float: left;">
                                            <input type="text" name="incoterms" id="incoterms" value="<?php echo(!empty($bp_order_details['incoterms'])) ? $bp_order_details['incoterms'] : ""; ?>" placeholder="Typed Response" style="width: 97%;float: left;" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                        <?php if($view==0){ ?>
                                        <img onclick="showHidePaymentDiv(2)" src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt="">
                                        <?php } ?>
                                    </div>
                                </div>
                                <hr class="separator mt0" id="add-skus">
                                <h3 class="form-group-title">Add SKUs to Order</h3>
                                <div class="form-row form-row-2">
                                    <div class="form-group">
                                        <label for="">SKU Description</label>
                                        <select name="sku_id" id="sku_id" class="basic-single select-form-mro" onchange="getskuDetails(this.value)" <?=$dropDownCss;?>> 
                                            <option value="">Select SKU Description</option>
                                            <?php
                                            foreach ($skuDetails as $skuValue) {
                                                ?> 
                                                <option value="<?php echo $skuValue['sku_id']; ?>">
                                                    <?php echo $skuValue['product_name']; ?> 
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="sku-list-wrapper add-sku-wrapper"> 
                                        <?php 
                                        $srNo = 0;
                                        if (empty($bp_skudetails)) { ?>
                                            <table class="table  product_location1 table-bordered table-striped disable_" id="admore_inner_table_0">
                                                <tbody >
                                                    <tr tr_inner_count="0" class="active">
                                                
                                                    </tr> 
                                                </tbody>
                                            </table>
                                        <?php } else { ?>               
                                            <table class="table  product_location1 table-bordered table-striped disable_" id="admore_inner_table_0">
                                                <tbody >
                                                    <?php 
                                                    foreach ($bp_skudetails as $key => $value) {  
                                                    ?>
                                                        <tr tr_inner_count="<?= $key ?>" class="active">
                                                            <td>  
                                                                <div class="sit-row">
                                                                    <div class="sit-single sku-item-code">
                                                                        <p class="sit-single-title">Item Code</p>
                                                                        <p class="sit-single-value"><?=$bp_skudetails[$key]['sku_number'];?></p>
                                                                        <input type="hidden" name="sku_id[<?= $key ?>]" id="sku_id<?= $key ?>" value="<?= (!empty($bp_skudetails[$key]['sku_id'])) ? $bp_skudetails[$key]['sku_id'] : '' ?>" placeholder="Enter Contact Person" class="input-form-mro">
                                                                    </div>
                                                                    <div class="sit-single sku-pd">
                                                                        <p class="sit-single-title">Product Description</p>
                                                                        <p class="sit-single-value"><?=$bp_skudetails[$key]['product_name'];?></p>
                                                                    </div>
                                                                    <div class="sit-single sku-size">
                                                                        <p class="sit-single-title">Size</p>
                                                                        <p class="sit-single-value"><?=$bp_skudetails[$key]['size_name'];?></p>
                                                                    </div>
                                                                    <div class="sit-single sku-qty">
                                                                        <p class="sit-single-title">Quantity<sup>*</sup></p>
                                                                        <input type="text" name="sku_qty[<?= $key ?>]" id="sku_qty<?= $key ?>" value="<?= (!empty($bp_skudetails[$key]['quantity'])) ? $bp_skudetails[$key]['quantity'] : '' ?>" class="input-form-mro" placeholder="Enter quantity" <?=$inputFieldCss;?>>
                                                                    </div>
                                                                    <div class="sit-single sku-price">
                                                                        <p class="sit-single-title">Price per unit in USD<sup>*</sup></p>
                                                                        <input type="text" name="sku_price[<?= $key ?>]" id="sku_price<?= $key ?>" value="<?= (!empty($bp_skudetails[$key]['price'])) ? $bp_skudetails[$key]['price'] : '' ?>" class="input-form-mro" placeholder="Enter Price" <?=$inputFieldCss;?>>
                                                                    </div>
                                                                    <div class="sit-single sku-remove">
                                                                        <?php if($view==0){ ?>
                                                                        <button title="Remove" class="remove-sku remove inner_remove" type="button"><img src="<?php echo base_url(); ?>assets/images/remove-dark.svg" alt="" class="add-more-img">&nbsp&nbspRemove</button>                          
                                                                        <?php } ?>
                                                                    </div> 
                                                                </div> 
                                                            </td>
                                                        </tr>
                                                    <?php } ?> 
                                                </tbody>
                                            </table>
                                        <?php } ?>
                                        <input type="hidden" id="count" value="<?=$srNo?>">
                                    </div>
                                </div> 
                            </div>
                            <div class="scroll-content-right">
                                <div class="vertical-scroll-links">
                                    <a href="#contract-details" class="active">Contract Details</a>
                                    <a href="#shipment-dates">Shipment Dates</a>
                                    <a href="#pickup-details">Billing and Shipping Addresses</a>
                                    <a href="#scroll-terms">Terms</a>
                                    <a href="#add-skus">Add SKUs to Order</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>
<script>
    //show and hide payment term and incoterm dropdown
    function showHidePaymentDiv(cnt){ 
        var act = "<?php echo base_url(); ?>bporder/getPaymentIncoterm";
        $.ajax({
            type: 'GET',
            url: act,
            data: {cnt: cnt},
            dataType: "json",
            success: function (data) {
                if(cnt==1){
                    $('#patment_terms_text').html(data.paymentTerm);
                }else{
                    $('#incoterms_text').html(data.incoTerm); 
                }
                $('.basic-single').select2();
            }
        }); 
    }
    //add sku details function
    function getskuDetails(sku_id){ 
        if(sku_id > 0){
            var act = "<?php echo base_url(); ?>bporder/getskuDetails";
            $.ajax({
                type: 'GET',
                url: act,
                data: {sku_id: sku_id},
                dataType: "json",
                success: function (data) { 
                        //var x = $(this).attr("panel-count");
                        var x = $('#count').val();
                        // alert(x);
                        // return;
                        var tx = $("#admore_inner_table_" + x + " tbody tr:last-child").attr("tr_inner_count");
                        tx++;
                        if (isNaN(tx) || tx == "undefined") {
                            tx = 0;
                        }                                         

                        var inner_tr = '<tr tr_inner_count="' + tx + '" class="active">' +
                                '<td>' +
                                '<div class="sit-row">' +
                                    '<div class="sit-single sku-item-code">' +
                                        '<p class="sit-single-title">Item Code</p>' +
                                        '<p class="sit-single-value">' + data.skuData.sku_number + '</p>' +
                                        '<input type="hidden" name="sku_id[' + tx + ']" id="sku_id' + tx + '" value="' + data.skuData.sku_id + '" placeholder="Enter Contact Person" class="input-form-mro">' +
                                    '</div>'+
                                    '<div class="sit-single sku-pd">'+
                                        '<p class="sit-single-title">Product Description</p>'+
                                        '<p class="sit-single-value">' + data.skuData.product_name + '</p>'+
                                    '</div>'+
                                    '<div class="sit-single sku-size">'+
                                        '<p class="sit-single-title">Size</p>'+
                                        '<p class="sit-single-value">' + data.skuData.size_name + '</p>'+
                                    '</div>'+
                                    '<div class="sit-single sku-qty">'+
                                        '<p class="sit-single-title">Quantity<sup>*</sup></p>'+
                                        '<input type="text" name="sku_qty[' + tx + ']" id="sku_qty' + tx + '" value="" class="input-form-mro" placeholder="Enter quantity">'+
                                    '</div>'+
                                    '<div class="sit-single sku-price">'+
                                        '<p class="sit-single-title">Price per unit in USD<sup>*</sup></p>'+
                                        '<input type="text" name="sku_price[' + tx + ']" id="sku_price' + tx + '" value="" class="input-form-mro" placeholder="Enter Price">'+
                                    '</div>'+ 
                                    '<div class="sit-single sku-remove">' +
                                    '<button title="Remove" class="remove-sku remove inner_remove" type="button"><img src="<?php echo base_url(); ?>assets/images/remove-dark.svg" alt="" class="add-more-img">&nbsp&nbspRemove</button>' +
                                    '</div>' + 
                                '</div>' +
                                '</td>' +
                                '</tr>';
                        $("#admore_inner_table_" + x + " tbody").append(inner_tr);
                        //console.log(inner_tr); 
                }
            });
        
        }
       
    }
    //remove sku details
    $(document).on("click", '.inner_remove', function () {
        var tr_inner_count = $(this).closest('tr').attr('tr_inner_count');
        $(this).closest('tr').remove(); 
    });

</script>
<script>
    //code for get business details and shipping details in dropdown also get payment and incoterm
    function getBillingShippingDetails(bp_id = 0, billing_id = '', shipping_id = '',paymentIcoterm=0) {
        var act = "<?php echo base_url(); ?>bporder/getBusinessPartner";
        $.ajax({
            type: 'GET',
            url: act,
            data: {bp_id: bp_id,billing_id: billing_id, shipping_id: shipping_id,isView:<?=$view?>},
            dataType: "json",
            success: function (data) {
                $('#billing_details_div').html(data.billing);
                $('#shipping_details_div').html(data.shipping);
                if(paymentIcoterm==0){
                    $('#patment_terms').val(data.payment);
                    $('#incoterms').val(data.incoterm);
                }
                $('.basic-single').select2();
            }
        });
        getBillAddress(billing_id);
        getShipAddress(shipping_id);
    }
    //code for show billing address details
    function getBillAddress(bd_id = 0) {
        var act = "<?php echo base_url(); ?>bporder/getBillingAddress";
        $.ajax({
            type: 'GET',
            url: act,
            data: {bd_id: bd_id},
            dataType: "json",
            success: function (data) {
                $('#billing_details_add_div').html(data.billing);
            }
        });
    }
    //code for show shipping address details
    function getShipAddress(sd_id = 0) {
        var act = "<?php echo base_url(); ?>bporder/getShipAddress";
        $.ajax({
            type: 'GET',
            url: act,
            data: {bd_id: sd_id},
            dataType: "json",
            success: function (data) {
                $('#shipping_details_add_div').html(data.billing);
            }
        });
    }


//code for edit case get business details and shipping details in dropdown also get payment and incoterm
<?php if (!empty($bp_order_details['business_partner_id'])) { ?>
        getBillingShippingDetails('<?php echo(!empty($bp_order_details['business_partner_id'])) ? $bp_order_details['business_partner_id'] : "0"; ?>', '<?php echo(!empty($bp_order_details['billing_details_id'])) ? $bp_order_details['billing_details_id'] : "0"; ?>', '<?php echo(!empty($bp_order_details['shipping_details_id'])) ? $bp_order_details['shipping_details_id'] : "0"; ?>','1');
<?php } ?>

</script> 
<script>

    $(document).ready(function () { 

        var vRules = {
            "contract_number": {required: true},
            "business_partner_id": {required: true},
            "shipment_start_date": {required: true},
            "duration": {required: true},
            "billing_details_id": {required: true},
            "shipping_details_id": {required: true}
        };
        var vMessages = {
            "contract_number": {required: "Please enter contract number."},
            "business_partner_id": {required: "Please select business partner."},
            "shipment_start_date": {required: "Please select shipment start date."},
            "duration": {required: "Please enter duration in weeks."},
            "billing_details_id": {required: "Please select billing address."},
            "shipping_details_id": {required: "Please select shipping address."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>bporder/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        console.log(response);
                        if (response.success) {
                            alert(response.msg);
                            setTimeout(function () {
                                window.location = "<?= base_url('bporder') ?>";
                            }, 1000);
                        } else {
                            alert(response.msg);
                        }
                    }
                });
            }
        });

    });
</script> 