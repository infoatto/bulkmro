<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Bporder extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('bpordermodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersOrderList")){
            redirect('dashboard');
        }
    }

    function index() {
        $data = array(); 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('bporder/index', $data);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")){
            redirect('dashboard');
        }
        $edit_datas = array();
        $edit_datas['bp_order_details'] = array();
        $edit_datas['bpDetails'] = array();
        $edit_datas['skuDetails'] = array();
        $edit_datas['bp_skudetails'] = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $bp_order_id = $url_prams['id'];
            $result = $this->common->getData("tbl_bp_order", "*", array("bp_order_id" => $bp_order_id));
            if (!empty($result)) {
                $edit_datas['bp_order_details'] = $result[0];
            }
            //order sku details data
            $skuData = $this->bpordermodel->getOrderSkuDetails($bp_order_id);
            if (!empty($skuData)) {
                $edit_datas['bp_skudetails'] = $skuData;
            }
        }
        //business partner data
        $bpDetails = $this->bpordermodel->getBusinessPartner();
        if (!empty($bpDetails)) {
            $edit_datas['bpDetails'] = $bpDetails;
        }
        //sku data
        $skuDetails = $this->common->getData("tbl_sku", "sku_id,sku_number,product_name", "");
        if (!empty($skuDetails)) {
            $edit_datas['skuDetails'] = $skuDetails;
        }
        $edit_datas['view'] = isset($_GET['view'])?$_GET['view']:0;
        
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('bporder/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        $get_result = $this->bpordermodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, $get_result['query_result'][$i]->contract_number);
                array_push($temp, date('d-m-Y', strtotime($get_result['query_result'][$i]->shipment_start_date)));
                array_push($temp, $get_result['query_result'][$i]->duration);
                array_push($temp, $get_result['query_result'][$i]->patment_terms);
                array_push($temp, $get_result['query_result'][$i]->incoterms);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")){   
                    $actionCol = '<a href="bporder/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->bp_order_id), '+/', '-_'), '=') . '" title="Edit" class="text-center"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                 if($this->privilegeduser->hasPrivilege("BusinessPartnersOrderView")){   
                    $actionCol .= '<a href="bporder/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->bp_order_id), '+/', '-_'), '=') . '&view=1" title="Edit" class="text-center" style="float:right;"><img src="' . base_url() . 'assets/images/view-btn-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "contract_number = '" . $this->input->post('contract_number') . "' ";
            if (!empty($this->input->post("bp_order_id"))) {
                $condition .= " AND bp_order_id <> " . $this->input->post("bp_order_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_bp_order", "bp_order_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['bp_order_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Contract Number already exist...'));
                exit;
            }

            $data = array();
            $data['contract_number'] = $this->input->post('contract_number');
            $data['business_partner_id'] = $this->input->post('business_partner_id');
            $data['shipment_start_date'] = !empty($this->input->post('shipment_start_date')) ? date("Y-m-d", strtotime($this->input->post('shipment_start_date'))) : NULL;
            $data['duration'] = $this->input->post('duration');
            $data['billing_details_id'] = $this->input->post('billing_details_id');
            $data['shipping_details_id'] = $this->input->post('shipping_details_id');
            $data['patment_terms'] = $this->input->post('patment_terms');
            $data['incoterms'] = $this->input->post('incoterms');
            $data['status'] = 'Active';

            if (!empty($this->input->post("bp_order_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_bp_order", $data, array("bp_order_id" => $this->input->post("bp_order_id")));

                $data1['bp_order_id'] = $this->input->post("bp_order_id");
                $resultdel = $this->common->deleteRecord('tbl_bp_order_sku', array("bp_order_id" => $this->input->post("bp_order_id")));
                if ($resultdel) {
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $data1['price'] = $this->input->post('sku_price')[$key];
                            $result1 = $this->common->insertData('tbl_bp_order_sku', $data1, '1');
                        }
                    }
                }

                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_bp_order", $data, "1");
                if ($result) {
                    $data1['bp_order_id'] = $result;
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $data1['price'] = $this->input->post('sku_price')[$key];
                            $result1 = $this->common->insertData('tbl_bp_order_sku', $data1, '1');
                        }
                    }
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }

    //business partner billing and shipping details dropdown
    function getBusinessPartner() {
        $bp_id = $_GET['bp_id'];
        $billing_id = $_GET['billing_id'];
        $shipping_id = $_GET['shipping_id'];
        $view = $_GET['isView'];
        $dropDownCss = '';
        if($view==1){
            $dropDownCss = "disabled"; 
        }

        $billingstr = "<select name='billing_details_id' id='billing_details_id' onchange='getBillAddress(this.value)' class='basic-single select-form-mro' $dropDownCss>"
                . "<option value=''>Select Billing Details</option>";
        $shippingstr = "<select name='shipping_details_id' id='shipping_details_id' onchange='getShipAddress(this.value)' class='basic-single select-form-mro' $dropDownCss>"
                . "<option value=''>Select Shipping Details</option>";
        $payment = '';
        $incoterm = '';
        if ($bp_id) {
            //billing details
            $billingDetail = $this->common->getData("tbl_billing_details", "*", array("business_partner_id" => $bp_id));
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    $selected = '';
                    $address = $value['contact_person'] . " (" . $value['address_line_1'] . ")";
                    if ($billing_id == $value['billing_details_id']) {
                        $selected = 'selected';
                    }
                    $billingstr = $billingstr . "<option value='" . $value['billing_details_id'] . "' $selected >" . $address . "</option>";
                }
            }
            $billingstr = $billingstr . "</select>";
            //shipping details
            $shippingDetail = $this->common->getData("tbl_shipping_details", "*", array("business_partner_id" => $bp_id));
            if (!empty($shippingDetail)) {
                foreach ($shippingDetail as $key => $value) {
                    $selected = '';
                    $address = $value['contact_person'] . " (" . $value['address_line_1'] . ")";
                    if ($shipping_id == $value['shipping_details_id']) {
                        $selected = 'selected';
                    }
                    $shippingstr = $shippingstr . "<option value='" . $value['shipping_details_id'] . "' $selected >" . $address . "</option>";
                }
            }
            $shippingstr = $shippingstr . "</select>";
            //business partner payment term value
            $bpDetail = $this->common->getData("tbl_business_partner", "payment_terms_id,incoterms_id", array("business_partner_id" => $bp_id));
            if (!empty($bpDetail[0]['payment_terms_id'])) {
                $paymentTermDetail = $this->common->getData("tbl_payment_term", "payment_term_value", array("payment_term_id" => $bpDetail[0]['payment_terms_id']));
                if (!empty($paymentTermDetail)) {
                    $payment = $paymentTermDetail[0]['payment_term_value'];
                }
            }
            //business partner icoterm value
            if (!empty($bpDetail[0]['incoterms_id'])) {
                $incotermDetail = $this->common->getData("tbl_incoterms", "incoterms_value", array("incoterms_id" => $bpDetail[0]['incoterms_id']));
                if (!empty($incotermDetail)) {
                    $incoterm = $incotermDetail[0]['incoterms_value'];
                }
            }
        }

        echo json_encode(array('billing' => $billingstr, 'shipping' => $shippingstr, 'payment' => $payment, 'incoterm' => $incoterm));
        exit;
    }

    //billing address details at click of billing details dropdown
    function getBillingAddress() {
        $bd_id = $_GET['bd_id'];
        $billingstr = "";
        if ($bd_id > 0) {
            $billingDetail = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $bd_id));
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    $countyStateCity = $this->getCountryStateCity($value['country_id'], $value['state_id'], $value['city_id']);
                    $address = $value['address_line_1'] . " " . $value['address_line_2'] . " " . $countyStateCity . " " . $value['zipcode'];
                    $billingstr = $billingstr . "<p class='bas-title'>Billing Address 11</p>
                          <p class='bas-text'>" . $address . "</p>
                          <p class='bas-title'>Billing Contact</p>
                          <p class='bas-text'>" . $value['contact_person'] . "</p> 
                          <p class='bas-title'>Billing Email</p>
                          <p class='bas-text'>" . $value['email'] . "</p>
                          <p class='bas-title'>Billing Phone</p>
                          <p class='bas-text'>" . $value['number'] . "";
                }
            }
        }
        echo json_encode(array('billing' => $billingstr));
        exit;
    }

    //shipping address details at click of shipping details dropdown
    function getShipAddress() {
        $bd_id = $_GET['bd_id'];
        $billingstr = "";
        if ($bd_id > 0) {
            $billingDetail = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $bd_id));
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    $countyStateCity = $this->getCountryStateCity($value['country_id'], $value['state_id'], $value['city_id']);
                    $address = $value['address_line_1'] . " " . $value['address_line_2'] . " " . $countyStateCity . " " . $value['zipcode'];
                    $billingstr = $billingstr . "<p class='bas-title'>Shipping Address</p>
                          <p class='bas-text'>" . $address . "</p>
                          <p class='bas-title'>Shipping Contact</p>
                          <p class='bas-text'>" . $value['contact_person'] . "</p> 
                          <p class='bas-title'>Shipping Email</p>
                          <p class='bas-text'>" . $value['email'] . "</p>
                          <p class='bas-title'>Shipping Phone</p>
                          <p class='bas-text'>" . $value['number'] . "";
                }
            }
        }

        echo json_encode(array('billing' => $billingstr));
        exit;
    }

    //country, state, city name
    function getCountryStateCity($countyId = 0, $stateID = 0, $cityID = 0) {
        $str = ',';
        $cityDetail = $this->common->getData("tbl_city", "city_name", array("city_id" => $cityID));
        if (!empty($cityDetail)) {
            $str = $str . $cityDetail[0]['city_name'] . ",";
        }
        $stateDetail = $this->common->getData("tbl_state", "state_name", array("state_id" => $stateID));
        if (!empty($stateDetail)) {
            $str = $str . " " . $stateDetail[0]['state_name'] . ",";
        }
        $countryDetail = $this->common->getData("tbl_country", "country_name", array("country_id" => $countyId));
        if (!empty($countryDetail)) {
            $str = $str . " " . $countryDetail[0]['country_name'];
        }
        return $str;
    }

    //business partner sku data
    function getskuDetails() {
        $sku_id = $_GET['sku_id'];
        $data = array();
        if ($sku_id > 0) {
            $skuDetail = $this->bpordermodel->getskuData($sku_id);
            if (!empty($skuDetail)) {
                $data = $skuDetail[0];
            }
        }
        echo json_encode(array('skuData' => $data));
        exit;
    }
    //payment term and incoterm dropdown
    function getPaymentIncoterm() {
        $cnt = $_GET['cnt'];
        $billingstr = '';
        $shippingstr = '';
        if ($cnt == 1) {
            $billingstr = "<select name='patment_terms' id='patment_terms' class='basic-single select-form-mro' style='width: 97%;'>"
                    . "<option value=''>Select Payment Terms</option>";
            //payment term data        
            $paymentTermDetails = $this->common->getData("tbl_payment_term", "payment_term_id,payment_term_value", "");
            if (!empty($paymentTermDetails)) {
                foreach ($paymentTermDetails as $key => $value) {
                    $billingstr = $billingstr . "<option value='" . $value['payment_term_value'] . "' >" . $value['payment_term_value'] . "</option>";
                }
            }
            $billingstr = $billingstr . "</select>";
        } else {
            $shippingstr = "<select name='incoterms' id='incoterms' class='basic-single select-form-mro' style='width: 97%;'>"
                    . "<option value=''>Select Incoterm</option>";
            //Incoterm data        
            $incotermDetails = $this->common->getData("tbl_incoterms", "incoterms_id,incoterms_value", "");
            if (!empty($incotermDetails)) {
                foreach ($incotermDetails as $key => $value) {
                    $shippingstr = $shippingstr . "<option value='" . $value['incoterms_value'] . "' >" . $value['incoterms_value'] . "</option>";
                }
            }
            $shippingstr = $shippingstr . "</select>";
        }

        echo json_encode(array('paymentTerm' => $billingstr, 'incoTerm' => $shippingstr));
        exit;
    }

}

?>
