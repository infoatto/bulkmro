<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Users extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('usersmodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("UsersList")){
            redirect('dashboard');
        }
    }

    function index() {
        $profileData = array();
        $user_id = $_SESSION['mro_session'][0]['user_id'];
        $result = $this->common->getData("tbl_users", "*", array("user_id" => $user_id));
        if (!empty($result)) {
            $profileData['user_details'] = $result[0];
        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('users/index', $profileData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("UsersAddEdit")){
            redirect('dashboard');
        }
        $user_id = "";
        $edit_datas = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $user_id = $url_prams['id'];
            $result = $this->common->getData("tbl_users", "*", array("user_id" => $user_id));
            if (!empty($result)) {
                $edit_datas['user_details'] = $result[0];
            }
        }
        //get all roles
        $edit_datas['roles_details'] = $this->common->getData("tbl_roles","*",array("status"=>"Active"));

        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('users/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        //code for get user data at list
        $get_result = $this->usersmodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->firstname) . " " . ucfirst($get_result['query_result'][$i]->lastname));
                array_push($temp, $get_result['query_result'][$i]->role_name);
                array_push($temp, $get_result['query_result'][$i]->email_id);
                array_push($temp, $get_result['query_result'][$i]->mobile_number);
                $status_class = ($get_result['query_result'][$i]->status == 'Active') ? 'status-active' : 'status-inactive';
                array_push($temp, "<span class='" . $status_class . " status-cls' onclick='changeStatusNew(`" . $get_result['query_result'][$i]->user_id . "`,`" . $get_result['query_result'][$i]->status . "`,this)'>" . $get_result['query_result'][$i]->status . "</span>");
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("UsersAddEdit")){    
                    $actionCol = '<a href="users/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->user_id), '+/', '-_'), '=') . '" title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "user_name = '" . $this->input->post('user_name') . "' ";
            if (!empty($this->input->post("user_id"))) {
                $condition .= " AND user_id <> " . $this->input->post("user_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_users", "user_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['user_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Username already exist...'));
                exit;
            }

            $data = array();
            $data['role_id'] = $this->input->post('role_id');
            $data['firstname'] = $this->input->post('firstname');
            $data['lastname'] = $this->input->post('lastname');
            $data['email_id'] = $this->input->post('email_id');
            $data['mobile_number'] = $this->input->post('mobile_number');
            $data['dob'] = !empty($this->input->post('dob')) ? date("Y-m-d", strtotime($this->input->post('dob'))) : NULL;
            $data['status'] = $this->input->post('status');
            $data['gender'] = $this->input->post('gender');
            $data['user_type'] = "Admin";
            //update password 
            $data['user_name'] = $this->input->post('user_name');
            $data['password'] = md5($this->input->post('password'));
            //profile image 
            if (!empty($_FILES['profile_picture'])) {
                $path = DOC_ROOT_FRONT . '/uploads/client_images/';
                if (!empty($this->input->post('existing_profile_image'))) {
                    $img_path = $path . "/" . $this->input->post('existing_profile_image');
                    if (is_file($img_path)) {
                        unlink($img_path);
                    }
                }
                $this->load->library('upload');
                $this->upload->initialize($this->set_upload_options());
                if (!$this->upload->do_upload('profile_picture')) {
                    $error = array('error' => $this->upload->display_errors());
                    echo json_encode(array('success' => false, 'msg' => $error));
                    exit;
                } else {
                    $file_data = array('upload_data' => $this->upload->data());
                    $data['profile_picture'] = $file_data['upload_data']['file_name'];
                }
            }
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            if (!empty($this->input->post("user_id"))) {
                //update user data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_users", $data, array("user_id" => $this->input->post("user_id")));
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert user data
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_users", $data, "1");
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }

            if ($result) {
                $_SESSION["mro_session"] = $this->common->getData("tbl_users", "*", array("user_id" => $_POST['user_id']));
                echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                exit;
            } else {
                echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                exit;
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }

    function deleteFile() {
        if (!empty($_POST['id']) && !empty($_POST['filename'])) {
            if (!empty($_POST['filename'])) {
                $path = DOC_ROOT_FRONT . '/uploads/client_images/';
                $img_path = $path . "/" . $_POST['filename'];
                if (is_file($img_path)) {
                    unlink($img_path);
                    $data = array();
                    $data['profile_picture'] = "";
                    $result = $this->common->updateData("tbl_users", $data, array("user_id" => $_POST['id']));
                    if ($result) {
                        echo json_encode(array('success' => true, 'msg' => 'File has been deleted successfully.'));
                        exit;
                    } else {
                        echo json_encode(array('success' => false, 'msg' => 'Some thing went wrong to delete this file.'));
                        exit;
                    }
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Some thing went wrong to delete this file.'));
            exit;
        }
    }

    function dataExist() {
        $condition = array("user_name" => $_POST['user_name']);
        if (!empty($_POST['user_id']) && $_POST['user_id'] != "") {
            $condition['user_id <>'] = $_POST['user_id'];
        }
        $result = $this->common->getData("tbl_users", "*", $condition);
        if ($result > 0) {
            echo json_encode(FALSE);
        } else {
            echo json_encode(TRUE);
        }
    }

    function set_upload_options($file_name = "") {
        //upload profile image in folder
        $config = array();
        if (!empty($file_name) && $file_name != "") {
            $config['file_name'] = $file_name;
        }
        $config['upload_path'] = DOC_ROOT_FRONT . "/uploads/client_images/";
        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0777, true);
        }
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '0';
        $config['overwrite'] = FALSE;
        return $config;
    }

}

?>
