<?php 
if($getsku){ ?>
    <div id="singleSkuDiv<?=$getsku[0]['sku_id']?>">
        <div class="form-group">
            <label for="">Item Code </label>
            <label for=""><?= ($getsku[0]['sku_number']) ?></label>
        </div>

        <div class="form-group">
            <label for="">Product Description</label>
            <label for=""><?= ($getsku[0]['product_name']) ?></label>
        </div>

        <div class="form-group">
            <label for="">Size </label>
            <label for=""><?= ($getsku[0]['size_name']) ?></label>
        </div>

        <input type="hidden" value="<?= $getsku[0]['sku_id'];?>" id="" name="sku[<?=$getsku[0]['sku_id']?>]">

        <div class="form-group">
            <label for="">Quantity</label>
            <input type="number" name="quantity[<?=$getsku[0]['sku_id']?>]"  value="" required id="quantity" placeholder="Enter quantity" class="input-form-mro">
        </div>

        <div class="form-group">
            <label for="">Price Per Unit</label>
            <input type="number" name="price_per_unit[<?=$getsku[0]['sku_id']?>]"  value="" required id="price_per_unit" placeholder="Enter price per unit" class="input-form-mro">
        </div>
        <div class="sit-single sku-remove">
            <button title="Remove" class="remove-sku"  onclick ="removeSku('<?= $getsku[0]['sku_id']?>')" type="button"><img src="<?= base_url('assets/images/remove-dark.svg');?>" alt="" class="add-more-img">&nbsp;&nbsp;Remove</button>
        </div>
    </div>
    <br>
<?php  } ?> 



    