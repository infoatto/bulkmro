<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class ListOfDocument extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('ListOfDocumentmodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("DocumentTypeList")){
            redirect('dashboard');
        }
    }

    function index() {
        $ListOfDocumentData = array();
        $result = $this->common->getData("tbl_category", "*", "");
        if (!empty($result)) {
            $categoryData['category_details'] = $result[0];
        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('index', $categoryData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("DocumentTypeAddEdit")){
            redirect('dashboard');
        }
        //add edit form
        $result = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $document_type_id = $url_prams['id'];
            
            $condition = "1=1  AND dt.document_type_id = '".$document_type_id."' ";
            $main_table = array("tbl_document_type as dt", array("dt.*"));
            $join_tables = array(
                array("left","tbl_sub_document_type as sdt","sdt.document_type_id = dt.document_type_id", array("sdt.sub_document_type_name,sdt.sub_document_type_id,sdt.sub_status,sdt.sub_document_prefix_name")),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,"",'');
            $result['document_type_details'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
           
            // echo "</pre>";
            // echo $this->db->last_query();
            // print_r($result);
            // exit;
            // $result['faq_section_details'] = $this->array_formation($faq_details,'faq_section_id');


        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('addEdit', $result);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        //get record at list
        $get_result = $this->ListOfDocumentmodel->getRecords($_GET);
        // echo "<pre>";
        // print_r($get_result);
        // exit;
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            $j=0;
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                ++$j;
                // array_push($temp, ucfirst($get_result['query_result'][$i]->document_type_squence));
                array_push($temp, $j);
                array_push($temp, ucfirst($get_result['query_result'][$i]->document_type_name)."<span class='squence' tr_row=".$get_result['query_result'][$i]->document_type_id."></span>");
                array_push($temp, ucfirst($get_result['query_result'][$i]->sub_document_type));
                array_push($temp, ucfirst($get_result['query_result'][$i]->status));
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("DocumentTypeAddEdit")){    
                    $actionCol = '<a href="listOfDocument/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->document_type_id), '+/', '-_'), '=') . '" title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        // echo "<pre>";
        // print_r($_POST);
        // exit;

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "document_type_name = '" . trim($this->input->post('document_type_name')) . "' ";
            if (!empty($this->input->post("document_type_id"))) {
                $condition .= " AND document_type_id <> " . $this->input->post("document_type_id");
            }

            // check for existance 
            $exiting = $this->common->getData("tbl_document_type","*",$condition);

            if (!empty($exiting[0]['document_type_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Document Type already exist...'));
                exit;
            }

            $data = array();
            $data['document_type_name'] = trim($this->input->post('document_type_name'));
            $data['document_prefix_name'] = trim(strtoupper($this->input->post('document_prefix_name')));
            $data['status'] = $this->input->post('status');
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['created_on'] = date("Y-m-d H:i:s");

            if (!empty($this->input->post("document_type_id"))) {
                //update data
                $condition = " document_type_id = '".$this->input->post("document_type_id")."' ";
                $result = $this->common->updateData("tbl_document_type", $data, $condition);
                if ($result) {
                     
                    if(!empty($_POST['sub_document_type_name']) && isset($_POST['sub_document_type_name'])){
                        foreach ($_POST['sub_document_type_name'] as $key => $value) {
                            $data =array();
                            $data['document_type_id'] = $this->input->post("document_type_id");
                            $data['sub_document_type_name'] = $value;
                            $data['sub_document_prefix_name'] = trim(strtoupper($_POST['sub_document_prefix_name'][$key]));
                            
                            $data['sub_status'] = $_POST['sub_status'][$key];
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $result = $this->common->insertData("tbl_sub_document_type", $data, "1");
                        }

                    }
                    if(!empty($_POST['sub_document_type_name_existing']) && isset($_POST['sub_document_type_name_existing'])){
                        foreach ($_POST['sub_document_type_name_existing'] as $key => $value) {
                            $data =array();
                            $data['document_type_id'] = $this->input->post("document_type_id");
                            $data['sub_document_type_name'] = $value;
                            $data['sub_document_prefix_name'] = trim(strtoupper($_POST['sub_document_prefix_name_existing'][$key]));
                            $data['sub_status'] = $_POST['sub_status_existing'][$key];
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $condition = "sub_document_type_id = '".$key."' ";
                            $result = $this->common->updateData("tbl_sub_document_type", $data, $condition);
                        }

                    }
                    
                    // forloop start here
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['document_type_name'] = trim($this->input->post('document_type_name'));
                $data['document_prefix_name'] = trim(strtoupper($this->input->post('document_type_name')));
                $data['status'] = $this->input->post('status');
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_document_type", $data, "1");
                if ($result) {
                     
                    $document_type_id = $result;
                    // forloop start here
                    if(!empty($_POST['sub_document_type_name']) && isset($_POST['sub_document_type_name'])){
                        foreach ($_POST['sub_document_type_name'] as $key => $value) {
                            $data =array();
                            $data['document_type_id'] = $document_type_id;
                            $data['sub_document_type_name'] = $value;
                            $data['sub_document_prefix_name'] = trim(strtoupper($_POST['sub_document_prefix_name'][$key]));

                            $data['sub_status'] = $_POST['sub_status'][$key];
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $result = $this->common->insertData("tbl_sub_document_type", $data, "1");
                        }

                    }
                    if($result){
                        echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                        exit;
                    }else{
                        echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                        exit;
                    }
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }

    
	public function changeorder()
	{
        // echo "<pre>";
        // print_r($_POST);
        // exit;
		$positions = $this->input->post('position');
		if($positions){
			$order_sequence = 1;
			foreach ($positions as $key => $value) {
				$data = array();
				$data['document_type_squence'] = $order_sequence;
				$condition = "document_type_id = '".$value."' ";
				$result = $this->common->updateData("tbl_document_type",$data,$condition);	
				$order_sequence++;
			}
			echo json_encode(array("success"=>true, 'msg'=>'position updated'));
			$this->session->set_flashdata('message-ordering', '<strong>Success!</strong> position updated.');
			exit;
		}else{
			echo json_encode(array("success"=>false, 'msg'=>'No themes left for re-ordering'));
			exit;
		}
	}

    function deleteDocumentType(){

        $Result = $this->common->deleteRecord('tbl_sub_document_type', 'sub_document_type_id = '.$_POST['id']);
		if($Result)
		{
			echo json_encode(array("success" => "1", "msg" => "deleted successfully.", "body" => NULL));
			exit;
		}else{
			echo json_encode(array("success" => "0", "msg" => "Delete Failed", "body" => NULL));
			exit;
		}	
    }

}

?>
