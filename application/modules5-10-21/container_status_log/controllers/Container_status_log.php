<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Container_status_log extends CI_Controller
{
	function __construct(){
            parent::__construct();
            $this->load->model('container_status_logmodel','',TRUE);
            $this->load->model('common_model/common_model','common',TRUE);
            checklogin();
            if(!$this->privilegeduser->hasPrivilege("ContainerStatusLog")){
                redirect('dashboard');
            }
	}

	function index(){
            $containerData['container_data']  = array(); 
            $containerData['container_status']  = array(); 
            $result = $this->common->getData("tbl_container","container_id,container_number","");
            if(!empty($result)){
                $containerData['container_data'] = $result;
            } 
            
            $result1 = $this->common->getData("tbl_container_status","container_status_id,container_status_name","");
            if(!empty($result1)){
                $containerData['container_status'] = $result1;
            } 
            
            
            $this->load->view('template/head.php');
            $this->load->view('template/navigation.php'); 
            $this->load->view('container_status_log/index',$containerData);
            $this->load->view('template/footer.php');                
            $this->load->view('template/footer-scripts.php'); 
	} 

	function fetch(){  
            $get_result = $this->container_status_logmodel->getRecords($_GET);  
            $result = array();
            $result["sEcho"]= $_GET['sEcho'];
            $result["iTotalRecords"] = $get_result['totalRecords'];	//iTotalRecords get no of total recors
            $result["iTotalDisplayRecords"]= $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
            $items = array();
            if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0){
                for($i=0;$i<sizeof($get_result['query_result']);$i++){
                    $temp = array();
                    array_push($temp, $get_result['query_result'][$i]->container_number);
                    array_push($temp, $get_result['query_result'][$i]->container_status_name); 
                    array_push($temp, date('d/m/y h:s:A', strtotime($get_result['query_result'][$i]->created_on))); 
                    array_push($temp, $get_result['query_result'][$i]->firstname .' '. $get_result['query_result'][$i]->lastname);
                    array_push($items, $temp);
                }
            }
            $result["aaData"] = $items;
            echo json_encode($result);
            exit;
	}
        
}

?>
