<div class="login-wrapper"> 
    <div class="login-content">
        <div class="login-logo"><img src="assets/images/BulkMRO_logo.svg" alt=""></div>
        <div class="login-form">
            <form action="" id="loginForm" method="post" enctype="multipart/form-data">
                <h3 class="form-group-title">Login</h3>
                <div class="form-group">
                    <label for="username">Username<sup>*</sup></label>
                    <input type="text" id="username" name="username" placeholder="Enter username" class="input-form-mro">  
                </div>
                <div class="form-group">
                    <label for="password">Password<sup>*</sup></label>
                    <input type="password" id="password" name="password" placeholder="Enter password" class="input-form-mro">
                </div>
                <div>
                    <button type="submit" class="btn-primary-mro w100 btn btn-primary btn-rounded btn-login">Sign In</button>
                </div>
                <div class="login-bottom-links">
                    <a href="#" class="fp-link link-mro">Forgot Password?</a>
                </div>
            </form>
        </div>
        <div class="fp-form">
            <form>
                <h3 class="form-group-title">Forgot Password?</h3>
                <div class="form-group">
                    <label for="">Enter Email ID<sup>*</sup></label>
                    <input type="text" placeholder="Enter email ID" class="input-form-mro">
                </div>        
                <div>
                    <button type="submit" class="btn-primary-mro w100">Reset Password</button>
                </div>
                <div class="login-bottom-links">
                    <a href="#" class="login-link link-mro">Back to Login</a>
                </div>
            </form>
        </div>
    </div>

</div> 
<script>
    $(document).ready(function () {
        // login form validate and submit
        $("#loginForm").validate({
            rules: {
                "username": {required: true},
                "password": {required: true}
            },
            messages: {
                "username": {required: "Please, enter Username"},
                "password": {required: "Please, enter Password"}
            }, submitHandler: function (form) {
                var act = "<?php echo base_url(); ?>login/loginValidate";
                $(".login-error-msg").hide();
                $(".login-success-msg").hide();
                $("#loginForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    success: function (response) {
                        if (response.success) {
                            $(".login-success-msg").html(response.msg + "<br><br>");
                            $(".login-error-msg").hide();
                            $(".login-success-msg").show();
                            setTimeout(function () {
                                window.location = "<?php echo base_url(); ?>";
                            }, 1000);
                        } else {
                            $(".login-error-msg").html(response.msg + "<br><br>");
                            $(".login-error-msg").show();
                            return false;
                        }
                    }
                });
                return false;
            }
        });  
    });
</script>