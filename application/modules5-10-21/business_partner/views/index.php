<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard") ?>">Home</a>
                            <span>></span>
                            <p>Dashboard</p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title"><a href="<?php echo base_url("business_partner"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Business Partners </h1>
                        </div>
                    </div>
                    <div class="title-sec-right">
                        <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")) { ?>
                            <a href="<?php echo base_url("bporder/addEdit"); ?>" class="btn-secondary-mro"><img src="assets/images/add-icon-dark.svg" alt=""> New Business Partner Order</a>
                        <?php } ?>
                        <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersAddEdit")) { ?>
                            <a href="<?php echo base_url("business_partner/addEdit"); ?>" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> Add Business Partner</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="page-content-wrapper1">
                    <div id="serchfilter" class="filter-sec-wrapper">
                        <div class="filter-search dataTables_filter searchFilterClass">
                            <input type="text" id="sSearch_0" name="sSearch_0" class="searchInput filter-search-input" placeholder="Business Name">
                        </div>
                        <div class="dataTables_filter searchFilterClass">
                            <select name="sSearch_1" id="sSearch_1" class="searchInput basic-single select-mro select-xl">
                                <option value="">Business Partner Type</option>
                                <option value="Customer">Customer</option>
                                <option value="Vendor">Vendor</option>
                            </select>
                        </div>
                        <div class="dataTables_filter searchFilterClass">
                            <select name="sSearch_2" id="sSearch_2" class="searchInput basic-single select-mro select-xl">
                                <option value="">Business Partner Category</option>
                                <option value="Government">Government</option>
                                <option value="Corporate">Corporate</option>
                                <option value="Distributor">Distributor</option>
                                <option value="Supplier">Supplier</option>
                                <option value="Freight Forwarder">Freight Forwarder</option>
                                <option value="Customs Broker">Customs Broker</option>
                                <option value="Inspection Agent">Inspection Agent</option>
                                <option value="Manufacturer">Manufacturer</option>
                                <option value="Distributor">Distributor</option>
                                <option value="Commission Agent">Commission Agent</option>
                            </select>
                        </div>
                        <div class="form-group clear-search-filter">
                            <button class="btn-primary-mro" onclick="clearSearchFilters();">Clear Search</button>
                        </div>
                    </div>
                </div>
                <div class="bp-list-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Business Name</th>
                                    <th>Partner Type</th>
                                    <th>Partner Category</th>
                                    <th>Contact Person</th>
                                    <th>Phone Number</th>
                                    <th>Email Address</th>
                                    <th class="table-action-cls" style="width:84px !important;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>