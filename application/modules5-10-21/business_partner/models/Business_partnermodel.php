<?PHP

class Business_partnermodel extends CI_Model {

    function getRecords($get) {
        $table = "tbl_business_partner";
        $table_id = 'business_partner_id';
        $default_sort_column = 'i.business_partner_id';
        $default_sort_order = 'desc';
        $condition = "1=1 ";
        $colArray = array('i.business_name', 'i.business_type', 'i.business_category');
        $sortArray = array('i.business_name', 'i.business_type', 'i.business_category', 'i.contact_person', 'i.phone_number', 'i.email');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

        for ($i = 0; $i < count($colArray); $i++) {
            if (isset($_GET['Searchkey_' . $i]) && $_GET['Searchkey_' . $i] != "") {
                $condition .= " AND " . $colArray[$i] . " LIKE '%" . $_GET['Searchkey_' . $i] . "%' ";
            }
        }

        $this->db->select('i.*');
        $this->db->from("$table as i");
        //$this -> db -> join("tbl_roles as r","r.role_id = i.role_id");
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page);

        $query = $this->db->get();
        //echo $this->db->last_query();exit;


        $this->db->select('i.*');
        $this->db->from("$table as i");
        //$this -> db -> join("tbl_roles as r","r.role_id = i.role_id");
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);

        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    }
    
    function deleteRecord($tbl_name,$business_partner_id,$detailsColumn,$detailsId){
        $condition = " business_partner_id=$business_partner_id  AND  $detailsColumn <> $detailsId ";
        $this->db->where("($condition)");
        if($this->db->delete($tbl_name)){
            return true;
        }else{
            return false;
        }
    }

}

?>
