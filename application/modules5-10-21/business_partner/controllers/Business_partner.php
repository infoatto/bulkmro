<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Business_partner extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('business_partnermodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersList")){
            redirect('dashboard');
        }
    }

    function index() {
        $data = array();
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('business_partner/index', $data);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersAddEdit")){
            redirect('dashboard');
        }
        //add edit form
        $business_partner_id = "0";
        $edit_datas = array();
        $edit_datas['billingCountry'] = array();
        $edit_datas['billingState'] = array();
        $edit_datas['billingCity'] = array();
        $edit_datas['paymentTermData'] = array();
        $edit_datas['incotermData'] = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $business_partner_id = $url_prams['id'];
            //business partner get data in case of edit
            $result = $this->common->getData("tbl_business_partner", "*", array("business_partner_id" => $business_partner_id));
            if (!empty($result)) {
                $edit_datas['business_partner_details'] = $result[0];
                //billing details get data in case of edit
                $billingDetail = $this->common->getData("tbl_billing_details", "*", array("business_partner_id" => $business_partner_id));
                if (!empty($billingDetail)) {
                    $edit_datas['billing_details'] = $billingDetail;
                }
                //shipping details get data in case of edit
                $shippingDetail = $this->common->getData("tbl_shipping_details", "*", array("business_partner_id" => $business_partner_id));
                if (!empty($shippingDetail)) {
                    $edit_datas['shippingDetail'] = $shippingDetail;
                }
            }
        }
        //country data
        $country = $this->common->getData("tbl_country", "country_id,country_name", "");
        if (!empty($country)) {
            $edit_datas['country'] = $country;
        }
        //state data
        $billingState = $this->common->getData("tbl_state", "state_id,state_name,country_id", "");
        if (!empty($billingState)) {
            $edit_datas['billingState'] = $billingState;
        }
        //city data
        $billingCity = $this->common->getData("tbl_city", "city_id,city_name,state_id", "");
        if (!empty($billingCity)) {
            $edit_datas['billingCity'] = $billingCity;
        }
        //payment term data
        $paymentTermData = $this->common->getData("tbl_payment_term", "payment_term_id,payment_term_value", "");
        if (!empty($paymentTermData)) {
            $edit_datas['paymentTermData'] = $paymentTermData;
        }
        //incoterm data
        $incotermData = $this->common->getData("tbl_incoterms", "incoterms_id,incoterms_value", "");
        if (!empty($incotermData)) {
            $edit_datas['incotermData'] = $incotermData;
        } 
        $edit_datas['view'] = isset($_GET['view'])?$_GET['view']:0; 
        
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('business_partner/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        //get list data
        $get_result = $this->business_partnermodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->business_name));
                array_push($temp, $get_result['query_result'][$i]->business_type);
                array_push($temp, $get_result['query_result'][$i]->business_category);
                array_push($temp, $get_result['query_result'][$i]->contact_person);
                array_push($temp, $get_result['query_result'][$i]->phone_number);
                array_push($temp, $get_result['query_result'][$i]->email);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("BusinessPartnersAddEdit")){   
                    $actionCol = '<a href="business_partner/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->business_partner_id), '+/', '-_'), '=') . '" title="Edit" class="text-center"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                } 
                if($this->privilegeduser->hasPrivilege("BusinessPartnersView")){   
                    $actionCol .= '<a href="business_partner/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->business_partner_id), '+/', '-_'), '=') . '&view=1" title="Edit" class="text-center" style="float:right;"><img src="' . base_url() . 'assets/images/view-btn-icon.svg" alt="View" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "business_name = '" . $this->input->post('business_name') . "' ";
            if (!empty($this->input->post("business_partner_id"))) {
                $condition .= " AND business_partner_id <> " . $this->input->post("business_partner_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['business_partner_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Business Name already exist...'));
                exit;
            }

            $data = array();
            $data['business_name'] = $this->input->post('business_name');
            $data['business_type'] = $this->input->post('business_type');
            $data['business_category'] = $this->input->post('business_category');
            $data['contact_person'] = $this->input->post('contact_person');
            $data['email'] = $this->input->post('email');
            $data['phone_number'] = $this->input->post('phone_number');
            $data['alias'] = $this->input->post('alias');
            $data['payment_terms_id'] = $this->input->post('payment_terms_id');
            $data['incoterms_id'] = $this->input->post('incoterms_id');
            $data['status'] = 'Active';

            if (!empty($this->input->post("business_partner_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_business_partner", $data, array("business_partner_id" => $this->input->post("business_partner_id")));

                $data1['business_partner_id'] = $this->input->post("business_partner_id"); 
                foreach ($this->input->post('billing_person') as $key => $value) {
                    $data1['contact_person'] = $this->input->post('billing_person')[$key];
                    $data1['email'] = $this->input->post('billing_email')[$key];
                    $data1['number'] = $this->input->post('billing_phone')[$key];
                    $data1['address_line_1'] = $this->input->post('billing_line_one')[$key];
                    $data1['address_line_2'] = $this->input->post('billing_line_two')[$key];
                    $data1['country_id'] = $this->input->post('billing_country')[$key];
                    $data1['state_id'] = $this->input->post('billing_state')[$key];
                    $data1['city_id'] = $this->input->post('billing_city')[$key];
                    $data1['zipcode'] = $this->input->post('billing_zipcode')[$key];
                    //check billing details data
                    $billingDetailsData = $this->common->getData("tbl_billing_details", "billing_details_id", array("business_partner_id" => $this->input->post("business_partner_id"),"billing_details_id" => (int)$this->input->post("billing_details_id")[$key]));
                    if (!empty($billingDetailsData)) {
                        //update if exits
                        $result1 = $this->common->updateData('tbl_billing_details', $data1, array("business_partner_id" => $this->input->post("business_partner_id"),"billing_details_id" => (int)$this->input->post("billing_details_id")[$key])); 
                    }else{ 
                        //insert if not exits
                        if(!empty($this->input->post('billing_person')[$key])){
                            $result1 = $this->common->insertData('tbl_billing_details', $data1, '1');
                        } 
                    } 
                } 

                $data2['business_partner_id'] = $this->input->post("business_partner_id");                
                foreach ($this->input->post('shipping_person') as $key => $value) {
                    $data2['contact_person'] = $this->input->post('shipping_person')[$key];
                    $data2['email'] = $this->input->post('shipping_email')[$key];
                    $data2['number'] = $this->input->post('shipping_phone')[$key];
                    $data2['address_line_1'] = $this->input->post('shipping_line_one')[$key];
                    $data2['address_line_2'] = $this->input->post('shipping_line_two')[$key];
                    $data2['country_id'] = $this->input->post('shipping_country')[$key];
                    $data2['state_id'] = $this->input->post('shipping_state')[$key];
                    $data2['city_id'] = $this->input->post('shipping_city')[$key];
                    $data2['zipcode'] = $this->input->post('shipping_zipcode')[$key]; 
                    
                    //check shipping details data
                    $shippingDetailsData = $this->common->getData("tbl_shipping_details", "shipping_details_id", array("business_partner_id" => $this->input->post("business_partner_id"),"shipping_details_id" => (int)$this->input->post("shipping_details_id")[$key]));
                    if (!empty($shippingDetailsData)) {
                        //update if exits
                        $result2 = $this->common->updateData('tbl_shipping_details', $data2, array("business_partner_id" => $this->input->post("business_partner_id"),"shipping_details_id" => (int)$this->input->post("shipping_details_id")[$key])); 
                    }else{ 
                        //insert if not exits
                        if(!empty($this->input->post('shipping_person')[$key])){
                            $result2 = $this->common->insertData('tbl_shipping_details', $data2, '1');
                        } 
                    } 
                } 

                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_business_partner", $data, "1");

                if ($result) {
                    //insert billing details
                    $data1['business_partner_id'] = $result;
                    if (!empty($this->input->post('billing_person'))) {
                        foreach ($this->input->post('billing_person') as $key => $value) {
                            $data1['contact_person'] = $value;
                            $data1['email'] = $this->input->post('billing_email')[$key];
                            $data1['number'] = $this->input->post('billing_phone')[$key];
                            $data1['address_line_1'] = $this->input->post('billing_line_one')[$key];
                            $data1['address_line_2'] = $this->input->post('billing_line_two')[$key];
                            $data1['country_id'] = $this->input->post('billing_country')[$key];
                            $data1['state_id'] = $this->input->post('billing_state')[$key];
                            $data1['city_id'] = $this->input->post('billing_city')[$key];
                            $data1['zipcode'] = $this->input->post('billing_zipcode')[$key];
                            $result1 = $this->common->insertData('tbl_billing_details', $data1, '1');
                        }
                    }
                    //insert shipping details
                    $data2['business_partner_id'] = $result;
                    if (!empty($this->input->post('shipping_person'))) {
                        foreach ($this->input->post('shipping_person') as $key => $value) {
                            $data2['contact_person'] = $value;
                            $data2['email'] = $this->input->post('shipping_email')[$key];
                            $data2['number'] = $this->input->post('shipping_phone')[$key];
                            $data2['address_line_1'] = $this->input->post('shipping_line_one')[$key];
                            $data2['address_line_2'] = $this->input->post('shipping_line_two')[$key];
                            $data2['country_id'] = $this->input->post('shipping_country')[$key];
                            $data2['state_id'] = $this->input->post('shipping_state')[$key];
                            $data2['city_id'] = $this->input->post('shipping_city')[$key];
                            $data2['zipcode'] = $this->input->post('shipping_zipcode')[$key];
                            $result2 = $this->common->insertData('tbl_shipping_details', $data2, '1');
                        }
                    }

                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }

    function getBusinessPartner() {
        //business partner dropdown at click of business type
        $businessType = $_GET['businessType'];
        $view = $_GET['isView'];
        $dropDownCss = '';
        if($view==1){
            $dropDownCss = "disabled"; 
        }
        
        $str = '';
        $selected = 'selected';
        if ($businessType == 'Customer') {
            $str = "<select name='business_category' id='business_category' class='basic-single select-form-mro' $dropDownCss>"
                    . "<option value=''>Select Business Partner Category</option>"
                    . "<option value='Government'>Government</option>"
                    . "<option value='Corporate'>Corporate</option>"
                    . "<option value='Distributor'>Distributor</option></select>";
        } else {
            $str = "<select name='business_category' id='business_category' class='basic-single select-form-mro' $dropDownCss>"
                    . "<option value=''>Select Business Partner Category</option>"
                    . "<option value='Supplier'>Supplier</option>"
                    . "<option value='Freight Forwarder'>Freight Forwarder</option>"
                    . "<option value='Customs Broker'>Customs Broker</option>"
                    . "<option value='Inspection Agent'>Inspection Agent</option>"
                    . "<option value='Manufacturer'>Manufacturer</option>"
                    . "<option value='Distributor'>Distributor</option>"
                    . "<option value='Commission Agent'>Commission Agent</option></select>";
        }
        echo json_encode(array('dropdown' => $str));
        exit;
    }

    function getState() {
        //get state dropdown
        $country_id = $_GET['country_id'];
        $idname = $_GET['idname'];
        $selected_id = $_GET['selected_id'];
        $cnt = $_GET['cnt'];
        $is_bill = $_GET['is_bill'];
        $stateDetail = array();
        $data = $this->common->getData("tbl_state", "state_id,state_name", array("country_id" => (int) $country_id));
        $statestr = '<select name="' . $idname . '" id="' . $idname . '" class="basic-single select-form-mro"  onchange="getCity(this.value,' . $cnt . ',' . $is_bill . ')" >'
                . '<option value="">Select State</option>';
        if ($country_id > 0) {
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    if (!empty($selected_id) && $selected_id == $value['state_id']) {
                        $srt = "selected";
                    } else {
                        $srt = '';
                    }
                    $statestr = $statestr . '<option value="' . $value['state_id'] . '"  ' . $srt . ' >' . str_replace("'", "", $value['state_name']) . '</option>';
                }
            }
            $statestr = $statestr . '</select>';
        }
        echo json_encode(array('state' => $statestr));
        exit;
    }

    function getCity() {
        //get city dropdown
        $state_id = $_GET['state_id'];
        $idname = $_GET['idname'];
        $selected_id = $_GET['selected_id'];
        $cnt = $_GET['cnt'];
        $stateDetail = array();
        $data = $this->common->getData("tbl_city", "city_id,city_name", array("state_id" => (int) $state_id));
        $statestr = '<select name="' . $idname . '" id="' . $idname . '" class="basic-single select-form-mro">'
                . '<option value="">Select City</option>';
        if ($state_id > 0) {
            if (!empty($data)) {
                foreach ($data as $key => $value) {
                    if (!empty($selected_id) && $selected_id == $value['city_id']) {
                        $srt = "selected";
                    } else {
                        $srt = '';
                    }
                    $statestr = $statestr . '<option value="' . $value['city_id'] . '"  ' . $srt . ' >' . str_replace("'", "", $value['city_name']) . '</option>';
                }
            }
            $statestr = $statestr . '</select>';
        }
        echo json_encode(array('city' => $statestr));
        exit;
    }

}

?>
