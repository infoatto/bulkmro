<?PHP
class Inventory_summarymodel extends CI_Model
{
	function getRecords($get){
            //echo '<pre>'; print_r($get);die;
            $table = "tbl_size"; 
            $default_sort_column = 'c.size_id';
            $default_sort_order = 'desc';
            $condition = "1=1 "; 

            $colArray = array('c.size_name','c.status');
            $sortArray = array('c.size_name','c.status');

            $page = $get['iDisplayStart'];	// iDisplayStart starting offset of limit funciton
            $rows = $get['iDisplayLength'];	// iDisplayLength no of records from the offset

            // sort order by column
            $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
            $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

            for($i=0;$i<count($colArray);$i++){ 
                if(isset($_GET['Searchkey_'.$i]) && $_GET['Searchkey_'.$i] !=""){
                        $condition .= " AND ".$colArray[$i]." LIKE '%".$_GET['Searchkey_'.$i]."%' ";
                }  
            }

            $this -> db -> select('c.*');
            $this -> db -> from("$table as c"); 
            $this->db->where("($condition)");
            $this->db->order_by($sort, $order);
            $this->db->limit($rows,$page); 
            $query = $this -> db -> get();
            //echo $this->db->last_query();exit; 

            $this -> db -> select('c.*');
            $this -> db -> from("$table as c");
            $this->db->where("($condition)");
            $this->db->order_by($sort, $order); 
            $query1 = $this -> db -> get();

            if($query -> num_rows() > 0){
                $totcount = $query1 -> num_rows();
                return array("query_result" => $query->result(), "totalRecords" => $totcount);
            }else{
                return array("totalRecords" => 0);
            }
	}
        
        function getProductDetails($container_id = 0) {
            $condition = " c.container_id = " . $container_id . " ";
            $this->db->select('c.*,sku.product_name,sku.sku_number,b.brand_name,s.size_name,u.uom_name,bp.business_name');
            $this->db->from("tbl_container_sku as c");
            $this->db->join("tbl_sku as sku", "c.sku_id = sku.sku_id");
            $this->db->join("tbl_brand as b", "b.brand_id = sku.brand_id");
            $this->db->join("tbl_size as s", "s.size_id = sku.size_id");
            $this->db->join("tbl_uom as u", "u.uom_id = sku.uom_id");
            $this->db->join("tbl_business_partner as bp", "bp.business_partner_id = sku.manufacturer_id"); 
            $this->db->where("($condition)");
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getAddressDetails($container_id = 0) {
            $condition = " c.container_id = " . $container_id . " ";
            $this->db->select('o.customer_contract_id,o.supplier_contract_id,o.customer_billing_details_id,o.customer_shipping_details_id,o.supplier_billing_details_id,o.supplier_shipping_details_id');
            $this->db->from("tbl_container as c");
            $this->db->join("tbl_order as o", "o.order_id = c.order_id"); 
            $this->db->where("($condition)");
            $query = $this->db->get(); 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
}
?>
