<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//session_start(); //we need to call PHP's session object to access it through CI
class Permission extends CI_Controller
{
	function __construct(){
            parent::__construct();
            $this->load->model('permissionmodel','',TRUE);
            $this->load->model('common_model/common_model','common',TRUE);
            checklogin();
            if(!$this->privilegeduser->hasPrivilege("PermissionList")){
                redirect('dashboard');
            }
	}

	function index(){
            $permissionData = array(); 
            $this->load->view('template/head.php');
            $this->load->view('template/navigation.php'); 
            $this->load->view('permission/index',$permissionData);
            $this->load->view('template/footer.php');                
            $this->load->view('template/footer-scripts.php'); 
	}

	function addEdit(){ 
            if(!$this->privilegeduser->hasPrivilege("PermissionAddEdit")){
                redirect('dashboard');
            }
            $edit_datas = array();
            if(!empty($_GET['text']) && isset($_GET['text'])){
                $varr=base64_decode(strtr($_GET['text'], '-_', '+/'));
                parse_str($varr,$url_prams);
                $perm_id = $url_prams['id'];
                $result = $this->common->getData("tbl_permissions","*",array("perm_id"=>$perm_id));
                if(!empty($result)){
                    $edit_datas['permission_details'] = $result[0];
                } 
            }
            $this->load->view('template/head.php');
            $this->load->view('template/navigation.php'); 
            $this->load->view('permission/addEdit',$edit_datas);
            $this->load->view('template/footer.php');                
            $this->load->view('template/footer-scripts.php'); 
	}

	function fetch(){  
            $get_result = $this->permissionmodel->getRecords($_GET); 
            $result = array();
            $result["sEcho"]= $_GET['sEcho'];
            $result["iTotalRecords"] = $get_result['totalRecords'];	//iTotalRecords get no of total recors
            $result["iTotalDisplayRecords"]= $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
            $items = array();
            if(!empty($get_result['query_result']) && count($get_result['query_result']) > 0){
                for($i=0;$i<sizeof($get_result['query_result']);$i++){
                    $temp = array();
                    array_push($temp, ucfirst($get_result['query_result'][$i]->perm_desc)); 
                    $actionCol = '';
                    if($this->privilegeduser->hasPrivilege("PermissionAddEdit")){
                        $actionCol ='<a href="permission/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->perm_id), '+/', '-_'), '=').'" title="Edit" class="text-center" style="float:left;width:100%;"><img src="'.base_url().'assets/images/edit-icon.svg" alt="Edit" ></a>';
                    }
                    array_push($temp, $actionCol); 
                    array_push($items, $temp);
                }
            }
            $result["aaData"] = $items;
            echo json_encode($result);
            exit;
	} 

	function submitForm(){ 
            if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
                /*check duplicate entry*/
                $condition = "perm_desc = '".$this->input->post('perm_desc')."' ";
                if(!empty($this->input->post("perm_id"))){
                    $condition .= " AND perm_id <> ".$this->input->post("perm_id");
                }

                $chk_client_sql = $this->common->Fetch("tbl_permissions","perm_id",$condition);
                $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

                if(!empty($rs_client[0]['perm_id'])){
                    echo json_encode(array('success'=>false, 'msg'=>'Permission already exist...'));
                    exit;
                }

                $data = array();
                $data['perm_desc'] = $this->input->post('perm_desc'); 
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                if(!empty($this->input->post("perm_id"))){
                    $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['updated_on'] = date("Y-m-d H:i:s");
                    $result = $this->common->updateData("tbl_permissions",$data,array("perm_id"=>$this->input->post("perm_id")));
                    if($result){
                        echo json_encode(array('success'=>true, 'msg'=>'Record Updated Successfully.'));
                        exit;
                    }else{
                        echo json_encode(array('success'=>false, 'msg'=>'Problem while Updating data.'));
                        exit;
                    }
                }else{ 
                    $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['created_on'] = date("Y-m-d H:i:s");
                    $result = $this->common->insertData("tbl_permissions",$data,"1");
                    if($result){
                        echo json_encode(array('success'=>true, 'msg'=>'Record Inserted Successfully.'));
                        exit;
                    }else{
                        echo json_encode(array('success'=>false, 'msg'=>'Problem while Inserting data.'));
                        exit;
                    }
                }  
            }else{
                echo json_encode(array('success'=>false, 'msg'=>'Problem while add/edit data.'));
                exit;
            }
	}  

}

?>
