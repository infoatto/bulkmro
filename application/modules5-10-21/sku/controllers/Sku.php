<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Sku extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('skumodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("SKUList")){
            redirect('dashboard');
        }
    }

    function index() {
        $data = array();
        $result = $this->common->getData("tbl_sku", "*", "");
        if (!empty($result)) {
            $data['sku_details'] = $result[0];
        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('sku/index', $data);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("SKUAddEdit")){
            redirect('dashboard');
        }
        //add edit form
        $sku_id = "";
        $edit_datas = array();
        $edit_datas['categoryData'] = array();
        $edit_datas['brandData'] = array();
        $edit_datas['businessPartnerData'] = array();
        $edit_datas['sizeData'] = array();
        $edit_datas['uomData'] = array();

        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $sku_id = $url_prams['id'];
            $result = $this->common->getData("tbl_sku", "*", array("sku_id" => $sku_id));
            if (!empty($result)) {
                $edit_datas['sku_details'] = $result[0];
            }
        }
        //get category data
        $categoryData = $this->common->getData("tbl_category", "category_id,category_name", "");
        if (!empty($categoryData)) {
            $edit_datas['categoryData'] = $categoryData;
        }
        //get brand data
        $brandData = $this->common->getData("tbl_brand", "brand_id,brand_name", "");
        if (!empty($brandData)) {
            $edit_datas['brandData'] = $brandData;
        }
        //get business partner data
        $businessPartnerData = $this->common->getData("tbl_business_partner", "business_partner_id,business_name", array("business_type" => "Vendor", "business_category" => "Manufacturer"));
        if (!empty($businessPartnerData)) {
            $edit_datas['businessPartnerData'] = $businessPartnerData;
        }
        //get size data
        $sizeData = $this->common->getData("tbl_size", "size_id,size_name", "");
        if (!empty($sizeData)) {
            $edit_datas['sizeData'] = $sizeData;
        }
        //get uom data
        $uomData = $this->common->getData("tbl_uom", "uom_id,uom_name", "");
        if (!empty($uomData)) {
            $edit_datas['uomData'] = $uomData;
        }

        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('sku/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        $get_result = $this->skumodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->sku_number));
                array_push($temp, $get_result['query_result'][$i]->sku_vendor);
                array_push($temp, $get_result['query_result'][$i]->product_name);
                array_push($temp, $get_result['query_result'][$i]->status);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("SKUAddEdit")){    
                    $actionCol = '<a href="sku/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->sku_id), '+/', '-_'), '=') . '" title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "sku_number = '" . $this->input->post('sku_number') . "' ";
            if (!empty($this->input->post("sku_id"))) {
                $condition .= " AND sku_id <> " . $this->input->post("sku_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['sku_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'BM SKU Number already exist...'));
                exit;
            }

            $data = array();
            $data['sku_number'] = $this->input->post('sku_number');
            $data['sku_vendor'] = $this->input->post('sku_vendor');
            $data['category_id'] = $this->input->post('category_id');
            $data['brand_id'] = $this->input->post('brand_id');
            $data['manufacturer_id'] = $this->input->post('manufacturer_id');
            $data['product_name'] = $this->input->post('product_name');
            $data['size_id'] = $this->input->post('size_id');
            $data['uom_id'] = $this->input->post('uom_id');
            $data['status'] = $this->input->post('status');

            if (!empty($this->input->post("sku_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_sku", $data, array("sku_id" => $this->input->post("sku_id")));
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_sku", $data, "1");
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }

}

?>
