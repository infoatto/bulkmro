<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class DocumentMaster extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('documentMastermodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("DocumentMasterList")){
            redirect('dashboard');
        }
    }

    function index() {
        $DocumentMasterData = array();
        $result = $this->common->getData("tbl_category", "*", "");
        if (!empty($result)) {
            $categoryData['category_details'] = $result[0];
        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('index', $categoryData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("DocumentMasterAddEdit")){
            redirect('dashboard');
        }
        //add edit form
        $result = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $document_type_id = $url_prams['id'];

            $condition = "1=1  AND document_type_id = '".$document_type_id."' ";

            if(!empty($_GET['subDoc']) && isset($_GET['subDoc'])) {
                $varr = base64_decode(strtr($_GET['subDoc'], '-_', '+/'));
                parse_str($varr, $url_prams);
                $sub_document_type_id = $url_prams['id'];

            }
            
            $condition .=" && sub_document_type_id = '".$sub_document_type_id."'";
            $result['custom_field'] = $this->common->getData("tbl_custom_field_structure","*",$condition);
        }
        // echo $this->db->last_query();
        $condition =" status ='Active' ";
        $result['document_type'] = $this->common->getData("tbl_document_type","*",$condition);

        // echo "<pre>";
        // echo $this->db->last_query();
        // print_r($result);
        // exit;

        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('addEdit', $result);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function getSubdocumentdiv(){   
        $condition =" sub_status ='Active' AND document_type_id = '".$_POST['id']."' ";
        $result['Subdocument'] = $this->common->getData("tbl_sub_document_type","*",$condition);
        if($result){

            $result['subdoctype'] = $_POST['subdoctype']; 
            // print_r($result);
            // exit;
            $html = $this->load->view('subdocumenthtml',$result,true);
            echo json_encode(array('success' => true, 'msg' => 'Record found.','html' => $html));
            exit;
        }else{
            echo json_encode(array('success' => false, 'msg' => 'No recor found.'));
            exit;

        }
    }

    function fetch() {
        //get record at list
        $get_result = $this->documentMastermodel->getRecords($_GET);
        // echo "<pre>";
        // print_r($get_result);
        // exit;
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            $j=0;
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                ++$j;
                // array_push($temp, ucfirst($get_result['query_result'][$i]->custom_field_structure_id));
                array_push($temp, $j);
                array_push($temp, ucfirst($get_result['query_result'][$i]->document_type_name));
                array_push($temp, ucfirst($get_result['query_result'][$i]->sub_document_type_name));
                // array_push($temp, ucfirst($get_result['query_result'][$i]->status));
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("DocumentMasterAddEdit")){ 
                    $actionCol = '<a href="documentMaster/addEdit?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->document_type_id), '+/', '-_'), '=').'&subDoc='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->sub_document_type_id), '+/', '-_'), '=').' " title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        // echo "<pre>";
        // print_r($_POST);
        // exit;

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "document_type_id = '" . trim($this->input->post('document_type')) . "' ";
            if (!empty($this->input->post("sub_document_type"))) {
                $condition .= "&&  sub_document_type_id = " . $this->input->post("sub_document_type");
            }

            if (!empty($this->input->post("document_type"))) {
                $condition .= " AND document_type_id <> " . $this->input->post("document_type");
            }
            if (!empty($this->input->post("sub_document_type"))) {
                $condition .= " AND document_type_id <> " . $this->input->post("sub_document_type");
            }

            // check for existance 
            $exiting = $this->common->getData("tbl_custom_field_structure","*",$condition);

            if (!empty($exiting[0]['document_type_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Custom Field is already exist for this combination!!...'));
                exit;
            }

            if(empty($_POST['custom_field_title']) && !isset($_POST['custom_field_title'])){
                echo json_encode(array('success' => false, 'msg' => 'Custom Field not found'));
                exit;
            }

            if (!empty($this->input->post("custom_field_structure_id"))) {
                //update data
                    if(!empty($_POST['custom_field_title']) && isset($_POST['custom_field_title'])){
                        foreach ($_POST['custom_field_title'] as $key => $value) {
                            $data = array();
                            $data['document_type_id'] = trim($this->input->post('document_type'));
                            $data['sub_document_type_id'] = trim(!empty($this->input->post('sub_document_type'))?$this->input->post('sub_document_type'):'0');
                            $data['custom_field_title'] = trim($value);
                            $data['custom_field_type'] = trim($_POST['custom_type'][$key]);
                            $data['custom_field_value'] = trim($_POST['custom_field_value'][$key]);
                            $data['status'] = trim($_POST['status'][$key]);
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $result = $this->common->insertData("tbl_custom_field_structure", $data, "1");
                        }

                    }
                    if(!empty($_POST['custom_field_title_existing']) && isset($_POST['custom_field_title_existing'])){
                        foreach ($_POST['custom_field_title_existing'] as $key => $value) {

                            $data['document_type_id'] = trim($this->input->post('document_type'));
                            $data['sub_document_type_id'] = trim(!empty($this->input->post('sub_document_type'))?$this->input->post('sub_document_type'):'0');
                            $data['custom_field_title'] = trim($value);
                            $data['custom_field_type'] = trim($_POST['custom_type_existing'][$key]);
                            $data['custom_field_value'] = trim($_POST['custom_field_value_existing'][$key]);
                            $data['status'] = trim($_POST['status_existing'][$key]);
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");

                            $condition = "custom_field_structure_id = '".$key."' ";
                            $result = $this->common->updateData("tbl_custom_field_structure", $data, $condition);
                        }

                    }
                    
                    // forloop start here
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                
            } else {

                //insert data
                if(!empty($_POST['custom_field_title']) && isset($_POST['custom_field_title'])){
                    foreach ($_POST['custom_field_title'] as $key => $value) {
                        $data = array();
                        $data['document_type_id'] = trim($this->input->post('document_type'));
                        $data['sub_document_type_id'] = trim($this->input->post('sub_document_type'));
                        $data['custom_field_title'] = trim($value);
                        $data['custom_field_type'] = trim($_POST['custom_type'][$key]);
                        $data['custom_field_value'] = trim($_POST['custom_field_value'][$key]);
                        $data['status'] = trim($_POST['status'][$key]);
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['created_on'] = date("Y-m-d H:i:s");
                        $result = $this->common->insertData("tbl_custom_field_structure", $data, "1");
                    }
                }else{
                    echo json_encode(array('success' => false, 'msg' => 'Custom Field not found'));
                    exit;
                }
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }

    
	public function changeorder()
	{
        // echo "<pre>";
        // print_r($_POST);
        // exit;
		$positions = $this->input->post('position');
		if($positions){
			$order_sequence = 1;
			foreach ($positions as $key => $value) {
				$data = array();
				$data['document_type_squence'] = $order_sequence;
				$condition = "document_type_id = '".$value."' ";
				$result = $this->common->updateData("tbl_document_type",$data,$condition);	
				$order_sequence++;
			}
			echo json_encode(array("success"=>true, 'msg'=>'position updated'));
			$this->session->set_flashdata('message-ordering', '<strong>Success!</strong> position updated.');
			exit;
		}else{
			echo json_encode(array("success"=>false, 'msg'=>'No themes left for re-ordering'));
			exit;
		}
	}

    function deleteCustomField(){

        $Result = $this->common->deleteRecord('tbl_custom_field_structure', 'custom_field_structure_id = '.$_POST['id']);
		if($Result)
		{
			echo json_encode(array("success" => "1", "msg" => "deleted successfully.", "body" => NULL));
			exit;
		}else{
			echo json_encode(array("success" => "0", "msg" => "Delete Failed", "body" => NULL));
			exit;
		}	
    }

}

?>
