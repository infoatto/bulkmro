<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("size"); ?>">Size List</a>
                                <span>></span>
                                <p>Add Size</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add Size</h1>
                            </div>
                        </div>          
                        <button class="btn-primary-mro">Save</button>
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="size_id" id="size_id" value="<?php echo(!empty($size_details['size_id'])) ? $size_details['size_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="SizeName">Size Name</label>
                                    <input type="text" name="size_name" id="size_name" class="input-form-mro" value="<?php echo(!empty($size_details['size_name'])) ? $size_details['size_name'] : ""; ?>">
                                </div> 
                                <div class="form-group">
                                    <label for="Status">Status</label>
                                    <select name="status" id="status" class="select-form-mro">
                                        <option value="Active" <?php echo(!empty($size_details['status']) && $size_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                        <option value="In-active" <?php echo(!empty($size_details['status']) && $size_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                    </select>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {
        var vRules = {
            "size_name": {required: true}
        };
        var vMessages = {
            "size_name": {required: "Please enter size name."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>size/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show(); 
                        if (response.success) {
                            alert(response.msg);
                            setTimeout(function () {
                                window.location = "<?= base_url('size') ?>";
                            }, 1000);
                        } else {
                            alert(response.msg);
                        }
                    }
                });
            }
        });

    });
</script>