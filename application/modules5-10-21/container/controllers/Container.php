<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Container extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('containermodel', '', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("ContainersList")){
            redirect('dashboard');
        }
    }

    function index() {
        $cityData = array(); 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('container/index', $cityData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("ContainersAddEdit")){
            redirect('dashboard');
        }
        //code for add edit form
        $edit_datas = array();
        $edit_datas['container_details'] = array();
        $edit_datas['contractDetails'] = array();
        $edit_datas['chaDetails'] = array();
        $edit_datas['freightForwarder'] = array();
        $edit_datas['skudetails'] = array();
        $container_id = '';
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $container_id = $url_prams['id'];
            $result = $this->common->getData("tbl_container", "*", array("container_id" => $container_id));
            if (!empty($result)) {
                $edit_datas['container_details'] = $result[0];
            }
            //container sku details
            $skuData = $this->containermodel->getContainerSkuDetails($container_id);
            if (!empty($skuData)) {
                $edit_datas['skudetails'] = $skuData;
            }
        }
        $contractDetails = $this->common->getData("tbl_order", "order_id,contract_number", "");
        if (!empty($contractDetails)) {
            $edit_datas['contractDetails'] = $contractDetails;
        }
        
        //CHA data
        $chaDetails = $this->common->getData("tbl_business_partner", "business_partner_id,business_name", array("business_type" => "Vendor", "business_category" => "Customs Broker"));
        if (!empty($chaDetails)) {
            $edit_datas['chaDetails'] = $chaDetails;
        }

        //Freight Forwarder data 
        $freightForwarder = $this->common->getData("tbl_business_partner", "business_partner_id,business_name", array("business_type" => "Vendor", "business_category" => "Freight Forwarder"));
        if (!empty($freightForwarder)) {
            $edit_datas['freightForwarder'] = $freightForwarder;
        }
        
        $edit_datas['view'] = isset($_GET['view'])?$_GET['view']:0; 
         
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('container/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        //code for get city record at list
        $get_result = $this->containermodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->container_number));
                array_push($temp, $get_result['query_result'][$i]->contract_number);
                array_push($temp, $get_result['query_result'][$i]->status);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("ContainersAddEdit")){ 
                    $actionCol = '<a href="container/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->container_id), '+/', '-_'), '=') . '" title="Edit" class="text-center"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                if($this->privilegeduser->hasPrivilege("ContainersView")){   
                    $actionCol .= '<a href="container/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->container_id), '+/', '-_'), '=') . '&view=1" title="Edit" class="text-center" style="float:right;"><img src="' . base_url() . 'assets/images/view-btn-icon.svg" alt="Edit" ></a>';
                } 
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "container_number = '" . $this->input->post('container_number') . "' ";
            if (!empty($this->input->post("container_id"))) {
                $condition .= " AND container_id <> " . $this->input->post("container_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['tbl_container'])) {
                echo json_encode(array('success' => false, 'msg' => 'Container Number already exist...'));
                exit;
            }

            $data = array();
            $data['container_number'] = $this->input->post('container_number');
            $data['order_id'] = $this->input->post('order_id');
            $data['broker_id'] = $this->input->post('broker_id'); 
            $data['freight_forwarder_id'] = $this->input->post('freight_forwarder_id'); 
            $data['eta'] = !empty($this->input->post('eta')) ? date("Y-m-d", strtotime($this->input->post('eta'))) : NULL;
            $data['revised_eta'] = !empty($this->input->post('revised_eta')) ? date("Y-m-d", strtotime($this->input->post('revised_eta'))) : NULL;
            $data['etd'] = !empty($this->input->post('etd')) ? date("Y-m-d", strtotime($this->input->post('etd'))) : NULL;
            $data['revised_etd'] = !empty($this->input->post('revised_etd')) ? date("Y-m-d", strtotime($this->input->post('revised_etd'))) : NULL; 
            $data['ett'] = $this->input->post('ett'); 
            $data['revised_ett'] = $this->input->post('revised_ett'); 
            $data['liner_name'] = $this->input->post('liner_name'); 
            $data['liner_tracker_url'] = $this->input->post('liner_tracker_url'); 
            $data['pol'] = $this->input->post('pol'); 
            $data['pod'] = $this->input->post('pod'); 
            $data['vessel_name'] = $this->input->post('vessel_name'); 
            $data['vessel_tracker_url'] = $this->input->post('vessel_tracker_url');
            $data['flag'] = $this->input->post('flag');
            $data['status'] = 'Active';
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            if (!empty($this->input->post("container_id"))) {
                //update container
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_container", $data, array("container_id" => $this->input->post("container_id")));
                
                //delete and insert container sku
                $data1['container_id'] = $this->input->post("container_id");
                $resultdel = $this->common->deleteRecord('tbl_container_sku', array("container_id" => $this->input->post("container_id")));
                if ($resultdel) {
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $result1 = $this->common->insertData('tbl_container_sku', $data1, '1');
                        }
                    } 
                }
                
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert container
                $data['container_status_id'] = 1;
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_container", $data, "1");
                if ($result) {
                    //insert container sku
                    $data1['container_id'] = $result;
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $result1 = $this->common->insertData('tbl_container_sku', $data1, '1');
                        }
                    }
                    
                    $data2['container_id'] = $result;
                    $data2['container_status_id'] = 1;
                    $data2['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data2['created_on'] = date("Y-m-d H:i:s");
                    $result2 = $this->common->insertData("tbl_container_status_log", $data2, "1"); 
                    
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }
    
    function getContractNumber(){
        $containerNumber = '600001';
        $containerNumberData = $this->containermodel->getContainerNumber();
        if(!empty($containerNumberData)){
            $containerNumber = $containerNumberData[0]['container_number'];
            $containerNumber += 1;
        }
        echo json_encode(array('containerNumber' => $containerNumber)); 
        exit;
    }

    function getContractDetails() { 
        $contract_id = $_GET['contract_id'];
        $is_edit = $_GET['is_edit'];
        $contactDetail = array();
        $broker_id = 0;
        $freight_forwarder_id = 0;
        $customerContact = '';
        $customerPartner = '';
        $supplierContact = '';
        $supplierPartner = '';
        $custmerSupplierStr = '';
        $skuStr = '';
        $customerContactId = 0;
        $supplierContactId = 0;
        $contactDetail = $this->common->getData("tbl_order", "customer_contract_id,supplier_contract_id,broker_id,freight_forwarder_id", array("order_id" => (int) $contract_id));
        if(!empty($contactDetail)){
            $broker_id = $contactDetail[0]['broker_id'];
            $freight_forwarder_id = $contactDetail[0]['freight_forwarder_id'];
            $customer_contract_id = $contactDetail[0]['customer_contract_id'];
            $supplier_contract_id = $contactDetail[0]['supplier_contract_id'];
            $customerDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $customer_contract_id));
            if(!empty($customerDetail)){
                $customerContactId = $customerDetail[0]['bp_order_id'];
                $customerContact = $customerDetail[0]['contract_number'];
                $customerPartnerId = $customerDetail[0]['business_partner_id'];
                $customerPartnerDetail = $this->common->getData("tbl_business_partner", "business_name", array("business_partner_id " => (int) $customerPartnerId));
                if(!empty($customerPartnerDetail)){
                    $customerPartner = $customerPartnerDetail[0]['business_name'];
                }
            }
            $supplierDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $supplier_contract_id));
            if(!empty($supplierDetail)){
                $supplierContactId = $supplierDetail[0]['bp_order_id'];
                $supplierContact = $supplierDetail[0]['contract_number'];
                $supplierPartnerId = $supplierDetail[0]['business_partner_id'];
                $supplierPartnerDetail = $this->common->getData("tbl_business_partner", "business_name", array("business_partner_id " => (int) $supplierPartnerId));
                if(!empty($supplierPartnerDetail)){
                    $supplierPartner = $supplierPartnerDetail[0]['business_name'];
                }
            }
            $url = "http://bulkmro.webshowcase-india.com/bporder/addEdit?text";
            if($_SERVER['HTTP_HOST']=='localhost'){
                $url = "http://localhost/bulkmro/bporder/addEdit?text";
            }
            $customerContractStr = '<p class="sit-single-value">'.$customerContact.'</p>';
            $supplierContractStr = '<p class="sit-single-value">'.$supplierContact.'</p>';
            if($this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")){
                $customerContractStr = '<p class="sit-single-value"><a href="'.$url.'=' . rtrim(strtr(base64_encode("id=" . $customerContactId), '+/', '-_'), '=') . '" target="_blank"><u>'.$customerContact.'</u></a></p>';
                $supplierContractStr = '<p class="sit-single-value"><a href="'.$url.'=' . rtrim(strtr(base64_encode("id=" . $supplierContactId), '+/', '-_'), '=') . '" target="_blank"><u>'.$supplierContact.'</u></a></p>';
            }
            
            $custmerSupplierStr = '<div class="sit-row">
                                        <div class="sit-single">
                                            <p class="sit-single-title">Customer Name</p>
                                            <p class="sit-single-value">'.$customerPartner.'</p>
                                        </div>
                                        <div class="sit-single">
                                            <p class="sit-single-title">Customer Order</p>
                                            '.$customerContractStr.'
                                        </div>
                                        <div class="sit-single">
                                            <p class="sit-single-title">Vendor Name</p>
                                            <p class="sit-single-value">'.$supplierPartner.'</p>
                                        </div>
                                        <div class="sit-single">
                                            <p class="sit-single-title">Vendor Order</p>
                                            '.$supplierContractStr.'
                                        </div>
                                    </div>';
                if($is_edit==0){                    
                    $skuDetail = $this->containermodel->getskuData($contract_id);
                    if (!empty($skuDetail)) {
                        foreach ($skuDetail as $key => $value) {
                            $skuStr .= '<div class="sit-row">
                                <div class="sit-single sku-item-code">
                                    <p class="sit-single-title">Item Code</p>
                                    <p class="sit-single-value">'.$value["sku_number"].'</p>
                                    <input type="hidden" name="sku_id[' . $key . ']" id="sku_id' . $key . '" value="' . $value["sku_id"] . '">
                                </div>
                                <div class="sit-single sku-pd">
                                    <p class="sit-single-title">Product Description</p>
                                    <p class="sit-single-value">' . $value["product_name"] . '</p>
                                </div>
                                <div class="sit-single sku-size">
                                    <p class="sit-single-title">Size</p>
                                    <p class="sit-single-value">' . $value["size_name"] . '</p>
                                </div>
                                <div class="sit-single sku-qty">
                                    <p class="sit-single-title">Quantity<sup>*</sup></p>
                                    <input type="text" name="sku_qty[' . $key . ']" id="sku_qty' . $key . '" value="' . $value["quantity"] . '" class="input-form-mro" placeholder="Enter quantity">
                                </div> 
                            </div>';
                        }
                    }
                }
        }
         
        echo json_encode(array('custmerSupplier' => $custmerSupplierStr,'sku' => $skuStr,'broker_id' =>$broker_id ,'freight_forwarder_id' => $freight_forwarder_id)); 
        exit;
    }
    
    function getETTDays(){
        $revised_eta = $_GET['revised_eta'];
        $revised_etd = $_GET['revised_etd'];
        $days = '';
        if(!empty($revised_eta) && !empty($revised_etd)){
            $days = DaysDiffBetweenTwoDays($revised_etd,$revised_eta);
        }
        echo json_encode(array('days' => $days)); 
        exit;
    }

}

?>
