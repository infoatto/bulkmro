<?PHP

class Containermodel extends CI_Model {

    function getRecords($get) { 
        $table = "tbl_container";
        $default_sort_column = 'c.container_id';
        $default_sort_order = 'c.container_number desc';
        $condition = "1=1 ";
        $sortArray = array('c.container_number');

        $colArray = array('c.container_number', 'o.order_id');
        $sortArray = array('c.container_number', 'o.contract_number','c.status');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

        for ($i = 0; $i < count($colArray); $i++) {
            if (isset($_GET['Searchkey_' . $i])) { //&& $_GET['Searchkey_'.$i] !=""
                $condition .= " AND " . $colArray[$i] . " LIKE '%" . $_GET['Searchkey_' . $i] . "%' ";
            }
        }

        $this->db->select('c.container_id,c.container_number,c.status,o.contract_number');
        $this->db->from("$table as c");
        $this->db->join("tbl_order as o", "o.order_id = c.order_id"); 
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page);
        $query = $this->db->get();
        //echo $this->db->last_query();exit; 

        $this->db->select('c.container_id,c.container_number,c.status,o.contract_number');
        $this->db->from("$table as c");
        $this->db->join("tbl_order as o", "o.order_id = c.order_id");
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    } 
    
    function getskuData($contract_id = 0) {
        $condition = " osku.order_id = " . $contract_id . " ";
        $this->db->select('osku.sku_id,osku.quantity,sku.sku_number,sku.product_name,s.size_name');
        $this->db->from("tbl_sku as sku");
        $this->db->join("tbl_size as s", "s.size_id = sku.size_id",'left');
        $this->db->join("tbl_order_sku as osku", "osku.sku_id = sku.sku_id");
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getContainerSkuDetails($container_id = 0) {
        $condition = " c.container_id = " . $container_id . " ";
        $this->db->select('c.*,sku.sku_id,sku.sku_number,sku.product_name,s.size_name');
        $this->db->from("tbl_container_sku as c");
        $this->db->join("tbl_sku as sku", "c.sku_id = sku.sku_id");
        $this->db->join("tbl_size as s", "s.size_id = sku.size_id",'left');
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getContainerNumber(){
        $table = "tbl_container"; 
        $sort = 'c.container_number';
        $order = 'c.container_number desc';
        $condition = "1=1 "; 
        $this->db->select('c.container_number');
        $this->db->from("$table as c");
        $this->db->where("($condition)");
        $this->db->order_by('c.container_number desc');
        $this->db->limit(1);
        $query = $this->db->get();
        //echo $this->db->last_query();exit; 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

}

?>
