<?php
//start code of view option    
$dropDownCss = '';
$inputFieldCss = '';
if($view==1){
    $dropDownCss = "disabled";
    $inputFieldCss = "readonly";
}
//end code of view option
?>
<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("container"); ?>">All Containers</a>
                                <span>></span>
                                <p>New Container Details</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">New Container Details</h1>
                            </div>
                        </div>
                        <?php if($view==0){ ?>
                            <button type="submit" class="btn-primary-mro">Save</button>
                        <?php } ?>
                    </div>
                    <div class="page-content-wrapper master-order-wrapper">
                        <input type="hidden" name="container_id" id="container_id" value="<?php echo(!empty($container_details['container_id'])) ? $container_details['container_id'] : ""; ?>">
                        <div class="scroll-content-wrapper master-order">
                            <div class="scroll-content-left master-order-left" id="container-details"> 
                                <h3 class="form-group-title">Container Details</h3>
                                <div class="form-row form-row-3">
                                    <div class="form-group input-edit">
                                        <label for="">Container Number <sup>*</sup></label>
                                        <input type="text" name="container_number" id="container_number" value="<?php echo(!empty($container_details['container_number'])) ? $container_details['container_number'] : ""; ?>" placeholder="Enter Contract Number" class="input-form-mro" <?=$inputFieldCss;?>>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label for="">Master Contract<sup>*</sup></label>
                                        <select name="order_id" id="order_id" class="basic-single select-form-mro" onchange="getContractDetails(this.value)" <?=$dropDownCss;?>>
                                            <option value="">Select Master Contract</option>
                                            <?php
                                            foreach ($contractDetails as $ccValue) {
                                                $ccID = (isset($container_details['order_id']) ? $container_details['order_id'] : 0);
                                                ?> 
                                                <option value="<?php echo $ccValue['order_id']; ?>"  <?php
                                                if ($ccValue['order_id'] == $ccID) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                            <?php echo $ccValue['contract_number']; ?> 
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="new-container-details">
                                    <hr class="separator mt0">
                                    <div class="view-mo-wrapper" id="customerSupplierDetail">

                                    </div>
                                    <hr class="separator mt0" id="sku-qty">
                                    <h3 class="form-group-title">SKUs in Container</h3>
                                    <div class="form-row form-row-3">
                                        <div class="sku-list-wrapper add-sku-wrapper" id="skuDetails">
                                            <?php
                                            foreach ($skudetails as $key => $value) {
                                                ?>
                                                <div class="sit-row">
                                                    <div class="sit-single sku-item-code">
                                                        <p class="sit-single-title">Item Code</p>
                                                        <p class="sit-single-value"><?= $skudetails[$key]['sku_number']; ?></p>
                                                        <input type="hidden" name="sku_id[<?= $key ?>]" id="sku_id<?= $key ?>" value="<?= (!empty($skudetails[$key]['sku_id'])) ? $skudetails[$key]['sku_id'] : '' ?>">
                                                    </div>
                                                    <div class="sit-single sku-pd">
                                                        <p class="sit-single-title">Product Description</p>
                                                        <p class="sit-single-value"><?= $skudetails[$key]['product_name']; ?></p>
                                                    </div>
                                                    <div class="sit-single sku-size">
                                                        <p class="sit-single-title">Size</p>
                                                        <p class="sit-single-value"><?= $skudetails[$key]['size_name']; ?></p>
                                                    </div>
                                                    <div class="sit-single sku-qty">
                                                        <p class="sit-single-title">Quantity<sup>*</sup></p>
                                                        <input type="text" name="sku_qty[<?= $key ?>]" id="sku_qty<?= $key ?>" value="<?= (!empty($skudetails[$key]['quantity'])) ? $skudetails[$key]['quantity'] : '' ?>" class="input-form-mro" placeholder="Enter quantity" <?=$inputFieldCss;?>>
                                                    </div> 
                                                </div> 
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <hr class="separator mt0" id="pickup-details">
                                    <div class="form-row form-row-2">
                                        <div class="form-group input-edit" id="freight_forwarder_div">
                                            <label for="">Freight Forwarder</label>
                                            <select name="freight_forwarder_id" id="freight_forwarder_id" class="basic-single select-form-mro" style="width: 100%;" <?=$dropDownCss;?>> 
                                                <option value="0">Select Freight Forwarder</option>
                                                <?php
                                                foreach ($freightForwarder as $value) {
                                                    $bpID = (isset($container_details['freight_forwarder_id']) ? $container_details['freight_forwarder_id'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['business_partner_id']; ?>"  <?php
                                                    if ($value['business_partner_id'] == $bpID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['business_name']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div>
                                        <div class="form-group input-edit">
                                            <label for="">CHA</label>
                                            <select name="broker_id" id="broker_id" class="basic-single select-form-mro" style="width: 100%;" <?=$dropDownCss;?>> 
                                                <option value="0">Select CHA</option>
                                                <?php
                                                foreach ($chaDetails as $value) {
                                                    $bpID = (isset($container_details['broker_id']) ? $container_details['broker_id'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['business_partner_id']; ?>"  <?php
                                                    if ($value['business_partner_id'] == $bpID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['business_name']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="form-row form-row-4">
                                        <div class="form-group datepicker">
                                            <label for="">ETA</label>
                                            <input type="date" name="eta" id="eta" value="<?php echo(!empty($container_details['eta'])) ? date('Y-m-d', strtotime($container_details['eta'])) : date("m/d/Y"); ?>" onchange="fillRevisedETA(this.value);fillETT();" class="form-control valid" aria-invalid="false" <?=$inputFieldCss;?>> 
                                        </div>
                                        <div class="form-group datepicker">
                                            <label for="">Revised ETA</label>
                                            <input type="date" name="revised_eta" id="revised_eta" value="<?php echo(!empty($container_details['revised_eta'])) ? date('Y-m-d', strtotime($container_details['revised_eta'])) : date("m/d/Y"); ?>" onchange="fillETT();" class="form-control valid" aria-invalid="false" <?=$inputFieldCss;?>> 
                                        </div>
                                        <div class="form-group">
                                            <label for="">ETD</label>
                                            <input type="date" name="etd" id="etd" value="<?php echo(!empty($container_details['etd'])) ? date('Y-m-d', strtotime($container_details['etd'])) : date("m/d/Y"); ?>" onchange="fillRevisedETD(this.value);fillETT();" class="form-control valid" aria-invalid="false" <?=$inputFieldCss;?>> 
                                        </div>
                                        <div class="form-group">
                                            <label for="">Revised ETD</label>
                                            <input type="date" name="revised_etd" id="revised_etd" value="<?php echo(!empty($container_details['revised_etd'])) ? date('Y-m-d', strtotime($container_details['revised_etd'])) : date("m/d/Y"); ?>" onchange="fillETT();" class="form-control valid" aria-invalid="false" <?=$inputFieldCss;?>> 
                                        </div>
                                    </div>
                                    <div class="form-row form-row-2">
                                        <div class="form-group">
                                            <label for="">ETT</label>
                                            <input type="text" name="ett" id="ett" value="<?php echo(!empty($container_details['ett'])) ? $container_details['ett'] : ""; ?>" placeholder="No ETA or ETD Entered" class="input-form-mro" <?=$inputFieldCss;?>>
                                            <!--<a href="#/"><img src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt=""></a>-->
                                        </div>
                                        <div class="form-group"><!--input-edit-->
                                            <label for="">Revised ETT</label>
                                            <input type="text" name="revised_ett" id="revised_ett" value="<?php echo(!empty($container_details['revised_ett'])) ? $container_details['revised_ett'] : ""; ?>" placeholder="No ETA or ETD Entered" class="input-form-mro" <?=$inputFieldCss;?>>
                                            <!--<a href="#/"><img src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt=""></a>-->
                                        </div>
                                    </div>
                                    <div class="form-row form-row-2">
                                        <div class="form-group">
                                            <label for="">Liner Name</label>
                                            <input type="text" name="liner_name" id="liner_name" value="<?php echo(!empty($container_details['liner_name'])) ? $container_details['liner_name'] : ""; ?>" placeholder="Enter Liner Name" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                        <div class="form-group ">
                                            <label for="">Liner Tracker URL</label>
                                            <input type="text" name="liner_tracker_url" id="liner_tracker_url" value="<?php echo(!empty($container_details['liner_tracker_url'])) ? $container_details['liner_tracker_url'] : ""; ?>" placeholder="Enter Liner Tracker URL" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                    </div>
                                    <div class="form-row form-row-2">
                                        <div class="form-group">
                                            <label for="">POL</label>
                                            <input type="text" name="pol" id="pol" value="<?php echo(!empty($container_details['pol'])) ? $container_details['pol'] : ""; ?>" placeholder="Enter POL" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                        <div class="form-group ">
                                            <label for="">POD</label>
                                            <input type="text" name="pod" id="pod" value="<?php echo(!empty($container_details['pod'])) ? $container_details['pod'] : ""; ?>" placeholder="Enter POD" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                    </div>
                                    <div class="form-row form-row-2">
                                        <div class="form-group">
                                            <label for="">Vessel Name</label>
                                            <input type="text" name="vessel_name" id="vessel_name" value="<?php echo(!empty($container_details['vessel_name'])) ? $container_details['vessel_name'] : ""; ?>" placeholder="Enter Vessel Name" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Vessel Tracker URL</label>
                                            <input type="text" name="vessel_tracker_url" id="vessel_tracker_url" value="<?php echo(!empty($container_details['vessel_tracker_url'])) ? $container_details['vessel_tracker_url'] : ""; ?>" placeholder="Enter Vessel Tracker URL" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                    </div>
                                    <div class="form-row form-row-2">
                                        <div class="form-group">
                                            <label for="">Flag</label>
                                            <input type="text" name="flag" id="flag" value="<?php echo(!empty($container_details['flag'])) ? $container_details['flag'] : ""; ?>" placeholder="Enter Flag" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div> 
                                    </div>
                                </div> 
                            </div>
                            <div class="scroll-content-right master-order-right">
                                <div class="vertical-scroll-links">
                                    <a href="#container-details" class="active">Container</a>
                                    <a href="#sku-qty">SKU Quantities</a>
                                    <a href="#pickup-details">Shipping Details</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section> 
<script>
    //code for autofill container number at time of add new container
<?php if (empty($container_details['container_id'])) { ?>
        function fillContainerNumber() {
            var act = "<?php echo base_url(); ?>container/getContractNumber";
            $.ajax({
                type: 'GET',
                url: act,
                data: {},
                dataType: "json",
                success: function (data) {
                    $('#container_number').val(data.containerNumber);
                }
            });
        }

        fillContainerNumber();
<?php } ?>
    //code for get master contract details
    function getContractDetails(contract_id, is_edit = 0) {
        var act = "<?php echo base_url(); ?>container/getContractDetails";
        $.ajax({
            type: 'GET',
            url: act,
            data: {contract_id: contract_id, is_edit: is_edit,isView:<?=$view?>},
            dataType: "json",
            success: function (data) {
                $('.new-container-details').show();
                $('#customerSupplierDetail').html(data.custmerSupplier);
                if (is_edit == 0) {
                    $('#skuDetails').html(data.sku);
                    $("#freight_forwarder_id").select2().select2('val', data.freight_forwarder_id);
                    $("#broker_id").select2().select2('val', data.broker_id);
                }
            }
        });
    }

    //code for fill Revised ETA
    function fillRevisedETA(value) {
        $('#revised_eta').val(value);
    }

    //code for fill Revised ETD
    function fillRevisedETD(value) {
        $('#revised_etd').val(value);
    }

    function fillETT() {
        var revised_eta = $('#revised_eta').val();
        var revised_etd = $('#revised_etd').val();
        var act = "<?php echo base_url(); ?>container/getETTDays";
        $.ajax({
            type: 'GET',
            url: act,
            data: {revised_eta: revised_eta, revised_etd: revised_etd},
            dataType: "json",
            success: function (data) {
                $('#ett').val(data.days);
                $('#revised_ett').val(data.days);
            }
        });
    }

    //code for get state in case of edit city
<?php if (!empty($container_details['container_id'])) { ?>
        getContractDetails('<?php echo(!empty($container_details['order_id'])) ? $container_details['order_id'] : "0"; ?>', '1');
<?php } ?>
</script>
<script>

    $(document).ready(function () {
        var vRules = {
            "container_number": {required: true},
            "order_id": {required: true}
        };
        var vMessages = {
            "container_number": {required: "Please enter container number."},
            "order_id": {required: "Please select master contract."}
        };
        //check and save city data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>container/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        if (response.success) {
                            alert(response.msg);
                            setTimeout(function () {
                                window.location = "<?= base_url('container') ?>";
                            }, 1000);
                        } else {
                            alert(response.msg);
                        }
                    }
                });
            }
        });

    });
</script>