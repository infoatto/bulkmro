  
  <?php
    if($getSelectedCustomField){
        foreach ($getSelectedCustomField as $key => $value) { ?>
            <input type="hidden" name="custom_field_structure_id[]" id="custom_field_structure_id" value="<?= $value['custom_field_structure_id']?>">
            <input type="hidden" name="document_type_id" id="document_type_id" value="<?= $value['document_type_id']?>">
            <input type="hidden" name="sub_document_type_id" id="sub_document_type_id" value="<?= $value['sub_document_type_id']?>">
            <input type="hidden" name="uploaded_document_number" id="uploaded_document_number" value="<?= $value['uploaded_document_number']?>">

            <?php if($value['custom_field_type'] == 'date'){ ?>
                <div class="form-group ">
                    <label for=""><?= $value['custom_field_title']?></label>
                    
                    <input type="date" name="<?= $value['custom_field_structure_id']?>" value="<?= $value['custom_field_structure_value']?>" readonly required id="<?= str_replace(' ', '_',trim($value['custom_field_structure_id']));?>" placeholder="Enter date" class="input-form-mro readonly">
                </div>
            <?php  } elseif ($value['custom_field_type'] == 'text') { ?>
                <div class="form-group ">
                    <label for=""><?= $value['custom_field_title']?></label>
                    <input type="text" name="<?= str_replace(' ', '_',trim($value['custom_field_structure_id']));?>" value="<?= $value['custom_field_structure_value']?>"  required id="<?= str_replace(' ', '_',trim($value['custom_field_structure_id']));?>" readonly placeholder="Enter data" class="input-form-mro readonly">
                </div>
            <?php } elseif ($value['custom_field_type'] == 'dropdown') { ?>

                <div class="form-group">
                    <div class="form-group">
                        <label for=""><?= $value['custom_field_title']?></label>
                        <div class="multiselect-dropdown-wrapper">
                        <div class="md-value">
                            Select value
                        </div>
                        <div class="md-list-wrapper">
                            <input type="text" placeholder="Search" class="md-search">
                            <div class="md-list-items">
                            <?php if($ListOfContainerData){
                                foreach ($ListOfContainerData as $key => $value) { ?>
                                <div class="mdli-single ud-list-single">
                                    <label class="container-checkbox"> <?= $value['containerData']['container_number'] ?>
                                        <input type="checkbox"  id="<?= $value['custom_field_structure_id']; ?>"  value="<?= $value['custom_field_structure_value']?>"  name="<?= $value['custom_field_structure_id']?>">
                                        <span class="checkmark-checkbox"></span>
                                    </label>
                                </div>
                                <?php
                            } }
                            ?> 
                            </div> 
                        </div>
                        </div>
                    </div>
                </div>
                
            <?php }elseif ($value['custom_field_title'] == 'number'){ ?>
                
            <?php }?>
        <?php }
    }?> 
        
         