<?php
if($formView){ ?>
    <div id="tabs">
        <ul>
            <?php  foreach ($formView as $key => $value) { ?>
                <li data-tab="tabs-<?=$key?>"><span><?= $value['document_name']?></a></li> 
            <?php  } ?>
        </ul>

        <?php  foreach ($formView as $key => $value) { ?>
            <div class="tabs-<?=$key?> tabs-modal">
                <iframe src="<?= base_url('container_document/').$value['uploaded_document_file']?>" style="width:100%;height:100%;"></iframe>
            </div> 
        <?php  } ?>
    </div>
<?php }else{ ?>
    <div>
        <p><h3>No Preview Available</h3></p>
    </div>
<?php } ?>

<script>

    // $("#tabs").tabs().hide();
</script>