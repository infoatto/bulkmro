<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                            <span>></span>
                            <a href="<?php echo base_url("listOfContainer"); ?>">All Containers</a>
                            <span>></span>
                            <p># <?= $container_details['container_number']; ?></p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title">Container # <?= $container_details['container_number']; ?></h1>
                            <input type="hidden" name="container_id" id="container_id" value="<?php echo (!empty($container_details['container_id'])) ? $container_details['container_id'] : ""; ?>">
                            <div class="nav-divider"></div>
                            <div class="cs-status-text">
                                <p class="container-status"><?php echo (!empty($show_details['containerStatus'])) ? $show_details['containerStatus'] : ""; ?></p>
                            </div>
                            <div class="cs-status-text" style="flex: 0 0 160px;max-width: none;">
                                <p class="last-update-title">Last Updated</p>
                                <p class="last-update-name"><?php echo (!empty($show_details['updateBy'])) ? $show_details['updateBy'] : ""; ?></p>
                            </div>
                            <?php
                            if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) {
                                ?>
                                <a href="<?php echo $editURL ?>" target="_blank"><img src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt=""></a>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <div class="title-sec-right">
                        <a href="#/" class="btn-primary-mro"><img src="<?php echo base_url(); ?>assets/images/upload-icon-white.svg" alt=""> Upload File</a>
                        <a href="#/" class="btn-transparent-mro"><img src="<?php echo base_url(); ?>assets/images/duplicate-icon.svg" alt=""> Duplicate</a>
                    </div>
                </div>
                <div class="shipping-info-top">
                    <h2 class="sec-title">Shipping Information</h2>
                    <hr class="separator">
                    <div class="sit-wrapper">
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Freight Forwarder</p>
                                <p class="sit-single-value"><?php echo (!empty($freightForwarder)) ? $freightForwarder : ""; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">CHA</p>
                                <p class="sit-single-value"><?php echo (!empty($CHA)) ? $CHA : ""; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">POL</p>
                                <p class="sit-single-value"><?= $container_details['pol']; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">POD</p>
                                <p class="sit-single-value"><?= $container_details['pod']; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Liner Name</p>
                                <p class="sit-single-value"><?= $container_details['liner_name']; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Vessel Name</p>
                                <p class="sit-single-value"><?= $container_details['vessel_name']; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Flag</p>
                                <p class="sit-single-value"><?= $container_details['flag']; ?></p>
                            </div>
                        </div>
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">ETD</p>
                                <p class="sit-single-value"><?= date('d-M-y', strtotime($container_details['etd'])); ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Revised ETD</p>
                                <p class="sit-single-value"><?= date('d-M-y', strtotime($container_details['revised_etd'])); ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">ETA</p>
                                <p class="sit-single-value"><?= date('d-M-y', strtotime($container_details['eta'])); ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Revised ETA</p>
                                <p class="sit-single-value"><?= date('d-M-y', strtotime($container_details['revised_eta'])); ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">ETT</p>
                                <p class="sit-single-value"><?= $container_details['ett']; ?> Days</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Revised ETT</p>
                                <p class="sit-single-value"><?= $container_details['revised_ett']; ?> Days</p>
                            </div>
                        </div>
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Customer Alias</p>
                                <p class="sit-single-value">REM</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Vendor Alias</p>
                                <p class="sit-single-value">HON</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Container #</p>
                                <p class="sit-single-value">Waiting for FCR</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Container Seal #</p>
                                <p class="sit-single-value">Waiting for FCR</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">FF Seal #</p>
                                <p class="sit-single-value">Waiting for FCR</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Email Sent On</p>
                                <p class="sit-single-value">30-Jun-21</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Email Subject</p>
                                <p class="sit-single-value">Lorem ipsum dolor si amet...</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="shipping-tabs-wrapper">
                    <div class="mro-tabs-container">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-documents-tab" data-toggle="tab" href="#nav-documents" role="tab" aria-controls="nav-documents" aria-selected="true">Documents</a>
                                <!-- <a class="nav-item nav-link" id="nav-linked-containers-tab" data-toggle="tab" href="#nav-linked-containers" role="tab" aria-controls="nav-linked-containers" aria-selected="true">Linked Containers</a> -->
                                <a class="nav-item nav-link " id="nav-products-tab" data-toggle="tab" href="#nav-products" role="tab" aria-controls="nav-products" aria-selected="false">Products</a>
                                <a class="nav-item nav-link " id="nav-invoices-tab" data-toggle="tab" href="#nav-invoices" role="tab" aria-controls="nav-invoices" aria-selected="false">Invoices</a>
                                <a class="nav-item nav-link" id="nav-addresses-tab" data-toggle="tab" href="#nav-addresses" role="tab" aria-controls="nav-addresses" aria-selected="false">Addresses</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent"> 
                            <div class="tab-pane fade show active" id="nav-documents" role="tabpanel" aria-labelledby="nav-documents-tab">
                                
                            </div>
                            <div class="tab-pane fade" id="nav-linked-containers" role="tabpanel" aria-labelledby="nav-linked-containers-tab">
                                <input type="hidden" id="link_container_cnt" value="0">
                                <div class="tab-content-wrapper" id="link_container_details">
                                    
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-products" role="tabpanel" aria-labelledby="nav-products-tab">
                                <div class="tab-content-wrapper">
                                    <h3 class="form-group-title">Product Details
                                        <?php
                                        if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) {
                                            ?>
                                            <a href="<?php echo $editURL ?>#sku-qty" target="_blank"><img src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt=""></a>
                                        <?php }
                                        ?>
                                    </h3>
                                    <div class="bp-list-wrapper">
                                        <input type="hidden" id="product_cnt" value="0">
                                        <div class="table-responsive" id="product_details">

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-invoices" role="tabpanel" aria-labelledby="nav-invoices-tab">
                                <input type="hidden" id="invoices_cnt" value="0">
                                <div id="invoices_details">

                                </div>
                                <input type="hidden" id="invoices_cnt" value="0">
                                <div id="invoices_details">
                                    
                                </div> 
                            </div>
                            <div class="tab-pane fade" id="nav-addresses" role="tabpanel" aria-labelledby="nav-addresses-tab">
                                <input type="hidden" id="address_cnt" value="0">
                                <div id="address_details">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- custom modal -->
<div class="overlay"></div>
<div class="mro-modal-wrapper">
    <div class="mro-modal-header">
        <a href="#/" class="mro-close-modal">
            <img src="<?php echo base_url(); ?>assets/images/modal-close-dark.svg" alt="Close">
        </a>
    </div>
    <div class="mro-modal-body">
        <div class="ud-step upload-fri-step-1">
            <div class="upload-doc-title">
                <div class="ud-title-left"> 
                    <h3>Invoice Detail View</h3>
                </div>
            </div>
            <div class="select-doc-wrapper">
                <div class="sd-left">
                    <!--          <h4 class="sd-subtitle mb20">Enter Custom Data</h4>-->
                    <div id="custom_data_field"></div>
                </div>
                <div class="sd-right">
                    <h4 class="sd-subtitle">Form View</h4>
                    <span id="form-view"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- custom modal -->  

<!-- custom modal -->
<div class="overlay overlay-document"></div>
<div class="mro-modal-wrapper mro-modal-wrapper-document">
    <form id="documentStep5" enctype="multipart/form-data" method="post">
        <div class="mro-modal-header">
            <a href="#/" class="mro-close-modal">
                <img src="<?php echo base_url(); ?>assets/images/modal-close-dark.svg" alt="Close">
            </a>
        </div>
        <div class="mro-modal-body">
            <div class="ud-step upload-fri-step-1">
                <div class="upload-doc-title">
                    <div class="ud-title-left"> 
                        <h3>Review Document Titles</h3>
                    </div>
                </div>
                <div class="ud-title-right document-save">

                </div>
                <div class="select-doc-wrapper">
                    <div class="sd-left">
                        <h4 class="sd-subtitle mb20">Enter Custom Data</h4>
                        <div id="custom_data_field_document"></div>
                    </div>
                    <div class="sd-right">
                        <h4 class="sd-subtitle">Form View</h4>
                        <span id="form-view-document"></span>
                    </div>
                </div>
                <div class="upload-doc-next document-save">

                </div>
            </div>
        </div>
    </form>
</div>
<!-- custom modal --> 

</body>

</html>

<script>
    $(document).ready(function () {
        var vRules = {};
        var vMessages = {};
        
        $("#documentStep5").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>container_details/submitFormStep5";
                $("#documentStep5").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".save-record").hide();
                    },
                    success: function (response) {
                        $(".save-record").show();
                        console.log(response);
                        if (response.success) {
                            alert(response.msg); 
                        }  
                    }
                });
            }
        });
        
        
        $('#nav-documents-tab').on('click', function () {
            getDocumentDetails();
        }) 
        getDocumentDetails();
        
     
        $('#nav-products-tab').on('click', function () {
            var product_cnt = $('#product_cnt').val();
            //if (product_cnt == 0) {
                var container_id = $('#container_id').val();
                var act = "<?php echo base_url(); ?>container_details/getProductDetails";
                $.ajax({
                    type: 'GET',
                    url: act,
                    data: {
                        container_id: container_id
                    },
                    dataType: "json",
                    success: function (data) {
                        $('#product_details').html(data.productDetails);
                        $('#product_cnt').val('1');
                    }
                });
            //}
        })

        $('#nav-addresses-tab').on('click', function () {
            var address_cnt = $('#address_cnt').val();
            //if (address_cnt == 0) {
                var container_id = $('#container_id').val();
                var act = "<?php echo base_url(); ?>container_details/getAddressDetails";
                $.ajax({
                    type: 'GET',
                    url: act,
                    data: {
                        container_id: container_id
                    },
                    dataType: "json",
                    success: function (data) {
                        $('#address_details').html(data.productDetails);
                        $('#address_cnt').val('1');
                    }
                });
            //}
        })

        $('#nav-invoices-tab').on('click', function () {
            //var invoices_cnt = $('#invoices_cnt').val();
            //if(invoices_cnt==0){
            var container_id = $('#container_id').val();
            var act = "<?php echo base_url(); ?>container_details/getInvoiceDetails";
            $.ajax({
                type: 'GET',
                url: act,
                data: {
                    container_id: container_id
                },
                dataType: "json",
                success: function (data) {
                    $('#invoices_details').html(data.invoiceDetails);
                    $('#invoices_cnt').val('1');
                }
            });
            //}
        })
        
        $('#nav-invoices-tab').on('click', function () {
            var invoices_cnt = $('#invoices_cnt').val();
            if(invoices_cnt==0){
                var container_id = $('#container_id').val();
                var act = "<?php echo base_url(); ?>container_details/getInvoiceDetails";
                $.ajax({
                    type: 'GET',
                    url: act,
                    data: {container_id:container_id},
                    dataType: "json",
                    success: function (data) { 
                        $('#invoices_details').html(data.invoiceDetails);
                        $('#invoices_cnt').val('1');
                    }
                }); 
            }
        })
        
        $('#nav-linked-containers-tab').on('click', function () {
            var link_container_cnt = $('#link_container_cnt').val();
            //if(link_container_cnt==0){
                var container_id = $('#container_id').val();
                var act = "<?php echo base_url(); ?>container_details/getLinkContainerDetails";
                $.ajax({
                    type: 'GET',
                    url: act,
                    data: {
                        container_id: container_id
                    },
                    dataType: "json",
                    success: function (data) {
                        $('#link_container_details').html(data.linkDetails);
                        $('#link_container_cnt').val('1');
                    }
                });
            //}
        }) 
    });  
        
    function getDocumentDetails(){ 
        var container_id = $('#container_id').val();
        var act = "<?php echo base_url(); ?>container_details/getDocumentDetails";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                container_id: container_id
            },
            dataType: "json",
            success: function (data) {
                $('#nav-documents').html(data.documentDetails); 
            }
        }); 
    } 

    function showUpdateButton(cnt) {
        $('#cs-status-update' + cnt).show();
        $('#cs-status-text' + cnt).hide();
    }

    function updateInvoiceDetails(id, cnt) {
        var invoice_date = $('#invoice_date' + cnt).val();
        var invoice_status = $('#invoice_status' + cnt).val();
        var paid_date = $('#paid_date' + cnt).val();
        var invoice_value = $('#invoice_value' + cnt).val();
        var act = "<?php echo base_url(); ?>container_details/updateInvoiceDetails";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                id: id,
                invoice_date: invoice_date,
                invoice_status: invoice_status,
                paid_date: paid_date,
                invoice_value: invoice_value
            },
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $('#cs-status-update' + cnt).hide();
                    $('#cs-status-text' + cnt).show();
                    $('#last-update-title' + cnt).hide();
                    $('#last-update-name' + cnt).hide();
                    $('#success-msg' + cnt).show();
                    $('#success-msg' + cnt).html(data.msg);
                }
            }
        });
    }

    function showInvoiceDetails(id) {
        $('.overlay,.mro-modal-wrapper').show();
        var act = "<?php echo base_url(); ?>container_details/getInvoiceDetailsView";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                id: id
            },
            dataType: "json",
            success: function (data) {
                $('#custom_data_field').html(data.invoiceDetails);
                $('#form-view').html(data.invoicePDF);
            }
        });
    }
    
    function showDocumentDetails(document_type_id=0,container_id=0,document_number='',status='') {
        $('.overlay-document,.mro-modal-wrapper-document').show();
        var act = "<?php echo base_url(); ?>container_details/getUploadDetailsView";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                document_type_id: document_type_id,container_id:container_id,document_number:document_number,status:status
            },
            dataType: "json",
            success: function (data) {
                $('#custom_data_field_document').html(data.custom_field);
                $('#form-view-document').html(data.form_view);
                $('.document-save').html(data.savehtml);
            }
        });
    }
    
    
    function documentApprove(document_type_id=0,container_id=0) {
        $('.overlay-document,.mro-modal-wrapper-document').show();
        var act = "<?php echo base_url(); ?>container_details/documentApprove";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                document_type_id: document_type_id,container_id:container_id
            },
            dataType: "json",
            success: function (data) { 
                $('.checkedClass').html('Unchecked');
            }
        });
    }
    
    function showSaveButton(){
        $(".update-record").hide();
        $(".save-record").show(); 
        $(".readonly").attr("readonly", false); 
    }
    
    //$(".readonly").attr("readonly", false); 
    
    
    
</script>