<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("state"); ?>">State List</a>
                                <span>></span>
                                <p>Add State</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add State</h1>
                            </div>
                        </div>          
                        <button class="btn-primary-mro">Save</button>
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="state_id" id="state_id" value="<?php echo(!empty($state_details['state_id'])) ? $state_details['state_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="state_name">State Name</label>
                                    <input type="text" name="state_name" id="state_name" class="input-form-mro" value="<?php echo(!empty($state_details['state_name'])) ? $state_details['state_name'] : ""; ?>">
                                </div> 
                                <div class="form-group">
                                    <label for="country_name">Country Name</label>
                                    <select name="country_id" id="country_id" class="basic-single select-form-mro">
                                        <option value="">Please Select</option>
                                        <?php
                                        foreach ($countryData as $value) {
                                            $countryID = (isset($state_details['country_id']) ? $state_details['country_id'] : 0);
                                            ?> 
                                            <option value="<?php echo $value['country_id']; ?>" <?php if ($value['country_id'] == $countryID) { echo "selected"; } ?>>
                                            <?php echo $value['country_name']; ?> 
                                            </option> 
                                            <?php
                                        }
                                        ?>
                                    </select> 
                                </div>  
                            </div>  
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {
        var vRules = {
            "state_name": {required: true},
            "country_id": {required: true}
        };
        var vMessages = {
            "state_name": {required: "Please enter State Name."},
            "country_id": {required: "Please select Country Name."}
        };
        //check and save state
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>state/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        console.log(response);
                        if (response.success) {
                            alert(response.msg);
                            setTimeout(function () {
                                window.location = "<?= base_url('state') ?>";
                            }, 1000);
                        } else {
                            alert(response.msg);
                        }
                    }
                });
            }
        });

    });
</script>