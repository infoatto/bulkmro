<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter PDF Library
 *
 * @package			CodeIgniter
 * @subpackage		Libraries
 * @category		Libraries
 * @author			Muhanz
 * @license			MIT License
 * @link			https://github.com/hanzzame/ci3-pdf-generator-library
 *
 */
require_once('application/libraries/dompdf/autoload.inc.php');

use Dompdf\Dompdf;

class Pdf_dom {

    public function create($html, $filename) {
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);
        //$dompdf->setPaper('A4', 'landscape');
        $dompdf->render(); 
        $dompdf->stream($filename . '.pdf', array("Attachment" => 0));
    }

}
