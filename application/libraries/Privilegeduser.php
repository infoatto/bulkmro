<?php
class Privilegeduser
{
    //override User method
    /* public function getByUsername($user_id) 
    {	 
        $this->CI =& get_instance();
        $this->CI->load->database();
        
        $user_privilage = array(); 
        $this->CI->db->select('role_id');
        $this->CI->db->from("tbl_users");
        $this->CI->db->where("user_id='".$user_id."'"); 
        $query = $this->CI->db->get(); 
        $row_rolid = $query->result(); 
        $role_id = $row_rolid[0]->role_id;  
        
        $this->CI->db->select('t2.perm_desc');
        $this->CI->db->from("tbl_role_perm as t1");
        $this->CI->db->join("tbl_permissions as t2","t1.perm_id = t2.perm_id"); 
        $this->CI->db->where("t1.role_id = '$role_id'"); 
        $query1 = $this->CI->db->get();
        $result = $query1->result(); 
        foreach ($result as $value) { 
            $user_privilage[$value->perm_desc] = true;
        }  
        $_SESSION["mro_session"]["user_privilage"] = $user_privilage;    		
    }
 
    // check if user has a specific privilege
    public function hasPrivilege($perm) 
    {   
        $user_privilage = $_SESSION["mro_session"]["user_privilage"];
     	return isset($user_privilage[$perm]);       
    } */
   
   // Override User method
    public function getByUsername($user_id) 
    {
        $user_privilage = array();

        // Establish a MySQLi connection
        $conn = mysqli_connect("localhost", "root", "Survival@Bulkmro@123", "logistic_track_bulkmro");

        if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }

        // Fetch role ID for the user
        $sel_roleid = mysqli_query($conn, "SELECT role_id FROM tbl_users WHERE user_id='" . mysqli_real_escape_string($conn, $user_id) . "'");
        $row_rolid = mysqli_fetch_array($sel_roleid);
        $role_id = $row_rolid['role_id'];

        // Fetch permissions based on role ID
        $sql = mysqli_query($conn, "SELECT t2.perm_desc 
                                    FROM tbl_role_perm AS t1 
                                    JOIN tbl_permissions AS t2 ON t1.perm_id = t2.perm_id 
                                    WHERE t1.role_id = '" . mysqli_real_escape_string($conn, $role_id) . "'");

        // Assign each permission description to the privilege array
        while ($row = mysqli_fetch_object($sql)) {
            $user_privilage[$row->perm_desc] = true;
        }

        // Store privileges in session (updated session structure as per second code)
        $_SESSION["user_privilage"] = $user_privilage;

        // Close the MySQLi connection
        mysqli_close($conn);
    }

    // Check if the user has a specific privilege
    public function hasPrivilege($perm) 
    {
        // Retrieve the privileges from the session
        $user_privilage = $_SESSION["user_privilage"];

        // Return true if the permission exists in the user's privileges
        return isset($user_privilage[$perm]);       
    }

}
?>