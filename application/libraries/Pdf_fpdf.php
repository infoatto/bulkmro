<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/* 
 *  ======================================= 
 *  Author     : Muhammad Surya Ikhsanudin 
 *  License    : Protected 
 *  Email      : mutofiyah@gmail.com 
 *   
 *  Dilarang merubah, mengganti dan mendistribusikan 
 *  ulang tanpa sepengetahuan Author 
 *  ======================================= 
 */  

 require_once APPPATH."/third_party/fpdf17/fpdf.php"; 
class Pdf extends FPDF { 

    public function __construct() { 

        parent::__construct('P','mm',array(90,400)); 
    } 

		    var $javascript;
		    var $n_js;

		    function IncludeJS($script) {
		        $this->javascript=$script;
		    }

		    function _putjavascript() {
		        $this->_newobj();
		        $this->n_js=$this->n;
		        $this->_out('<<');
		        $this->_out('/Names [(EmbeddedJS) '.($this->n+1).' 0 R ]');
		        $this->_out('>>');
		        $this->_out('endobj');
		        $this->_newobj();
		        $this->_out('<<');
		        $this->_out('/S /JavaScript');
		        $this->_out('/JS '.$this->_textstring($this->javascript));
		        $this->_out('>>');
		        $this->_out('endobj');
		    }

		    function _putresources() {
		        parent::_putresources();
		        if (!empty($this->javascript)) {
		            $this->_putjavascript();
		        }
		    }

		    function _putcatalog() {
		        parent::_putcatalog();
		        if (isset($this->javascript)) {
		            $this->_out('/Names <</JavaScript '.($this->n_js).' 0 R>>');
		        }
		    }
		    function AutoPrint($dialog=false)
			{
			    //Embed some JavaScript to show the print dialog or start printing immediately
			    $param=($dialog ? 'true' : 'false');
			    $script="print($param);";
			    $this->IncludeJS($script);
			}



}