<?php
//start code of view option    
$dropDownCss = '';
$inputFieldCss = '';
if($view==1){
    $dropDownCss = "disabled";
    $inputFieldCss = "readonly";
}
//end code of view option
?>
<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("order"); ?>">Master Contract List</a>
                                <span>></span>  
                                <?php if ($view == 0) { ?> 
                                <p>New Master Contract</p>
                                <?php }else{ ?>
                                <p>Master Contract# <?php echo (!empty($order_details['contract_number'])) ? $order_details['contract_number'] : ""; ?></p>
                                <?php } ?>
                            </div>
                            <div class="page-title-wrapper">
                                <?php if ($view == 0) { ?>
                                <h1 class="page-title">Master Contract</h1>
                                <?php }else{ ?>
                                <h1 class="page-title">Master Contract# <?php echo (!empty($order_details['contract_number'])) ? $order_details['contract_number'] : ""; ?></h1>
                                <?php } ?> 
                            </div>
                        </div>
                        <div class="title-sec-right">
                        <?php if($view==0){ ?>
                             <button type="submit" class="btn-primary-mro">Save</button> 
                             <a href="<?php echo base_url("order"); ?>" class="btn-transparent-mro cancel">Cancel</a>
                        <?php }else{  
                            echo $containerBible.$actionEdit;
                        } ?>
                        </div>
                    </div>
                    <div id="docuement_loader" style="display: none;">
                        <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                    </div>
                    <div class="page-content-wrapper master-order-wrapper">
                        <input type="hidden" name="order_id" id="order_id" value="<?php echo(!empty($order_details['order_id'])) ? $order_details['order_id'] : ""; ?>">
                        <div class="scroll-content-wrapper master-order">
                            <div class="scroll-content-left master-order-left" id="contract-details"> 
                                <h3 class="form-group-title" id="section-mro-1">Order Details</h3>
                                <div class="form-row form-row-4">
                                    <div class="form-group">
                                        <label for="">Customer Contract Number<sup>*</sup></label>
                                        <select name="customer_contract_id" id="customer_contract_id" class="basic-single select-form-mro" onchange="getBillingShippingDetails(this.value,'customer');removeErroMsg('customer_contract_id');" <?=$dropDownCss;?>> 
                                            <option value="" disabled selected hidden>Select Customer Contract</option>
                                            <?php
                                            foreach ($customerDetails as $ccValue) {
                                                $ccID = (isset($order_details['customer_contract_id']) ? $order_details['customer_contract_id'] : 0);
                                                ?> 
                                                <option value="<?php echo $ccValue['bp_order_id']; ?>"  <?php
                                                if ($ccValue['bp_order_id'] == $ccID) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                            <?php echo $ccValue['contract_number']; ?>-<?php echo $ccValue['alias']; ?>  
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select> 
                                    </div> 
                                    <div class="form-group">
                                        <label for="">Supplier Contract Number <sup>*</sup></label>
                                        <select name="supplier_contract_id" id="supplier_contract_id" class="basic-single select-form-mro" onchange="getBillingShippingDetails(this.value,'supplier');removeErroMsg('supplier_contract_id');" <?=$dropDownCss;?>> 
                                            <option value="" disabled selected hidden>Select Supplier Contract</option>
                                            <?php
                                            foreach ($supplierDetails as $bpValue) {
                                                $bpID = (isset($order_details['supplier_contract_id']) ? $order_details['supplier_contract_id'] : 0);
                                                ?> 
                                                <option value="<?php echo $bpValue['bp_order_id']; ?>"  <?php
                                                if ($bpValue['bp_order_id'] == $bpID) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                            <?php echo $bpValue['contract_number']; ?>-<?php echo $bpValue['alias']; ?>  
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                    <div class="form-group input-edit" id="contract_number_div">
                                        <label for="">Master Contract Number <sup>*</sup></label>
                                        <input type="text" name="contract_number" id="contract_number" value="<?php echo(!empty($order_details['contract_number'])) ? $order_details['contract_number'] : ""; ?>" placeholder="Customer-Supplier Contract #" class="input-form-mro" readonly="readonly" style="width:100%">                                        
                                    </div> 
                                    
                                    <div class="form-group">
                                        <label for="">Nick Name<sup>*</sup></label>
                                        <input type="text" name="nick_name" id="nick_name" value="<?php echo (!empty($order_details['nick_name'])) ? $order_details['nick_name'] : ""; ?>" placeholder="Nick Name" class="input-form-mro" <?= $inputFieldCss; ?>>
                                    </div>
                                    
                                </div>
                                <div class="form-row form-row-3">  
                                    <div class="form-group">
                                        <label for="">CHA</label> 
                                        <select name="broker_id[]" id="broker_id" class="multiselect" multiple <?=$dropDownCss;?>>  
                                            <?php
                                            $bpIDArr1 = (isset($order_details['broker_id']) ? explode(",", $order_details['broker_id']) : array());
                                            foreach ($chaDetails as $value) { 
                                                ?> 
                                                <option value="<?php echo $value['business_partner_id']; ?>"  <?php
                                                if (in_array($value['business_partner_id'], $bpIDArr1)) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                <?php echo $value['alias']; ?> 
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select> 
                                    </div>                                    
                                    <div class="form-group">
                                        <label for="">Freight Forwarder</label> 
                                        <select name="freight_forwarder_id[]" id="freight_forwarder_id" class="multiselect" multiple <?=$dropDownCss;?>>  
                                            <?php
                                            $bpIDArr = (isset($order_details['freight_forwarder_id']) ? explode(",", $order_details['freight_forwarder_id']) : array());
                                            foreach ($freightForwarder as $value) { 
                                                ?> 
                                                <option value="<?php echo $value['business_partner_id']; ?>"  <?php
                                                if (in_array($value['business_partner_id'], $bpIDArr)) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                <?php echo $value['alias']; ?> 
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select> 
                                    </div>
                                                
<!--                                    <div class="form-group">
                                        <label for="">Freight Forwarder</label>
                                        <div class="multiselect-dropdown-wrapper" style="width:100%">
                                            <div class="md-value">
                                                Search for Freight Forwarder
                                            </div>
                                            <div class="md-list-wrapper">
                                                <input type="text" placeholder="Search" class="md-search">
                                                <div class="md-list-items ud-list">
                                                    <?php 
                                                    if ($freightForwarder) {
                                                        $bpIDArr = (isset($order_details['freight_forwarder_id']) ? explode(",", $order_details['freight_forwarder_id']) : array());
                                                        foreach ($freightForwarder as $value) {  
                                                            ?>
                                                            <div class="mdli-single ud-list-single">
                                                                <label class="container-checkbox"><?= $value['alias'] ?>
                                                                    <input type="checkbox" id="sku_id<?= $value['business_partner_id'] ?>" name="freight_forwarder_id[]" value="<?= $value['business_partner_id'] ?>" <?php if(in_array($value['business_partner_id'], $bpIDArr)){ echo 'checked'; }?>>
                                                                    <span class="checkmark-checkbox"></span>
                                                                </label>
                                                            </div>
                                                            <?php 
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div> 
                                        </div> 
                                    </div>  -->
                                    
                                    <div class="form-group">
                                        <label for="Status">Status</label>
                                        <select name="status" id="status" class="select-form-mro" <?=$dropDownCss;?>>
                                            <option value="Active" <?php echo(!empty($order_details['status']) && $order_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                            <option value="In-active" <?php echo(!empty($order_details['status']) && $order_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                        </select>
                                    </div> 
                                </div>
                                <hr class="separator mt0" id="section-mro-2">
                                <h3 class="form-group-title">Shipment Dates</h3>
                                <div class="form-row form-row-3" id="start_date_duration_div">
                                    <div class="form-group input-edit">
                                        <label for="">Shipment Start Date<sup>*</sup></label>
                                        <div style="float: left;width: 100%;"> 
                                            <input type="text" name="shipment_start_date" id="shipment_start_date" value="<?php echo (!empty($order_details['shipment_start_date'])) ? date('d-m-Y', strtotime($order_details['shipment_start_date'])) : '' ?>"  style="width: 100%" class="form-control valid input-form-mro datepicker date-form-field" readonly>                      
                                        </div> 
                                    </div>
                                    <div class="form-group input-edit">
                                        <label for="">Duration in Weeks<sup>*</sup></label>
                                        <div style="float: left;width: 100%;">
                                            <input type="number" name="duration" id="duration" value="<?php echo(!empty($order_details['duration'])) ? $order_details['duration'] : ""; ?>" style="width: 95%" placeholder="Enter Duration in Weeks" class="input-form-mro valid" aria-invalid="false" <?=$inputFieldCss;?>>
                                        </div>
                                    </div>
                                </div>
                                <hr class="separator mt0" id="section-mro-3">
                                <h3 class="form-group-title">Billing and Shipping Addresses</h3>
                                <div class="form-row form-row-3">
                                    <div class="form-group">
                                        <label for="">Customer Billing Address</label>
                                        <div id="customer_billing_details_div">
                                            <select name="customer_billing_details_id" id="customer_billing_details_id" class="basic-single select-form-mro" <?=$dropDownCss;?>>
                                                <option value="" disabled selected hidden>Select Billing Address</option> 
                                            </select>
                                        </div>
                                        <div class="ba-single ba-select" id="customer_billing_details_add_div">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Customer Shipping Address</label>
                                        <div id="customer_shipping_details_div">
                                            <select name="customer_shipping_details_id" id="customer_shipping_details_id" class="basic-single select-form-mro" <?=$dropDownCss;?>>
                                                <option value="" disabled selected hidden>Select Shipping Address</option> 
                                            </select>
                                        </div>
                                        <div class="ba-single ba-select" id="customer_shipping_details_add_div">

                                        </div>
                                    </div>
                                </div>
                                <div class="form-row form-row-3">
                                    <div class="form-group">
                                        <label for="">Supplier Billing Address</label>
                                        <div id="supplier_billing_details_div">
                                            <select name="supplier_billing_details_id" id="supplier_billing_details_id" class="basic-single select-form-mro" <?=$dropDownCss;?>>
                                                <option value="" disabled selected hidden>Select Billing Address</option> 
                                            </select>
                                        </div>
                                        <div class="ba-single ba-select" id="supplier_billing_details_add_div">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Supplier Shipping Address</label>
                                        <div id="supplier_shipping_details_div">
                                            <select name="supplier_shipping_details_id" id="supplier_shipping_details_id" class="basic-single select-form-mro" <?=$dropDownCss;?>>
                                                <option value="" disabled selected hidden>Select Shipping Address</option> 
                                            </select>
                                        </div>
                                        <div class="ba-single ba-select" id="supplier_shipping_details_add_div">

                                        </div>
                                    </div>
                                </div>
                                <hr class="separator mt0" id="section-mro-4">
                                <h3 class="form-group-title">SKUs to Deliver</h3>
                                <div class="form-row form-row-3">
                                    <div class="sku-list-wrapper add-sku-wrapper" id="sku_details_div">
                                        <?php 
                                        foreach ($skudetails as $key => $value) {  
                                        ?>
                                            <div class="sit-row">
                                                <div class="sit-single sku-item-code">
                                                    <p class="sit-single-title">Item Code</p>
                                                    <p class="sit-single-value"><?=$skudetails[$key]['sku_number'];?></p>
                                                    <input type="hidden" name="sku_id[<?= $key ?>]" id="sku_id<?= $key ?>" value="<?= (!empty($skudetails[$key]['sku_id'])) ? $skudetails[$key]['sku_id'] : '' ?>">
                                                    <input type="hidden" name="sku_number[<?= $key ?>]" id="sku_number<?= $key ?>" value="<?= (!empty($skudetails[$key]['sku_number'])) ? $skudetails[$key]['sku_number'] : '' ?>">
                                                </div>
                                                <div class="sit-single sku-pd">
                                                    <p class="sit-single-title">Product Description</p>
                                                    <p class="sit-single-value"><?=$skudetails[$key]['product_name'];?></p>
                                                </div>
                                                <div class="sit-single sku-size">
                                                    <p class="sit-single-title">UoM</p>
                                                    <p class="sit-single-value"><?=$skudetails[$key]['uom_name'];?></p>
                                                </div>
                                                <div class="sit-single sku-size">
                                                    <p class="sit-single-title">Size</p>
                                                    <p class="sit-single-value"><?=$skudetails[$key]['size_name'];?></p>
                                                </div>
                                                <div class="sit-single sku-qty form-group">
                                                    <p class="sit-single-title">Quantity<sup>*</sup></p>
                                                    <input type="number" name="sku_qty[<?= $key ?>]" id="sku_qty<?= $key ?>" value="<?= (!empty($skudetails[$key]['quantity'])) ? $skudetails[$key]['quantity'] : '' ?>" class="input-form-mro qty" placeholder="Enter quantity" <?=$inputFieldCss;?>>
                                                </div> 
                                            </div> 
                                        <?php } ?> 
                                    </div>
                                </div>
                                <hr class="separator mt0" id="section-mro-5">
                                <?php if(empty($deliverySchedule)){?>
                                    <div class="load-delivery-schedule">
                                        <h3 class="form-group-title">Delivery Schedule</h3>
                                        <p><strong>Note:</strong> Add All SKUs before Loading Delivery Schedule</p>
                                        <span class="btn-primary-mro load-ds" onclick="loadDeliverySchedule();" style="cursor: pointer;">Load Delivery Schedule </span>
                                    </div>
                                <?php } ?>
                                <div class="delivery-schedule-data" style="display:<?php echo (!empty($deliverySchedule))? 'block':''?>;">
                                    <div class="title-sec-wrapper ds-title-wrapper">
                                        <h3 class="form-group-title">Delivery Schedule</h3>
                                        <?php if($view==0){ ?>
                                            <span class="btn-primary-mro load-ds" onclick="loadDeliverySchedule();" style="cursor: pointer;"><img src="<?php echo base_url(); ?>assets/images/redo-white.svg" alt=""> Reload </span> 
                                        <?php } ?>
                                    </div>
                                    <div id="delivery_schedule">
                                        <?php 
                                        $str = '';
                                        if(!empty($deliverySchedule)){
                                            $skuDateArr = array();
                                            foreach ($deliverySchedule as $key => $value) {  
                                                $skuDateArr[date('M Y', strtotime($value['week_date']))][$value['week_date']][$value['sku_id']]=array('qty'=>$value['sku_qty'],'sku'=>$value['sku_number']);
                                            }
                                            $str = ''; 
                                            $skuNumber_cnt = 0; 
                                            foreach ($skuDateArr as $monthYrs => $weekArr) {  
                                                   $str = $str.'<div class="delivery-schedule-wrapper">';
                                                       $str = $str.'<div class="ds-single-wrapper"> 
                                                                   <div class="ds-row ds-row-header">
                                                                       <div class="ds-month">
                                                                           <a href="#/" class="ds-toggle-icon"></a> <input type="text" class="input-form-mro" value="'.$monthYrs.'" '.$inputFieldCss.'>
                                                                       </div> 
                                                                   </div>';

                                                      $skuNumber_cnt = 0;
                                                      $qtyTotalArr = array(); 
                                                      $weeklyGrandTotal = 0;
                                                       foreach ($weekArr as $weekDate => $valueSKU) {
                                                           $skuNumber_str = '';
                                                           $skuQty_str = ''; 
                                                           $weekTotal = 0;
                                                           foreach ($valueSKU as $skuID => $skuQtyNumber) { 
                                                               if($skuNumber_cnt == 0){
                                                                   $uom = '';
                                                                   if(!empty($uomDetails[$skuID])){
                                                                       $uom = $uomDetails[$skuID];
                                                                   } 
                                                                   $skuNumber_str .= '<div class="ds-sku"><input type="hidden" name="schedule_skuNumber['.$skuID.']" class="input-form-mro" value="'.$skuQtyNumber['sku'].'">'.$skuQtyNumber['sku'].' ('.$uom.')</div>';  
                                                               }
                                                               $qty = $skuQtyNumber['qty'];
                                                               $skuQty_str .='<div class="ds-sku">
                                                                               <input type="text" name="schedule_skuQty['.$weekDate.']['.$skuID.']" class="input-form-mro" value="'.$qty.'" '.$inputFieldCss.'>
                                                                           </div>';

                                                               $qtyTotalArr[$skuID][] = $qty; 
                                                               $weekTotal += $qty;  
                                                           } 
                                                           
                                                           //weekly total header
                                                           $skuNumber_str .= '<div class="ds-sku">Weekly Total</div>';  
                                                           //weekly total row
                                                           $skuQty_str .='<div class="ds-sku">'.$weekTotal.'</div>';
                                                           
                                                           $weeklyGrandTotal += $weekTotal;
                                                           
                                                           //echo '<pre>'; print_r($weekTotalArr);die;
                                                          
                                                           //sku week date, sku number and qty code
                                                           $str .= '<div class="ds-expand-content" >';
                                                                    if($skuNumber_cnt == 0){
                                                                       $str .= '<div class="ds-row ds-row-week ds-row-week-header">
                                                                            <div class="ds-month"></div>
                                                                            <div class="ds-weeks">Weeks</div>
                                                                            '.$skuNumber_str.'
                                                                        </div>';
                                                                    }
                                                                    $str .= '<div class="ds-row ds-row-week">
                                                                         <div class="ds-month"></div>
                                                                         <div class="ds-weeks">
                                                                             <input type="text"  name="schedule_weekdate[]" class="input-form-mro" value="'.$weekDate.'" readonly>
                                                                         </div>
                                                                         '.$skuQty_str.' 
                                                                     </div> 
                                                                   </div>';
                                                            //end week date, sku number and qty code
                                                           $skuNumber_cnt++;
                                                       } 
                                                       //code for month wise qty total  $weeklyGrandTotal
                                                       $str .= '<div class="ds-row ds-row-total">
                                                                   <div class="ds-month"></div>
                                                                   <div class="ds-weeks">Total</div>'; 
                                                                   foreach ($qtyTotalArr as $skuId => $qty) {
                                                                       $qtyTotalData = 0;
                                                                       foreach ($qty as $key => $qtyTotal) {
                                                                           $qtyTotalData += $qtyTotal;
                                                                       }
                                                                       $str .='<div class="ds-sku">
                                                                       '.$qtyTotalData.'
                                                                       </div>'; 
                                                                   }  
                                                              //weekly grand total
                                                              $str .= '<div class="ds-sku">
                                                                       '.$weeklyGrandTotal.'
                                                                       </div></div>';
                                                        //end total

                                                       $str .='</div></div>'; 
                                           }  
                                           
                                         }
                                         echo $str;
                                         ?>
                                    </div> 
                                </div> 
                            </div>
                            <div class="scroll-content-right master-order-right">
                                <div class="vertical-scroll-links">
                                    <a href="#section-mro-1" class="active">Contract Details</a>
                                    <a href="#section-mro-2">Shipment Dates</a>
                                    <a href="#section-mro-3">Billing and Shipping Addresses</a>
                                    <a href="#section-mro-4">SKUs to Deliver</a>
                                    <a href="#section-mro-5">Delivery Schedule</a>
                                </div>
                            </div>
                        </div>


                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>

<!-- JS & CSS library of MultiSelect plugin --> 
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.multiselect.css" /> 
<script src="<?php echo base_url(); ?>assets/js/plugin/multiselect/jquery.multiselect.js" type="text/javascript"></script>

<script> 
    
    jQuery('.multiselect').multiselect({
        columns: 1,
        placeholder: 'Please Select',
        search: true,
        selectAll: true, 
    });   
    
    //get customer and supplier contract and fill in master contract
    function getCustomerContact(isedit=0){
        var customer_id = $('#customer_contract_id').val();
        var supplier_id = $('#supplier_contract_id').val();
        if(customer_id > 0 && supplier_id > 0){
            var act = "<?php echo base_url(); ?>order/getOrderContract";
            $.ajax({
                type: 'GET',
                url: act,
                data: {customer_id: customer_id,supplier_id: supplier_id},
                dataType: "json",
                success: function (data) {
                    if(isedit==0){ 
                        $('#contract_number_div').html(data.contract);
                    }
                }
            });
        }
    }

    //remove sku details
    $(document).on("click", '.inner_remove', function () {
        var tr_inner_count = $(this).closest('tr').attr('tr_inner_count');
        $(this).closest('tr').remove();
    });

</script>
<script>
    //code for get business details and shipping details in dropdown also get payment and incoterm
    function getBillingShippingDetails(bpo_id = 0, type, billing_id = '', shipping_id = '', isedit = 0) {
        //get customer supplier contract number and fill in master contract
        getCustomerContact(isedit);
        var act = "<?php echo base_url(); ?>order/getBusinessPartner";
        $.ajax({
            type: 'GET',
            url: act,
            data: {type: type, bpo_id: bpo_id, billing_id: billing_id, shipping_id: shipping_id,isView:<?=$view?>,isedit:isedit},
            dataType: "json",
            success: function (data) {
                if(type =='customer'){
                    $('#customer_billing_details_div').html(data.billing);
                    $('#customer_shipping_details_div').html(data.shipping); 
                    if(isedit == 0){
                        $('#sku_details_div').html(data.sku);
                        $('#start_date_duration_div').html(data.duration_date);
                        $('.datepicker').datepicker({
                            format: 'dd-mm-yyyy'
                        });
                    }
                    
                    getBillAddress(data.billing_details_id,0);
                    getShipAddress(data.shipping_details_id,0);
                }else{
                    $('#supplier_billing_details_div').html(data.billing);
                    $('#supplier_shipping_details_div').html(data.shipping); 
                    
                    getBillAddress(data.billing_details_id,1);
                    getShipAddress(data.shipping_details_id,1);
                }
                
                $('.basic-single').select2();
            }
        });  
    }
    //code for show billing address details
    function getBillAddress(bd_id = 0,cnt) {
        var act = "<?php echo base_url(); ?>order/getBillingAddress";
        $.ajax({
            type: 'GET',
            url: act,
            data: {bd_id: bd_id},
            dataType: "json",
            success: function (data) {
                if(cnt==0){
                    $('#customer_billing_details_add_div').html(data.billing);
                }else{
                    $('#supplier_billing_details_add_div').html(data.billing);
                }
            }
        });
    }
    //code for show shipping address details
    function getShipAddress(sd_id = 0,cnt) {
        var act = "<?php echo base_url(); ?>order/getShipAddress";
        $.ajax({
            type: 'GET',
            url: act,
            data: {bd_id: sd_id},
            dataType: "json",
            success: function (data) {
                if(cnt==0){
                    $('#customer_shipping_details_add_div').html(data.billing);
                }else{
                    $('#supplier_shipping_details_add_div').html(data.billing);
                }
            }
        });
    }


//code for edit case get business details and shipping details in dropdown also get payment and incoterm
<?php if (!empty($order_details['order_id'])) { ?>
        getBillingShippingDetails('<?php echo(!empty($order_details['customer_contract_id'])) ? $order_details['customer_contract_id'] : "0"; ?>','customer', '<?php echo(!empty($order_details['customer_billing_details_id'])) ? $order_details['customer_billing_details_id'] : "0"; ?>', '<?php echo(!empty($order_details['customer_shipping_details_id'])) ? $order_details['customer_shipping_details_id'] : "0"; ?>', '1');
        getBillingShippingDetails('<?php echo(!empty($order_details['supplier_contract_id'])) ? $order_details['supplier_contract_id'] : "0"; ?>','supplier', '<?php echo(!empty($order_details['supplier_billing_details_id'])) ? $order_details['supplier_billing_details_id'] : "0"; ?>', '<?php echo(!empty($order_details['supplier_shipping_details_id'])) ? $order_details['supplier_shipping_details_id'] : "0"; ?>', '1');
<?php } ?>

</script> 
<script>
    function loadDeliverySchedule(){ 
        var data = $('#addEditForm').serialize();
        var act = "<?php echo base_url(); ?>order/loadDeliverySchedule";
        $.ajax({  
            url: act,
            type: 'POST', 
            data: data,
            dataType: 'json',
            cache: false,
            clearForm: false,
            success: function (data) {  
                $('#delivery_schedule').html(data.str);
                
            }
        });
    }
</script> 
<script>
    
    function qtyCheckValidation(){
        var isCheck = false;
        var count = $('.qty').length; 
        for (let i = 0; i < count; i++) {
             if($('#sku_qty'+i).val() > 0){
                 var isCheck = true;
             }
        } 
        //console.log(isCheck);
        return isCheck;
    } 

    $(document).ready(function () {  

        var vRules = {
            "contract_number": {required: true},
            "customer_contract_id": {required: true},
            "supplier_contract_id": {required: true},
            "nick_name": {required: true}, 
            "shipment_start_date": {required: true},
            "duration": {required: true} 
        };
        var vMessages = {
            "contract_number": {required: "Please enter master contract number."},
            "customer_contract_id": {required: "Please select customer contract number."},
            "supplier_contract_id": {required: "Please select supplier contract number."},
            "nick_name": {required: "Please enter nick name."},
            "shipment_start_date": {required: "Please select shipment start date."},
            "duration": {required: "Please enter duration in weeks."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function(error, e) {
              e.parents('.form-group').append(error);
            },
            submitHandler: function (form)
            { 
                var qtyCheck = qtyCheckValidation(); 
                if(qtyCheck){  
                    var act = "<?php echo base_url(); ?>order/submitForm";
                    $("#addEditForm").ajaxSubmit({
                        url: act,
                        type: 'post',
                        dataType: 'json',
                        cache: false,
                        clearForm: false,
                        beforeSubmit: function (arr, $form, options) {
                            $(".btn-primary-mro").hide();
                            $("#docuement_loader").show();
                        },
                        success: function (response) { 
                            $("#docuement_loader").hide();
                            showInsertUpdateMessage(response.msg,response);                        
                            if (response.success) { 
                                setTimeout(function () {                                
                                    window.location = "<?= base_url('order') ?>";
                                }, 3000);
                            }  
                            $(".btn-primary-mro").show(); 
                        }
                    });
                }
            }
        });

    }); 
    
    <?php  if($view==1){ ?>
        $('.ms-options-wrap').css('pointer-events','none');
    <?php } ?>
</script> 
 