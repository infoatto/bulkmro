<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Order extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Ordermodel', 'ordermodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("MasterContractList")){
            redirect('dashboard');
        }
    }

    function index() {
        $data = array();
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('order/index', $data);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() { 
        $edit_datas = array();
        $edit_datas['supplierDetails'] = array();
        $edit_datas['customerDetails'] = array();
        $edit_datas['chaDetails'] = array();
        $edit_datas['freightForwarder'] = array();
        $edit_datas['order_details'] = array();

        $edit_datas['skuDetails'] = array();
        $edit_datas['skudetails'] = array();
        $edit_datas['deliverySchedule'] = array();
        $edit_datas['uomDetails'] = array();
        
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $order_id = $url_prams['id'];
            $result = $this->common->getData("tbl_order", "*", array("order_id" => $order_id));
            if (!empty($result)) {
                $edit_datas['order_details'] = $result[0];
            }
            //order sku details data
            $skuData = $this->ordermodel->getOrderSkuDetails($order_id);
            if (!empty($skuData)) {
                $edit_datas['skudetails'] = $skuData;
            }
            //delivery schedule data
            $deliveryResult = $this->common->getData("tbl_order_delivery_schedule", "*", array("order_id" => $order_id));
            if (!empty($deliveryResult)) {
                $edit_datas['deliverySchedule'] = $deliveryResult;
                //get UoM
                $edit_datas['uomDetails'] = $this->getUoM();
            }
            
            $edit_datas['actionEdit'] = '';
            if($this->privilegeduser->hasPrivilege("MasterContractAddEdit") || $this->privilegeduser->hasPrivilege("MasterContractEdit")){   
                $edit_datas['actionEdit'] = '<a href="order/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $order_id), '+/', '-_'), '=') . '" title="Edit" class="text-center btn-primary-mro"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" >Edit</a>';
            }
            
            $edit_datas['containerBible'] = '';
            if($this->privilegeduser->hasPrivilege("ContainersDetailsList")){   
                $url = base_url()."listOfContainer?text=" . rtrim(strtr(base64_encode("order_id=" . $order_id), '+/', '-_'), '=');
                $edit_datas['containerBible'] = '<a href="'.$url.'" title="Edit" class="text-center btn-transparent-mro cancel">View Container Bible</a>';
            }
            
        }

        //supplier data
        $customerDetails = $this->ordermodel->getCustomerContract();
        if (!empty($customerDetails)) {
            $edit_datas['customerDetails'] = $customerDetails;
        }

        //supplier data
        $supplierDetails = $this->ordermodel->getSupplierContract();
        if (!empty($supplierDetails)) {
            $edit_datas['supplierDetails'] = $supplierDetails;
        }

        //CHA data
        $chaDetails = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias", array("business_type" => "Vendor", "business_category" => "Customs Broker", "status" => "Active"),'alias','asc');
        if (!empty($chaDetails)) {
            $edit_datas['chaDetails'] = $chaDetails;
        }

        //CHA data 
        $freightForwarder = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias", array("business_type" => "Vendor", "business_category" => "Freight Forwarder", "status" => "Active"),'alias','asc');
        if (!empty($freightForwarder)) {
            $edit_datas['freightForwarder'] = $freightForwarder;
        }

        //sku data
        $skuDetails = $this->common->getData("tbl_sku", "sku_id,sku_number", array("status" => "Active"),'sku_number','asc');
        if (!empty($skuDetails)) {
            $edit_datas['skuDetails'] = $skuDetails;
        }
        
        $edit_datas['view'] = isset($_GET['view'])?$_GET['view']:0; 
       
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('order/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        $get_result = $this->ordermodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, $get_result['query_result'][$i]->contract_number);
                array_push($temp, $get_result['query_result'][$i]->nick_name);
                array_push($temp, date('d-M-Y', strtotime($get_result['query_result'][$i]->shipment_start_date)));
                array_push($temp, $get_result['query_result'][$i]->duration);
                array_push($temp, $get_result['query_result'][$i]->status);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("MasterContractAddEdit") || $this->privilegeduser->hasPrivilege("MasterContractEdit")){   
                    $actionCol = '<a href="order/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->order_id), '+/', '-_'), '=') . '" title="Edit" class="text-center"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                if($this->privilegeduser->hasPrivilege("MasterContractView")){   
                    $actionCol .= '<a href="order/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->order_id), '+/', '-_'), '=') . '&view=1" title="View" class="text-center" style="float:right;"><img src="' . base_url() . 'assets/images/view-btn-icon.svg" alt="Edit" ></a>';
                } 
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")){
            redirect('dashboard');
        } 
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "contract_number = '" . $this->input->post('contract_number') . "' ";
            if (!empty($this->input->post("order_id"))) {
                $condition .= " AND order_id <> " . $this->input->post("order_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_order", "order_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['order_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Master Contract Number already exist...'));
                exit;
            }

            $data = array();
            $data['contract_number'] = $this->input->post('contract_number');
            $data['customer_contract_id'] = $this->input->post('customer_contract_id');
            $data['supplier_contract_id'] = $this->input->post('supplier_contract_id');
            $data['nick_name'] = $this->input->post('nick_name');
            $data['broker_id'] = !empty($this->input->post('broker_id'))? implode(",", $this->input->post('broker_id')):'';
            $data['freight_forwarder_id'] = !empty($this->input->post('freight_forwarder_id'))? implode(",", $this->input->post('freight_forwarder_id')):''; 
            $data['shipment_start_date'] = !empty($this->input->post('shipment_start_date')) ? date("Y-m-d", strtotime($this->input->post('shipment_start_date'))) : NULL;
            $data['duration'] = $this->input->post('duration');
            $data['customer_billing_details_id'] = $this->input->post('customer_billing_details_id');
            $data['customer_shipping_details_id'] = $this->input->post('customer_shipping_details_id');
            $data['supplier_billing_details_id'] = $this->input->post('supplier_billing_details_id');
            $data['supplier_shipping_details_id'] = $this->input->post('supplier_shipping_details_id');
            $data['status'] = $this->input->post('status');
            //echo $data['shipment_start_date'];die;
            if (!empty($this->input->post("order_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_order", $data, array("order_id" => $this->input->post("order_id")));

                $data1['order_id'] = $this->input->post("order_id");
                $resultdel = $this->common->deleteRecord('tbl_order_sku', array("order_id" => $this->input->post("order_id")));
                if ($resultdel) {
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $result1 = $this->common->insertData('tbl_order_sku', $data1, '1');
                        }
                    } 
                }
                //delivery schedule delete and insert
                $data2['order_id'] = $this->input->post("order_id"); 
                $resultdel_schedule = $this->common->deleteRecord('tbl_order_delivery_schedule', array("order_id" => $this->input->post("order_id")));
                //if ($resultdel_schedule) {
                    if (!empty($this->input->post('schedule_skuQty'))) {
                        foreach ($this->input->post('schedule_skuQty') as $weekDate => $skuData) {
                            $data2['week_date'] = $weekDate;
                            foreach ($skuData as $skuID => $skuQty) {
                                $data2['sku_id'] = $skuID;
                                $data2['sku_number'] = $this->input->post('schedule_skuNumber')[$skuID];
                                $data2['sku_qty'] = $skuQty; 
                                $result2 = $this->common->insertData('tbl_order_delivery_schedule', $data2, '1');
                            } 
                        }
                    } 
                //} 
              
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_order", $data, "1");
                if ($result) {
                    $data1['order_id'] = $result;
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $result1 = $this->common->insertData('tbl_order_sku', $data1, '1');
                        }
                    } 
                    //delivery schedule insert
                    $data2['order_id'] = $result;  
                    if (!empty($this->input->post('schedule_skuQty'))) {
                        foreach ($this->input->post('schedule_skuQty') as $weekDate => $skuData) {
                            $data2['week_date'] = $weekDate;
                            foreach ($skuData as $skuID => $skuQty) {
                                $data2['sku_id'] = $skuID;
                                $data2['sku_number'] = $this->input->post('schedule_skuNumber')[$skuID];
                                $data2['sku_qty'] = $skuQty; 
                                $result2 = $this->common->insertData('tbl_order_delivery_schedule', $data2, '1');
                            } 
                        }
                    }                     
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    } 
    
    function importOrder() { 
        if(!$this->privilegeduser->hasPrivilege("MasterContractImport")){
            redirect('dashboard');
        }
        require_once('application/libraries/SimpleXLSX.php');
       
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["excelfile"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["excelfile"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die; 
                $xlsx = new SimpleXLSX($_FILES['excelfile']['tmp_name']);
                
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) { 
                        if (trim($data[0]) != "Customer Contract Number") {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        if (trim($data[1]) != "Supplier Contract Number" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[2]) != "Master Contract Number" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        } 
                        
                        if (trim($data[3]) != "CHA" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        } 
                        
                        if (trim($data[4]) != "Freight Forwarder" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        } 
                        
                        if (trim($data[5]) != "Status" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }   
                        
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[2])){
                            continue;
                        } 
                        
                        if (trim($data[0]) == "") {
                            $error .= "Customer Contract Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[1]) == "") {
                            $error .= "Supplier Contract Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[2]) == "") {
                            $error .= "Master Contract Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                        
                        if (trim($data[5]) != "Active" && trim($data[2]) != "In-active") {
                            $error .= "Status cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[2])){
                            continue;
                        }  
                        
                        $customer_contract_number = str_replace("'", "&#39;", trim($data[0]));
                        $supplier_contract_number = str_replace("'", "&#39;", trim($data[1])); 
                        
                        $customer_condition_contract = "contract_number = '" . $customer_contract_number . "' AND status='Active' "; 
                        $chk_customer_contract_sql = $this->common->Fetch("tbl_bp_order", "bp_order_id,shipment_start_date,duration,billing_details_id,shipping_details_id", $customer_condition_contract);
                        $rs_customer_contract = $this->common->MySqlFetchRow($chk_customer_contract_sql, "array"); 
                        
                        $supplier_condition_contract = "contract_number = '" . $supplier_contract_number . "' AND status='Active' "; 
                        $chk_supplier_contract_sql = $this->common->Fetch("tbl_bp_order", "bp_order_id,billing_details_id,shipping_details_id", $supplier_condition_contract);
                        $rs_supplier_contract = $this->common->MySqlFetchRow($chk_supplier_contract_sql, "array"); 
                        
                        if (!empty($rs_customer_contract[0]['bp_order_id']) && !empty($rs_supplier_contract[0]['bp_order_id'])) {  
                             
                            $master_contract_number = str_replace("'", "&#39;", trim($data[2])); 
                            $broker_name = str_replace("'", "&#39;", trim($data[3]));
                            $ff_name = str_replace("'", "&#39;", trim($data[4]));
                            $status = str_replace("'", "&#39;", trim($data[5]));
                            $nickName = str_replace("'", "&#39;", trim($data[6]));
                            
                            $customer_contract_id = $rs_customer_contract[0]['bp_order_id'];
                            $shipment_start_date = date('Y-m-d', strtotime($rs_customer_contract[0]['shipment_start_date']));
                            $week = (int)$rs_customer_contract[0]['duration'];
                            $customer_billing_details_id = $rs_customer_contract[0]['billing_details_id'];
                            $customer_shipping_details_id = $rs_customer_contract[0]['shipping_details_id'];
                            $supplier_billing_details_id = $rs_supplier_contract[0]['billing_details_id'];
                            $supplier_shipping_details_id = $rs_supplier_contract[0]['shipping_details_id']; 
                             
                            //get CHA id
                            $broker_id = '';
                            $broker_id_arr = array();
                            $broker_nameArr = explode(",", trim($broker_name)); 
                            if(!empty($broker_nameArr)){
                                foreach ($broker_nameArr as $value) {
                                    $condition_broker = "alias = '" . trim($value) . "' AND business_category ='Customs Broker' "; 
                                    $chk_broker_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $condition_broker);
                                    $rs_broker = $this->common->MySqlFetchRow($chk_broker_sql, "array"); 
                                    if (!empty($rs_broker[0]['business_partner_id'])) {
                                        $broker_id_arr[] = $rs_broker[0]['business_partner_id'];
                                    } 
                                } 
                            }
                            if(!empty($broker_id_arr)){
                                $broker_id = implode(",", $broker_id_arr);
                            }
                            
                            //get FF id
                            $freight_forwarder_id = '';
                            $freight_forwarder_id_arr = array();
                            $freight_nameArr = explode(",", trim($ff_name)); 
                            if(!empty($freight_nameArr)){
                                foreach ($freight_nameArr as $value) {
                                    $condition_ff = "alias = '" . trim($value) . "' AND business_category ='Freight Forwarder' "; 
                                    $chk_ff_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $condition_ff);
                                    $rs_ff = $this->common->MySqlFetchRow($chk_ff_sql, "array");
                                    if (!empty($rs_ff[0]['business_partner_id'])) {
                                        $freight_forwarder_id_arr[] = $rs_ff[0]['business_partner_id'];
                                    } 
                                } 
                            }
                            if(!empty($freight_forwarder_id_arr)){
                                $freight_forwarder_id = implode(",", $freight_forwarder_id_arr);
                            } 
                            
                            $data = array();
                            $data['contract_number'] = $master_contract_number;
                            $data['customer_contract_id'] = $customer_contract_id;
                            $data['supplier_contract_id'] = $rs_supplier_contract[0]['bp_order_id'];
                            $data['nick_name'] = $nickName;
                            $data['broker_id'] = $broker_id;
                            $data['freight_forwarder_id'] = $freight_forwarder_id;
                            $data['shipment_start_date'] = $shipment_start_date;
                            $data['duration'] = $week;
                            $data['customer_billing_details_id'] = $customer_billing_details_id;
                            $data['customer_shipping_details_id'] = $customer_shipping_details_id;
                            $data['supplier_billing_details_id'] = $supplier_billing_details_id;
                            $data['supplier_shipping_details_id'] = $supplier_shipping_details_id;
                            $data['status'] = $status; 
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");  

                            $condition_order = "contract_number = '" . $master_contract_number . "' AND status='" . $status . "' "; 
                            $chk_order_sql = $this->common->Fetch("tbl_order", "order_id", $condition_order);
                            $rs_order = $this->common->MySqlFetchRow($chk_order_sql, "array"); 
                            $order_id = 0; 
                             //update-insert bussiness partner contract data 
                            if (!empty($rs_order[0]['order_id'])) {                            
                                $result = $this->common->updateData("tbl_order", $data, array("order_id" => $rs_order[0]['order_id']));  
                                if ($result) {
                                    $order_id = $rs_order[0]['order_id'];
                                    $imported++;
                                } else {
                                    $notimported++;
                                }
                            }else{
                                 //insert data 
                                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['created_on'] = date("Y-m-d H:i:s");
                                $result = $this->common->insertData("tbl_order", $data, "1");
                                if ($result) {
                                    $order_id = $result;
                                    $imported++;
                                } else {
                                    $notimported++;
                                } 
                            } 
                            
                            if($order_id > 0){ 
                                $skuDetails = $this->common->getData("tbl_bp_order_sku", "sku_id,quantity", array("bp_order_id" => $customer_contract_id));
                                if (!empty($skuDetails)) {
                                    
                                    $skuIdsArr = array();
                                    $skuNumberArr = array();
                                    $skuQtyArr = array(); 
                                    
                                    //order sku code
                                    $data1 = array();
                                    $data1['order_id'] = $order_id;
                                    $resultdel = $this->common->deleteRecord('tbl_order_sku', array("order_id" => $order_id));
                                    foreach ($skuDetails as $value) {  
                                        $skuIdsArr[] = $value['sku_id']; 
                                        $skuQtyArr[] = $value['quantity']; 
                                        
                                        $condition_sku = "sku_id = '" . $value['sku_id'] . "' "; 
                                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_number", $condition_sku);
                                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array");  
                                        if (!empty($rs_sku[0]['sku_number'])) {                            
                                            $skuNumberArr[] = $rs_sku[0]['sku_number'];
                                        } 
                                        
                                        $data1['sku_id'] = $value['sku_id'];
                                        $data1['quantity'] = $value['quantity'];
                                        $result1 = $this->common->insertData('tbl_order_sku', $data1, '1');
                                    }  
                                    
                                    
                                    //load delivery schedule code
                                    $days = ($week*7);
                                    //echo $days;die;
                                    $startDate = $shipment_start_date;  
                                    $endDate = date("Y-m-d", strtotime($startDate." +$days days")); 

                                    $endDate = strtotime($endDate);
                                    $dataArr = array();  
                                    for($i = strtotime('Monday', strtotime($startDate)); $i < $endDate; $i = strtotime('+1 week', $i)){
                                        $dataArr[]= date('Y-m-d', $i);
                                    } 
                                    
                                    $skuDateArr = array();  
                                    foreach ($dataArr as $keyValue => $valueData) {
                                        foreach ($skuIdsArr as $keySKU => $valueSKU) {
                                            $skuDateArr[$valueData][$valueSKU]=array('qty'=>$skuQtyArr[$keySKU],'sku'=>$skuNumberArr[$keySKU]);
                                        } 
                                    } 
                                    
                                    $resultde2 = $this->common->deleteRecord('tbl_order_delivery_schedule', array("order_id" => $order_id)); 
                                    $data2 = array(); 
                                    $data2['order_id'] = $order_id;
                                    
                                    $first_index_date = 0; 
                                    $qtyTotalArr = array();
                                    foreach ($skuDateArr as $weekDate => $valueSKU) {  
                                        foreach ($valueSKU as $skuID => $skuQtyNumber) {  
                                            $qty = (int)($skuQtyNumber['qty']/$week);
                                            //set float value for first key
                                            $float_value = 0;
                                            if($first_index_date == 0){
                                                $divided_value = ($skuQtyNumber['qty']/$week);
                                                if(is_float($divided_value)){
                                                    $without_float_total = ((int)$divided_value) * $week;
                                                    $float_value = ($skuQtyNumber['qty'] - $without_float_total);
                                                }
                                                $qty = $qty+$float_value;                                
                                            } 

                                            $data2['week_date'] = $weekDate; 
                                            $data2['sku_id'] = $skuID;
                                            $data2['sku_number'] = $skuQtyNumber['sku'];
                                            $data2['sku_qty'] = $qty; 
                                            $result2 = $this->common->insertData('tbl_order_delivery_schedule', $data2, '1');  
                                        }
                                        $first_index_date++; 
                                    }   
                                    
                                }  
                                
                            } 
                        } 
                    }
                   
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    } 
    
    //get UoM 
    function getUoM(){
        $result = array();
        $uomDetail = $this->ordermodel->getUoM(); 
        if (!empty($uomDetail)) {
            foreach ($uomDetail as $key => $value) {
                $result[$value['sku_id']] = $value['uom_name'];
            }
        }
        return $result;
    }
    
    //get customer and supplier contract number
    function getOrderContract() {
        $customer_id = $_GET['customer_id'];
        $supplier_id = $_GET['supplier_id'];
        $str = 0;
        if ($customer_id > 0 && $supplier_id > 0) {
            $detail = $this->common->getData("tbl_bp_order", "contract_number", array("bp_order_id" => $customer_id, "status" => "Active"));
            if (!empty($detail)) {
                $str = $detail[0]['contract_number'];
            }
            $suppContract = $this->common->getData("tbl_bp_order", "contract_number", array("bp_order_id" => $supplier_id, "status" => "Active"));
            if (!empty($suppContract)) {
                $str = $str . '-' . $suppContract[0]['contract_number'];
            }
        }
        
        $html = '<label for="">Master Contract Number <sup>*</sup></label>
                 <input type="text" name="contract_number" id="contract_number" value="'.$str.'" placeholder="Customer-Supplier Contract #" class="input-form-mro" readonly="readonly" style="width:100%">';
        echo json_encode(array('contract' => $html));
        exit;
    }

    //business partner billing and shipping details dropdown
    function getBusinessPartner() {
        $type = $_GET['type'];
        $bpo_id = $_GET['bpo_id'];
        $billing_id = $_GET['billing_id'];
        $shipping_id = $_GET['shipping_id'];
        $view = $_GET['isView'];
        $isedit = $_GET['isedit'];
        $dropDownCss = '';
        if($view==1){
            $dropDownCss = "disabled"; 
        }
        
        $billing = '';
        $shipping = '';
        $cnt = 0;
        if ($type == 'customer') {
            $billing = 'customer_billing_details_id';
            $shipping = 'customer_shipping_details_id';
        } else {
            $billing = 'supplier_billing_details_id';
            $shipping = 'supplier_shipping_details_id';
            $cnt = 1;
        }

        $billingstr = "<select name='" . $billing . "' id='" . $billing . "' onchange='getBillAddress(this.value," . $cnt . ")' class='basic-single select-form-mro' $dropDownCss>"
                . "<option value='0'>Select Billing Address</option>";
        $shippingstr = "<select name='" . $shipping . "' id='" . $shipping . "' onchange='getShipAddress(this.value," . $cnt . ")' class='basic-single select-form-mro' $dropDownCss>"
                . "<option value='0'>Select Shipping Address</option>";
        $sku = '';
        $duration = '';
        $date = '';
        $billing_details_id = $billing_id;
        $shipping_details_id = $shipping_id;
        if ($bpo_id > 0 && $type != '') {
            //billing details  
            $selected = '';
            if($isedit==0){
                $selected = 'selected';
            }
            $billingDetail = $this->ordermodel->getBillingDetails("tbl_billing_details", $bpo_id);
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) { 
                    if(!empty($value['address_line_1']) || !empty($value['address_line_2'])){
                        $address = $value['address_line_1'] . " " . $value['address_line_2'] . "";
                        if ($billing_id == $value['billing_details_id']) {
                            $selected = 'selected';
                        }
                        if($isedit==0){
                            $billing_details_id = $value['billing_details_id'];
                        }                    
                        $billingstr = $billingstr . "<option value='" . $value['billing_details_id'] . "' $selected >" . $address . "</option>";
                    }
                    if ($type == 'customer') {
                        $date = date('d-m-Y', strtotime($value['shipment_start_date']));
                        $duration = $value['duration'];
                    }
                }
            }
            $billingstr = $billingstr . "</select>";
            //shipping details
            $shippingDetail = $this->ordermodel->getShippingDetails("tbl_shipping_details", $bpo_id);
            if (!empty($shippingDetail)) {
                foreach ($shippingDetail as $key => $value) { 
                    if(!empty($value['address_line_1']) || !empty($value['address_line_2'])){
                        $address = $value['address_line_1'] . " " . $value['address_line_2'] . "";
                        if ($shipping_id == $value['shipping_details_id']) {
                            $selected = 'selected';
                        }
                        if($isedit==0){
                            $shipping_details_id = $value['shipping_details_id'];
                        }
                        $shippingstr = $shippingstr . "<option value='" . $value['shipping_details_id'] . "' $selected >" . $address . "</option>";
                    }
                }
            }
            $shippingstr = $shippingstr . "</select>";
            if ($type == 'customer') {
                //sku details
                $skuDetail = $this->ordermodel->getskuData($bpo_id);
                if (!empty($skuDetail)) {
                    foreach ($skuDetail as $key => $value) {
                        $sku = $sku . '<div class="sit-row">
                            <div class="sit-single sku-item-code">
                                <p class="sit-single-title">Item Code</p>
                                <p class="sit-single-value">'.$value["sku_number"].'</p>
                                <input type="hidden" name="sku_id[' . $key . ']" id="sku_id' . $key . '" value="' . $value["sku_id"] . '">
                                <input type="hidden" name="sku_number[' . $key . ']" id="sku_number' . $key . '" value="' . $value["sku_number"] . '">
                            </div>
                            <div class="sit-single sku-pd">
                                <p class="sit-single-title">Product Description</p>
                                <p class="sit-single-value">' . $value["product_name"] . '</p>
                            </div>
                            <div class="sit-single sku-size">
                                <p class="sit-single-title">UoM</p>
                                <p class="sit-single-value">' . $value["uom_name"] . '</p>
                            </div>
                            <div class="sit-single sku-size">
                                <p class="sit-single-title">Size</p>
                                <p class="sit-single-value">' . $value["size_name"] . '</p>
                            </div>
                            <div class="sit-single sku-qty form-group">
                                <p class="sit-single-title">Quantity<sup>*</sup></p>
                                <input type="number" name="sku_qty[' . $key . ']" id="sku_qty' . $key . '" value="' . $value["quantity"] . '" class="input-form-mro qty" placeholder="Enter quantity">
                            </div> 
                        </div>';
                    }
                }
            }
        }
        
        $duration_date = '<div class="form-group input-edit">
                            <label for="">Shipment Start Date<sup>*</sup></label>
                            <div style="float: left;width: 100%;">
                                <input type="text" name="shipment_start_date" id="shipment_start_date" value="'.$date.'" style="width: 100%" class="form-control valid input-form-mro datepicker date-form-field" readonly>                                                                
                            </div> 
                        </div>
                        <div class="form-group input-edit">
                            <label for="">Duration in Weeks<sup>*</sup></label>
                            <div style="float: left;width: 100%;">
                                <input type="number" name="duration" id="duration" value="'.$duration.'" style="width: 100%" placeholder="Enter Duration in Weeks" class="input-form-mro valid" aria-invalid="false" >
                            </div>
                        </div>'; 
        
        //echo $duration_date;die;
        echo json_encode(array('billing' => $billingstr, 'shipping' => $shippingstr, 'sku' => $sku, 'duration_date' => $duration_date,'billing_details_id' => $billing_details_id, 'shipping_details_id' => $shipping_details_id));
        exit;
    }

    //billing address details at click of billing details dropdown
    function getBillingAddress() {
        $bd_id = $_GET['bd_id'];
        $billingstr = "";
        if ($bd_id > 0) {
            $billingDetail = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $bd_id));
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    if(!empty($value['address_line_1']) || !empty($value['address_line_2'])){
                        $countyStateCity = $this->getCountryStateCity($value['country_id'], $value['state_id'], $value['city_id']);
                        $address = $value['address_line_1'] . " " . $value['address_line_2'] . " " . $countyStateCity . " " . $value['zipcode'];
                        $billingstr = $billingstr . "<p class='bas-title'>Billing Address 11</p>
                              <p class='bas-text'>" . $address . "</p>
                              <p class='bas-title'>Billing Contact</p>
                              <p class='bas-text'>" . $value['contact_person'] . "</p> 
                              <p class='bas-title'>Billing Email</p>
                              <p class='bas-text'>" . $value['email'] . "</p>
                              <p class='bas-title'>Billing Phone</p>
                              <p class='bas-text'>" . $value['number'] . "";
                    }
                }
            }
        }
        echo json_encode(array('billing' => $billingstr));
        exit;
    }

    //shipping address details at click of shipping details dropdown
    function getShipAddress() {
        $bd_id = $_GET['bd_id'];
        $billingstr = "";
        if ($bd_id > 0) {
            $billingDetail = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $bd_id));
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    if(!empty($value['address_line_1']) || !empty($value['address_line_2'])){
                        $countyStateCity = $this->getCountryStateCity($value['country_id'], $value['state_id'], $value['city_id']);
                        $address = $value['address_line_1'] . " " . $value['address_line_2'] . " " . $countyStateCity . " " . $value['zipcode'];
                        $billingstr = $billingstr . "<p class='bas-title'>Shipping Address</p>
                              <p class='bas-text'>" . $address . "</p>
                              <p class='bas-title'>Shipping Contact</p>
                              <p class='bas-text'>" . $value['contact_person'] . "</p> 
                              <p class='bas-title'>Shipping Email</p>
                              <p class='bas-text'>" . $value['email'] . "</p>
                              <p class='bas-title'>Shipping Phone</p>
                              <p class='bas-text'>" . $value['number'] . "";
                    }
                }
            }
        }

        echo json_encode(array('billing' => $billingstr));
        exit;
    }

    //country, state, city name
    function getCountryStateCity($countyId = 0, $stateID = 0, $cityID = 0) {
        $str = ',';
        $cityDetail = $this->common->getData("tbl_city", "city_name", array("city_id" => $cityID));
        if (!empty($cityDetail)) {
            $str = $str . $cityDetail[0]['city_name'] . ",";
        }
        $stateDetail = $this->common->getData("tbl_state", "state_name", array("state_id" => $stateID));
        if (!empty($stateDetail)) {
            $str = $str . " " . $stateDetail[0]['state_name'] . ",";
        }
        $countryDetail = $this->common->getData("tbl_country", "country_name", array("country_id" => $countyId));
        if (!empty($countryDetail)) {
            $str = $str . " " . $countryDetail[0]['country_name'];
        }
        return $str;
    } 

    function loadDeliverySchedule() {
        //echo '<pre>'; print_r($this->input->post()); die;
        $skuIdsArr = $this->input->post("sku_id");
        $skuNumberArr = $this->input->post("sku_number");
        $skuQtyArr = $this->input->post("sku_qty");
        $week = $this->input->post("duration");         
        $days = ($week*7);
        //echo $days;die;
        $startDate = $this->input->post("shipment_start_date"); 
        $endDate = date("Y-m-d", strtotime($startDate." +$days days")); 
         
        $endDate = strtotime($endDate);
        $dataArr = array();
        
        for($i = strtotime('Monday', strtotime($startDate)); $i < $endDate; $i = strtotime('+1 week', $i)){
            $dataArr[date('M Y', $i)][]= date('Y-m-d', $i);
        }
        $skuDateArr = array();
        foreach ($dataArr as $key => $value) {
            foreach ($value as $keyValue => $valueData) {
                foreach ($skuIdsArr as $keySKU => $valueSKU) {
                    $skuDateArr[$key][$valueData][$valueSKU]=array('qty'=>$skuQtyArr[$keySKU],'sku'=>$skuNumberArr[$keySKU]);
                } 
            }
        }  
        //echo '<pre>'; print_r($skuDateArr); die;
        
        $str = ''; 
        $skuNumber_cnt = 0;
        $first_index_date = 0;
        foreach ($skuDateArr as $monthYrs => $weekArr) {
                $str = $str.'<div class="delivery-schedule-wrapper">
                    <!--<div class="ds-row ds-row-top">
                        <div class="ds-month">Month</div>
                        <div class="ds-weeks"></div>
                        <div class="ds-sku">BM12345</div>
                        <div class="ds-sku">BM12345</div>
                        <div class="ds-sku">BM12345</div>
                        <div class="ds-sku">BM12345</div>
                        <div class="ds-sku">BM12345</div>
                        <div class="ds-sku">BM12345</div>
                    </div>-->';
                    $str = $str.'<!-- Single month header -->
                            <!-- Single month wrapper -->
                            <div class="ds-single-wrapper">
                                <!-- Single month header -->
                                <div class="ds-row ds-row-header">
                                    <div class="ds-month">
                                        <a href="#/" class="ds-toggle-icon"></a> <input type="text" class="input-form-mro" value="'.$monthYrs.'" readonly="readonly">
                                    </div> 
                                </div>';
                   
                    $skuNumber_cnt = 0;
                    $qtyTotalArr = array();
                    $weeklyGrandTotal = 0;
                    foreach ($weekArr as $weekDate => $valueSKU) {
                        $skuNumber_str = '';
                        $skuQty_str = '';
                        $weekTotal = 0;
                        foreach ($valueSKU as $skuID => $skuQtyNumber) {
                            if($skuNumber_cnt == 0){
                                $uom = '';
                                if(!empty($this->getUoM()[$skuID])){
                                    $uom = $this->getUoM()[$skuID];
                                } 
                                $skuNumber_str .= '<div class="ds-sku"><input type="hidden" name="schedule_skuNumber['.$skuID.']" class="input-form-mro" value="'.$skuQtyNumber['sku'].'">'.$skuQtyNumber['sku'].' ('.$uom.')</div>';  
                            }
                            
                            $qty = (int)($skuQtyNumber['qty']/$week);
                            //set float value for first key
                            $float_value = 0;
                            if($first_index_date == 0){
                                $divided_value = ($skuQtyNumber['qty']/$week);
                                if(is_float($divided_value)){
                                    $without_float_total = ((int)$divided_value) * $week;
                                    $float_value = ($skuQtyNumber['qty'] - $without_float_total);
                                }
                                $qty = $qty+$float_value;                                
                            }
                            $skuQty_str .='<div class="ds-sku">
                                            <input type="text" name="schedule_skuQty['.$weekDate.']['.$skuID.']" class="input-form-mro" value="'.(int)$qty.'">
                                        </div>';
                            
                            $qtyTotalArr[$skuID][] = (int)$qty; 
                            $weekTotal += $qty; 
                            
                        }
                        $first_index_date++;
                        
                        //weekly total header
                        $skuNumber_str .= '<div class="ds-sku">Weekly Total</div>';  
                        //weekly total row
                        $skuQty_str .='<div class="ds-sku">'.$weekTotal.'</div>';

                        $weeklyGrandTotal += $weekTotal;
                       
                        //sku week date, sku number and qty code
                        $str .= ' 
                                <!-- Single month content -->
                                <div class="ds-expand-content">';
                                if($skuNumber_cnt == 0){
                                   $str .= '<div class="ds-row ds-row-week ds-row-week-header">
                                        <div class="ds-month"></div>
                                        <div class="ds-weeks">Weeks</div>
                                        '.$skuNumber_str.'
                                    </div>';
                                }
                                   $str .= '<div class="ds-row ds-row-week">
                                        <div class="ds-month"></div>
                                        <div class="ds-weeks">
                                            <input type="text"  name="schedule_weekdate[]" class="input-form-mro" value="'.$weekDate.'" readonly="readonly">
                                        </div>
                                        '.$skuQty_str.'
                                    </div> 
                                </div>
                                <!-- Single month content -->
                            '; 
                        $skuNumber_cnt++;
                    } 
                    //code for month wise qty total
                    $str .= '<div class="ds-row ds-row-total">
                                <div class="ds-month"></div>
                                <div class="ds-weeks">Total</div>'; 
                                foreach ($qtyTotalArr as $skuId => $qty) {
                                    $qtyTotalData = 0;
                                    foreach ($qty as $key => $qtyTotal) {
                                        $qtyTotalData += $qtyTotal;
                                    }
                                    $str .='<div class="ds-sku">
                                    '.$qtyTotalData.'
                                    </div>'; 
                                }
                                //weekly grand total
                                $str .= '<div class="ds-sku">
                                         '.$weeklyGrandTotal.'
                                         </div></div>';
                           $str .= '</div>';

                    $str .='</div></div>'; 
                    
        }

        echo json_encode(array('str' => $str));
        exit;
    }

}

?>
