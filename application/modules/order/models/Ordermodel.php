<?PHP

class Ordermodel extends CI_Model {

    function getRecords($get) {
        $table = "tbl_order";
        $table_id = 'order_id';
        $default_sort_column = 'i.order_id';
        $default_sort_order = 'desc';
        $condition = "1=1 ";
        $colArray = array('i.contract_number','i.nick_name');
        $sortArray = array('i.contract_number','i.nick_name', 'i.shipment_start_date', 'i.duration', 'i.status');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

        for ($i = 0; $i < count($colArray); $i++) {
            if (isset($_GET['Searchkey_' . $i]) && $_GET['Searchkey_' . $i] != "") {
                $condition .= " AND " . $colArray[$i] . " LIKE '%" . $_GET['Searchkey_' . $i] . "%' ";
            }
        }

        $this->db->select('i.*');
        $this->db->from("$table as i");
        
        //condition for Customer and Supplier roles user
        if($_SESSION["mro_session"]['user_role'] =='Customer'){
            $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
            $condition .= " AND bp.business_partner_id = ".$bpID;  
            $this -> db -> join("tbl_bp_order as bpo","bpo.bp_order_id = i.customer_contract_id","left");
            $this -> db -> join("tbl_business_partner as bp","bp.business_partner_id = bpo.business_partner_id","left");
        }else if($_SESSION["mro_session"]['user_role'] =='Supplier'){
            $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
            $condition .= " AND bp.business_partner_id = ".$bpID; 
            $this -> db -> join("tbl_bp_order as bpo","bpo.bp_order_id = i.supplier_contract_id","left");
            $this -> db -> join("tbl_business_partner as bp","bp.business_partner_id = bpo.business_partner_id","left");
        }  
         
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page);

        $query = $this->db->get();
        //echo $this->db->last_query();exit;


        $this->db->select('i.*');
        $this->db->from("$table as i");
        //condition for Customer and Supplier roles user
        if($_SESSION["mro_session"]['user_role'] =='Customer'){
           $condition .= " AND bp.created_by = ".$_SESSION["mro_session"][0]['user_id']; 
           $this -> db -> join("tbl_bp_order as bpo","bpo.bp_order_id = i.customer_contract_id","left");
           $this -> db -> join("tbl_business_partner as bp","bp.business_partner_id = bpo.business_partner_id","left");
        }else if($_SESSION["mro_session"]['user_role'] =='Supplier'){
           $condition .= " AND bp.created_by = ".$_SESSION["mro_session"][0]['user_id']; 
           $this -> db -> join("tbl_bp_order as bpo","bpo.bp_order_id = i.supplier_contract_id","left");
           $this -> db -> join("tbl_business_partner as bp","bp.business_partner_id = bpo.business_partner_id","left");
        }  
        
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);

        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    } 
    
    function getCustomerContract() {
        $condition = " bp.business_type ='Customer' AND bpo.status='Active' ";        
        //condition for Customer and Supplier roles user
        if($_SESSION["mro_session"]['user_role'] =='Customer'){
            $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
            $condition .= " AND bp.business_partner_id = ".$bpID;  
        }
        $this->db->select('bpo.bp_order_id,bpo.contract_number,bp.alias');
        $this->db->from("tbl_bp_order as bpo");
        $this->db->join("tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id AND bp.status='Active'");
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getSupplierContract() {
        $condition = " bp.business_type ='Vendor' AND bp.business_category in ('Distributor','Manufacturer') AND bpo.status='Active' ";
        //condition for Customer and Supplier roles user
        if($_SESSION["mro_session"]['user_role'] =='Supplier'){
            $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
            $condition .= " AND bp.business_partner_id = ".$bpID;  
        }
        $this->db->select('bpo.bp_order_id,bpo.contract_number,bp.alias');
        $this->db->from("tbl_bp_order as bpo");
        $this->db->join("tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id AND bp.status='Active' ");
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getBillingDetails($table,$bpo_id) {
        $condition = " bpo.bp_order_id =".$bpo_id." ";
        $this->db->select('bd.*,bpo.shipment_start_date,bpo.duration');
        $this->db->from("$table as bd");
        $this->db->join("tbl_bp_order as bpo", "bpo.billing_details_id = bd.billing_details_id","right");
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getShippingDetails($table,$bpo_id) {
        $condition = " bpo.bp_order_id =".$bpo_id." ";
        $this->db->select('sd.*');
        $this->db->from("$table as sd");
        $this->db->join("tbl_bp_order as bpo", "bpo.shipping_details_id = sd.shipping_details_id","right");
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getskuData($bpo_id = 0) {
        $condition = " bposku.bp_order_id = " . $bpo_id . " ";
        $this->db->select('bposku.sku_id,bposku.quantity,sku.sku_number,sku.product_name,uom.uom_name,s.size_name');
        $this->db->from("tbl_sku as sku");
        $this->db->join("tbl_bp_order_sku as bposku", "bposku.sku_id = sku.sku_id");
        $this->db->join("tbl_uom as uom", "uom.uom_id = sku.uom_id",'left');
        $this->db->join("tbl_size as s", "s.size_id = sku.size_id",'left');
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getOrderSkuDetails($bp_order_id = 0) {
        $condition = " bpo.order_id = " . $bp_order_id . " ";
        $this->db->select('bpo.*,sku.sku_id,sku.sku_number,sku.product_name,uom.uom_name,s.size_name');
        $this->db->from("tbl_order_sku as bpo");
        $this->db->join("tbl_sku as sku", "bpo.sku_id = sku.sku_id");
        $this->db->join("tbl_uom as uom", "uom.uom_id = sku.uom_id",'left');
        $this->db->join("tbl_size as s", "s.size_id = sku.size_id",'left');
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getUoM(){
        $condition = " sku.status='Active' ";
        $this->db->select('sku.sku_id,uom.uom_name');
        $this->db->from("tbl_sku as sku");
        $this->db->join("tbl_uom as uom", "uom.uom_id = sku.uom_id"); 
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

}

?>
