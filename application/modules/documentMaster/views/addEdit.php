<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form name="addEditForm" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("documentMaster"); ?>">Document Master List</a>
                                <span>></span>
                                <p>Add Document Custom Fields</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add Document</h1>
                            </div>
                        </div>          
                        <div class="title-sec-right"> 
                            <button type="submit" class="btn-primary-mro">Save</button>
                            <a href="<?php echo base_url("documentMaster"); ?>" class="btn-transparent-mro cancel">Cancel</a> 
                        </div>  
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="custom_field_structure_id" id="custom_field_structure_id" value="<?php echo(!empty($custom_field[0]['custom_field_structure_id'])) ? $custom_field[0]['custom_field_structure_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <!-- <label for="documentName">Document Type Name</label>
                                    <input type="text" name="document_type_name" id="document_type_name" class="input-form-mro" value="<?php echo(!empty($custom_field[0]['document_type_name'])) ? $custom_field[0]['document_type_name'] : ""; ?>"> -->

                                    <label for="Status">Document Type<sup>*</sup></label>
                                    <select name="document_type" id="document_type" class="select-form-mro" onchange="getSubDocumentType(this.value)">
                                        <option value="">Select Document Type</option>
                                        <?php foreach ($document_type as $key => $value) { 
                                            $sel='';
                                            if($custom_field[0]['document_type_id']){
                                                $sel = ($value['document_type_id'] == ($custom_field[0]['document_type_id'])?"selected":"");
                                            }
                                            ?>
                                            <option value="<?= $value['document_type_id']?>" <?= $sel?>><?= $value['document_type_name'] ?></option>    
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                           
                            <div id="subDocumenttypeDiv"></div>
                            
                            <!-- <div class="form-row form-row-3">Add Sub Document type</div> -->
                            

                            <div id="documentCustomfieldDiv">
                                <?php if(!empty($custom_field) && isset($custom_field)){
                                    $i= 0;
                                        foreach ($custom_field as $key => $value) { 
                                            $i = ++$i;
                                            if(!empty($value['custom_field_title']) && isset($value['custom_field_title'])){ ?>
                                                <div class="form-row form-row-4 CustomfieldDiv" id="CustomfieldDiv<?= $value['custom_field_structure_id'];?>" >
                                                    <div class="form-group">
                                                        <label for="documentName">Custom field title <?= $i?></label>
                                                        <input type="text" required name="custom_field_title_existing[<?= $value['custom_field_structure_id'];?>]" placeholder="Enter title of Custom field" id="custom_field_title" class="input-form-mro" value="<?= $value['custom_field_title']?>">
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="Status">Custom Type <?= $i?></label>
                                                        <select name="custom_type_existing[<?= $value['custom_field_structure_id'];?>]" id="custom_type" class="select-form-mro" required onchange="enableValue(this.value,<?= $value['custom_field_structure_id'];?>)">
                                                            <option value="" >Select Custom Type</option>
                                                            <option value="text" <?= (!empty($value['custom_field_type']) && $value['custom_field_type'] == "text") ? "selected" : ""; ?> >Text</option>
                                                            <option value="date"  <?= (!empty($value['custom_field_type']) && $value['custom_field_type'] == "date") ? "selected" : ""; ?>>Date</option>
                                                            <option value="dropdown" <?= (!empty($value['custom_field_type']) && $value['custom_field_type'] == "dropdown") ? "selected" : ""; ?> >DropDown</option>
                                                            <option value="file" <?= (!empty($value['custom_field_type']) && $value['custom_field_type'] == "file") ? "selected" : ""; ?> >File</option>
                                                        </select>
                                                    </div>
                                                    <div class="form-group" style="display:<?= (!empty($value['custom_field_slug'])?"block":"none")?>" id="dropdown_value<?= $value['custom_field_structure_id'];?>">
                                                        <label for="documentName"> Enter value comma separated </label>
                                                        <input type="text" placeholder="Enter value seperated by comma" required name="custom_field_value_existing[<?= $value['custom_field_structure_id'];?>]" id="custom_field_value<?= $value['custom_field_structure_id'];?>" class="input-form-mro" value="<?= $value['custom_field_slug']?>">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="Status">Status</label>
                                                        <select name="status_existing[<?= $value['custom_field_structure_id'];?>]" id="status" class="select-form-mro" required>
                                                            <option value="Active"  <?= (!empty($value['status']) && $value['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                                            <option value="In-active" <?= (!empty($value['status']) && $value['status'] == "In-Active") ? "selected" : ""; ?> >In-active</option>
                                                        </select>
                                                    </div>
                                                    
                                                    <div class="form-group remove-field-btn">
                                                        <button type="button" class="btn btn-sm btn-primary-mro" onclick="removeCustomfieldExisting('<?= $value['custom_field_structure_id'];?>')">X<button>
                                                    </div>
                                                </div>
                                        <?php }
                                        }
                                    } ?>
                            </div>

                            <div>
                                <button type="button" class="btn btn-primary-mro" onclick="addCustomfield(0);">+ Add Custom Field</button>
                            </div>
                            <br>
                           
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     
<script>

    function removeCustomfieldExisting(customfieldId){
        var r = confirm("Are you sure to wants to delete this custom field ");
        if (r == true) {
        $.ajax({
            url: "<?php echo base_url(); ?>documentMaster/deleteCustomField",
            data: {
            "id": customfieldId
            },
            dataType: 'json',
            cache: false,
            clearForm: false,
            async: false,
            type: "POST",
            success: function(response) { 
                showInsertUpdateMessage(response.msg,response);                        
                if (response.success) { 
                    setTimeout(function() {
                    location.reload();
                    }, 3000); 
                } 
            }
        });
        }
    }

    function addCustomfield(){

        var cnt =  $('#documentCustomfieldDiv').children('.CustomfieldDiv').length;
        // var cnt = cnt;
        cnt = ++cnt;
        console.log(cnt);
        var html = ' <div class="form-row form-row-4 CustomfieldDiv" id="CustomfieldDiv'+cnt+'" >'+
                        '<div class="form-group">'+
                            '<label for="documentName">Custom field title '+cnt+'</label>'+
                            '<input type="text" required name="custom_field_title['+cnt+']" placeholder="Enter title of Custom field" id="custom_field_title" class="input-form-mro" value="">'+
                        '</div>'+
                        
                        '<div class="form-group">'+
                            '<label for="Status">Custom Type '+cnt+'</label>'+
                            '<select name="custom_type['+cnt+']" id="custom_type" class="select-form-mro" required onchange="enableValue(this.value,'+cnt+')">'+
                                '<option value="" >Select Custom Type</option>'+
                                '<option value="text" >Text</option>'+
                                '<option value="date" >Date</option>'+
                                '<option value="dropdown" >DropDown</option>'+
                                '<option value="file" >File</option>'+
                            '</select>'+
                        '</div>'+
                        '<div class="form-group" style="display:none" id="dropdown_value'+cnt+'">'+
                            '<label for="documentName"> Enter value comma separated </label>'+
                            '<input type="text" placeholder="Enter value seperated by comma" required name="custom_field_value['+cnt+']" id="custom_field_value'+cnt+'" class="input-form-mro" value="">'+
                        '</div>'+

                        '<div class="form-group">'+
                            '<label for="Status">Status</label>'+
                            '<select name="status['+cnt+']" id="status" class="select-form-mro" required>'+
                                '<option value="Active" >Active</option>'+
                                '<option value="In-active" >In-active</option>'+
                            '</select>'+
                        '</div>'+
                        
                        '<div class="form-group remove-field-btn">' +
                            '<button type="button" class="btn btn-sm btn-primary-mro" onclick="removeSubDocument('+cnt+')">X<button>' +
                        '</div>' +
                    '</div>';
        $("#documentCustomfieldDiv").append(html);            
    }

    function removeSubDocument(cnt) {
        console.log(cnt);
        $("#CustomfieldDiv"+cnt).remove();
    }

    // $(document).on("change",".enableValue",function(){

    // });

    <?php if(!empty($custom_field) && isset($custom_field)) { ?>
        getSubDocumentType('<?= $custom_field[0]['document_type_id'] ?>','<?= $custom_field[0]['sub_document_type_id'] ?>');
    <?php } ?>
     
    function enableValue(type,val){
        console.log(type);
        console.log(val);
        if(type == 'dropdown'){
            $("#dropdown_value"+val).show();
        }else{
            $("#dropdown_value"+val).hide();
            $("#custom_field_value"+val).val(" ");
        }

    }

    
    $(document).ready(function () {

        var vRules = {
            "document_type": {required: true}
        };
        var vMessages = {
            "document_type": {required: "Please Select the Document first."}
        };
        //check and save document
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>documentMaster/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        // $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('documentMaster') ?>";
                            }, 3000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });

    function getSubDocumentType(documenttype,subdoctype =null){
        console.log(documenttype);
        $.ajax({
            url:"<?php echo base_url('documentMaster/getSubdocumentdiv');?>",
            type: 'post',
            dataType: 'json',
            data:{id:documenttype,subdoctype},
            success:function(response){
                if(response){
                    $('#subDocumenttypeDiv').html(response.html);
                }
                console.log(response);
                // window.location.reload();
            }

            })



    }
</script>