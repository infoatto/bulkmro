<?php 
if($Subdocument){ ?>
<div class="form-row form-row-3">
    <div class="form-group">
        <label for="sub_document_type">Sub Document Type</label>
        <select name="sub_document_type" id="sub_document_type" class="select-form-mro" required>
            <option value="">Select Document Type</option>
            <?php foreach ($Subdocument as $key => $value) {
                 $sel = "";
                 if(!empty($subdoctype) && isset($subdoctype)){
                     $sel = ($subdoctype == $value['sub_document_type_id']?"selected":"");
                 }
                 ?>
                <option value="<?= $value['sub_document_type_id']?>" <?= $sel;?>><?= $value['sub_document_type_name'] ?></option>    
            <?php } ?>
        </select>
    </div>
</div>
<?php } ?>
