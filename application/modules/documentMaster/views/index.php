<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                            <span>></span>
                            <p>All Document Custom Field</p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title"><a href="<?php echo base_url("documentMaster"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Document Custom Field</h1>
                        </div>
                    </div>
                    <div class="title-sec-right">
                        <?php if ($this->privilegeduser->hasPrivilege("DocumentMasterAddEdit")) { ?>
                            <a href="<?php echo base_url("documentMaster/addEdit"); ?>" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> Add Document Custom field</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="page-content-wrapper1">
                    <div id="serchfilter">
                        <div class="form-row form-row-4">
                            <div class="form-group dataTables_filter searchFilterClass">
                                <input type="text" id="sSearch_0" name="sSearch_0" class="searchInput filter-search-field" placeholder="Document Name">
                            </div>
                            <div class="form-group dataTables_filter searchFilterClass">
                                <input type="text" id="sSearch_1" name="sSearch_1" class="searchInput filter-search-field" placeholder="Sub Document Name">
                            </div>
                            <div class="form-group clear-search-filter">
                                <button class="btn-primary-mro" onclick="clearSearchFilters();">Clear Search</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="bp-list-wrapper">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>SR no</th>
                                            <th>Document Name</th>
                                            <th>Sub Document Name</th>
                                            <!-- <th>Status</th>  -->
                                            <th class="table-action-cls" style="width:50px !important;">Actions</th>
                                        </tr>
                                    </thead>
                                    <?php if(!empty($this->session->flashdata)){ ?>
                                        <?php if (!empty($this->session->flashdata('message-ordering'))) { ?>
                                            <div class="alert alert-success" id="MSG">
                                            <?php echo $this->session->flashdata('message-ordering'); ?>
                                        <?php } ?>
                                    <?php } ?>
                                    <tbody class="row_position">
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>