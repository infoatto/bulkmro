<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Bporder extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Bpordermodel', 'bpordermodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersOrderList")){
            redirect('dashboard');
        }
    }

    function index() {
        $data = array(); 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('bporder/index', $data);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() { 
        $edit_datas = array();
        $edit_datas['bp_order_details'] = array();
        $edit_datas['bpDetails'] = array();
        $edit_datas['skuDetails'] = array();
        $edit_datas['bp_skudetails'] = array();
        $edit_datas['contractDetails'] = array(); 
        
        $edit_datas['view'] = isset($_GET['view'])?$_GET['view']:0;
        
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $bp_order_id = $url_prams['id'];
            $result = $this->common->getData("tbl_bp_order", "*", array("bp_order_id" => $bp_order_id));
            if (!empty($result)) {
                $edit_datas['bp_order_details'] = $result[0];
                $bpNameData = $this->common->getData("tbl_business_partner", "alias",array("business_partner_id" => $result[0]['business_partner_id'])); 
                $edit_datas['bp_name'] = $bpNameData[0]['alias'];
                
                //orders tab details
                if($edit_datas['view']==1 && $bp_order_id > 0){  
                    $main_table = array("tbl_order as o", array('o.order_id,o.contract_number as master_contract,o.shipment_start_date,o.duration,o.updated_on'));
                    if($result[0]['business_type'] =='Customer'){
                        $condition = "1=1  AND o.customer_contract_id = ".$bp_order_id." ";
                        $join_tables = array(
                            array("left","tbl_order_sku as osku","osku.order_id = o.order_id", array()),
                            array("left","tbl_sku as sku","osku.sku_id = sku.sku_id", array()),
                            array("left","tbl_category as category","category.category_id = sku.category_id", array("GROUP_CONCAT(category.category_name) as category")),
                            array("","tbl_bp_order as bpo","bpo.bp_order_id = o.customer_contract_id", array("bpo.bp_order_id,bpo.contract_number,bpo.nick_name")),
                            array("left","tbl_users as u","u.user_id = o.updated_by", array("u.firstname,u.lastname")),
                        ); 
                    }else{
                        $condition = "1=1  AND o.supplier_contract_id = ".$bp_order_id." ";
                        $join_tables = array(
                            array("left","tbl_order_sku as osku","osku.order_id = o.order_id", array()),
                            array("left","tbl_sku as sku","osku.sku_id = sku.sku_id", array()),
                            array("left","tbl_category as category","category.category_id = sku.category_id", array("GROUP_CONCAT(category.category_name) as category")),
                            array("inner","tbl_bp_order as bpo","bpo.bp_order_id = o.supplier_contract_id", array("bpo.bp_order_id,bpo.contract_number,bpo.nick_name")),
                            array("left","tbl_users as u","u.user_id = o.updated_by", array("u.firstname,u.lastname")),
                        ); 
                    } 
                    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,"",'o.order_id');
                    $edit_datas['contractDetails'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                    //echo $this->db->last_query();  die;
                }
            }
            //order sku details data
            $skuData = $this->bpordermodel->getOrderSkuDetails($bp_order_id);
            if (!empty($skuData)) {
                $edit_datas['bp_skudetails'] = $skuData;
            }
            
            $edit_datas['actionEdit'] = '';
            if($this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit") || $this->privilegeduser->hasPrivilege("BusinessPartnersOrderEdit")){   
                $edit_datas['actionEdit'] = '<a href="bporder/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $bp_order_id), '+/', '-_'), '=') . '" title="Edit" class="text-center btn-primary-mro"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" >Edit</a>';
            }
            
        }
        //business partner data
        $condition = " business_category in ('Government','Corporate','Distributor','Manufacturer') AND status = 'Active' ";
        //condition for Customer and Supplier roles user 
        if($_SESSION["mro_session"]['user_role'] =='Customer'){
            $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
            $condition .= " AND business_type = 'Customer' AND business_partner_id =".$bpID." "; 
        }else if($_SESSION["mro_session"]['user_role'] =='Supplier'){
            $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
            $condition .= " AND business_type = 'Vendor' AND business_partner_id =".$bpID." ";  
        } 
        
        $bpDetails = $this->common->getData("tbl_business_partner", "business_partner_id,alias", $condition,'alias','asc'); 
        if (!empty($bpDetails)) {
            $edit_datas['bpDetails'] = $bpDetails;
        }
        //sku data
        $skuDetails = $this->common->getData("tbl_sku", "sku_id,sku_number,product_name", array("status"=>"Active"),'sku_number','asc');
        if (!empty($skuDetails)) {
            $edit_datas['skuDetails'] = $skuDetails;
        } 
        
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('bporder/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        $get_result = $this->bpordermodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, $get_result['query_result'][$i]->contract_number);
                array_push($temp, $get_result['query_result'][$i]->business_type);
                array_push($temp, $get_result['query_result'][$i]->nick_name);
                array_push($temp, date('d-M-Y', strtotime($get_result['query_result'][$i]->shipment_start_date)));
                array_push($temp, $get_result['query_result'][$i]->duration);
                array_push($temp, $get_result['query_result'][$i]->patment_terms);
                array_push($temp, $get_result['query_result'][$i]->incoterms);
                array_push($temp, $get_result['query_result'][$i]->status);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit") || $this->privilegeduser->hasPrivilege("BusinessPartnersOrderEdit")){   
                    $actionCol = '<a href="bporder/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->bp_order_id), '+/', '-_'), '=') . '" title="Edit" class="text-center"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                 if($this->privilegeduser->hasPrivilege("BusinessPartnersOrderView")){   
                    $actionCol .= '<a href="bporder/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->bp_order_id), '+/', '-_'), '=') . '&view=1" title="View" class="text-center" style="float:right;"><img src="' . base_url() . 'assets/images/view-btn-icon.svg" alt="View" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")){
            redirect('dashboard');
        }
        
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "contract_number = '" . $this->input->post('contract_number') . "' ";
            if (!empty($this->input->post("bp_order_id"))) {
                $condition .= " AND bp_order_id <> " . $this->input->post("bp_order_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_bp_order", "bp_order_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['bp_order_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Contract Number already exist...'));
                exit;
            }

            $data = array();
            $data['contract_number'] = $this->input->post('contract_number');
            $data['business_partner_id'] = $this->input->post('business_partner_id');
            $data['nick_name'] = $this->input->post('nick_name');
            $data['business_type'] = $this->input->post('business_type');
            $data['shipment_start_date'] = !empty($this->input->post('shipment_start_date')) ? date("Y-m-d", strtotime($this->input->post('shipment_start_date'))) : NULL;
            $data['duration'] = $this->input->post('duration');
            $data['billing_details_id'] = $this->input->post('billing_details_id');
            $data['shipping_details_id'] = $this->input->post('shipping_details_id');
            $data['patment_terms'] = $this->input->post('patment_terms');
            $data['incoterms'] = $this->input->post('incoterms');
            $data['status'] = $this->input->post('status');

            if (!empty($this->input->post("bp_order_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_bp_order", $data, array("bp_order_id" => $this->input->post("bp_order_id")));                
                
                //update master contract number
                if(trim($this->input->post('contract_number')) != trim($this->input->post('old_contract_number'))){
                    if(trim($this->input->post('business_type')) == 'Customer'){
                        $contractNumberData = $this->common->getData("tbl_order", "order_id,contract_number,supplier_contract_id", array("customer_contract_id"=>$this->input->post("bp_order_id")));
                        if (!empty($contractNumberData)) {
                            foreach ($contractNumberData as $value) {
                                $contractUpdate = array();
                                $supplierContData = $this->common->getData("tbl_bp_order", "contract_number", array("bp_order_id"=>$value['supplier_contract_id'])); 
                                $contractUpdate['contract_number'] = trim($this->input->post('contract_number')).'-'.$supplierContData[0]['contract_number'];
                                $resultContract = $this->common->updateData("tbl_order", $contractUpdate, array("customer_contract_id" => $this->input->post("bp_order_id")));    
                            }  
                        }
                    }else{
                        $contractNumberData = $this->common->getData("tbl_order", "order_id,contract_number,customer_contract_id", array("supplier_contract_id"=>$this->input->post("bp_order_id")));
                        if (!empty($contractNumberData)) {
                            foreach ($contractNumberData as $value) {
                                $contractUpdate = array();
                                $customerContData = $this->common->getData("tbl_bp_order", "contract_number", array("bp_order_id"=>$value['customer_contract_id'])); 
                                $contractUpdate['contract_number'] = $customerContData[0]['contract_number'].'-'.trim($this->input->post('contract_number')); 
                                $resultContract = $this->common->updateData("tbl_order", $contractUpdate, array("supplier_contract_id" => $this->input->post("bp_order_id")));    
                            }  
                        }
                    }
                }

                $data1['bp_order_id'] = $this->input->post("bp_order_id");
                $resultdel = $this->common->deleteRecord('tbl_bp_order_sku', array("bp_order_id" => $this->input->post("bp_order_id")));
                if ($resultdel) {
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $data1['price'] = (!empty($this->input->post('sku_price')[$key])?$this->input->post('sku_price')[$key]:0.00);
                            $result1 = $this->common->insertData('tbl_bp_order_sku', $data1, '1');
                        }
                    }
                }

                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_bp_order", $data, "1");
                if ($result) {
                    $data1['bp_order_id'] = $result;
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $data1['price'] = (!empty($this->input->post('sku_price')[$key])?$this->input->post('sku_price')[$key]:0.00);
                            $result1 = $this->common->insertData('tbl_bp_order_sku', $data1, '1');
                        }
                    }
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }
    
    function importBPorder() { 
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersOrderImport")){
            redirect('dashboard');
        }
        require_once('application/libraries/SimpleXLSX.php');
       
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["excelfile"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["excelfile"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die; 
                 $xlsx = new SimpleXLSX($_FILES['excelfile']['tmp_name']);
                
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) { 
                        if (trim($data[0]) != "Contract Number") {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        if (trim($data[1]) != "Business Partner" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if (trim($data[2]) != "Status" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if (trim($data[4]) != "Duration in Weeks" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        
                        if (trim($data[5]) != "Business Partner Type" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }   
                        
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1]) && empty($data[2])){
                            continue;
                        } 
                        
                        if (trim($data[0]) == "") {
                            $error .= "Contract Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[1]) == "") {
                            $error .= "Business Partner cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                        
                        if (trim($data[2]) != "Active" && trim($data[2]) != "In-active") {
                            $error .= "Status cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[3]) == "") {
                            $error .= "Shipment Start Date cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[4]) == "" || (int)trim($data[4])==0 ) {
                            $error .= "Duration in Weeks cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                        
                        if (trim($data[5]) != "Vendor" && trim($data[5]) != "Customer") {
                            $error .= "Business Partner Type cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                        
                        if (trim($data[6]) == "") {
                            $error .= "Nick Name cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1]) && empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $contract_number = str_replace("'", "&#39;", trim($data[0]));
                        $bp_name = str_replace("'", "&#39;", trim($data[1]));                                            
                        $status = str_replace("'", "&#39;", trim($data[2])); 
                        $unixDateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[3]))); 
                        $start_date = date('Y-m-d',$unixDateVal); 
                        $week = str_replace("'", "&#39;", trim($data[4]));
                        $type = str_replace("'", "&#39;", trim($data[5]));   
                        $nickName = str_replace("'", "&#39;", trim($data[6]));   
                        $businessType = "Vendor";
                        $business_category = " AND business_category in ('Manufacturer','Distributor')";
                        if($type=='Customer'){
                            $businessType = "Customer";
                            $business_category = "";
                        }
                        
                        //get bussiness partner data
                        $condition_bp = "alias = '" . $bp_name . "' AND business_type ='".$businessType."' ".$business_category." "; 
                        $chk_bp_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id,payment_terms_id,incoterms_id", $condition_bp);
                        $rs_bp = $this->common->MySqlFetchRow($chk_bp_sql, "array");
                        //echo $this->db->last_query(); 
                        $bp_id = 0;
                        if (empty($rs_bp[0]['business_partner_id'])) {
                            continue;
                        }else{                            
                            $bp_id = $rs_bp[0]['business_partner_id'];
                            
                            //get billing details id
                            $condition_billing_details = "business_partner_id = " . $bp_id . " "; 
                            $chk_billing_details_sql = $this->common->Fetch("tbl_billing_details", "billing_details_id", $condition_billing_details);
                            $rs_billing_details = $this->common->MySqlFetchRow($chk_billing_details_sql, "array");                        
                            $billing_details_id = 0;
                            if (!empty($rs_billing_details[0]['billing_details_id'])) {
                                $billing_details_id = $rs_billing_details[0]['billing_details_id'];
                            } 
                            
                            //get shipping details id
                            $condition_shipping_details = "business_partner_id = " . $bp_id . " "; 
                            $chk_shipping_details_sql = $this->common->Fetch("tbl_shipping_details", "shipping_details_id", $condition_shipping_details);
                            $rs_shipping_details = $this->common->MySqlFetchRow($chk_shipping_details_sql, "array");                        
                            $shipping_details_id = 0;
                            if (!empty($rs_shipping_details[0]['shipping_details_id'])) {
                                $shipping_details_id = $rs_shipping_details[0]['shipping_details_id'];
                            } 

                            //get payment terms
                            $condition_payment_term = "payment_term_id = " . $rs_bp[0]['payment_terms_id'] . " "; 
                            $chk_payment_term_sql = $this->common->Fetch("tbl_payment_term", "payment_term_value", $condition_payment_term);
                            $rs_payment_term = $this->common->MySqlFetchRow($chk_payment_term_sql, "array");                        
                            $patment_terms = '';
                            if (!empty($rs_payment_term[0]['payment_term_value'])) {
                                $patment_terms = $rs_payment_term[0]['payment_term_value'];
                            } 
                            
                            //get incoterms
                            $condition_incoterms = "incoterms_id = " . $rs_bp[0]['incoterms_id'] . " "; 
                            $chk_incoterms_sql = $this->common->Fetch("tbl_incoterms", "incoterms_value", $condition_incoterms);
                            $rs_incoterms = $this->common->MySqlFetchRow($chk_incoterms_sql, "array");                        
                            $incoterms = '';
                            if (!empty($rs_incoterms[0]['incoterms_value'])) {
                                $incoterms = $rs_incoterms[0]['incoterms_value'];
                            } 

                            $data = array();
                            $data['contract_number'] = $contract_number;
                            $data['business_partner_id'] = $bp_id;
                            $data['nick_name'] = $nickName;
                            $data['business_type'] = $businessType;
                            $data['shipment_start_date'] = !empty($start_date) ? date("Y-m-d", strtotime($start_date)) : NULL;                            
                            $data['duration'] = (int)$week;  
                            $data['billing_details_id'] = $billing_details_id; 
                            $data['shipping_details_id'] = $shipping_details_id; 
                            $data['patment_terms'] = $patment_terms; 
                            $data['incoterms'] = $incoterms;
                            $data['status'] = $status; 
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");  

                            $condition_contract = "contract_number = '" . $contract_number . "' AND status='" . $status . "' "; 
                            $chk_contract_sql = $this->common->Fetch("tbl_bp_order", "bp_order_id", $condition_contract);
                            $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array"); 

                             //update-insert bussiness partner contract data 
                            if (!empty($rs_contract[0]['bp_order_id'])) {                            
                                $result = $this->common->updateData("tbl_bp_order", $data, array("bp_order_id" => $rs_contract[0]['bp_order_id']));  
                                if ($result) {  
                                    $imported++;
                                } else {
                                    $notimported++;
                                }
                            }else{
                                 //insert data 
                                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['created_on'] = date("Y-m-d H:i:s");
                                $result = $this->common->insertData("tbl_bp_order", $data, "1");
                                if ($result) {
                                    $imported++;
                                } else {
                                    $notimported++;
                                } 
                            }  
                        }
                        
                        
                    }
                     
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    } 
    
    function importBPorderSKU() { 
         
        if(!$this->privilegeduser->hasPrivilege("BusinessPartnersOrderImport")){
            redirect('dashboard');
        }
        require_once('application/libraries/SimpleXLSX.php');
       
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["skuexcelfile"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["skuexcelfile"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die;   
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) { 
                        if (trim($data[0]) != "Contract Number") {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        if (trim($data[1]) != "BM SKU Number" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[2]) != "Quantity" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
//                        if (trim($data[3]) != "Price per unit in USD" && $valid==0) {
//                            $error .= "Import sheet not valid.";
//                            $valid++;
//                        }  
                        
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[2])){
                            continue;
                        } 
                        
                        if (trim($data[0]) == "") {
                            $error .= "Contract Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[1]) == "") {
                            $error .= "BM SKU Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[2]) == "") {
                            $error .= "Quantity cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                        
//                        if (trim($data[3]) == "") {
//                            $error .= "Price per unit in USD cannot be empty in row : " . ($key + 1) . "<br>";
//                            $valid++;
//                        }  
                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $contract_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_number = str_replace("'", "&#39;", trim($data[1]));
                        $quantity = str_replace("'", "&#39;", trim($data[2]));                        
                        $price = str_replace("'", "&#39;", trim($data[3]));  
                        
                        //get order id
                        $condition_contract = "contract_number = '" . $contract_number . "' "; 
                        $chk_contract_sql = $this->common->Fetch("tbl_bp_order", "bp_order_id", $condition_contract);
                        $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array");
                        //echo $this->db->last_query(); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                        //condition for order and sku id blank
                        if (empty($rs_contract[0]['bp_order_id']) || empty($rs_sku[0]['sku_id'])) {
                            continue;
                        }else{                            
                            $bp_order_id = $rs_contract[0]['bp_order_id'];
                            $sku_id = $rs_sku[0]['sku_id'];
                            
                            $data = array();
                            $data['bp_order_id'] = $bp_order_id;
                            $data['sku_id'] = $sku_id;
                            $data['quantity'] = (int)$quantity;                            
                            $data['price'] = $price;   
                            
                            $this->common->deleteRecord('tbl_bp_order_sku', array("bp_order_id" => $bp_order_id,"sku_id" => $sku_id));  
                            
                            $result = $this->common->insertData("tbl_bp_order_sku", $data, "1");
                            if ($result) {
                                $imported++;
                            } else {
                                $notimported++;
                            }  
                        }  
                    }
                     
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    } 

    //business partner billing and shipping details dropdown
    function getBusinessPartner() {
        $bp_id = $_GET['bp_id'];
        $billing_id = $_GET['billing_id'];
        $shipping_id = $_GET['shipping_id'];
        $view = $_GET['isView'];
        $dropDownCss = '';
        if($view==1){
            $dropDownCss = "disabled"; 
        }

        $billingstr = "<select name='billing_details_id' id='billing_details_id' onchange='getBillAddress(this.value)' class='basic-single select-form-mro' $dropDownCss>"
                . "<option value='0' disabled selected hidden>Select Billing Details</option>";
        $shippingstr = "<select name='shipping_details_id' id='shipping_details_id' onchange='getShipAddress(this.value)' class='basic-single select-form-mro' $dropDownCss>"
                . "<option value='0' disabled selected hidden>Select Shipping Details</option>";
        $payment = '';
        $incoterm = '';
        if ($bp_id) {
            //billing details
            $billingDetail = $this->common->getData("tbl_billing_details", "*", array("business_partner_id" => $bp_id)); 
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    if(!empty($value['address_line_1']) || !empty($value['address_line_2'])){
                        $selected = '';
                        $address = $value['address_line_1'] . " " . $value['address_line_2'] . "";
                        if ($billing_id == $value['billing_details_id']) {
                            $selected = 'selected';
                        }
                        $billingstr = $billingstr . "<option value='" . $value['billing_details_id'] . "' $selected >" . $address . "</option>";
                    }
                }
            }
            $billingstr = $billingstr . "</select>";
            //shipping details
            $shippingDetail = $this->common->getData("tbl_shipping_details", "*", array("business_partner_id" => $bp_id));
            if (!empty($shippingDetail)) {
                foreach ($shippingDetail as $key => $value) {
                    if(!empty($value['address_line_1']) || !empty($value['address_line_2'])){
                        $selected = '';
                        $address = $value['address_line_1'] . " " . $value['address_line_2'] . "";
                        if ($shipping_id == $value['shipping_details_id']) {
                            $selected = 'selected';
                        }
                        $shippingstr = $shippingstr . "<option value='" . $value['shipping_details_id'] . "' $selected >" . $address . "</option>";
                    }
                }
            }
            $shippingstr = $shippingstr . "</select>";
            //business partner payment term value
            $bpDetail = $this->common->getData("tbl_business_partner", "business_type,payment_terms_id,incoterms_id", array("business_partner_id" => $bp_id));
            if (!empty($bpDetail[0]['payment_terms_id'])) {
                $paymentTermDetail = $this->common->getData("tbl_payment_term", "payment_term_value", array("payment_term_id" => $bpDetail[0]['payment_terms_id']),'payment_term_value','asc');
                if (!empty($paymentTermDetail)) {
                    $payment = $paymentTermDetail[0]['payment_term_value'];
                }
            }
            //business partner icoterm value
            if (!empty($bpDetail[0]['incoterms_id'])) {
                $incotermDetail = $this->common->getData("tbl_incoterms", "incoterms_value", array("incoterms_id" => $bpDetail[0]['incoterms_id']),'incoterms_value','asc');
                if (!empty($incotermDetail)) {
                    $incoterm = $incotermDetail[0]['incoterms_value'];
                }
            }
            
            if (!empty($bpDetail[0]['business_type'])) {
                $business_type = $bpDetail[0]['business_type'];
            }
        }

        echo json_encode(array('billing' => $billingstr, 'shipping' => $shippingstr, 'payment' => $payment, 'incoterm' => $incoterm, 'business_type' => $business_type));
        exit;
    }

    //billing address details at click of billing details dropdown
    function getBillingAddress() {
        $bd_id = $_GET['bd_id']; 
        $billingstr = "";
        if ($bd_id > 0) {
            $billingDetail = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $bd_id));
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    if(!empty($value['address_line_1']) || !empty($value['address_line_2'])){
                        $countyStateCity = $this->getCountryStateCity($value['country_id'], $value['state_id'], $value['city_id']);
                        $address = $value['address_line_1'] . " " . $value['address_line_2'] . " " . $countyStateCity . " " . $value['zipcode'];
                        $billingstr = $billingstr . "<p class='bas-title'>Billing Address</p>
                              <p class='bas-text'>" . $address . "</p>
                              <p class='bas-title'>Billing Contact</p>
                              <p class='bas-text'>" . $value['contact_person'] . "</p> 
                              <p class='bas-title'>Billing Email</p>
                              <p class='bas-text'>" . $value['email'] . "</p>
                              <p class='bas-title'>Billing Phone</p>
                              <p class='bas-text'>" . $value['number'] . "";
                    }
                }
            }
        }
        echo json_encode(array('billing' => $billingstr));
        exit;
    }

    //shipping address details at click of shipping details dropdown
    function getShipAddress() {
        $bd_id = $_GET['bd_id'];
        $billingstr = "";
        if ($bd_id > 0) {
            $billingDetail = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $bd_id));
            if (!empty($billingDetail)) {
                foreach ($billingDetail as $key => $value) {
                    if(!empty($value['address_line_1']) || !empty($value['address_line_2'])){
                        $countyStateCity = $this->getCountryStateCity($value['country_id'], $value['state_id'], $value['city_id']);
                        $address = $value['address_line_1'] . " " . $value['address_line_2'] . " " . $countyStateCity . " " . $value['zipcode'];
                        $billingstr = $billingstr . "<p class='bas-title'>Shipping Address</p>
                              <p class='bas-text'>" . $address . "</p>
                              <p class='bas-title'>Shipping Contact</p>
                              <p class='bas-text'>" . $value['contact_person'] . "</p> 
                              <p class='bas-title'>Shipping Email</p>
                              <p class='bas-text'>" . $value['email'] . "</p>
                              <p class='bas-title'>Shipping Phone</p>
                              <p class='bas-text'>" . $value['number'] . "";
                    }
                }
            }
        }

        echo json_encode(array('billing' => $billingstr));
        exit;
    }

    //country, state, city name
    function getCountryStateCity($countyId = 0, $stateID = 0, $cityID = 0) {
        $str = ',';
        $cityDetail = $this->common->getData("tbl_city", "city_name", array("city_id" => $cityID));
        if (!empty($cityDetail)) {
            $str = $str . $cityDetail[0]['city_name'] . ",";
        }
        $stateDetail = $this->common->getData("tbl_state", "state_name", array("state_id" => $stateID));
        if (!empty($stateDetail)) {
            $str = $str . " " . $stateDetail[0]['state_name'] . ",";
        }
        $countryDetail = $this->common->getData("tbl_country", "country_name", array("country_id" => $countyId));
        if (!empty($countryDetail)) {
            $str = $str . " " . $countryDetail[0]['country_name'];
        }
        return $str;
    }

    //business partner sku data
    function getskuDetails() {
        $sku_id = $_GET['sku_id']; 
        $data = array();
        if ($sku_id > 0) {
            $skuDetail = $this->bpordermodel->getskuData($sku_id);
            if (!empty($skuDetail)) {
                $data = $skuDetail[0];
            }
        }
        echo json_encode(array('skuData' => $data));
        exit;
    }
    
    function getskuDetailsDropdown() {
        $customField = '';
        $result['skuDetails'] = $this->common->getData("tbl_sku", "sku_id,sku_number,product_name", array("status"=>"Active"),'sku_number','asc');  
        if($result){
            $customField = $this->load->view("sku-dropdown",$result,true);
        } 
        echo json_encode(array('dropdown'=>$customField));
        exit;
    }
    //payment term and incoterm dropdown
    function getPaymentIncoterm() {
        $cnt = $_GET['cnt'];
        $billingstr = '';
        $shippingstr = '';
        if ($cnt == 1) {
            $billingstr = "<select name='patment_terms' id='patment_terms' class='basic-single select-form-mro' style='width: 97%;'>"
                    . "<option value='' disabled selected hidden>Select Payment Terms</option>";
            //payment term data        
            $paymentTermDetails = $this->common->getData("tbl_payment_term", "payment_term_id,payment_term_value", array("status"=>"Active"),'payment_term_value','asc');
            if (!empty($paymentTermDetails)) {
                foreach ($paymentTermDetails as $key => $value) {
                    $billingstr = $billingstr . "<option value='" . $value['payment_term_value'] . "' >" . $value['payment_term_value'] . "</option>";
                }
            }
            $billingstr = $billingstr . "</select>";
        } else {
            $shippingstr = "<select name='incoterms' id='incoterms' class='basic-single select-form-mro' style='width: 97%;'>"
                    . "<option value='' disabled selected hidden>Select Incoterm</option>";
            //Incoterm data        
            $incotermDetails = $this->common->getData("tbl_incoterms", "incoterms_id,incoterms_value", array("status"=>"Active"),'incoterms_value','asc');
            if (!empty($incotermDetails)) {
                foreach ($incotermDetails as $key => $value) {
                    $shippingstr = $shippingstr . "<option value='" . $value['incoterms_value'] . "' >" . $value['incoterms_value'] . "</option>";
                }
            }
            $shippingstr = $shippingstr . "</select>";
        }

        echo json_encode(array('paymentTerm' => $billingstr, 'incoTerm' => $shippingstr));
        exit;
    }

}

?>
