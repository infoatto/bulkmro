<?PHP

class Bpordermodel extends CI_Model {

    function getRecords($get) { 
        $table = "tbl_bp_order";
        $table_id = 'bp_order_id';
        $default_sort_column = 'i.bp_order_id';
        $default_sort_order = 'desc';
        $condition = "1=1 ";
        $colArray = array('i.contract_number','i.nick_name','i.business_type');
        $sortArray = array('i.contract_number','i.business_type','i.nick_name', 'i.shipment_start_date', 'i.duration', 'i.patment_terms', 'i.incoterms', 'i.status');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

        for ($i = 0; $i < count($colArray); $i++) { 
            if (isset($_GET['Searchkey_' . $i]) && $_GET['Searchkey_' . $i] != "") { 
                $condition .= " AND " . $colArray[$i] . " LIKE '%" . $_GET['Searchkey_' . $i] . "%' ";
            }
        } 
         

        $this->db->select('i.*');
        $this->db->from("$table as i"); 
        
        //condition for Customer and Supplier roles user
        if($_SESSION["mro_session"]['user_role'] =='Customer' || $_SESSION["mro_session"]['user_role'] =='Supplier'){
           $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
           $condition .= " AND bp.business_partner_id = ".$bpID; 
           $this -> db -> join("tbl_business_partner as bp","bp.business_partner_id = i.business_partner_id","left");
        }
        
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page);

        $query = $this->db->get();
        //echo $this->db->last_query();exit;


        $this->db->select('i.*');
        $this->db->from("$table as i");
        //condition for Customer and Supplier roles user
        if($_SESSION["mro_session"]['user_role'] =='Customer' || $_SESSION["mro_session"]['user_role'] =='Supplier'){
           $condition .= " AND bp.created_by = ".$_SESSION["mro_session"][0]['user_id']; 
           $this -> db -> join("tbl_business_partner as bp","bp.business_partner_id = i.business_partner_id","left");
        } 
        
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);

        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    } 

    function getskuData($sku_id = 0) {
        $condition = " sku.sku_id = " . $sku_id . "  AND sku.status = 'Active' ";
        $this->db->select('sku.sku_id,sku.sku_number,sku.product_name,s.size_name');
        $this->db->from("tbl_sku as sku");
        $this->db->join("tbl_size as s", "s.size_id = sku.size_id",'left');
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getOrderSkuDetails($bp_order_id = 0) {
        $condition = " bpo.bp_order_id = " . $bp_order_id . " ";
        $this->db->select('bpo.*,sku.sku_id,sku.sku_number,sku.product_name,s.size_name');
        $this->db->from("tbl_bp_order_sku as bpo");
        $this->db->join("tbl_sku as sku", "bpo.sku_id = sku.sku_id");
        $this->db->join("tbl_size as s", "s.size_id = sku.size_id",'left');
        $this->db->where("($condition)");
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

}

?>
