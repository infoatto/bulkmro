<?php
//start code of view option    
$dropDownCss = '';
$inputFieldCss = '';
if ($view == 1) {
    $dropDownCss = "disabled";
    $inputFieldCss = "readonly";
}
//end code of view option
$skuArr = array();
?>
<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("bporder"); ?>">Business Partners Contract List</a>
                                <span>></span>
                                <?php if ($view == 0) { ?>
                                    <p>New Business Partner Contract</p>
                                <?php } else { ?>
                                    <p><?= $bp_name; ?>, Contract# <?php echo (!empty($bp_order_details['contract_number'])) ? $bp_order_details['contract_number'] : ""; ?></p>
                                <?php } ?>
                            </div>
                            <div class="page-title-wrapper">
                                <?php if ($view == 0) { ?>
                                    <h1 class="page-title">Business Partner Contract</h1>
                                <?php } else { ?>
                                    <h1 class="page-title"><?= $bp_name; ?>, Contract# <?php echo (!empty($bp_order_details['contract_number'])) ? $bp_order_details['contract_number'] : ""; ?></h1>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="title-sec-right">
                        <?php if($view==0){ ?>
                             <button type="submit" class="btn-primary-mro">Save</button> 
                             <a href="<?php echo base_url("bporder"); ?>" class="btn-transparent-mro cancel">Cancel</a>
                        <?php }else{  
                            echo $actionEdit;
                        } ?>
                        </div> 
                    </div>
                    <div id="docuement_loader" style="display: none;">
                        <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                    </div>
                    <div class="page-content-wrapper" style="padding:0;">
                        <?php if ($view == 1) { ?>  
                            <div class="mro-tabs-container">
                                <nav>
                                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a class="nav-item nav-link active" id="main-details-tab" data-toggle="tab" href="#main-details" role="tab" aria-controls="main-details" aria-selected="true">Main Details</a>
                                        <a class="nav-item nav-link" id="other-details-tab" data-toggle="tab" href="#other-details" role="tab" aria-controls="other-details" aria-selected="true">Orders</a>
                                    </div>
                                </nav>
                                <div class="tab-content" id="nav-tabContent">
                                <?php } ?>      
                                <div class="tab-pane fade show active" id="main-details" role="tabpanel" aria-labelledby="main-details-tab" style="padding:30px">
                                    <input type="hidden" name="bp_order_id" id="bp_order_id" value="<?php echo (!empty($bp_order_details['bp_order_id'])) ? $bp_order_details['bp_order_id'] : ""; ?>">
                                    <div class="scroll-content-wrapper">
                                        <div class="scroll-content-left" id="contract-details">
                                            <h3 class="form-group-title" id="section-mro-1">Contract Details</h3>
                                            <div class="form-row form-row-4">
                                                <div class="form-group">
                                                    <label for="">Contract Number<sup>*</sup></label>
                                                    <input type="text" name="contract_number" id="contract_number" value="<?php echo (!empty($bp_order_details['contract_number'])) ? $bp_order_details['contract_number'] : ""; ?>" placeholder="Enter Contract Number" class="input-form-mro" <?= $inputFieldCss; ?>>
                                                    <input type="hidden" name="old_contract_number" id="old_contract_number" value="<?php echo (!empty($bp_order_details['contract_number'])) ? $bp_order_details['contract_number'] : ""; ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label for="BusinessPartner">Business Partner<sup>*</sup></label>
                                                    <select name="business_partner_id" id="business_partner_id" class="basic-single select-form-mro" onchange="getBillingShippingDetails(this.value);removeErroMsg('business_partner_id');" <?= $dropDownCss; ?>>
                                                        <option value="" disabled selected hidden>Select Business Partner</option>
                                                        <?php
                                                        foreach ($bpDetails as $bpValue) {
                                                            $bpID = (isset($bp_order_details['business_partner_id']) ? $bp_order_details['business_partner_id'] : 0);
                                                            ?>
                                                            <option value="<?php echo $bpValue['business_partner_id']; ?>" <?php
                                                            if ($bpValue['business_partner_id'] == $bpID) {
                                                                echo "selected";
                                                            }
                                                            ?>>
                                                                        <?php echo $bpValue['alias']; //business_name 
                                                                        ?>
                                                            </option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <label for="Status">Status</label>
                                                    <select name="status" id="status" class="select-form-mro" <?= $dropDownCss; ?>>
                                                        <option value="Active" <?php echo (!empty($bp_order_details['status']) && $bp_order_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                                        <option value="In-active" <?php echo (!empty($bp_order_details['status']) && $bp_order_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Nick Name<sup>*</sup></label>
                                                    <input type="text" name="nick_name" id="nick_name" value="<?php echo (!empty($bp_order_details['nick_name'])) ? $bp_order_details['nick_name'] : ""; ?>" placeholder="Nick Name" class="input-form-mro" <?= $inputFieldCss; ?>>
                                                </div>
                                            </div>
                                            <hr class="separator mt0" id="section-mro-2">
                                            <h3 class="form-group-title">Shipment Dates</h3>
                                            <div class="form-row form-row-3">
                                                <div class="form-group">
                                                    <label for="">Shipment Start Date<sup>*</sup></label>
                                                    <input type="text" name="shipment_start_date" id="shipment_start_date" value="<?php echo (!empty($bp_order_details['shipment_start_date'])) ? date('d-m-Y', strtotime($bp_order_details['shipment_start_date'])) : '' ?>" class="form-control datepicker valid input-form-mro date-form-field" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Duration in Weeks<sup>*</sup></label>
                                                    <input type="number" name="duration" id="duration" value="<?php echo (!empty($bp_order_details['duration'])) ? $bp_order_details['duration'] : ""; ?>" placeholder="Enter Duration in Weeks" class="input-form-mro" <?= $inputFieldCss; ?>>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Business Partner Type<sup>*</sup></label>
                                                    <input type="text" name="business_type" id="business_type" value="<?php echo (!empty($bp_order_details['business_type'])) ? $bp_order_details['business_type'] : ""; ?>" placeholder="Business Partner Type" class="input-form-mro" readonly="">
                                                </div>
                                            </div>
                                            <hr class="separator mt0" id="section-mro-3">
                                            <h3 class="form-group-title">Billing and Shipping Addresses</h3>
                                            <div class="form-row form-row-3">
                                                <div class="form-group">
                                                    <label for="">Billing Address</label>
                                                    <div id="billing_details_div">
                                                        <select name="billing_details_id" id="billing_details_id" class="select-form-mro" <?= $dropDownCss; ?>>
                                                            <option value="0" disabled selected hidden>Select Billing Address</option>
                                                        </select>
                                                    </div>
                                                    <div class="ba-single ba-select" id="billing_details_add_div">

                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">Ship To or From Address</label>
                                                    <div id="shipping_details_div">
                                                        <select name="shipping_details_id" id="shipping_details_id" class="select-form-mro" <?= $dropDownCss; ?>>
                                                            <option value="0" disabled selected hidden>Select Shipping Address</option>
                                                        </select>
                                                    </div>
                                                    <div class="ba-single ba-select" id="shipping_details_add_div">

                                                    </div>
                                                    <div id="section-mro-4"></div>
                                                </div>
                                            </div>
                                            <div id="section-mro-5"></div>
                                            <hr class="separator mt0">
                                            <h3 class="form-group-title">Terms</h3>
                                            <div class="form-row form-row-3">
                                                <div class="form-group input-edit">
                                                    <label for="">Payment Terms</label>
                                                    <div id="patment_terms_text" style="display:inline-block;width: 100%;">
                                                        <input type="text" name="patment_terms" id="patment_terms" value="<?php echo (!empty($bp_order_details['patment_terms'])) ? $bp_order_details['patment_terms'] : ""; ?>" placeholder="Typed Response" class="input-form-mro" <?= $inputFieldCss; ?>>
                                                        <?php if ($view == 0) { ?>
                                                            <a href="#/"><img onclick="showHidePaymentDiv(1)" src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt=""></a>
                                                        <?php } ?>
                                                    </div>

                                                </div>
                                                <div class="form-group input-edit">
                                                    <label for="">Incoterm</label>
                                                    <div id="incoterms_text" style="display:inline-block;width: 100%;">
                                                        <input type="text" name="incoterms" id="incoterms" value="<?php echo (!empty($bp_order_details['incoterms'])) ? $bp_order_details['incoterms'] : ""; ?>" placeholder="Typed Response" class="input-form-mro" <?= $inputFieldCss; ?>>
                                                        <?php if ($view == 0) { ?>
                                                            <a href="#/"><img onclick="showHidePaymentDiv(2)" src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt=""></a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr class="separator mt0">
                                            <h3 class="form-group-title">Add SKUs to Order</h3>
                                            <div class="form-row form-row-2">
                                                <?php /* <div class="form-group">
                                                  <label for="">SKU Description</label>
                                                  <select name="sku_id" id="sku_id" class="basic-single select-form-mro" onchange="getskuDetails(this.value)" <?=$dropDownCss;?>>
                                                  <option value="">Select SKU Description</option>
                                                  <?php
                                                  foreach ($skuDetails as $skuValue) {
                                                  ?>
                                                  <option value="<?php echo $skuValue['sku_id']; ?>">
                                                  <?php echo $skuValue['product_name']; ?>
                                                  </option>
                                                  <?php
                                                  }
                                                  ?>
                                                  </select>
                                                  </div> */ ?>

                                                <div class="form-group" id="sku-multiselect-id">
                                                    <label for="">SKU Description</label>
                                                    <div class="multiselect-dropdown-wrapper" style="width:100%">
                                                        <div class="md-value">
                                                            Search for SKU Description
                                                        </div>
                                                        <div class="md-list-wrapper">
                                                            <input type="text" placeholder="Search" class="md-search">
                                                            <div class="md-list-items ud-list">
                                                                <?php if ($skuDetails) {
                                                                    foreach ($skuDetails as $skuValue) {
                                                                        ?>
                                                                        <div class="mdli-single ud-list-single">
                                                                            <label class="container-checkbox"><?= $skuValue['sku_number'] ?> - <?= $skuValue['product_name'] ?>
                                                                                <input type="checkbox" id="sku_id<?= $skuValue['sku_id'] ?>" name="sku_id" value="<?= $skuValue['sku_id'] ?>">
                                                                                <span class="checkmark-checkbox"></span>
                                                                            </label>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="">&nbsp;</label>
<?php if ($view == 0) { ?>
                                                        <button class=" remove-sku" type="button" onclick="getskuDetails()" style="margin-top:18px;">
                                                            <img src="<?php echo base_url(); ?>assets/images/add-more-dark.svg" alt="" class="add-more-img">&nbsp;&nbsp;Add
                                                        </button>
<?php } ?>
                                                </div>
                                                <div class="sku-list-wrapper add-sku-wrapper">
                                                    <?php
                                                    $srNo = 0;
                                                    if (empty($bp_skudetails)) {
                                                        ?>
                                                        <table class="table  product_location1 table-bordered table-striped disable_" id="admore_inner_table_0">
                                                            <tbody>
                                                                <tr tr_inner_count="0" class="active">

                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                            <?php } else { ?>
                                                        <table class="table  product_location1 table-bordered table-striped disable_" id="admore_inner_table_0">
                                                            <tbody>
                                                                <?php
                                                                foreach ($bp_skudetails as $key => $value) {
                                                                    $skuArr[] = $bp_skudetails[$key]['sku_id'];
                                                                    ?>
                                                                    <tr tr_inner_count="<?= $key ?>" class="active">
                                                                        <td>
                                                                            <div class="sit-row">
                                                                                <div class="sit-single sku-item-code">
                                                                                    <p class="sit-single-title">Item Code</p>
                                                                                    <p class="sit-single-value"><?= $bp_skudetails[$key]['sku_number']; ?></p>
                                                                                    <input type="hidden" name="sku_id[<?= $key ?>]" id="sku_id<?= $key ?>" value="<?= (!empty($bp_skudetails[$key]['sku_id'])) ? $bp_skudetails[$key]['sku_id'] : '' ?>" placeholder="Enter Contact Person" class="input-form-mro">
                                                                                </div>
                                                                                <div class="sit-single sku-pd">
                                                                                    <p class="sit-single-title">Product Description</p>
                                                                                    <p class="sit-single-value"><?= $bp_skudetails[$key]['product_name']; ?></p>
                                                                                </div>
                                                                                <div class="sit-single sku-size">
                                                                                    <p class="sit-single-title">Size</p>
                                                                                    <p class="sit-single-value"><?= $bp_skudetails[$key]['size_name']; ?></p>
                                                                                </div>
                                                                                <div class="sit-single sku-qty form-group">
                                                                                    <p class="sit-single-title">Quantity<sup>*</sup></p>
                                                                                    <input type="number" required name="sku_qty[<?= $key ?>]" id="sku_qty<?= $key ?>" value="<?= (!empty($bp_skudetails[$key]['quantity'])) ? $bp_skudetails[$key]['quantity'] : '0' ?>" class="input-form-mro" placeholder="Enter quantity" <?= $inputFieldCss; ?>>
                                                                                </div>
                                                                                <div class="sit-single sku-price form-group">
                                                                                    <p class="sit-single-title">Price per unit in USD</p>
                                                                                    <input type="number" name="sku_price[<?= $key ?>]" id="sku_price<?= $key ?>" value="<?= (!empty($bp_skudetails[$key]['price'])) ? $bp_skudetails[$key]['price'] : '' ?>" class="input-form-mro" placeholder="Enter Price" <?= $inputFieldCss; ?>>
                                                                                </div>
                                                                                <div class="sit-single sku-remove">
                                                                                    <?php if ($view == 0) { ?>
                                                                                        <button title="Remove" class="remove-sku remove inner_remove" onclick="remove_sku('<?= $bp_skudetails[$key]['sku_id'] ?>', this)" type="button"><img src="<?php echo base_url(); ?>assets/images/remove-dark.svg" alt="" class="add-more-img">&nbsp&nbspRemove</button>
        <?php } ?>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                        <?php } ?>
                                                            </tbody>
                                                        </table>
<?php } ?>
                                                    <input type="hidden" id="count" value="<?= $srNo ?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="scroll-content-right">
                                            <div class="vertical-scroll-links">
                                                <a href="#section-mro-1" class="active">Contract Details</a>
                                                <a href="#section-mro-2">Shipment Dates</a>
                                                <a href="#section-mro-3">Billing and Shipping Addresses</a>
                                                <a href="#section-mro-4">Terms</a>
                                                <a href="#section-mro-5">Add SKUs to Order</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
<?php if ($view == 1) { ?>

                                    <div class="tab-pane fade" id="other-details" role="tabpanel" aria-labelledby="other-details-tab" style="padding:30px">
                                        <div class="bp-list-wrapper">
                                            <div class="table-responsive">
                                                <table class="table view-other-bporder">
                                                    <thead>
                                                        <tr>
                                                            <th>Contract Number</th>
                                                            <th>Shipment Start Date</th>
                                                            <th>Duration in Weeks</th>
                                                            <th style="width:250px">Product Category</th> 
                                                            <th>Linked Master Order</th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($contractDetails as $value) { 
                                                            $url = base_url()."order/addEdit?text";
                                                            $orderUrl = '<a href="'.$url.'=' . rtrim(strtr(base64_encode("id=" . $value['order_id']), '+/', '-_'), '=') . '&view=1" ><u>'.$value['master_contract'].'</u></a>';
                                                            $updatedBy ='';
                                                            if(!empty($value['firstname'])){
                                                                $updatedBy = $value['firstname'].' '.substr($value['lastname'],0, 1).', '.date('d-M-y', strtotime($value['updated_on']));
                                                            }
                                                            $containerBible = '#';
                                                            if($this->privilegeduser->hasPrivilege("ContainersDetailsList")){ 
                                                               $containerBible = base_url()."listOfContainer?text=" . rtrim(strtr(base64_encode("order_id=" . $value['order_id']."&bp_order_id=". $value['bp_order_id']), '+/', '-_'), '='); 
                                                            }
                                                            ?> 
                                                            <tr>
                                                                <td><?= !empty($value['nick_name'])?$value['nick_name'].', '.$value['contract_number']:$value['contract_number']?></td>
                                                                <td><?=date('d-M-y', strtotime($value['shipment_start_date'])) ?></td>
                                                                <td><?=$value['duration']?> Weeks</td>
                                                                <td><?= $value['category']?></td>
                                                                <td><?=$orderUrl?></td>
                                                                <td>
                                                                    <div class="cs-status-text">
                                                                        <p class="last-update-title">Last Updated</p>
                                                                        <p class="last-update-name"><?=$updatedBy?></p>
                                                                    </div>
                                                                </td>
                                                                <td><a href="<?=$containerBible?>" class="btn-grey-mro">View Container Bible</a></td>
                                                            </tr> 
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div> 
                                </div> 
                            </div>
<?php } ?>    
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<script>
    //show and hide payment term and incoterm dropdown
    function showHidePaymentDiv(cnt) {
        var act = "<?php echo base_url(); ?>bporder/getPaymentIncoterm";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                cnt: cnt
            },
            dataType: "json",
            success: function (data) {
                if (cnt == 1) {
                    $('#patment_terms_text').html(data.paymentTerm);
                } else {
                    $('#incoterms_text').html(data.incoTerm);
                }
                $('.basic-single').select2();
            }
        });
    }
    //add sku details function
    var sku = <?php echo json_encode($skuArr); ?>;

    function getskuDetails() {
        var chk = 0;
        var dropdownchk = 0;
        var array = [];
        $("input:checkbox[name=sku_id]:checked").each(function () {
            chk = 1;
            var sku_id = $(this).val();
            array.push(sku_id);
            if (sku.includes(sku_id.toString())) {
                showInsertUpdateMessage("Data is already added in below list", false, 1500);
                return false;
            } else {
                sku.push(sku_id);
                dropdownchk = 1;
            }
            console.log(sku);

            var act = "<?php echo base_url(); ?>bporder/getskuDetails";
            $.ajax({
                type: 'GET',
                url: act,
                data: {
                    sku_id: sku_id
                },
                dataType: "json",
                success: function (data) {
                    //var x = $(this).attr("panel-count");
                    var x = $('#count').val();
                    // alert(x);
                    // return;
                    var tx = $("#admore_inner_table_" + x + " tbody tr:last-child").attr("tr_inner_count");
                    tx++;
                    if (isNaN(tx) || tx == "undefined") {
                        tx = 0;
                    }

                    var inner_tr = '<tr tr_inner_count="' + tx + '" class="active">' +
                            '<td>' +
                            '<div class="sit-row">' +
                            '<div class="sit-single sku-item-code">' +
                            '<p class="sit-single-title">Item Code</p>' +
                            '<p class="sit-single-value">' + data.skuData.sku_number + '</p>' +
                            '<input type="hidden" name="sku_id[' + tx + ']" id="sku_id' + tx + '" value="' + data.skuData.sku_id + '" placeholder="Enter Contact Person" class="input-form-mro">' +
                            '</div>' +
                            '<div class="sit-single sku-pd">' +
                            '<p class="sit-single-title">Product Description</p>' +
                            '<p class="sit-single-value">' + data.skuData.product_name + '</p>' +
                            '</div>' +
                            '<div class="sit-single sku-size">' +
                            '<p class="sit-single-title">Size</p>' +
                            '<p class="sit-single-value">' + data.skuData.size_name + '</p>' +
                            '</div>' +
                            '<div class="sit-single sku-qty form-group">' +
                            '<p class="sit-single-title">Quantity<sup>*</sup></p>' +
                            '<input type="number" name="sku_qty[' + tx + ']" id="sku_qty' + tx + '" value="" class="input-form-mro" placeholder="Enter quantity" required>' +
                            '</div>' +
                            '<div class="sit-single sku-price form-group">' +
                            '<p class="sit-single-title">Price per unit in USD</p>' +
                            '<input type="number" name="sku_price[' + tx + ']" id="sku_price' + tx + '" value="0.00" class="input-form-mro" placeholder="Enter Price">' +
                            '</div>' +
                            '<div class="sit-single sku-remove">' +
                            '<button title="Remove" class="remove-sku remove inner_remove" onclick="remove_sku(' + data.skuData.sku_id + ',this)" type="button"><img src="<?php echo base_url(); ?>assets/images/remove-dark.svg" alt="" class="add-more-img">&nbsp&nbspRemove</button>' +
                            '</div>' +
                            '</div>' +
                            '</td>' +
                            '</tr>';
                    $("#admore_inner_table_" + x + " tbody").append(inner_tr);
                    //console.log(inner_tr);  
                }
            });
        });
        if (chk == 0) {
            showInsertUpdateMessage("Please select at least one checkbox", false, 1500);
        }
        if (dropdownchk == 1) {
            var act = "<?php echo base_url(); ?>bporder/getskuDetailsDropdown";
            $.ajax({
                type: 'GET',
                url: act,
                data: {},
                dataType: "json",
                success: function (data) {
                    $('#sku-multiselect-id').html(data.dropdown);
                }
            });
        }
    }
    //remove sku details
    //  $(document).on("click", '.inner_remove', function() {
    //    var tr_inner_count = $(this).closest('tr').attr('tr_inner_count');
    //    $(this).closest('tr').remove();
    //  });

    function remove_sku(sku_id, div) {
        var tr_inner_count = $(div).closest('tr').attr('tr_inner_count');
        $(div).closest('tr').remove();
        //sku.pop(sku_id);
        var result = arrayRemove(sku, sku_id);
        sku = result;
        console.log(sku);
    }

    function arrayRemove(arr, value) {
        return arr.filter(function (ele) {
            return ele != value;
        });
    }
</script>
<script>
    //code for get business details and shipping details in dropdown also get payment and incoterm
    function getBillingShippingDetails(bp_id = 0, billing_id = '', shipping_id = '', paymentIcoterm = 0) {
        var act = "<?php echo base_url(); ?>bporder/getBusinessPartner";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                bp_id: bp_id,
                billing_id: billing_id,
                shipping_id: shipping_id,
                isView: <?= $view ?>
            },
            dataType: "json",
            success: function (data) {
                $('#business_type').val(data.business_type);
                $('#billing_details_div').html(data.billing);
                $('#shipping_details_div').html(data.shipping);
                if (paymentIcoterm == 0) {
                    $('#patment_terms').val(data.payment);
                    $('#incoterms').val(data.incoterm);
                }
                $('.basic-single').select2();
            }
        });
        getBillAddress(billing_id);
        getShipAddress(shipping_id);
    }
    //code for show billing address details
    function getBillAddress(bd_id = 0) {
        var act = "<?php echo base_url(); ?>bporder/getBillingAddress";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                bd_id: bd_id
            },
            dataType: "json",
            success: function (data) {
                $('#billing_details_add_div').html(data.billing);
            }
        });
    }
    //code for show shipping address details
    function getShipAddress(sd_id = 0) {
        var act = "<?php echo base_url(); ?>bporder/getShipAddress";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                bd_id: sd_id
            },
            dataType: "json",
            success: function (data) {
                $('#shipping_details_add_div').html(data.billing);
            }
        });
    }


    //code for edit case get business details and shipping details in dropdown also get payment and incoterm
<?php if (!empty($bp_order_details['business_partner_id'])) { ?>
        getBillingShippingDetails('<?php echo (!empty($bp_order_details['business_partner_id'])) ? $bp_order_details['business_partner_id'] : "0"; ?>', '<?php echo (!empty($bp_order_details['billing_details_id'])) ? $bp_order_details['billing_details_id'] : "0"; ?>', '<?php echo (!empty($bp_order_details['shipping_details_id'])) ? $bp_order_details['shipping_details_id'] : "0"; ?>', '1');
<?php } ?>
</script>
<script>
    $(document).ready(function () {

        var vRules = {
            "contract_number": {
                required: true
            },
            "business_partner_id": {
                required: true
            },
            "nick_name": {
                required: true
            },
            "shipment_start_date": {
                required: true
            },
            "duration": {
                required: true
            },
            "business_type": {
                required: true
            }
        };
        var vMessages = {
            "contract_number": {
                required: "Please enter contract number."
            },
            "business_partner_id": {
                required: "Please select business partner."
            },
            "nick_name": {
                required: "Please enter nick name."
            },
            "shipment_start_date": {
                required: "Please select shipment start date."
            },
            "duration": {
                required: "Please enter duration in weeks."
            },
            "business_type": {
                required: "Please enter business partner type."
            }
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            submitHandler: function (form) {
                var act = "<?php echo base_url(); ?>bporder/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                        $("#docuement_loader").show();
                    },
                    success: function (response) {
                        $("#docuement_loader").hide();
                        showInsertUpdateMessage(response.msg, response);
                        if (response.success) {
                            setTimeout(function () {
                                window.location = "<?= base_url('bporder') ?>";
                            }, 3000);
                        }
                        $(".btn-primary-mro").show();
                    }
                });
            }
        });

    });
</script>