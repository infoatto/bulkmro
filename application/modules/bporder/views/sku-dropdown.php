<label for="">SKU Description</label>
<div class="multiselect-dropdown-wrapper" style="width:100%">
  <div class="md-value">
    Search for SKU Description
  </div>
  <div class="md-list-wrapper">
    <input type="text" placeholder="Search" class="md-search">
    <div class="md-list-items ud-list">
      <?php if ($skuDetails) {
        foreach ($skuDetails as $skuValue) { ?>
          <div class="mdli-single ud-list-single">
            <label class="container-checkbox"><?= $skuValue['sku_number'] ?> - <?= $skuValue['product_name'] ?>
              <input type="checkbox" id="sku_id<?= $skuValue['sku_id'] ?>" name="sku_id" value="<?= $skuValue['sku_id'] ?>">
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
      <?php
        }
      }
      ?>
    </div>
  </div>