<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                            <span>></span>
                            <p>All Business Partners Contract</p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title"><a href="<?php echo base_url("bporder"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Business Partners Contract</h1>
                        </div>
                    </div>
                    <div class="title-sec-right">
                        <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")) { ?>
                            <a href="<?php echo base_url("bporder/addEdit"); ?>" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> Add Business Partner Contract</a>
                        <?php } ?>
                        <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersOrderImport")) { ?>                            
                            <a href="#" class="btn-primary-mro" id="import">BP Contract Import</a>
                            <a href="#" class="btn-primary-mro" id="skuimport">BP Contract SKU Import</a>
                        <?php } ?>
                    </div>
                </div> 
                <div id="docuement_loader" style="display: none;">
                    <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                </div>
                <div class="page-content-wrapper" id="skuefrom" style="display:none;">
                    <form id="sku_excel_form" name="sku_excel_form" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload Excel</h6> 
                            <input type="file" id="skuexcelfile" name="skuexcelfile" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel('skuexcelfile')" class="btn-primary-mro upload-button-sku">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample_bp_contract_sku.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div>  
                <div class="page-content-wrapper" id="efrom" style="display:none;">
                    <form id="excel_form" name="excel_form" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload Excel</h6> 
                            <input type="file" id="excelfile" name="excelfile" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel('excelfile')" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample_bp_contract.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div> 
                </br> 
                
                <div class="page-content-wrapper1">
                    <div id="serchfilter" class="filter-sec-wrapper">
                        <div class="form-group filter-search dataTables_filter searchFilterClass">
                            <input type="text" id="sSearch_0" name="sSearch_0" class="searchInput filter-search-input" placeholder="Contract Number">
                        </div>
                        <div class="form-group filter-search dataTables_filter searchFilterClass">
                            <input type="text" id="sSearch_1" name="sSearch_1" class="searchInput filter-search-input" placeholder="Nick Name">
                        </div>
                        <div class="form-group  dataTables_filter searchFilterClass">
                            <select name="sSearch_2" id="sSearch_2" class="searchInput basic-single select-mro select-xl">
                                <option value="" disabled selected hidden>Business Partner Type</option>
                                <option value="Customer">Customer</option>
                                <option value="Vendor">Vendor</option>
                            </select>
                        </div>
                        <div class="form-group clear-search-filter">
                            <button type="reset"  onclick="refresh()"  class="select-small btn-primary-mro">Clear</button>
                        </div>
                    </div>
                </div>
                <div class="bp-list-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Contract Number</th>
                                    <th>Business Partner Type</th>
                                    <th>Nick Name</th>
                                    <th>Shipment Start Date</th>
                                    <th>Duration in Weeks</th>
                                    <th>Payment Terms</th>
                                    <th>Incoterm</th>
                                    <th>Status</th>
                                    <th class="table-action-cls" style="width:84px !important;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
 function refresh(){
    location.reload();
  }
 function uploadExcel(id)
 {
    if( document.getElementById(id).files.length == 0 ){ 
        showInsertUpdateMessage("Please Choose a xlsx file.",false);   
    }
    else
    {
        if(id=='excelfile'){
            var data = new FormData($('#excel_form')[0]);
            var url = '<?php echo base_url('bporder/importBPorder'); ?>';
            $(".upload-button").hide();
        }else{ 
            var data = new FormData($('#sku_excel_form')[0]);
            var url = '<?php echo base_url('bporder/importBPorderSKU'); ?>';
            $(".upload-button-sku").hide();
        } 
        $("#docuement_loader").show();
        $.ajax({
            url: url,
            type: 'POST',
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            data: data, 
            success: function(data)
            {
                $("#docuement_loader").hide();
                var html="";
                var response = JSON.parse(data); 
                
                if (response.success) {  
                    showInsertUpdateMessage(response.msg,response);
                    showInsertUpdateMessage("Total entry done: "+response.imported,response); 
                    if(response.notimported > 0){
                        showInsertUpdateMessage("Total entry failed: "+response.notimported,false);   
                    }
                    if(id=='excelfile'){                         
                        $('#excel_form')[0].reset();
                    }else{
                        $('#sku_excel_form')[0].reset();
                    } 
                    setTimeout(function () {  
                        location.reload();
                    }, 3000);
                }else{
                    showInsertUpdateMessage(response.msg,response,false);
                } 
                if(id=='excelfile'){  
                    $(".upload-button").show();
                }else{
                    $(".upload-button-sku").show();
                }
            }
        });
    }
}

$("#import").click(function(){
   $("#efrom").toggle();
});

$("#skuimport").click(function(){
   $("#skuefrom").toggle();
});

</script>