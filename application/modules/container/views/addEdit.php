<?php
//start code of view option    
$dropDownCss = '';
$inputFieldCss = '';
if($view==1){
    $dropDownCss = "disabled";
    $inputFieldCss = "readonly";
}
//end code of view option
?>
<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("container"); ?>">Containers List</a>
                                <span>></span>
                                <p>New Container Details</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">New Container Details</h1>
                            </div>
                        </div>
                        <div class="title-sec-right">
                            <?php if($view==0){ ?>
                                <button type="submit" class="btn-primary-mro">Save</button>
                                <a href="<?php echo base_url("container"); ?>" class="btn-transparent-mro cancel">Cancel</a>
                            <?php } ?>
                        </div>
                    </div>
                    <div id="docuement_loader" style="display: none;">
                        <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                    </div>
                    <div class="page-content-wrapper master-order-wrapper">
                        <input type="hidden" name="container_id" id="container_id" value="<?php echo(!empty($container_details['container_id'])) ? $container_details['container_id'] : ""; ?>">
                        <div class="scroll-content-wrapper master-order">
                            <div class="scroll-content-left master-order-left" id="container-details"> 
                                <h3 class="form-group-title" id="section-mro-1">Container Details</h3>
                                <div class="form-row form-row-3">
                                    <div class="form-group input-edit">
                                        <label for="">Container Number <sup>*</sup></label>
                                        <input type="text" name="container_number" id="container_number" value="<?php echo(!empty($container_details['container_number'])) ? $container_details['container_number'] : ""; ?>" placeholder="Enter Contract Number" class="input-form-mro" <?=$inputFieldCss;?>>
                                        
                                    </div>
                                    <div class="form-group">
                                        <label for="">Master Contract<sup>*</sup></label>
                                        <select name="order_id" id="order_id" class="basic-single select-form-mro" onchange="getContractDetails(this.value);removeErroMsg('order_id');" <?=$dropDownCss;?>>
                                            <option value="" disabled selected hidden>Select Master Contract</option>
                                            <?php
                                            foreach ($contractDetails as $ccValue) {
                                                $ccID = (isset($container_details['order_id']) ? $container_details['order_id'] : 0);
                                                ?> 
                                                <option value="<?php echo $ccValue['order_id']; ?>"  <?php
                                                if ($ccValue['order_id'] == $ccID) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                            <?php echo $ccValue['contract_number']; ?> (<?php echo $ccValue['cbp_name']; ?>-<?php echo $ccValue['sbp_name']; ?>)
                                                </option> 
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div> 
<!--                                    <div class="form-group">
                                        <label for="Status">Status</label>
                                        <select name="status" id="status" class="select-form-mro" <?=$dropDownCss;?>>
                                            <option value="Active" <?php echo(!empty($container_details['status']) && $container_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                            <option value="In-active" <?php echo(!empty($container_details['status']) && $container_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                        </select>
                                    </div>-->
                                </div>
                                <div class="new-container-details">
                                    <!-- <hr class="separator mt0"> -->
                                    <div class="view-mo-wrapper" id="customerSupplierDetail">

                                    </div>
                                    <hr class="separator mt0" id="section-mro-2">
                                    <h3 class="form-group-title">SKUs in Container</h3>
                                    <div class="form-row form-row-3">
                                        <div class="sku-list-wrapper add-sku-wrapper" id="skuDetails">
                                            <?php
                                            foreach ($skudetails as $key => $value) {
                                                ?>
                                                <div class="sit-row">
                                                    <div class="sit-single sku-item-code">
                                                        <p class="sit-single-title">Item Code</p>
                                                        <p class="sit-single-value"><?= $skudetails[$key]['sku_number']; ?></p>
                                                        <input type="hidden" name="sku_id[<?= $key ?>]" id="sku_id<?= $key ?>" value="<?= (!empty($skudetails[$key]['sku_id'])) ? $skudetails[$key]['sku_id'] : '' ?>">
                                                    </div>
                                                    <div class="sit-single sku-pd">
                                                        <p class="sit-single-title">Product Description</p>
                                                        <p class="sit-single-value"><?= $skudetails[$key]['product_name']; ?></p>
                                                    </div>
                                                    <div class="sit-single sku-size">
                                                        <p class="sit-single-title">Size</p>
                                                        <p class="sit-single-value"><?= $skudetails[$key]['size_name']; ?></p>
                                                    </div>
                                                    <div class="sit-single sku-qty form-group">
                                                        <p class="sit-single-title">Quantity<sup>*</sup></p>
                                                        <input type="number" required name="sku_qty[<?= $key ?>]" id="sku_qty<?= $key ?>" value="<?= (!empty($skudetails[$key]['quantity'])) ? $skudetails[$key]['quantity'] : '0' ?>"  onkeyup="getContainerSKUQtyEnter('sku_qty<?= $key ?>','<?= (!empty($skudetails[$key]['sku_id'])) ? $skudetails[$key]['sku_id'] : 0 ?>','<?=(isset($container_details['order_id']) ? $container_details['order_id'] : 0)?>');" class="input-form-mro qty" placeholder="Enter quantity" <?=$inputFieldCss;?>>
                                                    </div> 
                                                </div> 
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <hr class="separator mt0" id="section-mro-3">
                                    <div class="form-row form-row-2">
                                        <div class="form-group input-edit" id="freight_forwarder_div">
                                            <label for="">Freight Forwarder</label>
                                            <select name="freight_forwarder_id" id="freight_forwarder_id" class="basic-single select-form-mro" style="width: 100%;" <?=$dropDownCss;?>> 
                                                <option value="0" disabled selected hidden>Select Freight Forwarder</option>
                                                <?php
                                                foreach ($freightForwarder as $value) {
                                                    $bpID = (isset($container_details['freight_forwarder_id']) ? $container_details['freight_forwarder_id'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['business_partner_id']; ?>"  <?php
                                                    if ($value['business_partner_id'] == $bpID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['alias']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div>
                                        <div class="form-group input-edit" id="cha_div">
                                            <label for="">CHA</label>
                                            <select name="broker_id" id="broker_id" class="basic-single select-form-mro" style="width: 100%;" <?=$dropDownCss;?>> 
                                                <option value="0" disabled selected hidden>Select CHA</option>
                                                <?php
                                                foreach ($chaDetails as $value) {
                                                    $bpID = (isset($container_details['broker_id']) ? $container_details['broker_id'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['business_partner_id']; ?>"  <?php
                                                    if ($value['business_partner_id'] == $bpID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['alias']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="form-row form-row-4"> 
                                        <div class="form-group">
                                            <label for="">ETD</label>                                           
                                            <input type="text" name="etd" id="etd" value="<?php echo(!empty($container_details['etd'])) ? date('d-m-Y', strtotime($container_details['etd'])) : '' ?>" onchange="fillRevisedETD(this.value);fillETT();" class="form-control valid input-form-mro datepicker date-form-field" <?=$inputFieldCss;?> >                                             
                                        </div>
                                        <div class="form-group">
                                            <label for="">ETA</label>
                                            <input type="text" name="eta" id="eta" value="<?php echo(!empty($container_details['eta'])) ? date('d-m-Y', strtotime($container_details['eta'])) : '' ?>" onchange="fillRevisedETA(this.value);fillETT();" class="form-control valid input-form-mro datepicker date-form-field" <?=$inputFieldCss;?> >                                                              
                                        </div>
                                        <div class="form-group">
                                            <label for="">Actual date of Departure</label>
                                            <input type="text" name="revised_etd" id="revised_etd" value="<?php echo(!empty($container_details['revised_etd'])) ? date('d-m-Y', strtotime($container_details['revised_etd'])) : '' ?>" onchange="fillRevisedETT();" class="form-control valid input-form-mro datepicker date-form-field" <?=$inputFieldCss;?> > 
                                        </div>
                                        <div class="form-group">
                                            <label for="">Actual date of Arrival</label>
                                            <input type="text" name="revised_eta" id="revised_eta" value="<?php echo(!empty($container_details['revised_eta'])) ? date('d-m-Y', strtotime($container_details['revised_eta'])) : '' ?>" onchange="fillRevisedETT();" class="form-control valid input-form-mro datepicker date-form-field" <?=$inputFieldCss;?> > 
                                        </div>    
                                    </div>
                                    <div class="form-row form-row-2">
                                        <div class="form-group">
                                            <label for="">ETT In Days</label>
                                            <input type="text" name="ett" id="ett" value="<?php echo(!empty($container_details['ett'])) ? $container_details['ett'] : ""; ?>" placeholder="No ETA or ETD Entered" class="input-form-mro" <?=$inputFieldCss;?>>
                                            <!--<a href="#/"><img src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt=""></a>-->
                                        </div>
                                        <div class="form-group"><!--input-edit-->
                                            <label for="">Actual Transit Time In Days</label>
                                            <input type="text" name="revised_ett" id="revised_ett" value="<?php echo(!empty($container_details['revised_ett'])) ? $container_details['revised_ett'] : ""; ?>" placeholder="No ETA or ETD Entered" class="input-form-mro" <?=$inputFieldCss;?>>
                                            <!--<a href="#/"><img src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt=""></a>-->
                                        </div>
                                    </div>
                                    <div class="form-row form-row-2">
                                        <div class="form-group">
                                            <label for="">Liner Name</label>
                                            <select name="liner_name" id="liner_name" class="basic-single select-form-mro" style="width: 100%;" <?=$dropDownCss;?>> 
                                                <option value="0" disabled selected hidden>Select Liner Name</option>
                                                <?php
                                                foreach ($liner as $value) {
                                                    $linerID = (isset($container_details['liner_name']) ? $container_details['liner_name'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['liner_id']; ?>"  <?php
                                                    if ($value['liner_id'] == $linerID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['liner_name']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div>
                                        <div class="form-group ">
                                            <label for="">Liner Tracker URL</label>
                                            <input type="text" name="liner_tracker_url" id="liner_tracker_url" value="<?php echo(!empty($container_details['liner_tracker_url'])) ? $container_details['liner_tracker_url'] : ""; ?>" placeholder="Enter Liner Tracker URL" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                    </div>
                                    <div class="form-row form-row-2">
                                        <div class="form-group">
                                            <label for="">POL</label>
                                            <select name="pol" id="pol" class="basic-single select-form-mro" style="width: 100%;" <?=$dropDownCss;?>> 
                                                <option value="0" disabled selected hidden>Select POL</option>
                                                <?php
                                                foreach ($pol as $value) {
                                                    $polID = (isset($container_details['pol']) ? $container_details['pol'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['pol_id']; ?>"  <?php
                                                    if ($value['pol_id'] == $polID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['pol_name']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select>                                             
                                        </div>
                                        <div class="form-group ">
                                            <label for="">POD</label>
                                            <select name="pod" id="pod" class="basic-single select-form-mro" style="width: 100%;" <?=$dropDownCss;?>> 
                                                <option value="0" disabled selected hidden>Select POD</option>
                                                <?php
                                                foreach ($pod as $value) {
                                                    $podID = (isset($container_details['pod']) ? $container_details['pod'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['pod_id']; ?>"  <?php
                                                    if ($value['pod_id'] == $podID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['pod_name']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row form-row-2">
                                        <div class="form-group">
                                            <label for="">Vessel Name</label>
                                            <input type="text" name="vessel_name" id="vessel_name" value="<?php echo(!empty($container_details['vessel_name'])) ? $container_details['vessel_name'] : ""; ?>" placeholder="Enter Vessel Name" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                        <div class="form-group">
                                            <label for="">Vessel Tracker URL</label>
                                            <input type="text" name="vessel_tracker_url" id="vessel_tracker_url" value="<?php echo(!empty($container_details['vessel_tracker_url'])) ? $container_details['vessel_tracker_url'] : ""; ?>" placeholder="Enter Vessel Tracker URL" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                    </div>
                                    <div class="form-row form-row-3">
                                        <div class="form-group">
                                            <label for="">Flag</label>
                                            <select name="flag" id="flag" class="basic-single select-form-mro" style="width: 100%;" <?=$dropDownCss;?>> 
                                                <option value="0" disabled selected hidden>Select Flag</option>
                                                <?php
                                                foreach ($flag as $value) {
                                                    $flagID = (isset($container_details['flag']) ? $container_details['flag'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['flag_id']; ?>"  <?php
                                                    if ($value['flag_id'] == $flagID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['flag_name']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div> 
                                        <div class="form-group">
                                            <label for="">Lot Number</label>
                                            <input type="text" name="lot_number" id="lot_number" value="<?php echo(!empty($container_details['lot_number'])) ? $container_details['lot_number'] : ""; ?>" placeholder="Enter Lot Number" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                        <div class="form-group">
                                            <label for="">POD Terminal</label>
                                            <input type="text" name="terminal" id="terminal" value="<?php echo(!empty($container_details['terminal'])) ? $container_details['terminal'] : ""; ?>" placeholder="Enter Terminal" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                    </div>  
                                    <hr class="separator mt0" id="section-mro-4">
                                    <div class="form-row form-row-3"> 
                                        <div class="form-group">
                                            <label for="">Shipping Container</label>
                                            <input type="text" name="shipping_container" id="shipping_container" value="<?php echo(!empty($container_details['shipping_container'])) ? $container_details['shipping_container'] : ""; ?>" placeholder="Enter Shipping Container" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                        <div class="form-group">
                                            <label for="">SGS Seal</label>
                                            <input type="text" name="sgs_seal" id="sgs_seal" value="<?php echo(!empty($container_details['sgs_seal'])) ? $container_details['sgs_seal'] : ""; ?>" placeholder="Enter SGS Seal" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                        <div class="form-group">
                                            <label for="">FF Seal</label>
                                            <input type="text" name="ff_seal" id="ff_seal" value="<?php echo(!empty($container_details['ff_seal'])) ? $container_details['ff_seal'] : ""; ?>" placeholder="Enter FF Seal" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div>
                                    </div>
                                    
                                    <hr class="separator mt0" id="section-mro-5">
                                    <div class="form-row form-row-2">
                                        <div class="form-group">
                                            <label for="">Email Sent On</label>                                            
                                            <input type="text" name="email_sent_on" id="email_sent_on" value="<?php echo(!empty($container_details['email_sent_on'])) ? date('d-m-Y', strtotime($container_details['email_sent_on'])) : '' ?>" class="form-control valid input-form-mro datepicker date-form-field" <?=$inputFieldCss;?> readonly> 
                                        </div> 
                                        <div class="form-group">
                                            <label for="">Email Subject</label>
                                            <input type="text" name="email_subject" id="email_subject" value="<?php echo(!empty($container_details['email_subject'])) ? $container_details['email_subject'] : ""; ?>" placeholder="Enter Subject" class="input-form-mro" <?=$inputFieldCss;?>>
                                        </div> 
                                    </div> 
                                    <div class="form-row form-row-1">
                                        <div class="form-group">
                                            <label for="notes">Notes</label> 
                                            <textarea class="form-control" name="notes" id="notes" rows="3" cols="100" placeholder="Enter Notes"><?php echo(!empty($container_details['notes'])) ? $container_details['notes'] : ""; ?></textarea> 
                                        </div> 
                                    </div>
                                </div>
                            </div>
                            <div class="scroll-content-right master-order-right">
                                <div class="vertical-scroll-links">
                                    <a href="#section-mro-1" class="active">Container</a>
                                    <a href="#section-mro-2">SKU Quantities</a>
                                    <a href="#section-mro-3">Shipping Details</a>
                                    <a href="#section-mro-4">Seal Details</a> 
                                    <a href="#section-mro-5">Email Details</a> 
                                </div>
                            </div> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section> 
 
<script>
    //code for autofill container number at time of add new container
<?php if (empty($container_details['container_id'])) { ?>
        function fillContainerNumber() {
            var act = "<?php echo base_url(); ?>container/getContractNumber";
            $.ajax({
                type: 'GET',
                url: act,
                data: {},
                dataType: "json",
                success: function (data) {
                    $('#container_number').val(data.containerNumber);
                }
            });
        }

        fillContainerNumber();
<?php } ?>
    //code for get master contract details
    function getContractDetails(contract_id, is_edit = 0) {
        var act = "<?php echo base_url(); ?>container/getContractDetails";
        $.ajax({
            type: 'GET',
            url: act,
            data: {contract_id: contract_id, is_edit: is_edit,isView:<?=$view?>},
            dataType: "json",
            success: function (data) {
                $('.new-container-details').show();
                $('#customerSupplierDetail').html(data.custmerSupplier);
                if (is_edit == 0) {
                    //$("#freight_forwarder_id").select2().select2('val', data.freight_forwarder_id);
                    $("#freight_forwarder_div").html(data.freight_forwarder_id);
                    $("#freight_forwarder_id").select2();
                    $("#cha_div").html(data.broker_id);
                    $("#broker_id").select2();
                    $('#skuDetails').html(data.sku); 
                }
            }
        });
    }

    //code for fill Actual date of Arrival
    function fillRevisedETA(value) {
        $('#revised_eta').val(value);
    }

    //code for fill Actual date of Departure
    function fillRevisedETD(value) {
        $('#revised_etd').val(value);
    }

    function fillETT() {
        var revised_eta = $('#eta').val();
        var revised_etd = $('#etd').val();
        var act = "<?php echo base_url(); ?>container/getETTDays";
        $.ajax({
            type: 'GET',
            url: act,
            data: {revised_eta: revised_eta, revised_etd: revised_etd},
            dataType: "json",
            success: function (data) {
                $('#ett').val(data.days); 
            }
        });
        fillRevisedETT();
    }
    
    function fillRevisedETT() {
        var revised_eta = $('#revised_eta').val();
        var revised_etd = $('#revised_etd').val();
        var act = "<?php echo base_url(); ?>container/getETTDays";
        $.ajax({
            type: 'GET',
            url: act,
            data: {revised_eta: revised_eta, revised_etd: revised_etd},
            dataType: "json",
            success: function (data) { 
                $('#revised_ett').val(data.days);
            }
        });
    } 
    
    function getContainerSKUQtyEnter(id,sku_id,order_id){
        var container_id = $('#container_id').val();
        if(sku_id==0 || order_id==0){
            $('#'+id).val('0');
            return false;
        }
        var quantity = $('#'+id).val();
        
        var act = "<?php echo base_url(); ?>container/getContainerSKUQtyEnter";
        $.ajax({
            type: 'GET',
            url: act,
            data: {sku_id: sku_id, order_id: order_id, quantity: quantity,container_id:container_id},
            dataType: "json",
            success: function (response) {
                if(response.fail){
                    showInsertUpdateMessage(response.msg,false,1000);  
                    $('#'+id).val(response.remainingQty);
                }
            }
        });
    }

    //code for get state in case of edit city
<?php if (!empty($container_details['container_id'])) { ?>
        getContractDetails('<?php echo(!empty($container_details['order_id'])) ? $container_details['order_id'] : "0"; ?>', '1');
<?php } ?>
</script>
<script>  
    
    function qtyCheckValidation(){
        var isCheck = false;
        var count = $('.qty').length; 
        for (let i = 0; i < count; i++) {
             if($('#sku_qty'+i).val() > 0){
                 var isCheck = true;
             }
        } 
        //console.log(isCheck);
        return isCheck;
    }
    
    $(document).ready(function () {
        var vRules = {
            "container_number": {required: true},
            "order_id": {required: true}
        };
        var vMessages = {
            "container_number": {required: "Please enter container number."},
            "order_id": {required: "Please select master contract."}
        };
        //check and save city data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function(error, e) {
              e.parents('.form-group').append(error);
            },
            submitHandler: function (form)
            {
                
                var qtyCheck = qtyCheckValidation();
                
                if(qtyCheck){        
                    var act = "<?php echo base_url(); ?>container/submitForm";
                    $("#addEditForm").ajaxSubmit({
                        url: act,
                        type: 'post',
                        dataType: 'json',
                        cache: false,
                        clearForm: false,
                        beforeSubmit: function (arr, $form, options) {
                            $(".btn-primary-mro").hide();
                            $("#docuement_loader").show();
                        },
                        success: function (response) { 
                            $("#docuement_loader").hide();
                            showInsertUpdateMessage(response.msg,response);                        
                            if (response.success) { 
                                setTimeout(function () {                                
                                    window.location = "<?= base_url('listOfContainer') ?>";
                                }, 3000);
                            }  
                            $(".btn-primary-mro").show(); 
                        }
                    });
                }else{
                    showInsertUpdateMessage("Please enter at least one SKU Quantity",false,1500); 
                }
            }
        });

    });
</script>