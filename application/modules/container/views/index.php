<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                            <span>></span>
                            <p>All Containers</p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title"><a href="<?php echo base_url("container"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Containers</h1>
                        </div>
                    </div>
                    <div class="title-sec-right">
                        <?php if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) { ?>
                            <a href="<?php echo base_url("container/addEdit"); ?>" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> New Container Details</a>
                        <?php } ?>
                        <?php if ($this->privilegeduser->hasPrivilege("ContainersImport")) { ?>                            
                            <a href="#" class="btn-primary-mro" id="import">Container Import</a>
                            <a href="#" class="btn-primary-mro" id="skuimport">Container SKU Import</a>
                        <?php } ?>
                    </div>
                </div>
                
                <div id="docuement_loader" style="display: none;">
                    <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                </div>
                
                 <div class="page-content-wrapper" id="skuefrom" style="display:none;">
                    <form id="sku_excel_form" name="sku_excel_form" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload Excel</h6> 
                            <input type="file" id="skuexcelfile" name="skuexcelfile" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel('skuexcelfile')" class="btn-primary-mro upload-button-sku">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample_container_sku.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div>  
                <div class="page-content-wrapper" id="efrom" style="display:none;">
                    <form id="excel_form" name="excel_form" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload Excel</h6> 
                            <input type="file" id="excelfile" name="excelfile" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel('excelfile')" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample_container.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div> 
                
                <div class="page-content-wrapper" style="display:none;">
                    <form id="lotnumber_file_form" name="lotnumber_file_form" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload Excel</h6> 
                            <input type="file" id="lotnumber_file" name="lotnumber_file" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel('lotnumber_file')" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload Lot Number file
                            </a>                         
                        </div> 
                    </form>
                </div>
                
                </br> 
                <div class="page-content-wrapper1">
                    <div id="serchfilter" class="filter-sec-wrapper">
                        <div class="form-group filter-search dataTables_filter searchFilterClass">
                            <input type="text" id="sSearch_0" name="sSearch_0" class="searchInput filter-search-input" placeholder="Container Number">
                        </div>
                        <div class="form-group clear-search-filter">
                            <button class="btn-primary-mro" onclick="clearSearchFilters();">Clear Search</button>
                        </div>
                    </div>
                </div>
                <div class="bp-list-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Container Number</th>
                                    <th>Master Contract</th>
<!--                                    <th>Status</th>-->
                                    <th class="table-action-cls" style="width:84px !important;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

 function uploadExcel(id)
 {
    if( document.getElementById(id).files.length == 0 ){ 
        showInsertUpdateMessage("Please Choose a xlsx file.",false);   
    }
    else
    {
        if(id=='excelfile'){
            var data = new FormData($('#excel_form')[0]);
            var url = '<?php echo base_url('container/importContainer'); ?>';
            $(".upload-button").hide();
        }else if(id=='lotnumber_file'){
            var data = new FormData($('#lotnumber_file_form')[0]);
            var url = '<?php echo base_url('container/importLSFRILotNumber'); ?>'; 
        }else{ 
            var data = new FormData($('#sku_excel_form')[0]);
            var url = '<?php echo base_url('container/importContainerSKU'); ?>';
            $(".upload-button-sku").hide();
        } 
        $("#docuement_loader").show();
        $.ajax({
            url: url,
            type: 'POST',
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            data: data, 
            success: function(data)
            {
                $("#docuement_loader").hide();
                var html="";
                var response = JSON.parse(data); 
                
                if (response.success) {  
                    showInsertUpdateMessage(response.msg,response);
                    showInsertUpdateMessage("Total entry done: "+response.imported,response); 
                    if(response.notimported > 0){
                        showInsertUpdateMessage("Total entry failed: "+response.notimported,false);   
                    }
                    if(id=='excelfile'){                         
                        $('#excel_form')[0].reset();
                    }else{
                        $('#sku_excel_form')[0].reset();
                    } 
                    setTimeout(function () {  
                        location.reload();
                    }, 3000);
                }else{
                    showInsertUpdateMessage(response.msg,response,false);
                } 
                if(id=='excelfile'){  
                    $(".upload-button").show();
                }else{
                    $(".upload-button-sku").show();
                }
            }
        });
    }
}

$("#import").click(function(){
   $("#efrom").toggle();
   $("#skuefrom").hide();
});

$("#skuimport").click(function(){
   $("#skuefrom").toggle();
   $("#efrom").hide();
});

</script>