<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Container extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Containermodel', 'containermodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("ContainersList")){
            redirect('dashboard');
        }
    }

    function index() { 
        //code for update last status id in container table
//        $containerIds = $this->common->getData('tbl_container','container_id,created_by,created_on',"1=1"); 
//        if (!empty($containerIds)) { 
//            foreach ($containerIds as $value) {
//                $containerStatusData =  $this->containermodel->getContainerStatus($value['container_id']); 
//                if(!empty($containerStatusData)){ 
//                    $data = array();
//                    $data['last_container_status_id'] = $containerStatusData[0]['container_status_id']; 
//                    $data['last_status_by'] = $containerStatusData[0]['created_by'];
//                    $data['last_status_date'] = date('Y-m-d', strtotime($containerStatusData[0]['created_on']));
//                    $result = $this->common->updateData("tbl_container", $data, array("container_id" => $value['container_id'])); 
//                }else{ 
//                    $data = array();
//                    $data['last_container_status_id'] = 1; 
//                    $data['last_status_by'] = $value['created_by'];
//                    $data['last_status_date'] = date('Y-m-d', strtotime($value['created_on']));
//                    $result = $this->common->updateData("tbl_container", $data, array("container_id" => $value['container_id'])); 
//                }
//            } 
//        } 
 
        
// //code for update DO - Actual date of arrival in container Revised ETA
//        $qry = "SELECT cfds.uploaded_document_number,dluf.container_id,cfs.custom_field_title,cfds.custom_field_structure_value FROM `tbl_custom_field_data_submission` as cfds
//        INNER JOIN tbl_document_ls_uploaded_files as dluf ON dluf.uploaded_document_number = cfds.uploaded_document_number
//        INNER JOIN tbl_custom_field_structure as cfs ON cfds.custom_field_structure_id = cfs.custom_field_structure_id
//        WHERE cfds.custom_field_structure_id = 92 AND cfds.custom_field_structure_value <> ''
//        GROUP BY dluf.container_id  
//        ORDER BY `dluf`.`container_id` ASC "; 
//        
//        $query = $this->db->query($qry);
//        $queryResult = $query->result_array();
//        $i=0;
//        foreach ($queryResult as $value) {
//            $i++;
//            $data = array();
//            $data['revised_eta'] = $value['custom_field_structure_value'];  
//            $result = $this->common->updateData("tbl_container", $data, array("container_id" => $value['container_id']));
//        }
//        
//        echo $i;die;
         
        
        $cityData = array(); 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('container/index', $cityData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        //code for add edit form
        $edit_datas = array();
        $edit_datas['container_details'] = array();
        $edit_datas['contractDetails'] = array();
        $edit_datas['chaDetails'] = array();
        $edit_datas['freightForwarder'] = array();
        $edit_datas['skudetails'] = array();
        $container_id = '';
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $container_id = $url_prams['id'];
            $result = $this->common->getData("tbl_container", "*", array("container_id" => $container_id));
            if (!empty($result)) {
                $edit_datas['container_details'] = $result[0];
                
                //cha and Forwarder from master contract
                $condition = "status = 'Active' AND order_id = ".$result[0]['order_id']." ";  
                $orderData = $this->common->getData("tbl_order", "broker_id,freight_forwarder_id",$condition);
                if (!empty($orderData)) { 
                    //CHA data
                    if(!empty($orderData[0]['broker_id'])){
                        $condition = "status = 'Active' AND business_partner_id IN (".$orderData[0]['broker_id'].") ";        
                        $chaDetails = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias", $condition,'alias','asc');
                        if (!empty($chaDetails)) {
                            $edit_datas['chaDetails'] = $chaDetails;
                        }
                    }

                    //Freight Forwarder data 
                    if(!empty($orderData[0]['freight_forwarder_id'])){
                        $condition = "status = 'Active' AND business_partner_id IN (".$orderData[0]['freight_forwarder_id'].") ";  
                        $freightForwarder = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias",$condition,'alias','asc');
                        if (!empty($freightForwarder)) {
                            $edit_datas['freightForwarder'] = $freightForwarder;
                        } 
                    } 
                }  
            }
            //container sku details
            $skuData = $this->containermodel->getContainerSkuDetails($container_id);
            if (!empty($skuData)) {
                $edit_datas['skudetails'] = $skuData; 
            }
        }
        
        $main_table = array("tbl_order as o", array("o.order_id,o.contract_number")); 
        $condition = "1=1  AND o.status = 'Active' ";        
        $join_tables = array(
            array("","tbl_bp_order as cbpo","cbpo.bp_order_id = o.customer_contract_id", array()),
            array("","tbl_bp_order as sbpo","sbpo.bp_order_id = o.supplier_contract_id", array()),
            array("","tbl_business_partner as cbp","cbp.business_partner_id = cbpo.business_partner_id", array('cbp.alias as cbp_name')),
            array("","tbl_business_partner as sbp","sbp.business_partner_id = sbpo.business_partner_id", array('sbp.alias as sbp_name')) 
        );         
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
        $contractDetails = $this->common->MySqlFetchRow($rs, "array");  
        
        //$contractDetails = $this->common->getData("tbl_order", "order_id,contract_number", array("status" => "Active"));
        if (!empty($contractDetails)) {
            $edit_datas['contractDetails'] = $contractDetails;
        } 
        
        
        //Liner data 
        $liner = $this->common->getData("tbl_liner", "liner_id,liner_name", array("status" => 'Active'),'liner_name','asc');
        if (!empty($liner)) {
            $edit_datas['liner'] = $liner;
        }
        
        //POL data 
        $pol = $this->common->getData("tbl_pol", "pol_id,pol_name", array("status" => 'Active'),'pol_name','asc');
        if (!empty($pol)) {
            $edit_datas['pol'] = $pol;
        }
        
        //POD data 
        $pod = $this->common->getData("tbl_pod", "pod_id,pod_name", array("status" => 'Active'),'pod_name','asc');
        if (!empty($pod)) {
            $edit_datas['pod'] = $pod;
        }
        
        //Flag data 
        $flag = $this->common->getData("tbl_flag", "flag_id,flag_name", array("status" => 'Active'),'flag_name','asc');
        if (!empty($flag)) {
            $edit_datas['flag'] = $flag;
        }
        
        $edit_datas['view'] = isset($_GET['view'])?$_GET['view']:0; 
         
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('container/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        //code for get city record at list
        $get_result = $this->containermodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->container_number));
                array_push($temp, $get_result['query_result'][$i]->contract_number);
                //array_push($temp, $get_result['query_result'][$i]->status);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("ContainersAddEdit")){ 
                    $actionCol = '<a href="container/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->container_id), '+/', '-_'), '=') . '" title="Edit" class="text-center"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                if($this->privilegeduser->hasPrivilege("ContainersDetailsView")){  
                    $actionCol .= '<a href="'.base_url().'container_details?text='.rtrim(strtr(base64_encode("id=".$get_result['query_result'][$i]->container_id), '+/', '-_'), '=').'" title="View" class="text-center" style="float:right;"><img src="' . base_url() . 'assets/images/view-btn-icon.svg" alt="View" ></a>';
                } 
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }
    
    function duplicate() {
        if(!$this->privilegeduser->hasPrivilege("ContainersAddEdit")){
            redirect('dashboard');
        }
        //code for add edit form
        $edit_datas = array();
        $edit_datas['container_details'] = array();
        $edit_datas['contractDetails'] = array();
        $edit_datas['chaDetails'] = array();
        $edit_datas['freightForwarder'] = array();
        $edit_datas['skudetails'] = array();
        $container_id = '';
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $container_id = $url_prams['id'];
            $result = $this->common->getData("tbl_container", "*", array("container_id" => $container_id));
            if (!empty($result)) {
                $edit_datas['container_details'] = $result[0]; 
                
                //cha and Forwarder from master contract
                $condition = "status = 'Active' AND order_id = ".$result[0]['order_id']." ";  
                $orderData = $this->common->getData("tbl_order", "broker_id,freight_forwarder_id",$condition);
                if (!empty($orderData)) { 
                    //CHA data
                    if(!empty($orderData[0]['broker_id'])){
                        $condition = "status = 'Active' AND business_partner_id IN (".$orderData[0]['broker_id'].") ";        
                        $chaDetails = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias", $condition,'alias','asc');
                        if (!empty($chaDetails)) {
                            $edit_datas['chaDetails'] = $chaDetails;
                        }
                    }

                    //Freight Forwarder data 
                    if(!empty($orderData[0]['freight_forwarder_id'])){
                        $condition = "status = 'Active' AND business_partner_id IN (".$orderData[0]['freight_forwarder_id'].") ";  
                        $freightForwarder = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias",$condition,'alias','asc');
                        if (!empty($freightForwarder)) {
                            $edit_datas['freightForwarder'] = $freightForwarder;
                        } 
                    } 
                }  
                
            }
            //container sku details
            $skuData = $this->containermodel->getContainerSkuDetails($container_id);
            if (!empty($skuData)) {
                $edit_datas['skudetails'] = $skuData;
            }
        }
        
        $main_table = array("tbl_order as o", array("o.order_id,o.contract_number")); 
        $condition = "1=1  AND o.status = 'Active' ";        
        $join_tables = array(
            array("","tbl_bp_order as cbpo","cbpo.bp_order_id = o.customer_contract_id", array()),
            array("","tbl_bp_order as sbpo","sbpo.bp_order_id = o.supplier_contract_id", array()),
            array("","tbl_business_partner as cbp","cbp.business_partner_id = cbpo.business_partner_id", array('cbp.alias as cbp_name')),
            array("","tbl_business_partner as sbp","sbp.business_partner_id = sbpo.business_partner_id", array('sbp.alias as sbp_name')) 
        );         
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
        $contractDetails = $this->common->MySqlFetchRow($rs, "array");  
        
        //$contractDetails = $this->common->getData("tbl_order", "order_id,contract_number", array("status" => "Active"));
        if (!empty($contractDetails)) {
            $edit_datas['contractDetails'] = $contractDetails;
        } 
        
        //Liner data 
        $liner = $this->common->getData("tbl_liner", "liner_id,liner_name", array("status" => 'Active'),'liner_name','asc');
        if (!empty($liner)) {
            $edit_datas['liner'] = $liner;
        }
        
        //POL data 
        $pol = $this->common->getData("tbl_pol", "pol_id,pol_name", array("status" => 'Active'),'pol_name','asc');
        if (!empty($pol)) {
            $edit_datas['pol'] = $pol;
        }
        
        //POD data 
        $pod = $this->common->getData("tbl_pod", "pod_id,pod_name", array("status" => 'Active'),'pod_name','asc');
        if (!empty($pod)) {
            $edit_datas['pod'] = $pod;
        }
        
        //Flag data 
        $flag = $this->common->getData("tbl_flag", "flag_id,flag_name", array("status" => 'Active'),'flag_name','asc');
        if (!empty($flag)) {
            $edit_datas['flag'] = $flag;
        }
        
        $edit_datas['view'] = isset($_GET['view'])?$_GET['view']:0; 
         
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('container/duplicate', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function submitForm() {         
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "container_number = '" . $this->input->post('container_number') . "' ";
            if (!empty($this->input->post("container_id"))) {
                $condition .= " AND container_id <> " . $this->input->post("container_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['tbl_container'])) {
                echo json_encode(array('success' => false, 'msg' => 'Container Number already exist...'));
                exit;
            }

            $data = array();
            $data['container_number'] = $this->input->post('container_number');
            $data['order_id'] = $this->input->post('order_id');
            $data['broker_id'] = $this->input->post('broker_id'); 
            $data['freight_forwarder_id'] = $this->input->post('freight_forwarder_id'); 
            $data['eta'] = !empty($this->input->post('eta')) ? date("Y-m-d", strtotime($this->input->post('eta'))) : NULL;
            $data['revised_eta'] = !empty($this->input->post('revised_eta')) ? date("Y-m-d", strtotime($this->input->post('revised_eta'))) : NULL;
            $data['etd'] = !empty($this->input->post('etd')) ? date("Y-m-d", strtotime($this->input->post('etd'))) : NULL;
            $data['revised_etd'] = !empty($this->input->post('revised_etd')) ? date("Y-m-d", strtotime($this->input->post('revised_etd'))) : NULL; 
            $data['ett'] = $this->input->post('ett'); 
            $data['revised_ett'] = $this->input->post('revised_ett'); 
            $data['liner_name'] = $this->input->post('liner_name'); 
            $data['liner_tracker_url'] = $this->input->post('liner_tracker_url'); 
            $data['pol'] = $this->input->post('pol'); 
            $data['pod'] = $this->input->post('pod'); 
            $data['vessel_name'] = $this->input->post('vessel_name'); 
            $data['vessel_tracker_url'] = $this->input->post('vessel_tracker_url');
            $data['flag'] = $this->input->post('flag');
            $data['lot_number'] = $this->input->post('lot_number');
            $data['terminal'] = $this->input->post('terminal');
            $data['email_sent_on'] = !empty($this->input->post('email_sent_on')) ? date("Y-m-d", strtotime($this->input->post('email_sent_on'))) : NULL;
            $data['email_subject'] = $this->input->post('email_subject');
            $data['shipping_container'] = $this->input->post('shipping_container');
            $data['notes'] = $this->input->post('notes');
            $data['sgs_seal'] = $this->input->post('sgs_seal');
            $data['ff_seal'] = $this->input->post('ff_seal');
            $data['status'] = 'Active'; //$this->input->post('status');
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            if (!empty($this->input->post("container_id"))) {
                //update container
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_container", $data, array("container_id" => $this->input->post("container_id")));
                
                //delete and insert container sku
                $data1['container_id'] = $this->input->post("container_id");
                $resultdel = $this->common->deleteRecord('tbl_container_sku', array("container_id" => $this->input->post("container_id")));
                if ($resultdel) {
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $result1 = $this->common->insertData('tbl_container_sku', $data1, '1');
                        }
                    } 
                }
                
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Container updated successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while updating data.'));
                    exit;
                }
            } else {
                //insert container
                $data['container_status_id'] = 1;
                $data['last_container_status_id'] = 1;                
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $data['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['last_status_date'] = date("Y-m-d");
                $result = $this->common->insertData("tbl_container", $data, "1");
                if ($result) {
                    //insert container sku
                    $data1['container_id'] = $result;
                    if (!empty($this->input->post('sku_id'))) {
                        foreach ($this->input->post('sku_id') as $key => $value) {
                            $data1['sku_id'] = $value;
                            $data1['quantity'] = $this->input->post('sku_qty')[$key];
                            $result1 = $this->common->insertData('tbl_container_sku', $data1, '1');
                        }
                    }
                    
                    $data2['container_id'] = $result;
                    $data2['container_status_id'] = 1;
                    $data2['current_session_id'] = 'container';
                    $data2['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data2['created_on'] = date("Y-m-d H:i:s");
                    $result2 = $this->common->insertData("tbl_container_status_log", $data2, "1"); 
                    
                    echo json_encode(array('success' => true, 'msg' => 'New Container created successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }
    
    function importContainer() {         
        if(!$this->privilegeduser->hasPrivilege("ContainersImport")){
            redirect('dashboard');
        }
        require_once('application/libraries/SimpleXLSX.php');
       
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["excelfile"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["excelfile"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die; 
                 $xlsx = new SimpleXLSX($_FILES['excelfile']['tmp_name']);
                
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) { 
                        if (trim($data[0]) != "Container Number") {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        if (trim($data[1]) != "Master Contract" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if (trim($data[2]) != "CHA" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if (trim($data[3]) != "Freight Forwarder" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        
                        if (trim($data[4]) != "ETA" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if (trim($data[5]) != "Revised ETA" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if (trim($data[6]) != "ETD" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if (trim($data[7]) != "Revised ETD" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if (trim($data[8]) != "ETT" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if (trim($data[9]) != "Revised ETT" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1])){
                            continue;
                        } 
                        
                        if (trim($data[0]) == "") {
                            $error .= "Container Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[1]) == "") {
                            $error .= "Master Contract cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  
                        
                        if (trim($data[22]) != "Active" && trim($data[22]) != "In-active") {
                            $error .= "Status cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1])){
                            continue;
                        }  
                        
                        $order_number = str_replace("'", "&#39;", trim($data[1]));  
                        //get bussiness partner data
                        $condition_contract = "contract_number = '" . $order_number . "' AND status ='Active' "; 
                        $chk_contract_sql = $this->common->Fetch("tbl_order", "order_id,broker_id,freight_forwarder_id", $condition_contract);
                        $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array");
                        //echo $this->db->last_query(); 
                        $order_id = 0;
                        if (empty($rs_contract[0]['order_id'])) {
                            continue;
                        }else{       
                            $container_number = str_replace("'", "&#39;", trim($data[0]));
                            $order_id = $rs_contract[0]['order_id'];  
                            $broker_name = str_replace("'", "&#39;", trim($data[2])); 
                            $ff_name = str_replace("'", "&#39;", trim($data[3]));
                            $eta = '';
                            if(!empty(str_replace("'", "&#39;", trim($data[4])))){
                                $etaDateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[4]))); 
                                $eta = date('Y-m-d',$etaDateVal); 
                            }
                            $revisedeta = '';
                            if(!empty(str_replace("'", "&#39;", trim($data[5])))){
                                $revisedetaDateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[5]))); 
                                $revisedeta = date('Y-m-d',$etaDateVal); 
                            } 
                            
                            $etd = '';
                            if(!empty(str_replace("'", "&#39;", trim($data[6])))){
                                $etdDateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[6]))); 
                                $etd = date('Y-m-d',$etdDateVal);     
                            } 
                            
                            $revisedetd = '';
                            if(!empty(str_replace("'", "&#39;", trim($data[7])))){
                                $revisedetdDateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[7]))); 
                                $revisedetd = date('Y-m-d',$revisedetdDateVal);
                            }                     
                             
                            $ett = str_replace("'", "&#39;", trim($data[8]));
                            $revisedett = str_replace("'", "&#39;", trim($data[9]));
                            $LinerName = str_replace("'", "&#39;", trim($data[10]));
                            $LinerTrackerURL = str_replace("'", "&#39;", trim($data[11]));
                            $POL = str_replace("'", "&#39;", trim($data[12]));
                            $POD = str_replace("'", "&#39;", trim($data[13]));
                            $VesselName = str_replace("'", "&#39;", trim($data[14]));
                            $VesselTrackerURL = str_replace("'", "&#39;", trim($data[15]));
                            $Flag = str_replace("'", "&#39;", trim($data[16]));
                            $ShippingContainer = str_replace("'", "&#39;", trim($data[17]));
                            $SGSSeal = str_replace("'", "&#39;", trim($data[18]));
                            $FFSeal = str_replace("'", "&#39;", trim($data[19]));  
                            
                            $EmailSenton = '';
                            if(!empty(str_replace("'", "&#39;", trim($data[20])))){
                                $EmailSentonDateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[20]))); 
                                $EmailSenton = date('Y-m-d',$EmailSentonDateVal); 
                            }  
                            
                            $EmailSubject = str_replace("'", "&#39;", trim($data[21])); 
                            $Status = str_replace("'", "&#39;", trim($data[22]));
                            $lotNumber = str_replace("'", "&#39;", trim($data[23]));
                            $terminal = str_replace("'", "&#39;", trim($data[24]));
                            
                            $broker_id = 0;
                            if(!empty($broker_name)){
                                // get broker id
                                $condition_broker = "alias = '" . $broker_name . "' AND business_category ='Customs Broker' "; 
                                $chk_broker_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $condition_broker);
                                $rs_broker = $this->common->MySqlFetchRow($chk_broker_sql, "array");                                
                                if (!empty($rs_broker[0]['business_partner_id'])) { 
                                    if(!empty($rs_contract[0]['broker_id']) && $rs_broker[0]['business_partner_id'] > 0){ 
                                        $brokerIdsArr = explode(",", $rs_contract[0]['broker_id']); 
                                        if (!empty($brokerIdsArr)) {
                                            if(in_array($rs_broker[0]['business_partner_id'], $brokerIdsArr)){
                                                $broker_id = $rs_broker[0]['business_partner_id'];
                                            } 
                                        } 
                                    }  
                                } 
                            } 
                            
                            $freight_forwarder_id = 0;
                            if(!empty($ff_name)){
                                //get FF id
                                $condition_ff = "alias = '" . $ff_name . "' AND business_category ='Freight Forwarder' "; 
                                $chk_ff_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $condition_ff);
                                $rs_ff = $this->common->MySqlFetchRow($chk_ff_sql, "array"); 
                                if (!empty($rs_ff[0]['business_partner_id'])) { 
                                    if(!empty($rs_contract[0]['freight_forwarder_id']) && $rs_ff[0]['business_partner_id'] > 0){ 
                                        $freightForwarderIdsArr = explode(",", $rs_contract[0]['freight_forwarder_id']); 
                                        if (!empty($freightForwarderIdsArr)) {
                                            if(in_array($rs_ff[0]['business_partner_id'], $freightForwarderIdsArr)){
                                                $freight_forwarder_id = $rs_ff[0]['business_partner_id'];
                                            } 
                                        } 
                                    }  
                                }  
                            }  
                            
                            //get liner id
                            $condition_liner = "liner_name = '" . $LinerName . "' "; 
                            $chk_liner_sql = $this->common->Fetch("tbl_liner", "liner_id", $condition_liner);
                            $rs_liner = $this->common->MySqlFetchRow($chk_liner_sql, "array");                        
                            $liner_name = 0;
                            if (!empty($rs_liner[0]['liner_id'])) {
                                $liner_name = $rs_liner[0]['liner_id'];
                            } 
                            
                            //get pol id
                            $condition_pol = "pol_name = '" . $POL . "' "; 
                            $chk_pol_sql = $this->common->Fetch("tbl_pol", "pol_id", $condition_pol);
                            $rs_pol = $this->common->MySqlFetchRow($chk_pol_sql, "array");                        
                            $pol = 0;
                            if (!empty($rs_pol[0]['pol_id'])) {
                                $pol = $rs_pol[0]['pol_id'];
                            } 

                            //get pod id
                            $condition_pod = "pod_name = '" . $POD . "' "; 
                            $chk_pod_sql = $this->common->Fetch("tbl_pod", "pod_id", $condition_pod);
                            $rs_pod = $this->common->MySqlFetchRow($chk_pod_sql, "array");                        
                            $pod = 0;
                            if (!empty($rs_pod[0]['pod_id'])) {
                                $pod = $rs_pod[0]['pod_id'];
                            } 
                            
                            //get flag
                            $condition_flag = "flag_name = '" . $Flag . "' "; 
                            $chk_flag_sql = $this->common->Fetch("tbl_flag", "flag_id", $condition_flag);
                            $rs_flag = $this->common->MySqlFetchRow($chk_flag_sql, "array");                        
                            $flag = 0;
                            if (!empty($rs_flag[0]['flag_id'])) {
                                $flag = $rs_flag[0]['flag_id'];
                            }  

                            $data = array();
                            $data['container_number'] = $container_number;
                            $data['order_id'] = $order_id;
                            $data['broker_id'] = $broker_id; 
                            $data['freight_forwarder_id'] = $freight_forwarder_id; 
                            $data['eta'] = !empty($eta) ? $eta : NULL;
                            $data['revised_eta'] = !empty($revisedeta) ? $revisedeta : NULL;
                            $data['etd'] = !empty($etd) ? $etd : NULL;
                            $data['revised_etd'] = !empty($revisedetd) ? $revisedetd : NULL;
                            $data['ett'] = $ett; 
                            $data['revised_ett'] = $revisedett; 
                            $data['liner_name'] = $liner_name; 
                            $data['liner_tracker_url'] = $LinerTrackerURL; 
                            $data['pol'] = $pol; 
                            $data['pod'] = $pod; 
                            $data['vessel_name'] = $VesselName; 
                            $data['vessel_tracker_url'] = $VesselTrackerURL;
                            $data['flag'] = $flag;
                            $data['email_sent_on'] = !empty($EmailSenton) ? $EmailSenton : NULL;
                            $data['email_subject'] = $EmailSubject;
                            $data['shipping_container'] = $ShippingContainer;
                            $data['sgs_seal'] = $SGSSeal;
                            $data['ff_seal'] = $FFSeal;
                            $data['status'] = $Status;
                            $data['lot_number'] = $lotNumber;
                            $data['terminal'] = $terminal;                            
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s"); 
                            
                            //echo '<pre>'; print_r($data); echo '<br>';
                            $condition_container = "container_number = '" . $container_number . "' AND status='" . $Status . "' "; 
                            $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition_container);
                            $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array"); 

                             //update-insert bussiness partner contract data 
                            if (!empty($rs_container[0]['container_id'])) {                            
                                $result = $this->common->updateData("tbl_container", $data, array("container_id" => $rs_container[0]['container_id']));  
                                if ($result) {  
                                    $imported++;
                                } else {
                                    $notimported++;
                                }
                            }else{
                                 //insert data 
                                $data['container_status_id'] = 1;
                                $data['last_container_status_id'] = 1;
                                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['created_on'] = date("Y-m-d H:i:s");
                                $data['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['last_status_date'] = date("Y-m-d");
                                $result = $this->common->insertData("tbl_container", $data, "1");
                                
                                $data2 = array();
                                $data2['container_id'] = $result;
                                $data2['container_status_id'] = 1;
                                $data2['current_session_id'] = 'container';
                                $data2['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data2['created_on'] = date("Y-m-d H:i:s");
                                $result2 = $this->common->insertData("tbl_container_status_log", $data2, "1"); 
                                
                                if ($result) {
                                    $imported++;
                                } else {
                                    $notimported++;
                                } 
                            }  
                        } 
                    } 
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    } 
    
    function importContainerSKU() {  
        if(!$this->privilegeduser->hasPrivilege("ContainersImport")){
            redirect('dashboard');
        }
        require_once('application/libraries/SimpleXLSX.php');
       
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["skuexcelfile"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["skuexcelfile"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die;   
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) { 
                        if (trim($data[0]) != "Container Number") {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }
                        if (trim($data[1]) != "BM SKU Number" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        if (trim($data[2]) != "Quantity" && $valid==0) {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        }  
                        
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1])){
                            continue;
                        } 
                        
                        if (trim($data[0]) == "") {
                            $error .= "Container Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        } 
                        
                        if (trim($data[1]) == "") {
                            $error .= "BM SKU Number cannot be empty in row : " . ($key + 1) . "<br>";
                            $valid++;
                        }  

                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $container_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_number = str_replace("'", "&#39;", trim($data[1]));
                        $quantity = 0;
                        if(!empty(str_replace("'", "&#39;", trim($data[2])))){
                            $quantity = str_replace("'", "&#39;", trim($data[2]));  
                        } 
                        
                        //get order id
                        $condition_container = "container_number = '" . $container_number . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,order_id", $condition_container);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                        //echo $this->db->last_query(); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                        //condition for order and sku id blank
                        if (empty($rs_container[0]['container_id']) || empty($rs_sku[0]['sku_id'])) {
                            continue;
                        }else{                            
                            $container_id = $rs_container[0]['container_id'];
                            $order_id = $rs_container[0]['order_id'];
                            $sku_id = $rs_sku[0]['sku_id'];
                            
                            $condition_contract = "order_id = ".$order_id." AND sku_id = ".$sku_id." "; 
                            $chk_contract_sql = $this->common->Fetch("tbl_order_sku", "order_id", $condition_contract);
                            $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array");  
                            
                            if (!empty($rs_contract[0]['order_id'])) {
                                $data = array();
                                $data['container_id'] = $container_id;
                                $data['sku_id'] = $sku_id;  
                                
                                $this->common->deleteRecord('tbl_container_sku', array("container_id" => $container_id,"sku_id" => $sku_id)); 
                                
                                $qty = $this->getContainerSKUQty($sku_id,$order_id,(int)$quantity,'Duplicate');                                
                                $data['quantity'] = (int)$qty;

                                $result = $this->common->insertData("tbl_container_sku", $data, "1"); 

                                if ($result) {
                                    $imported++;
                                } else {
                                    $notimported++;
                                }  
                            }   
                        }  
                    }
                     
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    }  
    
    function importContainerLotNumber() {  
         
        require_once('application/libraries/SimpleXLSX.php');
       
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["lotnumber_file"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["lotnumber_file"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die;   
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) { 
                        if (trim($data[0]) != "Container Number") {
                            $error .= "Import sheet not valid.";
                            $valid++;
                        } 
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1])){
                            continue;
                        } 
                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) && empty($data[1])){
                            continue;
                        }  
                        
                        $container_number = str_replace("'", "&#39;", trim($data[0])); 
                        $lot_number = str_replace("'", "&#39;", trim($data[1]));
                        $condition_container = "container_number = '" . $container_number . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition_container);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                         
                        //condition for order and sku id blank
                        if (empty($rs_container[0]['container_id'])) {
                            continue;
                        }else{                            
                            $container_id = $rs_container[0]['container_id'];
                            $data = array(); 
                            $data['lot_number'] = $lot_number;  
                            $result = $this->common->updateData("tbl_container", $data, array("container_id" => $container_id));  
                            if ($result) {  
                                $imported++;
                            } else {
                                $notimported++;
                            } 
                        }  
                    }
                     
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    }  
    
    function importLSFRILotNumber() {  
         
        require_once('application/libraries/SimpleXLSX.php');
       
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES["lotnumber_file"]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES["lotnumber_file"]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows()); die;   
                foreach ($handle->rows() as $key => $data) {
                    //check for header column value
                    if ($key == 0) {  
                        if ($valid != 0) {
                            $statusData['success'] = false;
                            $statusData['msg'] = $error; 
                            print_r(json_encode($statusData));
                            exit;
                        }   
                    }else{
                        //check for blank value of all column value of row.
                        if(empty($data[0])){
                            continue;
                        } 
                    }  
                }
                if ($valid != 0) {
                    $statusData['success'] = false;
                    $statusData['msg'] = $error;
                    print_r(json_encode($statusData));
                    exit;
                } else {
                    foreach ($handle->rows() as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //code of insert lot number for LS and FRI
//                        $documentNumber = str_replace("'", "&#39;", trim($data[0]));
//                        $lotNumber = str_replace("'", "&#39;", trim($data[1]));
//                        $custom_field_structure_id = 17; //17 for fri, 10 for ls
//                        $document_type_id = 3; //3 for fri, 2 for ls
//                        
//                        $condition = " custom_field_structure_id = " . $custom_field_structure_id . " and document_type_id = " . $document_type_id . " and uploaded_document_number = '" . trim($documentNumber) . "' ";                        
//                        $deleted = $this->common->deleteRecord('tbl_custom_field_data_submission', $condition);  
//                        $dataUpdate = array();
//                        $dataUpdate['custom_field_structure_id'] = $custom_field_structure_id;
//                        $dataUpdate['custom_field_structure_value'] = $lotNumber;
//                        $dataUpdate['uploaded_document_number'] = trim($documentNumber);
//                        $dataUpdate['document_type_id'] = $document_type_id;
//                        $dataUpdate['sub_document_type_id'] = 0;
//                        $dataUpdate['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
//                        $dataUpdate['updated_on'] = date("Y-m-d H:i:s");
//                        $dataUpdate['created_by'] = $_SESSION['mro_session'][0]['user_id'];
//                        $dataUpdate['created_on'] = date("Y-m-d H:i:s");
//                        $dataUpdate['current_session_id'] = '';
//                        $result = $this->common->insertData("tbl_custom_field_data_submission", $dataUpdate, "1");
                        
                        //code of update 7501 two fields
//                        $documentNumber = str_replace("'", "&#39;", trim($data[0]));
//                        $mfees = str_replace("'", "&#39;", trim($data[1]));
//                        $hfee = str_replace("'", "&#39;", trim($data[2])); 
//                        $document_type_id = 7; // for 7501
//                      
//                        $condition = " custom_field_structure_id IN (84,85) and document_type_id = " . $document_type_id . " and uploaded_document_number = '" . trim($documentNumber) . "' ";                        
//                        $chk_container_sql = $this->common->Fetch("tbl_custom_field_data_submission", "custom_field_structure_id", $condition);
//                        $dataResult = $this->common->MySqlFetchRow($chk_container_sql, "array");
//                        if(!empty($dataResult)) {
//                            foreach ($dataResult as $value) {
//                                if($value['custom_field_structure_id']==84){
//                                    $data = array(); 
//                                    $data['custom_field_structure_value'] = $mfees;   
//                                    $condition1 = " custom_field_structure_id =84 and document_type_id = " . $document_type_id . " and uploaded_document_number = '" . trim($documentNumber) . "' ";                        
//                                    $result = $this->common->updateData("tbl_custom_field_data_submission", $data, $condition1); 
//                                }else{
//                                    $data = array(); 
//                                    $data['custom_field_structure_value'] = $hfee;   
//                                    $condition1 = " custom_field_structure_id =85 and document_type_id = " . $document_type_id . " and uploaded_document_number = '" . trim($documentNumber) . "' ";                        
//                                    $result = $this->common->updateData("tbl_custom_field_data_submission", $data, $condition1); 
//                                } 
//                            } 
//                            if ($result) {  
//                                $imported++;
//                            } else {
//                                $notimported++;
//                            }  
//                        }
                        
                        
                        $container_number = str_replace("'", "&#39;", trim($data[0])); 
                        $pod_terminal = str_replace("'", "&#39;", trim($data[1]));
                        $condition_container = "container_number = '" . $container_number . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition_container);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                          
                        if (empty($rs_container[0]['container_id'])) {
                            continue;
                        }else{                            
                            $container_id = $rs_container[0]['container_id'];
                            $data = array(); 
                            $data['terminal'] = $pod_terminal;  
                            $result = $this->common->updateData("tbl_container", $data, array("container_id" => $container_id));  
                            if ($result) {  
                                $imported++;
                            } else {
                                $notimported++;
                            } 
                        }  
                         
                    }
                     
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit;
                }
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    } 
    
    function getContractNumber(){
        $containerNumber = '600001';
        $containerNumberData = $this->containermodel->getContainerNumber();
        if(!empty($containerNumberData)){
            $containerNumber = $containerNumberData[0]['container_number'];
            $containerNumber += 1;
        }
        echo json_encode(array('containerNumber' => $containerNumber)); 
        exit;
    }

    function getContractDetails() { 
        $contract_id = $_GET['contract_id'];
        $is_edit = $_GET['is_edit'];
        $contactDetail = array();
        $broker_id = 0;
        $freight_forwarder_id = 0;
        $customerContact = '';
        $customerPartner = '';
        $supplierContact = '';
        $supplierPartner = '';
        $custmerSupplierStr = '';
        $skuStr = '';
        $customerContactId = 0;
        $supplierContactId = 0;
        $contactDetail = $this->common->getData("tbl_order", "customer_contract_id,supplier_contract_id,broker_id,freight_forwarder_id", array("order_id" => (int) $contract_id));
        if(!empty($contactDetail)){
            $broker_id = $contactDetail[0]['broker_id'];
            $freight_forwarder_id = $contactDetail[0]['freight_forwarder_id'];
            $customer_contract_id = $contactDetail[0]['customer_contract_id'];
            $supplier_contract_id = $contactDetail[0]['supplier_contract_id'];
            $customerDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $customer_contract_id));
            if(!empty($customerDetail)){
                $customerContactId = $customerDetail[0]['bp_order_id'];
                $customerContact = $customerDetail[0]['contract_number'];
                $customerPartnerId = $customerDetail[0]['business_partner_id'];
                $customerPartnerDetail = $this->common->getData("tbl_business_partner", "business_name,alias", array("business_partner_id " => (int) $customerPartnerId));
                if(!empty($customerPartnerDetail)){
                    $customerPartner = $customerPartnerDetail[0]['alias'];
                }
            }
            $supplierDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $supplier_contract_id));
            if(!empty($supplierDetail)){
                $supplierContactId = $supplierDetail[0]['bp_order_id'];
                $supplierContact = $supplierDetail[0]['contract_number'];
                $supplierPartnerId = $supplierDetail[0]['business_partner_id'];
                $supplierPartnerDetail = $this->common->getData("tbl_business_partner", "business_name,alias", array("business_partner_id " => (int) $supplierPartnerId));
                if(!empty($supplierPartnerDetail)){
                    $supplierPartner = $supplierPartnerDetail[0]['alias'];
                }
            }
            $url = base_url()."bporder/addEdit?text"; 
            
            $customerContractStr = '<p class="sit-single-value">'.$customerContact.'</p>';
            $supplierContractStr = '<p class="sit-single-value">'.$supplierContact.'</p>';
            if($this->privilegeduser->hasPrivilege("BusinessPartnersOrderView")){
                $customerContractStr = '<p class="sit-single-value"><a href="'.$url.'=' . rtrim(strtr(base64_encode("id=" . $customerContactId), '+/', '-_'), '=') . '&view=1" ><u>'.$customerContact.'</u></a></p>';
                $supplierContractStr = '<p class="sit-single-value"><a href="'.$url.'=' . rtrim(strtr(base64_encode("id=" . $supplierContactId), '+/', '-_'), '=') . '&view=1" ><u>'.$supplierContact.'</u></a></p>';
            }
            
            $custmerSupplierStr = '<div class="sit-row">
                                        <div class="sit-single">
                                            <p class="sit-single-title">Customer Name</p>
                                            <p class="sit-single-value">'.$customerPartner.'</p>
                                        </div>
                                        <div class="sit-single">
                                            <p class="sit-single-title">Customer Order</p>
                                            '.$customerContractStr.'
                                        </div>
                                        <div class="sit-single">
                                            <p class="sit-single-title">Vendor Name</p>
                                            <p class="sit-single-value">'.$supplierPartner.'</p>
                                        </div>
                                        <div class="sit-single">
                                            <p class="sit-single-title">Vendor Order</p>
                                            '.$supplierContractStr.'
                                        </div>
                                    </div>';
                if($is_edit==0){                    
                    $skuDetail = $this->containermodel->getskuData($contract_id);
                    if (!empty($skuDetail)) {
                        foreach ($skuDetail as $key => $value) {
                            
                            $qty = $this->getContainerSKUQty($value["sku_id"],$contract_id);
                            $htmlId = "'"."sku_qty".$key."'";
                            
                            $skuStr .= '<div class="sit-row">
                                <div class="sit-single sku-item-code">
                                    <p class="sit-single-title">Item Code</p>
                                    <p class="sit-single-value">'.$value["sku_number"].'</p>
                                    <input type="hidden" name="sku_id[' . $key . ']" id="sku_id' . $key . '" value="' . $value["sku_id"] . '">
                                </div>
                                <div class="sit-single sku-pd">
                                    <p class="sit-single-title">Product Description</p>
                                    <p class="sit-single-value">' . $value["product_name"] . '</p>
                                </div>
                                <div class="sit-single sku-size">
                                    <p class="sit-single-title">Size</p>
                                    <p class="sit-single-value">' . $value["size_name"] . '</p>
                                </div>
                                <div class="sit-single sku-qty form-group">
                                    <p class="sit-single-title">Quantity<sup>*</sup></p>
                                    <input type="number" required name="sku_qty[' . $key . ']" id="sku_qty' . $key . '" value="' . $qty . '" onkeyup="getContainerSKUQtyEnter('.$htmlId.','.$value["sku_id"].','.$contract_id.');" class="input-form-mro qty" placeholder="Enter quantity">
                                </div> 
                            </div>';
                        }
                    }
                }
                
                 //CHA data
                $broker_str = '<label for="">CHA</label>
                               <select name="broker_id" id="broker_id" class="basic-single select-form-mro" style="width: 100%;"> 
                                <option value="0" disabled selected hidden>Select CHA</option>';  
                if(!empty($broker_id)){
                    $condition = "status = 'Active' AND business_partner_id IN (".$broker_id.") ";        
                    $chaDetails = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias", $condition,'alias','asc');
                    if (!empty($chaDetails)) {
                        foreach ($chaDetails as $value) { 
                            $broker_str .= '<option value="'.$value['business_partner_id'].'">'.$value['alias'].'</option>'; 
                        } 
                    }
                } 
                $broker_str .= '</select>';

                //Freight Forwarder data 
                
                $ff_str = '<label for="">Freight Forwarder</label>
                               <select name="freight_forwarder_id" id="freight_forwarder_id" class="basic-single select-form-mro" style="width: 100%;"> 
                                <option value="0" disabled selected hidden>Select CHA</option>';  
                if(!empty($freight_forwarder_id)){
                    $condition = "status = 'Active' AND business_partner_id IN (".$freight_forwarder_id.") ";        
                    $freightForwarder = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias", $condition,'alias','asc');
                    if (!empty($freightForwarder)) {
                        foreach ($freightForwarder as $value) { 
                            $ff_str .= '<option value="'.$value['business_partner_id'].'">'.$value['alias'].'</option>'; 
                        } 
                    }
                } 
                $ff_str .= '</select>'; 
                
        }
         
        echo json_encode(array('custmerSupplier' => $custmerSupplierStr,'sku' => $skuStr,'broker_id' =>$broker_str ,'freight_forwarder_id' => $ff_str)); 
        exit;
    }
    
    function getETTDays(){
        $revised_eta = $_GET['revised_eta'];
        $revised_etd = $_GET['revised_etd'];
        $days = '';
        if(!empty($revised_eta) && !empty($revised_etd)){
            $days = DaysDiffBetweenTwoDays($revised_eta,$revised_etd);
        }
        echo json_encode(array('days' => abs($days))); 
        exit;
    }
    
    function getContainerSKUQty($sku_id,$contract_id,$quantity=0,$isDuplicate=''){  
        $totalContainerQty = 0;
        $contractQty = 0;        
        $condition = "c.order_id=".$contract_id."  AND csku.sku_id = '".$sku_id."' ";
        $main_table = array("tbl_container_sku as csku", array('csku.quantity'));
        $join_tables = array(
            array("inner","tbl_container as c","c.container_id = csku.container_id", array("c.order_id")),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,"",'');
        $containerSKUQty = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
        //echo $this->db->last_query();exit;
        if(!empty($containerSKUQty)){
            foreach ($containerSKUQty as $value) {
                $totalContainerQty += $value['quantity'];
            } 
        }
        
        $contractSKUQty = $this->common->getData("tbl_order_sku", "quantity", array("order_id " => (int) $contract_id,"sku_id" => $sku_id)); 
        if(!empty($contractSKUQty)){
            foreach ($contractSKUQty as $value) {
                $contractQty += $value['quantity'];
            } 
        }
        
        if($isDuplicate=='Duplicate'){
            $qty = 0;
            $totalQty = $contractQty - $totalContainerQty;
            if($totalQty < 0){
                $qty = 0;
            }else{  
                if($totalQty < $quantity){ 
                    $qty = $totalQty;
                }else{ 
                    $qty = $quantity;
                } 
            }
            //echo $qty;die;
            return $qty; 
        }else{
            $totalQty = $contractQty - $totalContainerQty;
            if($totalQty < 0){
                $totalQty = 0;
            } 
            return $totalQty;   
        }   
    }
    
     function getContainerSKUQtyEnter(){  
        $container_id = isset($_GET['container_id'])?$_GET['container_id']:''; 
        $sku_id = $_GET['sku_id'];  
        $contract_id = $_GET['order_id'];  
        $quantity = $_GET['quantity'];  
       
        $totalContainerQty = 0;
        $contractQty = 0;    
        if(!empty($container_id)){
            $condition = "c.order_id=".$contract_id."  AND csku.sku_id = '".$sku_id."' AND c.container_id <> ".$container_id." ";
        }else{
            $condition = "c.order_id=".$contract_id."  AND csku.sku_id = '".$sku_id."' ";
        }
        
        $main_table = array("tbl_container_sku as csku", array('csku.quantity'));
        $join_tables = array(
            array("inner","tbl_container as c","c.container_id = csku.container_id", array("c.order_id")),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,"",'');
        $containerSKUQty = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
        //echo $this->db->last_query();exit;
        if(!empty($containerSKUQty)){
            foreach ($containerSKUQty as $value) {
                $totalContainerQty += $value['quantity'];
            } 
        }
        
        $contractSKUQty = $this->common->getData("tbl_order_sku", "quantity", array("order_id " => (int) $contract_id,"sku_id" => $sku_id)); 
        if(!empty($contractSKUQty)){
            foreach ($contractSKUQty as $value) {
                $contractQty += $value['quantity'];
            } 
        }
        $remainingQty = 0;
        
        $totalQty = $contractQty - $totalContainerQty; 
        
        if($totalQty < 0){ 
            $remainingQty = 0;
            echo json_encode(array('fail'=>true, 'remainingQty' => $remainingQty,'msg' => 'Remaining Quantity '.$remainingQty));
            exit; 
        }else{ 
            if($quantity > $totalQty){
                $remainingQty = $totalQty;
                echo json_encode(array('fail'=>true,'remainingQty' => $remainingQty,'msg' => 'Remaining Quantity '.$remainingQty));
                exit;  
            }else{
                $remainingQty = $quantity;
                echo json_encode(array('fail'=>false,'remainingQty' => $remainingQty,'msg' => 'success'));
                exit; 
            }
        } 
       
    }
    
    function getRemainingSKUQuantity(){
        $contract_id = $_GET['contract_id'];
        $container_id =  $_GET['container_id'];
        $skuStr = '';  
        $skuDetail = $this->containermodel->getContainerSkuDetails($container_id); 
        //$skuDetail = $this->containermodel->getskuData($contract_id);
        if (!empty($skuDetail)) {
            foreach ($skuDetail as $key => $value) {

                $qty = $this->getContainerSKUQty($value["sku_id"],$contract_id,$value["quantity"],'Duplicate');
                $htmlId = "'"."sku_qty".$key."'";

                $skuStr .= '<div class="sit-row">
                    <div class="sit-single sku-item-code">
                        <p class="sit-single-title">Item Code</p>
                        <p class="sit-single-value">'.$value["sku_number"].'</p>
                        <input type="hidden" name="sku_id[' . $key . ']" id="sku_id' . $key . '" value="' . $value["sku_id"] . '">
                    </div>
                    <div class="sit-single sku-pd">
                        <p class="sit-single-title">Product Description</p>
                        <p class="sit-single-value">' . $value["product_name"] . '</p>
                    </div>
                    <div class="sit-single sku-size">
                        <p class="sit-single-title">Size</p>
                        <p class="sit-single-value">' . $value["size_name"] . '</p>
                    </div>
                    <div class="sit-single sku-qty form-group">
                        <p class="sit-single-title">Quantity<sup>*</sup></p>
                        <input type="number" required name="sku_qty[' . $key . ']" id="sku_qty' . $key . '" value="' . $qty . '" onkeyup="getContainerSKUQtyEnter('.$htmlId.','.$value["sku_id"].','.$contract_id.');" class="input-form-mro qty" placeholder="Enter quantity">
                    </div> 
                </div>';
            }
        }
         
        echo json_encode(array('sku' => $skuStr)); 
        exit;
    }

}

?>
