<?PHP

class Rolesmodel extends CI_Model {

    function getRecords($get) { 
        $table = "tbl_roles";
        $default_sort_column = 'c.role_id';
        $default_sort_order = 'desc';
        $condition = "1=1 ";

        $colArray = array('c.role_name', 'c.status');
        $sortArray = array('c.role_name', 'c.status');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

        for ($i = 0; $i < count($colArray); $i++) {
            if (isset($_GET['Searchkey_' . $i]) && $_GET['Searchkey_' . $i] != "") {
                $condition .= " AND " . $colArray[$i] . " LIKE '%" . $_GET['Searchkey_' . $i] . "%' ";
            }
        }

        $this->db->select('c.*');
        $this->db->from("$table as c");
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page);
        $query = $this->db->get();
        //echo $this->db->last_query();exit; 

        $this->db->select('c.*');
        $this->db->from("$table as c");
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    }
    
    
    	function getPermdata(){
	   $this -> db -> select('i.*');
	   $this -> db -> from('tbl_permissions as i');
	   $this->db->order_by('i.perm_desc', 'asc');
	   $query = $this -> db -> get();
	   
	   if($query -> num_rows() >= 1)
	   {
	     return $query->result();
	   }
	   else
	   {
	     return false;
	   }
	}
	
	function getSelPermdata($ID=0){ 
            $this -> db -> select('i.perm_id');
            $this -> db -> from('tbl_role_perm as i');
            $this -> db -> where('i.role_id', $ID);

            $query = $this -> db -> get(); 

            if($query -> num_rows() >= 1)
            {
                    return $query->result();
            }
            else
            {
                    return false;
            } 
	}

}

?>
