<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Roles extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Rolesmodel', 'rolesmodel', TRUE);
        $this->load->model('common_model/common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("RolesList")){
            redirect('dashboard');
        }
    }

    function index() {
        $roleData = array();
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('roles/index', $roleData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() { 
        if(!$this->privilegeduser->hasPrivilege("RolesAddEdit")){
            redirect('dashboard');
        }
        //add edit form
        $edit_datas = array();
        $role_id = 0;
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $role_id = $url_prams['id'];
            $result = $this->common->getData("tbl_roles", "*", array("role_id" => $role_id));
            if (!empty($result)) {
                $edit_datas['role_details'] = $result[0];
            }
        }
        
        $edit_datas['permissions'] = $this->rolesmodel->getPermdata();
        $edit_datas['selpermissions'] = $this->rolesmodel->getSelPermdata($role_id);
                
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('roles/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        $get_result = $this->rolesmodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->role_name));
                array_push($temp, ucfirst($get_result['query_result'][$i]->status));
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("RolesAddEdit")){
                    $actionCol = '<a href="roles/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->role_id), '+/', '-_'), '=') . '" title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() { 
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "role_name = '" . $this->input->post('role_name') . "' ";
            if (!empty($this->input->post("role_id"))) {
                $condition .= " AND role_id <> " . $this->input->post("role_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_roles", "role_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['role_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Role name already exist...'));
                exit;
            }

            $data = array();
            $data['role_name'] = $this->input->post('role_name');
            $data['status'] = $this->input->post('status');
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            if (!empty($this->input->post("role_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_roles", $data, array("role_id" => $this->input->post("role_id")));
                
                //delete and insert role permission
                if(!empty($this->input->post("perm_id"))){
                    $this->common->deleteRecord("tbl_role_perm",array("role_id" => $this->input->post("role_id")));
                    for($i=0;$i<sizeof($this->input->post("perm_id"));$i++){
                        $perm_data = array();
                        $perm_data['role_id']= $this->input->post("role_id");
                        $perm_data['perm_id']= $this->input->post("perm_id")[$i];
                        $rs = $this->common->insertData('tbl_role_perm',$perm_data,'1');
                    }
                }  
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['role_name'] = $this->input->post('role_name');
                $data['status'] = $this->input->post('status');
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_roles", $data, "1");
                
                //insert role permission data
                if(!empty($this->input->post("perm_id"))){
                    for($i=0;$i<sizeof($this->input->post("perm_id"));$i++){
                        $perm_data = array();
                        $perm_data['role_id']= $result;
                        $perm_data['perm_id']= $this->input->post("perm_id")[$i];
                        $rs = $this->common->insertData('tbl_role_perm',$perm_data,'1');
                    }
                }
                
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }

}

?>
