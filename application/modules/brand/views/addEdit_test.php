<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("brand"); ?>">Brand List</a>
                                <span>></span>
                                <p>Add Brand</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add Brand</h1>
                            </div>
                        </div>          
                        <button class="btn-primary-mro">Save</button>
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="brand_id" id="brand_id" value="<?php echo(!empty($brand_details['brand_id'])) ? $brand_details['brand_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="BrandName">Brand Name<sup>*</sup></label>
                                    <input type="text" name="brand_name" id="brand_name" class="input-form-mro" value="<?php echo(!empty($brand_details['brand_name'])) ? $brand_details['brand_name'] : ""; ?>">
                                </div> 
                                <div class="form-group">
                                    <label for="Status">Status</label>
                                    <select name="status" id="status" class="select-form-mro">
                                        <option value="Active" <?php echo(!empty($brand_details['status']) && $brand_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                        <option value="In-active" <?php echo(!empty($brand_details['status']) && $brand_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                    </select>
                                </div>
                                
                                <div class="form-group">
                                    <label for="">SKU Description</label>
                                    <div class="multiselect-dropdown-wrapper" style="width:100%">
                                        <div class="md-value">
                                            Search for SKU Description
                                        </div>
                                        <div class="md-list-wrapper">
                                            <input type="text" placeholder="Search" class="md-search">
                                            <div class="md-list-items ud-list">
                                                <?php 
                                                if ($skuDetails) {
                                                    foreach ($skuDetails as $skuValue) {  
                                                        ?>
                                                        <div class="mdli-single ud-list-single">
                                                            <label class="container-checkbox"><?= $skuValue['sku_number'] ?>
                                                                <input type="checkbox" id="sku_id<?= $skuValue['sku_id'] ?>" name="sku_id[]" value="<?= $skuValue['sku_id'] ?>" <?php if(in_array($skuValue['sku_id'], $skuIds)){ echo 'checked'; }?>>
                                                                <span class="checkmark-checkbox"></span>
                                                            </label>
                                                        </div>
                                                        <?php 
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div> 
                                    </div> 
                                </div> 
                                
                                
                            </div> 
                        </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {
        var vRules = {
            "brand_name": {required: true}
        };
        var vMessages = {
            "brand_name": {required: "Please enter brand name."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>brand/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        showInsertUpdateMessage(response.msg, response);
                        if (response.success) {
                            setTimeout(function () {
                                window.location = "<?= base_url('brand') ?>";
                            }, 3000);
                        }
                        $(".btn-primary-mro").show();
                    }
                });
            }
        });

    });
</script>