<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("pod"); ?>">POD List</a>
                                <span>></span>
                                <p>Add POD</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add POD</h1>
                            </div>
                        </div>   
                        <div class="title-sec-right"> 
                            <button type="submit" class="btn-primary-mro">Save</button>
                            <a href="<?php echo base_url("pod"); ?>" class="btn-transparent-mro cancel">Cancel</a> 
                        </div>   
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="pod_id" id="pod_id" value="<?php echo(!empty($pod_details['pod_id'])) ? $pod_details['pod_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="CategoryName">POD<sup>*</sup></label>
                                    <input type="text" name="pod_name" id="pod_name" class="input-form-mro" value="<?php echo(!empty($pod_details['pod_name'])) ? $pod_details['pod_name'] : ""; ?>">
                                </div> 
                                <div class="form-group">
                                    <label for="Status">Status</label>
                                    <select name="status" id="status" class="select-form-mro">
                                        <option value="Active" <?php echo(!empty($pod_details['status']) && $pod_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                        <option value="In-active" <?php echo(!empty($pod_details['status']) && $pod_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                    </select>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {
        var vRules = {
            "pod_name": {required: true}
        };
        var vMessages = {
            "pod_name": {required: "Please enter pod name."}
        };
        //check and save pod
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>pod/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('pod') ?>";
                            }, 1000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });
</script>