<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Pod extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Podmodel', 'podmodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("PODList")){
            redirect('dashboard');
        }
    }

    function index() {
        $podData = array();
        $result = $this->common->getData("tbl_pod", "*", "");
        if (!empty($result)) {
            $podData['pod_details'] = $result[0];
        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('pod/index', $podData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("PODAddEdit")){
            redirect('dashboard');
        }
        //add edit form
        $edit_datas = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $pod_id = $url_prams['id'];
            $result = $this->common->getData("tbl_pod", "*", array("pod_id" => $pod_id));
            if (!empty($result)) {
                $edit_datas['pod_details'] = $result[0];
            }
        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('pod/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        //get record at list
        $get_result = $this->podmodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->pod_name));
                array_push($temp, ucfirst($get_result['query_result'][$i]->status));
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("CategoryAddEdit")){    
                    $actionCol = '<a href="pod/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->pod_id), '+/', '-_'), '=') . '" title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "pod_name = '" . $this->input->post('pod_name') . "' ";
            if (!empty($this->input->post("pod_id"))) {
                $condition .= " AND pod_id <> " . $this->input->post("pod_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_pod", "pod_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['pod_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'POD already exist...'));
                exit;
            }

            $data = array();
            $data['pod_name'] = $this->input->post('pod_name');
            $data['status'] = $this->input->post('status');
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            if (!empty($this->input->post("pod_id"))) {
                //update data
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_pod", $data, array("pod_id" => $this->input->post("pod_id")));
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['pod_name'] = $this->input->post('pod_name');
                $data['status'] = $this->input->post('status');
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_pod", $data, "1");
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    } 

}

?>
