<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Loginmodel', 'loginmodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        if (!empty($_SESSION["mro_session"])) {
            redirect('dashboard');
        }
    }

    function index() {
        $this->load->view('template/head.php');
        $this->load->view('login/index');
        $this->load->view('template/footer-scripts.php');
    }

    function loginValidate() {
        //user login
        $result = $this->common->getData("tbl_users", "*", array("user_name" => $_POST['username'], "password" => md5($_POST['password']), "status" => "Active"));
        if ($result) {
            $_SESSION["mro_session"] = $result;
            $result1 = $this->common->getData("tbl_roles", "role_name", array("role_id" => $_SESSION["mro_session"][0]['role_id'], "status" => "Active"));
            if ($result1) {
                $_SESSION["mro_session"]['user_role'] = $result1[0]['role_name'];
            }
            $this->privilegeduser->getByUsername($_SESSION["mro_session"][0]['user_id']);
            echo json_encode(array("success" => true, "msg" => 'You are successfully logged in.'));
            exit;
        } else {
            echo json_encode(array("success" => false, "msg" => 'Username or Password incorrect.'));
            exit;
        }
    }

}

?>
