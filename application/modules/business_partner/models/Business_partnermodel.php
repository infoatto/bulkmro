<?PHP

class Business_partnermodel extends CI_Model {

    function getRecords($get) {
        $table = "tbl_business_partner";
        $table_id = 'business_partner_id';
        $default_sort_column = 'i.business_partner_id';
        $default_sort_order = 'desc';
        $condition = "1=1 ";
        $colArray = array('i.alias', 'i.business_type', 'i.business_category','i_status.invoice_status');
        $sortArray = array('i.alias', 'i.business_type', 'i.business_category', 'i.contact_person', 'i.phone_number', 'i.email', 'i.status');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order; 
        
        for ($i = 0; $i < count($colArray); $i++) {
            if (isset($_GET['Searchkey_' . $i]) && $_GET['Searchkey_' . $i] != "") { 
                $condition .= " AND " . $colArray[$i] . " LIKE '%" . $_GET['Searchkey_' . $i] . "%' ";  
            }
        }  
         
        //condition for Customer and Supplier roles user
        if($_SESSION["mro_session"]['user_role'] =='Customer' || $_SESSION["mro_session"]['user_role'] =='Supplier'){
           $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
           $condition .= " AND i.business_partner_id = ".$bpID; 
        }
        
        $this->db->select('i.*');
        $this->db->from("$table as i");
        $this->db->join("tbl_custom_field_invoice_data_submission as i_status","i_status.business_partner_id = i.business_partner_id","left");
        $this->db->where("($condition)");
        $this->db->group_by("i.business_partner_id");
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page);
        $query = $this->db->get();
        //echo $this->db->last_query();exit;

        $this->db->select('i.*');
        $this->db->from("$table as i");
        $this->db->join("tbl_custom_field_invoice_data_submission as i_status","i_status.business_partner_id = i.business_partner_id","left");
        $this->db->where("($condition)");
        $this->db->group_by("i.business_partner_id");
        $this->db->order_by($sort, $order);
        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    }
    
    function deleteRecord($tbl_name,$business_partner_id,$detailsColumn,$detailsId){
        $condition = " business_partner_id=$business_partner_id  AND  $detailsColumn <> $detailsId ";
        $this->db->where("($condition)");
        if($this->db->delete($tbl_name)){
            return true;
        }else{
            return false;
        }
    }

}

?>
