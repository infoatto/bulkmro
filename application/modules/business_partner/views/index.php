<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                            <span>></span>
                            <p>All Business Partners</p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title"><a href="<?php echo base_url("business_partner"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Business Partners </h1>
                        </div>
                    </div>
                    <div class="title-sec-right">
                        <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")) { ?>
                            <a href="<?php echo base_url("bporder/addEdit"); ?>" class="btn-secondary-mro"><img src="assets/images/add-icon-dark.svg" alt=""> New Business Partner Order</a>
                        <?php } ?>
                        <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersAddEdit")) { ?>
                            <a href="<?php echo base_url("business_partner/addEdit"); ?>" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> Add Business Partner</a>
                        <?php } ?>
                        <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersImport")) { ?>                            
                            <a href="#" class="btn-primary-mro" id="import">BP Import</a>
                        <?php } ?>
                    </div>
                </div>
                <div id="docuement_loader" style="display: none;">
                    <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                </div>
                <div class="page-content-wrapper" id="efrom" style="display:none;">
                    <form id="excel_form" name="excel_form" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload Excel</h6> 
                            <input type="file" id="excelfile" name="excelfile" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel()" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample_bp.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div> </br>     
                
                <div class="page-content-wrapper1">
                    <div id="serchfilter" class="filter-sec-wrapper">
                        <div class="form-group filter-search dataTables_filter searchFilterClass">
                            <input type="text" id="sSearch_0" name="sSearch_0" class="searchInput filter-search-input" placeholder="Business Alias">
                        </div>
                        <div class="form-group  dataTables_filter searchFilterClass">
                            <select name="sSearch_1" id="sSearch_1" onchange="getCategory(this.value);" class="searchInput basic-single select-mro select-xl">
                                <option value="" disabled selected hidden>Business Partner Type</option>
                                <option value="Customer">Customer</option>
                                <option value="Vendor">Vendor</option>
                            </select>
                        </div>
                        <div class="form-group dataTables_filter searchFilterClass"> 
                            <select name="sSearch_2" id="sSearch_2" class="searchInput basic-single select-mro select-xl">
                                <option value="" disabled selected hidden>Business Partner Category</option>
                            </select> 
                        </div>
                        
                        <div class="form-group dataTables_filter searchFilterClass">
                            <select name="sSearch_3" id="sSearch_3" class="searchInput basic-single select-mro select-xl">
                                <option value="" disabled selected hidden>Invoice Status</option>
                                <option value="paid">Paid</option>
                                <option value="unpaid">Un-Paid</option>
                            </select>
                        </div>
                        <div class="form-group clear-search-filter">
                            <button type="reset"  onclick="refresh()"  class="select-small btn-primary-mro">Clear</button>
                        </div>
                    </div>
                </div>
                <div class="bp-list-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Business Alias</th>
                                    <th>Partner Type</th>
                                    <th>Partner Category</th>
                                    <th>Contact Person</th>
                                    <th>Phone Number</th>
                                    <th>Email Address</th>
                                    <th>Status</th>
                                    <th class="table-action-cls" style="width:84px !important;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $( document ).ready(function() {
        var businessType = "";
        getCategory(businessType); 
    });
    
    function refresh(){
      location.reload();
    }
    
    //business partner category dropdown at click of business partner type 
    function getCategory(businessType) { 
        var act = "<?php echo base_url(); ?>business_partner/getBusinessPartnerAtList";
        $.ajax({
          type: 'GET',
          url: act,
          data: {
            businessType: businessType
          },
          dataType: "json",
          success: function(data) {
            $('#sSearch_2').html(data.dropdown);
            $('#sSearch_2').val('');
            $('.basic-single').select2();
          }
        }); 
    }


 function uploadExcel()
 {
    if( document.getElementById("excelfile").files.length == 0 ){ 
        showInsertUpdateMessage("Please Choose a xlsx file.",false);   
    }
    else
    {
        $(".upload-button").hide();
        $("#docuement_loader").show();
        var data = new FormData($('#excel_form')[0]);
        $.ajax({
            url: '<?php echo base_url('business_partner/importBusinessPartner'); ?>',
            type: 'POST',
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            data: data, 
            success: function(data)
            {
                $("#docuement_loader").hide();
                var html="";
                var response = JSON.parse(data); 
                
                if (response.success) {  
                    showInsertUpdateMessage(response.msg,response);
                    showInsertUpdateMessage("Total entry done: "+response.imported,response); 
                    if(response.notimported > 0){
                        showInsertUpdateMessage("Total entry failed: "+response.notimported,false);   
                    }
                    $('#excel_form')[0].reset();
                    setTimeout(function () {  
                        location.reload();
                    }, 3000);
                }else{
                    showInsertUpdateMessage(response.msg,response,false);
                }
                $(".upload-button").show();
            }
        });
    }
}

$("#import").click(function(){
   $("#efrom").toggle();
});
</script>