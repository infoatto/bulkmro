<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class State extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Statemodel', 'statemodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("StateList")){
            redirect('dashboard');
        }
    }

    function index() {
        $stateData = array();
        $result = $this->statemodel->getData(); 
        if (!empty($result)) {
            $stateData['state_details'] = $result[0];
        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('state/index', $stateData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function addEdit() {
        if(!$this->privilegeduser->hasPrivilege("StateAddEdit")){
            redirect('dashboard');
        }
        //add edit form code
        $edit_datas = array();
        $state_id = '';
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $state_id = $url_prams['id'];
            $result = $this->common->getData("tbl_state", "*", array("state_id" => $state_id));
            if (!empty($result)) {
                $edit_datas['state_details'] = $result[0];
            }
        }
        //get country code
        $countryData = $this->common->getData("tbl_country", "country_id,country_name", "");
        if (!empty($countryData)) {
            $edit_datas['countryData'] = $countryData;
        } 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('state/addEdit', $edit_datas);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function fetch() {
        //code for get state data at list
        $get_result = $this->statemodel->getRecords($_GET);
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                array_push($temp, ucfirst($get_result['query_result'][$i]->state_name));
                array_push($temp, $get_result['query_result'][$i]->country_name);
                $actionCol = '';
                if($this->privilegeduser->hasPrivilege("StateAddEdit")){  
                    $actionCol = '<a href="state/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->state_id), '+/', '-_'), '=') . '" title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    function submitForm() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "state_name = '" . $this->input->post('state_name') . "' ";
            if (!empty($this->input->post("state_id"))) {
                $condition .= " AND state_id <> " . $this->input->post("state_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_state", "state_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['state_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'State name already exist...'));
                exit;
            }

            $data = array();
            $data['state_name'] = $this->input->post('state_name');
            $data['country_id'] = $this->input->post('country_id');
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            if (!empty($this->input->post("state_id"))) {
                //update state
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_state", $data, array("state_id" => $this->input->post("state_id")));
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert state
                $data['state_name'] = $this->input->post('state_name');
                $data['country_id'] = $this->input->post('country_id');
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_state", $data, "1");
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }

}

?>
