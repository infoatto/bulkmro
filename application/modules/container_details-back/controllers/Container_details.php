<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Container_details extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Container_detailsmodel', 'container_detailsmodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
        if (!$this->privilegeduser->hasPrivilege("ContainersDetailsView")) {
            redirect('dashboard');
        }
    }

    function index() {
        $data['container_details'] = array();
        $data['show_details'] = array();
        $data['customerAlias'] = array();
        $data['supplierAlias'] = array();
        $data['fcr'] = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $container_id = $url_prams['id'];

            //document type dropdown code
            $LSFRIArr = array();
            $LSFRIDocumentNumber = $this->common->getData("tbl_document_uploaded_files", "document_type_id", array("container_id" => $container_id));
            $WithOutLSFRIDocumentNumber = $this->common->getData("tbl_document_ls_uploaded_files", "document_type_id", array("container_id" => $container_id));

            //LS and FRI number
            if (!empty($LSFRIDocumentNumber)) {
                foreach ($LSFRIDocumentNumber as $value) {
                    $LSFRIArr[$value['document_type_id']] = $value['document_type_id'];
                }
            }
            //without LS and FRI number
            if (!empty($WithOutLSFRIDocumentNumber)) {
                foreach ($WithOutLSFRIDocumentNumber as $value) {
                    $LSFRIArr[$value['document_type_id']] = $value['document_type_id'];
                }
            }

            $documentIdsString = '';
            if (!empty($LSFRIArr)) {
                $documentIdsString = implode("','", $LSFRIArr);
                $documentIdsString = "'" . $documentIdsString . "'";
            }

            $condition = " status = 'Active' ";
            if (!empty($documentIdsString)) {
                $condition = " status = 'Active' AND document_type_id NOT IN (" . $documentIdsString . ") ";
            }
            $data['documentType'] = $this->common->getData('tbl_document_type', '*', $condition, 'document_type_squence', 'asc');

            //fetch container data 
            $containerResult = $this->common->getData("tbl_container", "*", array("container_id" => $container_id));
            if (!empty($containerResult)) {
                $data['container_details'] = $containerResult[0];

                //container status  
                //$containerStatusData =  $this->container_detailsmodel->getContainerStatus($containerResult[0]['container_id']); 

                $condition = "container_status_id=" . $containerResult[0]['last_container_status_id'] . " ";
                $containerStatusData = $this->common->getData('tbl_container_status', 'container_status_name', $condition);
                if (!empty($containerStatusData)) {
                    $data['show_details']['containerStatus'] = $containerStatusData[0]['container_status_name'];
                }

                //last update by and date
                if (!empty($containerResult[0]['last_status_by'])) {
                    $updateByData = $this->common->getData("tbl_users", "firstname,lastname", array("user_id" => $containerResult[0]['last_status_by']));
                    if (!empty($updateByData)) {
                        $data['show_details']['updateBy'] = $updateByData[0]['firstname'] . ' ' . substr($updateByData[0]['lastname'], 0, 1) . ', ' . date('d-M-Y', strtotime($containerResult[0]['last_status_date']));
                    }
                } else {
                    $data['show_details']['updateBy'] = 'System, ' . date('d-M-Y', strtotime($containerResult[0]['last_status_date']));
                }

                //fetch CHA
                $freightForwarderData = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $containerResult[0]['freight_forwarder_id']));
                if (!empty($freightForwarderData)) {
                    $data['freightForwarder'] = $freightForwarderData[0]['alias'];
                }

                //CHA forwarder
                $brokerData = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $containerResult[0]['broker_id']));
                if (!empty($brokerData)) {
                    $data['CHA'] = $brokerData[0]['alias'];
                }

                //Liner data 
                $liner = $this->common->getData("tbl_liner", "liner_name", array("liner_id" => $containerResult[0]['liner_name']));
                if (!empty($liner)) {
                    $data['liner'] = $liner[0]['liner_name'];
                }

                //POL data 
                $pol = $this->common->getData("tbl_pol", "pol_name", array("pol_id" => $containerResult[0]['pol']));
                if (!empty($pol)) {
                    $data['pol'] = $pol[0]['pol_name'];
                }

                //POD data 
                $pod = $this->common->getData("tbl_pod", "pod_name", array("pod_id" => $containerResult[0]['pod']));
                if (!empty($pod)) {
                    $data['pod'] = $pod[0]['pod_name'];
                }

                //Flag data 
                $flag = $this->common->getData("tbl_flag", "flag_name", array("flag_id" => $containerResult[0]['flag']));
                if (!empty($flag)) {
                    $data['flag'] = $flag[0]['flag_name'];
                }

                //fetch customer and supplier billing and shipping details data
                $orderData = $this->container_detailsmodel->getAddressDetails($container_id);
                $customerContactBP = array();
                $supplierContactBP = array();
                $data['supplierContact'] = '';
                if (!empty($orderData)) {
                    foreach ($orderData as $value) {
                        $customerDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $value['customer_contract_id']));
                        if (!empty($customerDetail)) {
                            $customerContactBP = $customerDetail[0]['business_partner_id'];
                        }
                        $supplierDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $value['supplier_contract_id']));
                        if (!empty($supplierDetail)) {
                            $supplierContactBP = $supplierDetail[0]['business_partner_id'];
                            $data['supplierContact'] = $supplierDetail[0]['contract_number']; 
                            if ($this->privilegeduser->hasPrivilege("BusinessPartnersOrderView")) {
                                $data['supplierContact'] = '<a href="bporder/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $supplierDetail[0]['bp_order_id']), '+/', '-_'), '=') . '&view=1" ><u>' . $supplierDetail[0]['contract_number'] . '</u></a>';
                            } 
                        }
                    }
                }
                
                

                //fetch customer data
                $customerData = array();
                if (!empty($customerContactBP)) {
                    $customerData = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $customerContactBP));
                    if (!empty($customerData)) {
                        $data['customerAlias'] = $customerData[0]['alias'];
                    }
                }
                //echo '<pre>'; print_r($customerData);die;
                //fetch supplier data
                $supplierData = array();
                if (!empty($supplierContactBP)) {
                    $supplierData = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $supplierContactBP));
                    if (!empty($supplierData)) {
                        $data['supplierAlias'] = $supplierData[0]['alias'];
                    }
                }
                
                //master contract code
                $orderResult = $this->common->getData("tbl_order", "order_id,contract_number", array("order_id" => $containerResult[0]['order_id']));
                if (!empty($orderResult)) {
                    $data['masterContract'] = $orderResult[0]['contract_number']; 
                    if ($this->privilegeduser->hasPrivilege("MasterContractView")) {
                        $data['masterContract'] = '<a href="order/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $orderResult[0]['order_id']), '+/', '-_'), '=') . '&view=1" ><u>' . $orderResult[0]['contract_number'] . '</u></a>';                        
                    } 
                }
                
                //hbl number code
                $data['hblNumber'] = '';
                $condition = "container_id = ".$containerResult[0]['container_id']." and hbl_number is NOT null and hbl_number <> ''";
                $hblResult = $this->common->getData("tbl_custom_dynamic_data", "hbl_number", $condition);
                if (!empty($hblResult)) {
                    foreach ($hblResult as $value) {
                        $data['hblNumber'] = $value['hbl_number']; 
                    } 
                } 
                
                //brand name code
                $condition = " o_sku.order_id = " . $containerResult[0]['order_id'] . " ";
                $main_table = array("tbl_order_sku as o_sku", array()); 
                $join_tables = array( 
                    array("", "tbl_sku as sku", "sku.sku_id = o_sku.sku_id", array()),
                    array("", "tbl_brand as brand", "brand.brand_id = sku.brand_id", array('brand.brand_name')), 
                ); 
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                $bpData = $this->common->MySqlFetchRow($rs, "array");
                //echo $this->db->last_query();exit;
                $brandArr = array(); 
                if (!empty($bpData)) {
                    foreach ($bpData as $value) { 
                        $brandArr[$value['brand_name']] = $value['brand_name']; 
                    }
                } 
                $data['brandName'] = implode(",", $brandArr);
                
                $data['IHBL'] = '';
                $condition = "document_type_id=5 and sub_document_type_id=19 and container_id = ".$containerResult[0]['container_id']." ";
                $IHBLResult = $this->common->getData("tbl_document_ls_uploaded_files", "uploaded_document_number", $condition);
                if (!empty($IHBLResult)) {
                    $data['IHBL'] = $IHBLResult[0]['uploaded_document_number'];
                }

                //get FCR details
                // $fcrData = $this->container_detailsmodel->getFCRDetails($container_id);
                // if(!empty($fcrData)){
                //     foreach ($fcrData as $value) {  
                //         $data['fcr'][$value['custom_field_structure_id']] = $value['custom_field_structure_value'];
                //     }
                // }    
                // &contract=' . rtrim(strtr(base64_encode("order_id=" . $containerResult[0]['order_id']), '+/', '-_'), '=') . '
                $data['actionCol'] = '<a class="btn-transparent-mro" href="container/duplicate?text=' . rtrim(strtr(base64_encode("id=" . $containerResult[0]['container_id']), '+/', '-_'), '=') . '" ><img src="' . base_url() . 'assets/images/duplicate-icon.svg">Duplicate</a>';
            }

            $condition = "status = 'Active' ";
            $data['business_partner'] = $this->common->getData("tbl_business_partner", "*", $condition);


            $data['editURL'] = 'container/addEdit?text=' . $_GET['text'];

            $this->load->view('template/head.php');
            $this->load->view('template/navigation.php');
            $this->load->view('container_details/index', $data);
            $this->load->view('template/footer.php');
            $this->load->view('template/footer-scripts.php');
        }
    }

    function getEditableForm() {
        $container_id = $_GET['container_id'];
        $data['container_details'] = array();
        $data['containerStatusId'] = 0;
        $data['freightForwarderId'] = 0;
        $data['CHA_id'] = 0;
        $data['customerAlias'] = array();
        $data['supplierAlias'] = array();
        $data['containerStatusData'] = array();
        $data['freightForwarderData'] = array();
        $data['CHAData'] = array();

        $data['fcr'] = array();
        if (!empty($container_id) && $container_id > 0) {
            //fetch container data
            $containerResult = $this->common->getData("tbl_container", "*", array("container_id" => $container_id));
            if (!empty($containerResult)) {
                $data['container_details'] = $containerResult[0];

                //container status  
                $containerStatusData = $this->container_detailsmodel->getContainerStatus($containerResult[0]['container_id']);
                    
                if (!empty($containerStatusData)) {
                    $data['containerStatusId'] = $containerStatusData[0]['container_status_id'];
                }
                //cha and ff id from container
                $data['freightForwarderId'] = !empty($containerResult[0]['freight_forwarder_id']) ? $containerResult[0]['freight_forwarder_id'] : 0;
                $data['CHA_id'] = !empty($containerResult[0]['broker_id']) ? $containerResult[0]['broker_id'] : 0;

                //cha and ff from master contract
                $condition = "status = 'Active' AND order_id = " . $containerResult[0]['order_id'] . " ";
                $orderData = $this->common->getData("tbl_order", "broker_id,freight_forwarder_id", $condition);
                if (!empty($orderData)) {
                    //CHA data
                    if (!empty($orderData[0]['broker_id'])) {
                        $condition = "status = 'Active' AND business_partner_id IN (" . $orderData[0]['broker_id'] . ") ";
                        $chaDetails = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias", $condition, 'alias', 'asc');
                        if (!empty($chaDetails)) {
                            $data['CHAData'] = $chaDetails;
                        }
                    }
                    //Freight Forwarder data 
                    if (!empty($orderData[0]['freight_forwarder_id'])) {
                        $condition = "status = 'Active' AND business_partner_id IN (" . $orderData[0]['freight_forwarder_id'] . ") ";
                        $freightForwarder = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias", $condition, 'alias', 'asc');
                        if (!empty($freightForwarder)) {
                            $data['freightForwarderData'] = $freightForwarder;
                        }
                    }
                }

                //fetch customer and supplier billing and shipping details data
                $orderData = $this->container_detailsmodel->getAddressDetails($container_id);
                $customerContactBP = array();
                $supplierContactBP = array();
                $data['supplierContact'] = '';
                if (!empty($orderData)) {
                    foreach ($orderData as $value) {
                        $customerDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $value['customer_contract_id']));
                        if (!empty($customerDetail)) {
                            $customerContactBP = $customerDetail[0]['business_partner_id'];
                        }
                        $supplierDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $value['supplier_contract_id']));
                        if (!empty($supplierDetail)) {
                            $supplierContactBP = $supplierDetail[0]['business_partner_id']; 
                            $data['supplierContact'] = $supplierDetail[0]['contract_number'];  
                        }
                    }
                }

                //fetch customer data
                $customerData = array();
                if (!empty($customerContactBP)) {
                    $customerData = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $customerContactBP));
                    if (!empty($customerData)) {
                        $data['customerAlias'] = $customerData[0]['alias'];
                    }
                }
                //echo '<pre>'; print_r($customerData);die;
                //fetch supplier data
                $supplierData = array();
                if (!empty($supplierContactBP)) {
                    $supplierData = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $supplierContactBP));
                    if (!empty($supplierData)) {
                        $data['supplierAlias'] = $supplierData[0]['alias'];
                    }
                }
                
                $orderResult = $this->common->getData("tbl_order", "contract_number", array("order_id" => $containerResult[0]['order_id']));
                if (!empty($orderResult)) {
                    $data['masterContract'] = $orderResult[0]['contract_number'];
                }   
                
                $data['hblNumber'] = '';
                $condition = "container_id = ".$containerResult[0]['container_id']." and hbl_number is NOT null and hbl_number <> ''";
                $hblResult = $this->common->getData("tbl_custom_dynamic_data", "hbl_number", $condition);
                if (!empty($hblResult)) {
                    foreach ($hblResult as $value) {
                        $data['hblNumber'] = $value['hbl_number']; 
                    } 
                } 
                
                //brand name code
                $condition = " o_sku.order_id = " . $containerResult[0]['order_id'] . " ";
                $main_table = array("tbl_order_sku as o_sku", array()); 
                $join_tables = array( 
                    array("", "tbl_sku as sku", "sku.sku_id = o_sku.sku_id", array()),
                    array("", "tbl_brand as brand", "brand.brand_id = sku.brand_id", array('brand.brand_name')), 
                ); 
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                $bpData = $this->common->MySqlFetchRow($rs, "array");
                //echo $this->db->last_query();exit;
                $brandArr = array(); 
                if (!empty($bpData)) {
                    foreach ($bpData as $value) { 
                        $brandArr[$value['brand_name']] = $value['brand_name']; 
                    }
                } 
                $data['brandName'] = implode(",", $brandArr);
                
                $data['IHBL'] = '';
                $condition = "document_type_id=5 and sub_document_type_id=19 and container_id = ".$containerResult[0]['container_id']." ";
                $IHBLResult = $this->common->getData("tbl_document_ls_uploaded_files", "uploaded_document_number", $condition);
                if (!empty($IHBLResult)) {
                    $data['IHBL'] = $IHBLResult[0]['uploaded_document_number'];
                }

                //get FCR details
                //    $fcrData = $this->container_detailsmodel->getFCRDetails($container_id);
                //    if(!empty($fcrData)){
                //        foreach ($fcrData as $value) {  
                //            $data['fcr'][$value['custom_field_structure_id']] = $value['custom_field_structure_value'];
                //        }
                //    }                      
            }
            $condition = " status = 'Active'  "; //AND container_status_id IN (8,9,11,2)
            $containerStatusData = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", $condition, 'status_squence', 'asc');
            if (!empty($containerStatusData)) {
                $data['containerStatusData'] = $containerStatusData;
            }

            //Liner data 
            $liner = $this->common->getData("tbl_liner", "liner_id,liner_name", array("status" => 'Active'), 'liner_name', 'asc');
            if (!empty($liner)) {
                $data['liner'] = $liner;
            }

            //POL data 
            $pol = $this->common->getData("tbl_pol", "pol_id,pol_name", array("status" => 'Active'), 'pol_name', 'asc');
            if (!empty($pol)) {
                $data['pol'] = $pol;
            }

            //POD data 
            $pod = $this->common->getData("tbl_pod", "pod_id,pod_name", array("status" => 'Active'), 'pod_name', 'asc');
            if (!empty($pod)) {
                $data['pod'] = $pod;
            }

            //Flag data 
            $flag = $this->common->getData("tbl_flag", "flag_id,flag_name", array("status" => 'Active'), 'flag_name', 'asc');
            if (!empty($flag)) {
                $data['flag'] = $flag;
            }


            $html = "";
            $html = $this->load->view("index-editable-form", $data, true);

            echo json_encode(array('html' => $html));
            exit;
        }
    }

    function editableSubmitForm() {

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "container_number = '" . $this->input->post('container_number') . "' ";
            if (!empty($this->input->post("container_id"))) {
                $condition .= " AND container_id <> " . $this->input->post("container_id");
            }

            $chk_client_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
            $rs_client = $this->common->MySqlFetchRow($chk_client_sql, "array");

            if (!empty($rs_client[0]['container_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Container Number already exist...'));
                exit;
            }

            $data = array();
            $data['container_number'] = $this->input->post('container_number');
            $data['broker_id'] = !empty($this->input->post('broker_id')) ? $this->input->post('broker_id') : 0;
            $data['freight_forwarder_id'] = !empty($this->input->post('freight_forwarder_id')) ? $this->input->post('freight_forwarder_id') : 0;
            $data['revised_eta'] = !empty($this->input->post('revised_eta')) ? date("Y-m-d", strtotime($this->input->post('revised_eta'))) : NULL;
            $data['revised_etd'] = !empty($this->input->post('revised_etd')) ? date("Y-m-d", strtotime($this->input->post('revised_etd'))) : NULL;
            //$data['ett'] = $this->input->post('ett');
            $data['revised_ett'] = $this->input->post('revised_ett');
            $data['liner_name'] = $this->input->post('liner_name');
            $data['liner_tracker_url'] = $this->input->post('liner_tracker_url'); 
            $data['pol'] = $this->input->post('pol');
            $data['pod'] = $this->input->post('pod');
            $data['vessel_name'] = $this->input->post('vessel_name');
            $data['vessel_tracker_url'] = $this->input->post('vessel_tracker_url');            
            $data['flag'] = $this->input->post('flag'); 
            $data['lot_number'] = $this->input->post('lot_number');
            $data['terminal'] = $this->input->post('terminal');
            $data['notes'] = $this->input->post('notes');
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            if (!empty($this->input->post("container_id"))) {
                //update container
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $result = $this->common->updateData("tbl_container", $data, array("container_id" => $this->input->post("container_id")));

                if (!empty($this->input->post("container_status_id")) && ($this->input->post("old_container_status_id") != $this->input->post("container_status_id"))) {
                    $data1['container_id'] = $this->input->post("container_id");
                    $data1['container_status_id'] = $this->input->post("container_status_id");
                    $data1['created_on'] = date("Y-m-d H:i:s");
                    $data1['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data1['document_date'] = date("Y-m-d");
                    $result1 = $this->common->insertData('tbl_container_status_log', $data1, '1');

                    //last status in container table
                    $containerStatusUpdate = array();
                    $containerStatusUpdate['last_container_status_id'] = $this->input->post("container_status_id");
                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $this->input->post("container_id")));
                }

                if ($result) {

                    //container details show information
                    $container_id = $this->input->post("container_id");
                    $data['container_details'] = array();
                    $data['show_details'] = array();
                    $data['customerAlias'] = array();
                    $data['supplierAlias'] = array();
                    $data['fcr'] = array();
                    if (!empty($container_id) && $container_id > 0) {
                        //fetch container data
                        $containerResult = $this->common->getData("tbl_container", "*", array("container_id" => $container_id));
                        if (!empty($containerResult)) {
                            $data['container_details'] = $containerResult[0];

                            //container status  
                            $containerStatusData = $this->container_detailsmodel->getContainerStatus($containerResult[0]['container_id']);
                            if (!empty($containerStatusData)) {
                                $data['show_details']['containerStatus'] = $containerStatusData[0]['container_status_name'];
                            }

                            //last update by and date
                            $updateByData = $this->common->getData("tbl_users", "firstname,lastname", array("user_id" => $containerResult[0]['updated_by']));
                            if (!empty($updateByData)) {
                                $data['show_details']['updateBy'] = $updateByData[0]['firstname'] . ' ' . substr($updateByData[0]['lastname'], 0, 1) . ', ' . date('d-M-y', strtotime($containerResult[0]['updated_on']));
                            }

                            //fetch CHA
                            $freightForwarderData = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $containerResult[0]['freight_forwarder_id']));
                            if (!empty($freightForwarderData)) {
                                $data['freightForwarder'] = $freightForwarderData[0]['alias'];
                            }

                            //fetch freight forwarder
                            $brokerData = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $containerResult[0]['broker_id']));
                            if (!empty($brokerData)) {
                                $data['CHA'] = $brokerData[0]['alias'];
                            }

                            //fetch customer and supplier billing and shipping details data
                            $orderData = $this->container_detailsmodel->getAddressDetails($container_id);
                            $customerContactBP = array();
                            $supplierContactBP = array();
                            if (!empty($orderData)) {
                                foreach ($orderData as $value) {
                                    $customerDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $value['customer_contract_id']));
                                    if (!empty($customerDetail)) {
                                        $customerContactBP = $customerDetail[0]['business_partner_id'];
                                    }
                                    $supplierDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $value['supplier_contract_id']));
                                    if (!empty($supplierDetail)) {
                                        $supplierContactBP = $supplierDetail[0]['business_partner_id'];
                                    }
                                }
                            }

                            //fetch customer data
                            $customerData = array();
                            if (!empty($customerContactBP)) {
                                $customerData = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $customerContactBP));
                                if (!empty($customerData)) {
                                    $data['customerAlias'] = $customerData[0]['alias'];
                                }
                            }
                            //echo '<pre>'; print_r($customerData);die;
                            //fetch supplier data
                            $supplierData = array();
                            if (!empty($supplierContactBP)) {
                                $supplierData = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $supplierContactBP));
                                if (!empty($supplierData)) {
                                    $data['supplierAlias'] = $supplierData[0]['alias'];
                                }
                            }

                            //get FCR details
                            $fcrData = $this->container_detailsmodel->getFCRDetails($container_id);
                            if (!empty($fcrData)) {
                                foreach ($fcrData as $value) {
                                    $data['fcr'][$value['custom_field_structure_id']] = $value['custom_field_structure_value'];
                                }
                            }
                        }

                        $data['actionCol'] = '<a class="btn-transparent-mro" href="container/duplicate?text=' . rtrim(strtr(base64_encode("id=" . $containerResult[0]['container_id']), '+/', '-_'), '=') . '" ><img src="' . base_url() . 'assets/images/duplicate-icon.svg">Duplicate</a>';
                    }

                    $html = "";
                    $html = $this->load->view("index-without-editable", $data, true);

                    echo json_encode(array('success' => true, 'msg' => 'Container updated successfully.', 'html' => $html));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while updating data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while updating data.'));
            exit;
        }
    }

    function deleteDocumentType() {

        $Result = $this->common->deleteRecord('tbl_sub_document_type', 'sub_document_type_id = ' . $_POST['id']);
        if ($Result) {
            echo json_encode(array("success" => "1", "msg" => "Deleted successfully.", "body" => NULL));
            exit;
        } else {
            echo json_encode(array("success" => "0", "msg" => "Delete Failed", "body" => NULL));
            exit;
        }
    }

    public function setCurrentSessionId() {
        $current_session_id = round(microtime(true) * 1000);
        $_SESSION['mro_session'][0]['current_session_id'] = $current_session_id;
        if ($_SESSION['mro_session'][0]['current_session_id']) {
            echo json_encode(array("success" => "1", "msg" => $_SESSION['mro_session'][0]['current_session_id'], "body" => NULL));
            exit;
        } else {
            echo json_encode(array("success" => "0", "msg" => "Current session id not set", "body" => NULL));
            exit;
        }
    }

    public function submitFormStep2Details() {

        //code by ravendra this is use for back and then save record functionality.
        $this->common->deleteRecord("tbl_document_ls_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_document_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_invoice_ls_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");

        if (!empty($_POST['document_type']) && isset($_POST['document_type'])) {

            // insert query for  other type of document  is LS number found 
            if (!empty($_POST['ls_number']) && isset($_POST['ls_number'])) {
                if (!empty($_POST['container_ids']) && isset($_POST['container_ids'])) {
                    $data = array();
                    $data['document_type_id'] = $_POST['document_type'];
                    $data['ls_number'] = $_POST['ls_number'];
                    $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                    $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['updated_on'] = date("Y-m-d H:i:s");
                    $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['created_on'] = date("Y-m-d H:i:s");

                    foreach ($_POST['container_ids'] as $key => $value) {
                        $data['container_id'] = $value;
                        $result = $this->common->insertData("tbl_document_ls_container_mapping", $data, "1");
                    }
                    $type = "other";
                } else {
                    echo json_encode(array("success" => false, "msg" => "Select Atleast one LS!"));
                    exit;
                }
            } else {

                // insert data for fri n ls document type 
                if (!empty($_POST['container_ids']) && isset($_POST['container_ids'])) {
                    $data = array();
                    $data['document_type_id'] = $_POST['document_type'];
                    $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                    $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['updated_on'] = date("Y-m-d H:i:s");
                    $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['created_on'] = date("Y-m-d H:i:s");

                    foreach ($_POST['container_ids'] as $key => $value) {
                        $data['container_id'] = $value;
                        $result = $this->common->insertData("tbl_document_container_mapping", $data, "1");
                    }
                    $type = "LS";
                } else {
                    echo json_encode(array("success" => false, "msg" => "Select Atleast one LS!"));
                    exit;
                }
            }


            if ($result) {
                echo json_encode(array("success" => true, "type" => $type, "msg" => "Document mapped with container", "doc_id" => $_POST['document_type']));
                exit;
            } else {
                echo json_encode(array("success" => false, "type" => $type, "msg" => "Select Atleast one LS!", "doc_id" => $_POST['document_type']));
                exit;
            }
        } else {
            $type = "invoice";
            // this code for invoice mapping with container 
            if (!empty($_POST['business_partner']) && isset($_POST['business_partner'])) {
                if (!empty($_POST['ls_number']) && isset($_POST['ls_number'])) {
                    if (!empty($_POST['container_ids']) && isset($_POST['container_ids'])) {
                        $data = array();
                        $data['business_partner_id'] = $_POST['business_partner'];
                        $data['ls_number'] = $_POST['ls_number'];
                        $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['created_on'] = date("Y-m-d H:i:s");

                        foreach ($_POST['container_ids'] as $key => $value) {
                            $data['container_id'] = $value;
                            $result = $this->common->insertData("tbl_invoice_ls_container_mapping", $data, "1");
                        }
                        if ($result) {
                            echo json_encode(array("success" => true, "type" => $type, "msg" => "Invoice  mapped with container"));
                            exit;
                        } else {
                            echo json_encode(array("success" => false, "type" => $type, "msg" => "Select Atleast one LS!"));
                            exit;
                        }
                    } else {
                        echo json_encode(array("success" => false, "msg" => "Select Atleast one LS!"));
                        exit;
                    }
                } else {
                    echo json_encode(array("success" => false, "type" => $type, "msg" => "LS is missing for invoice type "));
                    exit;
                }
            } else {
                echo json_encode(array("success" => false, "type" => $type, "msg" => "Business partner missing "));
                exit;
            }
        }
    }

    public function getSelectedLsContainer() {

        if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {

            if ($_POST['type'] == 'other') {

                $condition = "1=1  AND dcm.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_document_ls_container_mapping as dcm", array('ls_number,document_type_id'));
                $join_tables = array(
                    array("", "tbl_container as c", "dcm.container_id = c.container_id", array("c.container_number,c.container_id")),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                $result['getSelectedContainer'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result


                if ($result) {
                    $html = '';
                    $selectedLsName = '';
                    foreach ($result['getSelectedContainer'] as $key => $value) {
                        $selectedLsName = $value['ls_number'];
                        $html .= '<div class="ud-list-single">
                                    <label class="">' . $value['container_number'] . '
                                    <input type="hidden" name="selectedContainer[]" value="' . $value['container_id'] . '" checked> 
                                    </label>
                                </div>
                                <input type="hidden" value="' . $value['ls_number'] . '"  name="selectedLsNumber" id="selectedLsNumber">  ';
                    }


                    $uploadSectionhtml = "";


                    $condition = "1=1  AND d.document_type_id = '" . $result['getSelectedContainer'][0]['document_type_id'] . "' ";
                    $main_table = array("tbl_document_type as d", array('d.document_type_name'));
                    $join_tables = array(
                        array("left", "tbl_sub_document_type as sd", "sd.document_type_id = d.document_type_id", array("sd.sub_document_type_name,sd.sub_document_type_id")),
                    );

                    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                    $getSubDocument = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                    if ($getSubDocument) {
                        foreach ($getSubDocument as $key => $value) {
                            if (!empty($value['sub_document_type_name'])) {
                                $uploadSectionhtml .= '<div class="mb20"><h4 class="sd-subtitle mb20">' . (!empty($value['sub_document_type_name']) ? $value['sub_document_type_name'] : " Upload Document ") . '</h4>
                                <input type="file"  name="step3_document[' . $value['sub_document_type_id'] . ']" id="step3_document' . $value['sub_document_type_id'] . '" required ></div>';
                            } else {
                                $uploadSectionhtml .= '<div class="mb20"><h4 class="sd-subtitle mb20"> Upload Document </h4>
                                <input type="file"  required name="step3_document[]" id="step3_document' . $value['sub_document_type_id'] . '"></div>';
                            }
                        }
                    }
                    echo json_encode(array("success" => true, "msg" => "Selected Ls container found", 'selectedLsName' => $selectedLsName, 'html' => $html, 'uploadSectionhtml' => $uploadSectionhtml));
                    exit;
                }
            } else {

                $condition = "1=1  AND icm.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_invoice_ls_container_mapping as icm", array('ls_number,business_partner_id'));
                $join_tables = array(
                    array("", "tbl_container as c", "icm.container_id = c.container_id", array("c.container_number,c.container_id")),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                $result['getSelectedContainer'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                if ($result) {
                    $html = '';
                    $selectedLsName = '';
                    foreach ($result['getSelectedContainer'] as $key => $value) {
                        $selectedLsName = $value['ls_number'];
                        $html .= '<div class="ud-list-single">
                                    <label class="">' . $value['container_number'] . '
                                    <input type="hidden" name="selectedContainer[]" value="' . $value['container_id'] . '" checked> 
                                    </label>
                                </div>
                                <input type="hidden" value="invoice"  name="invoice" id="invoice">
                                <input type="hidden" value="' . $value['ls_number'] . '"  name="selectedLsNumber" id="selectedLsNumber">';
                    }
                    $uploadSectionhtml = '';

                    $uploadSectionhtml .= '<p><h4 class="sd-subtitle mb20"> Upload Invoice </h4>
                    <input type="file"  required name="step3_document" id="step3_document"></p>';

                    echo json_encode(array("success" => true, "msg" => "Selected Ls container found", 'selectedLsName' => $selectedLsName, 'html' => $html, 'uploadSectionhtml' => $uploadSectionhtml));
                    exit;
                }
            }
        } else {
            echo json_encode(array("success" => false, "msg" => "Current session id not found "));
            exit;
        }
    }

    public function getSelectedContainer() {

        if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {

            $condition = "1=1  AND dcm.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
            $main_table = array("tbl_document_container_mapping as dcm", array());
            $join_tables = array(
                array("", "tbl_container as c", "dcm.container_id = c.container_id", array("c.container_number,c.container_id")),
            );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $result['getSelectedContainer'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
            // echo "<pre>";
            // print_r($result);
            // exit;

            if ($result) {
                $html = '';
                foreach ($result['getSelectedContainer'] as $key => $value) {

                    $html .= '<div class="ud-list-single">
                                ' . $value['container_number'] . '
                                <input type="hidden" name="selectedContainer[]" value="' . $value['container_id'] . '"> 
                            </div>';
                }
                echo json_encode(array("success" => true, "msg" => "", 'html' => $html));
                exit;
            } else {
                echo json_encode(array("success" => false, "msg" => "Current session id not found "));
                exit;
            }
        } else {
            echo json_encode(array("success" => false, "msg" => "Current session id not found "));
            exit;
        }
    }

    public function submitFormStep3Details() {
        //code by ravendra this is use for back and then save record functionality.
        $this->common->deleteRecord("tbl_document_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_document_ls_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_container_uploaded_document_status", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
        $statusLog = $this->common->getData("tbl_container_status_log", 'status_log_id', $condition);
        if (!empty($statusLog)) {
            $this->common->deleteRecord("tbl_container_status_log", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        }
        $this->common->deleteRecord("tbl_invoice_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");

        // echo "<pre>";
        // print_r($_POST);
        // print_r($_FILES);
        // exit;
        $upload = false;
        $data = array();
        $this->load->library('upload');
        $step4skip = false;
        if (!empty($_POST['invoice']) && isset($_POST['invoice'])) {
            $step4skip = true;
            if (isset($_FILES) && isset($_FILES["step3_document"]["name"])) {
                $this->upload->initialize($this->set_upload_options(0));
                if (!$this->upload->do_upload("step3_document")) {
                    $image_error = array('error' => $this->upload->display_errors());
                    echo json_encode(array("success" => false, "msg" => $image_error['error']));
                    exit;
                } else {
                    $image_data = array('upload_data' => $this->upload->data());
                    //function for upload file in live bucket
                    $full_path = $image_data['upload_data']['full_path'];                            
                    $resultUpload = set_s3_upload_file($full_path,0);
                    
                    $step3_document = $image_data['upload_data']['file_name'];
                    $data['uploaded_invoice_file'] = $step3_document;
                    $upload = true;
                }
            }

            if ($upload) {
                // get document selected in previous screen 
                $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_invoice_ls_container_mapping as icm", array("icm.*"));
                $join_tables = array(
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = icm.business_partner_id", array("bp.*")),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                $invoice_details = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                if ($invoice_details) {
                    if (!empty($_POST['selectedContainer']) && isset($_POST['selectedContainer'])) {

                        foreach ($_POST['selectedContainer'] as $key => $value) {
                            $data['container_id'] = $value;
                            $data['business_partner_id'] = $invoice_details[0]['business_partner_id'];
                            $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                            // $data['uploaded_document_number'] =   $step3_document;
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            // print_r($data);
                            $result = $this->common->insertData("tbl_invoice_uploaded_files", $data, "1");
                        }
                        if ($result) {
                            echo json_encode(array("success" => true, "msg" => "Document added to container!", 'type' => 'invoice', 'step4skip' => $step4skip));
                            exit;
                        } else {
                            echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                            exit;
                        }
                    } else {
                        echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                        exit;
                    }
                } else {
                    echo json_encode(array("success" => false, "msg" => "Selected business partber  Not found!"));
                    exit;
                }
            } else {
                echo json_encode(array("success" => false, "msg" => "Uploaded file is not proper !"));
                exit;
            }
        } else {
            $step4skip = false;
            if (!empty($_POST['selectedLsNumber']) && isset($_POST['selectedLsNumber'])) {
                $files = $_FILES;

                foreach ($files['step3_document']['name'] as $key => $value) {
                    $upload = false;
                    if (!empty($files['step3_document']['name'][$key]) && isset($files['step3_document']['name'][$key])) {

                        $_FILES['step3_document']['name'] = $files['step3_document']['name'][$key];
                        $_FILES['step3_document']['type'] = $files['step3_document']['type'][$key];
                        $_FILES['step3_document']['tmp_name'] = $files['step3_document']['tmp_name'][$key];
                        $_FILES['step3_document']['error'] = $files['step3_document']['error'][$key];
                        $_FILES['step3_document']['size'] = $files['step3_document']['size'][$key];

                        $this->upload->initialize($this->set_upload_options($_POST['document_type_id_hidden']));
                        if (!$this->upload->do_upload("step3_document")) {
                            $image_error = array('error' => $this->upload->display_errors());
                            echo json_encode(array("success" => false, "msg" => $image_error['error']));
                            exit;
                        } else {
                            $image_data = array('upload_data' => $this->upload->data());
                            //function for upload file in live bucket
                            $full_path = $image_data['upload_data']['full_path'];                            
                            $resultUpload = set_s3_upload_file($full_path,$_POST['document_type_id_hidden']);
                            $step3_document = $image_data['upload_data']['file_name'];
                            if ($key == '0') {
                                $uploaded_document_type_id['document_type_id'] = $key;
                                $data['uploaded_document_file'] = $step3_document;
                            } else {
                                $uploaded_document_file_name['file_name'][$key] = $step3_document;
                                $uploaded_document_type_id['sub_document_type_id'][$key] = $key;
                            }
                            $upload = true;
                        }
                    }
                }

                if ($upload) {

                    foreach ($files['step3_document']['name'] as $key => $value) {
                        // echo $value;
                        // this is for only document type  store 
                        if ($key == 0) {
                            // get document selected in previous screen 
                            $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                            $main_table = array("tbl_document_ls_container_mapping as dlcm", array("dlcm.*"));
                            $join_tables = array(
                                array("", "tbl_document_type as dt", "dt.document_type_id = dlcm.document_type_id", array("dt.*")),
                            );
                            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                            $document_type_details = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                            // $document_name = $document_type_details[0]['document_prefix_name'].'00001';
                            $document_name = $document_type_details[0]['document_prefix_name'];

                            // if($document_name){
                            //     $condition = "document_type_id = '".$document_type_details[0]['document_type_id']."' ";
                            //     $uploaded_document_data = $this->common->getDataLimit("tbl_document_ls_uploaded_files",'*',$condition,"document_uploaded_files_id","desc",'1');
                            // }
                            // if($uploaded_document_data){
                            //     $uploaded_document_name = ++$uploaded_document_data[0]['uploaded_document_number'];
                            // }else{
                            //     $uploaded_document_name = $document_name;
                            // }
                            // echo $uploaded_document_name ;
                            // exit; 
                            // get container status for document uploaded 
                            $condition = " document_type_id = '" . $document_type_details[0]['document_type_id'] . "' ";
                            $getContainerStatus = $this->common->getData('tbl_container_status', '*', $condition);

                            if ($document_name) {
                                if (!empty($_POST['selectedContainer']) && isset($_POST['selectedContainer'])) {
                                    foreach ($_POST['selectedContainer'] as $containerkey => $containervalue) {

                                        $condition = " container_id = '" . $containervalue . "' ";
                                        $getContainerNumber = $this->common->getData('tbl_container', 'container_number,revised_etd,last_container_status_id', $condition);

                                        $condition = " container_status_id = '" . $getContainerNumber[0]['last_container_status_id'] . "' ";
                                        $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status', 'status_squence', $condition);
                                        $lastContainerStatusSquence = 0;
                                        if (!empty($lastContainerStatusSquenceData)) {
                                            $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                        }

                                        $data['container_id'] = $containervalue;
                                        $data['document_type_id'] = $document_type_details[0]['document_type_id'];
                                        $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $data['uploaded_document_number'] = $document_name . $getContainerNumber[0]['container_number'];
                                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['updated_on'] = date("Y-m-d H:i:s");
                                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['created_on'] = date("Y-m-d H:i:s");
                                        // print_r($data);
                                        $result = $this->common->insertData("tbl_document_ls_uploaded_files", $data, "1");

                                        // insert record in tbl_container_uploaded_document_status for maintain the statsus for container with respective to document uploaded 
                                        $doc_status_data = array();
                                        $doc_status_data['container_id'] = $containervalue;
                                        $doc_status_data['document_type_id'] = $document_type_details[0]['document_type_id'];
                                        $doc_status_data['sub_document_type_id'] = 0;
                                        $doc_status_data['document_type_squence'] = $document_type_details[0]['document_type_squence'];
                                        $doc_status_data['status'] = 'Unchecked';
                                        $doc_status_data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                        $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                        $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1");

                                        //HBL document upload check
                                        if ($document_type_details[0]['document_type_id'] == 5) {
                                            if ($getContainerStatus && !empty($getContainerNumber[0]['revised_etd'])) {
                                                $ContainerStatus = array();
                                                $ContainerStatus['container_id'] = $containervalue;
                                                $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                                $ContainerStatus['document_date'] = date("Y-m-d");
                                                $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                                if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                                    //last status in container table
                                                    $containerStatusUpdate = array();
                                                    $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $containervalue));
                                                }
                                            }
                                        } else {
                                            if ($getContainerStatus) {
                                                $ContainerStatus = array();
                                                $ContainerStatus['container_id'] = $containervalue;
                                                $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                                $ContainerStatus['document_date'] = date("Y-m-d");
                                                $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                                if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                                    //last status in container table
                                                    $containerStatusUpdate = array();
                                                    $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $containervalue));
                                                }
                                            }
                                        }
                                    }
                                    if ($result) {
                                        echo json_encode(array("success" => true, "msg" => "Document added to container!", 'type' => 'LS', 'step4skip' => $step4skip));
                                        exit;
                                    } else {
                                        echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                                        exit;
                                    }
                                } else {
                                    echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                                    exit;
                                }
                            } else {
                                echo json_encode(array("success" => false, "msg" => "Selected Document Not found!"));
                                exit;
                            }
                        } else {
                            // this is for sub document type 
                            $condition = "sub_document_type_id = '" . $key . "' ";
                            $document_sub_type_details = $this->common->getData("tbl_sub_document_type", '*', $condition);

                            $document_name = $document_sub_type_details[0]['sub_document_prefix_name'];

                            // if($document_name){
                            //     $condition = "sub_document_type_id = '".$document_sub_type_details[0]['sub_document_type_id']."' ";
                            //     $uploaded_document_data = $this->common->getDataLimit("tbl_document_ls_uploaded_files",'*',$condition,"document_uploaded_files_id","desc",'1');
                            // }
                            // if($uploaded_document_data){
                            //     // echo $uploaded_document_data[0]['uploaded_document_number'];
                            //     $uploaded_document_name = ++$uploaded_document_data[0]['uploaded_document_number'];
                            // }else{
                            //     $uploaded_document_name = $document_name;
                            // }
                            // get document sequence for container status 
                            $condition = "document_type_id = '" . $document_sub_type_details[0]['document_type_id'] . "' ";
                            $document_type_details = $this->common->getData("tbl_document_type", '*', $condition);

                            // echo "<pre>";
                            // print_r($uploaded_document_type_id);
                            // print_r($uploaded_document_file_name);
                            // exit; 
                            // get container status for document uploaded 
                            $condition = " document_type_id = '" . $document_type_details[0]['document_type_id'] . "' ";
                            $getContainerStatus = $this->common->getData('tbl_container_status', '*', $condition);


                            if ($document_name) {
                                if (!empty($_POST['selectedContainer']) && isset($_POST['selectedContainer'])) {

                                    foreach ($_POST['selectedContainer'] as $containerkey => $containervalue) {
                                        $condition = " container_id = '" . $containervalue . "' ";
                                        $getContainerNumber = $this->common->getData('tbl_container', 'container_number,revised_etd,last_container_status_id', $condition);

                                        $condition = " container_status_id = '" . $getContainerNumber[0]['last_container_status_id'] . "' ";
                                        $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status', 'status_squence', $condition);
                                        $lastContainerStatusSquence = 0;
                                        if (!empty($lastContainerStatusSquenceData)) {
                                            $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                        }

                                        $data['container_id'] = $containervalue;
                                        $data['document_type_id'] = $document_sub_type_details[0]['document_type_id'];
                                        $data['sub_document_type_id'] = $uploaded_document_type_id['sub_document_type_id'][$key];
                                        $data['uploaded_document_file'] = $uploaded_document_file_name['file_name'][$key];
                                        $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $data['uploaded_document_number'] = $document_name . $getContainerNumber[0]['container_number'];
                                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['updated_on'] = date("Y-m-d H:i:s");
                                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['created_on'] = date("Y-m-d H:i:s");
                                        // print_r($data);
                                        $result = $this->common->insertData("tbl_document_ls_uploaded_files", $data, "1");

                                        // insert record in tbl_container_uploaded_document_status for maintain the statsus for container with respective to document uploaded 
                                        $doc_status_data = array();
                                        $doc_status_data['container_id'] = $containervalue;
                                        $doc_status_data['document_type_id'] = $document_type_details[0]['document_type_id'];
                                        $doc_status_data['sub_document_type_id'] = $uploaded_document_type_id['sub_document_type_id'][$key];
                                        $doc_status_data['document_type_squence'] = $document_type_details[0]['document_type_squence'];
                                        $doc_status_data['status'] = 'Unchecked';
                                        $doc_status_data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                        $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                        $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1");

                                        //HBL document upload check
                                        if ($document_type_details[0]['document_type_id'] == 5) {
                                            if ($getContainerStatus && !empty($getContainerNumber[0]['revised_etd'])) {
                                                $ContainerStatus = array();
                                                $ContainerStatus['container_id'] = $containervalue;
                                                $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                                $ContainerStatus['document_date'] = date("Y-m-d");
                                                $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                                if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                                    //last status in container table
                                                    $containerStatusUpdate = array();
                                                    $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $containervalue));
                                                }
                                            }
                                        } else {
                                            if ($getContainerStatus) {
                                                $ContainerStatus = array();
                                                $ContainerStatus['container_id'] = $containervalue;
                                                $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                                $ContainerStatus['document_date'] = date("Y-m-d");
                                                $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                                if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                                    //last status in container table
                                                    $containerStatusUpdate = array();
                                                    $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $containervalue));
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                                    exit;
                                }
                            } else {
                                echo json_encode(array("success" => false, "msg" => "Selected Document Not found!"));
                                exit;
                            }
                        }
                    } // foreach close here

                    if ($result) {
                        echo json_encode(array("success" => true, "msg" => "Document added to container !", 'type' => 'LS', 'step4skip' => $step4skip));
                        exit;
                    } else {
                        echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                        exit;
                    }
                } else {
                    echo json_encode(array("success" => false, "msg" => "Uploaded file is not proper !"));
                    exit;
                }
            } else {
                // this is for fr n ls document type 
                if (isset($_FILES) && isset($_FILES["step3_document"]["name"])) {
                    $this->upload->initialize($this->set_upload_options($_POST['document_type_id_hidden']));
                    if (!$this->upload->do_upload("step3_document")) {
                        $image_error = array('error' => $this->upload->display_errors());

                        echo json_encode(array("success" => false, "msg" => $image_error['error']));
                        exit;
                    } else {
                        $image_data = array('upload_data' => $this->upload->data());
                        //function for upload file in live bucket
                        $full_path = $image_data['upload_data']['full_path'];                            
                        $resultUpload = set_s3_upload_file($full_path,$_POST['document_type_id_hidden']);
                        $step3_document = $image_data['upload_data']['file_name'];
                        $data['uploaded_document_file'] = $step3_document;
                        $upload = true;
                    }
                }

                if ($upload) {
                    // get document selected in previous screen 
                    $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                    $main_table = array("tbl_document_container_mapping as dcm", array("dt.*"));
                    $join_tables = array(
                        array("", "tbl_document_type as dt", "dt.document_type_id = dcm.document_type_id", array("dt.*")),
                    );
                    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                    $document_type_details = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                    $document_name = $document_type_details[0]['document_prefix_name'] . '00001';

                    if ($document_name) {
                        $condition = "document_type_id = '" . $document_type_details[0]['document_type_id'] . "' ";
                        $uploaded_document_data = $this->common->getDataLimit("tbl_document_uploaded_files", '*', $condition, "document_uploaded_files_id", "desc", '1');
                    }

                    if ($uploaded_document_data) {
                        // echo $uploaded_document_data[0]['uploaded_document_number'];
                        $uploaded_document_name = ++$uploaded_document_data[0]['uploaded_document_number'];
                    } else {
                        $uploaded_document_name = $document_name;
                    }

                    // get container status for document uploaded 
                    $condition = " document_type_id = '" . $document_type_details[0]['document_type_id'] . "' ";
                    $getContainerStatus = $this->common->getData('tbl_container_status', '*', $condition);

                    if ($document_name) {
                        if (!empty($_POST['selectedContainer']) && isset($_POST['selectedContainer'])) {


                            foreach ($_POST['selectedContainer'] as $key => $value) {
                                $data['container_id'] = $value;
                                $data['document_type_id'] = $document_type_details[0]['document_type_id'];
                                $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                $data['uploaded_document_number'] = $uploaded_document_name;
                                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['updated_on'] = date("Y-m-d H:i:s");
                                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['created_on'] = date("Y-m-d H:i:s");
                                // print_r($data);
                                $result = $this->common->insertData("tbl_document_uploaded_files", $data, "1");

                                // insert record in tbl_container_uploaded_document_status for maintain the statsus for container with respective to document uploaded 
                                $doc_status_data = array();
                                $doc_status_data['container_id'] = $value;
                                $doc_status_data['document_type_id'] = $document_type_details[0]['document_type_id'];
                                $doc_status_data['sub_document_type_id'] = 0;
                                $doc_status_data['document_type_squence'] = $document_type_details[0]['document_type_squence'];
                                $doc_status_data['status'] = 'Unchecked';
                                $doc_status_data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1");

                                $condition = " container_id = '" . $value . "' ";
                                $getContainerRevised_etd = $this->common->getData('tbl_container', 'revised_etd,last_container_status_id', $condition);

                                $condition = " container_status_id = '" . $getContainerRevised_etd[0]['last_container_status_id'] . "' ";
                                $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status', 'status_squence', $condition);
                                $lastContainerStatusSquence = 0;
                                if (!empty($lastContainerStatusSquenceData)) {
                                    $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                }

                                //HBL document upload check
                                if ($document_type_details[0]['document_type_id'] == 5) {
                                    if ($getContainerStatus && !empty($getContainerRevised_etd[0]['revised_etd'])) {
                                        $ContainerStatus = array();
                                        $ContainerStatus['container_id'] = $value;
                                        $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                        $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                        $ContainerStatus['document_date'] = date("Y-m-d");
                                        $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                        if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                            //last status in container table
                                            $containerStatusUpdate = array();
                                            $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                            $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                            $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                            $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $value));
                                        }
                                    }
                                } else {
                                    if ($getContainerStatus) {
                                        $ContainerStatus = array();
                                        $ContainerStatus['container_id'] = $value;
                                        $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                        $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                        $ContainerStatus['document_date'] = date("Y-m-d");
                                        $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                        if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                            //last status in container table
                                            $containerStatusUpdate = array();
                                            $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                            $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                            $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                            $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $value));
                                        }
                                    }
                                }
                            }

                            if ($result) {
                                echo json_encode(array("success" => true, "msg" => "Document added to container!", 'type' => '', 'step4skip' => $step4skip));
                                exit;
                            } else {
                                echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                                exit;
                            }
                        } else {
                            echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                            exit;
                        }
                    } else {
                        echo json_encode(array("success" => false, "msg" => "Selected Document Not found!"));
                        exit;
                    }
                } else {
                    echo json_encode(array("success" => false, "msg" => "Uploaded file is not proper !"));
                    exit;
                }
            }
        }
    }

    public function getSelectedContainerDocument() {
        // echo "<pre>";
        // print_r($_POST);
        // exit;
        if ($_POST['type'] == 'LS') {

            if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {
                $condition = "1=1  AND dluf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.*,group_concat(dluf.uploaded_document_number) as uploaded_document_number"));
                $join_tables = array(
                    array("", "tbl_container as c", "dluf.container_id = c.container_id", array("c.container_number, c.container_id")),
                    array("", "tbl_document_type as dt", "dt.document_type_id = dluf.document_type_id", array("dt.document_prefix_name")),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'dluf.container_id');
                $result['getSelectedLsContainerDocument'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                // echo "<pre>";
                // print_r($result);
                // exit;
                if ($result) {
                    $html_container = '';
                    $html_uploaded = '';
                    $document_prefix_name = '';
                    foreach ($result['getSelectedLsContainerDocument'] as $key => $value) {
                        $document_prefix_name = $value['document_prefix_name'];
                        $html_container .= ' <div class="ud-list-single">
                                        <label class="container-checkbox">' . $value['container_number'] . '
                                        </label>
                                    </div>';

                        $html_uploaded .= '<div class="ud-list-single">
                                            <label class="container-checkbox">' . str_replace(',', ' ', $value['uploaded_document_number']) . '
                                            </label>
                                        </div>';
                    }

                    echo json_encode(array("success" => true, "msg" => "", 'html_container' => $html_container, 'html_uploaded' => $html_uploaded, 'document_prefix_name' => $document_prefix_name));
                    exit;
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "Current session id not found "));
                    exit;
                }
            } else {
                echo json_encode(array("success" => false, "msg" => "Current session id not found "));
                exit;
            }
        } else {

            if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {
                $condition = "1=1  AND duf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_document_uploaded_files as duf", array("duf.*"));
                $join_tables = array(
                    array("", "tbl_container as c", "duf.container_id = c.container_id", array("c.container_number, c.container_id")),
                    array("", "tbl_document_type as dt", "dt.document_type_id = duf.document_type_id", array("dt.document_prefix_name")),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                $result['getSelectedContainerDocument'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                if ($result) {
                    $html_container = '';
                    $html_uploaded = '';
                    $document_prefix_name = '';
                    foreach ($result['getSelectedContainerDocument'] as $key => $value) {
                        $document_prefix_name = $value['document_prefix_name'];

                        $html_container .= ' <div class="ud-list-single">
                                        <label class="container-checkbox">' . $value['container_number'] . '
                                        </label>
                                    </div>';

                        $html_uploaded .= '<div class="ud-list-single">
                                            <label class="container-checkbox">' . str_replace(',', ' ', $value['uploaded_document_number']) . '
                                            </label>
                                        </div>';
                    }

                    echo json_encode(array("success" => true, "msg" => "", 'html_container' => $html_container, 'html_uploaded' => $html_uploaded, 'document_prefix_name' => $document_prefix_name));
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "Current session id not found "));
                    exit;
                }
            } else {
                echo json_encode(array("success" => false, "msg" => "Current session id not found "));
                exit;
            }
        }
    }

    public function getSelectedCustomField() {

        // echo "<pre>";
        // print_r($_POST);
        // exit;
        $formView = '';
        if ($_POST['type'] == 'invoice') {

            if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {
                // get partner name 
                $condition = "1=1  AND iuf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_invoice_uploaded_files as iuf", array("iuf.*"));
                $join_tables = array(
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = iuf.business_partner_id", array('bp.alias')),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, '', 'iuf.current_session_id');
                $result['getCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                // get sku data
                $condition = "status = 'Active' ";
                $result['sku_details'] = $this->common->getData('tbl_sku', 'sku_id,sku_number', $condition);

                if ($result) {
                    $customField = $this->load->view("invoice-customer-field", $result, true);
                }

                if ($result) {
                    $formView = $this->load->view("invoice-form-view", $result, true);
                }

                echo json_encode(array("success" => true, "msg" => "Custom field data found ", 'customField' => $customField, 'formView' => $formView));
                exit;
            } else {
                echo json_encode(array("success" => false, "msg" => "Session data in found"));
                exit;
            }
        } else {
            if ($_POST['type'] == 'LS') {

                if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {
                    $condition = "1=1  AND dluf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' AND cfs.status = 'Active' ";
                    $main_table = array("tbl_custom_field_structure as cfs", array("cfs.*"));
                    $join_tables = array(
                        array("", "tbl_document_ls_uploaded_files as dluf", "dluf.document_type_id = cfs.document_type_id", array('group_concat(DISTINCT(dluf.uploaded_document_number)) as uploaded_document_number,uploaded_document_file ')),
                        array("left", "tbl_sub_document_type as sdt", "sdt.document_type_id = dluf.document_type_id", array('group_concat(DISTINCT(sdt.sub_document_type_id)) as sub_document_type_id ')),
                    );

                    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'custom_field_structure_id');
                    $result['getSelectedCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                    $result['containersSKUData'] = $this->getcontainersSKUData($_POST['type'], $_SESSION['mro_session'][0]['current_session_id']);
                    $customField = '';
                    $formView = '';


                    // get the manufacturer list 
                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category ='Manufacturer'  ";
                    $result['manufacturer'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number', $condition);

                    // get the supplier name  
                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Distributor','Manufacturer')  ";
                    $result['supplier_name'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number', $condition);


                    //  get hbl_number 
                    $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "'  ";
                    $get_container_id = $this->common->getData('tbl_document_ls_uploaded_files', 'container_id,document_type_id', $condition);

                    //supplier_update
                    $result['supplier_update'] = '';
                    if ($get_container_id) {
                        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND supplier_id IS NOT NULL";
                        $supplier_update = $this->common->getData('tbl_custom_dynamic_data', 'supplier_id,supplier_contactperson,supplier_contactnumber', $condition, 'custom_dynamic_data_id', 'desc');
                        if (!empty($supplier_update)) {
                            $result['supplier_update'] = $supplier_update;
                        }
                    }

                    $result['all_freight_forwarder'] = array();

                    if ($get_container_id) {
                        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND hbl_number IS NOT NULL";
                    //    if(isset($result['getSelectedCustomField'][0]['document_type_id'])){
                    //        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND hbl_number IS NOT NULL";
                    //    } 
                        $result['get_hbl_number'] = $this->common->getData('tbl_custom_dynamic_data', 'hbl_number', $condition, 'custom_dynamic_data_id', 'desc');
                        
                        $condition = "1=1  AND c.container_id = " . $get_container_id[0]['container_id'] . " ";
                        $main_table = array("tbl_container as c", array());
                        $join_tables = array(
                            array("", "tbl_order as o", "o.order_id = c.order_id", array('o.freight_forwarder_id')),
                        );
                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
                        $orderData = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                        if (!empty($orderData)) {
                            $condition = "status = 'Active' And business_partner_id IN (" . $orderData[0]['freight_forwarder_id'] . ")  ";
                            $result['all_freight_forwarder'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,alias', $condition);
                        }
                    }

                    // get Port of Loading;
                    $condition = "status = 'Active' ";
                    $result['all_pol'] = $this->common->getData('tbl_pol', '*', $condition);
                    // get Port of Discharge;
                    $condition = "status = 'Active' ";
                    $result['all_pod'] = $this->common->getData('tbl_pod', '*', $condition);
                    //get liner name
                    $condition = "status = 'Active' ";
                    $result['all_liner'] = $this->common->getData('tbl_liner', '*', $condition);

                    // get the supplier name  
                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Freight Forwarder')  ";
                    $result['all_ff'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,alias', $condition);

                    // echo "<pre>";
                    // print_r($result['get_hbl_number']);
                    // exit;

                    if (!empty($result['getSelectedCustomField'][0]['document_type_id'])) {
                        if ($result['getSelectedCustomField'][0]['document_type_id'] == 4) {
                            $customField = $this->load->view("custom-fcr", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 5) {
                            $customField = $this->load->view("custom-hbl", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 1) {
                            $customField = $this->load->view("custom-ins", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 10) {
                            $customField = $this->load->view("custom-arrival", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 7) {
                            $customField = $this->load->view("7501", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 8) {
                            $customField = $this->load->view("custom-do", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 9) {
                            $customField = $this->load->view("custom-do", $result, true);
                        }
                    }

                    // to get the  form view for both internal n external 
                    if ($result) {

                        $condition = "1=1  AND dluf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                        $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.*"));
                        $join_tables = array(
                            array("", "tbl_document_type as dt", "dt.document_type_id = dluf.document_type_id", array('dt.document_type_name as document_name')),
                            array("left", "tbl_sub_document_type as sdt", "sdt.document_type_id = dt.document_type_id && sdt.sub_document_type_id = dluf.sub_document_type_id", array('sdt.sub_document_type_name as sub_document_name')),
                        );

                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'document_type_id,sub_document_type_id');
                        $result['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                        // echo "<pre>";
                        // print_r( $result['formView']);
                        // exit;
                        $formView = $this->load->view("document-form-view", $result, true);
                    }

                    echo json_encode(array("success" => true, "msg" => "Custom field data found ", 'customField' => $customField, 'formView' => $formView));
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "Document not selected for it"));
                    exit;
                }
            } else {

                if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {
                    $condition = "1=1  AND duf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' AND cfs.status = 'Active' ";
                    $main_table = array("tbl_custom_field_structure as cfs", array("cfs.*"));
                    $join_tables = array(
                        array("", "tbl_document_uploaded_files as duf", "duf.document_type_id = cfs.document_type_id", array(' group_concat(DISTINCT(duf.uploaded_document_number)) as uploaded_document_number')),
                    );

                    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'custom_field_structure_id');
                    $result['getSelectedCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result                   

                    $result['containersSKUData'] = $this->getcontainersSKUData($_POST['type'], $_SESSION['mro_session'][0]['current_session_id']);


                    // get the manufacturer list 
                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category ='Manufacturer'  ";
                    $result['manufacturer'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number,alias', $condition);

                    // get the supplier name  
                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Distributor','Manufacturer')  ";
                    $result['supplier_name'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number,alias', $condition);

                    //get hbl_number 
                    $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "'  ";
                    $get_container_id = $this->common->getData('tbl_document_uploaded_files', 'container_id,document_type_id', $condition);

                    //supplier_update
                    $result['supplier_update'] = '';
                    if ($get_container_id) {
                        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND supplier_id IS NOT NULL";
                        $supplier_update = $this->common->getData('tbl_custom_dynamic_data', 'supplier_id,supplier_contactperson,supplier_contactnumber', $condition, 'custom_dynamic_data_id', 'desc');
                        if (!empty($supplier_update)) {
                            $result['supplier_update'] = $supplier_update;
                        }
                    }
                    //print_r($result['supplier_update']);die;

                    if ($get_container_id) {
                        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND document_type_id =".$result['getSelectedCustomField'][0]['document_type_id']." AND date_of_inspection IS NOT NULL";
                        $result['get_date_of_inspection'] = $this->common->getData('tbl_custom_dynamic_data', 'date_of_inspection', $condition, 'custom_dynamic_data_id', 'desc');
                    }

                    if ($get_container_id) {
                        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND document_type_id =".$result['getSelectedCustomField'][0]['document_type_id']." AND place_of_inspection IS NOT NULL";
                        $result['get_place_of_inspection'] = $this->common->getData('tbl_custom_dynamic_data', 'place_of_inspection', $condition, 'custom_dynamic_data_id', 'desc');
                    }

                    // get Port of Loading;
                    $condition = "status = 'Active' ";
                    $result['all_pol'] = $this->common->getData('tbl_pol', '*', $condition);
                    // get Port of Discharge;
                    $condition = "status = 'Active' ";
                    $result['all_pod'] = $this->common->getData('tbl_pod', '*', $condition);
                    //get liner name
                    $condition = "status = 'Active' ";
                    $result['all_liner'] = $this->common->getData('tbl_liner', '*', $condition);

                    // echo "<pre>";
                    // print_r($result['containersSKUData']);
                    // exit;

                    $customField = '';
                    if ($result) {
                        if ($result['getSelectedCustomField'][0]['document_type_id'] == 3) {
                            $customField = $this->load->view("custom-fri", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 2) {
                            $customField = $this->load->view("custom-ls", $result, true);
                        }
                    }

                    // to get the  form view for both internal n external 
                    if ($result) {

                        $condition = "1=1  AND duf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                        $main_table = array("tbl_document_uploaded_files as duf", array("duf.*"));
                        $join_tables = array(
                            array("", "tbl_document_type as dt", "dt.document_type_id = duf.document_type_id", array('dt.document_type_name as document_name')),
                        );

                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'document_type_id');
                        $result['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                        $formView = $this->load->view("document-form-view", $result, true);
                    }

                    echo json_encode(array("success" => true, "msg" => "Custom field data found ", 'customField' => $customField, 'formView' => $formView));
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "Document not selected for it"));
                    exit;
                }
            }
        }
    }

    public function submitFormStep5Details() {
        //     echo "<pre>";
        //     print_r($_POST);
        //     die;
        //    echo !empty($_POST['eta'][10]) ? date("Y-m-d", strtotime($_POST['eta'][10])) : NULL;die;
        //     code by ravendra this is use for back and then save record functionality.
        $this->common->deleteRecord("tbl_custom_field_data_submission", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_custom_field_invoice_data_submission", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_custom_field_invoice_data_submission_sku", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");


        // for invoice custom field data store in tbl_invoice_custom_field_response_store
        if (!empty($_POST['business_partner_id']) && isset($_POST['business_partner_id'])) {
            $data = array();

            if ($_POST['invoice_file_name']) {
                $renamed_file_name = explode('.', $_POST['invoice_file_name']);
                $file_name = $renamed_file_name[0];
            }
            $newinvoice_name = $file_name . '.pdf';
            $data['business_partner_id'] = $_POST['business_partner_id'];
            $data['invoice_file_name'] = $newinvoice_name;
            $data['invoice_value'] = $_POST['invoice_value'];
            $data['invoice_date'] = !empty($_POST['invoice_date']) ? date("Y-m-d", strtotime($_POST['invoice_date'])) : NULL;
            $data['invoice_status'] = $_POST['invoice_status'];
            $data['paid_date'] = !empty($_POST['paid_date']) ? date("Y-m-d", strtotime($_POST['paid_date'])) : NULL;
            // $data['sku_id']=  $_POST['sku_id'] ;
            // $data['quantity']=  $_POST['quantity'] ;
            // $data['price_per_unit']=  $_POST['price_per_unit'] ;
            $data['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['created_on'] = date("Y-m-d H:i:s");

            $result = $this->common->insertData("tbl_custom_field_invoice_data_submission", $data, "1");

            if ($result) {
                $custom_field_invoice_data_submission_id = $result;
                if (!empty($_POST['sku']) && isset($_POST['sku'])) {

                    foreach ($_POST['sku'] as $key => $value) {
                        $data = array();
                        $data['sku_id'] = $value;
                        $data['custom_field_invoice_data_submission_id'] = $custom_field_invoice_data_submission_id;
                        $data['quantity'] = $_POST['quantity'][$key];
                        $data['price_per_unit'] = $_POST['price_per_unit'][$key];
                        $data['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_field_invoice_data_submission_sku", $data, "1");
                    }
                }
                // check the pdf with old name id exist then remove it and move that file with new name 
                $old_invoice_file_name = 'container_document/' . $_POST['old_invoice_file_name'];
                //if (file_exists($old_invoice_file_name)) {
                    //$upload_path = DOC_ROOT_FRONT . "/container_document";
                    //rename($old_invoice_file_name, "container_document/$newinvoice_name");
                    // update the new file in tbl_invoice_uploaded_files
                    $data = array();
                    $data['uploaded_invoice_file'] = $_POST['old_invoice_file_name']; //$newinvoice_name
                    $condition = " current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                    $result = $this->common->updateData("tbl_invoice_uploaded_files", $data, $condition);
                    if ($result) {
                        echo json_encode(array("success" => true, "msg" => "Invoice successfully uploaded in container!", "type" => "invoice"));
                        exit;
                    } else {
                        echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                        exit;
                    }
                    //    } else {
                    //        echo "The file $old_invoice_file_name does not exist!!";
                    //    }
                exit;
            }
        }

        // cutomer field compulsory removed 
        if (!empty($_POST['custom_field_structure_id']) && isset($_POST['custom_field_structure_id'])) {
            $actualDateofArrival = '';
            $uploaded_document_number = explode(',', $_POST['uploaded_document_number']);
            if (!empty($_POST['sub_document_type_id']) && isset($_POST['sub_document_type_id'])) {
                $sub_document_type_cnt = explode(',', $_POST['sub_document_type_id']);

                foreach ($sub_document_type_cnt as $subDocumentkey => $subDocumentval) {
                    foreach ($_POST['custom_field_structure_id'] as $key => $value) { 
                        $data['custom_field_structure_id'] = $value;
                        $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                        $data['uploaded_document_number'] = $uploaded_document_number[$subDocumentkey];
                        $data['document_type_id'] = (!empty($_POST['document_type_id']) ? $_POST['document_type_id'] : " ");
                        $data['sub_document_type_id'] = $subDocumentval;
                        $data['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['created_on'] = date("Y-m-d H:i:s");
                        $result = $this->common->insertData("tbl_custom_field_data_submission", $data, "1");
                    }
                }
            } else {

                foreach ($_POST['custom_field_structure_id'] as $key => $value) {
                    //Actual date of Arrival check for DO upload
                    if(isset($_POST['dateOfArrival'])){
                        if($value == 92){
                            $actualDateofArrival = (!empty($_POST[$value]) ? date('Y-m-d', strtotime($_POST[$value])) : "");
                        }
                    }
                    $data['custom_field_structure_id'] = $value;
                    $data['uploaded_document_number'] = $uploaded_document_number[0];
                    $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                    $data['document_type_id'] = (!empty($_POST['document_type_id']) ? $_POST['document_type_id'] : " ");
                    $data['sub_document_type_id'] = (!empty($_POST['sub_document_type_id']) ? $_POST['sub_document_type_id'] : "0");
                    $data['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);
                    $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['updated_on'] = date("Y-m-d H:i:s");
                    $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['created_on'] = date("Y-m-d H:i:s");
                    $result = $this->common->insertData("tbl_custom_field_data_submission", $data, "1");
                }
            }

            // start  code modified by shiv on 21-10-21 for document type FRI N LS
            if (!empty($_POST['container_sku_id']) && isset($_POST['container_sku_id'])) {
                foreach ($_POST['container_sku_id'] as $key => $container_val) {
                    // $key == container id here
                    foreach ($container_val as $sku_key => $sku_val) {
                        $data1 = array();
                        $data1['quantity'] = $_POST['container_sku_qty'][$key][$sku_key];
                        $condition = "container_sku_id = " . $sku_key . " AND container_id = " . $key . " ";
                        $this->common->updateData("tbl_container_sku", $data1, $condition);

                        $data_manu = array();
                        $data_manu['manufacturer_id'] = $_POST['manufacturer'][$key][$sku_key];
                        $condition = "sku_id = " . $sku_key . "  ";
                        $this->common->updateData("tbl_sku", $data_manu, $condition);
                    }
                }
            }

            if (!empty($_POST['supplier_name']) && isset($_POST['supplier_name'])) {
                foreach ($_POST['supplier_name'] as $supp_key => $supp_val) {
                    foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                        $data_supp = array();
                        $data_supp['supplier_id '] = $supp_val;
                        $data_supp['supplier_contactperson'] = $_POST['contactpersonname'][$supp_key];
                        $data_supp['supplier_contactnumber'] = $_POST['contactpersonnumber'][$supp_key];
                        // $data_supp['sub_document_type_id'] = explode(',',$_POST['sub_document_type_id']);
                        $data_supp['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);

                        $condition = " container_id = " . $container_id . " AND document_type_id =".$_POST['document_type_id']." ";
                        $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                        if (!empty($dynamic_data)) {
                            $condition = " container_id = " . $dynamic_data[0]['container_id'] . "  AND document_type_id =".$_POST['document_type_id']." ";
                            $data_supp['updated_on'] = date("Y-m-d H:i:s");
                            $data_supp['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition);
                        } else {
                            $data_supp['container_id'] = $container_id;
                            $data_supp['document_type_id'] = $_POST['document_type_id'];
                            $data_supp['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data_supp['updated_on'] = date("Y-m-d H:i:s");
                            $data_supp['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data_supp['created_on'] = date("Y-m-d H:i:s");
                            $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                        }
                    }
                }
            }

            if (!empty($_POST['lot_number']) && isset($_POST['lot_number'])) {
                foreach ($_POST['lot_number'] as $seal_key => $seal_val) {
                    $data_lot = array();
                    $data_lot['lot_number'] = $_POST['lot_number'][$seal_key];
                    $condition = " container_id = " . $seal_key . " ";
                    $this->common->updateData("tbl_container", $data_lot, $condition);
                }
            }

            if (!empty($_POST['sgs_seal']) && isset($_POST['sgs_seal'])) {
                foreach ($_POST['sgs_seal'] as $seal_key => $seal_val) {
                    $data_seal = array();
                    $data_seal['sgs_seal'] = $seal_val;
                    $data_seal['shipping_container'] = $_POST['shipping_container'][$seal_key];
                    $data_seal['ff_seal'] = $_POST['ff_seal'][$seal_key];
                    if(isset($_POST['terminal'])){
                        $data_seal['terminal'] = $_POST['terminal'][$seal_key];
                    } 
                    $condition = " container_id = " . $seal_key . " ";
                    $this->common->updateData("tbl_container", $data_seal, $condition);
                }
            }

            // end  code modified by shiv on 21-10-21 for document type FRI N LS
            // for HBL N FCR document type 
            if (!empty($_POST['tbl_container_id']) && isset($_POST['tbl_container_id'])) {
                foreach ($_POST['tbl_container_id'] as $key => $value) {
                    $data2['freight_forwarder_id'] = $_POST['freight_forwarder_id'][$key];
                    $data2['liner_name'] = $_POST['liner_name'][$key];
                    $data2['pol'] = $_POST['pol'][$key];
                    $data2['pod'] = $_POST['pod'][$key];
                    $data2['etd'] = !empty($_POST['etd'][$key]) ? date("Y-m-d", strtotime($_POST['etd'][$key])) : NULL;
                    $data2['revised_etd'] = !empty($_POST['revised_etd'][$key]) ? date("Y-m-d", strtotime($_POST['revised_etd'][$key])) : NULL;
                    $data2['ett'] = $_POST['ett'][$key];
                    $data2['vessel_name'] = $_POST['vessel_name'][$key];

                    foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                        $condition = " container_id = " . $container_id . " ";
                        $this->common->updateData("tbl_container", $data2, $condition);
                    }
                }
            }

            if (!empty($_POST['revised_eta']) && isset($_POST['revised_eta'])) {
                foreach ($_POST['revised_eta'] as $eta_key => $eta_val) {
                    $data_eta = array();
                    $data_eta['etd'] = !empty($_POST['etd'][$eta_key]) ? date("Y-m-d", strtotime($_POST['etd'][$eta_key])) : NULL;
                    $data_eta['eta'] = !empty($_POST['eta'][$eta_key]) ? date("Y-m-d", strtotime($_POST['eta'][$eta_key])) : NULL;
                    $data_eta['revised_eta'] = !empty($eta_val) ? date("Y-m-d", strtotime($eta_val)) : NULL;
                    $condition = " container_id = " . $eta_key . " ";
                    //print_r($data_eta); die; 
                    foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                        $condition = " container_id = " . $container_id . " ";
                        $this->common->updateData("tbl_container", $data_eta, $condition);
                    }
                }
            }
            
            //update actual date of arrival when upload DO then update container revised eta field
            if(!empty($actualDateofArrival)){
                $data_revised_eta = array(); 
                $data_revised_eta['revised_eta'] = !empty($actualDateofArrival) ? $actualDateofArrival : NULL;
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $condition = " container_id = " . $container_id . " ";
                    $this->common->updateData("tbl_container", $data_revised_eta, $condition);
                }
            }

            // start to remove unchecked container id from mappings table in ls n without ls code by shiv on 6-10-21

            if (!empty($_POST['tbl_container_id']) && isset($_POST['tbl_container_id']) && !empty($_POST['selected_container_id']) && isset($_POST['selected_container_id'])) {
                $containerToBeDeleted = array_diff($_POST['tbl_container_id'], $_POST['selected_container_id']);
                if ($containerToBeDeleted) {
                    foreach ($containerToBeDeleted as $key => $value) {
                        # delete the record which container is unselected at the last step5 
                        $condition = "container_id = '" . $value . "' AND document_type_id = '" . $_POST['document_type_id'] . "' AND current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                        $this->common->deleteRecord('tbl_document_container_mapping', $condition);
                        $this->common->deleteRecord('tbl_document_ls_container_mapping', $condition);
                        $this->common->deleteRecord('tbl_document_uploaded_files', $condition);
                        $this->common->deleteRecord('tbl_document_ls_uploaded_files', $condition);
                        $this->common->deleteRecord('tbl_container_uploaded_document_status', $condition);
                    }
                }
            }
            //     end  to remove unchecked container id from mappings table in ls n without ls code by shiv on 6-10-21  
            //      get mapped container data
            //     $_SESSION['mro_session'][0]['current_session_id']) 
            //    $condition = "current_session_id = '".$_SESSION['mro_session'][0]['current_session_id']."' AND document_type_id = '".$_POST['document_type_id']."' ";
            //    $get_mapped_container = $this->common->getData('tbl_document_ls_container_mapping','container_id',$condition);
            //     for hbl 
            if (!empty($_POST['hbl_number']) && isset($_POST['hbl_number'])) {
                //    if(!empty($get_mapped_container) && isset($get_mapped_container)){
                //        foreach ($get_mapped_container as $hbl_key => $hbl_val) { 
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $data_hbl = array();
                    $data_hbl['hbl_number'] = $_POST['hbl_number'];
                    // $data_hbl['sub_document_type_id'] = explode(',',$_POST['sub_document_type_id']);
                    $data_hbl['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);

                    $condition = " container_id = " . $container_id . " AND document_type_id = ".$_POST['document_type_id']." ";
                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                    if (!empty($dynamic_data)) {
                        $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$_POST['document_type_id']." ";
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $this->common->updateData("tbl_custom_dynamic_data", $data_hbl, $condition);
                    } else {
                        $data_hbl['container_id'] = $container_id;
                        $data_hbl['document_type_id'] = $_POST['document_type_id'];
                        $data_hbl['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_dynamic_data", $data_hbl, "1");
                    }
                }
                //        }
                //    } 
            }

            if (!empty($_POST['date_of_inspection']) && isset($_POST['date_of_inspection'])) {
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $data_hbl = array();
                    $data_hbl['date_of_inspection'] = !empty($_POST['date_of_inspection']) ? date("Y-m-d", strtotime($_POST['date_of_inspection'])) : NULL;
                    $data_hbl['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);

                    $condition = " container_id = " . $container_id . " AND document_type_id =".$_POST['document_type_id']." ";
                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                    if (!empty($dynamic_data)) {
                        $condition = " container_id = " . $dynamic_data[0]['container_id'] . "  AND document_type_id =".$_POST['document_type_id']." ";
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $this->common->updateData("tbl_custom_dynamic_data", $data_hbl, $condition);
                    } else {
                        $data_hbl['container_id'] = $container_id;
                        $data_hbl['document_type_id'] = $_POST['document_type_id'];
                        $data_hbl['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_dynamic_data", $data_hbl, "1");
                    }
                }
            }

            if (!empty($_POST['place_of_inspection']) && isset($_POST['place_of_inspection'])) {
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $data_hbl = array();
                    $data_hbl['place_of_inspection'] = $_POST['place_of_inspection'];
                    $data_hbl['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);

                    $condition = " container_id = " . $container_id . " AND document_type_id =".$_POST['document_type_id']." ";
                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                    if (!empty($dynamic_data)) {
                        $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$_POST['document_type_id']." ";
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $this->common->updateData("tbl_custom_dynamic_data", $data_hbl, $condition);
                    } else {
                        $data_hbl['container_id'] = $container_id;
                        $data_hbl['document_type_id'] = $_POST['document_type_id'];
                        $data_hbl['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_dynamic_data", $data_hbl, "1");
                    }
                }
            }

            //code update date of document in container status log 
            $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND cfds.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
            $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));
            $join_tables = array(
                array("", "tbl_custom_field_structure as cfs", "cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, '', '');
            $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
            if (!empty($dateOfDocumentData)) {
                $data_doc_date = array();
                $condition = " current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $data_doc_date['document_date'] = date("Y-m-d", strtotime($dateOfDocumentData[0]['custom_field_structure_value']));
                $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);
            }

            if ($result) {
                echo json_encode(array("success" => true, "msg" => "Document successfully uploaded in container!", "type" => "document"));
                exit;
            } else {
                echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                exit;
            }
        } else {
            // send response without storing data into tble  as we are doing above
            echo json_encode(array("success" => true, "msg" => "Document successfully uploaded in container!", "type" => "document"));
            exit;
        }
    }

    public function deleteUncompletedUpload() {
        $condition = "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ";
        $containerIds = $this->common->getData('tbl_container_status_log', 'container_id', $condition);
        if (!empty($containerIds)) {
            foreach ($containerIds as $value) {
                $containerStatusData = $this->container_detailsmodel->getLastContainerStatus($value['container_id'], $_SESSION['mro_session'][0]['current_session_id']);
                if (!empty($containerStatusData)) {
                    //last status in container table
                    $containerStatusUpdate = array();
                    $containerStatusUpdate['last_container_status_id'] = $containerStatusData[0]['container_status_id'];
                    $containerStatusUpdate['last_status_by'] = $containerStatusData[0]['created_by'];
                    $containerStatusUpdate['last_status_date'] = date('Y-m-d', strtotime($containerStatusData[0]['created_on']));
                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $value['container_id']));
                }
            }
        }

        $this->common->deleteRecord("tbl_document_ls_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_document_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_invoice_ls_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_container_uploaded_document_status", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_custom_field_data_submission", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_custom_field_invoice_data_submission", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_custom_field_invoice_data_submission_sku", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_document_ls_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_document_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_invoice_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_container_uploaded_document_status", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");

        $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
        $statusLog = $this->common->getData("tbl_container_status_log", 'status_log_id', $condition);
        if (!empty($statusLog)) {
            $this->common->deleteRecord("tbl_container_status_log", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        }

        echo json_encode(array("success" => true, "msg" => "unwanted mapping deleted"));
        exit;
    }

    public function getLsListing() {
        $container_id = $_POST['container_id'];
        $condition = " 1=1 AND document_type_id = '2' AND container_id = " . $container_id . " ";
        $rs = $this->common->Fetch('tbl_document_uploaded_files', 'uploaded_document_number', $condition, '', 'uploaded_document_number');
        $result['LsNumberLisitng'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
        // echo "<pre>";
        // print_r($result);
        // exit;

        $html = "";
        $rightSideMsg = '';
        $rightSideMsg = '<div class="sd-right" id="containterListingForOtherDocument">
                            <p class="select-message">Choose LS from the left in order to choose containers
                            </p>
                        </div>';
        if ($result) {
            $html = $this->load->view("Ls-ListingView", $result, true);
            echo json_encode(array("success" => true, "msg" => "Ls Listing found", 'html' => $html, 'rightSideMsg' => $rightSideMsg));
            exit;
        } else {
            echo json_encode(array("success" => false, "msg" => "NO LS listing in created so far", 'html' => $html, "rightSideMsg" => $rightSideMsg));
            exit;
        }
    }

    public function getContainerListing() {
        $container_id = $_POST['container_id'];
        //$condition = "status = 'Active' AND container_id not in (select distinct(container_id) as container_id from tbl_document_uploaded_files where document_type_id = ".$_POST['document_type_id']." )";
        $condition = "status = 'Active' AND container_id =" . $container_id . " ";
        $result['containerListing'] = $this->common->getData('tbl_container', 'container_number,container_id', $condition);
        $html = "";
        $rightSideMsg = '';
        $rightSideMsg = '<div class="sd-right" id="containterListingForOtherDocument">
                            <p class="select-message">Choose containers to upload documents</p>
                        </div>';
        if ($result) {
            $html = $this->load->view("containerListingView", $result, true);
            echo json_encode(array("success" => true, "msg" => "Countainer Listing found", 'html' => $html, 'rightSideMsg' => $rightSideMsg));
            exit;
        } else {
            echo json_encode(array("success" => false, "msg" => "NO counter listing in created so far", 'html' => $html, "rightSideMsg" => $rightSideMsg));
            exit;
        }
    }

    public function getLSContainerListing() {
        $container_id = $_POST['container_id'];
        if (!empty($_POST['document_type_id']) && isset($_POST['document_type_id'])) {
            $condition = "1=1  AND duf.uploaded_document_number = '" . $_POST['val'] . "' AND duf.container_id = " . $container_id . " ";
        } else {
            $condition = "1=1  AND duf.uploaded_document_number = '" . $_POST['val'] . "'  ";
        }

        $main_table = array("tbl_document_uploaded_files as duf", array("duf.uploaded_document_number"));
        $join_tables = array(
            array("", "tbl_container as c", "duf.container_id = c.container_id", array("c.container_number,c.container_id")),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
        $result['LscontainerListing'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
        // echo "<pre>";
        // print_r($result);
        // exit;
        $html = '';
        if ($result) {
            $html = $this->load->view("LscontainerListingView", $result, true);
            echo json_encode(array("success" => true, "msg" => "Countainer Listing found", 'html' => $html));
            exit;
        } else {
            echo json_encode(array("success" => true, "msg" => "Custom field data found ", 'html' => $html));
            exit;
        }
    }

    function getProductDetails() {
        $result['container_id'] = $_GET['container_id'];
        $result['productData'] = $this->container_detailsmodel->getProductDetails($result['container_id']);
        $strtable = $this->load->view("tab-product", $result, true);
        echo json_encode(array('productDetails' => $strtable));
        exit;
    }

    function getAddressDetails() {
        $container_id = $_GET['container_id'];
        //fetch billing and shipping details id from master contact
        $orderData = $this->container_detailsmodel->getAddressDetails($container_id);
        $str = '';
        $custBillData = array();
        $custShipData = array();
        $suppBillData = array();
        $suppShipData = array();
        $customerContactId = 0;
        $supplierContactId = 0;
        $customerContact = '';
        $supplierContact = '';
        if (!empty($orderData)) {
            foreach ($orderData as $value) {
                //fetch customer and supplier billing and shipping details data
                $custBillData = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $value['customer_billing_details_id']));
                $custShipData = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $value['customer_shipping_details_id']));
                $suppBillData = $this->common->getData("tbl_billing_details", "*", array("billing_details_id" => $value['supplier_billing_details_id']));
                $suppShipData = $this->common->getData("tbl_shipping_details", "*", array("shipping_details_id" => $value['supplier_shipping_details_id']));

                //fetch customer and vendor contract 
                $customerDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $value['customer_contract_id']));
                if (!empty($customerDetail)) {
                    $customerContactId = $customerDetail[0]['bp_order_id'];
                    $customerContact = $customerDetail[0]['contract_number'];
                    $customerContactBP = $customerDetail[0]['business_partner_id'];
                }
                $supplierDetail = $this->common->getData("tbl_bp_order", "bp_order_id,business_partner_id,contract_number", array("bp_order_id" => (int) $value['supplier_contract_id']));
                if (!empty($supplierDetail)) {
                    $supplierContactId = $supplierDetail[0]['bp_order_id'];
                    $supplierContact = $supplierDetail[0]['contract_number'];
                    $supplierContactBP = $supplierDetail[0]['business_partner_id'];
                }
            }
        }
        //fetch customer data
        $customerData = array();
        if (!empty($customerContactBP)) {
            $customerData = $this->common->getData("tbl_business_partner", "*", array("business_partner_id" => $customerContactBP));
        }
        //echo '<pre>'; print_r($customerData);die;
        //fetch supplier data
        $supplierData = array();
        if (!empty($supplierContactBP)) {
            $supplierData = $this->common->getData("tbl_business_partner", "*", array("business_partner_id" => $supplierContactBP));
        }

        //customer address details 
        $countryId = (isset($custBillData[0]['country_id'])?$custBillData[0]['country_id']:0);
        $stateId = (isset($custBillData[0]['state_id'])?$custBillData[0]['state_id']:0);
        $cityId = (isset($custBillData[0]['city_id'])?$custBillData[0]['city_id']:0);
        $cust_bill_countyStateCity = $this->getCountryStateCity($countryId, $stateId, $cityId); 
        $cust_bill_address = (isset($custBillData[0]['address_line_1'])?$custBillData[0]['address_line_1']:'') . " " 
                . (isset($custBillData[0]['address_line_2'])?$custBillData[0]['address_line_2']:'') . " " 
                . $cust_bill_countyStateCity . " " . (isset($custBillData[0]['zipcode'])?$custBillData[0]['zipcode']:'');
        
        $countryId = (isset($custShipData[0]['country_id'])?$custShipData[0]['country_id']:0);
        $stateId = (isset($custShipData[0]['state_id'])?$custShipData[0]['state_id']:0);
        $cityId = (isset($custShipData[0]['city_id'])?$custShipData[0]['city_id']:0);
        $cust_ship_countyStateCity = $this->getCountryStateCity($countryId, $stateId, $cityId);
        $cust_ship_address = (isset($custShipData[0]['address_line_1'])?$custShipData[0]['address_line_1']:'') . " " 
                . (isset($custShipData[0]['address_line_2'])?$custShipData[0]['address_line_2']:'') . " " 
                . $cust_ship_countyStateCity . " " . (isset($custShipData[0]['zipcode'])?$custShipData[0]['zipcode']:'');

        //supplier address details
        $countryId = (isset($suppBillData[0]['country_id'])?$suppBillData[0]['country_id']:0);
        $stateId = (isset($suppBillData[0]['state_id'])?$suppBillData[0]['state_id']:0);
        $cityId = (isset($suppBillData[0]['city_id'])?$suppBillData[0]['city_id']:0);
        $supp_bill_countyStateCity = $this->getCountryStateCity($countryId, $stateId, $cityId);     
        $supp_bill_address = (isset($suppBillData[0]['address_line_1'])?$suppBillData[0]['address_line_1']:'') . " " 
                . (isset($suppBillData[0]['address_line_2'])?$suppBillData[0]['address_line_2']:'') . " " 
                . $supp_bill_countyStateCity . " " . (isset($suppBillData[0]['zipcode'])?$suppBillData[0]['zipcode']:'');
        
        $countryId = (isset($suppShipData[0]['country_id'])?$suppShipData[0]['country_id']:0);
        $stateId = (isset($suppShipData[0]['state_id'])?$suppShipData[0]['state_id']:0);
        $cityId = (isset($suppShipData[0]['city_id'])?$suppShipData[0]['city_id']:0); 
        $supp_ship_countyStateCity = $this->getCountryStateCity($countryId, $stateId, $cityId);     
        $supp_ship_address = (isset($suppShipData[0]['address_line_1'])?$suppShipData[0]['address_line_1']:'') . " " 
                . (isset($suppShipData[0]['address_line_2'])?$suppShipData[0]['address_line_2']:'') . " " 
                . $supp_ship_countyStateCity . " " . (isset($suppShipData[0]['zipcode'])?$suppShipData[0]['zipcode']:'');
        
                    
        $url = "bporder/addEdit?text";

        $customerContractStr = '<p class="sit-single-value">' . $customerContact . '</p>';
        $supplierContractStr = '<p class="sit-single-value">' . $supplierContact . '</p>';
        if ($this->privilegeduser->hasPrivilege("BusinessPartnersOrderView")) {
            $customerContractStr = '<p class="sit-single-value"><a href="' . $url . '=' . rtrim(strtr(base64_encode("id=" . $customerContactId), '+/', '-_'), '=') . '&view=1" ><u>' . $customerContact . '</u></a></p>';
            $supplierContractStr = '<p class="sit-single-value"><a href="' . $url . '=' . rtrim(strtr(base64_encode("id=" . $supplierContactId), '+/', '-_'), '=') . '&view=1" ><u>' . $supplierContact . '</u></a></p>';
        }
        $cus_contact_person = (isset($customerData[0]['contact_person'])?$customerData[0]['contact_person']:'');
        $cus_email = (isset($customerData[0]['email'])?$customerData[0]['email']:'');
        $cus_phone = (isset($customerData[0]['phone_number'])?$customerData[0]['phone_number']:'');
        $cus_bill_cont_num = (isset($custBillData[0]['number'])?$custBillData[0]['number']:'');
        $cus_ship_cont_num = (isset($custShipData[0]['number'])?$custShipData[0]['number']:'');
        
        $supp_contact_person = (isset($supplierData[0]['contact_person'])?$supplierData[0]['contact_person']:'');
        $supp_email = (isset($supplierData[0]['email'])?$supplierData[0]['email']:'');
        $supp_phone = (isset($supplierData[0]['phone_number'])?$supplierData[0]['phone_number']:'');
        $supp_bill_cont_num = (isset($suppBillData[0]['number'])?$suppBillData[0]['number']:'');
        $supp_ship_cont_num = (isset($suppShipData[0]['number'])?$suppShipData[0]['number']:'');

        $str .= '<div class="tab-content-wrapper">
                    <div class="sit-wrapper nat-content">
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Business Alias</p>
                                <p class="sit-single-value">' . $customerData[0]['alias'] . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Business Partner Type</p>
                                <p class="sit-single-value">' . $customerData[0]['business_type'] . ', ' . $customerData[0]['business_category'] . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Contact Name</p>
                                <p class="sit-single-value">' . $cus_contact_person . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Email</p>
                                <p class="sit-single-value">' . $cus_email . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Phone</p>
                                <p class="sit-single-value">' . $cus_phone . '</p>
                            </div>
                        </div>
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Billing Address</p>
                                <p class="sit-single-value">' . $cust_bill_address . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Billing Contact</p>
                                <p class="sit-single-value">' . $cus_bill_cont_num . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Address</p>
                                <p class="sit-single-value">' . $cust_ship_address . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Contact</p>
                                <p class="sit-single-value">' . $cus_ship_cont_num . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Customer Contract</p>
                                ' . $customerContractStr . '
                            </div>                      
                        </div>                    
                    </div>
                    <hr class="separator">
                    <div class="sit-wrapper nat-content">
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Vendor Alias</p>
                                <p class="sit-single-value">' . $supplierData[0]['alias'] . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Business Partner Type</p>
                                <p class="sit-single-value">' . $supplierData[0]['business_type'] . ', ' . $supplierData[0]['business_category'] . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Contact Name</p>
                                <p class="sit-single-value">' . $supp_contact_person . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Email</p>
                                <p class="sit-single-value">' . $supp_email . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Phone</p>
                                <p class="sit-single-value">' . $supp_phone . '</p>
                            </div>
                        </div> 
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Billing Address</p>
                                <p class="sit-single-value">' . $supp_bill_address . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Billing Contact</p>
                                <p class="sit-single-value">' . $supp_bill_cont_num . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Address</p>
                                <p class="sit-single-value">' . $supp_ship_address . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Contact</p>
                                <p class="sit-single-value">' . $supp_ship_cont_num . '</p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Vendor Contract</p>
                                ' . $supplierContractStr . '
                            </div>                      
                        </div>                    
                    </div>
                </div>';
        echo json_encode(array('productDetails' => $str));
        exit;
    }              

    //country, state, city name
    function getCountryStateCity($countyId = 0, $stateID = 0, $cityID = 0) {
        $str = ',';
        $cityDetail = $this->common->getData("tbl_city", "city_name", array("city_id" => $cityID));
        if (!empty($cityDetail)) {
            $str = $str . $cityDetail[0]['city_name'] . ",";
        }
        $stateDetail = $this->common->getData("tbl_state", "state_name", array("state_id" => $stateID));
        if (!empty($stateDetail)) {
            $str = $str . " " . $stateDetail[0]['state_name'] . ",";
        }
        $countryDetail = $this->common->getData("tbl_country", "country_name", array("country_id" => $countyId));
        if (!empty($countryDetail)) {
            $str = $str . " " . $countryDetail[0]['country_name'];
        }
        return $str;
    }

    //get invoice details
    function getInvoiceDetails() {
        $container_id = $_GET['container_id'];
        $invoiceData = $this->container_detailsmodel->getInvoiceDetails($container_id);
        $strtr = '';
        $str = '<div class="tab-content-wrapper">';
        if (!empty($invoiceData)) {
            $str .= '<h3 class="form-group-title">Uploaded Invoices</h3>
                    <div class="table-responsive">
                        <table class="table list-table uploaded-invoices-table">
                            <tbody>';
            $cnt = 0;
            foreach ($invoiceData as $value) {
                $paid = "";
                $unpaid = "";
                $partial = "";
                switch ($value['invoice_status']) {
                    case "paid":
                        $paid = "selected";
                        break;
                    case "unpaid":
                        $unpaid = "selected";
                        break;
                    default:
                        $partial = "selected";
                        break;
                }
                $strtr .= '<tr>
                                    <td style="width:150px">
                                        <div class="form-group ">
                                            <input type="text" name="invoice_date[' . $cnt . ']" id="invoice_date' . $cnt . '" value="' . date('d-m-Y', strtotime($value['invoice_date'])) . '" style="width: 122px" class="form-control input-form-mro valid editable-field datepicker date-form-field" readonly>
                                        </div>
                                    </td>
                                    <td style="width:250px">
                                        <div class="form-group">
                                            <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="' . $value['invoice_file_name'] . '" readonly>
                                        </div>
                                    </td>
                                    <td style="width:150px">
                                        ' . $value['alias'] . '
                                    </td>
                                    <td style="width:200px">
                                        <div class="form-group ">
                                            <select name="invoice_status[' . $cnt . ']" id="invoice_status' . $cnt . '" class="select-form-mro editable-field select-large">
                                                <option value="paid" ' . $paid . '>Paid</option>
                                                <option value="unpaid" ' . $unpaid . '>Un-Paid</option>
                                                <!--<option value="partial" ' . $partial . '>Partial</option>-->
                                            </select>
                                        </div>
                                    </td>
                                    <td style="width:150px">
                                        <div class="form-group ">
                                            <input type="text" name="paid_date[' . $cnt . ']" id="paid_date' . $cnt . '" value="' . date('d-m-Y', strtotime($value['paid_date'])) . '" style="width: 122px" class="form-control input-form-mro editable-field valid datepicker date-form-field" readonly>
                                        </div>
                                    </td>
                                    <td style="width:150px">
                                        <div class="form-group">
                                            <input type="text" name="invoice_value[' . $cnt . ']" id="invoice_value' . $cnt . '" placeholder="Enter Contract Number" class="input-form-mro editable-field" value="' . $value['invoice_value'] . '">
                                        </div>
                                    </td>
                                    <td style="width:170px">';
                if ($this->privilegeduser->hasPrivilege("DocumentInvoiceUpdate")) {
                    $strtr .= '<img onclick="showUpdateButton(' . $cnt . ');" src="' . base_url() . 'assets/images/edit-icon.svg" alt="" class="mr8"> ';
                }
                if ($this->privilegeduser->hasPrivilege("DocumentInvoiceView")) {
                    $strtr .= '<button class="btn-grey-mro" onclick="showInvoiceDetails(' . $value['custom_field_invoice_data_submission_id'] . ');">View</button>';
                }
                $strtr .= '</td>
                                    <td style="width:220px">
                                        <div class="cs-status-text" id="cs-status-text' . $cnt . '" style="display:block">
                                            <p class="last-update-title" id="last-update-title' . $cnt . '">Last Updated</p>
                                            <p class="last-update-name" id="last-update-name' . $cnt . '">' . $value['firstname'] . ' ' . substr($value['lastname'], 0, 1) . ' , ' . date('d-M-y', strtotime($value['updated_on'])) . '</p>
                                            <p id="success-msg' . $cnt . '" style="display:none;color: #09cc09;"></p>
                                        </div>
                                        <div class="cs-status-text" id="cs-status-update' . $cnt . '" style="display:none">
                                            <button class="btn-primary-mro" onclick="updateInvoiceDetails(' . $value['custom_field_invoice_data_submission_id'] . ',' . $cnt . ');">Done</button>
                                        </div>
                                    </td>
                                </tr> ';
                $cnt++;
            }
            $str = $str . $strtr . '</tbody>
                        </table>
                    </div>';
        } else {
            $str .= '<h3 class="form-group-title">No data available</h3>';
        }
        $str .= '</div>';

        echo json_encode(array('invoiceDetails' => $str));
        exit;
    }

    function updateInvoiceDetails() {
        $id = $_GET['id'];
        $invoice_date = !empty($_GET['invoice_date']) ? date("Y-m-d", strtotime($_GET['invoice_date'])) : NULL;
        $invoice_status = $_GET['invoice_status'];
        $paid_date = !empty($_GET['paid_date']) ? date("Y-m-d", strtotime($_GET['paid_date'])) : NULL;
        $invoice_value = $_GET['invoice_value'];
        $result = array();
        if ($id > 0 && $this->privilegeduser->hasPrivilege("DocumentInvoiceUpdate")) {
            $data['invoice_date'] = $invoice_date;
            $data['invoice_status'] = $invoice_status;
            $data['invoice_value'] = $invoice_value;
            $data['paid_date'] = $paid_date;
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            $result = $this->common->updateData("tbl_custom_field_invoice_data_submission", $data, array("custom_field_invoice_data_submission_id" => $id));
        }

        if ($result) {
            echo json_encode(array('success' => true, 'msg' => 'Successfully updated.'));
            exit;
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
            exit;
        }
    }

    function getLinkContainerDetails() {
        $container_id = $_GET['container_id'];
        $LSFRIArr = array();
        $WithOutLSFRIArr = array();
        $linkContainerData = array();
        $linkContainerLSIds = array();
        $linkContainerWithOutLSIds = array();
        $LSArr = array();
        $WithOutLSArr = array();
        $LSFRIDocumentNumber = $this->common->getData("tbl_document_uploaded_files", "uploaded_document_number", array("container_id" => $container_id, "document_type_id" => 2 ));
        $WithOutLSFRIDocumentNumber = $this->common->getData("tbl_document_ls_uploaded_files", "uploaded_document_number", array("container_id" => $container_id));

        //LS and FRI number
        if (!empty($LSFRIDocumentNumber)) {
            foreach ($LSFRIDocumentNumber as $value) {
                $LSFRIArr[] = $value['uploaded_document_number'];
            }
        }

        if (!empty($LSFRIArr)) {
            $documentNumberString = implode("','", $LSFRIArr);
            $documentNumberString = "'" . $documentNumberString . "'";
            $linkContainerLSIds = $this->container_detailsmodel->getLinkContainers($documentNumberString, 'LS');
        }

        foreach ($linkContainerLSIds as $value) {
            if ($value['container_id'] == $container_id) {
                continue;
            }
            $LSArr[] = $value['container_id'];
        }

        //without LS and FRI number
        if (!empty($WithOutLSFRIDocumentNumber)) {
            foreach ($WithOutLSFRIDocumentNumber as $value) {
                $WithOutLSFRIArr[] = $value['uploaded_document_number'];
            }
        }

        if (!empty($WithOutLSFRIArr)) {
            $documentNumberString = implode("','", $WithOutLSFRIArr);
            $documentNumberString = "'" . $documentNumberString . "'";
            $linkContainerWithOutLSIds = $this->container_detailsmodel->getLinkContainers($documentNumberString);
        }

        foreach ($linkContainerWithOutLSIds as $value) {
            if ($value['container_id'] == $container_id) {
                continue;
            }
            $WithOutLSArr[] = $value['container_id'];
        }

        $linkContainerData = array_unique(array_merge($LSArr, $WithOutLSArr), SORT_REGULAR);
        //    print_r($LSArr);
        //    print_r($WithOutLSArr);
        //    print_r($linkContainerData);die; 

        $str = '<div class="table-responsive">';
        if (!empty($linkContainerData)) {
            $str .= '<table class="table list-table linked-containers-table">
                        <thead>
                            <tr>
                                <th scope="col">Linked Containers</th>
                                <th scope="col">Status</th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>';

            foreach ($linkContainerData as $value) {
                $linkContainerDetailsData = $this->container_detailsmodel->getLinkContainerDetails($value);
                if (!empty($linkContainerDetailsData)) {
                    foreach ($linkContainerDetailsData as $value1) {
                        $containerNumber = '<a href="' . base_url() . 'container_details?text=' . rtrim(strtr(base64_encode("id=" . $value1['container_id']), '+/', '-_'), '=') . ' " title="Container Details">' . $value1['container_number'] . '</a>';
                        $str .= '<tr>
                                             <td><u>' . $containerNumber . '</u></td>
                                             <td>
                                                 <div class="cs-status-text">
                                                     <p class="container-status">' . $value1['container_status_name'] . '</p>
                                                 </div>
                                             </td>
                                             <td>
                                                 <div class="cs-status-text">
                                                     <p class="last-update-title">Last Updated</p>
                                                     <p class="last-update-name">' . $value1['firstname'] . ' ' . substr($value1['lastname'], 0, 1) . ' , ' . date('d-M-y', strtotime($value1['created_on'])) . '</p>
                                                 </div>
                                             </td>
                                         </tr>';
                    }
                }
            }
            $str .= '</tbody>
                    </table>';
        } else {
            $str .= '<h3 class="form-group-title">No data available</h3>';
        }
        $str .= '</div>';

        echo json_encode(array('linkDetails' => $str));
        exit;
    }

    function getInvoiceDetailsView() {
        $id = $_GET['id'];
        $invoiceDetails = '';
        $invoicePDF = '';
        $invoiceData = $this->container_detailsmodel->getInvoiceDetailsView($id);
        $invoiceSKUData = $this->container_detailsmodel->getInvoiceSKUDetails($id);
        //echo '<pre>'; print_r($invoiceData); die;     
        if ($invoiceData) {
            $invoiceDetails = '<div class="form-group">
                <label for="">Business Partner Name</label>
                <input type="text"  value="' . $invoiceData[0]['alias'] . '" readonly class="input-form-mro"> 
            </div>
            <div class="form-group">
                <input type="text" value="' . $invoiceData[0]['invoice_file_name'] . '" readonly placeholder="Enter data" class="input-form-mro">
                <i class="icon-edit"></i> 
               
            </div>
            <div class="form-group">
                <label for="">Total Invoice Value</label>
                <input type="text" value="' . $invoiceData[0]['invoice_value'] . '" readonly placeholder="Enter data" class="input-form-mro">
            </div>
            <div class="form-group">
                <label for="">Invoice Date</label>
                <input type="text" value="' . date('d-m-Y', strtotime($invoiceData[0]['invoice_date'])) . '" readonly placeholder="Enter data" class="form-control input-form-mro datepicker date-form-field" readonly>
            </div>
            <div class="form-group">
                <label for="">Invoice Status</label>
                <select class="input-form-mro" readonly> 
                    <option value="">' . $invoiceData[0]['invoice_status'] . '</option> 
                </select>
            </div>
            <div class="form-group">
                <label for="">Paid Date</label>
                <input type="text" value="' . date('d-m-Y', strtotime($invoiceData[0]['paid_date'])) . '" readonly placeholder="Enter data" class="form-control input-form-mro datepicker date-form-field" readonly>
            </div>';
            if (!empty($invoiceSKUData)) {
                foreach ($invoiceSKUData as $value) {
                    $invoiceDetails .= '<div class="form-group">
                                <label for="">SKU</label>
                                <select class="input-form-mro" readonly>
                                    <option value="">' . $value['sku_number'] . '</option> 
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="">Item Code </label>
                                <span>' . $value['sku_number'] . '</span>
                            </div>

                            <div class="form-group">
                                <label for="">Product Description</label>
                                <span>' . $value['product_name'] . '</span>
                            </div>

                            <div class="form-group">
                                <label for="">Size </label>
                                <span>' . $value['size_name'] . '</span>
                            </div> 
                            <div class="form-group">
                                <label for="">Quantity</label>
                                <input type="number" value="' . $value['quantity'] . '" readonly placeholder="Enter quantity" class="input-form-mro">
                            </div>

                            <div class="form-group">
                                <label for="">Price Per Unit</label>
                                <input type="number" value="' . $value['price_per_unit'] . '" readonly placeholder="Enter price per unit" class="input-form-mro">
                            </div>';
                }
            }
            
            $docPath = getDocumentFolder(0);
            $fileNameArr = explode('.', $invoiceData[0]['invoice_file_name']);
            $fileName = $fileNameArr[0].'.'.strtolower($fileNameArr[1]);
            $path = "container_document/".$docPath.'/'.$fileName;
            $url = get_s3_upload_file($path);
            $invoicePDF = '<div><iframe src="'.$url.'#zoom=FitH&page=anchor"  style="width:100%;height:900px;"></iframe></div>';
        }

        echo json_encode(array('invoiceDetails' => $invoiceDetails, 'invoicePDF' => $invoicePDF));
        exit;
    }

    function getDocumentDetails() {
        $container_id = $_GET['container_id'];  
        $documentUploadedDetails = array(); 
        if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') {
            $documentUploadedData = $this->container_detailsmodel->getDocumentUploaded($container_id);
            if (!empty($documentUploadedData)) {
                foreach ($documentUploadedData as $value) {
                    $documentUploadedDetails[$value['document_type_squence']] = $value;
                }
            }
        }
        $documentIsUploadedData = $this->container_detailsmodel->getDocumentIsUploaded($container_id);
        if (!empty($documentIsUploadedData)) {
            foreach ($documentIsUploadedData as $value) {
                $documentUploadedDetails[$value['document_type_squence']] = $value;
            }
        }
        
        ksort($documentUploadedDetails); 
        
        $containerSKUData = $this->container_detailsmodel->getContainerSizeData($container_id);
        //echo '<pre>'; print_r($containerSKUData);die;
        
        $containerFieldsData = $this->common->getData("tbl_custom_dynamic_data", "*", array("container_id" => $container_id));

        $containerFieldsDataArr = array();
        if (!empty($containerFieldsData)) {
            foreach ($containerFieldsData as $value) {
                $containerFieldsDataArr[$value['document_type_id']] = $value;
            }
        }       
        

        $cm = "'";
        $str = '';
        $documentStatus = 'Unchecked';
        $str .= '<div class="tab-content-wrapper container-details-doc-tab">';
        if (!empty($documentUploadedDetails)) {
            foreach ($documentUploadedDetails as $value) {

                $documentStatusData = $this->container_detailsmodel->getDocumentStatus($value['document_type_id'], $container_id);

                if (!empty($documentStatusData)) {
                    $documentStatus = $documentStatusData[0]['status'];
                    $approvedDate = date('d-M-y', strtotime($documentStatusData[0]['updated_on']));
                    $approvedBy = $documentStatusData[0]['firstname'] . ' ' . $documentStatusData[0]['lastname'];
                }
                $colorClass = "css-grey";
                if ($documentStatus == "Unchecked") {
                    $colorClass = "css-yellow";
                } else {
                    $colorClass = "css-green";
                }

                $str .= '<div class="cd-doc-row">
                        <div class="cdd-row-left">
                            <div class="doc-flag">
                                <div class="css-flag ' . $colorClass . '"></div>
                            </div>
                            <div class="doc-title">
                                <p class="doc-name">' . str_replace(',', '<br/>', $value['uploaded_document_number']) . '</p>
                                <p class="doc-date">' . date('d-M-y', strtotime($value['updated_on'])) . '</p>
                                <div class="cs-status-text">
                                    <p class="last-update-title">Last Updated</p>
                                    <p class="last-update-name">' . $value['firstname'] . ' ' . substr($value['lastname'], 0, 1) . '</p>
                                    <p class="last-update-date">' . date('d-M-y', strtotime($value['updated_on'])) . '</p>
                                </div>
                            </div>
                        </div>
                        <div class="cdd-row-center">
                            <div class="cd-doc-wrapper">
                                <div class="doc-single">
                                    <div class="sit-single">
                                        <p class="sit-single-title">Size</p>';
                if (!empty($containerSKUData)) {
                    foreach ($containerSKUData as $value1) {
                        $str .= '<p class="sit-single-value">' . $value1['size_name'] . '</p>';
                    }
                }
                $str .= '<p class="sit-single-value"><strong>Total</strong></p>
                                    </div>
                                </div>
                                <div class="doc-single">
                                    <div class="sit-single">
                                        <p class="sit-single-title">Quantity</p>';
                $qtyTotal = 0;
                if (!empty($containerSKUData)) {
                    foreach ($containerSKUData as $value2) {
                        $str .= '<p class="sit-single-value">' . $value2['total'] . '</p>';
                        $qtyTotal += $value2['total'];
                    }
                }
                $str .= '<p class="sit-single-value"><strong>' . $qtyTotal . '</strong></p>
                                    </div>
                                </div>';

                $documentsFieldsData = $this->container_detailsmodel->getDocumentsFields($value['document_type_id'], $value['document_number']); 
                    
                if (!empty($documentsFieldsData)) {
                    $hblcnt = 0;
                    foreach ($documentsFieldsData as $value3) {

                        $custom_field_structure_value = $value3['custom_field_structure_value'];
                        if ($value3['custom_field_type'] == 'date') {
                            $custom_field_structure_value = !empty($value3['custom_field_structure_value']) ? date('d-M-Y', strtotime($value3['custom_field_structure_value'])) : '';
                        }

                        $str .= $this->displayDocumentValue($containerFieldsDataArr, $value['document_type_id'], $value3['custom_field_title'], $custom_field_structure_value);

                        if($hblcnt==0 && ($value['document_type_id'] == "1" || $value['document_type_id'] == "4" || $value['document_type_id'] == "5" || $value['document_type_id'] == "7" || $value['document_type_id'] == "10")){
                            $str.=  '<div class="doc-single">
                                    <div class="sit-single"><p class="sit-single-title">HBL Number</p> 
                                    <p class="sit-single-value">'.$containerFieldsDataArr[$value['document_type_id']]['hbl_number'].'</p>
                                    </div>
                                    </div>'; 
                            $hblcnt++;
                        }
                    }
                }

                $str .= '</div>
                        </div>
                        <div class="cdd-row-right popup">';
                if ($this->privilegeduser->hasPrivilege("DocumentsView")) {
                    $str .= '<button class="btn-grey-mro" onclick="showDocumentDetails(' . $value['document_type_id'] . ',' . $container_id . ',' . $cm . $value['document_number'] . $cm . ',' . $cm . $documentStatus . $cm . ');" title="Document Approval">View</button>';
                }
                if ($this->privilegeduser->hasPrivilege("DocumentsReupload")) {
                    $str .= '<a class="btn-grey-mro" onclick="reuploadDocument(' . $value['document_type_id'] . ',' . $container_id . ',' . $cm . $value['document_number'] . $cm . ',' . $cm . $documentStatus . $cm . ');" style="cursor: pointer;" title="Reupload Document"><img src="' . base_url() . 'assets/images/redo.svg" alt=""></a>';
                }
                if ($documentStatus == 'checked') {
                    $str .= '&nbsp<a class="btn-grey-mro" onclick="showApprovedBy(' . $value['document_type_id'] . ');" title="Approved Information"><img class="info" src="' . base_url() . 'assets/images/info-btn.png" alt=""></a>';
                }
                $str .= '<span class="popuptext" id="myPopup' . $value['document_type_id'] . '">
                                    <span>' . $approvedBy . '</span></br><span>' . $approvedDate . '</span>
                                </span>
                       </div>
                    </div>';
            }
        } else {
            $str .= '<h3 class="form-group-title">No data available</h3>';
        }
        $str .= '</div>';
        echo json_encode(array('documentDetails' => $str));
        exit;
    }

    function getUploadDetailsView() {
        $document_type_id = $_GET['document_type_id'];
        $container_id = $_GET['container_id'];
        $document_number = $_GET['document_number'];
        $status = $_GET['status'];

        $leftsidehtml = "";
        $form_view = '';
        $cm = "'";

        if ($document_type_id != '2' && $document_type_id != '3') {

            $condition = "1=1  AND dluf.container_id = " . $container_id . " AND dluf.document_type_id = " . $document_type_id . " AND cfs.status = 'Active' ";
            $main_table = array("tbl_custom_field_structure as cfs", array("cfs.*"));
            $join_tables = array(
                array("left", "tbl_custom_field_data_submission as cfds", "cfds.custom_field_structure_id = cfs.custom_field_structure_id AND cfds.uploaded_document_number='" . $document_number . "'", array('cfds.custom_field_structure_value')),
                array("", "tbl_document_ls_uploaded_files as dluf", "dluf.document_type_id = cfs.document_type_id", array('group_concat(DISTINCT(dluf.uploaded_document_number)) as uploaded_document_number,uploaded_document_file ')),
                array("left", "tbl_sub_document_type as sdt", "sdt.document_type_id = dluf.document_type_id", array('group_concat(DISTINCT(sdt.sub_document_type_id)) as sub_document_type_id ')),
            );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'custom_field_structure_id');
            $result['getSelectedCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
            //echo $this->db->last_query();exit; 
              
            //$result['getSelectedCustomField'] = $this->container_detailsmodel->getDocumentsFields($document_type_id,$document_number); 

            $result['containersSKUData'] = $this->getcontainersSKUDataDetails($document_type_id, $document_number, $container_id);
            $leftsidehtml = '';
            $form_view = '';



            // get the manufacturer list 
            $condition = "status = 'Active' And business_type = 'Vendor' AND business_category ='Manufacturer'  ";
            $result['manufacturer'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number', $condition);

            // get the supplier name  
            $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Distributor','Manufacturer')  ";
            $result['supplier_name'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number', $condition);

            //supplier_update
            $result['supplier_update'] = '';
            $condition = "container_id = '" . $container_id . "' AND document_type_id = '".$document_type_id."' AND supplier_id IS NOT NULL";
            $supplier_update = $this->common->getData('tbl_custom_dynamic_data', 'supplier_id,supplier_contactperson,supplier_contactnumber', $condition, 'custom_dynamic_data_id', 'desc');
            if (!empty($supplier_update)) {
                $result['supplier_update'] = $supplier_update;
            }


            //  get hbl_number         
            $condition = "container_id = '" . $container_id . "' AND hbl_number IS NOT NULL  AND document_type_id = " . $document_type_id . " ";
            $result['get_hbl_number'] = $this->common->getData('tbl_custom_dynamic_data', 'hbl_number', $condition, 'custom_dynamic_data_id', 'desc');

            $result['all_freight_forwarder'] = array();
            $condition = "1=1  AND c.container_id = " . $container_id . " ";
            $main_table = array("tbl_container as c", array());
            $join_tables = array(
                array("", "tbl_order as o", "o.order_id = c.order_id", array('o.freight_forwarder_id')),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
            $orderData = $this->common->MySqlFetchRow($rs, "array"); // fetch result

            if (!empty($orderData)) {
                $condition = "status = 'Active' And business_partner_id IN (" . $orderData[0]['freight_forwarder_id'] . ")  ";
                $result['all_freight_forwarder'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,alias', $condition);
            }

            // get Port of Loading;
            $condition = "status = 'Active' ";
            $result['all_pol'] = $this->common->getData('tbl_pol', '*', $condition);
            // get Port of Discharge;
            $condition = "status = 'Active' ";
            $result['all_pod'] = $this->common->getData('tbl_pod', '*', $condition);
            //get liner name
            $condition = "status = 'Active' ";
            $result['all_liner'] = $this->common->getData('tbl_liner', '*', $condition);

            // echo "<pre>";
            // print_r($result['get_hbl_number']);
            // exit;

            if (!empty($result['getSelectedCustomField'][0]['document_type_id'])) {
                if ($result['getSelectedCustomField'][0]['document_type_id'] == 4) {
                    $leftsidehtml = $this->load->view("custom-fcr-update", $result, true);
                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 5) {
                    $leftsidehtml = $this->load->view("custom-hbl-update", $result, true);
                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 1) {
                    $leftsidehtml = $this->load->view("custom-ins-update", $result, true);
                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 10) {
                    $leftsidehtml = $this->load->view("custom-arrival-update", $result, true);
                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 7) {
                    $leftsidehtml = $this->load->view("7501-update", $result, true);
                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 8) {
                    $leftsidehtml = $this->load->view("custom-do-update", $result, true);
                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 9) {
                    $leftsidehtml = $this->load->view("custom-do-update", $result, true);
                }
            }

            // to get the  form view for both internal n external 
            if ($result) {
                
                if ($_SESSION["mro_session"]['user_role'] == 'Customer' || $_SESSION["mro_session"]['user_role'] == 'Supplier') {
                    $condition = "1=1  AND dluf.container_id = " . $container_id . " AND dluf.document_type_id = " . $document_type_id . " AND sdt.sub_document_type_id IN (15,17,20) ";
                    $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.*"));
                    $join_tables = array(
                        array("", "tbl_document_type as dt", "dt.document_type_id = dluf.document_type_id", array('dt.document_type_name as document_name')),
                        array("", "tbl_sub_document_type as sdt", "sdt.document_type_id = dt.document_type_id && sdt.sub_document_type_id = dluf.sub_document_type_id", array('sdt.sub_document_type_name as sub_document_name')),
                    ); 
                }else{
                    $condition = "1=1  AND dluf.container_id = " . $container_id . " AND dluf.document_type_id = " . $document_type_id . " ";
                    $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.*"));
                    $join_tables = array(
                        array("", "tbl_document_type as dt", "dt.document_type_id = dluf.document_type_id", array('dt.document_type_name as document_name')),
                        array("left", "tbl_sub_document_type as sdt", "sdt.document_type_id = dt.document_type_id && sdt.sub_document_type_id = dluf.sub_document_type_id", array('sdt.sub_document_type_name as sub_document_name')),
                    ); 
                }

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'document_type_id,sub_document_type_id');
                $result['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                //echo $this->db->last_query();exit; 
                // echo "<pre>";
                // print_r( $result['formView']);
                // exit;
                $form_view = $this->load->view("document-form-view", $result, true);
            }
        } else {
            //    $condition = "1=1  AND duf.uploaded_document_number = '".$document_number."' ";  
            //    $main_table = array("tbl_custom_field_structure as cfs", array("cfs.*"));
            //    $join_tables = array(
            //        array("","tbl_document_uploaded_files as duf","duf.document_type_id = cfs.document_type_id", array(' group_concat(DISTINCT(duf.uploaded_document_number)) as uploaded_document_number')),
            //    );

            //    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,"",'custom_field_structure_id');
            //    $result['getSelectedCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
            
            $condition = "1=1  AND dluf.container_id = " . $container_id . " AND dluf.document_type_id = " . $document_type_id . " AND cfs.status = 'Active' ";
            $main_table = array("tbl_custom_field_structure as cfs", array("cfs.*"));
            $join_tables = array(
                array("left", "tbl_custom_field_data_submission as cfds", "cfds.custom_field_structure_id = cfs.custom_field_structure_id AND cfds.uploaded_document_number='" . $document_number . "'", array('cfds.custom_field_structure_value')),
                array("", "tbl_document_uploaded_files as dluf", "dluf.document_type_id = cfs.document_type_id", array('group_concat(DISTINCT(dluf.uploaded_document_number)) as uploaded_document_number,uploaded_document_file ')),                    
            );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'custom_field_structure_id');
            $result['getSelectedCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
            // echo '<pre>'; print_r($result['getSelectedCustomField']);exit;   
                    
            //$result['getSelectedCustomField'] = $this->container_detailsmodel->getDocumentsFields($document_type_id, $document_number);
            //echo '<pre>'; print_r($result['getSelectedCustomField']); die;  
            $result['containersSKUData'] = $this->getcontainersSKUDataDetails($document_type_id, $document_number, $container_id);
            // get the manufacturer list 
            $condition = "status = 'Active' And business_type = 'Vendor' AND business_category ='Manufacturer'  ";
            $result['manufacturer'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number,alias', $condition);

            // get the supplier name  
            $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Distributor','Manufacturer')  ";
            $result['supplier_name'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number,alias', $condition);

            //supplier_update
            $result['supplier_update'] = '';
            $condition = "container_id = '" . $container_id . "' AND document_type_id = '".$document_type_id."' AND supplier_id IS NOT NULL";
            $supplier_update = $this->common->getData('tbl_custom_dynamic_data', 'supplier_id,supplier_contactperson,supplier_contactnumber', $condition, 'custom_dynamic_data_id', 'desc');
            if (!empty($supplier_update)) {
                $result['supplier_update'] = $supplier_update;
            }

            $condition = "container_id = '" . $container_id . "' AND document_type_id =".$result['getSelectedCustomField'][0]['document_type_id']."  AND date_of_inspection IS NOT NULL";
            $result['get_date_of_inspection'] = $this->common->getData('tbl_custom_dynamic_data', 'date_of_inspection', $condition, 'custom_dynamic_data_id', 'desc');

            $condition = "container_id = '" . $container_id . "' AND document_type_id =".$result['getSelectedCustomField'][0]['document_type_id']."  AND place_of_inspection IS NOT NULL";
            $result['get_place_of_inspection'] = $this->common->getData('tbl_custom_dynamic_data', 'place_of_inspection', $condition, 'custom_dynamic_data_id', 'desc');

            // get Port of Loading;
            $condition = "status = 'Active' ";
            $result['all_pol'] = $this->common->getData('tbl_pol', '*', $condition);
            // get Port of Discharge;
            $condition = "status = 'Active' ";
            $result['all_pod'] = $this->common->getData('tbl_pod', '*', $condition);
            //get liner name
            $condition = "status = 'Active' ";
            $result['all_liner'] = $this->common->getData('tbl_liner', '*', $condition);
            // echo "<pre>";
            // print_r($result['containersSKUData']);
            // exit;

            $leftsidehtml = '';
            if ($result) {
                if ($result['getSelectedCustomField'][0]['document_type_id'] == 3) {
                    $leftsidehtml = $this->load->view("custom-fri-update", $result, true);
                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 2) {
                    $leftsidehtml = $this->load->view("custom-ls-update", $result, true);
                }
            }

            // to get the  form view for both internal n external 
            if ($result) {
                $condition = "1=1  AND duf.uploaded_document_number = '" . $document_number . "' ";
                $main_table = array("tbl_document_uploaded_files as duf", array("duf.*"));
                $join_tables = array(
                    array("", "tbl_document_type as dt", "dt.document_type_id = duf.document_type_id", array('dt.document_type_name as document_name')),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'document_type_id');
                $result['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                // echo "<pre>";
                // echo $document_number;
                // print_r($result['formView']);exit;

                $form_view = $this->load->view("document-form-view", $result, true);
            }
        }
        $savehtml = '';

        if ($this->privilegeduser->hasPrivilege("DocumentAddEdit")) {
            $savehtml .= '<span class="btn-secondary-mro update-record" onclick="showSaveButton();"><img src="' . base_url() . 'assets/images/update-icon.svg" alt=""> Update</span>  
                    <button class="btn-secondary-mro save-record" style="display: none;">Save</button>';
        }
        if ($this->privilegeduser->hasPrivilege("DocumentApproval")) {
            if ($status == 'Unchecked') {
                $savehtml .= '<span class="btn-primary-mro checkedClass" onclick="documentApprove(' . $document_type_id . ',' . $container_id . ',' . $cm . $document_number . $cm . ');"><img src="' . base_url() . 'assets/images/check.png" alt=""> Check</span> ';
            } else {
                $savehtml .= ' <span class="btn-primary-mro checkedClass" style="cursor: auto;">Checked</span>';
            }
        }

        echo json_encode(array('custom_field' => $leftsidehtml, 'form_view' => $form_view, 'savehtml' => $savehtml));
        exit;
    }

    function reuploadDocument() {
        $document_type_id = $_GET['document_type_id'];
        $container_id = $_GET['container_id'];
        $document_number = $_GET['document_number'];
        $status = $_GET['status'];

        $leftsidehtml = "";
        $form_view = '';
        $cm = "'";

        //    $result['getSelectedCustomField'] = $this->container_detailsmodel->getDocumentsFields($document_type_id,$document_number);  
        //    $result['containersSKUData'] = $this->getcontainersSKUDataDetails($document_type_id,$document_number,$container_id); 

        if ($document_type_id == 2 || $document_type_id == 3) {
            $condition = "1=1  AND duf.uploaded_document_number = '" . $document_number . "' ";
            $main_table = array("tbl_document_uploaded_files as duf", array("duf.*"));
            $join_tables = array(
                array("", "tbl_document_type as dt", "dt.document_type_id = duf.document_type_id", array('dt.document_type_name as document_name')),
            );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'document_type_id');
            $result['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
        } else {

            $condition = "1=1  AND dluf.container_id = " . $container_id . " AND dluf.document_type_id = " . $document_type_id . " ";
            $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.*"));
            $join_tables = array(
                array("", "tbl_document_type as dt", "dt.document_type_id = dluf.document_type_id", array('dt.document_type_name as document_name')),
                array("left", "tbl_sub_document_type as sdt", "sdt.document_type_id = dt.document_type_id && sdt.sub_document_type_id = dluf.sub_document_type_id", array('sdt.sub_document_type_name as sub_document_name')),
            );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'document_type_id,sub_document_type_id');
            $result['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
        }
        $leftsidehtml = $this->load->view("customer-field-reupload", $result, true);

        $condition = "1=1  AND d.document_type_id = '" . $document_type_id . "' ";
        $main_table = array("tbl_document_type as d", array('d.document_type_name'));
        $join_tables = array(
            array("left", "tbl_sub_document_type as sd", "sd.document_type_id = d.document_type_id", array("sd.sub_document_type_name,sd.sub_document_type_id")),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
        $getSubDocument = $this->common->MySqlFetchRow($rs, "array"); // fetch result 

        $result1['getSubDocument'] = $getSubDocument;
        $result1['document_type_id'] = $document_type_id;
        $result1['container_id'] = $container_id;
        //echo '<pre>'; print_r($result1); die;

        $form_view = $this->load->view("document-reupload-form", $result1, true);

        $savehtml = '';

        if ($this->privilegeduser->hasPrivilege("DocumentAddEdit")) {
            $savehtml .= '<button class="btn-primary-mro">Save</button>';
        }
        echo json_encode(array('custom_field' => $leftsidehtml, 'form_view' => $form_view, 'savehtml' => $savehtml));
        exit;
    }

    function documentApprove() {
        $document_type_id = $_GET['document_type_id'];
        $container_id = $_GET['container_id'];
        $document_number = $_GET['document_number'];

        $table = "tbl_document_ls_uploaded_files";
        if ($document_type_id == '2' || $document_type_id == '3') {
            $table = "tbl_document_uploaded_files";
        }
        $containerIds = $this->common->getData($table, "container_id", array("uploaded_document_number" => $document_number));
        if (!empty($containerIds)) {
            foreach ($containerIds as $value) {
                $data['status'] = 'Checked';
                $data['updated_on'] = date("Y-m-d H:i:s");
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $condition = " container_id = " . $value['container_id'] . " and document_type_id = " . $document_type_id . " ";
                $result = $this->common->updateData("tbl_container_uploaded_document_status", $data, $condition);
            }
        }

        if ($result) {
            echo json_encode(array("success" => true, "msg" => "Document Approved! "));
            exit;
        } else {
            echo json_encode(array("success" => false, "msg" => "Not Approved!"));
            exit;
        }
    }

    public function submitFormStep5() {
        // echo "<pre>";
        // print_r($_POST['document_type_id']);
        // exit;       
        // cutomer field compulsory removed   

        if (!empty($_POST['custom_field_structure_id']) && isset($_POST['custom_field_structure_id'])) {
            $uploaded_document_number = explode(',', $_POST['uploaded_document_number']);
            $actualDateofArrival = '';
            if (!empty($_POST['sub_document_type_id']) && isset($_POST['sub_document_type_id'])) {
                $sub_document_type_cnt = explode(',', $_POST['sub_document_type_id']);
                foreach ($sub_document_type_cnt as $subDocumentkey => $subDocumentval) {
                    foreach ($_POST['custom_field_structure_id'] as $key => $value) {

                        $condition = " custom_field_structure_id = " . $value . " and document_type_id = " . $_POST['document_type_id'] . " and sub_document_type_id= '" . $subDocumentval . "'  and uploaded_document_number = '" . $uploaded_document_number[$subDocumentkey] . "' ";
                        $dynamic_data = $this->common->getData('tbl_custom_field_data_submission', 'custom_data_submission_id,current_session_id', $condition, 'custom_data_submission_id', 'desc');
                        if (!empty($dynamic_data)) {
                            $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $condition = " custom_field_structure_id = " . $value . " and document_type_id = " . $_POST['document_type_id'] . " and sub_document_type_id= '" . $subDocumentval . "'  and uploaded_document_number = '" . $uploaded_document_number[$subDocumentkey] . "' ";
                            $result = $this->common->updateData("tbl_custom_field_data_submission", $data, $condition);
                        } else {
                            $data['custom_field_structure_id'] = $value;
                            $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                            $data['uploaded_document_number'] = $uploaded_document_number[$subDocumentkey];
                            $data['document_type_id'] = (!empty($_POST['document_type_id']) ? $_POST['document_type_id'] : " ");
                            $data['sub_document_type_id'] = $subDocumentval;
                            $data['current_session_id'] = '';
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $result = $this->common->insertData("tbl_custom_field_data_submission", $data, "1");
                        }
                    }
                }
            } else {
                foreach ($_POST['custom_field_structure_id'] as $key => $value) {
                    $condition = " custom_field_structure_id = " . $value . " and document_type_id = " . $_POST['document_type_id'] . " and uploaded_document_number = '" . $uploaded_document_number[0] . "' ";
                    //$dynamic_data = $this->common->getData('tbl_custom_field_data_submission', 'custom_data_submission_id', $condition, 'custom_data_submission_id', 'desc');                    
                    $deleted = $this->common->deleteRecord('tbl_custom_field_data_submission', $condition); 
                    //    if (!empty($dynamic_data)) { 
                    //        //Actual date of Arrival check for DO upload
                    //        if(isset($_POST['dateOfArrival'])){
                    //            if($value == 92){
                    //                $actualDateofArrival = (!empty($_POST[$value]) ? date('Y-m-d', strtotime($_POST[$value])) : "");
                    //            }
                    //        }
                        
                    //        $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                    //        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                    //        $data['updated_on'] = date("Y-m-d H:i:s");
                    //        $condition = " custom_field_structure_id = " . $value . " and document_type_id = " . $_POST['document_type_id'] . " and uploaded_document_number = '" . $uploaded_document_number[0] . "' ";
                    //        $result = $this->common->updateData("tbl_custom_field_data_submission", $data, $condition);
                    //    } else {
                    if($deleted){
                        //Actual date of Arrival check for DO upload
                        if(isset($_POST['dateOfArrival'])){
                            if($value == 92){
                                $actualDateofArrival = (!empty($_POST[$value]) ? date('Y-m-d', strtotime($_POST[$value])) : "");
                            }
                        }
                        $data['custom_field_structure_id'] = $value;
                        $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                        $data['uploaded_document_number'] = $uploaded_document_number[0];
                        $data['document_type_id'] = (!empty($_POST['document_type_id']) ? $_POST['document_type_id'] : "0");
                        $data['sub_document_type_id'] = (!empty($_POST['sub_document_type_id']) ? $_POST['sub_document_type_id'] : "0");
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['created_on'] = date("Y-m-d H:i:s");
                        $data['current_session_id'] = '';
                        $result = $this->common->insertData("tbl_custom_field_data_submission", $data, "1");
                    }
                }
            }

            // start  code modified by shiv on 21-10-21 for document type FRI N LS
            if (!empty($_POST['container_sku_id']) && isset($_POST['container_sku_id'])) {
                foreach ($_POST['container_sku_id'] as $key => $container_val) {
                    // $key == container id here
                    foreach ($container_val as $sku_key => $sku_val) {
                        $data1 = array();
                        $data1['quantity'] = $_POST['container_sku_qty'][$key][$sku_key];
                        $condition = "container_sku_id = " . $sku_key . " AND container_id = " . $key . " ";
                        $this->common->updateData("tbl_container_sku", $data1, $condition);
                        $data_manu = array();
                        $data_manu['manufacturer_id'] = $_POST['manufacturer'][$key][$sku_key];
                        $condition = "sku_id = " . $sku_key . "  ";
                        $this->common->updateData("tbl_sku", $data_manu, $condition);
                    }
                }
            }

            if (!empty($_POST['lot_number']) && isset($_POST['lot_number'])) {
                foreach ($_POST['lot_number'] as $seal_key => $seal_val) {
                    $data_lot = array();
                    $data_lot['lot_number'] = $_POST['lot_number'][$seal_key];
                    $condition = " container_id = " . $seal_key . " ";
                    $this->common->updateData("tbl_container", $data_lot, $condition);
                }
            }

            if (!empty($_POST['supplier_name']) && isset($_POST['supplier_name'])) {
                foreach ($_POST['supplier_name'] as $supp_key => $supp_val) {
                    foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                        $data_supp = array();
                        $data_supp['supplier_id '] = $supp_val;
                        $data_supp['supplier_contactperson'] = $_POST['contactpersonname'][$supp_key];
                        $data_supp['supplier_contactnumber'] = $_POST['contactpersonnumber'][$supp_key];
                        // $data_supp['sub_document_type_id'] = explode(',',$_POST['sub_document_type_id']);
                        $data_supp['current_session_id'] = '';

                        $condition = " container_id = " . $container_id . " AND document_type_id =".$_POST['document_type_id']." ";
                        $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                        if (!empty($dynamic_data)) {
                            $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$_POST['document_type_id']." ";
                            $data_supp['updated_on'] = date("Y-m-d H:i:s");
                            $data_supp['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition);
                        } else {
                            $data_supp['container_id'] = $container_id;
                            $data_supp['document_type_id'] = $_POST['document_type_id'];
                            $data_supp['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data_supp['updated_on'] = date("Y-m-d H:i:s");
                            $data_supp['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data_supp['created_on'] = date("Y-m-d H:i:s");
                            $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                        }
                    }
                }
            }

            if (!empty($_POST['sgs_seal']) && isset($_POST['sgs_seal'])) {
                foreach ($_POST['sgs_seal'] as $seal_key => $seal_val) {
                    $data_seal = array();
                    $data_seal['sgs_seal'] = $seal_val;
                    $data_seal['shipping_container'] = $_POST['shipping_container'][$seal_key];
                    $data_seal['ff_seal'] = $_POST['ff_seal'][$seal_key];
                    if(isset($_POST['terminal'])){
                        $data_seal['terminal'] = $_POST['terminal'][$seal_key];
                    } 
                    $condition = " container_id = " . $seal_key . " ";
                    $this->common->updateData("tbl_container", $data_seal, $condition);
                }
            }

            // end  code modified by shiv on 21-10-21 for document type FRI N LS
            // for HBL N FCR document type 
            if (!empty($_POST['tbl_container_id']) && isset($_POST['tbl_container_id'])) {
                foreach ($_POST['tbl_container_id'] as $key => $value) {
                    $data2['freight_forwarder_id'] = $_POST['freight_forwarder_id'][$key];
                    $data2['liner_name'] = $_POST['liner_name'][$key];
                    $data2['pol'] = $_POST['pol'][$key];
                    $data2['pod'] = $_POST['pod'][$key];
                    $data2['etd'] = !empty($_POST['etd'][$key]) ? date("Y-m-d", strtotime($_POST['etd'][$key])) : NULL;
                    $data2['revised_etd'] = !empty($_POST['revised_etd'][$key]) ? date("Y-m-d", strtotime($_POST['revised_etd'][$key])) : NULL;
                    $data2['ett'] = $_POST['ett'][$key];
                    $data2['vessel_name'] = $_POST['vessel_name'][$key];
                    foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                        $condition = " container_id = " . $container_id . " ";
                        $this->common->updateData("tbl_container", $data2, $condition);
                    }
                }
            }
            
            //update actual date of arrival when upload DO then update container revised eta field
            if(!empty($actualDateofArrival)){
                $data_revised_eta = array(); 
                $data_revised_eta['revised_eta'] = !empty($actualDateofArrival) ? $actualDateofArrival : NULL;
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $condition = " container_id = " . $container_id . " ";
                    $this->common->updateData("tbl_container", $data_revised_eta, $condition);
                }
            }

            if (!empty($_POST['revised_eta']) && isset($_POST['revised_eta'])) {
                foreach ($_POST['revised_eta'] as $eta_key => $eta_val) {
                    $data_eta = array();
                    $data_eta['etd'] = !empty($_POST['etd'][$eta_key]) ? date("Y-m-d", strtotime($_POST['etd'][$eta_key])) : NULL;
                    $data_eta['eta'] = !empty($_POST['eta'][$eta_key]) ? date("Y-m-d", strtotime($_POST['eta'][$eta_key])) : NULL;
                    $data_eta['revised_eta'] = !empty($eta_val) ? date("Y-m-d", strtotime($eta_val)) : NULL;
                    $condition = " container_id = " . $eta_key . " ";
                    //print_r($data_eta); die; 
                    foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                        $condition = " container_id = " . $container_id . " ";
                        $this->common->updateData("tbl_container", $data_eta, $condition);
                    }
                }
            }

            if (!empty($_POST['hbl_number']) && isset($_POST['hbl_number'])) {
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $data_hbl = array();
                    $data_hbl['hbl_number'] = $_POST['hbl_number'];
                    $data_hbl['current_session_id'] = '';

                    $condition = " container_id = " . $container_id . "  AND document_type_id =".$_POST['document_type_id']." ";
                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                    if (!empty($dynamic_data)) {
                        $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$_POST['document_type_id']." ";
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $this->common->updateData("tbl_custom_dynamic_data", $data_hbl, $condition);
                    } else {
                        $data_hbl['container_id'] = $container_id;
                        $data_hbl['document_type_id'] = $_POST['document_type_id'];
                        $data_hbl['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_dynamic_data", $data_hbl, "1");
                    }
                }
            }
                    
            if (isset($_POST['date_of_inspection'])) {
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $data_hbl = array();
                    $data_hbl['date_of_inspection'] = !empty($_POST['date_of_inspection']) ? date("Y-m-d", strtotime($_POST['date_of_inspection'])) : NULL;
                    $data_hbl['current_session_id'] = '';

                    $condition = " container_id = " . $container_id . " AND document_type_id =".$_POST['document_type_id']." ";
                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                    if (!empty($dynamic_data)) {
                        $condition = " container_id = " . $dynamic_data[0]['container_id'] . "  AND document_type_id =".$_POST['document_type_id']." ";
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $this->common->updateData("tbl_custom_dynamic_data", $data_hbl, $condition);
                    } else {
                        $data_hbl['container_id'] = $container_id;
                        $data_hbl['document_type_id'] = $_POST['document_type_id'];
                        $data_hbl['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_dynamic_data", $data_hbl, "1");
                    }
                }
            }

            if (isset($_POST['place_of_inspection'])) {
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $data_hbl = array();
                    $data_hbl['place_of_inspection'] = $_POST['place_of_inspection'];
                    $data_hbl['current_session_id'] = '';

                    $condition = " container_id = " . $container_id . " AND document_type_id =".$_POST['document_type_id']." ";
                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                    if (!empty($dynamic_data)) { 
                        $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$_POST['document_type_id']." ";
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $this->common->updateData("tbl_custom_dynamic_data", $data_hbl, $condition);
                    } else {
                        $data_hbl['container_id'] = $container_id;
                        $data_hbl['document_type_id'] = $_POST['document_type_id'];
                        $data_hbl['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_dynamic_data", $data_hbl, "1");
                    }
                }
            }

            //code update date of document in container status log 
            foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND cfds.uploaded_document_number = '" . $uploaded_document_number[0] . "' AND cfds.document_type_id = " . $_POST['document_type_id'] . " ";
                $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));
                $join_tables = array(
                    array("", "tbl_custom_field_structure as cfs", "cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, 'cfds.custom_field_structure_value ASC', '');
                $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
                //print_r($dateOfDocumentData);die;
                if (!empty($dateOfDocumentData)) {
                    $condition = " document_type_id = " . $_POST['document_type_id'] . " ";
                    $statuIdData = $this->common->getData('tbl_container_status', 'container_status_id', $condition);
                    if (!empty($statuIdData)) {
                        $data_doc_date = array();
                        $condition = " container_id = " . $container_id . " AND container_status_id = " . $statuIdData[0]['container_status_id'] . " ";
                        $data_doc_date['document_date'] = date("Y-m-d", strtotime($dateOfDocumentData[0]['custom_field_structure_value']));
                        $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);
                    }
                }
            }

            if ($result) {
                echo json_encode(array("success" => true, "msg" => "Document successfully updated in container!", "type" => "document"));
                exit;
            } else {
                echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                exit;
            }
        } else {
            //document reupload code
            if (!empty($_POST['container_id']) && isset($_POST['container_id']) && !empty($_POST['document_type_id']) && isset($_POST['document_type_id'])) {

                $this->load->library('upload');
                $result = array();
                // this is for fr n ls document type 
                if ($_POST['document_type_id'] == '2' || $_POST['document_type_id'] == '3') {
                    if (isset($_FILES) && isset($_FILES["step3_document"]["name"])) {
                        $this->upload->initialize($this->set_upload_options($_POST['document_type_id']));
                        if (!$this->upload->do_upload("step3_document")) {
                            $image_error = array('error' => $this->upload->display_errors());
                            echo json_encode(array("success" => false, "msg" => $image_error['error']));
                            exit;
                        } else {
                            $image_data = array('upload_data' => $this->upload->data());
                            //function for upload file in live bucket
                            $full_path = $image_data['upload_data']['full_path'];                            
                            $resultUpload = set_s3_upload_file($full_path,$_POST['document_type_id']); 
                            $step3_document = $image_data['upload_data']['file_name'];
                            $data['uploaded_document_file'] = $step3_document;
                            $upload = true;
                        }
                    }

                    if ($upload) {
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $condition = " container_id = " . $_POST['container_id'] . " AND document_type_id = " . $_POST['document_type_id'] . " ";
                        $result = $this->common->updateData("tbl_document_uploaded_files", $data, $condition);
                        // echo $this->db->last_query();exit;
                    }
                } else {
                    $files = $_FILES;
                    foreach ($files['step3_document']['name'] as $key => $value) {
                        $upload = false;
                        if (!empty($files['step3_document']['name'][$key]) && isset($files['step3_document']['name'][$key])) {

                            $_FILES['step3_document']['name'] = $files['step3_document']['name'][$key];
                            $_FILES['step3_document']['type'] = $files['step3_document']['type'][$key];
                            $_FILES['step3_document']['tmp_name'] = $files['step3_document']['tmp_name'][$key];
                            $_FILES['step3_document']['error'] = $files['step3_document']['error'][$key];
                            $_FILES['step3_document']['size'] = $files['step3_document']['size'][$key];

                            $this->upload->initialize($this->set_upload_options($_POST['document_type_id']));
                            if (!$this->upload->do_upload("step3_document")) {
                                $image_error = array('error' => $this->upload->display_errors());
                                echo json_encode(array("success" => false, "msg" => $image_error['error']));
                                exit;
                            } else {
                                $image_data = array('upload_data' => $this->upload->data());
                                //function for upload file in live bucket
                                $full_path = $image_data['upload_data']['full_path'];                            
                                $resultUpload = set_s3_upload_file($full_path,$_POST['document_type_id']);
                                
                                $step3_document = $image_data['upload_data']['file_name'];
                                if ($key == '0') {
                                    $uploaded_document_type_id['document_type_id'] = $key;
                                    $data['uploaded_document_file'] = $step3_document;
                                } else {
                                    $uploaded_document_file_name['file_name'][$key] = $step3_document;
                                    $uploaded_document_type_id['sub_document_type_id'][$key] = $key;
                                }
                                $upload = true;
                            }
                        }
                    }

                    if ($upload) {

                        foreach ($files['step3_document']['name'] as $key => $value) {
                            if ($key == 0) {
                                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['updated_on'] = date("Y-m-d H:i:s");
                                $condition = " container_id = " . $_POST['container_id'] . " AND document_type_id = " . $_POST['document_type_id'] . " ";
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $data, $condition);
                            } else {
                                $data['uploaded_document_file'] = $uploaded_document_file_name['file_name'][$key];
                                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['updated_on'] = date("Y-m-d H:i:s");
                                $condition = " container_id = " . $_POST['container_id'] . " AND document_type_id = " . $_POST['document_type_id'] . " AND sub_document_type_id = " . $uploaded_document_type_id['sub_document_type_id'][$key] . " ";
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $data, $condition);
                            }
                        }
                    }
                }

                if ($result) {
                    echo json_encode(array("success" => true, "msg" => "Reupload document successfully ! ", "type" => "reupload"));
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "Something wrong!", "type" => ""));
                    exit;
                }
            }
        }
    }

    function getcontainersSKUData($documentType = '', $sessionId = '') {
        if ($documentType == 'LS') {
            $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.container_id,dluf.document_type_id"));
        } else {
            $main_table = array("tbl_document_uploaded_files as dluf", array("dluf.container_id,dluf.document_type_id"));
        }
        $condition = "1=1  AND dluf.current_session_id = '" . $sessionId . "' ";

        $join_tables = array(
            array("", "tbl_container_sku as csku", "csku.container_id = dluf.container_id", array('csku.*')),
            array("", "tbl_sku as sku", "sku.sku_id = csku.sku_id", array('sku.sku_number,sku.manufacturer_id')),
            array("", "tbl_container as c", "c.container_id = dluf.container_id", array('c.*')),
            array("", "tbl_order as o", "o.order_id = c.order_id", array()),
            array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array()),
            array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias,bp.business_name,bp.business_partner_id')),
            array("left", "tbl_business_partner as ff", "ff.business_partner_id = c.freight_forwarder_id", array('ff.business_partner_id as ff_id, ff.alias as ff_name')),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, '', 'csku.container_sku_id');
        $containerData = $this->common->MySqlFetchRow($rs, "array");
        // echo $this->db->last_query();exit;
        $resultArr = array();
        foreach ($containerData as $value) {
            if ($value['document_type_id'] == '1' || $value['document_type_id'] == '2' || $value['document_type_id'] == '3' || $value['document_type_id'] == '4' || $value['document_type_id'] == '5' || $value['document_type_id'] == '7' || $value['document_type_id'] == '10' || $value['document_type_id'] == '8') {
                $resultArr[$value['container_id']][$value['container_sku_id']] = $value;
            }
        }
        //echo '<pre>'; print_r($resultArr);die;
        return $resultArr;
    }

    function getcontainersSKUDataDetails($document_type_id = '', $document_number = '', $container_id = 0) {
        if ($document_type_id == '2' || $document_type_id == '3') {
            $main_table = array("tbl_document_uploaded_files as dluf", array("dluf.container_id,dluf.document_type_id"));
        } else {
            $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.container_id,dluf.document_type_id")); //dluf.container_id,
        }
        $condition = "1=1  AND dluf.uploaded_document_number = '" . $document_number . "' AND dluf.container_id=" . $container_id . " ";

        $join_tables = array(
            array("", "tbl_container_sku as csku", "csku.container_id = dluf.container_id", array('csku.*')),
            array("", "tbl_sku as sku", "sku.sku_id = csku.sku_id", array('sku.sku_number,sku.manufacturer_id')),
            array("", "tbl_container as c", "c.container_id = dluf.container_id", array('c.*')),
            array("", "tbl_order as o", "o.order_id = c.order_id", array()),
            array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array()),
            array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias,bp.business_name,bp.business_partner_id')),
            array("left", "tbl_business_partner as ff", "ff.business_partner_id = c.freight_forwarder_id", array('ff.business_partner_id as ff_id, ff.alias as ff_name')),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, '', 'csku.container_sku_id');
        $containerData = $this->common->MySqlFetchRow($rs, "array");
        // echo $this->db->last_query();exit;
        $resultArr = array();
        foreach ($containerData as $value) {
            if ($value['document_type_id'] == '1' || $value['document_type_id'] == '2' || $value['document_type_id'] == '3' || $value['document_type_id'] == '4' || $value['document_type_id'] == '5' || $value['document_type_id'] == '7' || $value['document_type_id'] == '10' || $value['document_type_id'] == '8') {
                $resultArr[$value['container_id']][$value['container_sku_id']] = $value;
            }
        }
        //echo '<pre>'; print_r($resultArr);die;
        return $resultArr;
    }

    public function getPersonDetail() {
        // get business person detail
        $condition = "status = 'Active' AND business_partner_id = " . $_POST['supplier_id'] . " ";
        $supplier_details = $this->common->getData('tbl_business_partner', '*', $condition);
        // echo "<pre>";
        // print_r($supplier_details);
        // exit;
        if ($supplier_details) {
            echo json_encode(array("success" => true, "msg" => "supplier detail found successfully !", "contactpersonname" => $supplier_details[0]['contact_person'], "contactpersonnumber" => $supplier_details[0]['phone_number']));
            exit;
        } else {
            echo json_encode(array("success" => false, "msg" => "supplier detail not found"));
            exit;
        }
    }

    public function set_upload_options($documentTypeId = 0, $file_name = "") {  
        $config = array();
        if(!empty($file_name) &&  $file_name != ""){
                $config['file_name']   = $file_name.time();
        }

        $docPath = getDocumentFolder($documentTypeId);
        // if($_SERVER['HTTP_HOST']=='localhost'){
        //     $config['upload_path'] = DOC_ROOT_FRONT."/container_document/".$docPath;
        // }else{
        //     $config['upload_path'] = DOC_ROOT_FRONT."/demo/container_document/".$docPath;
        // } 
        
        $config['upload_path'] = DOC_ROOT_FRONT."/container_document/".$docPath;
        
        if(!is_dir($config['upload_path'])){
                mkdir($config['upload_path'],0777, true);		
        }
        $config['allowed_types'] = 'pdf';
        $config['max_size']      = '25000';
        $config['overwrite']     = FALSE;
        // $config['min_width']            = 1000;
        // $config['min_height']           = 1000;
        return $config; 
    }             

    public function displayDocumentValue($containerFieldsData = array(), $document_type_id = 0, $custom_field_title = '', $custom_field_structure_value = '') {
        $str = '<div class="doc-single">
                    <div class="sit-single">';
        switch (true) {
            case ($document_type_id == "2" || $document_type_id == "3"):
                $place_of_inspection = '';
                $date_of_inspection = '';  
                if(isset($containerFieldsData[$document_type_id])){
                    $place_of_inspection = $containerFieldsData[$document_type_id]['place_of_inspection']; 
                    $date_of_inspection = $containerFieldsData[$document_type_id]['date_of_inspection'];
                } 
            
                if ($custom_field_title == 'Place of Inspection') { 
                    $str .= '<p class="sit-single-title">' . $custom_field_title . '</p> 
                                            <p class="sit-single-value">' . $place_of_inspection . '</p>';
                }else if ($custom_field_title == 'Date of Inspection') {
                    $date = !empty($date_of_inspection) ? date('d-M-Y', strtotime($date_of_inspection)) : '';
                    $str .= '<p class="sit-single-title">' . $custom_field_title . '</p> 
                                            <p class="sit-single-value">' . $date . '</p>';
                } else {
                    $str .= '<p class="sit-single-title">' . $custom_field_title . '</p> 
                                            <p class="sit-single-value">' . $custom_field_structure_value . '</p>';
                }
                break;

            default: 
                if ($custom_field_title == 'HBL Number') { 
                    
                } else {
                    $str .= '<p class="sit-single-title">' . $custom_field_title . '</p> 
                                    <p class="sit-single-value">' . $custom_field_structure_value . '</p>';
                }
                break;
        }

        $str .= '</div>
                </div>';
        return $str;
    }

    public function updateContainerSKUQty() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            //echo '<pre>'; print_r($_POST['qty']);die;
            if (!empty($_POST['qty']) && isset($_POST['qty'])) {
                $result = 0;
                foreach ($_POST['qty'] as $key => $value) {
                    $data = array();
                    $data['quantity'] = $value;
                    $condition = "container_sku_id = " . $key . " ";
                    $result = $this->common->updateData("tbl_container_sku", $data, $condition);
                }
                if ($result) {
                    echo json_encode(array('success' => true, 'msg' => 'Container SKU Quantity updated successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while updating data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while updating data.'));
            exit;
        }
    }

    function getContainerSKUQtyEnter() {
        $container_id = isset($_GET['container_id']) ? $_GET['container_id'] : '';
        $sku_id = $_GET['sku_id'];
        $contract_id = $_GET['order_id'];
        $quantity = $_GET['quantity'];

        $totalContainerQty = 0;
        $contractQty = 0;
        if (!empty($container_id)) {
            $condition = "c.order_id=" . $contract_id . "  AND csku.sku_id = '" . $sku_id . "' AND c.container_id <> " . $container_id . " ";
        } else {
            $condition = "c.order_id=" . $contract_id . "  AND csku.sku_id = '" . $sku_id . "' ";
        }
        $main_table = array("tbl_container_sku as csku", array('csku.quantity'));
        $join_tables = array(
            array("inner", "tbl_container as c", "c.container_id = csku.container_id", array("c.order_id")),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
        $containerSKUQty = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
        //echo $this->db->last_query();exit;
        if (!empty($containerSKUQty)) {
            foreach ($containerSKUQty as $value) {
                $totalContainerQty += $value['quantity'];
            }
        }

        $contractSKUQty = $this->common->getData("tbl_order_sku", "quantity", array("order_id " => (int) $contract_id, "sku_id" => $sku_id));
        if (!empty($contractSKUQty)) {
            foreach ($contractSKUQty as $value) {
                $contractQty += $value['quantity'];
            }
        }
        $remainingQty = 0;

        $totalQty = $contractQty - $totalContainerQty;

        if ($totalQty < 0) {
            $remainingQty = 0;
            echo json_encode(array('fail' => true, 'remainingQty' => $remainingQty, 'msg' => 'Remaining Quantity ' . $remainingQty));
            exit;
        } else {
            if ($quantity > $totalQty) {
                $remainingQty = $totalQty;
                echo json_encode(array('fail' => true, 'remainingQty' => $remainingQty, 'msg' => 'Remaining Quantity ' . $remainingQty));
                exit;
            } else {
                $remainingQty = $quantity;
                echo json_encode(array('fail' => false, 'remainingQty' => $remainingQty, 'msg' => 'success'));
                exit;
            }
        }
    }

}

?>
