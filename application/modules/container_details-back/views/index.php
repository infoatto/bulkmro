<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div id="container-editable-div">                    
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                            <span>></span>
                            <a href="<?php echo base_url("listOfContainer"); ?>">Container Bible</a>
                            <span>></span>
                            <p># <?= $container_details['container_number']; ?></p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title">Container # <?= $container_details['container_number']; ?></h1>
                            <input type="hidden" name="container_id" id="container_id" value="<?php echo (!empty($container_details['container_id'])) ? $container_details['container_id'] : ""; ?>">
                            <div class="nav-divider"></div>
                            <div class="cs-status-text">
                                <p class="container-status"><?php echo (!empty($show_details['containerStatus'])) ? $show_details['containerStatus'] : ""; ?></p>
                            </div>
                            <div class="cs-status-text" style="flex: 0 0 160px;max-width: none;">
                                <p class="last-update-title">Last Updated</p>
                                <p class="last-update-name"><?php echo (!empty($show_details['updateBy'])) ? $show_details['updateBy'] : ""; ?></p>
                            </div>
                            <?php
                            if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) {
                                ?>
                            <img onclick="editableDetails('<?php echo (!empty($container_details['container_id'])) ? $container_details['container_id'] : "0"; ?>');" src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt="">
                            <?php }
                            ?>
                        </div>
                    </div>
                    <div class="title-sec-right">
                        <?php if ($this->privilegeduser->hasPrivilege("DocumentUpload")) { ?>              
                            <a href="javascript:void(0);" class="btn-primary-mro upload-document-details" onclick="openDocModal()"><img src="assets/images/upload-icon-white.svg" alt=""> Upload File</a>
<!--                            <a href="#/" class="btn-primary-mro"><img src="<?php echo base_url(); ?>assets/images/upload-icon-white.svg" alt=""> Upload File</a>-->
                        <?php } ?> 
                        
                        <?php
                        if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) {
                             echo $actionCol;
                        }
                        ?> 
                    </div>
                </div>
                <div class="shipping-info-top">
                    <h2 class="sec-title">Shipping Information</h2>
                    <hr class="separator">
                    <div class="sit-wrapper">
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Freight Forwarder</p>
                                <p class="sit-single-value"><?php echo (!empty($freightForwarder)) ? $freightForwarder : ""; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">CHA</p>
                                <p class="sit-single-value"><?php echo (!empty($CHA)) ? $CHA : ""; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">POL</p>
                                <p class="sit-single-value"><?php echo (!empty($pol)) ? $pol : ""; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">POD</p>
                                <p class="sit-single-value"><?php echo (!empty($pod)) ? $pod : ""; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Liner Name</p>
                                <p class="sit-single-value"><?php echo (!empty($liner)) ? $liner : ""; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Vessel Name</p>
                                <p class="sit-single-value"><?= $container_details['vessel_name']; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Flag</p>
                                <p class="sit-single-value"><?php echo (!empty($flag)) ? $flag : ""; ?></p>
                            </div>
                        </div>
                        <div class="sit-row">  
                            <div class="sit-single">
                                <p class="sit-single-title">Actual date of Departure</p>
                                <p class="sit-single-value"><?= !empty($container_details['revised_etd'])?date('d-M-Y', strtotime($container_details['revised_etd'])):''; ?></p>
                            </div> 
                            <div class="sit-single">
                                <p class="sit-single-title">Actual date of Arrival</p>
                                <p class="sit-single-value"><?= !empty($container_details['revised_eta'])?date('d-M-Y', strtotime($container_details['revised_eta'])):''; ?></p>
                            </div> 
                            <div class="sit-single">
                                <p class="sit-single-title">Actual Transit Time</p>
                                <p class="sit-single-value"><?= $container_details['revised_ett']; ?> Days</p>
                            </div>  
                            <div class="sit-single">
                                <p class="sit-single-title">Vessel Tracker URL</p>
                                <p class="sit-single-value"><?= (!empty($container_details['vessel_tracker_url'])) ? '<a href="'.$container_details['vessel_tracker_url'].'" title="'.$container_details['vessel_tracker_url'].'" target="_blank"><u>Click Here</u></a>' : ""; ?></p>
                            </div> 
                            <div class="sit-single">
                                <p class="sit-single-title">Liner Tracker URL</p>
                                <p class="sit-single-value"><?= (!empty($container_details['liner_tracker_url'])) ? '<a href="'.$container_details['liner_tracker_url'].'" title="'.$container_details['liner_tracker_url'].'" target="_blank"><u>Click Here</u></a>' : ""; ?></p>
                            </div> 
                            <div class="sit-single">
                                <p class="sit-single-title">Lot Number</p>
                                <p class="sit-single-value"><?= (!empty($container_details['lot_number'])) ? $container_details['lot_number'] : ""; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">POD Terminal</p>
                                <p class="sit-single-value"><?= (!empty($container_details['terminal'])) ? $container_details['terminal'] : ""; ?></p>
                            </div>
                        </div>
                        <div class="sit-row"> 
                            <div class="sit-single">
                                <p class="sit-single-title">Customer Alias</p>
                                <p class="sit-single-value"><?= (!empty($customerAlias)) ? $customerAlias : "";?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Vendor Alias</p>
                                <p class="sit-single-value"><?= (!empty($supplierAlias)) ? $supplierAlias : "";?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Shipping Container</p>
                                <p class="sit-single-value"><?= (!empty($container_details['shipping_container'])) ? $container_details['shipping_container'] : "Waiting for FCR"; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">SGS Seal #</p>
                                <p class="sit-single-value"><?= (!empty($container_details['sgs_seal'])) ? $container_details['sgs_seal'] : "Waiting for FCR"; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">FF Seal #</p>
                                <p class="sit-single-value"><?= (!empty($container_details['ff_seal'])) ? $container_details['ff_seal'] : "Waiting for FCR"; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">HBL Number #</p>
                                <p class="sit-single-value"><?= $hblNumber ?></p>
                            </div> 
                            <div class="sit-single">
                                <p class="sit-single-title">Internal HBL #</p>
                                <p class="sit-single-value"><?=$IHBL ?></p>
                            </div> 
                        </div>
                        <div class="sit-row">
                            <div class="sit-single">
                                <p class="sit-single-title">Supplier Contract #</p>
                                <p class="sit-single-value"><?= $supplierContact; ?></p>
                            </div>
                            <div class="sit-single">
                                <p class="sit-single-title">Master Contract</p>
                                <p class="sit-single-value"><?= (isset($masterContract)) ? $masterContract : "";?></p>
                            </div> 
                            <div class="sit-single">
                                <p class="sit-single-title">Brand</p>
                                <p class="sit-single-value"><?= (!empty($brandName)) ? $brandName : "";?></p>
                            </div> 
                        </div> 
                        <div class="sit-row">
                            <div>
                                <p class="sit-single-title">Notes</p>
                                <p class="sit-single-value"><?= (!empty($container_details['notes'])) ? $container_details['notes'] : ""; ?></p>
                            </div> 
                        </div>
                    </div>
                </div>
                </div>
                <div class="shipping-tabs-wrapper">
                    <div class="mro-tabs-container">
                        <nav>
                            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-documents-tab" data-toggle="tab" href="#nav-documents" role="tab" aria-controls="nav-documents" aria-selected="true">Documents</a>
                                <a class="nav-item nav-link" id="nav-linked-containers-tab" data-toggle="tab" href="#nav-linked-containers" role="tab" aria-controls="nav-linked-containers" aria-selected="true">Linked Containers</a>
                                <a class="nav-item nav-link " id="nav-products-tab" data-toggle="tab" href="#nav-products" role="tab" aria-controls="nav-products" aria-selected="false">Products</a>
                                <a class="nav-item nav-link " id="nav-invoices-tab" data-toggle="tab" href="#nav-invoices" role="tab" aria-controls="nav-invoices" aria-selected="false">Invoices</a>
                                <a class="nav-item nav-link" id="nav-addresses-tab" data-toggle="tab" href="#nav-addresses" role="tab" aria-controls="nav-addresses" aria-selected="false">Addresses</a>
                            </div>
                        </nav>
                        <div class="tab-content" id="nav-tabContent"> 
                            <div id="documents-loader" style="display: none;text-align: center;">
                                <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                            </div>
                            <div class="tab-pane fade show active" id="nav-documents" role="tabpanel" aria-labelledby="nav-documents-tab"> 
                                
                            </div>
                            <div class="tab-pane fade" id="nav-linked-containers" role="tabpanel" aria-labelledby="nav-linked-containers-tab">
                                <input type="hidden" id="link_container_cnt" value="0">
                                <div class="tab-content-wrapper" id="link_container_details">
                                    
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-products" role="tabpanel" aria-labelledby="nav-products-tab">
                                <div class="tab-content-wrapper" id="product_details">
                                    <input type="hidden" id="product_cnt" value="0"> 
                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-invoices" role="tabpanel" aria-labelledby="nav-invoices-tab">
                                <input type="hidden" id="invoices_cnt" value="0">
                                <div id="invoices_details">

                                </div>
                            </div>
                            <div class="tab-pane fade" id="nav-addresses" role="tabpanel" aria-labelledby="nav-addresses-tab">
                                <input type="hidden" id="address_cnt" value="0">
                                <div id="address_details">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 

<!-- Upload Document-->
<div class="overlay-document-details"></div> 
<div id="upload-document-first-modal" style="display:none" id="cnt1">
    <!-- Select document type -->
    <div class="mro-modal-header">
        <a href="javascript:void(0)" class="mro-close-modal">
            <img src="<?php echo base_url(); ?>assets/images/modal-close-dark.svg" onclick="closeModel()" alt="Close">
        </a>
    </div>
    <!-- <div class="fortoastrLoader"></div> -->
   <!-- <div class="toast-container" style="border:1px solid black"></div> -->
    <div class="doc-type-wrapper upload-fri-step-1" >
      <h3 class="form-group-title">Upload Document</h3>
      <div class="form-radio-wrapper">
        <div class="form-radio">
          <label class="container-radio">Document
            <input type="radio" checked="checked" name="upload_type" value="upload-document">
            <span class="checkmark"></span>
          </label>
        </div>
        <div class="form-radio">
          <label class="container-radio">Invoice
            <input type="radio" name="upload_type" value="upload-invoice">
            <span class="checkmark"></span>
          </label>
        </div>
      </div>
      <div class="form-cta">
        <button class="btn-primary-mro fri-step-1">Next</button>
      </div>
    </div>
    <!-- Select document type -->
</div>

<div id="upload-doc-content" class="mro-modal-wrapper-document-details mro-upload-doc-modal-cls" style="display:none;min-width:100%;max-width:100%;height: 1800px;">

    <!-- Upload FRI/LS Step 2 -->
    <div class="mro-modal-header">
        <a href="javascript:void(0)" class="mro-close-modal">
            <img src="<?php echo base_url(); ?>assets/images/modal-close-dark.svg" onclick="closeModel()" alt="Close">
        </a>
    </div>
   
  <div class="ud-step upload-fri-step-2" id="cnt2">
    <div class="upload-doc-title">
      <div class="ud-title-left">
        <a href="#/" class="fri-step-2-back"><img src="assets/images/ud-back.svg" alt="" onclick="backDocument('cnt1','cnt2')"></a>
        <h3>Container Listing</h3>
      </div>
    </div>
    <form action="" id="documentStep2Details" enctype="multipart/form-data"  method="post">
      <div class="select-doc-wrapper">
        <div class="sd-left">
          <span id="document_type_div">
            <h4 class="sd-subtitle">Document Type</h4>
            <select id="document_type" name="document_type" class="select-doc-type select-form-mro mb20" onchange="getLsListing(this.value)" required>
                <?php if($documentType){ ?>
                    <option value="">Select Document Type</option>
                    <?php foreach ($documentType as $key => $value) { ?>
                        <option value="<?= $value['document_type_id'] ?>"><?= $value['document_type_name']?></option>
                    <?php }

                } ?>
            
              </select>
          </span>
            
          <span id="business_partner_div">
             <h4 class="sd-subtitle">Business Partner</h4>
              <select id="business_partner" name="business_partner" class="select-doc-type select-form-mro mb20" onchange="getLsListing('invoice')" >
                  <?php if($business_partner){ ?>
                      <option value="">Select Business Partner</option>
                      <?php foreach ($business_partner as $key => $value) { ?>
                          <option value="<?= $value['business_partner_id'] ?>"><?= $value['alias']?></option>
                      <?php }
                  } ?>
              </select>
          </span> 
		  
          <div id="Ls-listing"> 
             <!-- this whole div while replace once user choose the document type other than FR LS dynamically via ajax call ls LISTING will appear than -->
            <h4 class="sd-subtitle">Selected Container</h4>
            <?php echo (!empty($container_details['container_number'])) ? $container_details['container_number'] : ""; ?>
            <input type="hidden" name="container_ids[]" id="containerno" value="<?php echo (!empty($container_details['container_id'])) ? $container_details['container_id'] : ""; ?>">            
            
          </div>
        </div>
        <div class="sd-right" id="containterListingForOtherDocument">
          <p class="select-message">Choose containers to upload documents</p>
        </div>
      <div class="upload-doc-next">
        <button type="submit" class="btn-primary-mro fri-step-2">Next</button>
      </div>
      </div>
    </form>                  
  </div>
  <!-- Upload FRI/LS Step 2 -->
  <!-- Upload FRI/LS Step 3 -->
  
   <div class="ud-step upload-fri-step-3" id="cnt3">
      <form action=""  id="documentStep3Details" enctype="multipart/form-data" method="post">
      <input type="hidden" name="document_type_id_hidden" id="document_type_id_hidden" value="0">
      <div class="upload-doc-title">
        <div class="ud-title-left">
          <a href="#/" class="fri-step-3-back"><img src="assets/images/ud-back.svg" alt="" onclick="backDocument('cnt2','cnt3')"></a>
          <h3><span id="choose_container">Choose Containers</span> </h3>
        </div>
        <!-- <div class="ud-title-right">
          <a href="#/" class="btn-primary-mro">Back</a>
        </div> -->
      </div>
      <div class="select-doc-wrapper">
        <div class="overlay-document-modal" style="display: none;"></div>
        <div class="sd-left">
          <div>
            <span id="selectedLsName"></span>  
          </div>
          <h4 class="sd-subtitle">
            Selected Containers</h4>
          <div class="ud-list mt20">
            <div id="selectedContainer"></div>     
          </div>
        </div>
        <div class="sd-right" >
          <div id="docuement_loader" style="display: none;">
            <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
          </div>
          <div id="Lsdocumentupload">
            <h4 class="sd-subtitle mb20">Upload Document</h4>
            <input type="file" name="step3_document" id="step3_document">
          </div>
          <!-- <span id="step3_document_upload_error" class="error"></span> -->
        </div>
      </div>
      <div class="upload-doc-next">
        <button  type="submit"  class="btn-primary-mro fri-step-3">Next</button>
      </div>
      </form>
    </div>
  
  <!-- Upload FRI/LS Step 3 -->
  <!-- Upload FRI/LS Step 4 -->
  <div class="ud-step upload-fri-step-4" id="cnt4">
    <div class="upload-doc-title">
      <div class="ud-title-left">
        <a href="#/" class="fri-step-4-back"><img src="assets/images/ud-back.svg" alt="" onclick="backDocument('cnt3','cnt4')"></a>
        <h3>Upload Document</h3>
      </div>
      <!-- <div class="ud-title-right">
        <a href="#/" class="btn-primary-mro">Back</a>
      </div> -->
    </div>
    <div class="select-doc-wrapper">
      <div class="sd-left">
        <h4 class="sd-subtitle">Containers</h4>
        <div class="ud-list ud-list-doc-names mt20" id="step4_selectedContainerDocument"></div>
      </div>
      <div class="sd-right">
        <h4 class="sd-subtitle">
          <span id="step4_uploadedContainerDocumentName">FRI Title</span></h4>
        <div class="ud-list ud-list-doc-names mt20" id="step4_selectedContainerUploadedDocument">
        </div>
      </div>
    </div>
    <div class="upload-doc-next">
      <button class="btn-primary-mro fri-step-4">Next</button>
    </div>
  </div>
  <!-- Upload FRI/LS Step 4 -->
  <!-- Upload FRI/LS Step 5 -->
  <div class="ud-step upload-fri-step-5" id="cnt5">
    <form id="documentStep5Details" enctype="multipart/form-data" method="post">
      <div class="upload-doc-title">
        <div class="ud-title-left">
          <a href="#/" class="fri-step-5-back"><img src="assets/images/ud-back.svg" alt="" onclick="backDocument('cnt4','cnt5')"></a>
          <h3>Review Document Titles</h3>
        </div>
        <div class="ud-title-right">
          <button class="btn-grey-mro">Clear</button>
          <button class="btn-primary-mro">Save</button>
        </div>
      </div>
      <div class="select-doc-wrapper">
        <div class="sd-left" >
          <h4 class="sd-subtitle mb20">Enter Custom Data</h4>
            <div id="custom_data_field"></div>              
        </div>
          <div class="sd-right">
          <h4 class="sd-subtitle">Form View</h4>
           <span id="form-view"></span>             
        </div>
      </div>
      <div class="upload-doc-next">
        <button class="btn-grey-mro">Clear</button>
        <button type="submit"  class="btn-primary-mro">Save</button>
      </div>
    </form>
  </div>
  <!-- Upload FRI/LS Step 5 -->
</div>
<!-- Upload Document--> 

<!-- custom modal -->
<div class="overlay"></div>
<div class="mro-modal-wrapper mro-modal-wrapper-document mro-upload-doc-modal-cls" style="min-width:100%;max-width:100%;/* height: 1800px; */">
    <div class="mro-modal-header">
        <a href="#/" class="mro-close-modal">
            <img src="<?php echo base_url(); ?>assets/images/modal-close-dark.svg" alt="Close">
        </a>
    </div>
    <div class="mro-modal-body">
        <div class="ud-step upload-fri-step-1">
            <div class="upload-doc-title">
                <div class="ud-title-left"> 
                    <h3>Invoice Detail View</h3>
                </div>
            </div>
            <div class="select-doc-wrapper">
                <div class="sd-left">
                    <!--          <h4 class="sd-subtitle mb20">Enter Custom Data</h4>-->
                    <div id="invoice-data-view"></div>
                </div>
                <div class="sd-right">
<!--                    <h4 class="sd-subtitle">Form View</h4>-->
                    <span id="invoice-form-view"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- custom modal -->  

<!-- custom modal -->
<div class="overlay-document"></div>
<div class="mro-modal-wrapper-document mro-upload-doc-modal-cls" style="min-width:100%;max-width:100%;/* height: 1800px; */">
    <form id="documentStep5" enctype="multipart/form-data" method="post">
        <div class="mro-modal-header">
            <a href="#/" class="mro-close-modal">
                <img src="<?php echo base_url(); ?>assets/images/modal-close-dark.svg" onclick="(function(){location.reload(); return false;})();return false;" alt="Close">
            </a>
        </div>
        <div class="mro-modal-body">
            <div class="ud-step upload-fri-step-1">
                <div class="upload-doc-title">
                    <div class="ud-title-left"> 
                        <h3 class="form-title">Review Document Titles</h3>
                    </div>
                    <div class="ud-title-right document-save"></div>
                </div> 
                
                <div class="select-doc-wrapper">
                    <div class="sd-left" >
                        <h4 class="sd-subtitle mb20 page-title">Enter Custom Data</h4>
                        <div id="custom_data_field_document"></div>
                    </div>
                    <div class="sd-right">
<!--                        <h4 class="sd-subtitle"></h4>-->
                        <span id="form-view-document"></span>
                    </div>
                </div>
                <div class="upload-doc-next document-save"></div>
            </div>
        </div>
    </form>
</div>
<!-- custom modal --> 

</body>

</html>

<script> 
    function openDocModal(){
        $('.overlay-document-details').show();       
        $('#upload-document-first-modal').show(); 
        $.ajax({
            url: "<?= base_url('container_details/setCurrentSessionId')?>",
            type: "POST",
            dataType: "json",
            success: function(response){ 
                console.log(response.msg);   
            }
        });        
    } 
    
    function backDocument(showId,hideId){
        $.each($("input[name='upload_type']:checked"), function(){
            if($(this).val()=='upload-invoice'){
                if(showId=='cnt4'){
                    $('#cnt3').show();
                    $('#'+hideId).hide();
                }else{
                  if(showId == 'cnt1'){
                    $("#upload-doc-content").hide();
                    $("#upload-document-first-modal").show();
                    $(".upload-fri-step-1").show();
                  }
                    $('#'+showId).show();
                    $('#'+hideId).hide();
                }
            }else{
                if(showId == 'cnt1'){
                  $("#upload-doc-content").hide();
                  $("#upload-document-first-modal").show();
                  $(".upload-fri-step-1").show();
                  // $("#uupload-document-first-modal").show();
                  
                }else{
                  $('#'+showId).show();
                  $('#'+hideId).hide();
                }
            }             
        });
    }
    
    $(document).ready(function () {  
        
         var vRules = {
            "document_type": {required: true}
        };
        var vMessages = {
            "document_type": {required: "Please Enter Document Type."}
        };
        $("#documentStep2Details").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>container_details/submitFormStep2Details";
                
                $("#documentStep2Details").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        // $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        console.log(response); 
                        showInsertUpdateMessage(response.msg,response);    
                        if (response.success) {
                            if(response.type == 'LS'){
                              getSelectedContainer(response.type);
                            }else{
                              getSelectedLsContainer(response.type);
                            }
                            $('.upload-fri-step-2').hide();
                            $('.upload-fri-step-3').show();
                            $('#document_type_id_hidden').val(response.doc_id);                            
                        } else {  
                            return false;
                        }
                    }
                });
            }
        });

        // for step 2 submission 
        var vRules = {
            "step3_document": {required: true}
        };
        var vMessages = {
            "step3_document": {required: "Please Select the document for upload"}
        };
        //check and save country
        $("#documentStep3Details").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>container_details/submitFormStep3Details";
                
                $("#documentStep3Details").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                        $("#docuement_loader").show();
                        $(".overlay-document-modal").show();
                    },
                    success: function (response) { 
                        $(".btn-primary-mro").show();
                        $("#docuement_loader").hide();
                        $(".overlay-document-modal").hide();
                        console.log(response);
                        showInsertUpdateMessage(response.msg,response);
                        if (response.success) { 
                            if(response.type == 'LS' || response.type == ''){
                              getSelectedContainerDocument(response.type); 
                              getSelectedCustomField(response.type);
                              $('.upload-fri-step-3').hide();
                              $('.upload-fri-step-4').show();
                            }else{
                              getSelectedCustomField(response.type);
                              if(response.type == 'invoice'){
                                $('.upload-fri-step-5').show();
                                $('.upload-fri-step-3').hide();

                              }
                            }
                            
                        } else {
                            $("#step3_document_upload_error").html(response.msg);
                            return false;
                        }
                    }
                });
            }
        });

        //check and save country
        $("#documentStep5Details").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>container_details/submitFormStep5Details";
                $("#documentStep5Details").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        // $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        //Document successfully uploaded to containers
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) {
                            setTimeout(function () {                                
                               window.location = "<?= base_url()?>container_details?text='<?= rtrim(strtr(base64_encode("id=".$container_details['container_id']), '+/', '-_'), '=')?>'"; 
                            }, 1000);
                        }else{
                            return false;
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });
        
        function getSelectedContainer(){
          $.ajax({
            url: "<?= base_url('container_details/getSelectedContainer')?>",
            type: "POST",
            dataType: "json",
            success: function(response){ 
                showInsertUpdateMessage(response.msg,response);                        
                if (response.success) { 
                   $("#selectedContainer").html(response.html);
                }  
                $(".btn-primary-mro").show(); 
              
            }
          }); 
        }
        
        function getSelectedLsContainer(type){
          $.ajax({
            url: "<?= base_url('container_details/getSelectedLsContainer')?>",
            type: "POST",
            dataType: "json",
            data:{type},
            success: function(response){
              if(response.success){
                $("#selectedContainer").html(response.html);
                $("#selectedLsName").html(response.selectedLsName);
                $("#choose_container").html("Choose LS");
                $("#Lsdocumentupload").html(response.uploadSectionhtml);
              }else{   
                showInsertUpdateMessage(response.msg,response);
              }
              
            }
          }); 
        }

        function getSelectedContainerDocument(type){
          $.ajax({
            url: "<?= base_url('container_details/getSelectedContainerDocument')?>",
            type: "POST",
            dataType: "json",
            data :{type},
            success: function(response){
              if(response.success){
                $("#step4_uploadedContainerDocumentName").html(response.document_prefix_name + ' Title '); 
                $("#step4_selectedContainerDocument").html(response.html_container);
                $("#step4_selectedContainerUploadedDocument").html(response.html_uploaded); 
              }else{   
                showInsertUpdateMessage(response.msg,response);
              }
              
            }
          }); 
        }
        

        function getSelectedCustomField(type){
          $.ajax({
            url: "<?= base_url('container_details/getSelectedCustomField')?>",
            type: "POST",
            dataType: "json",
            data :{type},
            success: function(response){
              if(response.success){
                $("#custom_data_field").html(response.customField);
                $("#form-view").html(response.formView);
                $('.basic-single').select2({
                  searchInputPlaceholder: 'Search'
                });
              }else{   
                showInsertUpdateMessage(response.msg,response);
              }
              
            }
          }); 
        }
        
        
        
        var vRules1 = {};
        var vMessages1 = {};
        
        $("#documentStep5").validate({
            rules: vRules1,
            messages: vMessages1,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>container_details/submitFormStep5";
                $("#documentStep5").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".save-record").hide();
                    },
                    success: function (response) {
                        $(".save-record").show();
                        console.log(response); 
                        showInsertUpdateMessage(response.msg,response,1000);
                        if(response.type='reupload'){
                            setTimeout(function(){ 
                                location.reload(); 
                            }, 1000);
                        } 
                    }
                });
            }
        });
        
        
        $('#nav-documents-tab').on('click', function () {
            getDocumentDetails();
        }) 
        getDocumentDetails();
        
     
        $('#nav-products-tab').on('click', function () {
            var product_cnt = $('#product_cnt').val();
            //if (product_cnt == 0) {
                var container_id = $('#container_id').val();
                var act = "<?php echo base_url(); ?>container_details/getProductDetails";
                $.ajax({
                    type: 'GET',
                    url: act,
                    data: {
                        container_id: container_id
                    },
                    dataType: "json",
                    success: function (data) {
                        $('#product_details').html(data.productDetails);
                        $('#product_cnt').val('1');
                    }
                });
            //}
        })

        $('#nav-addresses-tab').on('click', function () {
            var address_cnt = $('#address_cnt').val();
            //if (address_cnt == 0) {
                var container_id = $('#container_id').val();
                var act = "<?php echo base_url(); ?>container_details/getAddressDetails";
                $.ajax({
                    type: 'GET',
                    url: act,
                    data: {
                        container_id: container_id
                    },
                    dataType: "json",
                    success: function (data) {
                        $('#address_details').html(data.productDetails);
                        $('#address_cnt').val('1');
                    }
                });
            //}
        })

        $('#nav-invoices-tab').on('click', function () {
            //var invoices_cnt = $('#invoices_cnt').val();
            //if(invoices_cnt==0){
            var container_id = $('#container_id').val();
            var act = "<?php echo base_url(); ?>container_details/getInvoiceDetails";
            $.ajax({
                type: 'GET',
                url: act,
                data: {
                    container_id: container_id
                },
                dataType: "json",
                success: function (data) {
                    $('#invoices_details').html(data.invoiceDetails);
                    $('#invoices_cnt').val('1'); 
                    $('.datepicker').datepicker({
                        format: 'dd-mm-yyyy'
                    });
                }
            });
            //}
        })
        
        
        $('#nav-linked-containers-tab').on('click', function () {
            var link_container_cnt = $('#link_container_cnt').val();
            //if(link_container_cnt==0){
                var container_id = $('#container_id').val();
                var act = "<?php echo base_url(); ?>container_details/getLinkContainerDetails";
                $.ajax({
                    type: 'GET',
                    url: act,
                    data: {
                        container_id: container_id
                    },
                    dataType: "json",
                    success: function (data) {
                        $('#link_container_details').html(data.linkDetails);
                        $('#link_container_cnt').val('1');
                    }
                });
            //}
        }) 
    }); 
    
      //  start  these functions are for othere document type 
        
    function getLsListing(document_type_id){
      console.log(document_type_id);
        if(document_type_id){
          if(!['2','3'].includes(document_type_id) ||  document_type_id == 'invoice'){
              // to get the LS from tbl for this type of document 
              $.ajax({
                url: "<?= base_url('container_details/getLsListing')?>",
                type: "POST",
                dataType: "json",
                data:{document_type_id:document_type_id,container_id:$('#container_id').val()},
                success: function(response){
                   console.log(response);
                  if(response.success){
                    $("#Ls-listing").html(response.html);
                    $("#containterListingForOtherDocument").html(response.rightSideMsg);
                  }else{   
                    showInsertUpdateMessage(response.msg,response);
                    $("#Ls-listing").html(response.html);
                    $("#containterListingForOtherDocument").html(response.rightSideMsg);
                  } 
                }
              }); 
          } else {
            // call for container listing for  fr n LS 
              $.ajax({
                url: "<?= base_url('container_details/getContainerListing')?>",
                type: "POST",
                dataType: "json",
                data:{document_type_id:document_type_id,container_id:$('#container_id').val()},
                success: function(response){
                  if(response.success){
                    $("#Ls-listing").html(response.html);
                    $("#containterListingForOtherDocument").html(response.rightSideMsg);
                  }else{   
                    showInsertUpdateMessage(response.msg,response);
                    $("#Ls-listing").html(response.html);
                    $("#containterListingForOtherDocument").html(response.rightSideMsg);
                  } 
                }
              }); 
          }

        }else{  
          showInsertUpdateMessage("Please select the document type!",false);
        }
      }
      
      
     function closeModel(){
        if(confirm("Are you sure you want to leave this process?") ){ 
            $("#upload-doc-content").modal("hide");            
            $("#upload-document-first-modal").hide();
            $("#upload-document-first-modal").modal("hide");
            $.ajax({
                url: "<?= base_url('container_details/deleteUncompletedUpload')?>",
                type: "POST",
                dataType: "json",
                success: function(response){
                  location.reload();
                  // if(response.success){
                  //   location.reload();
                  // }else{  
                  //   location.reload();
                  // } 
                }
              });  
        } 
    }

      function getvalue(val) {
        if (val) {
            $("#ls_number").val(val);
            var test = $("#ls_number" + val).text();
            $('.sd-value').html(test);
            var document_type_id = $("#document_type").val();
            // console.log(document_type_id);
            // return false;
            // call ajax to get the container which is belonging to this LS number 
            $.ajax({
                url: "<?= base_url('container_details/getLSContainerListing')?>",
                type: "POST",
                dataType: "json",
                data: {val:val,document_type_id:document_type_id,container_id:$('#container_id').val()},
                success: function(response){
                  if(response.success){
                    $("#containterListingForOtherDocument").html(response.html);
                  }else{  
                    showInsertUpdateMessage(response.msg,response);    
                    $("#containterListingForOtherDocument").html(response.html);
                  } 
                }
              }); 

        } else {  
          showInsertUpdateMessage("Select the value first",false);
        }
    }


    var sku = [];
    function getSku(sku_id){
      if(sku_id){
        if(sku.includes(sku_id.toString())){
          showInsertUpdateMessage("Data is already added in below list",false);
          return false;
          }else{
          sku.push(sku_id);
          console.log("new value");
        } 
        console.log(sku);
        if(sku_id){
          $.ajax({
                  url: "<?= base_url('listOfContainer/getsku')?>",
                  type: "POST",
                  dataType: "json",
                  data: {sku_id},
                  success: function(response){
                    if(response.success){
                      $("#sku_div").append(response.htmlsku);
                      // $("#product_desc").html(response.product_desc);
                      // $("#item_size").html(response.item_size);
                    }else{  
                      // alert(response.msg)
                      // $("#").html(response.html);
                    } 
                  }
                }); 
        }else{ 
          showInsertUpdateMessage("Select sku value",false);
          $("#item_code").html('');
          $("#product_desc").html('');
          $("#item_size").html('');
        }
      }
    }
    
    function getPersonDetail(supplier_id,container_number){
      console.log(container_number);
      if(supplier_id){
          $.ajax({
                  url: "<?= base_url('container_details/getPersonDetail')?>",
                  type: "POST",
                  dataType: "json",
                  data: {supplier_id},
                  success: function(response){
                    if(response.success){
                      $("#contactpersonname"+container_number).val(response.contactpersonname);
                      $("#contactpersonnumber"+container_number).val(response.contactpersonnumber);
                      // $("#product_desc").html(response.product_desc);
                      // $("#item_size").html(response.item_size);
                    }else{  
                      // alert(response.msg)
                      // $("#").html(response.html);
                    } 
                  }
                }); 
        }else{
          alert("No Supplier selected");
        }
    }

    function removeSku(sku_id){
      sku = sku.filter(function(item) {
          return item !== sku_id
      })
      $("#singleSkuDiv"+sku_id).remove();
    }
      //  end  these functions are for othere document type 
    
    function editableDetails(container_id){
        if(container_id > 0){ 
            var act = "<?php echo base_url(); ?>container_details/getEditableForm";
            $.ajax({
                type: 'GET',
                url: act,
                data: {
                    container_id: container_id
                },
                dataType: "json",
                success: function (data) {
                    $('#container-editable-div').html(data.html); 
                }
            }); 
        }
    }
        
    function getDocumentDetails(){  
        $("#documents-loader").show();
        var container_id = $('#container_id').val();
        var act = "<?php echo base_url(); ?>container_details/getDocumentDetails";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                container_id: container_id
            },
            dataType: "json",
            success: function (data) {
                $("#documents-loader").hide();
                $('#nav-documents').html(data.documentDetails); 
            }
        }); 
    } 

    function showUpdateButton(cnt) {
        $('#cs-status-update' + cnt).show();
        $('#cs-status-text' + cnt).hide();
    }

    function updateInvoiceDetails(id, cnt) {
        var invoice_date = $('#invoice_date' + cnt).val();
        var invoice_status = $('#invoice_status' + cnt).val();
        var paid_date = $('#paid_date' + cnt).val();
        var invoice_value = $('#invoice_value' + cnt).val();
        var act = "<?php echo base_url(); ?>container_details/updateInvoiceDetails";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                id: id,
                invoice_date: invoice_date,
                invoice_status: invoice_status,
                paid_date: paid_date,
                invoice_value: invoice_value
            },
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $('#cs-status-update' + cnt).hide();
                    $('#cs-status-text' + cnt).show();
                    $('#last-update-title' + cnt).hide();
                    $('#last-update-name' + cnt).hide();
                    $('#success-msg' + cnt).show();
                    $('#success-msg' + cnt).html(data.msg);
                }
            }
        });
    }

    function showInvoiceDetails(id) {
        $('.overlay,.mro-modal-wrapper').show();
        var act = "<?php echo base_url(); ?>container_details/getInvoiceDetailsView";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                id: id
            },
            dataType: "json",
            success: function (data) {
                $('#invoice-data-view').html(data.invoiceDetails);
                $('#invoice-form-view').html(data.invoicePDF); 
                $('.datepicker').datepicker({
                    format: 'dd-mm-yyyy'
                });
            }
        });
    }
    
    function showDocumentDetails(document_type_id=0,container_id=0,document_number='',status='') {
        $('.overlay-document,.mro-modal-wrapper-document').show();
        $('body').css({'overflow-y':'hidden'});
        var act = "<?php echo base_url(); ?>container_details/getUploadDetailsView";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                document_type_id: document_type_id,container_id:container_id,document_number:document_number,status:status
            },
            dataType: "json",
            success: function (data) {
                $('#custom_data_field_document').html(data.custom_field);
                $('#form-view-document').html(data.form_view);
                $('.document-save').html(data.savehtml);
                $('.basic-single').select2();
            }
        });
    }
    
    
    function reuploadDocument(document_type_id=0,container_id=0,document_number='',status='') {
        $('.overlay-document,.mro-modal-wrapper-document').show();
        var act = "<?php echo base_url(); ?>container_details/reuploadDocument";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                document_type_id: document_type_id,container_id:container_id,document_number:document_number,status:status
            },
            dataType: "json",
            success: function (data) {
                $('.form-title').html('Reupload Document');
                $('.page-title').html('Document');
                $('#custom_data_field_document').html(data.custom_field);
                $('#form-view-document').html(data.form_view);
                $('.document-save').html(data.savehtml);
            }
        });
    }
    
    
    function documentApprove(document_type_id=0,container_id=0,document_number='') { 
        if(confirm("Are you sure you want to approve?") ){ 
            var act = "<?php echo base_url(); ?>container_details/documentApprove";
            $.ajax({
                type: 'GET',
                url: act,
                data: {
                    document_type_id: document_type_id,container_id:container_id,document_number:document_number
                },
                dataType: "json",
                success: function (response) { 
                    $('.checkedClass').html('Checked');
                    $('.checkedClass').css('cursor','auto');  
                    showInsertUpdateMessage(response.msg,response);
                }
            }); 
        }
    }
    
    function showSaveButton(){
        $(".update-record").hide();
        $(".save-record").show(); 
        $(".readonly").attr("readonly", false); 
        $(".readonly").attr("disabled", false); 
    }  
    
    function showApprovedBy(id){
        var popup = document.getElementById("myPopup"+id);
        popup.classList.toggle("show");
    }
    
    
</script> 