<?php if ($productData) { ?>
    <h3 class="form-group-title">Product Details
        <?php
        if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) {
            ?>
            <img src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt="" id="qty-edit" style="cursor: pointer;">
        <?php }
        ?>
    </h3>
    <form action="" id="product-qty-form" method="post" enctype="multipart/form-data">
        <div class="bp-list-wrapper">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Sr. No.</th>
                            <th scope="col">Product Description</th>
                            <th scope="col">BM SKU</th>
                            <th scope="col">Manufacturer</th>
                            <th scope="col">Manufacturer SKU</th>
                            <th scope="col">Brand</th>
                            <th scope="col">Size</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">UoM</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        $srNo = 0;
                        foreach ($productData as $value) {
                            $srNo++;
                            ?>  
                            <tr>
                                <td><?= $srNo ?></td>
                                <td><?= $value['product_name'] ?></td>
                                <td><?= $value['sku_number'] ?></td>
                                <td><?= $value['alias'] ?></td>
                                <td><?= $value['sku_vendor'] ?></td>
                                <td><?= $value['brand_name'] ?></td>
                                <td><?= $value['size_name'] ?></td>
                                <td style="width:200px;">
                                    <input type="number" name="qty[<?= $value['container_sku_id'] ?>]" id="qty_<?= $value['container_sku_id'] ?>" value="<?= $value['quantity'] ?>" onkeyup="getContainerSKUQtyEnter('qty_<?= $value['container_sku_id'] ?>','<?=$value['sku_id'] ?>','<?=$value['order_id']?>');" placeholder="Quantity" class="input-form-mro product-qty" readonly aria-invalid="false">
                                </td>
                                <td><?= $value['uom_name'] ?></td>
                            </tr>  
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="title-sec-right" id="sku-qty-update">  
             
        </div>
    </form> 

<?php } ?> 
<script>
    $('#qty-edit').on('click', function () {
        $(".product-qty").attr("readonly", false);
        $("#sku-qty-update").html('<button type="submit" class="btn-primary-mro">Update</button>');
    });
    
    function getContainerSKUQtyEnter(id,sku_id,order_id){
        var container_id = $('#container_id').val();
        if(sku_id==0 || order_id==0){
            $('#'+id).val('0');
            return false;
        }
        var quantity = $('#'+id).val();
        
        var act = "<?php echo base_url(); ?>container/getContainerSKUQtyEnter";
        $.ajax({
            type: 'GET',
            url: act,
            data: {sku_id: sku_id, order_id: order_id, quantity: quantity,container_id:container_id},
            dataType: "json",
            success: function (response) {
                if(response.fail){
                    showInsertUpdateMessage(response.msg,false,1000);  
                    $('#'+id).val(response.remainingQty);
                }
            }
        });
    }

    $(document).ready(function () {
        var vRules = {};
        var vMessages = {};
        $("#product-qty-form").validate({
            rules: vRules,
            messages: vMessages,
            errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>container_details/updateContainerSKUQty";
                $("#product-qty-form").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        
                    },
                    success: function (response) {
                        showInsertUpdateMessage(response.msg, response);
                    }
                });
            }
        });

    });
</script>


