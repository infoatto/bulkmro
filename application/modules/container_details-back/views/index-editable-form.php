<form action="" id="editableForm" method="post" enctype="multipart/form-data">
<div class="title-sec-wrapper">
    <div class="title-sec-left">         
        <div class="page-title-wrapper">  
            <div class="form-group">
                <label for="">Container Number<sup>*</sup></label>
                <input type="text" name="container_number" id="container_number" value="<?php echo(!empty($container_details['container_number'])) ? $container_details['container_number'] : ""; ?>" placeholder="Enter Container Number" class="input-form-mro">
                <input type="hidden" name="container_id" id="container_id" value="<?php echo (!empty($container_details['container_id'])) ? $container_details['container_id'] : ""; ?>">
                <input type="hidden" name="old_container_status_id" id="old_container_status_id" value="<?php echo (!empty($containerStatusId)) ? $containerStatusId : "0"; ?>">
            </div> 
            <div class="nav-divider"></div>
            <div>
                <p class="container-status"> 
                    <select name="container_status_id" id="container_status_id" class="basic-single select-form-mro">
                        <option value="" disabled selected hidden>Select Container Status</option>
                        <?php
                        foreach ($containerStatusData as $value) { 
                            ?> 
                            <option value="<?php echo $value['container_status_id']; ?>" <?php
                            if ($value['container_status_id'] == $containerStatusId) {
                                echo "selected";
                            }
                            ?>>
                            <?php echo $value['container_status_name']; ?> 
                            </option> 
                            <?php
                        }
                        ?>
                    </select> 
                </p>
            </div> 
           
        </div>
    </div> 
    <div class="title-sec-right"> 
         <?php
        if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) {
            ?>
            <button type="submit" class="btn-primary-mro">Save</button>&nbsp&nbsp&nbsp
            <button type="button" class="btn-transparent-mro cancel">Cancel</button>
        <?php }
        ?>
    </div>
</div>
<div class="shipping-info-top">
    <h2 class="sec-title">Shipping Information</h2>
    <hr class="separator">
    <div class="sit-wrapper">
        <div class="sit-row">
            <div class="sit-single">
                <p class="sit-single-title">Freight Forwarder</p>
                <p class="sit-single-value"> 
                    <select name="freight_forwarder_id" id="freight_forwarder_id" class="basic-single select-form-mro">
                        <option value="">Select Freight Forwarder</option>
                        <?php
                        foreach ($freightForwarderData as $value) { 
                            ?> 
                            <option value="<?php echo $value['business_partner_id']; ?>" <?php
                            if ($value['business_partner_id'] == $freightForwarderId) {
                                echo "selected";
                            }
                            ?>>
                            <?php echo $value['alias']; ?> 
                            </option> 
                            <?php
                        }
                        ?>
                    </select> 
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">CHA</p>
                <p class="sit-single-value">
                    <select name="broker_id" id="broker_id" class="basic-single select-form-mro">
                        <option value="">Select CHA</option>
                        <?php
                        foreach ($CHAData as $value) { 
                            ?> 
                            <option value="<?php echo $value['business_partner_id']; ?>" <?php
                            if ($value['business_partner_id'] == $CHA_id) {
                                echo "selected";
                            }
                            ?>>
                            <?php echo $value['alias']; ?> 
                            </option> 
                            <?php
                        }
                        ?>
                    </select> 
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">POL</p>
                <p class="sit-single-value"> 
                    <select name="pol" id="pol" class="basic-single select-form-mro"> 
                        <option value="0">Select POL</option>
                        <?php
                        foreach ($pol as $value) {
                            $polID = (isset($container_details['pol']) ? $container_details['pol'] : 0);
                            ?> 
                            <option value="<?php echo $value['pol_id']; ?>"  <?php
                            if ($value['pol_id'] == $polID) {
                                echo "selected";
                            }
                            ?>>
                                        <?php echo $value['pol_name']; ?> 
                            </option> 
                            <?php
                        }
                        ?>
                    </select>  
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">POD</p>
                <p class="sit-single-value">
                    <select name="pod" id="pod" class="basic-single select-form-mro"> 
                        <option value="0">Select POD</option>
                        <?php
                        foreach ($pod as $value) {
                            $podID = (isset($container_details['pod']) ? $container_details['pod'] : 0);
                            ?> 
                            <option value="<?php echo $value['pod_id']; ?>"  <?php
                            if ($value['pod_id'] == $podID) {
                                echo "selected";
                            }
                            ?>>
                                        <?php echo $value['pod_name']; ?> 
                            </option> 
                            <?php
                        }
                        ?>
                    </select> 
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Liner Name</p>
                <p class="sit-single-value">
                    <select name="liner_name" id="liner_name" class="basic-single select-form-mro"> 
                        <option value="0">Select Liner Name</option>
                        <?php
                        foreach ($liner as $value) {
                            $linerID = (isset($container_details['liner_name']) ? $container_details['liner_name'] : 0);
                            ?> 
                            <option value="<?php echo $value['liner_id']; ?>"  <?php
                            if ($value['liner_id'] == $linerID) {
                                echo "selected";
                            }
                            ?>>
                                        <?php echo $value['liner_name']; ?> 
                            </option> 
                            <?php
                        }
                        ?>
                    </select>
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Vessel Name</p>
                <p class="sit-single-value">
                    <input type="text" name="vessel_name" id="vessel_name" value="<?php echo(!empty($container_details['vessel_name'])) ? $container_details['vessel_name'] : ""; ?>" placeholder="Enter Vessel Name" class="input-form-mro">  
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Flag</p>
                <p class="sit-single-value">
                    <select name="flag" id="flag" class="basic-single select-form-mro"> 
                        <option value="0">Select Flag</option>
                        <?php
                        foreach ($flag as $value) {
                            $flagID = (isset($container_details['flag']) ? $container_details['flag'] : 0);
                            ?> 
                            <option value="<?php echo $value['flag_id']; ?>"  <?php
                            if ($value['flag_id'] == $flagID) {
                                echo "selected";
                            }
                            ?>>
                                        <?php echo $value['flag_name']; ?> 
                            </option> 
                            <?php
                        }
                        ?>
                    </select>  
                </p>
            </div>
        </div>
        <div class="sit-row"> 
            <div class="sit-single">
                <p class="sit-single-title">Actual date of Departure</p>
                <p class="sit-single-value"> 
                    <input type="text" name="revised_etd" id="revised_etd" value="<?php echo(!empty($container_details['revised_etd'])) ? date('d-m-Y', strtotime($container_details['revised_etd'])) : date("m/d/Y"); ?>" onchange="fillETT();" class="form-control valid input-form-mro datepicker date-form-field">                 
                </p>
            </div> 
            <div class="sit-single">
                <p class="sit-single-title">Actual date of Arrival</p>
                <p class="sit-single-value">
                    <input type="text" name="revised_eta" id="revised_eta" value="<?php echo(!empty($container_details['revised_eta'])) ? date('d-m-Y', strtotime($container_details['revised_eta'])) : date("m/d/Y"); ?>" onchange="fillETT();" class="form-control valid input-form-mro datepicker date-form-field">                                          
                </p>
            </div>   
            
<!--            <div class="sit-single">
                <p class="sit-single-title">ETT In Days</p>
                <p class="sit-single-value"> <input type="text" readonly name="ett" id="ett" value="<?php echo(!empty($container_details['ett'])) ? $container_details['ett'] : ""; ?>" placeholder="No ETA or ETD Entered" class="input-form-mro"></p>                    
            </div>-->
            <div class="sit-single">
                <p class="sit-single-title">Actual Transit Time In Days</p>
                <p class="sit-single-value"><input type="text" readonly name="revised_ett" id="revised_ett" value="<?php echo(!empty($container_details['revised_ett'])) ? $container_details['revised_ett'] : ""; ?>" placeholder="No ETA or ETD Entered" class="input-form-mro"></p>
            </div>  
            <div class="sit-single">
                <p class="sit-single-title">Vessel Tracker URL</p> 
                <p class="sit-single-value"> 
                    <input type="text" name="vessel_tracker_url" id="vessel_tracker_url" value="<?php echo(!empty($container_details['vessel_tracker_url'])) ? $container_details['vessel_tracker_url'] : ""; ?>" placeholder="Vessel Tracker URL Entered" class="input-form-mro">
                </p>
            </div> 
            <div class="sit-single">
                <p class="sit-single-title">Liner Tracker URL</p> 
                <p class="sit-single-value"> 
                    <input type="text" name="liner_tracker_url" id="liner_tracker_url" value="<?php echo(!empty($container_details['liner_tracker_url'])) ? $container_details['liner_tracker_url'] : ""; ?>" placeholder="Liner Tracker URL Entered" class="input-form-mro">
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Lot Number</p>
                <p class="sit-single-value"> 
                    <input type="text" name="lot_number" id="lot_number" value="<?php echo(!empty($container_details['lot_number'])) ? $container_details['lot_number'] : ""; ?>" placeholder="Lot Number Entered" class="input-form-mro">                        
                </p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">POD Terminal</p>
                <p class="sit-single-value"><input type="text" name="terminal" id="terminal" value="<?php echo(!empty($container_details['terminal'])) ? $container_details['terminal'] : ""; ?>" placeholder="Terminal Entered" class="input-form-mro"></p>
            </div>

        </div>
        <div class="sit-row"> 
            <div class="sit-single">
                <p class="sit-single-title">Customer Alias</p>
                <p class="sit-single-value" style="margin-top:20px;"><?= (!empty($customerAlias)) ? $customerAlias : "";?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Vendor Alias</p>
                <p class="sit-single-value" style="margin-top:20px;"><?= (!empty($supplierAlias)) ? $supplierAlias : "";?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Shipping Container</p>
                <p class="sit-single-value" style="margin-top:20px;"><?= (!empty($container_details['shipping_container'])) ? $container_details['shipping_container'] : "Waiting for FCR"; ?></p>
            </div> 
            <div class="sit-single">
                <p class="sit-single-title">SGS Seal #</p>
                <p class="sit-single-value" style="margin-top:20px;"><?= (!empty($container_details['sgs_seal'])) ? $container_details['sgs_seal'] : "Waiting for FCR"; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">FF Seal #</p>
                <p class="sit-single-value" style="margin-top:20px;"><?= (!empty($container_details['ff_seal'])) ? $container_details['ff_seal'] : "Waiting for FCR"; ?></p>
            </div> 
            <div class="sit-single">
                <p class="sit-single-title">HBL #</p>
                <p class="sit-single-value" style="margin-top:20px;"><?= $hblNumber ?></p>
            </div> 
            <div class="sit-single">
                <p class="sit-single-title">Internal HBL #</p>
                <p class="sit-single-value" style="margin-top:20px;"><?=$IHBL ?></p>
            </div> 
        </div>
        <div class="sit-row">
            <div class="sit-single">
                <p class="sit-single-title">Supplier Contract #</p>
                <p class="sit-single-value" style="margin-top:20px;"><?= $supplierContact ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Master Contract</p>
                <p class="sit-single-value" style="margin-top:20px;"><?= (isset($masterContract)) ? $masterContract : "";?></p>
            </div> 
            <div class="sit-single">
                <p class="sit-single-title">Brand</p>
                <p class="sit-single-value" style="margin-top:20px;"><?= (!empty($brandName)) ? $brandName : "";?></p>
            </div> 
        </div>
        <div class="sit-row">
            <div>
                <p class="sit-single-title">Notes</p>
                <p class="sit-single-value">
                    <textarea class="form-control" name="notes" id="notes" rows="2" cols="121" placeholder="Enter Notes"><?php echo(!empty($container_details['notes'])) ? $container_details['notes'] : ""; ?></textarea> 
                </p>
            </div>
        </div>
        
    </div>
</div>
</form> 

<script>
$('.datepicker').datepicker({
    format: 'dd-mm-yyyy'
});
</script> 
<script> 
    $(document).ready(function () {  
        var vRules = {
            "container_number": {required: true} 
        };
        var vMessages = {
            "container_number": {required: "Please enter container number."} 
        }; 
        $("#editableForm").validate({
            rules: vRules,
            messages: vMessages,
            errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function(error, e) {
              e.parents('.form-group').append(error);
            },
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>container_details/editableSubmitForm";
                $("#editableForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) {
                             setTimeout(function () {                                
                                //$('#container-editable-div').html(response.html);  
                                location.reload(); return false;
                            }, 2000); 
                        }else{
                            $(".btn-primary-mro").show();
                        } 
                    }
                });
            }
        });
        
    }); 
    
    
    $('.basic-single').select2({
        searchInputPlaceholder: 'Search'
    });
    
     function fillETT() {
        var revised_eta = $('#revised_eta').val();
        var revised_etd = $('#revised_etd').val();
        var act = "<?php echo base_url(); ?>container/getETTDays";
        $.ajax({
            type: 'GET',
            url: act,
            data: {revised_eta: revised_eta, revised_etd: revised_etd},
            dataType: "json",
            success: function (data) {
                //$('#ett').val(data.days);
                $('#revised_ett').val(data.days);
            }
        });
    }  
    
    $('.cancel').click(function(event){
        location.reload(); return false;
    }); 
</script>