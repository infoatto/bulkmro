<div class="title-sec-wrapper">
    <div class="title-sec-left">
        <div class="breadcrumb">
            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
            <span>></span>
            <a href="<?php echo base_url("listOfContainer"); ?>">All Containers</a>
            <span>></span>
            <p># <?= $container_details['container_number']; ?></p>
        </div>
        <div class="page-title-wrapper">
            <h1 class="page-title">Container # <?= $container_details['container_number']; ?></h1>
            <input type="hidden" name="container_id" id="container_id" value="<?php echo (!empty($container_details['container_id'])) ? $container_details['container_id'] : ""; ?>">
            <div class="nav-divider"></div>
            <div class="cs-status-text">
                <p class="container-status"><?php echo (!empty($show_details['containerStatus'])) ? $show_details['containerStatus'] : ""; ?></p>
            </div>
            <div class="cs-status-text" style="flex: 0 0 160px;max-width: none;">
                <p class="last-update-title">Last Updated</p>
                <p class="last-update-name"><?php echo (!empty($show_details['updateBy'])) ? $show_details['updateBy'] : ""; ?></p>
            </div>
            <?php
            if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) {
            ?>
            <img onclick="editableDetails('<?php echo (!empty($container_details['container_id'])) ? $container_details['container_id'] : "0"; ?>');" src="<?php echo base_url(); ?>assets/images/edit-icon.svg" alt="">
            <?php }
            ?>
        </div>
    </div> 
    <div class="title-sec-right">
        <a href="#/" class="btn-primary-mro"><img src="<?php echo base_url(); ?>assets/images/upload-icon-white.svg" alt=""> Upload File</a>
        <?php
        if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) {
             echo $actionCol;
        }
        ?> 
    </div>
</div>
<div class="shipping-info-top">
    <h2 class="sec-title">Shipping Information</h2>
    <hr class="separator">
    <div class="sit-wrapper">
        <div class="sit-row">
            <div class="sit-single">
                <p class="sit-single-title">Freight Forwarder</p>
                <p class="sit-single-value"><?php echo (!empty($freightForwarder)) ? $freightForwarder : ""; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">CHA</p>
                <p class="sit-single-value"><?php echo (!empty($CHA)) ? $CHA : ""; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">POL</p>
                <p class="sit-single-value"><?= $container_details['pol']; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">POD</p>
                <p class="sit-single-value"><?= $container_details['pod']; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Liner Name</p>
                <p class="sit-single-value"><?= $container_details['liner_name']; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Vessel Name</p>
                <p class="sit-single-value"><?= $container_details['vessel_name']; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Flag</p>
                <p class="sit-single-value"><?= $container_details['flag']; ?></p>
            </div>
        </div>
        <div class="sit-row">
            <div class="sit-single">
                <p class="sit-single-title">ETD</p>
                <p class="sit-single-value"><?= date('d-M-Y', strtotime($container_details['etd'])); ?></p>
            </div> 
            <div class="sit-single">
                <p class="sit-single-title">Actual date of Departure</p>
                <p class="sit-single-value"><?= date('d-M-Y', strtotime($container_details['revised_etd'])); ?></p>
            </div>
           <div class="sit-single">
                <p class="sit-single-title">ETA</p>
                <p class="sit-single-value"><?= date('d-M-Y', strtotime($container_details['eta'])); ?></p>
            </div> 
            <div class="sit-single">
                <p class="sit-single-title">Revised ETA</p>
                <p class="sit-single-value"><?= date('d-M-Y', strtotime($container_details['revised_eta'])); ?></p>
            </div>
            
            <div class="sit-single">
                <p class="sit-single-title">ETT</p>
                <p class="sit-single-value"><?= $container_details['ett']; ?> Days</p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Actual Transit Time</p>
                <p class="sit-single-value"><?= $container_details['revised_ett']; ?> Days</p>
            </div>
        </div>
        <div class="sit-row">
            <div class="sit-single">
                <p class="sit-single-title">Customer Alias</p>
                <p class="sit-single-value"><?= (!empty($customerAlias)) ? $customerAlias : "";?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Vendor Alias</p>
                <p class="sit-single-value"><?= (!empty($supplierAlias)) ? $supplierAlias : "";?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Shipping Container</p>
                <p class="sit-single-value"><?= (!empty($fcr['44'])) ? $fcr['44'] : "Waiting for FCR"; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">SGS Seal #</p>
                <p class="sit-single-value"><?= (!empty($fcr['46'])) ? $fcr['46'] : "Waiting for FCR"; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">FF Seal #</p>
                <p class="sit-single-value"><?= (!empty($fcr['45'])) ? $fcr['45'] : "Waiting for FCR"; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Email Sent On</p>
                <p class="sit-single-value"><?= (!empty($container_details['email_sent_on'])) ? date('d-M-Y', strtotime($container_details['email_sent_on'])) : ""; ?></p>
            </div>
            <div class="sit-single">
                <p class="sit-single-title">Email Subject</p>
                <p class="sit-single-value"><?= $container_details['email_subject']; ?></p>
            </div>
        </div>
    </div>
</div>