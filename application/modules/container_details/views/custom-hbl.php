<?php
    if ($getSelectedCustomField) { ?>  
      <!--end container sku table--> 
      <?php if (!empty($containersSKUData)) { ?>
          <div class="bp-list-wrapper">
              <div class="table-responsive  table-1000">
                <table class="table table-striped document-sku-table no-footer" style="width: 650px !important;" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width:15%">Container</th>
                            <th style="width:15%">SKU</th>
                            <th style="width:20%">Quantity</th> 
<!--                            <th style="width:17%">SGS Seal</th>-->
                            <th style="width:25%">FF Seal</th>
                            <th style="width:25%">Shipping Container</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($containersSKUData as $containerId => $containerSkuData) {
                                $containerNumberArr = array();
                                $businessNameArr = array();
                                foreach ($containerSkuData as $containerSkuId => $value) {  
                                    ?>
                                <tr>
                                    <td> 
                                        <?php
                                        if (!in_array($value['container_number'], $containerNumberArr)) { ?>
                                            <input type="checkbox" checked name="selected_container_id[]" id="container_id<?=$containerId?>" value="<?= $containerId ?>"/>
                                                <?=$value['container_number'] ?>

                                        <?php }
                                        ?>
                                    </td>
                                    <td><?= $value['sku_number'] ?>
                                        <input type="hidden" name="all_container_id[<?= $value['container_id']?>]" id="all_container_id<?= $containerSkuId ?>" value="<?= (!empty($value['container_id'])) ? $value['container_id'] : '0' ?>">
                                        <input type="hidden" name="container_sku_id[<?= $value['container_id']?>][<?= $containerSkuId ?>]" id="container_sku_id<?= $containerSkuId ?>" value="<?= (!empty($value['container_sku_id'])) ? $value['container_sku_id'] : '0' ?>">
                                        <input type="hidden" name="manufacturer[<?= $value['container_id']?>][<?= $containerSkuId ?>]" id="manufacturer<?= $containerSkuId ?>" value="<?= (!empty($value['manufacturer_id'])) ? $value['manufacturer_id'] : '0' ?>">
                                    </td>
                                    <td>
                                        <input type="number" name="container_sku_qty[<?= $value['container_id']?>][<?= $containerSkuId ?>]" id="container_sku_qty<?= $containerSkuId ?>" value="<?= (!empty($value['quantity'])) ? $value['quantity'] : '0' ?>" class="input-form-mro"  placeholder="Enter quantity">
                                    </td> 
                                    
                                    <?php  if (!in_array($value['alias'], $businessNameArr)) { ?>
                                        <input type="hidden" name="sgs_seal[<?= $value['container_id'] ?>]"  id="sgs_seal[<?= $value['container_id'] ?>]" placeholder="SGS Seal " value="<?= (!empty($value['sgs_seal'])?$value['sgs_seal']:"") ?>" class="input-form-mro"> 
                                    <?php } ?>
                                    
                                    <td>
                                    <?php  if (!in_array($value['alias'], $businessNameArr)) { ?>
                                        <input type="text" name="ff_seal[<?= $value['container_id'] ?>]"  id="ff_seal[<?= $value['container_id'] ?>]" placeholder="FF Seal" value="<?= (!empty($value['sgs_seal'])?$value['sgs_seal']:"") ?>" class="input-form-mro"> 
                                    <?php } ?>
                                    </td>
                                    <td>
                                    <?php  if (!in_array($value['alias'], $businessNameArr)) { ?>
                                        <input type="text" name="shipping_container[<?= $value['container_id'] ?>]"  id="shipping_container[<?= $value['container_id'] ?>]" placeholder="Shipping Container" value="<?= (!empty($value['shipping_container'])?$value['shipping_container']:"") ?>" class="input-form-mro"> 
                                    <?php } ?>
                                    </td> 
                                </tr>
                            <?php
                                    $containerNumberArr[] = $value['container_number'];
                                    $businessNameArr[] = $value['alias'];
                                } ?> 
                        <?php

                            } ?>
                    </tbody>
                </table>
              </div>
          </div>
      <?php } ?>
      
      <?php if (!empty($containersSKUData)) { ?> 
            <?php
                $cnt = 0;
                foreach ($containersSKUData as $containerId => $containerSkuData) {                      
                    foreach ($containerSkuData as $containerSkuId => $value) {
                        if($cnt==0){
                        ?> 
                        <div class="form-group supplier">
                            <label for="">Name of FF</label> 
                            <select name="freight_forwarder_id[<?= $containerId ?>]" class="basic-single select-form-mro select-large"  id="freight_forwarder_id[<?= $containerId ?>]">
                                <option value="">Select FF Name</option>
                                    <?php if($all_freight_forwarder){ 
                                        $sel = "";
                                        foreach ($all_freight_forwarder as $ffkey => $ffval) { 
                                            $sel =($ffval['business_partner_id'] == $value['freight_forwarder_id']?"selected":""); ?>
                                                <option value="<?= $ffval['business_partner_id']; ?>"<?= $sel;?>><?= $ffval['alias']; ?> </option>
                                        <?php } ?>
                                    <?php } ?>
                            </select>  
                        </div> 
                        <div class="form-group">
                            <label for="">Liner Name</label> 
                            <input type="hidden" name="tbl_container_id[<?= $containerId ?>]" id="tbl_container_id<?= $containerId ?>" value="<?= (!empty($containerId)) ? $containerId : '' ?>">
                            <select name="liner_name[<?= $containerId ?>]" class="basic-single select-form-mro select-large"  id="liner_name[<?= $containerId ?>]">
                                <option value="">Select liner Name</option>
                                    <?php if($all_liner){ 
                                        $sel = "";
                                        foreach ($all_liner as $linerkey => $linerval) { 
                                            $sel =($linerval['liner_id'] == $value['liner_name']?"selected":""); ?>
                                                <option value="<?= $linerval['liner_id']; ?>"<?= $sel;?>><?= $linerval['liner_name']; ?> </option>
                                        <?php } ?>
                                    <?php } ?>
                            </select>    
                        </div>
                        <div class="form-group">
                            <label for="">Port of Loading</label> 
                            <select name="pol[<?= $containerId ?>]" class="basic-single select-form-mro select-large"  id="pol<?= $containerId ?>">
                                <option value="">Port of Loading</option>
                                    <?php if($all_pol){ 
                                        $sel = "";
                                        foreach ($all_pol as $polkey => $polval) { 
                                            $sel =($polval['pol_id'] == $value['pol']?"selected":""); 
                                            ?>
                                            <option value="<?= $polval['pol_id']; ?>"<?= $sel;?>><?= $polval['pol_name']; ?> </option>
                                        <?php } ?>
                                    <?php } ?>
                            </select> 
                        </div> 
                        <div class="form-group">
                            <label for="">Port of Discharge </label> 
                            <select name="pod[<?= $containerId ?>]" class="basic-single select-form-mro select-large"  id="pod<?= $containerId ?>">
                                <option value="">Port of Discharge</option>
                                    <?php if($all_pod){ 
                                        $sel = "";
                                        foreach ($all_pod as $podkey => $podval) { 
                                            $sel =($podval['pod_id'] == $value['pod']?"selected":""); 
                                            ?>
                                            <option value="<?= $podval['pod_id']; ?>"<?= $sel;?>><?= $podval['pod_name']; ?> </option>
                                        <?php } ?>
                                    <?php } ?>
                            </select> 
                        </div> 
                        <div class="form-group">
                            <label for="">ETD</label> 
                            <input type="text" name="etd[<?= $containerId ?>]"  container_id="<?= $containerId ?>"  id="etd<?= $containerId ?>"   value="<?php echo(!empty($value['etd'] )) ? date('d-m-Y', strtotime($value['etd'])) :date('d-m-Y') ?>" placeholder="Enter data" readonly class="form-control input-form-mro datepicker date-form-field">
                        </div>  
                        <div class="form-group">
                            <label for="">Actual date of Departure</label> 
                            <input type="text" name="revised_etd[<?= $containerId ?>]"  id="revised_etd<?= $containerId ?>" value="<?php echo(!empty($value['revised_etd'])) ? date('d-m-Y', strtotime($value['revised_etd'])) : date('d-m-Y'); ?>" placeholder="Enter data" readonly class="form-control input-form-mro datepicker date-form-field">
                        </div>  
                        <div class="form-group">
                            <label for="">Transit Days</label> 
                             <input type="text" name="ett[<?= $containerId ?>]"  id="ett<?= $containerId ?>" value="<?= (!empty($value['ett'])?$value['ett'] : "") ?>" placeholder="Enter data" readonly class="form-control input-form-mro">
                        </div>   
                        <div class="form-group">
                            <label for="">Vessel Name</label>
                            <input type="text" name="vessel_name[<?= $containerId ?>]"  id="vessel_name<?= $containerId ?>" value="<?= (!empty($value['vessel_name'])?$value['vessel_name'] : "") ?>" placeholder="Enter data" class="form-control input-form-mro">
                        </div>  
                    <?php                                 
                        } 
                        $cnt++;
                    } ?> 
            <?php 
                } ?> 
      <?php } ?>

      <?php foreach ($getSelectedCustomField as $key => $value) {
          ?>
        
        <input type="hidden" name="document_type_id" id="document_type_id" value="<?= $value['document_type_id'] ?>">
        <input type="hidden" name="sub_document_type_id" id="sub_document_type_id" value="<?= $value['sub_document_type_id'] ?>">
        <input type="hidden" name="uploaded_document_number" id="uploaded_document_number" value="<?= $value['uploaded_document_number'] ?>">

          <?php if ($value['custom_field_type'] == 'date') { ?>
            <input type="hidden" name="custom_field_structure_id[]" id="custom_field_structure_id" value="<?= $value['custom_field_structure_id'] ?>">
              <div class="form-group ">
                  <label for=""><?= $value['custom_field_title'] ?></label>
                  <input type="text" name="<?= $value['custom_field_structure_id'] ?>"  id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter date" readonly class="form-control input-form-mro datepicker date-form-field">
                  <!-- datepicker date-form-field form-control  -->
              </div>
              <?php  } elseif ($value['custom_field_type'] == 'text') {
                if($value['custom_field_slug'] == 'hbl_number'){ ?>
                    <div class="form-group">
                        <label for=""><?= $value['custom_field_title'] ?></label>
                        <input type="text" name="hbl_number"  value="<?= (!empty($get_hbl_number[0]['hbl_number'])?$get_hbl_number[0]['hbl_number']:"")?>" id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter data" class="input-form-mro">
                    </div>
                <?php } else { ?>
                    <input type="hidden" name="custom_field_structure_id[]" id="custom_field_structure_id" value="<?= $value['custom_field_structure_id'] ?>">

                    <div class="form-group">
                        <label for=""><?= $value['custom_field_title'] ?></label>
                        <input type="text" name="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>"  id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter data" class="input-form-mro">
                    </div>
                <?php } ?>
          <?php } elseif ($value['custom_field_type'] == 'dropdown') { ?>

              <div class="form-group">
                  <div class="form-group">
                      <label for=""><?= $value['custom_field_title'] ?></label>
                      <div class="multiselect-dropdown-wrapper">
                          <div class="md-value">
                              Select value
                          </div>
                          <div class="md-list-wrapper">
                              <input type="text" placeholder="Search" class="md-search">
                              <div class="md-list-items">
                                  <?php if ($ListOfContainerData) {
                                        foreach ($ListOfContainerData as $key => $value) { ?>
                                          <div class="mdli-single ud-list-single">
                                              <label class="container-checkbox"> <?= $value['containerData']['container_number'] ?>
                                                  <input type="checkbox" id="<?= $value['custom_field_structure_id']; ?>" value="<?= $value['containerData']['container_id'] ?>" name="<?= $value['custom_field_structure_id'] ?>">
                                                  <span class="checkmark-checkbox"></span>
                                              </label>
                                          </div>
                                  <?php
                                        }
                                    }
                                    ?>
                              </div>
                              <div class="md-cta">
                                  <!-- <a href="#/" class="btn-primary-mro md-done">Done</a>
                            <a href="#/" class="btn-secondary-mro md-clear">Clear All</a> -->
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

          <?php } elseif ($value['custom_field_title'] == 'number') { ?>

          <?php } ?>
  <?php }
    } ?>

<script>
      // INITIALIZE DATEPICKER PLUGIN
        $('.datepicker').datepicker({
                format: 'dd-mm-yyyy',
                autoclose: true,
        }).on('changeDate', function (selected) {
            var minDate = new Date(selected.date.valueOf());
            var container_id = $(this).attr("container_id");
            $('#revised_etd'+container_id).datepicker('setStartDate', minDate);


            $('#revised_etd'+container_id).datepicker().on('changeDate', function (selected) {
                var maxDate = new Date(selected.date.valueOf());
                $('#etd'+container_id).datepicker('setEndDate', maxDate);
            });
            
            $('#revised_etd'+container_id).change(function() {
                var eta_date = $('#etd'+container_id).datepicker('getDate');
                var revised_etd =  $('#revised_etd'+container_id).datepicker('getDate');
                console.log(eta_date);
                console.log(revised_etd);
                    var days   = (revised_etd - eta_date)/1000/60/60/24;
                    // alert(days);
                    if (eta_date<revised_etd) {
                        console.log(days);
                        $("#ett"+container_id).val(days);

                    }
            });
        });

</script>