<?PHP
class Container_detailsmodel extends CI_Model
{
	function getRecords($get){
            //echo '<pre>'; print_r($get);die;
            $table = "tbl_size"; 
            $default_sort_column = 'c.size_id';
            $default_sort_order = 'desc';
            $condition = "1=1 "; 

            $colArray = array('c.size_name','c.status');
            $sortArray = array('c.size_name','c.status');

            $page = $get['iDisplayStart'];	// iDisplayStart starting offset of limit funciton
            $rows = $get['iDisplayLength'];	// iDisplayLength no of records from the offset

            // sort order by column
            $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
            $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

            for($i=0;$i<count($colArray);$i++){ 
                if(isset($_GET['Searchkey_'.$i]) && $_GET['Searchkey_'.$i] !=""){
                        $condition .= " AND ".$colArray[$i]." LIKE '%".$_GET['Searchkey_'.$i]."%' ";
                }  
            }

            $this -> db -> select('c.*');
            $this -> db -> from("$table as c"); 
            $this->db->where("($condition)");
            $this->db->order_by($sort, $order);
            $this->db->limit($rows,$page); 
            $query = $this -> db -> get();
            //echo $this->db->last_query();exit; 

            $this -> db -> select('c.*');
            $this -> db -> from("$table as c");
            $this->db->where("($condition)");
            $this->db->order_by($sort, $order); 
            $query1 = $this -> db -> get();

            if($query -> num_rows() > 0){
                $totcount = $query1 -> num_rows();
                return array("query_result" => $query->result(), "totalRecords" => $totcount);
            }else{
                return array("totalRecords" => 0);
            }
	} 
        
        function getFCRDetails($container_id=0){   
            $condition = " cds.custom_field_structure_id IN (44,45,46) AND cds.document_type_id = 4  AND dluf.container_id = ".$container_id." ";
            $this->db->select('cds.custom_field_structure_id,cds.custom_field_title,cdfs.custom_field_structure_value');
            $this->db->from("tbl_document_ls_uploaded_files as dluf");
            $this->db->join("tbl_custom_field_data_submission as cdfs", "cdfs.document_type_id = dluf.document_type_id AND dluf.uploaded_document_number = cdfs.uploaded_document_number",'left');
            $this->db->join("tbl_custom_field_structure as cds", "cds.document_type_id = dluf.document_type_id and cdfs.custom_field_structure_id=cds.custom_field_structure_id",'left'); 
            $this->db->where("($condition)");
            $this->db->group_by("dluf.container_id,cdfs.custom_field_structure_id"); 
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getProductDetails($container_id = 0) {
            $condition = " c.container_id = " . $container_id . " ";
            $this->db->select('c.*,cont.order_id,sku.product_name,sku.sku_number,sku.sku_vendor,b.brand_name,s.size_name,u.uom_name,bp.alias');
            $this->db->from("tbl_container_sku as c");
            $this->db->join("tbl_container as cont", "c.container_id = cont.container_id");
            $this->db->join("tbl_sku as sku", "c.sku_id = sku.sku_id");
            $this->db->join("tbl_brand as b", "b.brand_id = sku.brand_id",'left');
            $this->db->join("tbl_size as s", "s.size_id = sku.size_id",'left');
            $this->db->join("tbl_uom as u", "u.uom_id = sku.uom_id",'left');
            $this->db->join("tbl_business_partner as bp", "bp.business_partner_id = sku.manufacturer_id",'left'); 
            $this->db->where("($condition)");
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getAddressDetails($container_id = 0) {
            $condition = " c.container_id = " . $container_id . " ";
            $this->db->select('o.customer_contract_id,o.supplier_contract_id,o.customer_billing_details_id,o.customer_shipping_details_id,o.supplier_billing_details_id,o.supplier_shipping_details_id');
            $this->db->from("tbl_container as c");
            $this->db->join("tbl_order as o", "o.order_id = c.order_id"); 
            $this->db->where("($condition)");
            $query = $this->db->get(); 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getContainerStatus($container_status_id) {
            $condition = " csl.container_id = " . $container_status_id . " "; 
            $this->db->select('cs.container_status_id,cs.container_status_name');
            $this->db->from("tbl_container_status_log as csl");
            $this->db->join("tbl_container_status as cs", "cs.container_status_id = csl.container_status_id"); 
            $this->db->where("($condition)"); 
            $this->db->order_by("csl.status_log_id DESC"); 
            $this->db->limit("1"); 
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getInvoiceDetails($container_id = 0) {
            $condition = " iuf.container_id = " . $container_id . " "; 
            $this->db->select('iuf.container_id,cids.custom_field_invoice_data_submission_id, cids.invoice_file_name, cids.invoice_date, cids.paid_date, cids.invoice_value , cids.invoice_status, bp.alias, cids.updated_on, u.firstname , u.lastname');
            $this->db->from("tbl_invoice_uploaded_files as iuf");
            $this->db->join("tbl_custom_field_invoice_data_submission as cids", "cids.business_partner_id = iuf.business_partner_id");
            $this->db->join("tbl_business_partner as bp", "bp.business_partner_id = iuf.business_partner_id"); 
            $this->db->join("tbl_users as u", "u.user_id = cids.updated_by",'left');  
            $this->db->where("($condition)"); 
            $this->db->group_by("cids.custom_field_invoice_data_submission_id"); 
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        } 
        
        function getInvoiceDetailsView($id = 0) {
            $condition = " cids.custom_field_invoice_data_submission_id = " . $id . " "; 
            $this->db->select('cids.invoice_file_name,cids.invoice_value,cids.invoice_date,cids.invoice_status,cids.paid_date,cids.quantity,cids.price_per_unit,bp.alias');
            $this->db->from("tbl_custom_field_invoice_data_submission as cids"); 
            $this->db->join("tbl_business_partner as bp", "bp.business_partner_id = cids.business_partner_id",'left');  
            $this->db->where("($condition)");  
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getInvoiceSKUDetails($id = 0) {
            $condition = " cids.custom_field_invoice_data_submission_id = " . $id . " "; 
            $this->db->select('cids.quantity,cids.price_per_unit,sku.sku_number,sku.product_name,s.size_name');
            $this->db->from("tbl_custom_field_invoice_data_submission_sku as cids");  
            $this->db->join("tbl_sku as sku", "cids.sku_id = sku.sku_id");
            $this->db->join("tbl_size as s", "s.size_id = sku.size_id",'left'); 
            $this->db->where("($condition)");  
            $query = $this->db->get(); 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getLinkContainers($documentNumberString = '',$type='') {
            $table = "tbl_document_ls_uploaded_files";
            if($type=='LS'){
                $table = "tbl_document_uploaded_files";
            }            
            $condition = " uploaded_document_number in (" . $documentNumberString . ") "; 
            $this->db->select('container_id');
            $this->db->from("$table"); 
            $this->db->where("($condition)"); 
            $this->db->group_by("container_id"); 
            $this->db->order_by("container_id");
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        
        function getLinkContainerDetails($container_id = 0) {  
            $condition = " csl.container_id = " . $container_id . " ";  
            $this->db->select('csl.created_on,c.container_id,c.container_number,cs.container_status_name,u.firstname,u.lastname'); 
            $this->db->from("tbl_container_status_log as csl"); 
            $this->db->join("tbl_container_status as cs", "cs.container_status_id = csl.container_status_id"); 
            $this->db->join("tbl_container as c", "c.container_id = csl.container_id"); 
            $this->db->join("tbl_users as u", "u.user_id = csl.created_by",'left');
            $this->db->where("($condition)");
            $this->db->order_by("csl.status_log_id DESC");
            $this->db->limit("1"); 
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getDocumentUploaded($container_id = 0) {
            $condition = " duf.container_id = " . $container_id . " "; 
            $this->db->select('dt.document_type_squence,duf.document_type_id,duf.uploaded_document_number, duf.uploaded_document_number as document_number, duf.updated_on, duf.uploaded_document_file,cuds.status,u.firstname,u.lastname');
            $this->db->from("tbl_document_uploaded_files as duf");
            $this->db->join("tbl_document_type as dt", "dt.document_type_id = duf.document_type_id");
            $this->db->join("tbl_container_uploaded_document_status as cuds", "cuds.document_type_id = duf.document_type_id",'left');
            $this->db->join("tbl_users as u", "u.user_id = duf.updated_by",'left');
            $this->db->where("($condition)"); 
            $this->db->group_by("duf.document_type_id");  
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getDocumentIsUploaded($container_id = 0) {  
            
            if ($_SESSION["mro_session"]['user_role'] == 'Customer' || $_SESSION["mro_session"]['user_role'] == 'Supplier') {
                $condition = " duf.container_id = " . $container_id . " AND dt.document_type_name IN ('FCR','HBL','INS') AND sdt.sub_document_type_id IN (15,17,20) "; 
                $this->db->select('sdt.sub_document_type_id,dt.document_type_squence,duf.document_type_id,GROUP_CONCAT(DISTINCT(duf.uploaded_document_number)) as  uploaded_document_number, duf.uploaded_document_number as document_number,duf.updated_on, duf.uploaded_document_file,cuds.status,u.firstname,u.lastname');
                $this->db->from("tbl_sub_document_type as sdt"); 
                $this->db->join("tbl_document_ls_uploaded_files as duf", "duf.sub_document_type_id = sdt.sub_document_type_id");
                $this->db->join("tbl_document_type as dt", "dt.document_type_id = sdt.document_type_id"); 
                $this->db->join("tbl_container_uploaded_document_status as cuds", "cuds.document_type_id = duf.document_type_id",'left');
                $this->db->join("tbl_users as u", "u.user_id = duf.updated_by",'left');  
                $this->db->where("($condition)"); 
                $this->db->group_by("duf.document_type_id");
            }else{
                $condition = " duf.container_id = " . $container_id . " "; 
                $this->db->select('dt.document_type_squence,duf.document_type_id,GROUP_CONCAT(DISTINCT(duf.uploaded_document_number)) as  uploaded_document_number, duf.uploaded_document_number as document_number,duf.updated_on, duf.uploaded_document_file,cuds.status,u.firstname,u.lastname');
                $this->db->from("tbl_document_ls_uploaded_files as duf"); 
                $this->db->join("tbl_document_type as dt", "dt.document_type_id = duf.document_type_id");
                $this->db->join("tbl_container_uploaded_document_status as cuds", "cuds.document_type_id = duf.document_type_id",'left');
                $this->db->join("tbl_users as u", "u.user_id = duf.updated_by",'left'); 
                $this->db->where("($condition)"); 
                $this->db->group_by("duf.document_type_id");
            } 
             
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getContainerSizeData($container_id = 0) {
            $qry = "SELECT SUM(csku.quantity) as total,size.size_name FROM tbl_container_sku as csku
                    INNER JOIN tbl_sku as sku ON sku.sku_id = csku.sku_id
                    INNER JOIN tbl_size as size ON size.size_id = sku.size_id
                    WHERE csku.container_id = ".$container_id." 
                    group by size.size_id
                    ORDER by size.size_name desc";
            $query = $this->db->query($qry);
            //echo $this->db->last_query();exit;
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getContainerSKUData($container_id = 0) {
            $condition = " c.container_id = " . $container_id . " ";
            $this->db->select('c.quantity,sku.sku_number');
            $this->db->from("tbl_container_sku as c");
            $this->db->join("tbl_sku as sku", "c.sku_id = sku.sku_id"); 
            $this->db->where("($condition)");
            $query = $this->db->get();
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        
        function getDocumentsFields($document_type_id = 0, $document_number = '') {  
            $condition = " cfs.document_type_id =".$document_type_id." and cfds.uploaded_document_number = '".$document_number."' AND cfs.status = 'Active' "; 
            $this->db->select('cfs.document_type_id,cfs.custom_field_title, cfs.sub_document_type_id,cfs.custom_field_slug,cfds.custom_field_structure_value,cfs.custom_field_type,cfs.custom_field_structure_id,cfds.uploaded_document_number');
            $this->db->from("tbl_custom_field_structure as cfs"); 
            $this->db->join("tbl_custom_field_data_submission as cfds", "cfds.custom_field_structure_id = cfs.custom_field_structure_id","left"); 
            $this->db->where("($condition)"); 
            $this->db->group_by("cfs.custom_field_structure_id");  
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getDocumentStatus($document_type_id=0,$container_id=0){
            $condition = " ds.document_type_id =".$document_type_id." and ds.container_id = ".$container_id." "; 
            $this->db->select('ds.status,ds.updated_on,u.firstname,u.lastname');
            $this->db->from("tbl_container_uploaded_document_status as ds"); 
            $this->db->join("tbl_users as u", "u.user_id = ds.updated_by","left"); 
            $this->db->where("($condition)");   
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        function getUpdatedSupplier($container_id = 0, $document_type_id = 0) {  
            $condition = "1=1  AND cdd.container_id=".$container_id." AND cdd.document_type_id =".$document_type_id." "; 
            $this->db->select('cdd.*, bp.business_partner_id,bp.alias,bp.contact_person,bp.phone_number');
            $this->db->from("tbl_custom_dynamic_data as cdd"); 
            $this->db->join("tbl_business_partner as bp", "bp.business_partner_id = cdd.supplier_id","left"); 
            $this->db->where("($condition)");   
            $this->db->order_by("cdd.custom_dynamic_data_id DESC");
            $this->db->limit(1);
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        
        function getLastContainerStatus($container_id,$session_id='') {
            $condition = " log.container_id = " . $container_id . " AND log.current_session_id <> '".$session_id."' "; 
            $this->db->select('log.container_status_id,log.created_by,log.created_on');
            $this->db->from("tbl_container_status_log as log");
            $this->db->join("tbl_container_status as cs", "cs.container_status_id = log.container_status_id"); 
            $this->db->where("($condition)"); 
            $this->db->order_by("cs.status_squence DESC"); 
            $this->db->limit("1"); 
            $query = $this->db->get();
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                return $query->result_array();
            } else {
                return false;
            }
        }
        
        
       
        
}
?>
