<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class AllDocuments extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('AllDocumentsmodel', 'allDocumentsmodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("AllDocuments")){
            redirect('dashboard');
        }
    }

    function index() {
        $data = array();
        //document type
        $documentType = $this->common->getData("tbl_document_type", "document_type_id,document_type_name", "");
        if (!empty($documentType)) {
            $data['documentType'] = $documentType;
        }
        
        //containers
        $containers = $this->common->getData("tbl_container", "container_id,container_number", "");
        if (!empty($containers)) {
            $data['containers'] = $containers;
        }
        
        //sku
        $sku = $this->common->getData("tbl_sku", "sku_id,sku_number,product_name", "");
        if (!empty($sku)) {
            $data['sku'] = $sku;
        }
        
        $condition = " status = 'Active' ";
        $bpContractData = $this->common->getData("tbl_bp_order","bp_order_id,contract_number",$condition);
        if(!empty($bpContractData)){
            $data['bpContractData'] = $bpContractData;
        } 
        
        $condition = " status = 'Active' ";
        $orderData = $this->common->getData("tbl_order","order_id,contract_number",$condition);
        if(!empty($orderData)){
            $data['orderData'] = $orderData;
        } 
        
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('allDocuments/index', $data);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    } 

    function fetch() {
        //get list data
        $get_result = $this->allDocumentsmodel->getRecords($_GET);
        //echo '<pre>'; print_r($get_result);die;
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            $containerIdArr = array();
            foreach ($get_result['query_result'] as $key => $value) {
                $temp = array();
                //array_push($temp, $value['document_type_name']);
                //$uploaded_document_number = str_replace(',', '<br />', $value['uploaded_document_number']); 
                $uploaded_document_file = "'".$value['uploaded_document_file']."'".","."'".$value['uploaded_document_number']."'"; 
                $uploaded_document_number = '<span onclick="showDocument('.$uploaded_document_file.')" title="View Document" style="cursor: pointer;"><u>'.$value['uploaded_document_number'].'</u></span>';                                                 
                array_push($temp, $uploaded_document_number);
                $container_number = '<a href="'.base_url().'container_details?text='.rtrim(strtr(base64_encode("id=".$value['container_id']), '+/', '-_'), '=').' "><u>'.$value['container_number'].'</u></a>';                 
                array_push($temp, $container_number);
                array_push($temp, !empty($value['document_date'])?date('d-M-Y',strtotime($value['document_date'])):'');
                array_push($temp, !empty($value['upload_date'])?date('d-M-Y',strtotime($value['upload_date'])):''); 
                $skuStr = '';
                $totalQty = 0;
                //if (!in_array($value['container_id'], $containerIdArr)) { 
                $condition = " csku.container_id = ".$value['container_id']." ";
                $main_table = array("tbl_container_sku as csku", array('csku.quantity'));
                $join_tables = array(
                    array("","tbl_sku as sku","sku.sku_id = csku.sku_id", array("sku.sku_number")),
                ); 
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,"",'');
                $resultSKU = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                if(!empty($resultSKU)){
                    foreach ($resultSKU as $skuValue) {
                        if(empty($skuStr)){
                            $skuStr = $skuValue['sku_number'];
                        }else{
                            $skuStr = $skuStr .' , '.$skuValue['sku_number'];
                        } 
                        $totalQty += $skuValue['quantity'];
                    }
                }
                //} 
                array_push($temp, $skuStr);
                if($totalQty==0){
                    array_push($temp, ''); 
                }else{
                    array_push($temp, $totalQty); 
                } 
                array_push($items, $temp); 
                //$containerIdArr[] = $value['container_id'];
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    } 
    
    
    function getFilterdata(){ 
        $get_result = $this->allDocumentsmodel->getFilterRecords($_POST);
        //echo '<pre>'; print_r($get_result);die;
        $result = array();  
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            $result['result'] = $get_result['query_result'];
            $containerListingDiv = $this->load->view('document-table-data',$result,true); 
        }else{
            $containerListingDiv = 'Record Not Found';
        }  
        
        if($containerListingDiv){
            echo json_encode(array('success' => true, 'msg' => 'record Found',"all_document_div"=>$containerListingDiv));
            exit;
        }else{
            echo json_encode(array('success' => false, 'msg' => 'record not Found',"all_document_div"=>$containerListingDiv));
            exit;
        } 
    }
    
    function getDocumentView(){
        $result['file_name'] = $_GET['fileName']; 
        $result['Doc_name'] = $_GET['DocName'];     
        $result['doc_id'] = $_GET['docId'];     
        $form_view = '';  
        $form_view = $this->load->view("document-form-view",$result,true);        
        echo json_encode(array('form_view' => $form_view));
        exit;
    }
    
}

?>
