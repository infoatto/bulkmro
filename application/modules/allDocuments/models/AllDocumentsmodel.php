<?PHP

class AllDocumentsmodel extends CI_Model {

    function getRecords($get) { 
        $condition = "1=1 ";  
        
        $page = $get['iDisplayStart'];	// iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength'];	// iDisplayLength no of records from the offset
        
        $colArray = array('csku.sku_id','duf.document_type_id','c.container_id');
        for ($i = 0; $i < count($colArray); $i++) {
            if (isset($_GET['Searchkey_' . $i]) && $_GET['Searchkey_' . $i] != "") {
                $condition .= " AND " . $colArray[$i] . " = " . $_GET['Searchkey_' . $i] . " ";
            }
        } 
        
        if(!empty($_GET['Searchkey_3']) && isset($_GET['Searchkey_3'])){
            $condition .= " AND (bpoc.bp_order_id = ".$_GET['Searchkey_3']." ) "; //OR bpos.bp_order_id = ".$_GET['Searchkey_3']."
        }
        
        if(!empty($_GET['Searchkey_4']) && isset($_GET['Searchkey_4'])){  
            $condition .= " AND o.order_id = ".$_GET['Searchkey_4']." ";
        }
        
//        $qry = "SELECT * FROM `tbl_document_uploaded_files` WHERE `container_id` = 1 ORDER BY `updated_on` DESC";  
//        $query2 = $this->db->query($qry);
//        $result = $query2->result_array(); 

        $this->db->select('c.container_id,c.container_number,duf.uploaded_document_number,duf.document_type_id,duf.uploaded_document_file,duf.updated_on as upload_date,dt.document_type_squence, cfds.custom_field_structure_value as document_date');
        $this->db->from("tbl_container as c");
        $this->db->join("tbl_container_sku as csku","csku.container_id =c.container_id","");
        $this->db->join("tbl_document_uploaded_files as duf","duf.container_id =c.container_id","");
        $this->db->join("tbl_document_type as dt","dt.document_type_id =duf.document_type_id","");
        $this->db->join("tbl_custom_field_structure as cfs","cfs.document_type_id =duf.document_type_id AND custom_field_title='Date of Document' ","left");
        $this->db->join("tbl_custom_field_data_submission as cfds","cfds.custom_field_structure_id =cfs.custom_field_structure_id and cfds.uploaded_document_number = duf.uploaded_document_number","left");
        $this->db->join("tbl_order as o","o.order_id = c.order_id","left");
        $this->db->join("tbl_bp_order as bpoc","bpoc.bp_order_id = o.customer_contract_id OR bpoc.bp_order_id = o.supplier_contract_id","left");
        //$this->db->join("tbl_bp_order as bpos","bpos.bp_order_id = o.supplier_contract_id","left"); 
        $this->db->where("($condition)");
        $this->db->group_by("duf.uploaded_document_number");  
        //$this->db->limit($rows,$page);
        $query = $this->db->get(); 
        //echo $this->db->last_query();exit;
        $dataArr = array();
        if ($query->num_rows() > 0) {
            $result = $query->result_array();
            foreach ($result as $value) {
                $sequence = $value['document_type_squence'];
                if($sequence=='10'){
                    $sequence = 9;
                }
                $dataArr[$value['container_number'].'#'.$sequence.'#'.$value['uploaded_document_number']] = $value;
            }
        }
        
        $this->db->select('c.container_id,c.container_number,duf.uploaded_document_number,duf.document_type_id,duf.uploaded_document_file,duf.updated_on as upload_date,dt.document_type_squence, cfds.custom_field_structure_value as document_date');
        $this->db->from("tbl_container as c");
        $this->db->join("tbl_container_sku as csku","csku.container_id =c.container_id","");
        $this->db->join("tbl_document_ls_uploaded_files as duf","duf.container_id =c.container_id","");
        $this->db->join("tbl_document_type as dt","dt.document_type_id =duf.document_type_id","");
        $this->db->join("tbl_custom_field_structure as cfs","cfs.document_type_id =duf.document_type_id AND custom_field_title='Date of Document' ","left");
        $this->db->join("tbl_custom_field_data_submission as cfds","cfds.custom_field_structure_id =cfs.custom_field_structure_id and cfds.uploaded_document_number = duf.uploaded_document_number","left");
        $this->db->join("tbl_order as o","o.order_id = c.order_id","left");
        $this->db->join("tbl_bp_order as bpoc","bpoc.bp_order_id = o.customer_contract_id OR bpoc.bp_order_id = o.supplier_contract_id","left"); 
        $this->db->where("($condition)");
        $this->db->group_by("duf.uploaded_document_number"); 
        //$this->db->group_by("`duf`.`document_type_id`,`c`.`container_id`");  
        //$this->db->limit($rows,$page);
        $query1 = $this->db->get();  
        
        //echo $this->db->last_query();exit;        
        if ($query1->num_rows() > 0) {
            $result1 = $query1->result_array();
            foreach ($result1 as $value) {
                $sequence = $value['document_type_squence'];
                if($sequence=='10'){
                    $sequence = 9;
                }
                $dataArr[$value['container_number'].'#'.$sequence.'#'.$value['uploaded_document_number']] = $value;
            }
        }
        ksort($dataArr);
        
//        $this->db->select('c.container_number,duf.uploaded_document_number,dt.document_type_squence');
//        $this->db->from("tbl_container as c");
//        $this->db->join("tbl_document_uploaded_files as duf","duf.container_id =c.container_id",""); 
//        $this->db->join("tbl_document_type as dt","dt.document_type_id =duf.document_type_id","");
//        $this->db->where("($condition)");
//        $this->db->group_by("duf.uploaded_document_number"); 
//        $queryTotal1 = $this->db->get(); 
//        $queryTotal1 = $queryTotal1->result_array();
//        $totalDataArr = array();
//        foreach ($queryTotal1 as $value) { 
//            $totalDataArr[$value['container_number'].'#'.$value['document_type_squence'].'#'.$value['uploaded_document_number']] = $value;
//        }
//        
//        $this->db->select('c.container_number,GROUP_CONCAT(DISTINCT(duf.uploaded_document_number)) as uploaded_document_number,dt.document_type_squence');
//        $this->db->from("tbl_container as c");
//        $this->db->join("tbl_document_ls_uploaded_files as duf","duf.container_id =c.container_id",""); 
//        $this->db->join("tbl_document_type as dt","dt.document_type_id =duf.document_type_id","");
//        $this->db->where("($condition)");
//        $this->db->group_by("`duf`.`document_type_id`,`c`.`container_id`");  
//        $queryTotal2 = $this->db->get();
//        $queryTotal2 = $queryTotal2->result_array(); 
//        foreach ($queryTotal2 as $value) { 
//            $totalDataArr[$value['container_number'].'#'.$value['document_type_squence'].'#'.$value['uploaded_document_number']] = $value;
//        } 
      
        if (count($dataArr) > 0) {
            $totcount = 0;//$query1->num_rows();
            return array("query_result" => $dataArr, "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    } 
    
    function getFilterRecords($post) { 
        $dataArr = array();
        $join = '';
        $condition = "1=1 ";  
        if(!empty($post['sku_id']) && isset($post['sku_id'])){
            $join .= " LEFT JOIN `tbl_container_sku` as `csku` ON `csku`.`container_id` =`c`.`container_id` ";
            $condition .= " AND csku.sku_id = ".$post['sku_id']." ";  
        }
        $doc_type_id = 0;
        if(!empty($post['doc_type_id']) && isset($post['doc_type_id'])){
            $condition .= " AND duf.document_type_id = ".$post['doc_type_id']." ";  
            $doc_type_id = $post['doc_type_id'];
        }  
        if(!empty($post['container_id']) && isset($post['container_id'])){
            $condition .= " AND c.container_id = ".$post['container_id']." ";  
        }  
        if(!empty($post['bp_contract_id']) && isset($post['bp_contract_id'])){ 
            $orderIdsStr = ''; 
            $orderIdsStr = getOrderIdsByBPContract($post['bp_contract_id']); 
            if($orderIdsStr != ''){
                $condition .= " AND c.order_id IN (".$orderIdsStr.") ";
            } 
        } 
        if(!empty($post['m_contract_id']) && isset($post['m_contract_id'])){   
            $condition .= " AND c.order_id = ".$post['m_contract_id']." ";
        }  
        //check for LS (2) and FRI (3)
        if($doc_type_id==0 || ($doc_type_id==2 || $doc_type_id==3)){ 
            $qry = "SELECT `c`.`container_id`, `c`.`container_number`, `duf`.`uploaded_document_number`, `duf`.`document_type_id`, `duf`.`uploaded_document_file`, `duf`.`updated_on` as `upload_date`, `dt`.`document_type_squence`, `cfds`.`custom_field_structure_value` as `document_date`
                    FROM `tbl_container` as `c`
                    JOIN `tbl_document_uploaded_files` as `duf` ON `duf`.`container_id` =`c`.`container_id`
                    JOIN `tbl_document_type` as `dt` ON `dt`.`document_type_id` =`duf`.`document_type_id`
                    LEFT JOIN `tbl_custom_field_structure` as `cfs` ON `cfs`.`document_type_id` =`duf`.`document_type_id` AND `custom_field_title`='Date of Document' 
                    LEFT JOIN `tbl_custom_field_data_submission` as `cfds` ON `cfds`.`custom_field_structure_id` =`cfs`.`custom_field_structure_id` and `cfds`.`uploaded_document_number` = `duf`.`uploaded_document_number`
                    ".$join." 
                    WHERE ".$condition."
                    GROUP BY `duf`.`uploaded_document_number` "; 
            $query = $this->db->query($qry); 
            //echo $this->db->last_query();exit; 
            if ($query->num_rows() > 0) {
                $result = $query->result_array();
                foreach ($result as $value) {
                    $sequence = $value['document_type_squence'];
                    if($sequence=='10'){
                        $sequence = 9;
                    }
                    $dataArr[$value['container_number'].'#'.$sequence.'#'.$value['uploaded_document_number']] = $value;
                }
            } 
        }
        if($doc_type_id==0 || ($doc_type_id != 2 && $doc_type_id != 3)){ 
            $qry1 = "SELECT `c`.`container_id`, `c`.`container_number`, `duf`.`uploaded_document_number`, `duf`.`document_type_id`, `duf`.`uploaded_document_file`, `duf`.`updated_on` as `upload_date`, `dt`.`document_type_squence`, `cfds`.`custom_field_structure_value` as `document_date`
                    FROM `tbl_container` as `c` 
                    JOIN `tbl_document_ls_uploaded_files` as `duf` ON `duf`.`container_id` =`c`.`container_id`
                    JOIN `tbl_document_type` as `dt` ON `dt`.`document_type_id` =`duf`.`document_type_id`
                    LEFT JOIN `tbl_custom_field_structure` as `cfs` ON `cfs`.`document_type_id` =`duf`.`document_type_id` AND `custom_field_title`='Date of Document' 
                    LEFT JOIN `tbl_custom_field_data_submission` as `cfds` ON `cfds`.`custom_field_structure_id` =`cfs`.`custom_field_structure_id` and `cfds`.`uploaded_document_number` = `duf`.`uploaded_document_number`
                    ".$join." 
                    WHERE ".$condition." 
                    GROUP BY `duf`.`uploaded_document_number` "; 
            //echo $qry1;die;
            $query1 = $this->db->query($qry1);  
            if ($query1->num_rows() > 0) {
                $result1 = $query1->result_array();
                foreach ($result1 as $value) {
                    $sequence = $value['document_type_squence'];
                    if($sequence=='10'){
                        $sequence = 9;
                    }
                    $dataArr[$value['container_number'].'#'.$sequence.'#'.$value['uploaded_document_number']] = $value;
                }
            }
        }
        
        
//         $qry = "SELECT `c`.`container_id`, `c`.`container_number`, `duf`.`uploaded_document_number`, `duf`.`document_type_id`, `duf`.`uploaded_document_file`, `duf`.`updated_on` as `upload_date`, `dt`.`document_type_squence`, `cfds`.`custom_field_structure_value` as `document_date`
//                FROM `tbl_container` as `c`
//                JOIN `tbl_document_uploaded_files` as `duf` ON `duf`.`container_id` =`c`.`container_id`
//                JOIN `tbl_document_type` as `dt` ON `dt`.`document_type_id` =`duf`.`document_type_id`
//                LEFT JOIN `tbl_custom_field_structure` as `cfs` ON `cfs`.`document_type_id` =`duf`.`document_type_id` AND `custom_field_title`='Date of Document' 
//                LEFT JOIN `tbl_custom_field_data_submission` as `cfds` ON `cfds`.`custom_field_structure_id` =`cfs`.`custom_field_structure_id` and `cfds`.`uploaded_document_number` = `duf`.`uploaded_document_number`
//                WHERE ".$condition."
//                GROUP BY `duf`.`uploaded_document_number` 
//                UNION ALL
//                SELECT `c`.`container_id`, `c`.`container_number`, `duf`.`uploaded_document_number`, `duf`.`document_type_id`, `duf`.`uploaded_document_file`, `duf`.`updated_on` as `upload_date`, `dt`.`document_type_squence`, `cfds`.`custom_field_structure_value` as `document_date`
//                FROM `tbl_container` as `c` 
//                JOIN `tbl_document_ls_uploaded_files` as `duf` ON `duf`.`container_id` =`c`.`container_id`
//                JOIN `tbl_document_type` as `dt` ON `dt`.`document_type_id` =`duf`.`document_type_id`
//                LEFT JOIN `tbl_custom_field_structure` as `cfs` ON `cfs`.`document_type_id` =`duf`.`document_type_id` AND `custom_field_title`='Date of Document' 
//                LEFT JOIN `tbl_custom_field_data_submission` as `cfds` ON `cfds`.`custom_field_structure_id` =`cfs`.`custom_field_structure_id` and `cfds`.`uploaded_document_number` = `duf`.`uploaded_document_number`
//                WHERE ".$condition." 
//                GROUP BY `duf`.`uploaded_document_number` "; 
//        $query = $this->db->query($qry); 
//        //echo $this->db->last_query();exit;
//        $dataArr = array();
//        if ($query->num_rows() > 0) {
//            $result = $query->result_array();
//            foreach ($result as $value) {
//                $sequence = $value['document_type_squence'];
//                if($sequence=='10'){
//                    $sequence = 9;
//                }
//                $dataArr[$value['container_number'].'#'.$sequence.'#'.$value['uploaded_document_number']] = $value;
//            }
//        }  
        
        ksort($dataArr);   
        //print_r($dataArr);die;
        if (count($dataArr) > 0) {
            $totcount = 0;//$query1->num_rows();
            return array("query_result" => $dataArr, "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    } 
}

?>
