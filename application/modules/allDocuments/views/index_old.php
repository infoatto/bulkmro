<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                            <span>></span>
                            <p>All Documents</p>
                        </div> 
                        <div class="page-title-wrapper">
                            <h1 class="page-title"><a href="<?php echo base_url("allDocuments"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Documents</h1>
                        </div>
                    </div> 
                </div>
                
                <div class="page-content-wrapper1">
                    <div id="serchfilter" class="filter-sec-wrapper">
                        <div class="form-group  dataTables_filter searchFilterClass"> 
                            <select name="sSearch_0" id="sSearch_0" class="searchInput basic-single select-mro select-xl">
                                <option value="" disabled selected hidden>SKU</option>
                                <?php
                                foreach ($sku as $value) { 
                                    ?> 
                                    <option value="<?php echo $value['sku_id']; ?>">
                                        <?php echo $value['sku_number']; ?> 
                                    </option> 
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group  dataTables_filter searchFilterClass"> 
                            <select name="sSearch_1" id="sSearch_1" class="searchInput basic-single select-mro select-xl">
                                <option value="" disabled selected hidden>Document Type</option>
                                <?php
                                foreach ($documentType as $value) { 
                                    ?> 
                                    <option value="<?php echo $value['document_type_id']; ?>">
                                        <?php echo $value['document_type_name']; ?> 
                                    </option> 
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group dataTables_filter searchFilterClass"> 
                            <select name="sSearch_2" id="sSearch_2" class="searchInput basic-single select-mro select-xl">
                                <option value="" disabled selected hidden>Container</option>
                                <?php
                                foreach ($containers as $value) { 
                                    ?> 
                                    <option value="<?php echo $value['container_id']; ?>">
                                        <?php echo $value['container_number']; ?> 
                                    </option> 
                                    <?php
                                }
                                ?>
                            </select>
                        </div>   
                        
                        <div class="form-group dataTables_filter searchFilterClass"> 
                            <select name="sSearch_3" id="sSearch_3" class="searchInput basic-single select-large"> 
                                <option value="" disabled selected hidden>BP Contract</option>
                                <?php foreach ($bpContractData as $value) { ?>
                                <option value="<?=$value['bp_order_id']?>"><?=$value['contract_number']?></option>
                                <?php } ?>
                            </select>
                        </div> 

                        <div class="form-group dataTables_filter searchFilterClass"> 
                            <select name="sSearch_4" id="sSearch_4" class="searchInput basic-single select-large"> 
                                <option value="" disabled selected hidden>Master Contract</option>
                                <?php foreach ($orderData as $value) { ?>
                                <option value="<?=$value['order_id']?>"><?=$value['contract_number']?></option>
                                <?php } ?>
                            </select>
                        </div> 
                        
                        <div class="form-group dataTables_filter searchFilterClass"> 
                           <button type="reset"  onclick="refresh()"  class="select-small btn-primary-mro">
                            Clear
                          </button>
                        </div>  
                    </div> 
                </div>
                <div class="bp-list-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
<!--                                    <th>Document Type</th>-->
                                    <th>Document Name</th>
                                    <th>Container</th>
                                    <th>Date of Document</th>
                                    <th>Date of Upload</th>
                                    <th style="width:500px">SKU</th>
                                    <th>Total Quantity</th> 
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="overlay"></div>
<div class="mro-modal-wrapper mro-modal-wrapper-document mro-upload-doc-modal-cls" style="min-width:80%;max-width:80%;height: 1950px;">
    <div class="mro-modal-header">
        <a href="#/" class="mro-close-modal">
            <img src="<?php echo base_url(); ?>assets/images/modal-close-dark.svg" alt="Close">
        </a>
    </div>
    <div class="mro-modal-body">
        <div class="ud-step upload-fri-step-1">
<!--            <div class="upload-doc-title">
                <div class="ud-title-left"> 
                    <h3>Document View</h3>
                </div>
            </div>-->
            <div class="select-doc-wrapper"> 
                <div class="" style="flex: 0 0 100%;">
<!--                    <h4 class="sd-subtitle">Form View</h4>-->
                    <span id="invoice-form-view"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function showDocument(fileName='',DocName=''){ 
        if(fileName != ''){
            $('.overlay,.mro-modal-wrapper').show();
            var act = "<?php echo base_url(); ?>allDocuments/getDocumentView";
            $.ajax({
                type: 'GET',
                url: act,
                data: {
                    fileName: fileName,DocName: DocName
                },
                dataType: "json",
                success: function (data) { 
                    $('#invoice-form-view').html(data.form_view);  
                }
            });
        } 
    }
    function refresh(){
      location.reload();
    }
</script>
<style>
    .dataTables_info{
        display: none;
    } 
    .dataTables_paginate{
        display: none;
    } 
</style>