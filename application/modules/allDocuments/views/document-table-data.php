<div class="table-responsive">
    <table class="table table-striped basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
<!--                                    <th>Document Type</th>-->
                <th>Document Name</th>
                <th>Container</th>
                <th>Date of Document</th>
                <th>Date of Upload</th>
                <th style="width:500px">SKU</th>
                <th>Total Quantity</th> 
            </tr>
        </thead>
        <tbody>
            <?php   
            $skuStr = ''; 
            $skuStrHold = array();
            $totalQtyHold = array();
            $containerIdArr = array();
            foreach ($result as $key => $value) { 
                //$uploaded_document_number = str_replace(',', '<br />', $value['uploaded_document_number']); 
                $uploaded_document_file = "'".$value['uploaded_document_file']."'".","."'".$value['uploaded_document_number']."'"; 
                $uploaded_document_number = '<span onclick="showDocument('.$uploaded_document_file.','.$value['document_type_id'].')" title="View Document" style="cursor: pointer;"><u>'.$value['uploaded_document_number'].'</u></span>';                                                                 
                $container_number = '<a href="'.base_url().'container_details?text='.rtrim(strtr(base64_encode("id=".$value['container_id']), '+/', '-_'), '=').' "><u>'.$value['container_number'].'</u></a>';                                  
                if (!in_array($value['container_id'], $containerIdArr)) { 
                    $resultSKU = getSKUNumberAndQtyByContainerId($value['container_id']);                    
                    $skuArr = array();
                    $totalQty = 0;
                    $skuStr = '';
                    if(!empty($resultSKU)){
                        foreach ($resultSKU as $skuValue) { 
                            $skuArr[] = $skuValue['sku_number'];
                            $totalQty += $skuValue['quantity'];
                        }
                    } 
                    if(!empty($skuArr)){
                       $skuStr = implode(",", $skuArr);
                    } 
                }else{ 
                    $skuStr = $skuStrHold[$value['container_id']];
                    $totalQty = $totalQtyHold[$value['container_id']];
                }  
                ?> 
                <tr>
                    <td><?=$uploaded_document_number?></td>
                    <td><?=$container_number?></td>
                    <td><?=(!empty($value['document_date'])?date('d-M-Y',strtotime($value['document_date'])):'')?></td>
                    <td><?=(!empty($value['upload_date'])?date('d-M-Y',strtotime($value['upload_date'])):'')?></td>
                    <td style="width:500px"><?=$skuStr?></td>
                    <td><?php  echo ($totalQty == 0 ) ? '' : $totalQty; ?></td> 
                </tr>
                
           <?php   
                $containerIdArr[] = $value['container_id'];
                $skuStrHold[$value['container_id']] = $skuStr;
                $totalQtyHold[$value['container_id']] = $totalQty; 
            } 
            ?>
        </tbody>
    </table>
</div>