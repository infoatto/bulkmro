<?php
//echo '<pre>'; print_r($containersSKUData);die;
    if ($getSelectedCustomField) { ?>
      <!--start container sku table-->
      <?php if (!empty($containersSKUData)) { ?>
          <div class="bp-list-wrapper">
              <div class="table-responsive  table-1000">
                <table class="table table-striped document-sku-table no-footer" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Container</th>
                            <th>SKU</th>
                            <th style="width:14%">Quantity</th> 
                            <th style="width:14%">Manufacturer</th> 
                            <th style="width:17%">SGS Seal</th>
                            <th style="width:17%">FF Seal</th>
                            <th style="width:17%">Shipping Container</th> 
                            <th style="width:17%"> Lot Number</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($containersSKUData as $containerId => $containerSkuData) {
                                $containerNumberArr = array();
                                $businessNameArr = array();
                                foreach ($containerSkuData as $containerSkuId => $value) { 
                                    // print_r($value);
                                    
                                    ?>
                                <tr>
                                    <td>
                                        <?php
                                            if (!in_array($value['container_number'], $containerNumberArr)) {
                                                echo $value['container_number']; 
                                            ?>
                                              
                                            <?php }
                                            ?>
                                    </td>
                                    <td><?= $value['sku_number'] ?>
                                        <input type="hidden" name="all_container_id[<?= $value['container_id']?>]" id="all_container_id<?= $containerSkuId ?>" value="<?= (!empty($value['container_id'])) ? $value['container_id'] : '0' ?>">
                                        <input type="hidden" name="container_sku_id[<?= $value['container_id']?>][<?= $containerSkuId ?>]" id="container_sku_id<?= $containerSkuId ?>" value="<?= (!empty($value['container_sku_id'])) ? $value['container_sku_id'] : '0' ?>">
                                    </td>
                                    <td>
                                        <input type="number" name="container_sku_qty[<?= $value['container_id']?>][<?= $containerSkuId ?>]" id="container_sku_qty<?= $containerSkuId ?>" value="<?= (!empty($value['quantity'])) ? $value['quantity'] : '0' ?>" class="input-form-mro"  placeholder="Enter quantity">
                                    </td>
                                    
                                    <td>
                                        <select name="manufacturer[<?= $value['container_id']?>][<?= $containerSkuId ?>]"  class="basic-single select-form-mro" id="manufacturer<?= $containerSkuId ?>">
                                            <option value="">Manufacturer</option>
                                                <?php if($manufacturer){ 
                                                    $sel ="";
                                                    foreach ($manufacturer as $manufacturerkey => $manufacturerval) { 
                                                        $sel =($manufacturerval['business_partner_id'] == $value['manufacturer_id']?"selected":""); 
                                                        ?>
                                                        <option value="<?= $manufacturerval['business_partner_id']; ?>"<?= $sel?> ><?= $manufacturerval['alias']; ?> </option>
                                                    <?php } ?>
                                                <?php } ?>
                                        </select>
                                    </td> 
                                    <td>
                                    <?php  if (!in_array($value['alias'], $businessNameArr)) { ?>
                                        <input type="text" name="sgs_seal[<?= $value['container_id'] ?>]"  id="sgs_seal[<?= $value['container_id'] ?>]" placeholder="SGS Seal " value="<?= (!empty($value['sgs_seal'])?$value['sgs_seal']:"") ?>" class="input-form-mro"> 
                                    <?php } ?>
                                    </td>
                                    <td>
                                    <?php  if (!in_array($value['alias'], $businessNameArr)) { ?>
                                        <input type="text" name="ff_seal[<?= $value['container_id'] ?>]"  id="ff_seal[<?= $value['container_id'] ?>]" placeholder="FF Seal" value="<?= (!empty($value['sgs_seal'])?$value['sgs_seal']:"") ?>" class="input-form-mro"> 
                                    <?php } ?>
                                    </td>
                                    
                                    <td>
                                    <?php  if (!in_array($value['alias'], $businessNameArr)) { ?>
                                        <input type="text" name="shipping_container[<?= $value['container_id'] ?>]"  id="shipping_container[<?= $value['container_id'] ?>]" placeholder="Shipping Container" value="<?= (!empty($value['shipping_container'])?$value['shipping_container']:"") ?>" class="input-form-mro"> 
                                    <?php } ?>
                                    </td>
                                    <td>
                                        <?php  if (!in_array($value['alias'], $businessNameArr)) { ?>
                                        <input type="text" name="lot_number[<?= $value['container_id'] ?>]"  id="lot_number[<?= $value['container_id'] ?>]" placeholder="Lot Number" value="<?= (!empty($value['lot_number'])?$value['lot_number']:"") ?>" class="input-form-mro"> 
                                        <?php } ?>
                                    </td> 
                                </tr>
                            <?php
                                    $containerNumberArr[] = $value['container_number'];
                                    $businessNameArr[] = $value['alias'];
                                } ?>
                            <!-- <tr style="height: 30px;">
                                <td colspan="4"></td>
                            </tr> -->
                        <?php

                            } ?>
                    </tbody>
                </table>
              </div>
          </div>
      <?php } ?>
      <!--end container sku table-->
      
      
      <?php if (!empty($containersSKUData)) { ?>
       <?php
            $cnt = 0;
            foreach ($containersSKUData as $containerId => $containerSkuData) {  
                foreach ($containerSkuData as $containerSkuId => $value) {  
                    if ($cnt==0) { 
                            $businesspartnerid = !empty($supplier_update[0]['supplier_id'])?$supplier_update[0]['supplier_id']:$value['business_partner_id'];
                            $container_number = $value['container_number'];
                            if(empty($supplier_update[0]['supplier_id'])){
                                echo "<script>getPersonDetail($businesspartnerid,$container_number);</script>"; 
                            }
                            ?>  
      
                    <div class="form-group supplier"> 
                        <label for="">Supplier Name</label> 
                            <select name="supplier_name[<?= $value['container_id'] ?>]" id="supplier_name" class="basic-single select-form-mro" onchange="getPersonDetail(this.value,'<?= $value['container_number']?>');">
                                <option value="">Supplier</option>
                                    <?php if($supplier_name){ 
                                        $sel = "";
                                        foreach ($supplier_name as $supplierkey => $supplierval) { 
                                            $sel =($supplierval['business_partner_id'] == (!empty($supplier_update[0]['supplier_id'])?$supplier_update[0]['supplier_id']:$value['business_partner_id'])?"selected":""); 
                                            ?>
                                            <option value="<?= $supplierval['business_partner_id']; ?>"<?= $sel;?>><?= $supplierval['alias']; ?> </option>
                                        <?php } ?>
                                    <?php } ?>
                            </select> 
                    </div>  
                    <div class="form-group "> 
                        <label for="">Contact Name</label> 
                        <input type="text" name="contactpersonname[<?= $value['container_id']?>]" id="contactpersonname<?=$value['container_number']?>" value="<?=(!empty($supplier_update[0]['supplier_contactperson'])?$supplier_update[0]['supplier_contactperson']:'') ?>" class="input-form-mro">                         
                    </div> 
                    <div class="form-group "> 
                        <label for="">Contact Number</label> 
                        <input type="text" name="contactpersonnumber[<?= $value['container_id']?>]" id="contactpersonnumber<?=$value['container_number']?>" value="<?=(!empty($supplier_update[0]['supplier_contactnumber'])?$supplier_update[0]['supplier_contactnumber']:'') ?>" class="input-form-mro">                        
                    </div>
            <?php
                    }
                    $cnt++;
                }  
            } 
      }
      ?>

      <?php foreach ($getSelectedCustomField as $key => $value) { ?>

          <input type="hidden" name="custom_field_structure_id[]" id="custom_field_structure_id" value="<?= $value['custom_field_structure_id'] ?>">
          <input type="hidden" name="document_type_id" id="document_type_id" value="<?= $value['document_type_id'] ?>">
          <input type="hidden" name="sub_document_type_id" id="sub_document_type_id" value="<?= $value['sub_document_type_id'] ?>">
          <input type="hidden" name="uploaded_document_number" id="uploaded_document_number" value="<?= $value['uploaded_document_number'] ?>">

           <?php if ($value['custom_field_type'] == 'date') {  
          
                if($value['custom_field_slug'] == 'date_of_inspection'){ ?>
                    <div class="form-group">
                        <label for=""><?= $value['custom_field_title'] ?></label>
                        <input type="text" name="date_of_inspection"  value="<?= (!empty($get_date_of_inspection[0]['date_of_inspection'])?date('d-m-Y', strtotime($get_date_of_inspection[0]['date_of_inspection'])):"")?>" id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter data" class="form-control input-form-mro datepicker date-form-field">
                    </div>
                <?php } else{ ?> 
                    <div class="form-group ">
                        <label for=""><?= $value['custom_field_title'] ?></label> 
                        <input type="text"  name="<?= $value['custom_field_structure_id'] ?>"  id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter date"  readonly class="form-control input-form-mro datepicker date-form-field">
                    </div>
                <?php } ?> 
              
          <?php  } elseif ($value['custom_field_type'] == 'text') {  
          
                if($value['custom_field_slug'] == 'place_of_inspection'){ ?>
                    <div class="form-group">
                        <label for=""><?= $value['custom_field_title'] ?></label>
                        <input type="text" name="place_of_inspection"  value="<?= (!empty($get_place_of_inspection[0]['place_of_inspection'])?$get_place_of_inspection[0]['place_of_inspection']:"")?>" id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter data" class="input-form-mro">
                    </div>
                <?php } else{ ?> 
                    <div class="form-group">
                        <label for=""><?= $value['custom_field_title'] ?></label>
                        <input type="text" name="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>"  id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter data" class="input-form-mro">
                    </div>
                <?php } ?> 
          <?php } elseif ($value['custom_field_type'] == 'dropdown') { ?>

              <div class="form-group">
                  <div class="form-group">
                      <label for=""><?= $value['custom_field_title'] ?></label>
                      <div class="multiselect-dropdown-wrapper">
                          <div class="md-value">
                              Select value
                          </div>
                          <div class="md-list-wrapper">
                              <input type="text" placeholder="Search" class="md-search">
                              <div class="md-list-items">
                                  <?php if ($ListOfContainerData) {
                                        foreach ($ListOfContainerData as $key => $value) { ?>
                                          <div class="mdli-single ud-list-single">
                                              <label class="container-checkbox"> <?= $value['containerData']['container_number'] ?>
                                                  <input type="checkbox" id="<?= $value['custom_field_structure_id']; ?>" value="<?= $value['containerData']['container_id'] ?>" name="<?= $value['custom_field_structure_id'] ?>">
                                                  <span class="checkmark-checkbox"></span>
                                              </label>
                                          </div>
                                  <?php
                                        }
                                    }
                                    ?>
                              </div>
                              <div class="md-cta">
                                  <!-- <a href="#/" class="btn-primary-mro md-done">Done</a>
                            <a href="#/" class="btn-secondary-mro md-clear">Clear All</a> -->
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

          <?php } elseif ($value['custom_field_title'] == 'number') { ?>

          <?php } ?>
  <?php }
    } ?>
<script>
      // INITIALIZE DATEPICKER PLUGIN
      $('.datepicker').datepicker({
                format: 'dd-mm-yyyy'
            });
</script>