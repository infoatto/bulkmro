<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("listOfContainer"); ?>">Container Bible</a>
                                <span>></span>
                                <p>Update Information for Shipping</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Update Information for Shipping</h1>
                            </div>
                        </div>
                        <div class="title-sec-right"> 
                            <button type="submit" class="btn-primary-mro">Save</button>&nbsp&nbsp&nbsp
                            <button type="button" class="btn-transparent-mro cancel">Cancel</button> 
                        </div>
                    </div>
                    <div id="docuement_loader" style="display: none;">
                        <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                    </div>
                    <div class="page-content-wrapper master-order-wrapper"> 
                        <div class="scroll-content-wrapper master-order">
                            <div class="scw-left"> 
                                Filters
                                <div class="applied-filters-list">
                                <?php 
                                if(!empty($filter)){
                                    foreach ($filter as $value) {
                                        ?>
                                        <div class="applied-filter-single">
                                            <p class="afs-title"><?=$value['title']?></p>
                                            <p class="afs-value"><?=$value['value']?></p>
                                        </div>
                                        <?php
                                    }
                                }
                                ?> 
                                </div>
                                Selected Containers
                                <?php foreach ($containers as $value) { ?>
                                    <div style="margin-top:5px;">
                                        <label class="container-checkbox">
                                            <input class="chk" type="checkbox" id="<?= $value['container_id']; ?>" name="container_ids[]" value="<?= $value['container_id']; ?>" checked>                                                         
                                            <span class="checkmark-checkbox"></span><?= $value['container_number']; ?>
                                        </label> 
                                    </div> 
                                <?php } ?>
                            </div> 
                            <div class="scw-right">  
                                <div class="new-container-details11">  
                                    <div class="form-row form-row-3">
                                        <div class="form-group">
                                            <label for="">Container Status</label>
                                            <input type="hidden" name="last_container_status_id" id="last_container_status_id" value="<?=$container_details['last_container_status_id']?>">  
                                            <div id="container_status_id_div">
                                                <select name="container_status_update_id" id="container_status_update_id" onchange="checkPreviousStatus(<?=$container_details['last_container_status_id']?>,this.value)" class="basic-single select-form-mro" style="width: 100%;">
                                                    <option value="" disabled selected hidden>Select Container Status</option>
                                                    <?php
                                                    foreach ($containerStatusData as $value) {
                                                        $lastContainerStatusId = isset($container_details['last_container_status_id'])?$container_details['last_container_status_id']:'';
                                                       ?> 
                                                       <option value="<?php echo $value['container_status_id']; ?>"   <?php
                                                            if ($value['container_status_id'] == $lastContainerStatusId) {
                                                                echo "selected";
                                                            }
                                                            ?>>
                                                           <?php echo $value['container_status_name']; ?> 
                                                       </option> 
                                                       <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>    
                                        </div> 
                                        <div class="form-group input-edit" id="freight_forwarder_div">
                                            <label for="">Freight Forwarder</label>
                                            <select name="freight_forwarder_id" id="freight_forwarder_id" class="basic-single select-form-mro" style="width: 100%;"> 
                                                <option value="0">Select Freight Forwarder</option>
                                                <?php
                                                foreach ($freightForwarder as $value) {
                                                    $bpID = (isset($container_details['freight_forwarder_id']) ? $container_details['freight_forwarder_id'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['business_partner_id']; ?>"  <?php
                                                    if ($value['business_partner_id'] == $bpID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['alias']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div>
                                        <div class="form-group input-edit" id="cha_div">
                                            <label for="">CHA</label>
                                            <select name="broker_id" id="broker_id" class="basic-single select-form-mro" style="width: 100%;"> 
                                                <option value="0">Select CHA</option>
                                                <?php
                                                foreach ($chaDetails as $value) {
                                                    $bpID = (isset($container_details['broker_id']) ? $container_details['broker_id'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['business_partner_id']; ?>"  <?php
                                                    if ($value['business_partner_id'] == $bpID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['alias']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="form-row form-row-3">  
                                        <div class="form-group">
                                            <label for="">Actual date of Departure</label>
                                            <input type="text" name="revised_etd" id="revised_etd" value="<?php echo(!empty($container_details['revised_etd'])) ? date('d-m-Y', strtotime($container_details['revised_etd'])) : '' ?>" onchange="fillETT();" class="form-control valid input-form-mro datepicker date-form-field"> 
                                        </div>
                                        <div class="form-group">
                                            <label for="">Actual date of Arrival</label>
                                            <input type="text" name="revised_eta" id="revised_eta" value="<?php echo(!empty($container_details['revised_eta'])) ? date('d-m-Y', strtotime($container_details['revised_eta'])) : '' ?>" onchange="fillETT();" class="form-control valid input-form-mro datepicker date-form-field"> 
                                        </div>     
                                        <div class="form-group"><!--input-edit-->
                                            <label for="">Actual Transit Time In Days</label>
                                            <input type="text" name="revised_ett" id="revised_ett" value="<?php echo(!empty($container_details['revised_ett'])) ? $container_details['revised_ett'] : ""; ?>" placeholder="No ETA or ETD Entered" class="input-form-mro">                                            
                                        </div>
                                    </div>
                                    
                                    <div class="form-row form-row-3">
                                        <div class="form-group">
                                            <label for="">Liner Name</label>
                                            <select name="liner_name" id="liner_name" class="basic-single select-form-mro" style="width: 100%;"> 
                                                <option value="0">Select Liner Name</option>
                                                <?php
                                                foreach ($liner as $value) {
                                                    $linerID = (isset($container_details['liner_name']) ? $container_details['liner_name'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['liner_id']; ?>"  <?php
                                                    if ($value['liner_id'] == $linerID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['liner_name']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div>
                                        <div class="form-group">
                                            <label for="">Liner Tracker URL</label>
                                            <input type="text" name="liner_tracker_url" id="liner_tracker_url" value="<?php echo(!empty($container_details['liner_tracker_url'])) ? $container_details['liner_tracker_url'] : ""; ?>" placeholder="Enter Liner Tracker URL" class="input-form-mro">
                                        </div>
                                        
                                        <div class="form-group">
                                            <label for="">POL</label>
                                            <select name="pol" id="pol" class="basic-single select-form-mro" style="width: 100%;"> 
                                                <option value="0">Select POL</option>
                                                <?php
                                                foreach ($pol as $value) {
                                                    $polID = (isset($container_details['pol']) ? $container_details['pol'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['pol_id']; ?>"  <?php
                                                    if ($value['pol_id'] == $polID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['pol_name']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select>                                             
                                        </div>
                                    </div>
                                    <div class="form-row form-row-3"> 
                                        <div class="form-group ">
                                            <label for="">POD</label>
                                            <select name="pod" id="pod" class="basic-single select-form-mro" style="width: 100%;"> 
                                                <option value="0">Select POD</option>
                                                <?php
                                                foreach ($pod as $value) {
                                                    $podID = (isset($container_details['pod']) ? $container_details['pod'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['pod_id']; ?>"  <?php
                                                    if ($value['pod_id'] == $podID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['pod_name']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div> 
                                        
                                        <div class="form-group">
                                            <label for="">Vessel Name</label>
                                            <input type="text" name="vessel_name" id="vessel_name" value="<?php echo(!empty($container_details['vessel_name'])) ? $container_details['vessel_name'] : ""; ?>" placeholder="Enter Vessel Name" class="input-form-mro">
                                        </div>
                                        <div class="form-group">
                                            <label for="">Vessel Tracker URL</label>
                                            <input type="text" name="vessel_tracker_url" id="vessel_tracker_url" value="<?php echo(!empty($container_details['vessel_tracker_url'])) ? $container_details['vessel_tracker_url'] : ""; ?>" placeholder="Enter Vessel Tracker URL" class="input-form-mro">
                                        </div>
                                    </div>
                                  
                                    <div class="form-row form-row-3">
                                        <div class="form-group">
                                            <label for="">Flag</label>
                                            <select name="flag" id="flag" class="basic-single select-form-mro" style="width: 100%;"> 
                                                <option value="0">Select Flag</option>
                                                <?php
                                                foreach ($flag as $value) {
                                                    $flagID = (isset($container_details['flag']) ? $container_details['flag'] : 0);
                                                    ?> 
                                                    <option value="<?php echo $value['flag_id']; ?>"  <?php
                                                    if ($value['flag_id'] == $flagID) {
                                                        echo "selected";
                                                    }
                                                    ?>>
                                                                <?php echo $value['flag_name']; ?> 
                                                    </option> 
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </div>  
                                        <div class="form-group">
                                            <label for="">POD Terminal</label>
                                            <input type="text" name="terminal" id="terminal" value="<?php echo(!empty($container_details['terminal'])) ? $container_details['terminal'] : ""; ?>" placeholder="Enter Terminal" class="input-form-mro">
                                        </div>
                                    </div> 
                                   
                                </div>  
                            </div> 
                        </div>
                    </div>
                    
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left"> </div>
                        <div class="title-sec-right"> 
                            <button type="submit" class="btn-primary-mro">Save</button>&nbsp&nbsp&nbsp
                            <button type="button" class="btn-transparent-mro cancel">Cancel</button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section> 
<script>

    //code for fill Actual date of Arrival
    function fillRevisedETA(value) {
        $('#revised_eta').val(value);
    }

    //code for fill Actual date of Departure
    function fillRevisedETD(value) {
        $('#revised_etd').val(value);
    }

    function fillETT() {
        var revised_eta = $('#revised_eta').val();
        var revised_etd = $('#revised_etd').val();
        var act = "<?php echo base_url(); ?>container/getETTDays";
        $.ajax({
            type: 'GET',
            url: act,
            data: {revised_eta: revised_eta, revised_etd: revised_etd},
            dataType: "json",
            success: function (data) {
                $('#revised_ett').val(data.days);
            }
        });
    }
    
    function checkPreviousStatus(last_container_status_id,container_status_id){ 
        var act = "<?php echo base_url(); ?>listOfContainer/checkPreviousContainerStatus";
        $.ajax({
            type: 'GET',
            url: act,
            data: {last_container_status_id: last_container_status_id, container_status_id: container_status_id},
            dataType: "json",
            success: function (response) {
                if (!response.success) {
                    showInsertUpdateMessage(response.msg, response); 
                    var chtml = '<select name="container_status_update_id" id="container_status_update_id" onchange="checkPreviousStatus(<?=$container_details['last_container_status_id']?>,this.value)" class="basic-single select-form-mro" style="width: 100%;">';
                                    chtml+=  '<option value="" disabled selected hidden>Select Container Status</option>';
                                        <?php
                                        foreach ($containerStatusData as $value) {
                                            $lastContainerStatusId = isset($container_details['last_container_status_id'])?$container_details['last_container_status_id']:'';
                                           ?> 
                                           chtml+='<option value="<?php echo $value['container_status_id']; ?>"   <?php
                                                if ($value['container_status_id'] == $lastContainerStatusId) {
                                                    echo "selected";
                                                }
                                                ?>>';
                                          chtml+='<?php echo $value['container_status_name']; ?>'; 
                                          chtml+='</option>'; 
                                          <?php
                                        }
                                        ?>
                                  chtml+='</select>'; 
                    $('#container_status_id_div').html(chtml);  
                    $('.basic-single').select2();
                }                
            }
        });
    } 

    $(document).ready(function () {
        var vRules = {};
        var vMessages = {};
        //check and save city data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>listOfContainer/multiUpdateContainerSubmit";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                        $("#docuement_loader").show();
                    },
                    success: function (response) {
                        $("#docuement_loader").hide();
                        showInsertUpdateMessage(response.msg, response);
                        if (response.success) {
                            setTimeout(function () {
                                window.location = "<?= base_url('listOfContainer') ?>";
                            }, 3000);
                        }
                        $(".btn-primary-mro").show();
                    }
                });
            }
        });

    });

    $('.cancel').click(function (event) {
        window.location = "<?= base_url() ?>listOfContainer";
    });
</script>