<?php
if($getCustomField){ ?>
<div class="form-group">
    <label for="">Business Partner Alias</label>
    <input type="text"  value="<?= $getCustomField[0]['alias']?>" name="business_partner_name"   id="business_partner_name" placeholder="Enter data" class="input-form-mro">
    <input type="hidden"  value="<?= $getCustomField[0]['business_partner_id']?>" name="business_partner_id"   id="business_partner_id" placeholder="Enter data" class="input-form-mro">
</div>
<div class="form-group">
    <input type="text" name="invoice_file_name"  value="<?= $getCustomField[0]['uploaded_invoice_file']?>"  id="invoice_file_name" placeholder="Enter data" class="input-form-mro">
    <i class="icon-edit"></i> 
    <input type="hidden" name="old_invoice_file_name"  value="<?= $getCustomField[0]['uploaded_invoice_file']?>"  id="old_invoice_file_name" placeholder="Enter data" class="input-form-mro">
</div>
<div class="form-group">
    <label for="">Total Invoice Value</label>
    <input type="text" name="invoice_value"  value=""  id="invoice_value" placeholder="Enter data" class="input-form-mro">
</div>
<div class="form-group">
    <label for="">Invoice Date</label>
    <input type="text" name="invoice_date"  value=""  id="invoice_date" placeholder="Enter data" readonly class="form-control input-form-mro datepicker date-form-field">
</div>
<div class="form-group">
    <label for="">Invoice Status</label>
    <select name="invoice_status"   class="input-form-mro" id="invoice_status">
        <option value="">Select Invoice Status</option>
            <option value="paid">Paid</option>
            <option value="unpaid">Un-Paid</option>
            <!-- <option value="partial">Partial</option> -->
    </select>
</div>
<div class="form-group">
    <label for="">Paid Date</label>
    <input type="text" name="paid_date"  value=""  id="paid_date" placeholder="Enter data"  readonly class="form-control input-form-mro datepicker date-form-field">
</div>

<div class="form-group">
    <label for="">SKU</label>
    <select name="sku_id" class="input-form-mro basic-single select-large"  onchange="getSku(this.value)" id="sku">
        <option value="">Select Sku</option>
            <?php foreach ($sku_details as $key => $value) { ?>
                <option value="<?= $value['sku_id'];?>"><?= $value['sku_number'];?></option>
            <?php } ?>
    </select>
</div>

<div id="sku_div">

</div>



<?php } ?>
<script>
        $('.datepicker').datepicker({
                format: 'dd-mm-yyyy'
            });
</script>