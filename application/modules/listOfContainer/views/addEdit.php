<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form name="addEditForm" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("listOfDocument"); ?>">Document Type</a>
                                <span>></span>
                                <p>Add Document Type</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add Document</h1>
                            </div>
                        </div>          
                        <button class="btn-primary-mro">Save</button>
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="document_type_id" id="document_type_id" value="<?php echo(!empty($document_type_details[0]['document_type_id'])) ? $document_type_details[0]['document_type_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="documentName">Document Type Name</label>
                                    <input type="text" name="document_type_name" id="document_type_name" class="input-form-mro" value="<?php echo(!empty($document_type_details[0]['document_type_name'])) ? $document_type_details[0]['document_type_name'] : ""; ?>">
                                </div>
                             
                                <div class="form-group">
                                    <label for="Status">Status</label>
                                    <select name="status" id="status" class="select-form-mro">
                                        <option value="Active" <?php echo(!empty($document_type_details['status']) && $document_type_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                        <option value="In-active" <?php echo(!empty($document_type_details['status']) && $document_type_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-row form-row-3">Add Sub Document type</div>
                            
                            <div>
                                <button type="button" class="btn btn-primary" onclick="addSubDocumentType(0);">+ Sub Document Type </button>
                            </div>
                            <br>
                            <div id="subDocumenttypeDiv">
                                <?php if(!empty($document_type_details) && isset($document_type_details)){
                                    $i= 0;
                                        foreach ($document_type_details as $key => $value) { 
                                            $i = ++$i;
                                            if(!empty($value['sub_document_type_name']) && isset($value['sub_document_type_name'])){ ?>
                                             <div class="form-row form-row-3 subdocumentDiv" id="subdocumentDiv<?= $value['sub_document_type_id'];?>">
                                                <div class="form-group">
                                                    <label for="documentName">Sub Document Type <?= $i?>  </label>
                                                    <input type="text" required name="sub_document_type_name_existing[<?= $value['sub_document_type_id'];?>]" value="<?= $value['sub_document_type_name'] ?>" id="sub_document_type_name" class="input-form-mro" value="">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <label for="Status">Status</label>
                                                    <select name="sub_status_existing[<?= $value['sub_document_type_id'];?>]" id="sub_status" class="select-form-mro" required>
                                                        <option value="Active" <?php echo(!empty($value['sub_status']) && $value['sub_status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                                        <option value="In-active" <?php echo(!empty($value['sub_status']) && $value['sub_status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                                    </select>
                                                </div>
                                                
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-sm btn-primary" onclick="removeSubDocumentType('<?= $value['sub_document_type_id'];?>')">X<button>
                                                </div>
                                            </div>
                                        <?php }
                                        }
                                    } ?>
                                
                            </div>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     
<script>

    function removeSubDocumentType(SubDocumentTypeID){
        
    var r = confirm("Are you sure to wants to delete this Sub Document type");
    if (r == true) {
      $.ajax({
        url: "<?php echo base_url(); ?>listOfDocument/deleteDocumentType",
        data: {
          "id": SubDocumentTypeID
        },
        dataType: 'json',
        cache: false,
        clearForm: false,
        async: false,
        type: "POST",
        success: function(response) {
            if(response.success){
                alert(response.msg)
                setTimeout(function() {
                location.reload();
                }, 1000); 
            }else{
                alert(response.msg)
                  return false;
            }
        }
      });
    }
  }

    function addSubDocumentType(){

        var cnt =  $('#subDocumenttypeDiv').children('.subdocumentDiv').length;
        // var cnt = cnt;
        cnt = ++cnt;
        console.log(cnt);
        var html = ' <div class="form-row form-row-3 subdocumentDiv" id="subdocumentDiv'+cnt+'" >'+
                        '<div class="form-group">'+
                            '<label for="documentName">Sub Document Type '+cnt+'</label>'+
                            '<input type="text" required name="sub_document_type_name['+cnt+']" id="sub_document_type_name" class="input-form-mro" value="">'+
                        '</div>'+
                        
                        '<div class="form-group">'+
                            '<label for="Status">Status</label>'+
                            '<select name="sub_status['+cnt+']" id="sub_status" class="select-form-mro" required>'+
                                '<option value="Active" <?php echo(!empty($document_type_details['status']) && $document_type_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>'+
                                '<option value="In-active" <?php echo(!empty($document_type_details['status']) && $document_type_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>'+
                            '</select>'+
                        '</div>'+
                        
                        '<div class="form-group">' +
                            '<button type="button" class="btn btn-sm btn-primary" onclick="removeSubDocument('+cnt+')">X<button>' +
                        '</div>' +
                    '</div>';
        $("#subDocumenttypeDiv").append(html);            
    }

    function removeSubDocument(cnt) {
        console.log(cnt);
        $("#subdocumentDiv"+cnt).remove();
    }

    $(document).ready(function () {
        var vRules = {
            "document_type_name": {required: true}
        };
        var vMessages = {
            "document_type_name": {required: "Please Enter Document Type Name."}
        };
        //check and save document
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>listOfDocument/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        console.log(response);
                        if (response.success) {
                            alert(response.msg);
                            setTimeout(function () {
                                window.location = "<?= base_url('listOfDocument') ?>";
                            }, 1000);
                        } else {
                            alert(response.msg);
                        }
                    }
                });
            }
        });

    });
</script>