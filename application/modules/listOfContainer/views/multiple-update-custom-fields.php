<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("listOfContainer"); ?>">Container Bible</a>
                                <span>></span>
                                <p>Update Information for <?=$document_type_name;?></p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Update Information for <?=$document_type_name;?></h1>
                            </div>
                        </div>
                        <div class="title-sec-right">  
                            <button type="submit" class="btn-primary-mro">Save</button>&nbsp&nbsp&nbsp
                            <button type="button" class="btn-transparent-mro cancel">Cancel</button> 
                        </div>
                    </div>
                    <div id="docuement_loader" style="display: none;">
                        <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                    </div>
                    <div class="page-content-wrapper master-order-wrapper"> 
                        <div class="scroll-content-wrapper master-order">
                            <div class="scw-left"> 
                                Filters
                                <div class="applied-filters-list">
                                <?php 
                                if(!empty($filter)){
                                    foreach ($filter as $value) {
                                        ?>
                                        <div class="applied-filter-single">
                                            <p class="afs-title"><?=$value['title']?></p>
                                            <p class="afs-value"><?=$value['value']?></p>
                                        </div>
                                        <?php
                                    }
                                }
                                ?> 
                                </div>
                                Selected Containers
                                <?php foreach ($containers as $value) { ?>
                                    <div style="margin-top:5px;">
                                        <label class="container-checkbox">
                                            <input class="chk" type="checkbox" id="<?= $value['container_id']; ?>" name="container_ids[]" value="<?= $value['container_id']; ?>" checked>                                                         
                                            <span class="checkmark-checkbox"></span><?= $value['container_number']; ?>
                                        </label> 
                                    </div> 
                                <?php } ?>
                            </div> 
                            <div class="scw-right">  
                                <div class="new-container-details11">
                                    
                                    <div class="select-doc-wrapper">
                                        <div class="sd-left1" style="width:50%;">
                                            <?php
                                            echo $form_html;
                                            ?>               
                                        </div>
                                        <div style="width:5%;"></div>
                                        <div class="sd-right1" style="width:45%;">
                                            <?php
                                            echo $form_view;
                                            ?>              
                                        </div>
                                    </div>
                                     
                                </div>  
                            </div> 
                        </div>
                    </div>
                    
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left"> </div>
                        <div class="title-sec-right"> 
                            <button type="submit" class="btn-primary-mro">Save</button>&nbsp&nbsp&nbsp
                            <button type="button" class="btn-transparent-mro cancel">Cancel</button> 
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section> 
<script> 

    $(document).ready(function () {
        var vRules = {};
        var vMessages = {};
        //check and save city data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>listOfContainer/multiUpdateCustomFieldsSubmit";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                        $("#docuement_loader").show();
                    },
                    success: function (response) {
                        $("#docuement_loader").hide();
                        showInsertUpdateMessage(response.msg, response);
                        if (response.success) {
                            setTimeout(function () {
                                window.location = "<?= base_url('listOfContainer') ?>";
                            }, 3000);
                        }
                        $(".btn-primary-mro").show();
                    }
                });
            }
        });

    });

    $('.cancel').click(function (event) {
        window.location = "<?= base_url() ?>listOfContainer";
    });
    
    
    function getPersonDetail(supplier_id, container_number) { 
        if (supplier_id) {
            $.ajax({
                url: "<?= base_url('listOfContainer/getPersonDetail') ?>",
                type: "POST",
                dataType: "json",
                data: {supplier_id},
                success: function (response) {
                    if (response.success) {
                        $("#contactpersonname" + container_number).val(response.contactpersonname);
                        $("#contactpersonnumber" + container_number).val(response.contactpersonnumber);
                        // $("#product_desc").html(response.product_desc);
                        // $("#item_size").html(response.item_size);
                    } else {
                        // alert(response.msg)
                        // $("#").html(response.html);
                    }
                }
            });
        } else {
            alert("No Supplier selected");
        }
    }
</script>