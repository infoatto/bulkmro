<style> 
    .error{
        color: red;
    }   
</style>
<section class="main-sec">
    <div class="container">
        <div class="row">
<!--            <form action="" id="updateContainerStatus" method="post" enctype="multipart/form-data">  -->
                <div class="col-12">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <p>Container Bible</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title"><a href="#/" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> Containers</h1>
                            </div>
                        </div>
                        <div class="title-sec-right"> 
                            <div id="status_update_div" style="display: none;">
                                <?php if ($this->privilegeduser->hasPrivilege("MultipleContainerUpdate")) { ?>
<!--                                    <select name="container_status_update_id" id="container_status_update_id" class="basic-single select-form-mro" style="width: 200px;">
                                        <option value="" disabled selected hidden>Select Container Status</option>
                                        <?php
                                        foreach ($containerStatusData as $value) {
                                            ?> 
                                            <option value="<?php echo $value['container_status_id']; ?>">
                                                <?php echo $value['container_status_name']; ?> 
                                            </option> 
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <button type="submit" class="btn-primary-mro">Save</button>-->
                                    <button type="button" class="btn-primary-mro open-multi-update"><img src="assets/images/edit-icon.svg" alt="">Update</button>
                                <?php } ?>
                            </div>
                            <div id="document_upload_div">
                                <?php if ($this->privilegeduser->hasPrivilege("DocumentUpload")) { ?>
                                    <!--<a href="javascript:void(0);" class="btn-primary-mro upload-document" data-fancybox="dialog" data-src="#upload-doc-content"><img src="assets/images/upload-icon-white.svg" alt=""> Upload File</a>-->
                                    <a href="javascript:void(0);" class="btn-primary-mro upload-document" onclick="openDocModal()"><img src="assets/images/upload-icon-white.svg" alt=""> Upload File</a>
                                <?php } ?>
                            </div> 
                        </div>
                    </div>
                    <?php if (!empty($msg)) { ?>  
                        <div class="page-title-wrapper show-message" id="msg-id">
                            <h6><?= $msg; ?></h6>
                        </div> 
                    <?php } ?>

                    <div class="filter-sec-wrapper fs-white-bg" id="serachFilterDiv">
                        <div id="serchfilter" class="filter-sec-wrapper"> 
                            <div class="form-group mt20" id="container_div">  
                                <div class="multiselect-dropdown-wrapper" id="multiselect-dropdown-wrapper" style='width:165px'>
                                    <div class="md-value searchfilter">
                                        Container 
                                    </div>
                                    <div class="md-list-wrapper searchlist-option-wrapper">
                                        <input type="text" placeholder="Search" class="md-search">
                                        <div class="md-list-items ud-list">
                                        <?php if ($containerData) {
                                            foreach ($containerData as $key => $value) {
                                                ?>
                                                    <div class="mdli-single ud-list-single">
                                                        <label class="container-checkbox"> <?= $value['container_number'] ?>
                                                            <input type="checkbox" id="sSearch_1_<?= $value['container_id'] ?>"  value="<?= $value['container_id'] ?>" class="checkboxclass" name="sSearch_1[]">
                                                            <span class="checkmark-checkbox"></span>
                                                        </label>
                                                    </div>
                                                <?php
                                            }
                                        }
                                        ?> 
                                        </div>
                                        <div class="md-cta">
                                            <p class="btn-primary-mro md-done" onclick='getFilters(1);'>Done</p>
                                            <p class="btn-secondary-mro md-clear searchlisting-clear">Clear All</p>
                                        </div>
                                    </div>
                                </div>
                                <?php //echo singleSelectSearchDropDown($containerData, 'sSearch_1', 'Container', '', 'container_id', 'container_number', '', "style='width:100px'", "onchange='getFilters(1);'"); ?>  
                            </div> 

                            <div class="form-group  mt20" id="status_div">
                                <?php echo singleSelectSearchDropDown($containerStatus, 'sSearch_2', 'Status', '', 'container_status_id', 'container_status_name', 'select-large', '', "onchange='getFilters(2);'"); ?> 
                            </div> 

                            <div class="form-group  mt20" id="fri_div">
                                <?php echo singleSelectSearchDropDown($FRI, 'sSearch_7', 'FRI', '', 'uploaded_document_number', 'uploaded_document_number', '', "style='width:90px'", "onchange='getFilters(7);'"); ?>  
                            </div> 

                            <div class="form-group  mt20" id="ls_div">
                                <?php echo singleSelectSearchDropDown($LS, 'sSearch_8', 'LS', '', 'uploaded_document_number', 'uploaded_document_number', '', "style='width:90px'", "onchange='getFilters(8);'"); ?>                     
                            </div>

                            <div class="form-group  mt20" id="hbl_div">
                                <?php echo singleSelectSearchDropDown($HBL, 'sSearch_12', 'HBL', '', 'uploaded_document_number', 'uploaded_document_number', '', "style='width:140px'", "onchange='getFilters(12);'"); ?>                                          
                            </div>

                            <div class="form-group  mt20" id="internal_hbl_div">
                                <?php echo singleSelectSearchDropDown($internalHBL, 'sSearch_13', 'Internal HBL', '', 'uploaded_document_number', 'uploaded_document_number', '', "style='width:110px'", "onchange='getFilters(13);'"); ?>                                                               
                            </div>

                            <div class="form-group  mt20" id="internal_fcr_div">
                                <?php echo singleSelectSearchDropDown($internalFCR, 'sSearch_14', 'Internal FCR', '', 'uploaded_document_number', 'uploaded_document_number', '', "style='width:110px'", "onchange='getFilters(14);'"); ?>                    
                            </div>

                            <div class="form-group  mt20" id="revised_div"> 
                                <select name="sSearch_15" id="sSearch_15" class="searchInput basic-single" style="width:110px" onchange="getFilters(15);"> 
                                    <option value="" disabled selected hidden>Revised ETA</option>
                                    <?php foreach ($revisedETA as $value) { ?>
                                        <option value="<?= date('Y-m-d', strtotime($value['revised_eta'])) ?>"><?= date('d-M-Y', strtotime($value['revised_eta'])) ?></option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="form-group  mt20" id="vessel_div">
                                <?php echo singleSelectSearchDropDown($vesselData, 'sSearch_5', 'Vessel', '', 'vessel_name', 'vessel_name', 'select-large', '', "onchange='getFilters(5);'"); ?>                     
                            </div> 

                            <div class="form-group  mt20" id="manufacture_div">
                                <?php echo singleSelectSearchDropDown($manufacture, 'sSearch_16', 'Manufacturer', '', 'business_partner_id', 'alias', '', "style='width:120px'", "onchange='getFilters(16);'"); ?>                                         
                            </div>

                            <div class="form-group  mt20" id="product_div">
                                <?php echo singleSelectSearchDropDown($productData, 'sSearch_3', 'Product', '', 'sku_id', 'product_name', 'select-large', '', "onchange='getFilters(3);'"); ?>                    
                            </div> 

                            <div class="form-group  mt20" id="brand_div">
                                <?php echo singleSelectSearchDropDown($brandData, 'sSearch_4', 'Brand', '', 'brand_id', 'brand_name', 'select-medium', '', "onchange='getFilters(4);'"); ?>                     
                            </div>

                            <div class="form-group  mt20" id="ff_div">
                                <?php echo singleSelectSearchDropDown($freightForwarder, 'sSearch_6', 'Freight Forwarder', '', 'business_partner_id', 'alias', 'select-large', '', "onchange='getFilters(6);'"); ?>                    
                            </div>

                            <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                                <div class="form-group  mt20" id="bp_div">
                                    <?php echo singleSelectSearchDropDown($bpData, 'sSearch_9', 'Business Partner', '', 'business_partner_id', 'alias', 'select-large', '', "onchange='getFilters(9);'"); ?>                    
                                </div> 
                            <?php } ?>

                            <div class="form-group  mt20" id="bp_contract_div">
                                <?php echo singleSelectSearchDropDown($bpContractData, 'sSearch_10', 'BP Contract', '', 'bp_order_id', 'contract_number', '', "style='width:130px'", "onchange='getFilters(10);'"); ?>                     
                            </div> 

                            <div class="form-group  mt20" id="contract_div">
                                <?php echo singleSelectSearchDropDown($orderData, 'sSearch_11', 'Master Contract', '', 'order_id', 'contract_number', 'select-large', '', "onchange='getFilters(11);'"); ?> 
                            </div> 

                            <div class="form-group  mt20">
                                <select name="sSearch_17" id="sSearch_17" class="searchInput basic-single" style="width:110px" onchange="getFilters(50);"> 
                                    <option value="ASC">Ascending</option>
                                    <option value="DESC" selected="selected">Descending</option> 
                                </select>
                            </div> 

                            <div class="form-group  mt20">
                                <button type="reset"  onclick="refresh()"  class="select-small btn-primary-mro">
                                    Clear
                                </button>

                            </div> 

                        </div>
                    </div>

                    <div class="container-list-wrapper">  
                        <div style="width:100%"> 
                            <div id="filter-loader" style="display: none;text-align: center;">
                                <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                            </div>
                            <div id="containerListingDiv">  
                                <div class="filter-tags-wrapper">
                                    <div class="filter-tag-list">
                                        <button type="button"  onclick="allcheck()"  class="btn-sm btn-primary-mro">Select All</button>
                                        <button type="button"  onclick="alluncheck()"  class="btn-sm btn-primary-mro">Unselect All</button>
                                    </div>
                                    <p class="container-number"><?= $totalRecords ?> Results</p>
                                </div>
                                <?php
                                if ($ListOfContainerData) {
                                    foreach ($ListOfContainerData as $key => $value) {
                                        // echo "<pre>";
                                        //   print_r($value['documentType']);
                                        ?>
                                        <div class="container-single-wrapper">
                                            <div class="cs-info">
                                                <div class="cs-info-cta">
                                                    <?php if ($this->privilegeduser->hasPrivilege("ContainersDetailsView")) { ?>
                                                        <a href="<?= base_url() ?>container_details?text='<?= rtrim(strtr(base64_encode("id=" . $value['containerData']['container_id']), '+/', '-_'), '=') ?>' " title="Container Details">
                                                            <img src="<?php echo base_url(); ?>assets/images/view-btn-icon.svg" alt=""></a>
                                                    <?php } ?>
                                                    <?php if ($this->privilegeduser->hasPrivilege("ContainersDetailsAddEdit")) { ?>
                                                                <!-- <a href="#/"><img src="assets/images/upload-btn-icon.svg" alt=""></a> -->
                                                    <?php } ?>
                                                    <?php if ($this->privilegeduser->hasPrivilege("MultipleContainerUpdate")) { ?>
                                                        <div style="margin-top:10px;">
                                                          <label class="container-checkbox">
                                                            <input class="chk" type="checkbox" id="<?= $value['containerData']['container_id']; ?>" name="container_ids[]" value="<?= $value['containerData']['container_id']; ?>" onclick="show_update_option(this);">                                                         
                                                            <span class="checkmark-checkbox"></span>
                                                          </label> 
                                                        </div> 
                                                    <?php } ?>
                                                </div>
                                                <div class="cs-status-text">
                                                    <p class="container-number"><?= $value['containerData']['container_number'] ?></p>
                                                    <p class="container-status"><?= (!empty($value['containerStatus']) ? $value['containerStatus'] : "") ?></p>
                                                    <p class="last-update-title">Last Updated</p>
                                                    <p class="last-update-name"><?= $value['updatedBy'] ?></p>
                                                    <p class="last-update-date"><?= $value['updatedOn'] ?></p>
                                                </div>
                                            </div>
                                            <div class="cs-status-wrapper">
                                                <!-- <div class="connector"></div> -->
                                                <?php
                                                if ($value['documentType']) {
                                                    foreach ($value['documentType'] as $dockey => $docval) {
 
                                                        $colorClass = "css-grey";
                                                        $containerId = $value['containerData']['container_id'];
                                                        if (!empty($DocumentStatusData[$containerId][$docval['document_type_id']])) {
                                                            if ($DocumentStatusData[$containerId][$docval['document_type_id']]["status"] == "Unchecked") {
                                                                $colorClass = "css-yellow";
                                                            } else if ($DocumentStatusData[$containerId][$docval['document_type_id']]["status"] == "NotSet") {
                                                                $colorClass = "css-red";
                                                            } else {
                                                                $colorClass = "css-green";
                                                            }
                                                        }
                                                        if (!empty($containers[$containerId]['job_not_completed_document_type_id'])) {
                                                            if (in_array($docval['document_type_id'], $containers[$containerId]['job_not_completed_document_type_id'])) {
                                                                $colorClass = "css-red";
                                                            }
                                                        }
                                                        ?>



                                                        <div class="css-single">
                                                            <div class="css-flag <?= $colorClass ?>"></div>
                                                            <div class="css-single-data">
                                                                <p class="css-single-data-title"><?= $docval['document_type_name'] ?></p>
                                                                <?php
                                                                if (!empty($getuploaded_Document[$value['containerData']['container_id']][$docval['document_type_id']][0]['document_type_id'])) {
                                                                    foreach ($getuploaded_Document[$value['containerData']['container_id']] as $uploadkey => $uploadval) {

                                                                        if (!empty($uploadval[0]['document_type_id']) && $docval['document_type_id'] == $uploadval[0]['document_type_id']) {
                                                                            ?>

                                                                            <p class="css-single-data-text"><?= str_replace(',', '<br/>', $uploadval[0]['uploaded_document_number']); ?></p>
                                                                            <p class="css-single-data-date"><?= date('d-M-Y', strtotime($uploadval[0]['created_on'])) ?></p> 

                                                                        <?php } ?>

                                                                    <?php } ?> 
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    <?php
                                                    }
                                                    //  document type end here 
                                                }
                                                ?>
                                            </div>
                                        </div>
                                <?php }
                            }
                            ?>
                            </div>     
                        </div> 
                    </div>
                </div>
<!--            </form>-->
        </div>
    </div>
</section>
<!-- Upload Document-->
<div class="overlay-document"></div>
<div id="upload-document-first-modal" style="display:none" id="cnt1">
    <!-- Select document type -->
    <div class="mro-modal-header">
        <a href="javascript:void(0)" class="mro-close-modal">
            <img src="<?php echo base_url(); ?>assets/images/modal-close-dark.svg" onclick="closeModel()" alt="Close">
        </a>
    </div>
    <!-- <div class="fortoastrLoader"></div> -->
    <!-- <div class="toast-container" style="border:1px solid black"></div> -->
    <div class="doc-type-wrapper upload-fri-step-1" >
        <h3 class="form-group-title">Upload Document</h3>
        <div class="form-radio-wrapper">
            <div class="form-radio">
                <label class="container-radio">Document
                    <input type="radio" checked="checked" name="upload_type" value="upload-document">
                    <span class="checkmark"></span>
                </label>
            </div>
            <div class="form-radio">
                <label class="container-radio">Invoice
                    <input type="radio" name="upload_type" value="upload-invoice">
                    <span class="checkmark"></span>
                </label>
            </div>
        </div>
        <div class="form-cta">
            <button class="btn-primary-mro fri-step-1">Next</button>
        </div>
    </div>
    <!-- Select document type -->
</div> 

<div id="upload-doc-content" class="mro-modal-wrapper-document mro-upload-doc-modal-cls" style="display:none;min-width:100%;max-width:100%;">
    <!-- Upload FRI/LS Step 2 -->
    <div class="mro-modal-header">
        <a href="javascript:void(0)" class="mro-close-modal">
            <img src="<?php echo base_url(); ?>assets/images/modal-close-dark.svg" onclick="closeModel()" alt="Close">
        </a>
    </div>

    <div class="ud-step upload-fri-step-2" id="cnt2">
        <div class="upload-doc-title">
            <div class="ud-title-left">
                <a href="#/" class="fri-step-2-back"><img src="assets/images/ud-back.svg" alt="" onclick="backDocument('cnt1', 'cnt2')"></a>
                <h3>Container Listing</h3>
            </div>
        </div>
        <form action="" id="documentStep2" enctype="multipart/form-data"  method="post">
            <div class="select-doc-wrapper">
                <div class="sd-left">
                    <span id="document_type_div">
                        <h4 class="sd-subtitle">Document Type</h4>
                        <select id="document_type" name="document_type" class="select-doc-type select-form-mro mb20" onchange="getLsListing(this.value)" required>
                            <?php if ($documentType) { ?>
                                <option value="">Select Document Type</option>
                                <?php foreach ($documentType as $key => $value) { ?>
                                    <option value="<?= $value['document_type_id'] ?>"><?= $value['document_type_name'] ?></option>
                                <?php }
                            }
                            ?> 
                        </select>
                    </span>

                    <span id="business_partner_div">
                        <h4 class="sd-subtitle">Business Partner</h4>
                        <select id="business_partner" name="business_partner" class="select-doc-type select-form-mro mb20" onchange="getLsListing('invoice')" >
                            <?php if ($business_partner) { ?>
                                <option value="">Select Business Partner Alias</option>
                                    <?php foreach ($business_partner as $key => $value) { ?>
                                    <option value="<?= $value['business_partner_id'] ?>"><?= $value['alias'] ?></option>
                                <?php }
                            }
                            ?>
                        </select>
                    </span> 

                    <div id="Ls-listing"> 
                        <!-- this whole div while replace once user choose the document type other than FR LS dynamically via ajax call ls LISTING will appear than -->
                        <h4 class="sd-subtitle">Choose Containers</h4>
                        <div class="multiselect-dropdown-wrapper">
                            <div class="md-value">
                                Search for container 
                            </div>
                            <div class="md-list-wrapper">
                                <input type="text" placeholder="Search" class="md-search">
                                <div class="md-list-items ud-list">
                                <?php if ($ListOfContainerData) {
                                    foreach ($ListOfContainerData as $key => $value) {
                                        ?>
                                            <div class="mdli-single ud-list-single">
                                                <label class="container-checkbox"> <?= $value['containerData']['container_number'] ?>
                                                    <input type="checkbox" id="containerno<?= $value['containerData']['container_id'] ?>"  value="<?= $value['containerData']['container_id'] ?>" name="container_ids[]">
                                                    <span class="checkmark-checkbox"></span>
                                                </label>
                                            </div>
                                        <?php
                                    }
                                }
                                ?> 
                                </div>
                                <div class="md-cta">
                                    <p class="btn-primary-mro md-done">Done</p>
                                    <p class="btn-secondary-mro md-clear">Clear All</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="sd-right" id="containterListingForOtherDocument">
                    <p class="select-message">Choose containers to upload documents</p>
                </div>
                <div class="upload-doc-next">
                    <button type="submit" class="btn-primary-mro fri-step-2">Next</button>
                </div>
            </div>
        </form>                  
    </div>
    <!-- Upload FRI/LS Step 2 -->
    <!-- Upload FRI/LS Step 3 -->

    <div class="ud-step upload-fri-step-3" id="cnt3">
        <form action=""  id="documentStep3" enctype="multipart/form-data" method="post">
            <input type="hidden" name="document_type_id_hidden" id="document_type_id_hidden" value="0">    
            <div class="upload-doc-title">
                <div class="ud-title-left">
                    <a href="#/" class="fri-step-3-back"><img src="assets/images/ud-back.svg" alt="" onclick="backDocument('cnt2', 'cnt3')"></a>
                    <h3><span id="choose_container">Choose Containers</span> </h3>
                </div>
                <!-- <div class="ud-title-right">
                  <a href="#/" class="btn-primary-mro">Back</a>
                </div> -->

            </div>
            <div class="select-doc-wrapper">
                <div class="overlay-document-modal" style="display: none;"></div>
                <div class="sd-left">
                    <div>
                        <span id="selectedLsName"></span>  
                    </div>
                    <h4 class="sd-subtitle">
                        Selected Containers</h4>
                    <div class="ud-list mt20">
                        <div id="selectedContainer"></div>     
                    </div>
                </div>

                <div class="sd-right" >
                    <div id="docuement_loader" style="display: none;">
                        <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                    </div>
                    <div id="Lsdocumentupload">
                        <h4 class="sd-subtitle mb20">Upload Document</h4>
                        <input type="file" name="step3_document" id="step3_document">
                    </div>
                    <!-- <span id="step3_document_upload_error" class="error"></span> -->
                </div>
            </div>
            <div class="upload-doc-next">
                <button  type="submit"  class="btn-primary-mro fri-step-3">Next</button>
            </div>
        </form>
    </div>

    <!-- Upload FRI/LS Step 3 -->
    <!-- Upload FRI/LS Step 4 -->
    <div class="ud-step upload-fri-step-4" id="cnt4">
        <div class="upload-doc-title">
            <div class="ud-title-left">
                <a href="#/" class="fri-step-4-back"><img src="assets/images/ud-back.svg" alt="" onclick="backDocument('cnt3', 'cnt4')"></a>
                <h3>Upload Document</h3>
            </div>
            <!-- <div class="ud-title-right">
              <a href="#/" class="btn-primary-mro">Back</a>
            </div> -->
        </div>
        <div class="select-doc-wrapper">
            <div class="sd-left">
                <h4 class="sd-subtitle">Containers</h4>
                <div class="ud-list ud-list-doc-names mt20" id="step4_selectedContainerDocument"></div>
            </div>
            <div class="sd-right">
                <h4 class="sd-subtitle">
                    <span id="step4_uploadedContainerDocumentName">FRI Title</span></h4>
                <div class="ud-list ud-list-doc-names mt20" id="step4_selectedContainerUploadedDocument">
                </div>
            </div>
        </div>
        <div class="upload-doc-next">
            <button class="btn-primary-mro fri-step-4">Next</button>
        </div>
    </div>
    <!-- Upload FRI/LS Step 4 -->
    <!-- Upload FRI/LS Step 5 -->
    <div class="ud-step upload-fri-step-5" id="cnt5">
        <form id="documentStep5" enctype="multipart/form-data" method="post">
            <div class="upload-doc-title">
                <div class="ud-title-left">
                    <a href="#/" class="fri-step-5-back"><img src="assets/images/ud-back.svg" alt="" onclick="backDocument('cnt4', 'cnt5')"></a>
                    <h3>Review Document Titles</h3>
                </div>
                <div class="ud-title-right">
                    <button class="btn-grey-mro">Clear</button>
                    <button class="btn-primary-mro">Save</button>
                </div>
            </div>
            <div class="select-doc-wrapper">
                <div class="sd-left">
                    <h4 class="sd-subtitle mb20"></h4>
                    <div id="custom_data_field"></div>              
                </div>
                <div class="sd-right">
                    <h4 class="sd-subtitle"></h4>
                    <span id="form-view"></span>             
                </div>
            </div>
            <div class="upload-doc-next">
                <button class="btn-grey-mro">Clear</button>
                <button type="submit"  class="btn-primary-mro">Save</button>
            </div>
        </form>
    </div>
    <!-- Upload FRI/LS Step 5 -->
</div>
<!-- Upload Document--> 

<!-- Update Information Modal -->
<div id="update-information-modal" style="display:none">
    <form action="<?=base_url(); ?>listOfContainer/multipleUpdateContainers" id="updateMultipleContainer" method="post" enctype="multipart/form-data">
        <div class="mro-modal-header">
            <a href="javascript:void(0)" class="mro-close-modal close-multi-update">
                <img src="<?php echo base_url(); ?>assets/images/modal-close-dark.svg" alt="Close">
            </a>
        </div>
        <div class="doc-type-wrapper" >
            <h3 class="form-group-title">Edit Information For</h3> 
            <input type="hidden" id="hold_containerIds" name="hold_containerIds" value=""> 
            <input type="hidden" id="filters_data" name="filters_data" value=""> 
            <div class="form-radio-wrapper">
                <div class="form-radio document-shipping-info-div">
                    <label class="container-radio">Shipping Information
                        <input type="radio" checked="checked" name="update_shipping_information" class="update_shipping_information" value="shipping">
                        <span class="checkmark"></span>
                    </label>
                </div>
                <div class="form-radio document-custom-fields-div">
                    <label class="container-radio">Document Custom Fields
                        <input type="radio" name="update_shipping_information" class="document_custom_fields" value="custom">
                        <span class="checkmark"></span>
                    </label>
                </div>
            </div>
            <div class="form-group edit_select_doc_type"> 
                <select id="update_document_type" name="update_document_type" class="searchInput basic-single select-mro select-xl " style="width:320px;">
                    <?php if ($documentType) { ?>
                        <option value="">Select Document Type</option>
                        <?php foreach ($documentType as $key => $value) { ?>
                            <option value="<?= $value['document_type_id'] ?>"><?= $value['document_type_name'] ?></option>
                        <?php }
                    }
                    ?> 
                </select> 
              </div>
            <div class="form-cta">
                <button type="submit"  class="btn-primary-mro">Next</button>
            </div>
        </div>
    </form>
</div>

</body>

</html>

<script>
    function openDocModal() {
        // $('.overlay-document,.mro-modal-wrapper-document').show();        
        $('#upload-document-first-modal').show();
        $('.overlay-document').show();
        $.ajax({
            url: "<?= base_url('listOfContainer/setCurrentSessionId') ?>",
            type: "POST",
            dataType: "json",
            success: function (response) {
                console.log(response.msg);
            }
        });
    }

    $(document).ready(function () {        

        // start these functions are for LS n FRI document type  
        var vRules = {
            "document_type": {required: true}
        };
        var vMessages = {
            "document_type": {required: "Please Enter Document Type."}
        };
        $("#documentStep2").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>listOfContainer/submitFormStep2";

                $("#documentStep2").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        // $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        console.log(response);
                        showInsertUpdateMessage(response.msg, response);
                        if (response.success) {
                            if (response.type == 'LS') {
                                getSelectedContainer(response.type);
                            } else {
                                getSelectedLsContainer(response.type);
                            }
                            $('.upload-fri-step-2').hide();
                            $('.upload-fri-step-3').show();
                            $('#document_type_id_hidden').val(response.doc_id);
                        } else {
                            return false;
                        }
                    }
                });
            }
        });

        // for step 2 submission 
        var vRules = {
            "step3_document": {required: true}
        };
        var vMessages = {
            "step3_document": {required: "Please Select the document for upload"}
        };
        //check and save country
        $("#documentStep3").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>listOfContainer/submitFormStep3";

                $("#documentStep3").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                        $("#docuement_loader").show();
                        $(".overlay-document-modal").show();
                    },
                    success: function (response) {
                        // return false;
                        $(".btn-primary-mro").show();
                        $("#docuement_loader").hide();
                        $(".overlay-document-modal").hide();
                        console.log(response);
                        showInsertUpdateMessage(response.msg, response);
                        if (response.success) {
                            if (response.type == 'LS' || response.type == '') {
                                getSelectedContainerDocument(response.type);
                                getSelectedCustomField(response.type);
                                $('.upload-fri-step-3').hide();
                                $('.upload-fri-step-4').show();
                            } else {
                                getSelectedCustomField(response.type);
                                if (response.type == 'invoice') {
                                    $('.upload-fri-step-5').show();
                                    $('.upload-fri-step-3').hide();

                                }
                            }

                        } else {
                            $("#step3_document_upload_error").html(response.msg);
                            return false;
                        }
                    }
                });
            }
        });

        //check and save country
        $("#documentStep5").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>listOfContainer/submitFormStep5";
                $("#documentStep5").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        // $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        //Document successfully uploaded to containers
                        showInsertUpdateMessage(response.msg, response);
                        if (response.success) {
                            setTimeout(function () {
                                window.location = "<?= base_url('listOfContainer') ?>";
                            }, 1000);
                        } else {
                            return false;
                        }
                        $(".btn-primary-mro").show();
                    }
                });
            }
        });

        function getSelectedContainer() {
            $.ajax({
                url: "<?= base_url('listOfContainer/getSelectedContainer') ?>",
                type: "POST",
                dataType: "json",
                success: function (response) {
                    showInsertUpdateMessage(response.msg, response);
                    if (response.success) {
                        $("#selectedContainer").html(response.html);
                    }
                    $(".btn-primary-mro").show();

                }
            });
        }

        function getSelectedLsContainer(type) {
            $.ajax({
                url: "<?= base_url('listOfContainer/getSelectedLsContainer') ?>",
                type: "POST",
                dataType: "json",
                data: {type},
                success: function (response) {
                    if (response.success) {
                        $("#selectedContainer").html(response.html);
                        $("#selectedLsName").html(response.selectedLsName);
                        $("#choose_container").html("Choose LS");
                        $("#Lsdocumentupload").html(response.uploadSectionhtml);
                    } else {
                        showInsertUpdateMessage(response.msg, response);
                    }

                }
            });
        }

        function getSelectedContainerDocument(type) {
            $.ajax({
                url: "<?= base_url('listOfContainer/getSelectedContainerDocument') ?>",
                type: "POST",
                dataType: "json",
                data: {type},
                success: function (response) {
                    if (response.success) {
                        $("#step4_uploadedContainerDocumentName").html(response.document_prefix_name + ' Title ');
                        $("#step4_selectedContainerDocument").html(response.html_container);
                        $("#step4_selectedContainerUploadedDocument").html(response.html_uploaded);
                    } else {
                        showInsertUpdateMessage(response.msg, response);
                    }

                }
            });
        }


        function getSelectedCustomField(type) {
            $.ajax({
                url: "<?= base_url('listOfContainer/getSelectedCustomField') ?>",
                type: "POST",
                dataType: "json",
                data: {type},
                success: function (response) {
                    if (response.success) {
                        $("#custom_data_field").html(response.customField);
                        $("#form-view").html(response.formView);
                        $('.basic-single').select2({
                            searchInputPlaceholder: 'Search'
                        });
                    } else {
                        showInsertUpdateMessage(response.msg, response);
                    }

                }
            });
        }
        // end these functions are for LS n FRI document type  


        //  $(document).on("change",".searchInput",function(){
//        $(document).on('change', '.searchInput', function(){  
//            getFilterdata();
//        });


        //var container_id = $("#sSearch_1").val(); 
        var status = $("#sSearch_2").val();
        var product_id = $("#sSearch_3").val();
        var brand_id = $("#sSearch_4").val();
        var vessel_id = $("#sSearch_5").val();
        var freight_forwarder = $("#sSearch_6").val();
        var fri = $("#sSearch_7").val();
        var ls = $("#sSearch_8").val();
        var bp = null;
<?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
            bp = $("#sSearch_9").val();
<?php } ?>

        var bp_contract = $("#sSearch_10").val();
        var contract = $("#sSearch_11").val();
        var hbl = $("#sSearch_12").val();
        var internal_hbl = $("#sSearch_13").val();
        var internal_fcr = $("#sSearch_14").val();
        var revised_eta = $("#sSearch_15").val();
        var manufacturer = $("#sSearch_16").val();
        if (status != null || product_id != null || contract != null || brand_id != null || vessel_id != null || freight_forwarder != null || fri != null || ls != null || bp != null || bp_contract != null || hbl != null || internal_hbl != null || internal_fcr != null || revised_eta != null || manufacturer != null) {

            getFilterdata();
        }
<?php if (!empty($order_id) || !empty($bp_order_id)) { ?>
            getFilterdata('<?= $order_id ?>', '<?= $bp_order_id ?>');
<?php } ?>

    });

    function getFilterdata(orderId = '', bpOrderId = '', f_clear='') {
        $("#filter-loader").show();
        $("#containerListingDiv").html('');
        // get filter data 
        //var container_id = $("#sSearch_1").val();
        var container_id = new Array();
        $("#multiselect-dropdown-wrapper input[type=checkbox]:checked").each(function () { 
            container_id.push(this.value);
        });  
        var status = $("#sSearch_2").val();
        var product_id = $("#sSearch_3").val();
        var brand_id = $("#sSearch_4").val();
        var vessel_id = $("#sSearch_5").val();
        var freight_forwarder = $("#sSearch_6").val();
        var fri = $("#sSearch_7").val();
        var ls = $("#sSearch_8").val();
        var bp = '';
        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
        bp = $("#sSearch_9").val();
        <?php } ?>
        if (bpOrderId == '') {
            var bp_contract = $("#sSearch_10").val();
        } else {
            $("#sSearch_10").val(bpOrderId);
            var bp_contract = bpOrderId;
        }

        if (orderId == '') {
            var contract = $("#sSearch_11").val();
        } else {
            $("#sSearch_11").val(orderId);
            var contract = orderId;
        }

        var hbl = $("#sSearch_12").val();
        var internal_hbl = $("#sSearch_13").val();
        var internal_fcr = $("#sSearch_14").val();
        var revised_eta = $("#sSearch_15").val();
        var manufacturer = $("#sSearch_16").val();
        var order_by = $("#sSearch_17").val();

        $.ajax({
            url: "<?= base_url('listOfContainer/getFilterdata') ?>",
            type: "POST",
            dataType: "json",
            data: {container_id, status, product_id, brand_id, vessel_id, freight_forwarder, fri, ls, bp, bp_contract, contract, hbl, internal_hbl, internal_fcr, revised_eta, manufacturer, order_by,f_clear},
            success: function (response) {
                $("#filter-loader").hide();
                if (response.success) {
                    $("#containerListingDiv").html(response.containerListingDiv);
                } else {
                    $("#containerListingDiv").html(response.containerListingDiv);
                }
            }
        });
    }

    //  start  these functions are for othere document type  
    function getLsListing(document_type_id) {
        console.log(document_type_id);
        if (document_type_id) {
            if (!['2', '3'].includes(document_type_id) || document_type_id == 'invoice') {
                // to get the LS from tbl for this type of document 
                $.ajax({
                    url: "<?= base_url('listOfContainer/getLsListing') ?>",
                    type: "POST",
                    dataType: "json",
                    data: {document_type_id},
                    success: function (response) {
                        console.log(response);
                        if (response.success) {
                            $("#Ls-listing").html(response.html);
                            $("#containterListingForOtherDocument").html(response.rightSideMsg);
                        } else {
                            showInsertUpdateMessage(response.msg, response);
                            $("#Ls-listing").html(response.html);
                            $("#containterListingForOtherDocument").html(response.rightSideMsg);
                        }
                    }
                });
            } else {
                // call for container listing for  fr n LS 
                $.ajax({
                    url: "<?= base_url('listOfContainer/getContainerListing') ?>",
                    type: "POST",
                    dataType: "json",
                    data: {document_type_id},
                    success: function (response) {
                        if (response.success) {
                            $("#Ls-listing").html(response.html);
                            $("#containterListingForOtherDocument").html(response.rightSideMsg);
                        } else {
                            showInsertUpdateMessage(response.msg, response);
                            $("#Ls-listing").html(response.html);
                            $("#containterListingForOtherDocument").html(response.rightSideMsg);
                        }
                    }
                });
            }

        } else {
            showInsertUpdateMessage("Please select the document type!", false);
        }
    }
    //  end  these functions are for othere document type 

    /* function of close popup */
    function closeModel() {
        if (confirm("Are you sure you want to leave this process?")) {
            $("#upload-doc-content").modal("hide");
            $("#upload-document-first-modal").modal("hide");
            $.ajax({
                url: "<?= base_url('listOfContainer/deleteUncompletedUpload') ?>",
                type: "POST",
                dataType: "json",
                success: function (response) {
                    location.reload();
                    // if(response.success){
                    //   location.reload();
                    // }else{  
                    //   location.reload();
                    // } 
                }
            });

        }
    }

    function getvalue(val) {
        if (val) {
            $("#ls_number").val(val);
            var test = $("#ls_number" + val).text();
            $('.sd-value').html(test);
            var document_type_id = $("#document_type").val();
            // console.log(document_type_id);
            // return false;
            // call ajax to get the container which is belonging to this LS number 
            $.ajax({
                url: "<?= base_url('listOfContainer/getLSContainerListing') ?>",
                type: "POST",
                dataType: "json",
                data: {val, document_type_id},
                success: function (response) {
                    if (response.success) {
                        $("#containterListingForOtherDocument").html(response.html);
                    } else {
                        showInsertUpdateMessage(response.msg, response);
                        $("#containterListingForOtherDocument").html(response.html);
                    }
                }
            });

        } else {
            showInsertUpdateMessage("Select the value first", false);
        }
    }


    var sku = [];
    function getSku(sku_id) {
        if (sku_id) {
            if (sku.includes(sku_id.toString())) {
                showInsertUpdateMessage("Data is already added in below list", false);
                return false;
            } else {
                sku.push(sku_id);
                console.log("new value");
            }
            console.log(sku);
            if (sku_id) {
                $.ajax({
                    url: "<?= base_url('listOfContainer/getsku') ?>",
                    type: "POST",
                    dataType: "json",
                    data: {sku_id},
                    success: function (response) {
                        if (response.success) {
                            $("#sku_div").append(response.htmlsku);
                            // $("#product_desc").html(response.product_desc);
                            // $("#item_size").html(response.item_size);
                        } else {
                            // alert(response.msg)
                            // $("#").html(response.html);
                        }
                    }
                });
            } else {
                showInsertUpdateMessage("Select sku value", false);
                $("#item_code").html('');
                $("#product_desc").html('');
                $("#item_size").html('');
            }
        }
    }

    function getPersonDetail(supplier_id, container_number) {
        console.log(container_number);
        if (supplier_id) {
            $.ajax({
                url: "<?= base_url('listOfContainer/getPersonDetail') ?>",
                type: "POST",
                dataType: "json",
                data: {supplier_id},
                success: function (response) {
                    if (response.success) {
                        $("#contactpersonname" + container_number).val(response.contactpersonname);
                        $("#contactpersonnumber" + container_number).val(response.contactpersonnumber);
                        // $("#product_desc").html(response.product_desc);
                        // $("#item_size").html(response.item_size);
                    } else {
                        // alert(response.msg)
                        // $("#").html(response.html);
                    }
                }
            });
        } else {
            alert("No Supplier selected");
        }
    }

    function removeSku(sku_id) {
        sku = sku.filter(function (item) {
            return item !== sku_id
        })
        $("#singleSkuDiv" + sku_id).remove();
    }

    function refresh() {
        location.reload();
    }

    /* At time of document upload process show previous screen function */
    function backDocument(showId, hideId) {
        $.each($("input[name='upload_type']:checked"), function () {
            if ($(this).val() == 'upload-invoice') {
                if (showId == 'cnt4') {
                    $('#cnt3').show();
                    $('#' + hideId).hide();
                } else {
                    if (showId == 'cnt1') {
                        $("#upload-doc-content").hide();
                        $("#upload-document-first-modal").show();
                        $(".upload-fri-step-1").show();
                    }
                    $('#' + showId).show();
                    $('#' + hideId).hide();
                }
            } else {
                if (showId == 'cnt1') {
                    $("#upload-doc-content").hide();
                    $("#upload-document-first-modal").show();
                    $(".upload-fri-step-1").show();
                    // $("#uupload-document-first-modal").show();

                } else {
                    $('#' + showId).show();
                    $('#' + hideId).hide();
                }
            }
        });
    }

    /* apply search filter function */
    function getFilters(cnt,f_clear='',container='') {
        //var container_id = $("#sSearch_1").val(); 
        /*container filter check*/
        if(container != 'container'){
            var container_id = new Array();
            $("#multiselect-dropdown-wrapper input[type=checkbox]:checked").each(function () { 
                container_id.push(this.value);
            }); 
        }else{
            var container_id = new Array();
        }
         
        var status = $("#sSearch_2").val();
        var product_id = $("#sSearch_3").val();
        var brand_id = $("#sSearch_4").val();
        var vessel_id = $("#sSearch_5").val();
        var freight_forwarder = $("#sSearch_6").val();
        var fri = $("#sSearch_7").val();
        var ls = $("#sSearch_8").val();
        var bp = '';
        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
            bp = $("#sSearch_9").val();
        <?php } ?>
        var bp_contract = $("#sSearch_10").val();
        var contract = $("#sSearch_11").val();
        var hbl = $("#sSearch_12").val();
        var internal_hbl = $("#sSearch_13").val();
        var internal_fcr = $("#sSearch_14").val();
        var revised_eta = $("#sSearch_15").val();
        var manufacturer = $("#sSearch_16").val();

        $.ajax({
            url: "<?= base_url('listOfContainer/getFilters') ?>",
            type: "POST",
            dataType: "json",
            data: {container_id, status, product_id, brand_id, vessel_id, freight_forwarder, fri, ls, bp, bp_contract, contract, hbl, internal_hbl, internal_fcr, revised_eta, manufacturer,f_clear},
            success: function (response) {
                //check filter id cnt and make filter. cnt have defined bases of id  
                switch (cnt) {

                    case 1: //container

                        $("#contract_div").html(response.contract);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                        $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 2: //status

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 3: //product

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 4: //brand

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 5: //vessel

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 6: //ff

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 7: //fri

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 8: //ls

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;

                    case 9: //bp

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 10: //bp contract

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>

                        break;

                    case 11: //contract  

                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;

                    case 12: //hbl  

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 13: //internal hbl

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 14: //internal fcr

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#revised_div").html(response.revised);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 15:  //revised eta

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#manufacture_div").html(response.manufacture);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;
                    case 16:  //manufacture

                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#revised_div").html(response.revised);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);

                        break;

                    default:
                        
                        $("#contract_div").html(response.contract);
                        $("#container_div").html(response.container);
                        $("#status_div").html(response.status);
                        $("#fri_div").html(response.fri);
                        $("#ls_div").html(response.ls);
                        $("#hbl_div").html(response.hbl);
                        $("#internal_hbl_div").html(response.internal_hbl);
                        $("#internal_fcr_div").html(response.internal_fcr);
                        $("#manufacture_div").html(response.manufacture);
                        $("#revised_div").html(response.revised);
                        $("#product_div").html(response.prodcut);
                        $("#brand_div").html(response.brand);
                        $("#vessel_div").html(response.vessel);
                        $("#ff_div").html(response.ff);
                        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
                            $("#bp_div").html(response.bp);
                        <?php } ?>
                        $("#bp_contract_div").html(response.bp_contract);
                        
                        break;
                }

                $('.basic-single').select2();
                //$("#serachFilterDiv").html(response.serachFilterData);  
                getFilterdata('','',f_clear);

            }
        });

    }
    
    /***** Multiple container update js code start from here ******/    
    /* show status dropdown and update button function */
    function show_update_option(el) { 
        var checkbox = $('[name="container_ids[]"]:checked').length;
        if (checkbox > 0) {
            $("#document_upload_div").hide();
            $("#status_update_div").show();
        } else {
            $("#document_upload_div").show();
            $("#status_update_div").hide();
        }
        
        if ($(el).prop("checked") == true) {
            $(el).closest('.container-single-wrapper').addClass('selected');
        }else{
            $(el).closest('.container-single-wrapper').removeClass('selected');
        } 
    } 

    /** start code of update multiple container status **/
    $(document).ready(function () {
        var vRules = {};
        var vMessages = {};
        //check and save data
        $("#updateContainerStatus").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                if ($('#container_status_update_id').val()) {
                    var act = "<?php echo base_url(); ?>listOfContainer/updateContainerStatus";
                    $("#updateContainerStatus").ajaxSubmit({
                        url: act,
                        type: 'post',
                        dataType: 'json',
                        cache: false,
                        clearForm: false,
                        beforeSubmit: function (arr, $form, options) {
                            $(".btn-primary-mro").hide();
                        },
                        success: function (response) {
                            showInsertUpdateMessage(response.msg, response);
                            if (response.success) {
                                setTimeout(function () {
                                    window.location = "<?= base_url('listOfContainer') ?>";
                                }, 3000);
                            }
                            $(".btn-primary-mro").show();
                        }
                    });
                } else {
                    showInsertUpdateMessage("Please select container status!", false);
                }
            }
        }); 

    });
    /** end code of update multiple container status **/
    
    /** open mutiple container update popup **/
    $('.open-multi-update').click(function() {  
        var values = new Array();
        $.each($("input[name='container_ids[]']:checked"), function() {
          values.push($(this).val()); 
        });  
        
        $("#hold_containerIds").val(values);
        $('#update-information-modal').show();
        $('.overlay-document').show(); 
        
        var container_id = $("#sSearch_1").val();
        var status = $("#sSearch_2").val();
        var product_id = $("#sSearch_3").val();
        var brand_id = $("#sSearch_4").val();
        var vessel_id = $("#sSearch_5").val();
        var ff = $("#sSearch_6").val();
        var fri = $("#sSearch_7").val();
        var ls = $("#sSearch_8").val();
        var bp = '';
        <?php if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') { ?>
            bp = $("#sSearch_9").val();
        <?php } ?>
        var bp_contract = $("#sSearch_10").val();
        var contract = $("#sSearch_11").val();
        var hbl = $("#sSearch_12").val();
        var internal_hbl = $("#sSearch_13").val();
        var internal_fcr = $("#sSearch_14").val();
        var revised_eta = $("#sSearch_15").val();
        var manufacturer = $("#sSearch_16").val();
        
//        if(fri == null && ls == null && internal_hbl == null && internal_fcr == null){
//            $('.document-custom-fields-div').hide(); 
//            $('.document-shipping-info-div').show();
//            $('.update_shipping_information').prop("checked", true);
//            $('.edit_select_doc_type').removeClass('show');
//        }else{
//            $('.document-custom-fields-div').show(); 
//            $('.document-shipping-info-div').hide();
//            $('.document_custom_fields').prop("checked", true); 
//            $('.edit_select_doc_type').addClass('show');
//        }
        
        const filterData = {
            container_id:container_id, status:status, product_id:product_id, brand_id:brand_id,
            vessel_id:vessel_id, ff:ff, fri:fri, ls:ls, bp:bp, bp_contract:bp_contract, contract:contract,
            hbl:hbl, internal_hbl:internal_hbl, internal_fcr:internal_fcr, revised_eta:revised_eta, manufacturer:manufacturer
        }; 
        $("#filters_data").val(JSON.stringify(filterData));
    }); 
    
    /** close update popup **/
    $('.close-multi-update').click(function() { 
        $('#update-information-modal').hide();
        $('.overlay-document').hide(); 
    });

    $('.document_custom_fields').click(function() {
      if($(this).is(':checked')) { 
        $('.edit_select_doc_type').addClass('show');
       }
    });
    $('.update_shipping_information').click(function() {
      if($(this).is(':checked')) { 
        $('.edit_select_doc_type').removeClass('show');
       }
    }); 
    
    /*document type check at time of update custom fields*/
    $( "#updateMultipleContainer" ).submit(function() {
        var val = $('[name="update_shipping_information"]:checked').val();
        if(val == 'custom' && $('#update_document_type').val() == ''){ 
            showInsertUpdateMessage("Please select document type!", false);
            return false;  
        }
    });  
    
    function filterCall(id){
        /*container filter check*/
        if(id=="sSearch_1"){
            $('.checkmark-checkbox').removeAttr('checked');
            getFilters(0,true,'container'); 
        }else{
            $('#'+id).val(''); 
            $('#'+id).select2({
                searchInputPlaceholder: 'Search'
            }); 
            getFilters(0,true); 
        } 
    }  
    
    function allcheck(){ 
        $(".chk" ).prop( "checked", true );
        $('.container-single-wrapper').addClass('selected'); 
        var checkbox = $('[name="container_ids[]"]:checked').length;
        if (checkbox > 0) {
            $("#document_upload_div").hide();
            $("#status_update_div").show();
        } else {
            $("#document_upload_div").show();
            $("#status_update_div").hide();
        }  
    } 
    function alluncheck(){ 
        $(".chk" ).prop( "checked", false );
        $('.container-single-wrapper').removeClass('selected'); 
        var checkbox = $('[name="container_ids[]"]:checked').length;
        if (checkbox > 0) {
            $("#document_upload_div").hide();
            $("#status_update_div").show();
        } else {
            $("#document_upload_div").show();
            $("#status_update_div").hide();
        }  
    } 
    /***** Multiple container update js code end here ******/

</script>