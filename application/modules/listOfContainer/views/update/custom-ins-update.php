  <?php
    if ($getSelectedCustomField) { ?>
    <?php if (!empty($containersSKUData)) { ?>
          <div class="bp-list-wrapper" style="display: none">
              <div class="table-responsive">
                <table class="table table-striped document-sku-table no-footer" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width:30%">Container</th>
                            <th style="width:30%">SKU</th>
                            <th style="width:40%">Quantity</th> 
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            foreach ($containersSKUData as $containerId => $containerSkuData) {
                                $containerNumberArr = array(); 
                                foreach ($containerSkuData as $containerSkuId => $value) { 
                                    // print_r($value); 
                                    ?>
                                <tr>
                                    <td>
                                        <?php
                                            if (!in_array($value['container_number'], $containerNumberArr)) {
                                                echo $value['container_number']; 
                                                $containerid = $value['container_number']; 
                                            }
                                            ?>
                                    </td>
                                    <td><?= $value['sku_number'] ?>
                                        <input type="hidden" name="all_container_id[<?= $value['container_id']?>]" id="all_container_id<?= $containerSkuId ?>" value="<?= (!empty($value['container_id'])) ? $value['container_id'] : '0' ?>">
                                        <input type="hidden" name="container_sku_id[<?= $value['container_id']?>][<?= $containerSkuId ?>]" id="container_sku_id<?= $containerSkuId ?>" value="<?= (!empty($value['container_sku_id'])) ? $value['container_sku_id'] : '0' ?>">
                                        <input type="hidden" name="manufacturer[<?= $value['container_id']?>][<?= $containerSkuId ?>]" id="manufacturer<?= $containerSkuId ?>" value="<?= (!empty($value['manufacturer_id'])) ? $value['manufacturer_id'] : '0' ?>">
                                    </td>
                                    <td>
                                        <input type="number" name="container_sku_qty[<?= $value['container_id']?>][<?= $containerSkuId ?>]" id="container_sku_qty<?= $containerSkuId ?>" value="<?= (!empty($value['quantity'])) ? $value['quantity'] : '0' ?>" class="input-form-mro "   placeholder="Enter quantity">
                                    </td> 
                                </tr>
                            <?php
                                    $containerNumberArr[] = $value['container_number']; 
                                } ?> 
                        <?php

                            } ?>
                    </tbody>
                </table>
              </div>
          </div>
      <?php } ?>

      <?php 
        $cnthide = 0;
        foreach ($getSelectedCustomField as $key => $value) { 
            if($cnthide==0){
            ?> 
                <input type="hidden" name="document_type_id" id="document_type_id" value="<?= $value['document_type_id'] ?>">
                <input type="hidden" name="sub_document_type_id" id="sub_document_type_id" value="<?= $value['sub_document_type_id'] ?>">
                <input type="hidden" name="uploaded_document_number" id="uploaded_document_number" value="<?= $value['uploaded_document_number'] ?>"> 
            <?php 
            }
          if ($value['custom_field_type'] == 'date') { ?>
            <input type="hidden" name="custom_field_structure_id[]" id="custom_field_structure_id" value="<?= $value['custom_field_structure_id'] ?>">
              <div class="form-group ">
                  <label for=""><?= $value['custom_field_title'] ?></label>
                  <!-- <input type="date" name="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>"  id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter date" class="input-form-mro"> -->
                  <input type="text"  name="<?= $value['custom_field_structure_id'] ?>"  id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" value="<?= !empty($value['custom_field_structure_value'])?date('d-m-Y', strtotime($value['custom_field_structure_value'])):''?>" placeholder="Enter date"   class="form-control input-form-mro datepicker date-form-field ">
              </div>
              <?php  } elseif ($value['custom_field_type'] == 'text') {
                if($value['custom_field_slug'] == 'hbl_number'){ ?>
                    <div class="form-group">
                        <label for=""><?= $value['custom_field_title'] ?></label>
                        <input type="text" value="<?= (!empty($get_hbl_number[0]['hbl_number'])? $get_hbl_number[0]['hbl_number'] :"")?>" name="hbl_number"  id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" placeholder="Enter data" class="input-form-mro " >
                    </div>
                <?php } else { ?>
                    <input type="hidden" name="custom_field_structure_id[]" id="custom_field_structure_id" value="<?= $value['custom_field_structure_id'] ?>">
                    <div class="form-group">
                        <label for=""><?= $value['custom_field_title'] ?></label>
                        <input type="text" name="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>"  id="<?= str_replace(' ', '_', trim($value['custom_field_structure_id'])); ?>" value="<?= $value['custom_field_structure_value']?>" placeholder="Enter data" class="input-form-mro " >
                    </div>
                <?php } ?>
          <?php } elseif ($value['custom_field_type'] == 'dropdown') { ?>

              <div class="form-group">
                  <div class="form-group">
                      <label for=""><?= $value['custom_field_title'] ?></label>
                      <div class="multiselect-dropdown-wrapper">
                          <div class="md-value">
                              Select value
                          </div>
                          <div class="md-list-wrapper">
                              <input type="text" placeholder="Search" class="md-search">
                              <div class="md-list-items">
                                  <?php if ($ListOfContainerData) {
                                        foreach ($ListOfContainerData as $key => $value) { ?>
                                          <div class="mdli-single ud-list-single">
                                              <label class="container-checkbox"> <?= $value['containerData']['container_number'] ?>
                                                  <input type="checkbox" id="<?= $value['custom_field_structure_id']; ?>" value="<?= $value['containerData']['container_id'] ?>" name="<?= $value['custom_field_structure_id'] ?>">
                                                  <span class="checkmark-checkbox"></span>
                                              </label>
                                          </div>
                                  <?php
                                        }
                                    }
                                    ?>
                              </div>
                              <div class="md-cta">
                                  <!-- <a href="#/" class="btn-primary-mro md-done">Done</a>
                            <a href="#/" class="btn-secondary-mro md-clear">Clear All</a> -->
                              </div>
                          </div>
                      </div>
                  </div>
              </div>

          <?php } elseif ($value['custom_field_title'] == 'number') { ?>

          <?php }  
        $cnthide++;
        }
    } ?>
 