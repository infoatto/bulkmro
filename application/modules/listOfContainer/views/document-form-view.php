<?php
if($formView){ ?>
    <div id="tabs">
        <ul>
            <?php  foreach ($formView as $key => $value) { ?>
                <li data-tab="tabs-<?=$key?>"><span><?= (!empty($value['sub_document_name'])?$value['sub_document_name']:$value['document_name'])?></a></li> 
            <?php  } ?>
        </ul>

        <?php  foreach ($formView as $key => $value) { 
            $docPath = getDocumentFolder($value['document_type_id']);
            $fileNameArr = explode('.', $value['uploaded_document_file']);
            $fileName = $fileNameArr[0].'.'.strtolower($fileNameArr[1]);
            $path = "container_document/".$docPath.'/'.$fileName;
            $url = get_s3_upload_file($path);
            ?>
            <div class="tabs-<?=$key?> tabs-modal">
                <iframe src="<?= $url?>#zoom=FitH&page=anchor" style="width:100%;height:700px;"></iframe>
            </div> 
        <?php  } ?>
    </div>
<?php }else{ ?>
    <div>
        <p><h3>No Preview Available</h3></p>
    </div>
<?php } ?>

<script>

    // $("#tabs").tabs().hide();
</script>