<div id="serchfilter" class="filter-sec-wrapper">
    
     <div class="form-group  mt20"> 
        <select name="sSearch_11" id="sSearch_11" class="searchInput basic-single select-large" onchange="getFilters('contract_div');"> 
            <option value="" disabled selected hidden>Master Contract</option>
            <?php 
            $contract = '';
            foreach ($orderData as $value) { ?>
                <option value="<?= $value['order_id'] ?>" <?php echo ($contract==$value['order_id']) ? 'selected' : ''?> ><?= $value['contract_number'] ?></option>
            <?php } ?>
        </select>
    </div>
    
   <div class="form-group mt20"> 
        <select name="sSearch_1" id="sSearch_1" class="searchInput basic-single" style="width:100px" onchange="getFilters('contract_div');"> 
            <option value="" disabled selected hidden>Container</option>
            <?php foreach ($containerData as $value) { ?>
            <option value="<?=$value['container_id']?>"><?=$value['container_number']?></option>
            <?php } ?>
        </select>
    </div> 

    <div class="form-group  mt20">
        <select name="sSearch_2" id="sSearch_2" class="searchInput basic-single select-large" onchange="getFilters('contract_div');"> 
            <option value="" disabled selected hidden>Status</option>
            <?php foreach ($containerStatus as $value) { ?>
            <option value="<?=$value['container_status_id']?>"><?=$value['container_status_name']?></option>
            <?php } ?>
        </select>
    </div> 

    <div class="form-group  mt20">
        <select name="sSearch_7" id="sSearch_7" class="searchInput basic-single" style="width:90px" onchange="getFilters('contract_div');"> 
            <option value="" disabled selected hidden>FRI</option>
            <?php foreach ($FRI as $value) { ?>
            <option value="<?=$value['uploaded_document_number']?>"><?=$value['uploaded_document_number']?></option>
            <?php } ?>
        </select>
    </div> 

    <div class="form-group  mt20">
        <select name="sSearch_8" id="sSearch_8" class="searchInput basic-single" style="width:90px" onchange="getFilters('contract_div');"> 
            <option value="" disabled selected hidden>LS</option>
            <?php foreach ($LS as $value) { ?>
            <option value="<?=$value['uploaded_document_number']?>"><?=$value['uploaded_document_number']?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group  mt20">
        <select name="sSearch_12" id="sSearch_12" class="searchInput basic-single" style="width:110px" onchange="getFilters('contract_div');"> 
            <option value="" disabled selected hidden>HBL</option>
            <?php foreach ($HBL as $value) { ?>
            <option value="<?=$value['uploaded_document_number']?>"><?=$value['uploaded_document_number']?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group  mt20">
        <select name="sSearch_13" id="sSearch_13" class="searchInput basic-single" style="width:110px"> 
            <option value="" disabled selected hidden>Internal HBL</option>
            <?php foreach ($internalHBL as $value) { ?>
            <option value="<?=$value['uploaded_document_number']?>"><?=$value['uploaded_document_number']?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group  mt20">
        <select name="sSearch_14" id="sSearch_14" class="searchInput basic-single" style="width:110px"> 
            <option value="" disabled selected hidden>Internal FCR</option>
            <?php foreach ($internalFCR as $value) { ?>
            <option value="<?=$value['uploaded_document_number']?>"><?=$value['uploaded_document_number']?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group  mt20">
        <select name="sSearch_15" id="sSearch_15" class="searchInput basic-single" style="width:110px"> 
            <option value="" disabled selected hidden>Revised ETA</option>
            <?php foreach ($revisedETA as $value) { ?>
            <option value="<?=date('Y-m-d', strtotime($value['revised_eta']))?>"><?=date('d-M-Y', strtotime($value['revised_eta']))?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group  mt20">
        <select name="sSearch_16" id="sSearch_16" class="searchInput basic-single" style="width:120px"> 
            <option value="" disabled selected hidden>Manufacturer</option>
            <?php foreach ($manufacture as $value) { ?>
            <option value="<?=$value['business_partner_id']?>"><?=$value['alias']?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group  mt20">
        <select name="sSearch_3" id="sSearch_3" class="searchInput basic-single select-large"> 
            <option value="" disabled selected hidden>Product</option>
            <?php foreach ($productData as $value) { ?>
            <option value="<?=$value['sku_id']?>"><?=$value['product_name']?></option>
            <?php } ?>
        </select>
    </div> 

    <div class="form-group  mt20">
        <select name="sSearch_4" id="sSearch_4" class="searchInput basic-single select-medium"> 
            <option value="" disabled selected hidden>Brand</option>
            <?php foreach ($brandData as $value) { ?>
            <option value="<?=$value['brand_id']?>"><?=$value['brand_name']?></option>
            <?php } ?>
        </select>
    </div> 

    <div class="form-group  mt20">
        <select name="sSearch_5" id="sSearch_5" class="searchInput basic-single select-large"> 
            <option value="" disabled selected hidden>Vessel </option>
            <?php foreach ($vesselData as $value) { ?>
            <option value="<?=$value['vessel_name']?>"><?=$value['vessel_name']?></option>
            <?php } ?>
        </select>
    </div> 

    <div class="form-group  mt20">
        <select name="sSearch_6" id="sSearch_6" class="searchInput basic-single select-large"> 
            <option value="" disabled selected hidden>Freight Forwarder</option>
            <?php foreach ($freightForwarder as $value) { ?>
            <option value="<?=$value['business_partner_id']?>"><?=$value['alias']?></option>
            <?php } ?>
        </select>
    </div> 
    <?php if(!empty($bpData)){ ?>
    <div class="form-group  mt20">
        <select name="sSearch_9" id="sSearch_9" class="searchInput basic-single select-large"> 
            <option value="" disabled selected hidden>Business Partner</option>
            <?php foreach ($bpData as $value) { ?>
            <option value="<?=$value['business_partner_id']?>"><?=$value['alias']?></option>
            <?php } ?>
        </select>
    </div> 
    <?php } ?>
    <div class="form-group  mt20">
        <select name="sSearch_10" id="sSearch_10" class="searchInput basic-single" style="width:130px"> 
            <option value="" disabled selected hidden>BP Contract</option>
            <?php foreach ($bpContractData as $value) { ?>
            <option value="<?=$value['bp_order_id']?>"><?=$value['contract_number']?></option>
            <?php } ?>
        </select>
    </div> 
    <div class="form-group  mt20">
        <select name="sSearch_17" id="sSearch_17" class="searchInput basic-single" style="width:110px"> 
            <option value="ASC">Ascending</option>
            <option value="DESC" selected="selected">Descending</option> 
        </select>
    </div> 

    <div class="form-group  mt20">
        <button type="reset"  onclick="refresh()"  class="select-small btn-primary-mro">
            Clear
        </button>

    </div>  
</div>
<script>
    $('.basic-single').select2();
</script>