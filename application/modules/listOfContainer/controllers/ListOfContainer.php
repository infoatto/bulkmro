<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class ListOfContainer extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('ListOfContainermodel', 'listOfContainermodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
        if (!$this->privilegeduser->hasPrivilege("ContainersDetailsList")) {
            redirect('dashboard');
        }
    }

    public function index() {
        $result = array();
        $result['containerData'] = array();
        $result['containerStatus'] = array();
        $result['productData'] = array();
        $result['brandData'] = array();
        $result['vesselData'] = array();
        $result['LS'] = array();
        $result['FRI'] = array();
        $result['ListOfContainerData'] = array();
        $result['bpData'] = array();
        $result['totalRecords'] = 0;
        $result['freightForwarder'] = array();
        

        $result['order_id'] = '';
        $result['bp_order_id'] = '';
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $result['order_id'] = $url_prams['order_id'];
            if (isset($url_prams['bp_order_id'])) {
                $result['bp_order_id'] = $url_prams['bp_order_id'];
            }
        }

        //check for when come from master contract and dashboard then not run below code and data will come by getfilterdata function
        if (empty($result['order_id']) && empty($result['bp_order_id'])) {
            $condition = " status = 'Active' ";
            $result['documentType'] = $this->common->getData('tbl_document_type', '*', $condition, 'document_type_squence', 'asc');

            //define array (document type ids) in case of container last status GOODS RECEIVED IN PORT 
            $jobNotCompleted = array(1, 2, 3, 4, 5, 10);
            $ListOfContainerData = $this->listOfContainermodel->getContainers();
            if ($ListOfContainerData['query_result']) {
                foreach ($ListOfContainerData['query_result'] as $key => $value) {
                    $condition = " status = 'Active' ";
                    $result['ListOfContainerData'][$key]['containerData'] = $value;
                    $result['ListOfContainerData'][$key]['documentType'] = $this->common->getData('tbl_document_type', '*', $condition, 'document_type_squence', 'asc');
                    //container status   
                    //$containerStatusData =  $this->listOfContainermodel->getContainerStatus($value['container_id']);                     
                    $condition = "container_status_id=" . $value['last_container_status_id'] . " ";
                    $containerStatusData = $this->common->getData('tbl_container_status', 'container_status_name', $condition);
                    if (!empty($containerStatusData)) {
                        $result['ListOfContainerData'][$key]['containerStatus'] = $containerStatusData[0]['container_status_name'];
                    }

                    //last update by and date
                    if (!empty($value['last_status_by'])) {
                        $updateByData = $this->common->getData("tbl_users", "firstname,lastname", array("user_id" => $value['last_status_by']));
                        if (!empty($updateByData)) {
                            $result['ListOfContainerData'][$key]['updatedBy'] = $updateByData[0]['firstname'] . ' ' . substr($updateByData[0]['lastname'], 0, 1);
                            $result['ListOfContainerData'][$key]['updatedOn'] = date('d-M-y', strtotime($value['last_status_date']));
                        }
                    } else {
                        $result['ListOfContainerData'][$key]['updatedBy'] = 'System';
                        $result['ListOfContainerData'][$key]['updatedOn'] = date('d-M-y', strtotime($value['last_status_date']));
                    } 
                                  
                    //code for document type color 
                    $DocumentStatusData = $this->listOfContainermodel->getDocumentInContainer($value['container_id']);
                    // $DocumentStatusData = $this->listOfContainermodel->getDocumentInContainer('11');  

                    $uploaded_document_type_id = array();
                    $uploaded_document_seqence_id = array();
                    // echo "<pre>";
                    // print_r($DocumentStatusData);
                    // exit;
                    if ($DocumentStatusData) {
                        foreach ($DocumentStatusData as $key1 => $value1) {
                            $uploaded_document_type_id[] = $value1['document_type_id'];
                            $uploaded_document_seqence_id[] = $value1['document_type_squence'];
                            // echo "container_id".$value['container_id'];
                            // print_r($uploaded_document_seqence_id);
                            $result['DocumentStatusData'][$value['container_id']][$value1['document_type_id']] = array("status" => $value1['status']);

                            // to get the uploaded document for each container
                            // for ls n fri 
                            $document_type_ids = array(2, 3);
                            if (in_array($value1['document_type_id'], $document_type_ids)) {
                                $condition = " 1=1 AND duf.container_id = " . $value['container_id'] . " AND document_type_id = " . $value1['document_type_id'] . " ";
                                // $condition = " 1=1 AND duf.container_id = 11 AND document_type_id = ".$value1['document_type_id']." ";

                                $rs = $this->common->Fetch('tbl_document_uploaded_files as duf', 'duf.*,group_concat(duf.uploaded_document_number) as uploaded_document_number', $condition, '', 'document_type_id');

                                $result['getuploaded_Document'][$value['container_id']][$value1['document_type_id']] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                            } else {
                                $condition = " 1=1 AND dluf.container_id = " . $value['container_id'] . " AND document_type_id = " . $value1['document_type_id'] . "  ";

                                $rs = $this->common->Fetch('tbl_document_ls_uploaded_files as dluf', 'dluf.*,group_concat(dluf.uploaded_document_number) as uploaded_document_number', $condition, '', 'document_type_id');

                                $result['getuploaded_Document'][$value['container_id']][$value1['document_type_id']] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                                // $result['getuploaded_Document'][$value['container_id']]  = $this->common->getData();
                            }
                        }
                    } 
                    // print_r( $result['getuploaded_Document']);
                    // exit; 

                    $less_doc_type_id = array();
                    if ($DocumentStatusData) {
                        $max_sequence = max(array_unique($uploaded_document_seqence_id));
                        // echo "max ".$max_sequence; 
                        // get all document which is less the max_squence 
                        $condition = "status = 'Active' AND  document_type_squence < " . $max_sequence . " ";
                        $get_docTypeid_lessSequence = $this->common->getData("tbl_document_type", 'document_type_id', $condition);

                        // print_r($get_docTypeid_lessSequence);
                        if ($get_docTypeid_lessSequence) {
                            foreach ($get_docTypeid_lessSequence as $seqkey => $seqval) {
                                $less_doc_type_id[] = $seqval['document_type_id'];
                            }
                        }
                        //check container last status is GOODS RECEIVED IN PORT(status id =7)
                        if ($value['last_container_status_id'] == 7) {
                            $result['containers'][$value['container_id']]['job_not_completed_document_type_id'] = array_diff($jobNotCompleted, $uploaded_document_type_id);
                        } else {
                            $result['containers'][$value['container_id']]['job_not_completed_document_type_id'] = array_diff($less_doc_type_id, $uploaded_document_type_id);
                        }
                    } else {
                        if ($value['last_container_status_id'] == 7) {
                            $result['containers'][$value['container_id']]['job_not_completed_document_type_id'] = $jobNotCompleted;
                        }
                    }
                } // container loop end here 
            }
            
            $result['totalRecords'] = $ListOfContainerData['totalRecords'];
        }
        //echo '<pre>'; print_r($result['containers']);die;


        $condition = "status = 'Active' ";
        $result['business_partner'] = $this->common->getData("tbl_business_partner", "*", $condition);

        //dropdown filter code start from here       
        //filter code when open container bible
        if (empty($result['order_id'])) {
            //condition for if not BP Login user
            if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') {
                //container filter data
                $containerData = $this->common->getData("tbl_container", "container_id,container_number", "");
                if (!empty($containerData)) {
                    $result['containerData'] = $containerData;
                }

                //container status filter data 
                $qry = "select cs.container_status_id,cs.container_status_name "
                        . "from tbl_container as c "
                        . "join tbl_container_status as cs on (cs.container_status_id = c.last_container_status_id) "
                        . "group by cs.container_status_name order by cs.status_squence Asc";
                $query = $this->db->query($qry);
                $containerStatus = $query->result_array(); 
                //$containerStatus = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", "1=1", "status_squence", "Asc");
                if (!empty($containerStatus)) {
                    $result['containerStatus'] = $containerStatus;
                }

                $condition = "1=1";
                $main_table = array("tbl_sku as sku", array("sku.*"));
                $join_tables = array(
                    array("", "tbl_container_sku as csku", "csku.sku_id = sku.sku_id", array()),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'sku.sku_id');
                $productData = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                //product filter data
                if (!empty($productData)) {
                    $result['productData'] = $productData;
                }

                // for sku id to get brand 
                // $sku_ids = '';
                $sku_ids = array(0);
                foreach ($productData as $key => $value) {
                    $sku_ids[] = $value['sku_id'];
                }

                $sku_ids = implode(',', $sku_ids);
                $condition = "1=1 AND sku_id IN ($sku_ids) ";
                $main_table = array("tbl_brand as b", array("b.*"));
                $join_tables = array(
                    array("", "tbl_sku as sku", "sku.brand_id = b.brand_id", array()),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'b.brand_id');
                $brandData = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                //brand filter data
                if (!empty($brandData)) {
                    $result['brandData'] = $brandData;
                }

                $condition = "status = 'Active'  AND  vessel_name  Is Not null  AND vessel_name !='' ";
                $vesselData = $this->common->getData("tbl_container", "distinct(vessel_name)", $condition);
                //vessel filter data
                if (!empty($vesselData)) {
                    $result['vesselData'] = $vesselData;
                }

                $condition = "business_category = 'Freight Forwarder' ";
                $main_table = array("tbl_business_partner as bp", array("business_partner_id,alias"));
                $join_tables = array(
                    array("", "tbl_container as c", "c.freight_forwarder_id = bp.business_partner_id", array()),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'freight_forwarder_id');
                $freightForwarder = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                //freight forwarder filter data
                if (!empty($freightForwarder)) {
                    $result['freightForwarder'] = $freightForwarder;
                }

                $condition = " business_category in ('Government','Corporate','Distributor','Manufacturer') AND status = 'Active' ";
                $bpData = $this->common->getData("tbl_business_partner", "business_partner_id,alias", $condition);
                //BP filter data
                if (!empty($bpData)) {
                    $result['bpData'] = $bpData;
                }

                $condition = " status = 'Active' ";
                $bpContractData = $this->common->getData("tbl_bp_order", "bp_order_id,contract_number", $condition);
                //BP contract filter data 
                if (!empty($bpContractData)) {
                    $result['bpContractData'] = $bpContractData;
                }

                $condition = " status = 'Active' ";
                $orderData = $this->common->getData("tbl_order", "order_id,contract_number", $condition);
                //master contract filter data
                if (!empty($orderData)) {
                    $result['orderData'] = $orderData;
                }


                $FRILSData = $this->listOfContainermodel->getFRIandLS();
                //LS and FRI filter data
                if (!empty($FRILSData)) {
                    foreach ($FRILSData as $value) {
                        if ($value['document_type_id'] == 2) {
                            $result['LS'][] = $value;
                        } else {
                            $result['FRI'][] = $value;
                        }
                    }
                } 
                
                //HBL filter data
                $condition = "cdd.hbl_number IS NOT NULL  AND dt.document_type_name = 'HBL' ";
                $main_table = array("tbl_custom_dynamic_data as cdd", array("cdd.hbl_number as uploaded_document_number"));
                $join_tables = array(
                    array("", "tbl_document_type as dt", "dt.document_type_id = cdd.document_type_id", array()),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'cdd.hbl_number');
                $hblData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                $result['HBL'] = array();
                if (!empty($hblData)) {
                    foreach ($hblData as $value) {
                        $result['HBL'][] = $value;
                    }
                } 
                
                //Internal HBL filter data 
                $condition = " uploaded_document_number like '%IHBL%' ";
                $rs = $this->common->Fetch("tbl_document_ls_uploaded_files","uploaded_document_number" ,$condition, "", 'uploaded_document_number');
                $internalHBLData = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                $result['internalHBL'] = array();
                if (!empty($internalHBLData)) {
                    foreach ($internalHBLData as $value) {
                        $result['internalHBL'][] = $value;
                    }
                }
                
                //Internal FCR filter data 
                $condition = " uploaded_document_number like '%IFCR%' ";
                $rs = $this->common->Fetch("tbl_document_ls_uploaded_files","uploaded_document_number" ,$condition, "", 'uploaded_document_number');
                $internalFCRData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
                $result['internalFCR'] = array();
                if (!empty($internalFCRData)) {
                    foreach ($internalFCRData as $value) {
                        $result['internalFCR'][] = $value;
                    }
                }
                
                //Rivesed ETA filter data 
                $condition = " revised_eta IS NOT NULL ";
                $rs = $this->common->Fetch("tbl_container","revised_eta" ,$condition, "", "revised_eta","revised_eta ASC");
                $revisedETAData = $this->common->MySqlFetchRow($rs, "array"); // fetch result   
                $result['revisedETA'] = array();
                if (!empty($revisedETAData)) {
                    foreach ($revisedETAData as $value) {
                        $result['revisedETA'][] = $value;
                    }
                }
                
                //Manufacture filter data 
                $main_table = array("tbl_sku as sku", array());
                $join_tables = array(
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = sku.manufacturer_id", array("bp.business_partner_id,bp.alias")),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, "", "", 'bp.alias');
                $manufactureData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                $result['manufacture'] = array();
                if (!empty($manufactureData)) {
                    foreach ($manufactureData as $value) {
                        $result['manufacture'][] = $value;
                    }
                }  
                
            } else {

                //condition for Customer and Supplier roles user
                if ($_SESSION["mro_session"]['user_role'] == 'Customer') {
                    $condition = " bpc.business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
                } else {
                    $condition = " bps.business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
                }
                $main_table = array("tbl_container as c", array('c.container_id,c.container_number,c.last_container_status_id'));
                $join_tables = array(
                    array("left", "tbl_order as o", "o.order_id = c.order_id", array()),
                    array("left", "tbl_bp_order as bpoc", "bpoc.bp_order_id = o.customer_contract_id", array()),
                    array("left", "tbl_bp_order as bpos", "bpos.bp_order_id = o.supplier_contract_id", array()),
                    array("left", "tbl_business_partner as bpc", "bpc.business_partner_id = bpoc.business_partner_id", array()),
                    array("left", "tbl_business_partner as bps", "bps.business_partner_id = bpos.business_partner_id", array()),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
                $containerData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
                $containerIdsStr = '0';
                $containerIdsArr = array();
                $containerStatusIdArr = array();
                //container filter data
                if (!empty($containerData)) {
                    $result['containerData'] = $containerData;
                    //hold container ids
                    foreach ($containerData as $value) {
                        if (!empty($containerIdsStr)) {
                            $containerIdsStr = $containerIdsStr . ',' . $value['container_id'];
                        } else {
                            $containerIdsStr = $value['container_id'];
                        }
                        $containerIdsArr[] = $value['container_id'];
                        //hold container last status ids
                        $containerStatusIdArr[] = $value['last_container_status_id'];
                    }
                }

                $condition = " c.status = 'Active'  AND  c.vessel_name  Is Not null  AND c.vessel_name !='' AND c.container_id IN (" . $containerIdsStr . ") ";
                $vesselData = $this->common->getData("tbl_container as c", "distinct(c.vessel_name)", $condition);
                if (!empty($vesselData)) {
                    $result['vesselData'] = $vesselData;
                }

                $condition = " bp.business_category = 'Freight Forwarder' AND c.container_id IN (" . $containerIdsStr . ") ";
                $main_table = array("tbl_business_partner as bp", array("bp.business_partner_id,bp.alias"));
                $join_tables = array(
                    array("", "tbl_container as c", "c.freight_forwarder_id = bp.business_partner_id", array()),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'c.freight_forwarder_id');
                $freightForwarder = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                //freight Forwarder filter data
                if (!empty($freightForwarder)) {
                    $result['freightForwarder'] = $freightForwarder;
                }

                //hold container status ids
                //            $containerStatusIdStr = '0';
                //            if(!empty($containerIdsArr)){
                //                foreach ($containerIdsArr as $value) {
                //                    $containerStatusData =  $this->listOfContainermodel->getContainerStatus($value); 
                //                    if(!empty($containerStatusData)){
                //                        if(!empty($containerStatusIdStr)){
                //                            $containerStatusIdStr = $containerStatusIdStr.','.$containerStatusData[0]['container_status_id'];
                //                        }else{
                //                            $containerStatusIdStr = $containerStatusData[0]['container_status_id'];
                //                        }
                //                    } 
                //                }
                //            }
                $containerStatusIdStr = 0;
                if (!empty($containerStatusIdArr)) {
                    $containerStatusIdStr = implode(",", $containerStatusIdArr);
                }

                $condition = " container_status_id IN (" . $containerStatusIdStr . ") ";
                $containerStatus = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", $condition, "status_squence", "Asc");
                //container status filter data
                if (!empty($containerStatus)) {
                    $result['containerStatus'] = $containerStatus;
                }

                $condition = "csku.container_id IN (" . $containerIdsStr . ")";
                $main_table = array("tbl_sku as sku", array("sku.*"));
                $join_tables = array(
                    array("", "tbl_container_sku as csku", "csku.sku_id = sku.sku_id", array()),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'sku.sku_id');
                $productData = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                //product filter data
                if (!empty($productData)) {
                    $result['productData'] = $productData;
                }

                // for sku id to get brand 
                // $sku_ids = '';
                $sku_ids = array(0);
                foreach ($productData as $key => $value) {
                    $sku_ids[] = $value['sku_id'];
                }

                $sku_ids = implode(',', $sku_ids);

                $condition = "1=1 AND sku_id IN ($sku_ids) ";
                $main_table = array("tbl_brand as b", array("b.*"));
                $join_tables = array(
                    array("", "tbl_sku as sku", "sku.brand_id = b.brand_id", array()),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'b.brand_id');
                $brandData = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                //brand filter data
                if (!empty($brandData)) {
                    $result['brandData'] = $brandData;
                }


                $condition = " bpo.status = 'Active' AND bp.business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
                $main_table = array("tbl_bp_order as bpo", array('bpo.bp_order_id,bpo.contract_number'));
                $join_tables = array(
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array()),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
                //BP contract filter data
                $bpContractData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                if (!empty($bpContractData)) {
                    $result['bpContractData'] = $bpContractData;
                }

                $main_table = array("tbl_order as o", array('o.order_id,o.contract_number'));
                //condition for Customer and Supplier roles user
                if ($_SESSION["mro_session"]['user_role'] == 'Customer') {
                    $condition = " o.status = 'Active' AND bp.business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
                    $join_tables = array(
                        array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id", array()),
                        array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array()),
                    );
                } else {
                    $condition = " o.status = 'Active' AND bp.business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
                    $join_tables = array(
                        array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array()),
                        array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array()),
                    );
                }
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
                $orderData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
                //Master contract filter data
                if (!empty($orderData)) {
                    $result['orderData'] = $orderData;
                }


                $FRILSData = $this->listOfContainermodel->getFRIandLS();
                //LS and FRI filter data
                if (!empty($FRILSData)) {
                    foreach ($FRILSData as $value) {
                        if (in_array($value['container_id'], $containerIdsArr)) {
                            if ($value['document_type_id'] == 2) {
                                $result['LS'][] = $value;
                            } else {
                                $result['FRI'][] = $value;
                            }
                        }
                    }
                }
                
                $containerIdsStr = "0";
                if(!empty($containerIdsArr)){
                    $containerIdsStr = implode(",", $containerIdsArr);
                }
                
                //HBL filter data 
                $condition = "cdd.hbl_number IS NOT NULL  AND dt.document_type_name = 'HBL' AND  cdd.container_id IN (".$containerIdsStr.") ";
                $main_table = array("tbl_custom_dynamic_data as cdd", array("cdd.hbl_number as uploaded_document_number"));
                $join_tables = array(
                    array("", "tbl_document_type as dt", "dt.document_type_id = cdd.document_type_id", array()),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'cdd.hbl_number');
                $hblData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                $result['HBL'] = array();
                if (!empty($hblData)) {
                    foreach ($hblData as $value) {
                        $result['HBL'][] = $value;
                    }
                }
                
                
                //Internal HBL filter data 
                $condition = " uploaded_document_number like '%IHBL%' AND container_id IN (".$containerIdsStr.") ";
                $rs = $this->common->Fetch("tbl_document_ls_uploaded_files","uploaded_document_number" ,$condition, "", 'uploaded_document_number');
                $internalHBLData = $this->common->MySqlFetchRow($rs, "array"); // fetch result   
                $result['internalHBL'] = array();
                if (!empty($internalHBLData)) {
                    foreach ($internalHBLData as $value) {
                        $result['internalHBL'][] = $value;
                    }
                }
                
                //Internal FCR filter data 
                $condition = " uploaded_document_number like '%IFCR%' AND container_id IN (".$containerIdsStr.") ";
                $rs = $this->common->Fetch("tbl_document_ls_uploaded_files","uploaded_document_number" ,$condition, "", 'uploaded_document_number');
                $internalFCRData = $this->common->MySqlFetchRow($rs, "array"); // fetch result   
                $result['internalFCR'] = array();
                if (!empty($internalFCRData)) {
                    foreach ($internalFCRData as $value) {
                        $result['internalFCR'][] = $value;
                    }
                }
                
                //Rivesed ETA filter data 
                $condition = " revised_eta IS NOT NULL  AND container_id IN (".$containerIdsStr.") ";
                $rs = $this->common->Fetch("tbl_container","revised_eta" ,$condition, "", "revised_eta","revised_eta ASC");
                $revisedETAData = $this->common->MySqlFetchRow($rs, "array"); // fetch result   
                $result['revisedETA'] = array();
                if (!empty($revisedETAData)) {
                    foreach ($revisedETAData as $value) {
                        $result['revisedETA'][] = $value;
                    }
                }
                
                //Manufacture filter data  
                $condition = " c_sku.container_id IN (" . $containerIdsStr . ") ";
                $main_table = array("tbl_container_sku as c_sku", array()); 
                $join_tables = array(
                    array("", "tbl_sku as sku", "sku.sku_id = c_sku.sku_id", array()),
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = sku.manufacturer_id", array("bp.business_partner_id,bp.alias")),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'bp.alias');
                $manufactureData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                $result['manufacture'] = array();
                if (!empty($manufactureData)) {
                    foreach ($manufactureData as $value) {
                        $result['manufacture'][] = $value;
                    }
                } 
                
                
            }
        } else {

            //filter code when come from master contract and dashboard
            $condition = "1=1 ";
            if (!empty($result['bp_order_id'])) {
                $condition .= " AND bpo.bp_order_id = " . $result['bp_order_id'] . " ";
            }
            if (!empty($result['order_id'])) {
                $condition .= " AND o.order_id = " . $result['order_id'] . " ";
            }

            $main_table = array("tbl_container as c", array('c.container_id,c.container_number,c.last_container_status_id'));
            $join_tables = array(
                array("left", "tbl_order as o", "o.order_id = c.order_id", array()),
                array("left", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id OR bpo.bp_order_id = o.supplier_contract_id", array()),
            );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
            $containerData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
            $containerIdsStr = '0';
            $containerIdsArr = array();
            $containerStatusIdArr = array();
            //container filter data
            if (!empty($containerData)) {
                $result['containerData'] = $containerData;
                //hold container ids
                foreach ($containerData as $value) {
                    if (!empty($containerIdsStr)) {
                        $containerIdsStr = $containerIdsStr . ',' . $value['container_id'];
                    } else {
                        $containerIdsStr = $value['container_id'];
                    }
                    $containerIdsArr[] = $value['container_id'];
                    //hold container last status ids
                    $containerStatusIdArr[] = $value['last_container_status_id'];
                }
            }

            $condition = " c.status = 'Active'  AND  c.vessel_name  Is Not null  AND c.vessel_name !='' AND c.container_id IN (" . $containerIdsStr . ") ";
            $vesselData = $this->common->getData("tbl_container as c", "distinct(c.vessel_name)", $condition);
            if (!empty($vesselData)) {
                $result['vesselData'] = $vesselData;
            }

            $condition = " bp.business_category = 'Freight Forwarder' AND c.container_id IN (" . $containerIdsStr . ") ";
            $main_table = array("tbl_business_partner as bp", array("bp.business_partner_id,bp.alias"));
            $join_tables = array(
                array("", "tbl_container as c", "c.freight_forwarder_id = bp.business_partner_id", array()),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'c.freight_forwarder_id');
            $freightForwarder = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
            //freight Forwarder filter data
            if (!empty($freightForwarder)) {
                $result['freightForwarder'] = $freightForwarder;
            }

            $containerStatusIdStr = 0;
            if (!empty($containerStatusIdArr)) {
                $containerStatusIdStr = implode(",", $containerStatusIdArr);
            }

            $condition = " container_status_id IN (" . $containerStatusIdStr . ") ";
            $containerStatus = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", $condition, "status_squence", "Asc");
            //container status filter data
            if (!empty($containerStatus)) {
                $result['containerStatus'] = $containerStatus;
            }

            $condition = "csku.container_id IN (" . $containerIdsStr . ")";
            $main_table = array("tbl_sku as sku", array("sku.*"));
            $join_tables = array(
                array("", "tbl_container_sku as csku", "csku.sku_id = sku.sku_id", array()),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'sku.sku_id');
            $productData = $this->common->MySqlFetchRow($rs, "array"); // fetch result
            //product filter data
            if (!empty($productData)) {
                $result['productData'] = $productData;
            }

            // for sku id to get brand 
            // $sku_ids = '';
            $sku_ids = array(0);
            foreach ($productData as $key => $value) {
                $sku_ids[] = $value['sku_id'];
            }

            $sku_ids = implode(',', $sku_ids);

            $condition = "1=1 AND sku_id IN ($sku_ids) ";
            $main_table = array("tbl_brand as b", array("b.*"));
            $join_tables = array(
                array("", "tbl_sku as sku", "sku.brand_id = b.brand_id", array()),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'b.brand_id');
            $brandData = $this->common->MySqlFetchRow($rs, "array"); // fetch result
            //brand filter data
            if (!empty($brandData)) {
                $result['brandData'] = $brandData;
            }

            $condition = " 1=1 ";
            if (!empty($result['bp_order_id'])) {
                $condition .= " AND bpo.bp_order_id = " . $result['bp_order_id'] . " ";
            }
            if (!empty($result['order_id'])) {
                $condition .= " AND o.order_id = " . $result['order_id'] . " ";
            }
            $main_table = array("tbl_order as o", array());
            $join_tables = array(
                array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id OR bpo.bp_order_id = o.supplier_contract_id", array()),
                array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id ", array('bp.business_partner_id,bp.alias')),
            );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
            $bpData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
            //BP filter data
            if (!empty($bpData)) {
                $result['bpData'] = $bpData;
            }

            $condition = " 1=1 ";
            if (!empty($result['bp_order_id'])) {
                $condition .= " AND bpo.bp_order_id = " . $result['bp_order_id'] . " ";
            }
            if (!empty($result['order_id'])) {
                $condition .= " AND o.order_id = " . $result['order_id'] . " ";
            }
            $main_table = array("tbl_order as o", array());
            $join_tables = array(
                array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id OR bpo.bp_order_id = o.supplier_contract_id", array('bpo.bp_order_id,bpo.contract_number')),
            );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
            //BP contract filter data
            $bpContractData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
            if (!empty($bpContractData)) {
                $result['bpContractData'] = $bpContractData;
            }

            $condition = " 1=1 ";
            if (!empty($result['bp_order_id'])) {
                $condition .= " AND bpo.bp_order_id = " . $result['bp_order_id'] . " ";
            }
            if (!empty($result['order_id'])) {
                $condition .= " AND o.order_id = " . $result['order_id'] . " ";
            }
            $main_table = array("tbl_order as o", array('o.order_id,o.contract_number'));
            $join_tables = array(
                array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id OR bpo.bp_order_id = o.supplier_contract_id", array()),
            );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
            $orderData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
            //Master contract filter data
            if (!empty($orderData)) {
                $result['orderData'] = $orderData;
            }

            $FRILSData = $this->listOfContainermodel->getFRIandLS();
            //LS and FRI filter data
            if (!empty($FRILSData)) {
                foreach ($FRILSData as $value) {
                    if (in_array($value['container_id'], $containerIdsArr)) {
                        if ($value['document_type_id'] == 2) {
                            $result['LS'][] = $value;
                        } else {
                            $result['FRI'][] = $value;
                        }
                    }
                }
            }
            
            
            $containerIdsStr = "0";
            if(!empty($containerIdsArr)){
                $containerIdsStr = implode(",", $containerIdsArr);
            }

            //HBL filter data 
            $condition = "cdd.hbl_number IS NOT NULL  AND dt.document_type_name = 'HBL' AND cdd.container_id IN (".$containerIdsStr.") ";
            $main_table = array("tbl_custom_dynamic_data as cdd", array("cdd.hbl_number as uploaded_document_number"));
            $join_tables = array(
                array("", "tbl_document_type as dt", "dt.document_type_id = cdd.document_type_id", array()),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'cdd.hbl_number');
            $hblData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
            $result['HBL'] = array();
            if (!empty($hblData)) {
                foreach ($hblData as $value) {
                    $result['HBL'][] = $value;
                }
            }


            //Internal HBL filter data 
            $condition = " uploaded_document_number like '%IHBL%' AND container_id IN (".$containerIdsStr.") ";
            $rs = $this->common->Fetch("tbl_document_ls_uploaded_files","uploaded_document_number" ,$condition, "", 'uploaded_document_number');
            $internalHBLData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
            $result['internalHBL'] = array();
            if (!empty($internalHBLData)) {
                foreach ($internalHBLData as $value) {
                    $result['internalHBL'][] = $value;
                }
            }

            //Internal FCR filter data 
            $condition = " uploaded_document_number like '%IFCR%' AND container_id IN (".$containerIdsStr.") ";
            $rs = $this->common->Fetch("tbl_document_ls_uploaded_files","uploaded_document_number" ,$condition, "", 'uploaded_document_number');
            $internalFCRData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
            $result['internalFCR'] = array();
            if (!empty($internalFCRData)) {
                foreach ($internalFCRData as $value) {
                    $result['internalFCR'][] = $value;
                }
            }

            //Rivesed ETA filter data 
            $condition = " revised_eta IS NOT NULL AND container_id IN (".$containerIdsStr.") ";
            $rs = $this->common->Fetch("tbl_container","revised_eta" ,$condition, "", "revised_eta","revised_eta ASC");
            $revisedETAData = $this->common->MySqlFetchRow($rs, "array"); // fetch result   
            $result['revisedETA'] = array();
            if (!empty($revisedETAData)) {
                foreach ($revisedETAData as $value) {
                    $result['revisedETA'][] = $value;
                }
            }
            
            //Manufacture filter data  
            $condition = " c_sku.container_id IN (" . $containerIdsStr . ") ";
            $main_table = array("tbl_container_sku as c_sku", array()); 
            $join_tables = array(
                array("", "tbl_sku as sku", "sku.sku_id = c_sku.sku_id", array()),
                array("", "tbl_business_partner as bp", "bp.business_partner_id = sku.manufacturer_id", array("bp.business_partner_id,bp.alias")),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'bp.alias');
            $manufactureData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
            $result['manufacture'] = array();
            if (!empty($manufactureData)) {
                foreach ($manufactureData as $value) {
                    $result['manufacture'][] = $value;
                }
            } 
            
            
        }
        
        $condition = " status = 'Active'  "; //AND container_status_id IN (8,9,11,2)
        $containerStatusData = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", $condition, 'status_squence', 'asc');
        if (!empty($containerStatusData)) {
            $result['containerStatusData'] = $containerStatusData;
        }

        $result['msg'] = isset($_GET['msg']) ? $_GET['msg'] : "";
        // echo "<pre>";
        // print_r($result);
        // exit;

        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('index', $result);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    function getFilterdata() {
        // echo "<pre>";
        // print_r($_POST);
        // exit;
        $result = array();
        $result['job_not_completed_document_type_id'] = array();
        $less_doc_type_id = array(); 

        //get filter result
        $ListOfContainerData = $this->listOfContainermodel->getFilterData($_POST);
        //define array in case of container last status GOODS RECEIVED IN PORT
        $jobNotCompleted = array(1, 2, 3, 4, 5, 10);

        $condition = " status = 'Active' ";
        $result['documentType'] = $this->common->getData('tbl_document_type', 'document_type_id,document_type_name', $condition, 'document_type_squence', 'asc');

        if (!empty($ListOfContainerData['query_result'])) {
            foreach ($ListOfContainerData['query_result'] as $key => $value) {
                $condition = " status = 'Active' ";
                $result['ListOfContainerData'][$key]['containerData'] = $value;
                $result['ListOfContainerData'][$key]['containerStatus'] = $value['container_status_name'];

                //last update by and date
                if (!empty($value['last_status_by'])) {
                    $updateByData = $this->common->getData("tbl_users", "firstname,lastname", array("user_id" => $value['last_status_by']));
                    if (!empty($updateByData)) {
                        $result['ListOfContainerData'][$key]['updatedBy'] = $updateByData[0]['firstname'] . ' ' . substr($updateByData[0]['lastname'], 0, 1);
                        $result['ListOfContainerData'][$key]['updatedOn'] = date('d-M-y', strtotime($value['last_status_date']));
                    }
                } else {
                    $result['ListOfContainerData'][$key]['updatedBy'] = 'System';
                    $result['ListOfContainerData'][$key]['updatedOn'] = date('d-M-y', strtotime($value['last_status_date']));
                }

                //ravendra commit start                
                //code for document type color 
                $DocumentStatusData = $this->listOfContainermodel->getDocumentInContainer($value['container_id']);
                // $DocumentStatusData = $this->listOfContainermodel->getDocumentInContainer('11'); 

                $uploaded_document_type_id = array();
                $uploaded_document_seqence_id = array();
                // echo "<pre>";
                // print_r($DocumentStatusData);
                // exit;
                if ($DocumentStatusData) {
                    foreach ($DocumentStatusData as $key1 => $value1) {
                        $uploaded_document_type_id[] = $value1['document_type_id'];
                        $uploaded_document_seqence_id[] = $value1['document_type_squence'];
                        // echo "container_id".$value['container_id'];
                        // print_r($uploaded_document_seqence_id);
                        $result['DocumentStatusData'][$value['container_id']][$value1['document_type_id']] = array("status" => $value1['status']);

                        // to get the uploaded document for each container
                        // for ls n fri 
                        $document_type_ids = array(2, 3);
                        if (in_array($value1['document_type_id'], $document_type_ids)) {
                            $condition = " 1=1 AND duf.container_id = " . $value['container_id'] . " AND document_type_id = " . $value1['document_type_id'] . " ";
                            // $condition = " 1=1 AND duf.container_id = 11 AND document_type_id = ".$value1['document_type_id']." ";

                            $rs = $this->common->Fetch('tbl_document_uploaded_files as duf', 'duf.*,group_concat(duf.uploaded_document_number) as uploaded_document_number', $condition, '', 'document_type_id');

                            $result['getuploaded_Document'][$value['container_id']][$value1['document_type_id']] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                        } else {
                            $condition = " 1=1 AND dluf.container_id = " . $value['container_id'] . " AND document_type_id = " . $value1['document_type_id'] . "  ";

                            $rs = $this->common->Fetch('tbl_document_ls_uploaded_files as dluf', 'dluf.*,group_concat(dluf.uploaded_document_number) as uploaded_document_number', $condition, '', 'document_type_id');

                            $result['getuploaded_Document'][$value['container_id']][$value1['document_type_id']] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                            // $result['getuploaded_Document'][$value['container_id']]  = $this->common->getData();
                        }
                    }
                }
                //ravendra commit end
                // print_r( $result['getuploaded_Document']);
                // exit; 
                $less_doc_type_id = array();
                if ($DocumentStatusData) {
                    $max_sequence = max(array_unique($uploaded_document_seqence_id));
                    // echo "max ".$max_sequence; 
                    // get all document which is less the max_squence 
                    $condition = "status = 'Active' AND  document_type_squence < " . $max_sequence . " ";
                    $get_docTypeid_lessSequence = $this->common->getData("tbl_document_type", 'document_type_id', $condition);

                    // print_r($get_docTypeid_lessSequence);
                    if ($get_docTypeid_lessSequence) {
                        foreach ($get_docTypeid_lessSequence as $seqkey => $seqval) {
                            $less_doc_type_id[] = $seqval['document_type_id'];
                        }
                    }
                    //check container last status is GOODS RECEIVED IN PORT(status id =7)
                    if ($value['last_container_status_id'] == 7) {
                        $result['containers'][$value['container_id']]['job_not_completed_document_type_id'] = array_diff($jobNotCompleted, $uploaded_document_type_id);
                    } else {
                        $result['containers'][$value['container_id']]['job_not_completed_document_type_id'] = array_diff($less_doc_type_id, $uploaded_document_type_id);
                    }
                } else {
                    if ($value['last_container_status_id'] == 7) {
                        $result['containers'][$value['container_id']]['job_not_completed_document_type_id'] = $jobNotCompleted;
                    }
                }
            } // container loop end here  
        }
        
        $result['totalRecords'] = $ListOfContainerData['totalRecords'];
        
        //get filters data and assign in array with filter id and value
        $filtersData = $_POST; 
        $result['filter'] = array(); 

        if(!empty($filtersData['container_id'])){ 
            $container_ids = implode(",",$filtersData['container_id']);
            $condition = " container_id IN (".$container_ids.") ";  
            $containersData = $this->common->getData("tbl_container", "container_number", $condition);
            $containerNumbers = array();
            foreach ($containersData as $value) {
                $containerNumbers[] = $value['container_number'];
            }
            $containerNum = implode(",", $containerNumbers);
            $result['filter'][] = array("title"=>"sSearch_1","value"=> $containerNum);
        } 
        if(!empty($filtersData['status'])){
            $value = $this->common->getData("tbl_container_status", "container_status_name", array("container_status_id" => $filtersData['status']));
            $result['filter'][] = array("title"=>"sSearch_2","value"=>isset($value[0]['container_status_name'])?$value[0]['container_status_name']:'');
        } 
        if(!empty($filtersData['fri'])){ 
            $result['filter'][] = array("title"=>"sSearch_7","value"=>$filtersData['fri']);
        }
        if(!empty($filtersData['ls'])){ 
            $result['filter'][] = array("title"=>"sSearch_8","value"=>$filtersData['ls']);
        } 
        if(!empty($filtersData['hbl'])){ 
            $result['filter'][] = array("title"=>"sSearch_12","value"=>$filtersData['hbl']);
        }
        if(!empty($filtersData['internal_hbl'])){ 
            $result['filter'][] = array("title"=>"sSearch_13","value"=>$filtersData['internal_hbl']);
        } 
        if(!empty($filtersData['internal_fcr'])){ 
            $result['filter'][] = array("title"=>"sSearch_14","value"=>$filtersData['internal_fcr']);
        } 
        if(!empty($filtersData['revised_eta'])){ 
            $result['filter'][] = array("title"=>"sSearch_15","value"=>date('d-M-y',strtotime($filtersData['revised_eta'])));
        } 
        if(!empty($filtersData['vessel_id'])){ 
            $result['filter'][] = array("title"=>"sSearch_5","value"=>$filtersData['vessel_id']);
        }
        if(!empty($filtersData['manufacturer'])){
            $value = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $filtersData['manufacturer']));
            $result['filter'][] = array("title"=>"sSearch_16","value"=>isset($value[0]['alias'])?$value[0]['alias']:'');
        }  
        if(!empty($filtersData['product_id'])){
            $value = $this->common->getData("tbl_sku", "product_name", array("sku_id" => $filtersData['product_id']));
            $result['filter'][] = array("title"=>"sSearch_3","value"=>isset($value[0]['product_name'])?$value[0]['product_name']:'');
        } 
        if(!empty($filtersData['brand_id'])){
            $value = $this->common->getData("tbl_brand", "brand_name", array("brand_id" => $filtersData['brand_id']));
            $result['filter'][] = array("title"=>"sSearch_4","value"=>isset($value[0]['brand_name'])?$value[0]['brand_name']:'');
        }
        if(!empty($filtersData['freight_forwarder'])){
            $value = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $filtersData['freight_forwarder']));
            $result['filter'][] = array("title"=>"sSearch_6","value"=>isset($value[0]['alias'])?$value[0]['alias']:'');
        }  
        if(!empty($filtersData['bp'])){
            $value = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $filtersData['bp']));
            $result['filter'][] = array("title"=>"sSearch_9","value"=>isset($value[0]['alias'])?$value[0]['alias']:'');
        } 
        if(!empty($filtersData['bp_contract'])){
            $value = $this->common->getData("tbl_bp_order", "contract_number", array("bp_order_id" => $filtersData['bp_contract']));
            $result['filter'][] = array("title"=>"sSearch_10","value"=>isset($value[0]['contract_number'])?$value[0]['contract_number']:'');
        }  
        if(!empty($filtersData['contract'])){
            $value = $this->common->getData("tbl_order", "contract_number", array("order_id" => $filtersData['contract']));
            $result['filter'][] = array("title"=>"sSearch_11","value"=>isset($value[0]['contract_number'])?$value[0]['contract_number']:'');
        }   

//        echo "<pre>";
//        print_r($result['filter']);
//        exit;
        // get desing based on container received data 

        $containerListingDiv = $this->load->view('containerList', $result, true);
        if ($containerListingDiv) {
            echo json_encode(array('success' => true, 'msg' => 'record Found', "containerListingDiv" => $containerListingDiv));
            exit;
        } else {
            echo json_encode(array('success' => false, 'msg' => 'record not Found', "containerListingDiv" => $containerListingDiv));
            exit;
        }
    }

    function getFilters() {
        //echo '<pre>'; print_r($_POST);die;
        $post = $_POST;  
        $resultArr = $this->listOfContainermodel->filterCondition($post); 
        $condition = $resultArr['condition'];
        $join = $resultArr['join'];
        
        $qry = "select c.container_id, c.last_container_status_id
                from tbl_container as c  
                ".$join."  
                where ".$condition."  
                group by c.container_id ";
        //echo $qry;die;
        $query = $this->db->query($qry); 
        $result = $query->result_array();
        
        $containerStatusIdArr = array();
        $containerIdsArr = array();
        if(!empty($result)){
            foreach ($result as $value) {
                $containerIdsArr[] = $value['container_id'];
                $containerStatusIdArr[] = $value['last_container_status_id'];
            }
        }
        $containerIdsStr = 0;
        if(!empty($containerIdsArr)){
            $containerIdsStr = implode(',', $containerIdsArr);
        }
          
        //Master Contract dropdown      
        $condition = "c.container_id IN (" . $containerIdsStr . ")";
        $main_table = array("tbl_container as c", array());
        $join_tables = array(
            array("", "tbl_order as o", "o.order_id = c.order_id", array('o.order_id,o.contract_number')),
        ); 
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'o.contract_number');
        $orderData = $this->common->MySqlFetchRow($rs, "array"); // fetch result   
        $str_contract = singleSelectSearchDropDown($orderData,'sSearch_11','Master Contract',$post['contract'],'order_id','contract_number','select-large','',"onchange='getFilters(11);'");

        //Container dropdown
        $condition = " container_id IN (" . $containerIdsStr . ") ";
        $containerData = $this->common->getData("tbl_container", "container_id,container_number,last_container_status_id", $condition);  
        //$str_container = singleSelectSearchDropDown($containerData,'sSearch_1','Container',$post['container_id'],'container_id','container_number','',"style='width:100px'","onchange='getFilters(1);'");  
         
        $str_container = '<div class="multiselect-dropdown-wrapper" id="multiselect-dropdown-wrapper" style="width:165px">
                            <div class="md-value searchfilter">
                                Container 
                            </div>
                            <div class="md-list-wrapper searchlist-option-wrapper">
                                <input type="text" placeholder="Search" class="md-search">
                                <div class="md-list-items ud-list">';
                                if ($containerData) {
                                    foreach ($containerData as $key => $value) { 
                                    $str_container.='<div class="mdli-single ud-list-single">
                                          <label class="container-checkbox">'.$value['container_number'].'
                                              <input type="checkbox" id="sSearch_1_'.$value['container_id'].'"  value="'.$value['container_id'].'" name="sSearch_1[]">
                                              <span class="checkmark-checkbox"></span>
                                          </label>
                                      </div>'; 
                                    }
                                } 
                                $str_container.='</div>
                                <div class="md-cta">
                                    <p class="btn-primary-mro md-done" onclick="getFilters(1);">Done</p>
                                    <p class="btn-secondary-mro md-clear searchlisting-clear">Clear All</p>
                                </div>
                            </div>
                        </div>';
        
        //Vessel dropdown
        $condition = " c.status = 'Active'  AND  c.vessel_name  Is Not null  AND c.vessel_name !='' AND c.container_id IN (" . $containerIdsStr . ") ";
        $vesselData = $this->common->getData("tbl_container as c", "distinct(c.vessel_name)", $condition);         
        $str_vessel = singleSelectSearchDropDown($vesselData,'sSearch_5','Vessel',$post['vessel_id'],'vessel_name','vessel_name','select-large','',"onchange='getFilters(5);'");
        
        //Freight Forwarder dropdown
        $condition = " bp.business_category = 'Freight Forwarder' AND c.container_id IN (" . $containerIdsStr . ") ";
        $main_table = array("tbl_business_partner as bp", array("bp.business_partner_id,bp.alias"));
        $join_tables = array(
            array("", "tbl_container as c", "c.freight_forwarder_id = bp.business_partner_id", array()),
        );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'c.freight_forwarder_id');
        $freightForwarder = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
        $str_ff = singleSelectSearchDropDown($freightForwarder,'sSearch_6','Freight Forwarder',$post['freight_forwarder'],'business_partner_id','alias','select-large','',"onchange='getFilters(6);'");
           
        //container status filter data
        $containerStatusIdStr = 0;
        if (!empty($containerStatusIdArr)) {
            $containerStatusIdStr = implode(",", $containerStatusIdArr);
        } 
        $condition = " container_status_id IN (" . $containerStatusIdStr . ") ";
        $containerStatus = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", $condition, "status_squence", "Asc");
        $str_status = singleSelectSearchDropDown($containerStatus,'sSearch_2','Status',$post['status'],'container_status_id','container_status_name','select-large','',"onchange='getFilters(2);'");
         
        //product filter data 
        $condition = "csku.container_id IN (" . $containerIdsStr . ")";
        $main_table = array("tbl_sku as sku", array("sku.*"));
        $join_tables = array(
            array("", "tbl_container_sku as csku", "csku.sku_id = sku.sku_id", array()),
        );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'sku.sku_id');
        $productData = $this->common->MySqlFetchRow($rs, "array"); // fetch result         
        $str_prodcut = singleSelectSearchDropDown($productData,'sSearch_3','Product',$post['product_id'],'sku_id','product_name','select-large','',"onchange='getFilters(3);'");
         

        // for sku id to get brand 
        // $sku_ids = '';
        $sku_ids = array(0);
        foreach ($productData as $key => $value) {
            $sku_ids[] = $value['sku_id'];
        }

        $sku_ids = implode(',', $sku_ids);
         //brand filter data
        $condition = "1=1 AND sku_id IN ($sku_ids) ";
        $main_table = array("tbl_brand as b", array("b.*"));
        $join_tables = array(
            array("", "tbl_sku as sku", "sku.brand_id = b.brand_id", array()),
        );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'b.brand_id');
        $brandData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
        $str_brand = singleSelectSearchDropDown($brandData,'sSearch_4','Brand',$post['brand_id'],'brand_id','brand_name','select-medium','',"onchange='getFilters(4);'");
        
        
        //BP filter data
        $str_bp = '';
        if ($_SESSION["mro_session"]['user_role'] != 'Customer' && $_SESSION["mro_session"]['user_role'] != 'Supplier') {
            $condition = " c.container_id IN (" . $containerIdsStr . ") ";
            $main_table = array("tbl_container as c", array());
            $join_tables = array(
                array("", "tbl_order as o", "o.order_id = c.order_id", array()),
                array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id OR bpo.bp_order_id = o.supplier_contract_id", array()),
                array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id ", array('bp.business_partner_id,bp.alias')),
            ); 
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,'','bp.alias');
            $bpData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
            $str_bp = singleSelectSearchDropDown($bpData,'sSearch_9','Business Partner',$post['bp'],'business_partner_id','alias','select-large','',"onchange='getFilters(9);'");
        } 
        
         //BP contract filter data 
        $condition = " c.container_id IN (" . $containerIdsStr . ") ";
        $main_table = array("tbl_container as c", array());
        $join_tables = array(
            array("", "tbl_order as o", "o.order_id = c.order_id", array()),
            array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id OR bpo.bp_order_id = o.supplier_contract_id", array('bpo.bp_order_id,bpo.contract_number')),
        ); 
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,'','bpo.contract_number');
        $bpContractData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
        $str_bp_contract = singleSelectSearchDropDown($bpContractData,'sSearch_10','BP Contract',$post['bp_contract'],'bp_order_id','contract_number','',"style='width:130px'","onchange='getFilters(10);'");
        
        //LS filter data 
        $condition = " document_type_id = 2 AND container_id IN (" . $containerIdsStr . ") ";
        $rs = $this->common->Fetch("tbl_document_uploaded_files","uploaded_document_number" ,$condition, "", 'uploaded_document_number');
        $LSData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
        $str_ls = singleSelectSearchDropDown($LSData,'sSearch_8','LS',$post['ls'],'uploaded_document_number','uploaded_document_number','',"style='width:90px'","onchange='getFilters(8);'");
          
        //FRI filter data
        $condition = " document_type_id = 3 AND container_id IN (" . $containerIdsStr . ") ";
        $rs = $this->common->Fetch("tbl_document_uploaded_files","uploaded_document_number" ,$condition, "", 'uploaded_document_number');
        $FRIData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
        $str_fri = singleSelectSearchDropDown($FRIData,'sSearch_7','FRI',$post['fri'],'uploaded_document_number','uploaded_document_number','',"style='width:90px'","onchange='getFilters(7);'"); 
         
        //HBL filter data 
        $condition = "cdd.hbl_number IS NOT NULL  AND dt.document_type_name = 'HBL' AND  cdd.container_id IN (".$containerIdsStr.") ";
        $main_table = array("tbl_custom_dynamic_data as cdd", array("cdd.hbl_number as uploaded_document_number"));
        $join_tables = array(
            array("", "tbl_document_type as dt", "dt.document_type_id = cdd.document_type_id", array()),
        );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'cdd.hbl_number');
        $hblData = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
        $result['HBL'] = array();
        if (!empty($hblData)) {
            foreach ($hblData as $value) {
                $result['HBL'][] = $value;
            }
        }
        
        $str_hbl = singleSelectSearchDropDown($hblData,'sSearch_12','HBL',$post['hbl'],'uploaded_document_number','uploaded_document_number','',"style='width:140px'","onchange='getFilters(12);'"); 
  
        //Internal HBL filter data 
        $condition = " uploaded_document_number like '%IHBL%' AND container_id IN (".$containerIdsStr.") ";
        $rs = $this->common->Fetch("tbl_document_ls_uploaded_files","uploaded_document_number" ,$condition, "", 'uploaded_document_number');
        $internalHBLData = $this->common->MySqlFetchRow($rs, "array"); // fetch result   
        $str_internal_hbl = singleSelectSearchDropDown($internalHBLData,'sSearch_13','Internal HBL',$post['internal_hbl'],'uploaded_document_number','uploaded_document_number','',"style='width:110px'","onchange='getFilters(13);'"); 
          
        //Internal FCR filter data 
        $condition = " uploaded_document_number like '%IFCR%' AND container_id IN (".$containerIdsStr.") ";
        $rs = $this->common->Fetch("tbl_document_ls_uploaded_files","uploaded_document_number" ,$condition, "", 'uploaded_document_number');
        $internalFCRData = $this->common->MySqlFetchRow($rs, "array"); // fetch result   
        $str_internal_fcr = singleSelectSearchDropDown($internalFCRData,'sSearch_14','Internal FCR',$post['internal_fcr'],'uploaded_document_number','uploaded_document_number','',"style='width:110px'","onchange='getFilters(14);'"); 
          
        //Rivesed ETA filter data 
        $condition = " revised_eta IS NOT NULL AND container_id IN (".$containerIdsStr.") ";
        $rs = $this->common->Fetch("tbl_container","revised_eta" ,$condition, "", "revised_eta","revised_eta ASC");
        $revisedETAData = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
        $str_revised = "<select name='sSearch_15' id='sSearch_15' class='searchInput basic-single' style='width:110px' onchange='getFilters(15)'>"
                . "<option value='' disabled selected hidden>Revised ETA</option>"; 
        if (!empty($revisedETAData)) {
            foreach ($revisedETAData as $value) {
                $selected = '';
                if($post['revised_eta'] == date('Y-m-d', strtotime($value['revised_eta']))){
                    $selected = 'selected';
                }
                $str_revised = $str_revised . "<option value='" . date('Y-m-d', strtotime($value['revised_eta'])) . "' ".$selected." >" . date('d-M-Y', strtotime($value['revised_eta'])) . "</option>";
            }
        }
        $str_revised = $str_revised . "</select>";


        $condition = " c_sku.container_id IN (" . $containerIdsStr . ") ";
        $main_table = array("tbl_container_sku as c_sku", array()); 
        $join_tables = array(
            array("", "tbl_sku as sku", "sku.sku_id = c_sku.sku_id", array()),
            array("", "tbl_business_partner as bp", "bp.business_partner_id = sku.manufacturer_id", array("bp.business_partner_id,bp.alias")),
        );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'bp.alias');
        $manufactureData = $this->common->MySqlFetchRow($rs, "array"); // fetch result
        $str_manufacture = singleSelectSearchDropDown($manufactureData,'sSearch_16','Manufacturer',$post['manufacturer'],'business_partner_id','alias','',"style='width:120px'","onchange='getFilters(16);'");  
          
        
        $resultData = array('contract'=>$str_contract, "container"=>$str_container, "vessel"=>$str_vessel, "ff"=>$str_ff,
                "status"=>$str_status, "prodcut"=>$str_prodcut, "brand"=>$str_brand,
                "bp"=>$str_bp, "bp_contract"=>$str_bp_contract, "ls"=>$str_ls, "fri"=>$str_fri,
                "hbl"=>$str_hbl, "internal_hbl"=>$str_internal_hbl, "internal_fcr"=>$str_internal_fcr,
                "revised"=>$str_revised, "manufacture"=>$str_manufacture);
        echo json_encode($resultData);
        exit; 
    }

    function addEdit() {
        if (!$this->privilegeduser->hasPrivilege("ContainersDetailsAddEdit")) {
            redirect('dashboard');
        }
        //add edit form
        $result = array();
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $document_type_id = $url_prams['id'];

            $condition = "1=1  AND dt.document_type_id = '" . $document_type_id . "' ";
            $main_table = array("tbl_document_type as dt", array("dt.*"));
            $join_tables = array(
                array("left", "tbl_sub_document_type as sdt", "sdt.document_type_id = dt.document_type_id", array("sdt.sub_document_type_name,sdt.sub_document_type_id,sdt.sub_status")),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $result['document_type_details'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
            // $result['faq_section_details'] = $this->array_formation($faq_details,'faq_section_id');
        }
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('addEdit', $result);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }

    public function fetch() {
        //get record at list
        $get_result = $this->ListOfDocumentmodel->getRecords($_GET);
        // echo "<pre>";
        // print_r($get_result);
        // exit;
        $result = array();
        $result["sEcho"] = $_GET['sEcho'];
        $result["iTotalRecords"] = $get_result['totalRecords']; //iTotalRecords get no of total recors
        $result["iTotalDisplayRecords"] = $get_result['totalRecords']; //iTotalDisplayRecords for display the no of records in data table.
        $items = array();
        if (!empty($get_result['query_result']) && count($get_result['query_result']) > 0) {
            $j = 0;
            for ($i = 0; $i < sizeof($get_result['query_result']); $i++) {
                $temp = array();
                ++$j;
                // array_push($temp, ucfirst($get_result['query_result'][$i]->document_type_squence));
                array_push($temp, $j);
                array_push($temp, ucfirst($get_result['query_result'][$i]->document_type_name) . "<span class='squence' tr_row=" . $get_result['query_result'][$i]->document_type_id . "></span>");
                array_push($temp, ucfirst($get_result['query_result'][$i]->sub_document_type));
                array_push($temp, ucfirst($get_result['query_result'][$i]->status));
                $actionCol = '';
                if ($this->privilegeduser->hasPrivilege("ContainersDetailsAddEdit")) {
                    $actionCol = '<a href="listOfDocument/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $get_result['query_result'][$i]->document_type_id), '+/', '-_'), '=') . '" title="Edit" class="text-center" style="float:left;width:100%;"><img src="' . base_url() . 'assets/images/edit-icon.svg" alt="Edit" ></a>';
                }
                array_push($temp, $actionCol);
                array_push($items, $temp);
            }
        }
        $result["aaData"] = $items;
        echo json_encode($result);
        exit;
    }

    public function submitForm() {
        // echo "<pre>";
        // print_r($_POST);
        // exit;

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            /* check duplicate entry */
            $condition = "document_type_name = '" . trim($this->input->post('document_type_name')) . "' ";
            if (!empty($this->input->post("document_type_id"))) {
                $condition .= " AND document_type_id <> " . $this->input->post("document_type_id");
            }

            // check for existance 
            $exiting = $this->common->getData("tbl_document_type", "*", $condition);

            if (!empty($exiting[0]['document_type_id'])) {
                echo json_encode(array('success' => false, 'msg' => 'Document Type already exist...'));
                exit;
            }

            $data = array();
            $data['document_type_name'] = trim($this->input->post('document_type_name'));
            $data['status'] = $this->input->post('status');
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['created_on'] = date("Y-m-d H:i:s");

            if (!empty($this->input->post("document_type_id"))) {
                //update data
                $condition = " document_type_id = '" . $this->input->post("document_type_id") . "' ";
                $result = $this->common->updateData("tbl_document_type", $data, $condition);
                if ($result) {

                    if (!empty($_POST['sub_document_type_name']) && isset($_POST['sub_document_type_name'])) {
                        foreach ($_POST['sub_document_type_name'] as $key => $value) {
                            $data = array();
                            $data['document_type_id'] = $this->input->post("document_type_id");
                            $data['sub_document_type_name'] = $value;
                            $data['sub_status'] = $_POST['sub_status'][$key];
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $result = $this->common->insertData("tbl_sub_document_type", $data, "1");
                        }
                    }
                    if (!empty($_POST['sub_document_type_name_existing']) && isset($_POST['sub_document_type_name_existing'])) {
                        foreach ($_POST['sub_document_type_name_existing'] as $key => $value) {
                            $data = array();
                            $data['document_type_id'] = $this->input->post("document_type_id");
                            $data['sub_document_type_name'] = $value;
                            $data['sub_status'] = $_POST['sub_status_existing'][$key];
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $condition = "sub_document_type_id = '" . $key . "' ";
                            $result = $this->common->updateData("tbl_sub_document_type", $data, $condition);
                        }
                    }

                    // forloop start here
                    echo json_encode(array('success' => true, 'msg' => 'Record Updated Successfully.'));
                    exit;
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Updating data.'));
                    exit;
                }
            } else {
                //insert data
                $data['document_type_name'] = trim($this->input->post('document_type_name'));
                $data['status'] = $this->input->post('status');
                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['updated_on'] = date("Y-m-d H:i:s");
                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                $data['created_on'] = date("Y-m-d H:i:s");
                $result = $this->common->insertData("tbl_document_type", $data, "1");
                if ($result) {

                    $document_type_id = $result;
                    // forloop start here
                    if (!empty($_POST['sub_document_type_name']) && isset($_POST['sub_document_type_name'])) {
                        foreach ($_POST['sub_document_type_name'] as $key => $value) {
                            $data = array();
                            $data['document_type_id'] = $document_type_id;
                            $data['sub_document_type_name'] = $value;
                            $data['sub_status'] = $_POST['sub_status'][$key];
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            $result = $this->common->insertData("tbl_sub_document_type", $data, "1");
                        }
                    }
                    if ($result) {
                        echo json_encode(array('success' => true, 'msg' => 'Record Inserted Successfully.'));
                        exit;
                    } else {
                        echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                        exit;
                    }
                } else {
                    echo json_encode(array('success' => false, 'msg' => 'Problem while Inserting data.'));
                    exit;
                }
            }
        } else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while add/edit data.'));
            exit;
        }
    }

    public function changeorder() {
        // echo "<pre>";
        // print_r($_POST);
        // exit;
        $positions = $this->input->post('position');
        if ($positions) {
            $order_sequence = 1;
            foreach ($positions as $key => $value) {
                $data = array();
                $data['document_type_squence'] = $order_sequence;
                $condition = "document_type_id = '" . $value . "' ";
                $result = $this->common->updateData("tbl_document_type", $data, $condition);
                $order_sequence++;
            }
            echo json_encode(array("success" => true, 'msg' => 'Position updated'));
            $this->session->set_flashdata('message-ordering', '<strong>Success!</strong> position updated.');
            exit;
        } else {
            echo json_encode(array("success" => false, 'msg' => 'No themes left for re-ordering'));
            exit;
        }
    }

    function deleteDocumentType() {

        $Result = $this->common->deleteRecord('tbl_sub_document_type', 'sub_document_type_id = ' . $_POST['id']);
        if ($Result) {
            echo json_encode(array("success" => "1", "msg" => "Deleted successfully.", "body" => NULL));
            exit;
        } else {
            echo json_encode(array("success" => "0", "msg" => "Delete Failed", "body" => NULL));
            exit;
        }
    }

    public function setCurrentSessionId() {
        $current_session_id = round(microtime(true) * 1000);
        $_SESSION['mro_session'][0]['current_session_id'] = $current_session_id;
        if ($_SESSION['mro_session'][0]['current_session_id']) {
            echo json_encode(array("success" => "1", "msg" => $_SESSION['mro_session'][0]['current_session_id'], "body" => NULL));
            exit;
        } else {
            echo json_encode(array("success" => "0", "msg" => "Current session id not set", "body" => NULL));
            exit;
        }
    }

    public function submitFormStep2() {
        //code by ravendra this is use for back and then save record functionality.
        $this->common->deleteRecord("tbl_document_ls_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_document_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_invoice_ls_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");

        if (!empty($_POST['document_type']) && isset($_POST['document_type'])) {

            // insert query for  other type of document  is LS number found 
            if (!empty($_POST['ls_number']) && isset($_POST['ls_number'])) {

                if (!empty($_POST['container_ids']) && isset($_POST['container_ids'])) {
                    $data = array();
                    $data['document_type_id'] = $_POST['document_type'];
                    $data['ls_number'] = $_POST['ls_number'];
                    $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                    $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['updated_on'] = date("Y-m-d H:i:s");
                    $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['created_on'] = date("Y-m-d H:i:s");

                    foreach ($_POST['container_ids'] as $key => $value) {
                        $data['container_id'] = $value;
                        $result = $this->common->insertData("tbl_document_ls_container_mapping", $data, "1");
                    }
                    $type = "other";
                } else {
                    echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                    exit;
                }
            } else {

                // insert data for fri n ls document type 
                if (!empty($_POST['container_ids']) && isset($_POST['container_ids'])) {
                    $data = array();
                    $data['document_type_id'] = $_POST['document_type'];
                    $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                    $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['updated_on'] = date("Y-m-d H:i:s");
                    $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['created_on'] = date("Y-m-d H:i:s");

                    foreach ($_POST['container_ids'] as $key => $value) {
                        $data['container_id'] = $value;
                        $result = $this->common->insertData("tbl_document_container_mapping", $data, "1");
                    }
                    $type = "LS";
                } else {
                    echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                    exit;
                }
            }

            if ($result) {
                echo json_encode(array("success" => true, "type" => $type, "msg" => "Document mapped with container", "doc_id" => $_POST['document_type']));
                exit;
            } else {
                echo json_encode(array("success" => false, "type" => $type, "msg" => "Select Atleast one container!", "doc_id" => $_POST['document_type']));
                exit;
            }
        } else {
            $type = "invoice";
            // this code for invoice mapping with container 
            if (!empty($_POST['business_partner']) && isset($_POST['business_partner'])) {
                if (!empty($_POST['ls_number']) && isset($_POST['ls_number'])) {
                    if (!empty($_POST['container_ids']) && isset($_POST['container_ids'])) {
                        $data = array();
                        $data['business_partner_id'] = $_POST['business_partner'];
                        $data['ls_number'] = $_POST['ls_number'];
                        $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['created_on'] = date("Y-m-d H:i:s");

                        foreach ($_POST['container_ids'] as $key => $value) {
                            $data['container_id'] = $value;
                            $result = $this->common->insertData("tbl_invoice_ls_container_mapping", $data, "1");
                        }
                        if ($result) {
                            echo json_encode(array("success" => true, "type" => $type, "msg" => "Invoice  mapped with container"));
                            exit;
                        } else {
                            echo json_encode(array("success" => false, "type" => $type, "msg" => "Select Atleast one container!"));
                            exit;
                        }
                    } else {
                        echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                        exit;
                    }
                } else {
                    echo json_encode(array("success" => false, "type" => $type, "msg" => "LS is missing for invoice type "));
                    exit;
                }
            } else {
                echo json_encode(array("success" => false, "type" => $type, "msg" => "Business partner missing "));
                exit;
            }
        }
    }

    public function getSelectedLsContainer() {

        if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {

            if ($_POST['type'] == 'other') {

                $condition = "1=1  AND dcm.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_document_ls_container_mapping as dcm", array('ls_number,document_type_id'));
                $join_tables = array(
                    array("", "tbl_container as c", "dcm.container_id = c.container_id", array("c.container_number,c.container_id")),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                $result['getSelectedContainer'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result


                if ($result) {
                    $html = '';
                    $selectedLsName = '';
                    foreach ($result['getSelectedContainer'] as $key => $value) {
                        $selectedLsName = $value['ls_number'];
                        $html .= '<div class="ud-list-single">
                                    <label class="container-checkbox">' . $value['container_number'] . '
                                    <input type="checkbox" name="selectedContainer[]" value="' . $value['container_id'] . '" checked onclick="return false" >
                                    <span class="checkmark-checkbox"></span>
                                    </label>
                                </div>
                                <input type="hidden" value="' . $value['ls_number'] . '"  name="selectedLsNumber" id="selectedLsNumber">  ';
                    }


                    $uploadSectionhtml = "";


                    $condition = "1=1  AND d.document_type_id = '" . $result['getSelectedContainer'][0]['document_type_id'] . "' ";
                    $main_table = array("tbl_document_type as d", array('d.document_type_name'));
                    $join_tables = array(
                        array("left", "tbl_sub_document_type as sd", "sd.document_type_id = d.document_type_id", array("sd.sub_document_type_name,sd.sub_document_type_id")),
                    );

                    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                    $getSubDocument = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                    if ($getSubDocument) {
                        foreach ($getSubDocument as $key => $value) {
                            if (!empty($value['sub_document_type_name'])) {
                                $uploadSectionhtml .= '<div class="mb20"><h4 class="sd-subtitle mb20">' . (!empty($value['sub_document_type_name']) ? $value['sub_document_type_name'] : " Upload Document ") . '</h4>
                                <input type="file"  name="step3_document[' . $value['sub_document_type_id'] . ']" id="step3_document' . $value['sub_document_type_id'] . '" required ></div>';
                            } else {
                                $uploadSectionhtml .= '<div class="mb20"><h4 class="sd-subtitle mb20"> Upload Document </h4>
                                <input type="file"  required name="step3_document[]" id="step3_document' . $value['sub_document_type_id'] . '"></div>';
                            }
                        }
                    }
                    echo json_encode(array("success" => true, "msg" => "Selected Ls container found", 'selectedLsName' => $selectedLsName, 'html' => $html, 'uploadSectionhtml' => $uploadSectionhtml));
                    exit;
                }
            } else {

                $condition = "1=1  AND icm.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_invoice_ls_container_mapping as icm", array('ls_number,business_partner_id'));
                $join_tables = array(
                    array("", "tbl_container as c", "icm.container_id = c.container_id", array("c.container_number,c.container_id")),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                $result['getSelectedContainer'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result


                if ($result) {
                    $html = '';
                    $selectedLsName = '';
                    foreach ($result['getSelectedContainer'] as $key => $value) {
                        $selectedLsName = $value['ls_number'];
                        $html .= '<div class="ud-list-single">
                                    <label class="container-checkbox">' . $value['container_number'] . '
                                    <input type="checkbox" name="selectedContainer[]" value="' . $value['container_id'] . '" checked>
                                    <span class="checkmark-checkbox"></span>
                                    </label>
                                </div>
                                <input type="hidden" value="invoice"  name="invoice" id="invoice">
                                <input type="hidden" value="' . $value['ls_number'] . '"  name="selectedLsNumber" id="selectedLsNumber">';
                    }
                    $uploadSectionhtml = '';

                    $uploadSectionhtml .= '<p><h4 class="sd-subtitle mb20"> Upload Invoice </h4>
                    <input type="file"  required name="step3_document" id="step3_document"></p>';

                    echo json_encode(array("success" => true, "msg" => "Selected Ls container found", 'selectedLsName' => $selectedLsName, 'html' => $html, 'uploadSectionhtml' => $uploadSectionhtml));
                    exit;
                }
            }
        } else {
            echo json_encode(array("success" => false, "msg" => "Current session id not found "));
            exit;
        }
    }

    public function getSelectedContainer() {

        if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {

            $condition = "1=1  AND dcm.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
            $main_table = array("tbl_document_container_mapping as dcm", array());
            $join_tables = array(
                array("", "tbl_container as c", "dcm.container_id = c.container_id", array("c.container_number,c.container_id")),
            );

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $result['getSelectedContainer'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
            // echo "<pre>";
            // print_r($result);
            // exit;

            if ($result) {
                $html = '';
                foreach ($result['getSelectedContainer'] as $key => $value) {

                    $html .= '<div class="ud-list-single">
                                <label class="container-checkbox">' . $value['container_number'] . '
                                <input type="checkbox" name="selectedContainer[]" value="' . $value['container_id'] . '" checked>
                                <span class="checkmark-checkbox"></span>
                                </label>
                            </div>';
                }
                echo json_encode(array("success" => true, "msg" => "", 'html' => $html));
                exit;
            } else {
                echo json_encode(array("success" => false, "msg" => "Current session id not found "));
                exit;
            }
        } else {
            echo json_encode(array("success" => false, "msg" => "Current session id not found "));
            exit;
        }
    }

    public function submitFormStep3() {

        //code by ravendra this is use for back and then save record functionality.
        $this->common->deleteRecord("tbl_document_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_document_ls_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_container_uploaded_document_status", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
        $statusLog = $this->common->getData("tbl_container_status_log", 'status_log_id', $condition);
        if (!empty($statusLog)) {
            $this->common->deleteRecord("tbl_container_status_log", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        }
        $this->common->deleteRecord("tbl_invoice_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");

        // echo "<pre>";
        // print_r($_POST);
        // print_r($_FILES);
        // exit;
        $upload = false;
        $data = array();
        $this->load->library('upload');
        $step4skip = false;
        if (!empty($_POST['invoice']) && isset($_POST['invoice'])) {
            $step4skip = true;
            if (isset($_FILES) && isset($_FILES["step3_document"]["name"])) {
                // $this->upload->initialize($this->set_upload_options(0));
                // if (!$this->upload->do_upload("step3_document")) {
                //     $image_error = array('error' => $this->upload->display_errors());
                //     echo json_encode(array("success" => false, "msg" => $image_error['error']));
                //     exit;
                // } else {
                //     $image_data = array('upload_data' => $this->upload->data());
                //     //function for upload file in live bucket
                //     $full_path = $image_data['upload_data']['full_path'];
                    $resultUpload = set_s3_upload_file($_FILES["step3_document"]["tmp_name"], 0,$_FILES["step3_document"]["name"]);
                    $step3_document =  basename($resultUpload);
                    // $step3_document = $image_data['upload_data']['file_name'];
                    $data['uploaded_invoice_file'] = $step3_document;
                    $upload = true;
                // }
            }

            if ($upload) {
                // get document selected in previous screen 
                $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_invoice_ls_container_mapping as icm", array("icm.*"));
                $join_tables = array(
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = icm.business_partner_id", array("bp.*")),
                );
                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                $invoice_details = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                if ($invoice_details) {
                    if (!empty($_POST['selectedContainer']) && isset($_POST['selectedContainer'])) {

                        foreach ($_POST['selectedContainer'] as $key => $value) {
                            $data['container_id'] = $value;
                            $data['business_partner_id'] = $invoice_details[0]['business_partner_id'];
                            $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                            // $data['uploaded_document_number'] =   $step3_document;
                            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['updated_on'] = date("Y-m-d H:i:s");
                            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data['created_on'] = date("Y-m-d H:i:s");
                            // print_r($data);
                            $result = $this->common->insertData("tbl_invoice_uploaded_files", $data, "1");
                        }
                        if ($result) {
                            echo json_encode(array("success" => true, "msg" => "Document added to container!", 'type' => 'invoice', 'step4skip' => $step4skip));
                            exit;
                        } else {
                            echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                            exit;
                        }
                    } else {
                        echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                        exit;
                    }
                } else {
                    echo json_encode(array("success" => false, "msg" => "Selected business partber  Not found!"));
                    exit;
                }
            } else {
                echo json_encode(array("success" => false, "msg" => "Uploaded file is not proper !"));
                exit;
            }
        } else {
            $step4skip = false;
            if (!empty($_POST['selectedLsNumber']) && isset($_POST['selectedLsNumber'])) {
                $files = $_FILES;

                foreach ($files['step3_document']['name'] as $key => $value) {
                    $upload = false;
                    if (!empty($files['step3_document']['name'][$key]) && isset($files['step3_document']['name'][$key])) {

                        $_FILES['step3_document']['name'] = $files['step3_document']['name'][$key];
                        $_FILES['step3_document']['type'] = $files['step3_document']['type'][$key];
                        $_FILES['step3_document']['tmp_name'] = $files['step3_document']['tmp_name'][$key];
                        $_FILES['step3_document']['error'] = $files['step3_document']['error'][$key];
                        $_FILES['step3_document']['size'] = $files['step3_document']['size'][$key];

                        // $this->upload->initialize($this->set_upload_options($_POST['document_type_id_hidden']));
                        // if (!$this->upload->do_upload("step3_document")) {
                        //     $image_error = array('error' => $this->upload->display_errors());
                        //     echo json_encode(array("success" => false, "msg" => $image_error['error']));
                        //     exit;
                        // } else {
                        //     $image_data = array('upload_data' => $this->upload->data());
                        //     //function for upload file in live bucket
                        //     $full_path = $image_data['upload_data']['full_path'];
                            $resultUpload = set_s3_upload_file($_FILES['step3_document']['tmp_name'], $_POST['document_type_id_hidden'],$_FILES['step3_document']['name']);
                            $step3_document =  basename($resultUpload);
                            // $step3_document = $image_data['upload_data']['file_name'];
                            if ($key == '0') {
                                $uploaded_document_type_id['document_type_id'] = $key;
                                $data['uploaded_document_file'] = $step3_document;
                            } else {
                                $uploaded_document_file_name['file_name'][$key] = $step3_document;
                                $uploaded_document_type_id['sub_document_type_id'][$key] = $key;
                            }
                            $upload = true;
                        // }
                    }
                }

                if ($upload) {

                    foreach ($files['step3_document']['name'] as $key => $value) {
                        // echo $value;
                        // this is for only document type  store 
                        if ($key == 0) {
                            // get document selected in previous screen 
                            $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                            $main_table = array("tbl_document_ls_container_mapping as dlcm", array("dlcm.*"));
                            $join_tables = array(
                                array("", "tbl_document_type as dt", "dt.document_type_id = dlcm.document_type_id", array("dt.*")),
                            );
                            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                            $document_type_details = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                            // $document_name = $document_type_details[0]['document_prefix_name'].'00001';
                            $document_name = $document_type_details[0]['document_prefix_name'];

                            // if($document_name){
                            //     $condition = "document_type_id = '".$document_type_details[0]['document_type_id']."' ";
                            //     $uploaded_document_data = $this->common->getDataLimit("tbl_document_ls_uploaded_files",'*',$condition,"document_uploaded_files_id","desc",'1');
                            // }
                            // if($uploaded_document_data){
                            //     $uploaded_document_name = ++$uploaded_document_data[0]['uploaded_document_number'];
                            // }else{
                            //     $uploaded_document_name = $document_name;
                            // }
                            // echo $uploaded_document_name ;
                            // exit; 
                            // get container status for document uploaded 
                            $condition = " document_type_id = '" . $document_type_details[0]['document_type_id'] . "' ";
                            $getContainerStatus = $this->common->getData('tbl_container_status', '*', $condition);

                            if ($document_name) {
                                if (!empty($_POST['selectedContainer']) && isset($_POST['selectedContainer'])) {
                                    foreach ($_POST['selectedContainer'] as $containerkey => $containervalue) {

                                        $condition = " container_id = '" . $containervalue . "' ";
                                        $getContainerNumber = $this->common->getData('tbl_container', 'container_number,revised_etd,last_container_status_id', $condition);

                                        $condition = " container_status_id = '" . $getContainerNumber[0]['last_container_status_id'] . "' ";
                                        $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status', 'status_squence', $condition);
                                        $lastContainerStatusSquence = 0;
                                        if (!empty($lastContainerStatusSquenceData)) {
                                            $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                        }

                                        $data['container_id'] = $containervalue;
                                        $data['document_type_id'] = $document_type_details[0]['document_type_id'];
                                        $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $data['uploaded_document_number'] = $document_name . $getContainerNumber[0]['container_number'];
                                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['updated_on'] = date("Y-m-d H:i:s");
                                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['created_on'] = date("Y-m-d H:i:s");
                                        // print_r($data);
                                        $result = $this->common->insertData("tbl_document_ls_uploaded_files", $data, "1");

                                        // insert record in tbl_container_uploaded_document_status for maintain the statsus for container with respective to document uploaded 
                                        $doc_status_data = array();
                                        $doc_status_data['container_id'] = $containervalue;
                                        $doc_status_data['document_type_id'] = $document_type_details[0]['document_type_id'];
                                        $doc_status_data['sub_document_type_id'] = 0;
                                        $doc_status_data['document_type_squence'] = $document_type_details[0]['document_type_squence'];
                                        $doc_status_data['status'] = 'Unchecked';
                                        $doc_status_data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                        $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                        $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1");
                                        //HBL document upload check
                                        if ($document_type_details[0]['document_type_id'] == 5) {
                                            if ($getContainerStatus && !empty($getContainerNumber[0]['revised_etd'])) {
                                                $ContainerStatus = array();
                                                $ContainerStatus['container_id'] = $containervalue;
                                                $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                                $ContainerStatus['document_date'] = date("Y-m-d");
                                                $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                                if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                                    //last status in container table
                                                    $containerStatusUpdate = array();
                                                    $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $containervalue));
                                                }
                                            }
                                        } else {
                                            if ($getContainerStatus) {
                                                $ContainerStatus = array();
                                                $ContainerStatus['container_id'] = $containervalue;
                                                $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                                $ContainerStatus['document_date'] = date("Y-m-d");
                                                $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                                if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                                    //last status in container table
                                                    $containerStatusUpdate = array();
                                                    $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $containervalue));
                                                }
                                            }
                                        }
                                    }
                                    if ($result) {
                                        echo json_encode(array("success" => true, "msg" => "Document added to container!", 'type' => 'LS', 'step4skip' => $step4skip));
                                        exit;
                                    } else {
                                        echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                                        exit;
                                    }
                                } else {
                                    echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                                    exit;
                                }
                            } else {
                                echo json_encode(array("success" => false, "msg" => "Selected Document Not found!"));
                                exit;
                            }
                        } else {
                            // this is for sub document type 
                            $condition = "sub_document_type_id = '" . $key . "' ";
                            $document_sub_type_details = $this->common->getData("tbl_sub_document_type", '*', $condition);

                            $document_name = $document_sub_type_details[0]['sub_document_prefix_name'];

                            // if($document_name){
                            //     $condition = "sub_document_type_id = '".$document_sub_type_details[0]['sub_document_type_id']."' ";
                            //     $uploaded_document_data = $this->common->getDataLimit("tbl_document_ls_uploaded_files",'*',$condition,"document_uploaded_files_id","desc",'1');
                            // }
                            // if($uploaded_document_data){
                            //     // echo $uploaded_document_data[0]['uploaded_document_number'];
                            //     $uploaded_document_name = ++$uploaded_document_data[0]['uploaded_document_number'];
                            // }else{
                            //     $uploaded_document_name = $document_name;
                            // }
                            // get document sequence for container status 
                            $condition = "document_type_id = '" . $document_sub_type_details[0]['document_type_id'] . "' ";
                            $document_type_details = $this->common->getData("tbl_document_type", '*', $condition);

                            // echo "<pre>";
                            // print_r($uploaded_document_type_id);
                            // print_r($uploaded_document_file_name);
                            // exit; 
                            // get container status for document uploaded 
                            $condition = " document_type_id = '" . $document_type_details[0]['document_type_id'] . "' ";
                            $getContainerStatus = $this->common->getData('tbl_container_status', '*', $condition);


                            if ($document_name) {
                                if (!empty($_POST['selectedContainer']) && isset($_POST['selectedContainer'])) {

                                    foreach ($_POST['selectedContainer'] as $containerkey => $containervalue) {
                                        $condition = " container_id = '" . $containervalue . "' ";
                                        $getContainerNumber = $this->common->getData('tbl_container', 'container_number,revised_etd,last_container_status_id', $condition);

                                        $condition = " container_status_id = '" . $getContainerNumber[0]['last_container_status_id'] . "' ";
                                        $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status', 'status_squence', $condition);
                                        $lastContainerStatusSquence = 0;
                                        if (!empty($lastContainerStatusSquenceData)) {
                                            $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                        }

                                        $data['container_id'] = $containervalue;
                                        $data['document_type_id'] = $document_sub_type_details[0]['document_type_id'];
                                        $data['sub_document_type_id'] = $uploaded_document_type_id['sub_document_type_id'][$key];
                                        $data['uploaded_document_file'] = $uploaded_document_file_name['file_name'][$key];
                                        $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $data['uploaded_document_number'] = $document_name . $getContainerNumber[0]['container_number'];
                                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['updated_on'] = date("Y-m-d H:i:s");
                                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['created_on'] = date("Y-m-d H:i:s");
                                        // print_r($data);
                                        $result = $this->common->insertData("tbl_document_ls_uploaded_files", $data, "1");

                                        // insert record in tbl_container_uploaded_document_status for maintain the statsus for container with respective to document uploaded 
                                        $doc_status_data = array();
                                        $doc_status_data['container_id'] = $containervalue;
                                        $doc_status_data['document_type_id'] = $document_type_details[0]['document_type_id'];
                                        $doc_status_data['sub_document_type_id'] = $uploaded_document_type_id['sub_document_type_id'][$key];
                                        $doc_status_data['document_type_squence'] = $document_type_details[0]['document_type_squence'];
                                        $doc_status_data['status'] = 'Unchecked';
                                        $doc_status_data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                        $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                        $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1");

                                        //HBL document upload check
                                        if ($document_type_details[0]['document_type_id'] == 5) {
                                            if ($getContainerStatus && !empty($getContainerNumber[0]['revised_etd'])) {
                                                $ContainerStatus = array();
                                                $ContainerStatus['container_id'] = $containervalue;
                                                $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                                $ContainerStatus['document_date'] = date("Y-m-d");
                                                $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                                if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                                    //last status in container table
                                                    $containerStatusUpdate = array();
                                                    $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $containervalue));
                                                }
                                            }
                                        } else {
                                            if ($getContainerStatus) {
                                                $ContainerStatus = array();
                                                $ContainerStatus['container_id'] = $containervalue;
                                                $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                                $ContainerStatus['document_date'] = date("Y-m-d");
                                                $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                                if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                                    //last status in container table
                                                    $containerStatusUpdate = array();
                                                    $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $containervalue));
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                                    exit;
                                }
                            } else {
                                echo json_encode(array("success" => false, "msg" => "Selected Document Not found!"));
                                exit;
                            }
                        }
                    } // foreach close here

                    if ($result) {
                        echo json_encode(array("success" => true, "msg" => "Document added to container !", 'type' => 'LS', 'step4skip' => $step4skip));
                        exit;
                    } else {
                        echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                        exit;
                    }
                } else {
                    echo json_encode(array("success" => false, "msg" => "Uploaded file is not proper !"));
                    exit;
                }
            } else {
                // this is for fr n ls document type 
                if (isset($_FILES) && isset($_FILES["step3_document"]["name"])) {
                    // $this->upload->initialize($this->set_upload_options($_POST['document_type_id_hidden']));
                    // if (!$this->upload->do_upload("step3_document")) {
                    //     $image_error = array('error' => $this->upload->display_errors());

                    //     echo json_encode(array("success" => false, "msg" => $image_error['error']));
                    //     exit;
                    // } else {
                    //     $image_data = array('upload_data' => $this->upload->data());
                    //     //function for upload file in live bucket
                    //     $full_path = $image_data['upload_data']['full_path'];
                        $resultUpload = set_s3_upload_file($_FILES["step3_document"]["tmp_name"], $_POST['document_type_id_hidden'],$_FILES["step3_document"]["name"]);
                        $step3_document =  basename($resultUpload);
                        // $step3_document = $image_data['upload_data']['file_name'];
                        $data['uploaded_document_file'] = $step3_document;
                        $upload = true;
                    // }
                }

                if ($upload) {
                    // get document selected in previous screen 
                    $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                    $main_table = array("tbl_document_container_mapping as dcm", array("dt.*"));
                    $join_tables = array(
                        array("", "tbl_document_type as dt", "dt.document_type_id = dcm.document_type_id", array("dt.*")),
                    );
                    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                    $document_type_details = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                    $document_name = $document_type_details[0]['document_prefix_name'] . '00001';

                    if ($document_name) {
                        $condition = "document_type_id = '" . $document_type_details[0]['document_type_id'] . "' ";
                        $uploaded_document_data = $this->common->getDataLimit("tbl_document_uploaded_files", '*', $condition, "document_uploaded_files_id", "desc", '1');
                    }

                    if ($uploaded_document_data) {
                        // echo $uploaded_document_data[0]['uploaded_document_number'];
                        $uploaded_document_name = ++$uploaded_document_data[0]['uploaded_document_number'];
                    } else {
                        $uploaded_document_name = $document_name;
                    }

                    // get container status for document uploaded 
                    $condition = " document_type_id = '" . $document_type_details[0]['document_type_id'] . "' ";
                    $getContainerStatus = $this->common->getData('tbl_container_status', '*', $condition);

                    if ($document_name) {
                        if (!empty($_POST['selectedContainer']) && isset($_POST['selectedContainer'])) {


                            foreach ($_POST['selectedContainer'] as $key => $value) {
                                $data['container_id'] = $value;
                                $data['document_type_id'] = $document_type_details[0]['document_type_id'];
                                $data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                $data['uploaded_document_number'] = $uploaded_document_name;
                                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['updated_on'] = date("Y-m-d H:i:s");
                                $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['created_on'] = date("Y-m-d H:i:s");
                                // print_r($data);
                                $result = $this->common->insertData("tbl_document_uploaded_files", $data, "1");

                                // insert record in tbl_container_uploaded_document_status for maintain the statsus for container with respective to document uploaded 
                                $doc_status_data = array();
                                $doc_status_data['container_id'] = $value;
                                $doc_status_data['document_type_id'] = $document_type_details[0]['document_type_id'];
                                $doc_status_data['sub_document_type_id'] = 0;
                                $doc_status_data['document_type_squence'] = $document_type_details[0]['document_type_squence'];
                                $doc_status_data['status'] = 'Unchecked';
                                $doc_status_data['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1");

                                $condition = " container_id = '" . $value . "' ";
                                $getContainerRevised_etd = $this->common->getData('tbl_container', 'revised_etd,last_container_status_id', $condition);

                                $condition = " container_status_id = '" . $getContainerRevised_etd[0]['last_container_status_id'] . "' ";
                                $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status', 'status_squence', $condition);
                                $lastContainerStatusSquence = 0;
                                if (!empty($lastContainerStatusSquenceData)) {
                                    $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                }

                                //HBL document upload check
                                if ($document_type_details[0]['document_type_id'] == 5) {
                                    if ($getContainerStatus && !empty($getContainerRevised_etd[0]['revised_etd'])) {
                                        $ContainerStatus = array();
                                        $ContainerStatus['container_id'] = $value;
                                        $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                        $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                        $ContainerStatus['document_date'] = date("Y-m-d");
                                        $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                        if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                            //last status in container table
                                            $containerStatusUpdate = array();
                                            $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                            $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                            $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                            $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $value));
                                        }
                                    }
                                } else {
                                    if ($getContainerStatus) {
                                        $ContainerStatus = array();
                                        $ContainerStatus['container_id'] = $value;
                                        $ContainerStatus['container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                        $ContainerStatus['current_session_id'] = $_SESSION['mro_session'][0]['current_session_id'];
                                        $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                        $ContainerStatus['document_date'] = date("Y-m-d");
                                        $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");

                                        if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                                            //last status in container table
                                            $containerStatusUpdate = array();
                                            $containerStatusUpdate['last_container_status_id'] = $getContainerStatus[0]['container_status_id'];
                                            $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                            $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                            $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $value));
                                        }
                                    }
                                }
                            }

                            if ($result) {
                                echo json_encode(array("success" => true, "msg" => "Document added to container!", 'type' => '', 'step4skip' => $step4skip));
                                exit;
                            } else {
                                echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                                exit;
                            }
                        } else {
                            echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                            exit;
                        }
                    } else {
                        echo json_encode(array("success" => false, "msg" => "Selected Document Not found!"));
                        exit;
                    }
                } else {
                    echo json_encode(array("success" => false, "msg" => "Uploaded file is not proper !"));
                    exit;
                }
            }
        }
    }

    public function getSelectedContainerDocument() {
        // echo "<pre>";
        // print_r($_POST);
        // exit;
        if ($_POST['type'] == 'LS') {

            if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {
                $condition = "1=1  AND dluf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.*,group_concat(dluf.uploaded_document_number) as uploaded_document_number"));
                $join_tables = array(
                    array("", "tbl_container as c", "dluf.container_id = c.container_id", array("c.container_number, c.container_id")),
                    array("", "tbl_document_type as dt", "dt.document_type_id = dluf.document_type_id", array("dt.document_prefix_name")),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'dluf.container_id');
                $result['getSelectedLsContainerDocument'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                // echo "<pre>";
                // print_r($result);
                // exit;
                if ($result) {
                    $html_container = '';
                    $html_uploaded = '';
                    $document_prefix_name = '';
                    foreach ($result['getSelectedLsContainerDocument'] as $key => $value) {
                        $document_prefix_name = $value['document_prefix_name'];
                        $html_container .= ' <div class="ud-list-single">
                                        <label class="container-checkbox">' . $value['container_number'] . '
                                        </label>
                                    </div>';

                        $html_uploaded .= '<div class="ud-list-single">
                                            <label class="container-checkbox">' . str_replace(',', ' ', $value['uploaded_document_number']) . '
                                            </label>
                                        </div>';
                    }

                    echo json_encode(array("success" => true, "msg" => "", 'html_container' => $html_container, 'html_uploaded' => $html_uploaded, 'document_prefix_name' => $document_prefix_name));
                    exit;
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "Current session id not found "));
                    exit;
                }
            } else {
                echo json_encode(array("success" => false, "msg" => "Current session id not found "));
                exit;
            }
        } else {

            if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {
                $condition = "1=1  AND duf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_document_uploaded_files as duf", array("duf.*"));
                $join_tables = array(
                    array("", "tbl_container as c", "duf.container_id = c.container_id", array("c.container_number, c.container_id")),
                    array("", "tbl_document_type as dt", "dt.document_type_id = duf.document_type_id", array("dt.document_prefix_name")),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                $result['getSelectedContainerDocument'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                if ($result) {
                    $html_container = '';
                    $html_uploaded = '';
                    $document_prefix_name = '';
                    foreach ($result['getSelectedContainerDocument'] as $key => $value) {
                        $document_prefix_name = $value['document_prefix_name'];

                        $html_container .= ' <div class="ud-list-single">
                                        <label class="container-checkbox">' . $value['container_number'] . '
                                        </label>
                                    </div>';

                        $html_uploaded .= '<div class="ud-list-single">
                                            <label class="container-checkbox">' . str_replace(',', ' ', $value['uploaded_document_number']) . '
                                            </label>
                                        </div>';
                    }

                    echo json_encode(array("success" => true, "msg" => "", 'html_container' => $html_container, 'html_uploaded' => $html_uploaded, 'document_prefix_name' => $document_prefix_name));
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "Current session id not found "));
                    exit;
                }
            } else {
                echo json_encode(array("success" => false, "msg" => "Current session id not found "));
                exit;
            }
        }
    }

    public function set_upload_options($documentTypeId = 0, $file_name = "") {
        $config = array();
        if (!empty($file_name) && $file_name != "") {
            $config['file_name'] = $file_name . time();
        }

        $docPath = getDocumentFolder($documentTypeId);
//                if($_SERVER['HTTP_HOST']=='localhost'){
//                    $config['upload_path'] = DOC_ROOT_FRONT."/container_document/".$docPath;
//                }else{
//                    $config['upload_path'] = DOC_ROOT_FRONT."/demo/container_document/".$docPath;
//                } 
        $config['upload_path'] = DOC_ROOT_FRONT . "/container_document/" . $docPath;
        if (!is_dir($config['upload_path'])) {
            mkdir($config['upload_path'], 0777, true);
        }
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '25000';
        $config['overwrite'] = FALSE;
        // $config['min_width']            = 1000;
        // $config['min_height']           = 1000;
        return $config;
    }

    public function getSelectedCustomField() {

        // echo "<pre>";
        // print_r($_POST);
        // exit;
        $formView = '';
        if ($_POST['type'] == 'invoice') {

            if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {
                // get partner name 
                $condition = "1=1  AND iuf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $main_table = array("tbl_invoice_uploaded_files as iuf", array("iuf.*"));
                $join_tables = array(
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = iuf.business_partner_id", array('bp.alias')),
                );

                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, '', 'iuf.current_session_id');
                $result['getCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                // get sku data
                $condition = "status = 'Active' ";
                $result['sku_details'] = $this->common->getData('tbl_sku', 'sku_id,sku_number', $condition);

                if ($result) {
                    $customField = $this->load->view("invoice-customer-field", $result, true);
                }

                if ($result) {
                    $formView = $this->load->view("invoice-form-view", $result, true);
                }

                echo json_encode(array("success" => true, "msg" => "Custom field data found ", 'customField' => $customField, 'formView' => $formView));
                exit;
            } else {
                echo json_encode(array("success" => false, "msg" => "Session data in found"));
                exit;
            }
        } else {
            if ($_POST['type'] == 'LS') {

                if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {
                    $condition = "1=1  AND dluf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' AND cfs.status = 'Active' ";
                    $main_table = array("tbl_custom_field_structure as cfs", array("cfs.*"));
                    $join_tables = array(
                        array("", "tbl_document_ls_uploaded_files as dluf", "dluf.document_type_id = cfs.document_type_id", array('group_concat(DISTINCT(dluf.uploaded_document_number)) as uploaded_document_number,uploaded_document_file ')),
                        array("left", "tbl_sub_document_type as sdt", "sdt.document_type_id = dluf.document_type_id", array('group_concat(DISTINCT(sdt.sub_document_type_id)) as sub_document_type_id ')),
                    );

                    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'custom_field_structure_id');
                    $result['getSelectedCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                    $result['containersSKUData'] = $this->getcontainersSKUData($_POST['type'], $_SESSION['mro_session'][0]['current_session_id']);
                    $customField = '';
                    $formView = '';


                    // get the manufacturer list 
                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category ='Manufacturer'  ";
                    $result['manufacturer'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number', $condition);

                    // get the supplier name  
                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Distributor','Manufacturer')  ";
                    $result['supplier_name'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number', $condition);


                    //  get hbl_number 
                    $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "'  ";
                    $get_container_id = $this->common->getData('tbl_document_ls_uploaded_files', 'container_id,document_type_id', $condition);

                    //supplier_update
                    $result['supplier_update'] = '';
                    if ($get_container_id) {
                        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND supplier_id IS NOT NULL";
                        $supplier_update = $this->common->getData('tbl_custom_dynamic_data', 'supplier_id,supplier_contactperson,supplier_contactnumber', $condition, 'custom_dynamic_data_id', 'desc');
                        if (!empty($supplier_update)) {
                            $result['supplier_update'] = $supplier_update;
                        }
                    }

                    $result['all_freight_forwarder'] = array();

                    if ($get_container_id) {
                        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND hbl_number IS NOT NULL";
                        $result['get_hbl_number'] = $this->common->getData('tbl_custom_dynamic_data', 'hbl_number', $condition, 'custom_dynamic_data_id', 'desc');

                        $condition = "1=1  AND c.container_id = " . $get_container_id[0]['container_id'] . " ";
                        $main_table = array("tbl_container as c", array());
                        $join_tables = array(
                            array("", "tbl_order as o", "o.order_id = c.order_id", array('o.freight_forwarder_id')),
                        );
                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
                        $orderData = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                        if (!empty($orderData)) {
                            $condition = "status = 'Active' And business_partner_id IN (" . $orderData[0]['freight_forwarder_id'] . ")  ";
                            $result['all_freight_forwarder'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,alias', $condition);
                        }
                    }

//                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category ='Freight Forwarder'  ";
//                    $result['all_freight_forwarder'] = $this->common->getData('tbl_business_partner','business_partner_id,business_name,alias',$condition); 
                    // get Port of Loading;
                    $condition = "status = 'Active' ";
                    $result['all_pol'] = $this->common->getData('tbl_pol', '*', $condition);
                    // get Port of Discharge;
                    $condition = "status = 'Active' ";
                    $result['all_pod'] = $this->common->getData('tbl_pod', '*', $condition);
                    //get liner name
                    $condition = "status = 'Active' ";
                    $result['all_liner'] = $this->common->getData('tbl_liner', '*', $condition);

                    // get the supplier name  
                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Freight Forwarder')  ";
                    $result['all_ff'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,alias', $condition);

                    // echo "<pre>";
                    // print_r($result['get_hbl_number']);
                    // exit;

                    if (!empty($result['getSelectedCustomField'][0]['document_type_id'])) {
                        if ($result['getSelectedCustomField'][0]['document_type_id'] == 4) {
                            $customField = $this->load->view("custom-fcr", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 5) {
                            $customField = $this->load->view("custom-hbl", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 1) {
                            $customField = $this->load->view("custom-ins", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 10) {
                            $customField = $this->load->view("custom-arrival", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 7) {
                            $customField = $this->load->view("7501", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 8) {
                            $customField = $this->load->view("custom-do", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 9) {
                            $customField = $this->load->view("custom-do", $result, true);
                        }
                    }

                    // to get the  form view for both internal n external 
                    if ($result) {

                        $condition = "1=1  AND dluf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                        $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.*"));
                        $join_tables = array(
                            array("", "tbl_document_type as dt", "dt.document_type_id = dluf.document_type_id", array('dt.document_type_name as document_name')),
                            array("left", "tbl_sub_document_type as sdt", "sdt.document_type_id = dt.document_type_id && sdt.sub_document_type_id = dluf.sub_document_type_id", array('sdt.sub_document_type_name as sub_document_name')),
                        );

                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'document_type_id,sub_document_type_id');
                        $result['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                        // echo "<pre>";
                        // print_r( $result['formView']);
                        // exit;
                        $formView = $this->load->view("document-form-view", $result, true);
                    }

                    echo json_encode(array("success" => true, "msg" => "Custom field data found ", 'customField' => $customField, 'formView' => $formView));
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "Document not selected for it"));
                    exit;
                }
            } else {

                if (!empty($_SESSION['mro_session'][0]['current_session_id']) && isset($_SESSION['mro_session'][0]['current_session_id'])) {
                    $condition = "1=1  AND duf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' AND cfs.status = 'Active' ";
                    $main_table = array("tbl_custom_field_structure as cfs", array("cfs.*"));
                    $join_tables = array(
                        array("", "tbl_document_uploaded_files as duf", "duf.document_type_id = cfs.document_type_id", array(' group_concat(DISTINCT(duf.uploaded_document_number)) as uploaded_document_number')),
                    );

                    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'custom_field_structure_id');
                    $result['getSelectedCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result                   

                    $result['containersSKUData'] = $this->getcontainersSKUData($_POST['type'], $_SESSION['mro_session'][0]['current_session_id']);


                    // get the manufacturer list 
                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category ='Manufacturer'  ";
                    $result['manufacturer'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number,alias', $condition);

                    // get the supplier name  
                    $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Distributor','Manufacturer')  ";
                    $result['supplier_name'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number,alias', $condition);

                    //get hbl_number 
                    $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "'  ";
                    $get_container_id = $this->common->getData('tbl_document_uploaded_files', 'container_id,document_type_id', $condition);

                    //supplier_update
                    $result['supplier_update'] = '';
                    if ($get_container_id) {
                        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND supplier_id IS NOT NULL";
                        $supplier_update = $this->common->getData('tbl_custom_dynamic_data', 'supplier_id,supplier_contactperson,supplier_contactnumber', $condition, 'custom_dynamic_data_id', 'desc');
                        if (!empty($supplier_update)) {
                            $result['supplier_update'] = $supplier_update;
                        }
                    }

                    if ($get_container_id) {
                        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND document_type_id =".$result['getSelectedCustomField'][0]['document_type_id']." AND date_of_inspection IS NOT NULL";
                        $result['get_date_of_inspection'] = $this->common->getData('tbl_custom_dynamic_data', 'date_of_inspection', $condition, 'custom_dynamic_data_id', 'desc');
                    }

                    if ($get_container_id) {
                        $condition = "container_id = '" . $get_container_id[0]['container_id'] . "' AND document_type_id =".$result['getSelectedCustomField'][0]['document_type_id']." AND place_of_inspection IS NOT NULL";
                        $result['get_place_of_inspection'] = $this->common->getData('tbl_custom_dynamic_data', 'place_of_inspection', $condition, 'custom_dynamic_data_id', 'desc');
                    }

                    // get Port of Loading;
                    $condition = "status = 'Active' ";
                    $result['all_pol'] = $this->common->getData('tbl_pol', '*', $condition);
                    // get Port of Discharge;
                    $condition = "status = 'Active' ";
                    $result['all_pod'] = $this->common->getData('tbl_pod', '*', $condition);
                    //get liner name
                    $condition = "status = 'Active' ";
                    $result['all_liner'] = $this->common->getData('tbl_liner', '*', $condition);

                    // echo "<pre>";
                    // print_r($result['containersSKUData']);
                    // exit;

                    $customField = '';
                    if ($result) {
                        if ($result['getSelectedCustomField'][0]['document_type_id'] == 3) {
                            $customField = $this->load->view("custom-fri", $result, true);
                        } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 2) {
                            $customField = $this->load->view("custom-ls", $result, true);
                        }
                    }

                    // to get the  form view for both internal n external 
                    if ($result) {

                        $condition = "1=1  AND duf.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                        $main_table = array("tbl_document_uploaded_files as duf", array("duf.*"));
                        $join_tables = array(
                            array("", "tbl_document_type as dt", "dt.document_type_id = duf.document_type_id", array('dt.document_type_name as document_name')),
                        );

                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'document_type_id');
                        $result['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                        $formView = $this->load->view("document-form-view", $result, true);
                    }

                    echo json_encode(array("success" => true, "msg" => "Custom field data found ", 'customField' => $customField, 'formView' => $formView));
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "Document not selected for it"));
                    exit;
                }
            }
        }
    }

    public function submitFormStep5() {
//         echo "<pre>";
//         print_r($_POST);
//         die;
//        echo !empty($_POST['eta'][10]) ? date("Y-m-d", strtotime($_POST['eta'][10])) : NULL;die;
        //code by ravendra this is use for back and then save record functionality.
        $this->common->deleteRecord("tbl_custom_field_data_submission", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_custom_field_invoice_data_submission", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_custom_field_invoice_data_submission_sku", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");


        // for invoice custom field data store in tbl_invoice_custom_field_response_store
        if (!empty($_POST['business_partner_id']) && isset($_POST['business_partner_id'])) {
            $data = array();

            if ($_POST['invoice_file_name']) {
                $renamed_file_name = explode('.', $_POST['invoice_file_name']);
                $file_name = $renamed_file_name[0];
            }
            $newinvoice_name = $file_name . '.pdf';
            $data['business_partner_id'] = $_POST['business_partner_id'];
            $data['invoice_file_name'] = $newinvoice_name;
            $data['invoice_value'] = $_POST['invoice_value'];
            $data['invoice_date'] = !empty($_POST['invoice_date']) ? date("Y-m-d", strtotime($_POST['invoice_date'])) : NULL;
            $data['invoice_status'] = $_POST['invoice_status'];
            $data['paid_date'] = !empty($_POST['paid_date']) ? date("Y-m-d", strtotime($_POST['paid_date'])) : NULL;
            // $data['sku_id']=  $_POST['sku_id'] ;
            // $data['quantity']=  $_POST['quantity'] ;
            // $data['price_per_unit']=  $_POST['price_per_unit'] ;
            $data['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s");
            $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['created_on'] = date("Y-m-d H:i:s");

            $result = $this->common->insertData("tbl_custom_field_invoice_data_submission", $data, "1");

            if ($result) {
                $custom_field_invoice_data_submission_id = $result;
                if (!empty($_POST['sku']) && isset($_POST['sku'])) {

                    foreach ($_POST['sku'] as $key => $value) {
                        $data = array();
                        $data['sku_id'] = $value;
                        $data['custom_field_invoice_data_submission_id'] = $custom_field_invoice_data_submission_id;
                        $data['quantity'] = $_POST['quantity'][$key];
                        $data['price_per_unit'] = $_POST['price_per_unit'][$key];
                        $data['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_field_invoice_data_submission_sku", $data, "1");
                    }
                }
                // check the pdf with old name id exist then remove it and move that file with new name 
                $old_invoice_file_name = 'container_document/' . $_POST['old_invoice_file_name'];
//                if (file_exists($old_invoice_file_name)) {
//                    $upload_path = DOC_ROOT_FRONT . "/container_document";
//                    rename($old_invoice_file_name, "container_document/$newinvoice_name");
                    // update the new file in tbl_invoice_uploaded_files
                    $data = array();
                    $data['uploaded_invoice_file'] = $_POST['old_invoice_file_name']; //$newinvoice_name;
                    $condition = " current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                    $result = $this->common->updateData("tbl_invoice_uploaded_files", $data, $condition);

                    if ($result) {
                        echo json_encode(array("success" => true, "msg" => "Invoice successfully uploaded in container!", "type" => "invoice"));
                        exit;
                    } else {
                        echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                        exit;
                    }
//                } else {
//                    echo "The file $old_invoice_file_name does not exist!!";
//                }
                exit;
            }
        }

        // cutomer field compulsory removed 
        if (!empty($_POST['custom_field_structure_id']) && isset($_POST['custom_field_structure_id'])) {
            $uploaded_document_number = explode(',', $_POST['uploaded_document_number']);
            $actualDateofArrival = '';
            if (!empty($_POST['sub_document_type_id']) && isset($_POST['sub_document_type_id'])) {
                $sub_document_type_cnt = explode(',', $_POST['sub_document_type_id']);

                foreach ($sub_document_type_cnt as $subDocumentkey => $subDocumentval) {
                    foreach ($_POST['custom_field_structure_id'] as $key => $value) {
                        $data['custom_field_structure_id'] = $value;
                        $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                        $data['uploaded_document_number'] = $uploaded_document_number[$subDocumentkey];
                        $data['document_type_id'] = (!empty($_POST['document_type_id']) ? $_POST['document_type_id'] : " ");
                        $data['sub_document_type_id'] = $subDocumentval;
                        $data['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s");
                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['created_on'] = date("Y-m-d H:i:s");
                        $result = $this->common->insertData("tbl_custom_field_data_submission", $data, "1");
                    }
                }
            } else {

                foreach ($_POST['custom_field_structure_id'] as $key => $value) {
                    //Actual date of Arrival check for DO upload
                    if(isset($_POST['dateOfArrival'])){
                        if($value == 92){
                            $actualDateofArrival = (!empty($_POST[$value]) ? date('Y-m-d', strtotime($_POST[$value])) : "");
                        }
                    }
                    $data['custom_field_structure_id'] = $value;
                    $data['uploaded_document_number'] = $uploaded_document_number[0];
                    $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                    $data['document_type_id'] = (!empty($_POST['document_type_id']) ? $_POST['document_type_id'] : " ");
                    $data['sub_document_type_id'] = (!empty($_POST['sub_document_type_id']) ? $_POST['sub_document_type_id'] : "0");
                    $data['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);
                    $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['updated_on'] = date("Y-m-d H:i:s");
                    $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                    $data['created_on'] = date("Y-m-d H:i:s");
                    $result = $this->common->insertData("tbl_custom_field_data_submission", $data, "1");
                }
            }

            // start  code modified by shiv on 21-10-21 for document type FRI N LS
            if (!empty($_POST['container_sku_id']) && isset($_POST['container_sku_id'])) {
                foreach ($_POST['container_sku_id'] as $key => $container_val) {
                    // $key == container id here
                    foreach ($container_val as $sku_key => $sku_val) {
                        $data1 = array();
                        $data1['quantity'] = $_POST['container_sku_qty'][$key][$sku_key];
                        $condition = "container_sku_id = " . $sku_key . " AND container_id = " . $key . " ";
                        $this->common->updateData("tbl_container_sku", $data1, $condition);

                        $data_manu = array();
                        $data_manu['manufacturer_id'] = $_POST['manufacturer'][$key][$sku_key];
                        $condition = "sku_id = " . $sku_key . "  ";
                        $this->common->updateData("tbl_sku", $data_manu, $condition);
                    }
                }
            }

            if (!empty($_POST['supplier_name']) && isset($_POST['supplier_name'])) {
                foreach ($_POST['supplier_name'] as $supp_key => $supp_val) {
                    foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                        $data_supp = array();
                        $data_supp['supplier_id '] = $supp_val;
                        $data_supp['supplier_contactperson'] = $_POST['contactpersonname'][$supp_key];
                        $data_supp['supplier_contactnumber'] = $_POST['contactpersonnumber'][$supp_key];
                        // $data_supp['sub_document_type_id'] = explode(',',$_POST['sub_document_type_id']);
                        $data_supp['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);

                        $condition = " container_id = " . $container_id . " AND document_type_id =".$_POST['document_type_id']." ";
                        $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                        if (!empty($dynamic_data)) {
                            $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$_POST['document_type_id']." ";
                            $data_supp['updated_on'] = date("Y-m-d H:i:s");
                            $data_supp['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition);
                        } else {
                            $data_supp['container_id'] = $container_id;
                            $data_supp['document_type_id'] = $_POST['document_type_id'];
                            $data_supp['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data_supp['updated_on'] = date("Y-m-d H:i:s");
                            $data_supp['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data_supp['created_on'] = date("Y-m-d H:i:s");
                            $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                        }
                    }
                }
            }

            if (!empty($_POST['lot_number']) && isset($_POST['lot_number'])) {
                foreach ($_POST['lot_number'] as $seal_key => $seal_val) {
                    $data_lot = array();
                    $data_lot['lot_number'] = $_POST['lot_number'][$seal_key];
                    $condition = " container_id = " . $seal_key . " ";
                    $this->common->updateData("tbl_container", $data_lot, $condition);
                }
            }

            if (!empty($_POST['sgs_seal']) && isset($_POST['sgs_seal'])) {
                foreach ($_POST['sgs_seal'] as $seal_key => $seal_val) {
                    $data_seal = array();
                    $data_seal['sgs_seal'] = $seal_val;
                    $data_seal['shipping_container'] = $_POST['shipping_container'][$seal_key];
                    $data_seal['ff_seal'] = $_POST['ff_seal'][$seal_key];
                    if(isset($_POST['terminal'])){
                        $data_seal['terminal'] = $_POST['terminal'][$seal_key];
                    } 
                    $condition = " container_id = " . $seal_key . " ";
                    $this->common->updateData("tbl_container", $data_seal, $condition);
                }
            }

            // end  code modified by shiv on 21-10-21 for document type FRI N LS
            // for HBL N FCR document type 
            if (!empty($_POST['tbl_container_id']) && isset($_POST['tbl_container_id'])) {
                foreach ($_POST['tbl_container_id'] as $key => $value) {
                    $data2['freight_forwarder_id'] = $_POST['freight_forwarder_id'][$key];
                    $data2['liner_name'] = $_POST['liner_name'][$key];
                    $data2['pol'] = $_POST['pol'][$key];
                    $data2['pod'] = $_POST['pod'][$key];
                    $data2['etd'] = !empty($_POST['etd'][$key]) ? date("Y-m-d", strtotime($_POST['etd'][$key])) : NULL;
                    $data2['revised_etd'] = !empty($_POST['revised_etd'][$key]) ? date("Y-m-d", strtotime($_POST['revised_etd'][$key])) : NULL;
                    $data2['ett'] = $_POST['ett'][$key];
                    $data2['vessel_name'] = $_POST['vessel_name'][$key];

                    foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                        $condition = " container_id = " . $container_id . " ";
                        $this->common->updateData("tbl_container", $data2, $condition);
                    }
                }
            }
            
            //update actual date of arrival when upload DO then update container revised eta field
            if(!empty($actualDateofArrival)){
                $data_revised_eta = array(); 
                $data_revised_eta['revised_eta'] = !empty($actualDateofArrival) ? $actualDateofArrival : NULL;
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $condition = " container_id = " . $container_id . " ";
                    $this->common->updateData("tbl_container", $data_revised_eta, $condition);
                }
            }

            if (!empty($_POST['revised_eta']) && isset($_POST['revised_eta'])) {
                foreach ($_POST['revised_eta'] as $eta_key => $eta_val) {
                    $data_eta = array();
                    $data_eta['etd'] = !empty($_POST['etd'][$eta_key]) ? date("Y-m-d", strtotime($_POST['etd'][$eta_key])) : NULL;
                    $data_eta['eta'] = !empty($_POST['eta'][$eta_key]) ? date("Y-m-d", strtotime($_POST['eta'][$eta_key])) : NULL;
                    $data_eta['revised_eta'] = !empty($eta_val) ? date("Y-m-d", strtotime($eta_val)) : NULL;
                    $condition = " container_id = " . $eta_key . " ";
                    //print_r($data_eta); die; 
                    foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                        $condition = " container_id = " . $container_id . " ";
                        $this->common->updateData("tbl_container", $data_eta, $condition);
                    }
                }
            }

            // start to remove unchecked container id from mappings table in ls n without ls code by shiv on 6-10-21

            if (!empty($_POST['tbl_container_id']) && isset($_POST['tbl_container_id']) && !empty($_POST['selected_container_id']) && isset($_POST['selected_container_id'])) {
                $containerToBeDeleted = array_diff($_POST['tbl_container_id'], $_POST['selected_container_id']);
                if ($containerToBeDeleted) {
                    foreach ($containerToBeDeleted as $key => $value) {
                        # delete the record which container is unselected at the last step5 
                        $condition = "container_id = '" . $value . "' AND document_type_id = '" . $_POST['document_type_id'] . "' AND current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                        $this->common->deleteRecord('tbl_document_container_mapping', $condition);
                        $this->common->deleteRecord('tbl_document_ls_container_mapping', $condition);
                        $this->common->deleteRecord('tbl_document_uploaded_files', $condition);
                        $this->common->deleteRecord('tbl_document_ls_uploaded_files', $condition);
                        $this->common->deleteRecord('tbl_container_uploaded_document_status', $condition);
                    }
                }
            }
            //end  to remove unchecked container id from mappings table in ls n without ls code by shiv on 6-10-21  
            //  get mapped container data
            // $_SESSION['mro_session'][0]['current_session_id']) 
//            $condition = "current_session_id = '".$_SESSION['mro_session'][0]['current_session_id']."' AND document_type_id = '".$_POST['document_type_id']."' ";
//            $get_mapped_container = $this->common->getData('tbl_document_ls_container_mapping','container_id',$condition);
            // for hbl 
            if (!empty($_POST['hbl_number']) && isset($_POST['hbl_number'])) {
//                if(!empty($get_mapped_container) && isset($get_mapped_container)){
//                    foreach ($get_mapped_container as $hbl_key => $hbl_val) { 
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $data_hbl = array();
                    $data_hbl['hbl_number'] = $_POST['hbl_number'];
                    // $data_hbl['sub_document_type_id'] = explode(',',$_POST['sub_document_type_id']);
                    $data_hbl['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);

                    $condition = " container_id = " . $container_id . " AND document_type_id =".$_POST['document_type_id']." ";
                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                    if (!empty($dynamic_data)) {
                        $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$_POST['document_type_id']." ";
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $this->common->updateData("tbl_custom_dynamic_data", $data_hbl, $condition);
                    } else {
                        $data_hbl['container_id'] = $container_id;
                        $data_hbl['document_type_id'] = $_POST['document_type_id'];
                        $data_hbl['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_dynamic_data", $data_hbl, "1");
                    }
                }
//                    }
//                } 
            }

            if (!empty($_POST['date_of_inspection']) && isset($_POST['date_of_inspection'])) {
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $data_hbl = array();
                    $data_hbl['date_of_inspection'] = !empty($_POST['date_of_inspection']) ? date("Y-m-d", strtotime($_POST['date_of_inspection'])) : NULL;
                    $data_hbl['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);

                    $condition = " container_id = " . $container_id . " AND document_type_id =".$_POST['document_type_id']." ";
                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                    if (!empty($dynamic_data)) {
                        $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$_POST['document_type_id']." ";
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $this->common->updateData("tbl_custom_dynamic_data", $data_hbl, $condition);
                    } else {
                        $data_hbl['container_id'] = $container_id;
                        $data_hbl['document_type_id'] = $_POST['document_type_id'];
                        $data_hbl['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_dynamic_data", $data_hbl, "1");
                    }
                }
            }

            if (!empty($_POST['place_of_inspection']) && isset($_POST['place_of_inspection'])) {
                foreach ($_POST['all_container_id'] as $cont_key => $container_id) {
                    $data_hbl = array();
                    $data_hbl['place_of_inspection'] = $_POST['place_of_inspection'];
                    $data_hbl['current_session_id'] = ($_SESSION['mro_session'][0]['current_session_id']);

                    $condition = " container_id = " . $container_id . " AND document_type_id =".$_POST['document_type_id']." ";
                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                    if (!empty($dynamic_data)) {
                        $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$_POST['document_type_id']." ";
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $this->common->updateData("tbl_custom_dynamic_data", $data_hbl, $condition);
                    } else {
                        $data_hbl['container_id'] = $container_id;
                        $data_hbl['document_type_id'] = $_POST['document_type_id'];
                        $data_hbl['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                        $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data_hbl['created_on'] = date("Y-m-d H:i:s");
                        $this->common->insertData("tbl_custom_dynamic_data", $data_hbl, "1");
                    }
                }
            }

            //code update date of document in container status log 
            $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND cfds.current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
            $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));
            $join_tables = array(
                array("", "tbl_custom_field_structure as cfs", "cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, '', '');
            $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
            if (!empty($dateOfDocumentData)) {
                $data_doc_date = array();
                $condition = " current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
                $data_doc_date['document_date'] = date("Y-m-d", strtotime($dateOfDocumentData[0]['custom_field_structure_value']));
                $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);
            }

//            //container status to Goods Received in port.
//            foreach ($_POST['all_container_id'] as $cont_key => $container_id) { 
//                $condition = " container_id = ".$container_id." "; 
//                $containerETA = $this->common->getData('tbl_container','eta',$condition); 
//                if(!empty($containerETA)){ 
//                    //If ETA is before Today () then auto change Status to Goods Received in port.
//                    $etaDate = date('Y-m-d', strtotime($containerETA[0]['eta']));
//                    if(strtotime($etaDate) < strtotime(date("Y-m-d"))){
//                        $ContainerStatus =array();
//                        $ContainerStatus['container_id'] = $container_id;
//                        $ContainerStatus['container_status_id'] = 7;
//                        $ContainerStatus['current_session_id'] =   $_SESSION['mro_session'][0]['current_session_id'];
//                        $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
//                        $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
//                        $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1");
//                    }
//                } 
//            }

            if ($result) {
                echo json_encode(array("success" => true, "msg" => "Document successfully uploaded in container!", "type" => "document"));
                exit;
            } else {
                echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                exit;
            }
        } else {
            // send response without storing data into tble  as we are doing above
            echo json_encode(array("success" => true, "msg" => "Document successfully uploaded in container!", "type" => "document"));
            exit;
        }
    }

    public function getLsListing() {

        $condition = " 1=1 AND document_type_id = '2' ";
        $rs = $this->common->Fetch('tbl_document_uploaded_files', 'uploaded_document_number', $condition, '', 'uploaded_document_number');
        $result['LsNumberLisitng'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
        // echo "<pre>";
        // print_r($result);
        // exit;

        $html = "";
        $rightSideMsg = '';
        $rightSideMsg = '<div class="sd-right" id="containterListingForOtherDocument">
                            <p class="select-message">Choose LS from the left in order to choose containers
                            </p>
                        </div>';
        if ($result) {
            $html = $this->load->view("Ls-ListingView", $result, true);
            echo json_encode(array("success" => true, "msg" => "Ls Listing found", 'html' => $html, 'rightSideMsg' => $rightSideMsg));
            exit;
        } else {
            echo json_encode(array("success" => false, "msg" => "NO LS listing in created so far", 'html' => $html, "rightSideMsg" => $rightSideMsg));
            exit;
        }
    }

    public function getContainerListing() {

        $condition = "status = 'Active' AND container_id not in (select distinct(container_id) as container_id from tbl_document_uploaded_files where document_type_id = " . $_POST['document_type_id'] . " )";

        $result['containerListing'] = $this->common->getData('tbl_container', 'container_number,container_id', $condition);

        $html = "";
        $rightSideMsg = '';
        $rightSideMsg = '<div class="sd-right" id="containterListingForOtherDocument">
                            <p class="select-message">Choose containers to upload documents</p>
                        </div>';
        if ($result) {
            $html = $this->load->view("containerListingView", $result, true);
            echo json_encode(array("success" => true, "msg" => "Countainer Listing found", 'html' => $html, 'rightSideMsg' => $rightSideMsg));
            exit;
        } else {
            echo json_encode(array("success" => false, "msg" => "NO counter listing in created so far", 'html' => $html, "rightSideMsg" => $rightSideMsg));
            exit;
        }
    }

    public function getLSContainerListing() {

        if (!empty($_POST['document_type_id']) && isset($_POST['document_type_id'])) {
            $condition = "1=1  AND duf.uploaded_document_number = '" . $_POST['val'] . "' AND duf.container_id not in (select distinct(container_id) as container_id from tbl_document_ls_uploaded_files where document_type_id = " . $_POST['document_type_id'] . ") ";
        } else {
            $condition = "1=1  AND duf.uploaded_document_number = '" . $_POST['val'] . "'  ";
        }

        $main_table = array("tbl_document_uploaded_files as duf", array("duf.uploaded_document_number"));
        $join_tables = array(
            array("", "tbl_container as c", "duf.container_id = c.container_id", array("c.container_number,c.container_id")),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
        $result['LscontainerListing'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
        // echo "<pre>";
        // print_r($result);
        // exit;
        $html = '';
        if ($result) {
            $html = $this->load->view("LscontainerListingView", $result, true);
            echo json_encode(array("success" => true, "msg" => "Countainer Listing found", 'html' => $html));
            exit;
        } else {
            echo json_encode(array("success" => true, "msg" => "Custom field data found ", 'html' => $html));
            exit;
        }
    }

    public function getsku() {

        $condition = " s.status = 'Active' AND si.status = 'Active' AND sku_id = '" . $_POST['sku_id'] . "' ";
        $main_table = array("tbl_sku as s", array("s.*"));
        $join_tables = array(
            array("", "tbl_size as si", "si.size_id = s.size_id", array('si.size_name')),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, '', 's.sku_id');
        $result['getsku'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
        // echo $this->db->last_query();
        // echo "<pre>";
        // print_r($getsku);
        // exit;
        $item_code = '';
        $item_size = '';
        $product_desc = '';
        $htmlsku = '';
        if ($result['getsku']) {

            $htmlsku = $this->load->view("invoice_sku", $result, true);
            // $item_code =  $getsku[0]['sku_number'];
            // $item_size = $getsku[0]['size_name'];
            // $product_desc = $getsku[0]['product_name'];
            echo json_encode(array("success" => true, "msg" => "SKU data found", 'htmlsku' => $htmlsku));
            exit;
        } else {
            echo json_encode(array("success" => false, "msg" => "SKU data found", 'htmlsku' => $htmlsku));
            exit;
        }
    }

    function getcontainersSKUData($documentType = '', $sessionId = '') {
        if ($documentType == 'LS') {
            $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.container_id,dluf.document_type_id"));
        } else {
            $main_table = array("tbl_document_uploaded_files as dluf", array("dluf.container_id,dluf.document_type_id"));
        }
        $condition = "1=1  AND dluf.current_session_id = '" . $sessionId . "' ";

        $join_tables = array(
            array("", "tbl_container_sku as csku", "csku.container_id = dluf.container_id", array('csku.*')),
            array("", "tbl_sku as sku", "sku.sku_id = csku.sku_id", array('sku.sku_number,sku.manufacturer_id')),
            array("", "tbl_container as c", "c.container_id = dluf.container_id", array('c.*')),
            array("", "tbl_order as o", "o.order_id = c.order_id", array()),
            array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array()),
            array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias,bp.business_name,bp.business_partner_id')),
            array("left", "tbl_business_partner as ff", "ff.business_partner_id = c.freight_forwarder_id", array('ff.business_partner_id as ff_id, ff.alias as ff_name')),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, '', 'csku.container_sku_id');
        $containerData = $this->common->MySqlFetchRow($rs, "array");
        // echo $this->db->last_query();exit;
        $resultArr = array();
        foreach ($containerData as $value) {
            if ($value['document_type_id'] == '1' || $value['document_type_id'] == '2' || $value['document_type_id'] == '3' || $value['document_type_id'] == '4' || $value['document_type_id'] == '5' || $value['document_type_id'] == '7' || $value['document_type_id'] == '10' || $value['document_type_id'] == '8') {
                $resultArr[$value['container_id']][$value['container_sku_id']] = $value;
            }
        }
        //echo '<pre>'; print_r($resultArr);die;
        return $resultArr;
    }

    public function getPersonDetail() {
        // get business person detail
        $condition = "status = 'Active' AND business_partner_id = " . $_POST['supplier_id'] . " ";
        $supplier_details = $this->common->getData('tbl_business_partner', '*', $condition);
        // echo "<pre>";
        // print_r($supplier_details);
        // exit;
        if ($supplier_details) {
            echo json_encode(array("success" => true, "msg" => "supplier detail found successfully !", "contactpersonname" => $supplier_details[0]['contact_person'], "contactpersonnumber" => $supplier_details[0]['phone_number']));
            exit;
        } else {
            echo json_encode(array("success" => false, "msg" => "supplier detail not found"));
            exit;
        }
    }

    public function deleteUncompletedUpload() {
        $condition = "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ";
        $containerIds = $this->common->getData('tbl_container_status_log', 'container_id', $condition);
        if (!empty($containerIds)) {
            foreach ($containerIds as $value) {
                $containerStatusData = $this->listOfContainermodel->getLastContainerStatus($value['container_id'], $_SESSION['mro_session'][0]['current_session_id']);
                if (!empty($containerStatusData)) {
                    //last status in container table
                    $containerStatusUpdate = array();
                    $containerStatusUpdate['last_container_status_id'] = $containerStatusData[0]['container_status_id'];
                    $containerStatusUpdate['last_status_by'] = $containerStatusData[0]['created_by'];
                    $containerStatusUpdate['last_status_date'] = date('Y-m-d', strtotime($containerStatusData[0]['created_on']));
                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $value['container_id']));
                }
            }
        }

        $this->common->deleteRecord("tbl_document_ls_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_document_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_invoice_ls_container_mapping", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_container_uploaded_document_status", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_custom_field_data_submission", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_custom_field_invoice_data_submission", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_custom_field_invoice_data_submission_sku", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_document_ls_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_document_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_invoice_uploaded_files", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        $this->common->deleteRecord("tbl_container_uploaded_document_status", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");

        $condition = "current_session_id = '" . $_SESSION['mro_session'][0]['current_session_id'] . "' ";
        $statusLog = $this->common->getData("tbl_container_status_log", 'status_log_id', $condition);
        if (!empty($statusLog)) {
            $this->common->deleteRecord("tbl_container_status_log", "current_session_id = " . $_SESSION['mro_session'][0]['current_session_id'] . " ");
        }

        echo json_encode(array("success" => true, "msg" => "unwanted mapping deleted"));
        exit;
    }

    function getLinkContainerIDs($container_id) {
        $LSFRIArr = array();
        $WithOutLSFRIArr = array();
        $linkContainerData = array();
        $linkContainerLSIds = array();
        $linkContainerWithOutLSIds = array();
        $LSArr = array();
        $WithOutLSArr = array();
        $LSFRIDocumentNumber = $this->common->getData("tbl_document_uploaded_files", "uploaded_document_number", array("container_id" => $container_id));
        $WithOutLSFRIDocumentNumber = $this->common->getData("tbl_document_ls_uploaded_files", "uploaded_document_number", array("container_id" => $container_id));

        //LS and FRI number
        if (!empty($LSFRIDocumentNumber)) {
            foreach ($LSFRIDocumentNumber as $value) {
                $LSFRIArr[] = $value['uploaded_document_number'];
            }
        }

        if (!empty($LSFRIArr)) {
            $documentNumberString = implode("','", $LSFRIArr);
            $documentNumberString = "'" . $documentNumberString . "'";
            $linkContainerLSIds = $this->listOfContainermodel->getLinkContainers($documentNumberString, 'LS');
        }

        foreach ($linkContainerLSIds as $value) {
            $LSArr[] = $value['container_id'];
        }

        //without LS and FRI number 
        if (!empty($WithOutLSFRIDocumentNumber)) {
            foreach ($WithOutLSFRIDocumentNumber as $value) {
                $WithOutLSFRIArr[] = $value['uploaded_document_number'];
            }
        }

        if (!empty($WithOutLSFRIArr)) {
            $documentNumberString = implode("','", $WithOutLSFRIArr);
            $documentNumberString = "'" . $documentNumberString . "'";
            $linkContainerWithOutLSIds = $this->listOfContainermodel->getLinkContainers($documentNumberString);
        }

        foreach ($linkContainerWithOutLSIds as $value) {
            $WithOutLSArr[] = $value['container_id'];
        }

        $linkContainerData = array();
        $linkContainerData = array_unique(array_merge($LSArr, $WithOutLSArr), SORT_REGULAR);
        //echo '<pre>'; print_r($linkContainerData); die;
        return $linkContainerData;
    }
    
    function updateContainerStatus(){
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') { 
            $container_ids = $this->input->post('container_ids');
            $container_status_id = $this->input->post('container_status_update_id');  
            
            /* get status_squence of selected container status */
            $condition = " container_status_id = '" . $container_status_id . "' ";
            $getContainerStatus = $this->common->getData('tbl_container_status', 'status_squence', $condition); 
            $result = '';
            if(!empty($container_ids) && !empty($container_status_id)){
                for($i=0;$i<sizeof($container_ids);$i++){   
                    $containerId = $container_ids[$i]; 
                    
                    $condition = " container_id = '" . $containerId . "' ";
                    $getContainerNumber = $this->common->getData('tbl_container', 'last_container_status_id', $condition);
                    
                    /* get status_squence of container */
                    $condition = " container_status_id = '" . $getContainerNumber[0]['last_container_status_id'] . "' ";
                    $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status', 'status_squence', $condition);
                    $lastContainerStatusSquence = 0;
                    if (!empty($lastContainerStatusSquenceData)) {
                        $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                    } 
                    /* check previous status squence should less than from selected status squence*/
                    if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                        $data = array();
                        $data['container_id'] = $containerId;
                        $data['container_status_id'] = $container_status_id;
                        $data['created_on'] = date("Y-m-d H:i:s");
                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['document_date'] = date("Y-m-d");
                        $result = $this->common->insertData('tbl_container_status_log', $data, '1'); 
                        //last status in container table 
                        $containerStatusUpdate = array();
                        $containerStatusUpdate['last_container_status_id'] = $container_status_id;
                        $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                        $result1 = $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $containerId));
                    }
                }
            } 
            if ($result) {
                echo json_encode(array('success' => true, 'msg' => 'Status updated successfully.'));
                exit;
            } else {
                echo json_encode(array('success' => false, 'msg' => 'Select correct container status for selected containers.'));
                exit;
            } 
        }
    }
    
    function multipleUpdateContainers(){  
        $edit_datas = array(); 
        if(isset($_POST['hold_containerIds'])){
            $containerIds = $_POST['hold_containerIds']; 
            $containerIdsArr = explode(',', $containerIds);
            if(!empty($containerIdsArr)){
                sort($containerIdsArr); 
                $container_id = $containerIdsArr[0];   
                $update_shipping_information = $_POST['update_shipping_information']; 

                //get filters data and assign in array with filter title and value
                $filtersData = json_decode($_POST['filters_data'], true); 
                $edit_datas['filter'] = array(); 

                if(!empty($filtersData['container_id'])){
                    $value = $this->common->getData("tbl_container", "container_number", array("container_id" => $filtersData['container_id']));
                    $edit_datas['filter'][] = array("title"=>"Container","value"=> isset($value[0]['container_number'])?$value[0]['container_number']:'');
                } 
                if(!empty($filtersData['status'])){
                    $value = $this->common->getData("tbl_container_status", "container_status_name", array("container_status_id" => $filtersData['status']));
                    $edit_datas['filter'][] = array("title"=>"Status","value"=>isset($value[0]['container_status_name'])?$value[0]['container_status_name']:'');
                } 
                if(!empty($filtersData['fri'])){ 
                    $edit_datas['filter'][] = array("title"=>"FRI","value"=>$filtersData['fri']);
                }
                if(!empty($filtersData['ls'])){ 
                    $edit_datas['filter'][] = array("title"=>"LS","value"=>$filtersData['ls']);
                } 
                if(!empty($filtersData['hbl'])){ 
                    $edit_datas['filter'][] = array("title"=>"HBL","value"=>$filtersData['hbl']);
                }
                if(!empty($filtersData['internal_hbl'])){ 
                    $edit_datas['filter'][] = array("title"=>"Internal HBL","value"=>$filtersData['internal_hbl']);
                } 
                if(!empty($filtersData['internal_fcr'])){ 
                    $edit_datas['filter'][] = array("title"=>"Internal FCR","value"=>$filtersData['internal_fcr']);
                } 
                if(!empty($filtersData['revised_eta'])){ 
                    $edit_datas['filter'][] = array("title"=>"Revised ETA","value"=>date('d-M-y',strtotime($filtersData['revised_eta'])));
                } 
                if(!empty($filtersData['vessel_id'])){ 
                    $edit_datas['filter'][] = array("title"=>"Vessel","value"=>$filtersData['vessel_id']);
                }
                if(!empty($filtersData['manufacturer'])){
                    $value = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $filtersData['manufacturer']));
                    $edit_datas['filter'][] = array("title"=>"Manufacturer","value"=>isset($value[0]['alias'])?$value[0]['alias']:'');
                }  
                if(!empty($filtersData['product_id'])){
                    $value = $this->common->getData("tbl_sku", "product_name", array("sku_id" => $filtersData['product_id']));
                    $edit_datas['filter'][] = array("title"=>"Product","value"=>isset($value[0]['product_name'])?$value[0]['product_name']:'');
                } 
                if(!empty($filtersData['brand_id'])){
                    $value = $this->common->getData("tbl_brand", "brand_name", array("brand_id" => $filtersData['brand_id']));
                    $edit_datas['filter'][] = array("title"=>"Brand","value"=>isset($value[0]['brand_name'])?$value[0]['brand_name']:'');
                }
                if(!empty($filtersData['ff'])){
                    $value = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $filtersData['ff']));
                    $edit_datas['filter'][] = array("title"=>"Freight Forwarder","value"=>isset($value[0]['alias'])?$value[0]['alias']:'');
                }  
                if(!empty($filtersData['bp'])){
                    $value = $this->common->getData("tbl_business_partner", "alias", array("business_partner_id" => $filtersData['bp']));
                    $edit_datas['filter'][] = array("title"=>"Business Partner","value"=>isset($value[0]['alias'])?$value[0]['alias']:'');
                } 
                if(!empty($filtersData['bp_contract'])){
                    $value = $this->common->getData("tbl_bp_order", "contract_number", array("bp_order_id" => $filtersData['bp_contract']));
                    $edit_datas['filter'][] = array("title"=>"BP Contract","value"=>isset($value[0]['contract_number'])?$value[0]['contract_number']:'');
                }  
                if(!empty($filtersData['contract'])){
                    $value = $this->common->getData("tbl_order", "contract_number", array("order_id" => $filtersData['contract']));
                    $edit_datas['filter'][] = array("title"=>"Master Contract","value"=>isset($value[0]['contract_number'])?$value[0]['contract_number']:'');
                }  

                //get selected containers
                $qry = "SELECT container_id,container_number  FROM tbl_container 
                    WHERE container_id IN (".$_POST['hold_containerIds'].") ";  
                $query = $this->db->query($qry);
                $edit_datas['containers'] = $query->result_array(); 

                if($update_shipping_information == 'shipping'){  
                    $edit_datas['container_details'] = array();
                    $edit_datas['contractDetails'] = array();
                    $edit_datas['chaDetails'] = array();
                    $edit_datas['freightForwarder'] = array();
                    $edit_datas['skudetails'] = array(); 

                    $result = $this->common->getData("tbl_container", "*", array("container_id" => $container_id));
                    if (!empty($result)) {
                        $edit_datas['container_details'] = $result[0]; 

                        //cha and Forwarder from master contract
                        $condition = "status = 'Active' AND order_id = ".$result[0]['order_id']." ";  
                        $orderData = $this->common->getData("tbl_order", "broker_id,freight_forwarder_id",$condition);
                        if (!empty($orderData)) { 
                            //CHA data
                            if(!empty($orderData[0]['broker_id'])){
                                $condition = "status = 'Active' AND business_partner_id IN (".$orderData[0]['broker_id'].") ";        
                                $chaDetails = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias", $condition,'alias','asc');
                                if (!empty($chaDetails)) {
                                    $edit_datas['chaDetails'] = $chaDetails;
                                }
                            }

                            //Freight Forwarder data 
                            if(!empty($orderData[0]['freight_forwarder_id'])){
                                $condition = "status = 'Active' AND business_partner_id IN (".$orderData[0]['freight_forwarder_id'].") ";  
                                $freightForwarder = $this->common->getData("tbl_business_partner", "business_partner_id,business_name,alias",$condition,'alias','asc');
                                if (!empty($freightForwarder)) {
                                    $edit_datas['freightForwarder'] = $freightForwarder;
                                } 
                            } 
                        }   
                    } 

                    $condition = " status = 'Active'  ";  
                    $containerStatusData = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", $condition, 'status_squence', 'asc');
                    if (!empty($containerStatusData)) {
                        $edit_datas['containerStatusData'] = $containerStatusData;
                    }

                    $main_table = array("tbl_order as o", array("o.order_id,o.contract_number")); 
                    $condition = "1=1  AND o.status = 'Active' ";        
                    $join_tables = array(
                        array("","tbl_bp_order as cbpo","cbpo.bp_order_id = o.customer_contract_id", array()),
                        array("","tbl_bp_order as sbpo","sbpo.bp_order_id = o.supplier_contract_id", array()),
                        array("","tbl_business_partner as cbp","cbp.business_partner_id = cbpo.business_partner_id", array('cbp.alias as cbp_name')),
                        array("","tbl_business_partner as sbp","sbp.business_partner_id = sbpo.business_partner_id", array('sbp.alias as sbp_name')) 
                    );         
                    $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
                    $contractDetails = $this->common->MySqlFetchRow($rs, "array");  

                    //$contractDetails = $this->common->getData("tbl_order", "order_id,contract_number", array("status" => "Active"));
                    if (!empty($contractDetails)) {
                        $edit_datas['contractDetails'] = $contractDetails;
                    } 

                    //Liner data 
                    $liner = $this->common->getData("tbl_liner", "liner_id,liner_name", array("status" => 'Active'),'liner_name','asc');
                    if (!empty($liner)) {
                        $edit_datas['liner'] = $liner;
                    }

                    //POL data 
                    $pol = $this->common->getData("tbl_pol", "pol_id,pol_name", array("status" => 'Active'),'pol_name','asc');
                    if (!empty($pol)) {
                        $edit_datas['pol'] = $pol;
                    }

                    //POD data 
                    $pod = $this->common->getData("tbl_pod", "pod_id,pod_name", array("status" => 'Active'),'pod_name','asc');
                    if (!empty($pod)) {
                        $edit_datas['pod'] = $pod;
                    }

                    //Flag data 
                    $flag = $this->common->getData("tbl_flag", "flag_id,flag_name", array("status" => 'Active'),'flag_name','asc');
                    if (!empty($flag)) {
                        $edit_datas['flag'] = $flag;
                    }    
                    //echo '<pre>'; print_r($edit_datas['filter']);die;  
                    $view = 'multiple-update-shipping-info';  
                }else if($update_shipping_information == 'custom'){     
                    $update_document_type = $_POST['update_document_type'];   
                    $document_type_id = $update_document_type;    
                    $condition = " document_type_id =".$document_type_id." ";
                    $document_type_name = $this->common->getData('tbl_document_type', 'document_type_name', $condition); 
                    $edit_datas['document_type_name'] = $document_type_name[0]['document_type_name'];  

                    $condition = " container_id = '" . $container_id . "' and document_type_id = '" . $document_type_id . "'  ";
                    if($document_type_id == '2' || $document_type_id == '3'){ 
                        $getDocumentNumber = $this->common->getData('tbl_document_uploaded_files', 'uploaded_document_number', $condition); 
                    }else{
                        $getDocumentNumber = $this->common->getData('tbl_document_ls_uploaded_files', 'uploaded_document_number', $condition);
                    }

                    $edit_datas['form_view'] = '';                
                    if(!empty($getDocumentNumber)){  
                        $document_number = $getDocumentNumber[0]['uploaded_document_number']; 

                        $leftsidehtml = "";
                        $form_view = '';
                        $cm = "'";

                        if ($document_type_id != '2' && $document_type_id != '3') {
                            //$document_type_id = 1;
                            $condition = "1=1  AND dluf.container_id = " . $container_id . " AND dluf.document_type_id = " . $document_type_id . " AND cfs.status = 'Active' ";
                            $main_table = array("tbl_custom_field_structure as cfs", array("cfs.*"));
                            $join_tables = array(
                                array("left", "tbl_custom_field_data_submission as cfds", "cfds.custom_field_structure_id = cfs.custom_field_structure_id AND cfds.uploaded_document_number='" . $document_number . "'", array('cfds.custom_field_structure_value')),
                                array("", "tbl_document_ls_uploaded_files as dluf", "dluf.document_type_id = cfs.document_type_id", array('group_concat(DISTINCT(dluf.uploaded_document_number)) as uploaded_document_number,uploaded_document_file ')),
                                array("left", "tbl_sub_document_type as sdt", "sdt.document_type_id = dluf.document_type_id", array('group_concat(DISTINCT(sdt.sub_document_type_id)) as sub_document_type_id ')),
                            );

                            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'custom_field_structure_id');
                            $result['getSelectedCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                            //echo $this->db->last_query();exit; 
                            //$result['getSelectedCustomField'] = $this->container_detailsmodel->getDocumentsFields($document_type_id,$document_number); 

                            $result['containersSKUData'] = $this->getcontainersSKUDataDetails($document_type_id, $document_number, $_POST['hold_containerIds']);
                            $leftsidehtml = '';
                            $form_view = '';

                            // get the manufacturer list 
                            $condition = "status = 'Active' And business_type = 'Vendor' AND business_category ='Manufacturer'  ";
                            $result['manufacturer'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number', $condition);

                            // get the supplier name  
                            $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Distributor','Manufacturer')  ";
                            $result['supplier_name'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number', $condition);

                            //supplier_update
                            $result['supplier_update'] = '';
                            $condition = "container_id = '" . $container_id . "' AND document_type_id = '".$document_type_id."' AND supplier_id IS NOT NULL";
                            $supplier_update = $this->common->getData('tbl_custom_dynamic_data', 'supplier_id,supplier_contactperson,supplier_contactnumber', $condition, 'custom_dynamic_data_id', 'desc');
                            if (!empty($supplier_update)) {
                                $result['supplier_update'] = $supplier_update;
                            }

                            //  get hbl_number         
                            $condition = "container_id = '" . $container_id . "' AND hbl_number IS NOT NULL  AND document_type_id = " . $document_type_id . " ";
                            $result['get_hbl_number'] = $this->common->getData('tbl_custom_dynamic_data', 'hbl_number', $condition, 'custom_dynamic_data_id', 'desc');

                            $result['all_freight_forwarder'] = array();
                            $condition = "1=1  AND c.container_id = " . $container_id . " ";
                            $main_table = array("tbl_container as c", array());
                            $join_tables = array(
                                array("", "tbl_order as o", "o.order_id = c.order_id", array('o.freight_forwarder_id')),
                            );
                            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition);
                            $orderData = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                            if(!empty($orderData)) {
                                $condition = "status = 'Active' And business_partner_id IN (" . $orderData[0]['freight_forwarder_id'] . ")  ";
                                $result['all_freight_forwarder'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,alias', $condition);
                            }

                            // get Port of Loading;
                            $condition = "status = 'Active' ";
                            $result['all_pol'] = $this->common->getData('tbl_pol', '*', $condition);
                            // get Port of Discharge;
                            $condition = "status = 'Active' ";
                            $result['all_pod'] = $this->common->getData('tbl_pod', '*', $condition);
                            //get liner name
                            $condition = "status = 'Active' ";
                            $result['all_liner'] = $this->common->getData('tbl_liner', '*', $condition);

                            if (!empty($result['getSelectedCustomField'][0]['document_type_id'])) {
                                if ($result['getSelectedCustomField'][0]['document_type_id'] == 4) {
                                    $leftsidehtml = $this->load->view("update/custom-fcr-update", $result, true);
                                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 5) {
                                    $leftsidehtml = $this->load->view("update/custom-hbl-update", $result, true);
                                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 1) {
                                    $leftsidehtml = $this->load->view("update/custom-ins-update", $result, true);
                                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 10) {
                                    $leftsidehtml = $this->load->view("update/custom-arrival-update", $result, true);
                                }else if ($result['getSelectedCustomField'][0]['document_type_id'] == 7) {
                                    $leftsidehtml = $this->load->view("update/7501-update", $result, true);
                                }else if ($result['getSelectedCustomField'][0]['document_type_id'] == 8) {
                                    $leftsidehtml = $this->load->view("update/custom-do-update", $result, true);
                                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 9) {
                                    $leftsidehtml = $this->load->view("update/custom-do-update", $result, true);
                                }  
                            } 

                            //$leftsidehtml = $this->load->view("update/custom-ins-update", $result, true);

                            // to get the  form view for both internal n external 
                            if ($result) { 
                                 //re-upload document code
                                $condition = "1=1  AND d.document_type_id = '" . $document_type_id . "' ";
                                $main_table = array("tbl_document_type as d", array('d.document_type_name'));
                                $join_tables = array(
                                    array("left", "tbl_sub_document_type as sd", "sd.document_type_id = d.document_type_id", array("sd.sub_document_type_name,sd.sub_document_type_id")),
                                );

                                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                                $getSubDocument = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                                $result1 = array();
                                $result1['getSubDocument'] = $getSubDocument;
                                $result1['document_type_id'] = $document_type_id; 
                                $result['reupload'] = $this->load->view("update/document-reupload-form", $result1, true);  

                                if ($_SESSION["mro_session"]['user_role'] == 'Customer' || $_SESSION["mro_session"]['user_role'] == 'Supplier') {
                                    $condition = "1=1  AND dluf.container_id = " . $container_id . " AND dluf.document_type_id = " . $document_type_id . " AND sdt.sub_document_type_id IN (15,17,20) ";
                                    $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.*"));
                                    $join_tables = array(
                                        array("", "tbl_document_type as dt", "dt.document_type_id = dluf.document_type_id", array('dt.document_type_name as document_name')),
                                        array("", "tbl_sub_document_type as sdt", "sdt.document_type_id = dt.document_type_id && sdt.sub_document_type_id = dluf.sub_document_type_id", array('sdt.sub_document_type_name as sub_document_name')),
                                    ); 
                                }else{
                                    $condition = "1=1  AND dluf.container_id = " . $container_id . " AND dluf.document_type_id = " . $document_type_id . " ";
                                    $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.*"));
                                    $join_tables = array(
                                        array("", "tbl_document_type as dt", "dt.document_type_id = dluf.document_type_id", array('dt.document_type_name as document_name')),
                                        array("left", "tbl_sub_document_type as sdt", "sdt.document_type_id = dt.document_type_id && sdt.sub_document_type_id = dluf.sub_document_type_id", array('sdt.sub_document_type_name as sub_document_name')),
                                    ); 
                                }

                                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'document_type_id,sub_document_type_id');
                                $result['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result
                                //echo $this->db->last_query();exit;
                                $edit_datas['form_view'] = $this->load->view("update/document-form-view", $result, true);
                            }
                            
                            //hidden document id for ABI document
                            if(empty($leftsidehtml)){
                                $leftsidehtml = '<input type="hidden" name="document_type_id" id="document_type_id" value="'.$document_type_id.'">';
                            } 
                            $edit_datas['form_html'] = $leftsidehtml;  
                        } else {  
                            $condition = "1=1  AND dluf.container_id = " . $container_id . " AND dluf.document_type_id = " . $document_type_id . " AND cfs.status = 'Active' ";
                            $main_table = array("tbl_custom_field_structure as cfs", array("cfs.*"));
                            $join_tables = array(
                                array("left", "tbl_custom_field_data_submission as cfds", "cfds.custom_field_structure_id = cfs.custom_field_structure_id AND cfds.uploaded_document_number='" . $document_number . "'", array('cfds.custom_field_structure_value')),
                                array("", "tbl_document_uploaded_files as dluf", "dluf.document_type_id = cfs.document_type_id", array('group_concat(DISTINCT(dluf.uploaded_document_number)) as uploaded_document_number,uploaded_document_file ')),                    
                            );

                            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'custom_field_structure_id');
                            $result['getSelectedCustomField'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result

                            //$result['getSelectedCustomField'] = $this->container_detailsmodel->getDocumentsFields($document_type_id, $document_number);
                            //echo '<pre>'; print_r($result['getSelectedCustomField']); die;  
                            $result['containersSKUData'] = $this->getcontainersSKUDataDetails($document_type_id, $document_number, $_POST['hold_containerIds']);
                            //echo '<pre>'; print_r($result['containersSKUData']);die;   
                            // get the manufacturer list 
                            $condition = "status = 'Active' And business_type = 'Vendor' AND business_category ='Manufacturer'  ";
                            $result['manufacturer'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number,alias', $condition);

                            // get the supplier name  
                            $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Distributor','Manufacturer')  ";
                            $result['supplier_name'] = $this->common->getData('tbl_business_partner', 'business_partner_id,business_name,contact_person,phone_number,alias', $condition);

                            //supplier_update
                            $result['supplier_update'] = '';
                            $condition = "container_id = '" . $container_id . "' AND document_type_id = '".$document_type_id."' AND supplier_id IS NOT NULL";
                            $supplier_update = $this->common->getData('tbl_custom_dynamic_data', 'supplier_id,supplier_contactperson,supplier_contactnumber', $condition, 'custom_dynamic_data_id', 'desc');
                            if (!empty($supplier_update)) {
                                $result['supplier_update'] = $supplier_update;
                            }

                            $condition = "container_id = '" . $container_id . "' AND document_type_id =".$result['getSelectedCustomField'][0]['document_type_id']."  AND date_of_inspection IS NOT NULL";
                            $result['get_date_of_inspection'] = $this->common->getData('tbl_custom_dynamic_data', 'date_of_inspection', $condition, 'custom_dynamic_data_id', 'desc');

                            $condition = "container_id = '" . $container_id . "' AND document_type_id =".$result['getSelectedCustomField'][0]['document_type_id']."  AND place_of_inspection IS NOT NULL";
                            $result['get_place_of_inspection'] = $this->common->getData('tbl_custom_dynamic_data', 'place_of_inspection', $condition, 'custom_dynamic_data_id', 'desc');

                            // get Port of Loading;
                            $condition = "status = 'Active' ";
                            $result['all_pol'] = $this->common->getData('tbl_pol', '*', $condition);
                            // get Port of Discharge;
                            $condition = "status = 'Active' ";
                            $result['all_pod'] = $this->common->getData('tbl_pod', '*', $condition);
                            //get liner name
                            $condition = "status = 'Active' ";
                            $result['all_liner'] = $this->common->getData('tbl_liner', '*', $condition);

                            $leftsidehtml = '';
                            if ($result) {
                                if ($result['getSelectedCustomField'][0]['document_type_id'] == 3) {
                                    $leftsidehtml = $this->load->view("update/custom-fri-update", $result, true);
                                } else if ($result['getSelectedCustomField'][0]['document_type_id'] == 2) {
                                    $leftsidehtml = $this->load->view("update/custom-ls-update", $result, true);
                                } 
                            }

                            $edit_datas['form_html'] = $leftsidehtml;  

                            // to get the  form view for both internal n external 
                            $edit_datas['form_view'] = '';
                            if ($result) { 
                                //re-upload document code
                                $condition = "1=1  AND d.document_type_id = '" . $document_type_id . "' ";
                                $main_table = array("tbl_document_type as d", array('d.document_type_name'));
                                $join_tables = array(
                                    array("left", "tbl_sub_document_type as sd", "sd.document_type_id = d.document_type_id", array("sd.sub_document_type_name,sd.sub_document_type_id")),
                                );

                                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
                                $getSubDocument = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                                $result1 = array();
                                $result1['getSubDocument'] = $getSubDocument;
                                $result1['document_type_id'] = $document_type_id; 
                                $result['reupload'] = $this->load->view("update/document-reupload-form", $result1, true); 

                                //document view code
                                $condition = "1=1  AND duf.uploaded_document_number = '" . $document_number . "' ";
                                $main_table = array("tbl_document_uploaded_files as duf", array("duf.*"));
                                $join_tables = array(
                                    array("", "tbl_document_type as dt", "dt.document_type_id = duf.document_type_id", array('dt.document_type_name as document_name')),
                                );
                                $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", 'document_type_id');
                                $result['formView'] = $this->common->MySqlFetchRow($rs, "array"); // fetch result 
                                $edit_datas['form_view'] = $this->load->view("update/document-form-view", $result, true);
                            } 
                        }  
                    }  

                    $view = 'multiple-update-custom-fields';

                }else{
                    redirect(base_url().'listOfContainer');exit;
                } 

                $this->load->view('template/head.php');
                $this->load->view('template/navigation.php');
                $this->load->view($view, $edit_datas);
                $this->load->view('template/footer.php');
                $this->load->view('template/footer-scripts.php');

            }else{ 
                redirect(base_url().'listOfContainer');exit;
            } 
        }else{ 
            redirect(base_url().'listOfContainer');exit;
        }  
    }
    
    function multiUpdateContainerSubmit() {
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
            $data = array(); 
            $data['broker_id'] = $this->input->post('broker_id'); 
            $data['freight_forwarder_id'] = $this->input->post('freight_forwarder_id');  
            $data['revised_eta'] = !empty($this->input->post('revised_eta')) ? date("Y-m-d", strtotime($this->input->post('revised_eta'))) : NULL; 
            $data['revised_etd'] = !empty($this->input->post('revised_etd')) ? date("Y-m-d", strtotime($this->input->post('revised_etd'))) : NULL;  
            $data['revised_ett'] = $this->input->post('revised_ett'); 
            $data['liner_name'] = $this->input->post('liner_name'); 
            $data['liner_tracker_url'] = $this->input->post('liner_tracker_url'); 
            $data['pol'] = $this->input->post('pol'); 
            $data['pod'] = $this->input->post('pod'); 
            $data['vessel_name'] = $this->input->post('vessel_name'); 
            $data['vessel_tracker_url'] = $this->input->post('vessel_tracker_url');
            $data['flag'] = $this->input->post('flag'); 
            $data['terminal'] = $this->input->post('terminal'); 
            $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
            $data['updated_on'] = date("Y-m-d H:i:s"); 
            $container_ids = $this->input->post('container_ids');   
            //print_r($container_ids);die;
            
            $last_container_status_id = $this->input->post('last_container_status_id');
            $container_status_id = !empty($this->input->post('container_status_update_id'))?$this->input->post('container_status_update_id'):0;
            
            /* get status_squence of selected container status */
            $condition = " container_status_id = '" . $container_status_id . "' ";
            $getContainerStatus = $this->common->getData('tbl_container_status', 'status_squence', $condition); 
            
            $result = '';
            if(!empty($container_ids)){
                for($i=0;$i<sizeof($container_ids);$i++){   
                    $containerId = $container_ids[$i];
                    $result = $this->common->updateData("tbl_container", $data, array("container_id" => $containerId));
                    
                    /*container status update code start*/
                    if($last_container_status_id != $container_status_id){
                        $condition = " container_id = '" . $containerId . "' ";
                        $getContainerNumber = $this->common->getData('tbl_container', 'last_container_status_id', $condition);

                        /* get status_squence of container */
                        $condition = " container_status_id = '" . $getContainerNumber[0]['last_container_status_id'] . "' ";
                        $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status', 'status_squence', $condition);
                        $lastContainerStatusSquence = 0;
                        if (!empty($lastContainerStatusSquenceData)) {
                            $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                        } 
                        /* check previous status squence should less than from selected status squence*/
                        if ($lastContainerStatusSquence < $getContainerStatus[0]['status_squence']) {
                            $dataLog = array();
                            $dataLog['container_id'] = $containerId;
                            $dataLog['container_status_id'] = $container_status_id;
                            $dataLog['created_on'] = date("Y-m-d H:i:s");
                            $dataLog['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $dataLog['document_date'] = date("Y-m-d");
                            $result1 = $this->common->insertData('tbl_container_status_log', $dataLog, '1'); 
                            //last status in container table 
                            $containerStatusUpdate = array();
                            $containerStatusUpdate['last_container_status_id'] = $container_status_id;
                            $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                            $result2 = $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $containerId));
                        } 
                    }
                    /*container status update code end*/ 
                } 
            } 
            if ($result) {
                echo json_encode(array('success' => true, 'msg' => 'Containers updated successfully.'));
                exit;
            } else {
                echo json_encode(array('success' => false, 'msg' => 'Problem while updating data.'));
                exit;
            }
        }else {
            echo json_encode(array('success' => false, 'msg' => 'Problem while update data.'));
            exit;
        }
    }  
    
    function multiUpdateCustomFieldsSubmit(){ 
        $container_ids = isset($_POST['container_ids'])?$_POST['container_ids']:''; 
        $document_type_id = isset($_POST['document_type_id'])?$_POST['document_type_id']:'';
        $result = '';
        if(!empty($document_type_id) && !empty($container_ids)){
            if (!empty($_POST['custom_field_structure_id']) && isset($_POST['custom_field_structure_id'])) {  
                if($document_type_id == '2' || $document_type_id == '3' || $document_type_id == '7' || $document_type_id == '8' || $document_type_id == '9' || $document_type_id == '10'){  
                    for($i=0;$i<sizeof($container_ids);$i++){   
                        $containerId = $container_ids[$i]; 
                        $condition = " container_id = '" . $containerId . "' and document_type_id = '" . $document_type_id . "'  ";
                        if($document_type_id == '2' || $document_type_id == '3'){ 
                            $getDocumentNumber = $this->common->getData('tbl_document_uploaded_files', 'uploaded_document_number', $condition); 
                        }else{
                            $getDocumentNumber = $this->common->getData('tbl_document_ls_uploaded_files', 'uploaded_document_number', $condition);
                        }

                        if(!empty($getDocumentNumber)){
                            $uploaded_document_number = $getDocumentNumber[0]['uploaded_document_number'];  
                            $actualDateofArrival = ''; 
                            foreach ($_POST['custom_field_structure_id'] as $key => $value) {
                                $condition = " custom_field_structure_id = " . $value . " and document_type_id = " . $document_type_id . " and uploaded_document_number = '" . $uploaded_document_number . "' ";                                
                                $deleted = $this->common->deleteRecord('tbl_custom_field_data_submission', $condition);  
                                if($deleted){
                                    //Actual date of Arrival check for DO upload
                                    if(isset($_POST['dateOfArrival'])){
                                        if($value == 92){
                                            $actualDateofArrival = (!empty($_POST[$value]) ? date('Y-m-d', strtotime($_POST[$value])) : "");
                                        }
                                    }
                                    $data = array();
                                    $data['custom_field_structure_id'] = $value;
                                    $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                                    $data['uploaded_document_number'] = $uploaded_document_number;
                                    $data['document_type_id'] = $document_type_id;
                                    $data['sub_document_type_id'] = 0;
                                    $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $data['updated_on'] = date("Y-m-d H:i:s");
                                    $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $data['created_on'] = date("Y-m-d H:i:s");
                                    $data['current_session_id'] = '';
                                    $result = $this->common->insertData("tbl_custom_field_data_submission", $data, "1");
                                }
                            } 
                        } 
                    }  
                }else{ 
                    for($i=0;$i<sizeof($container_ids);$i++){   
                        $containerId = $container_ids[$i]; 
                        $condition = " container_id = '" . $containerId . "' and document_type_id = '" . $document_type_id . "'  ";  
                        $getDocumentNumber = $this->common->getData('tbl_document_ls_uploaded_files', 'uploaded_document_number,sub_document_type_id', $condition); 
                        if(!empty($getDocumentNumber)){ 
                            foreach ($getDocumentNumber as $docNumber) {  
                                $uploaded_document_number = $docNumber['uploaded_document_number']; 
                                $sub_document_type_id = $docNumber['sub_document_type_id'];   
                                foreach ($_POST['custom_field_structure_id'] as $key => $value) {
                                    $condition = " custom_field_structure_id = " . $value . " and document_type_id = " . $document_type_id . " and sub_document_type_id= '" . $sub_document_type_id . "'  and uploaded_document_number = '" . $uploaded_document_number . "' ";
                                    $dynamic_data = $this->common->getData('tbl_custom_field_data_submission', 'custom_data_submission_id,current_session_id', $condition, 'custom_data_submission_id', 'desc');
                                    $data = array();
                                    if (!empty($dynamic_data)) {
                                        $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['updated_on'] = date("Y-m-d H:i:s");
                                        $condition = " custom_field_structure_id = " . $value . " and document_type_id = " . $document_type_id . " and sub_document_type_id= '" . $sub_document_type_id . "'  and uploaded_document_number = '" . $uploaded_document_number . "' ";
                                        $result = $this->common->updateData("tbl_custom_field_data_submission", $data, $condition);
                                    } else {
                                        $data['custom_field_structure_id'] = $value;
                                        $data['custom_field_structure_value'] = (!empty($_POST[$value]) ? $_POST[$value] : "");
                                        $data['uploaded_document_number'] = $uploaded_document_number;
                                        $data['document_type_id'] = $document_type_id;
                                        $data['sub_document_type_id'] = $sub_document_type_id;
                                        $data['current_session_id'] = '';
                                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['updated_on'] = date("Y-m-d H:i:s");
                                        $data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $data['created_on'] = date("Y-m-d H:i:s");
                                        $result = $this->common->insertData("tbl_custom_field_data_submission", $data, "1");
                                    }
                                }  
                            }
                        } 
                    }    
                } 

                // sku quantity update code
    //                if (!empty($_POST['container_sku_id']) && isset($_POST['container_sku_id'])) {
    //                    foreach ($_POST['container_sku_id'] as $key => $container_val) {
    //                        // $key == container id here
    //                        foreach ($container_val as $sku_key => $sku_val) {
    //                            $data1 = array();
    //                            $data1['quantity'] = $_POST['container_sku_qty'][$key][$sku_key];
    //                            $condition = "container_sku_id = " . $sku_key . " AND container_id = " . $key . " ";
    //                            $this->common->updateData("tbl_container_sku", $data1, $condition);
    //
    //                            $data_manu = array();
    //                            $data_manu['manufacturer_id'] = $_POST['manufacturer'][$key][$sku_key];
    //                            $condition = "sku_id = " . $sku_key . "  ";
    //                            $this->common->updateData("tbl_sku", $data_manu, $condition);
    //                        }
    //                    }
    //                } 

    //                if (!empty($_POST['lot_number']) && isset($_POST['lot_number'])) {
    //                    foreach ($_POST['lot_number'] as $seal_key => $seal_val) {
    //                        $data_lot = array();
    //                        $data_lot['lot_number'] = $_POST['lot_number'][$seal_key];
    //                        $condition = " container_id = " . $seal_key . " ";
    //                        $this->common->updateData("tbl_container", $data_lot, $condition);
    //                    }
    //                }
    //
    //                if (!empty($_POST['sgs_seal']) && isset($_POST['sgs_seal'])) {
    //                    foreach ($_POST['sgs_seal'] as $seal_key => $seal_val) {
    //                        $data_seal = array();
    //                        $data_seal['sgs_seal'] = $seal_val;
    //                        $data_seal['shipping_container'] = $_POST['shipping_container'][$seal_key];
    //                        $data_seal['ff_seal'] = $_POST['ff_seal'][$seal_key];
    //                        if(isset($_POST['terminal'])){
    //                            $data_seal['terminal'] = $_POST['terminal'][$seal_key];
    //                        } 
    //                        $condition = " container_id = " . $seal_key . " ";
    //                        $this->common->updateData("tbl_container", $data_seal, $condition);
    //                    }
    //                }

                if (!empty($_POST['supplier_name']) && isset($_POST['supplier_name'])) {
                    foreach ($_POST['supplier_name'] as $supp_key => $supp_val) { 
                        for($i=0;$i<sizeof($container_ids);$i++){   
                            $container_id = $container_ids[$i];   
                            $data_supp = array();
                            $data_supp['supplier_id '] = $supp_val;
                            $data_supp['supplier_contactperson'] = $_POST['contactpersonname'][$supp_key];
                            $data_supp['supplier_contactnumber'] = $_POST['contactpersonnumber'][$supp_key];
                            // $data_supp['sub_document_type_id'] = explode(',',$_POST['sub_document_type_id']);
                            $data_supp['current_session_id'] = '';

                            $condition = " container_id = " . $container_id . " AND document_type_id =".$document_type_id." ";
                            $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                            if (!empty($dynamic_data)) {
                                $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$document_type_id." ";
                                $data_supp['updated_on'] = date("Y-m-d H:i:s");
                                $data_supp['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition);
                            } else {
                                $data_supp['container_id'] = $container_id;
                                $data_supp['document_type_id'] = $document_type_id;
                                $data_supp['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data_supp['updated_on'] = date("Y-m-d H:i:s");
                                $data_supp['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data_supp['created_on'] = date("Y-m-d H:i:s");
                                $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                            }
                        }
                    }
                }


                if (!empty($_POST['tbl_container_id']) && isset($_POST['tbl_container_id'])) {
                    foreach ($_POST['tbl_container_id'] as $key => $value) {
                        $data2 = array();
                        $data2['freight_forwarder_id'] = $_POST['freight_forwarder_id'][$key];
                        $data2['liner_name'] = $_POST['liner_name'][$key];
                        $data2['pol'] = $_POST['pol'][$key];
                        $data2['pod'] = $_POST['pod'][$key];
                        $data2['etd'] = !empty($_POST['etd'][$key]) ? date("Y-m-d", strtotime($_POST['etd'][$key])) : NULL;
                        $data2['revised_etd'] = !empty($_POST['revised_etd'][$key]) ? date("Y-m-d", strtotime($_POST['revised_etd'][$key])) : NULL;
                        $data2['ett'] = $_POST['ett'][$key];
                        $data2['vessel_name'] = $_POST['vessel_name'][$key]; 
                        for($i=0;$i<sizeof($container_ids);$i++){   
                            $container_id = $container_ids[$i];   
                            $condition = " container_id = " . $container_id . " ";
                            $this->common->updateData("tbl_container", $data2, $condition);
                        }
                    }
                }

                /*update actual date of arrival when upload DO then update container revised eta field*/
                if(!empty($actualDateofArrival)){
                    $data_revised_eta = array(); 
                    $data_revised_eta['revised_eta'] = !empty($actualDateofArrival) ? $actualDateofArrival : NULL;
                    for($i=0;$i<sizeof($container_ids);$i++){   
                        $container_id = $container_ids[$i]; 
                        $condition = " container_id = " . $container_id . " ";
                        $this->common->updateData("tbl_container", $data_revised_eta, $condition);
                    }
                }

                if (!empty($_POST['revised_eta']) && isset($_POST['revised_eta'])) {
                    foreach ($_POST['revised_eta'] as $eta_key => $eta_val) {
                        $data_eta = array();
                        $data_eta['etd'] = !empty($_POST['etd'][$eta_key]) ? date("Y-m-d", strtotime($_POST['etd'][$eta_key])) : NULL;
                        $data_eta['eta'] = !empty($_POST['eta'][$eta_key]) ? date("Y-m-d", strtotime($_POST['eta'][$eta_key])) : NULL;
                        $data_eta['revised_eta'] = !empty($eta_val) ? date("Y-m-d", strtotime($eta_val)) : NULL;
                        $condition = " container_id = " . $eta_key . " ";
                        //print_r($data_eta); die; 
                        for($i=0;$i<sizeof($container_ids);$i++){   
                            $container_id = $container_ids[$i]; 
                            $condition = " container_id = " . $container_id . " ";
                            $this->common->updateData("tbl_container", $data_eta, $condition);
                        }
                    }
                } 

                /*HBL Number, date of inspection and place of inspection insert/update code start here*/
                if ((!empty($_POST['hbl_number']) && isset($_POST['hbl_number'])) || isset($_POST['date_of_inspection']) || isset($_POST['place_of_inspection'])) {  
                    for($i=0;$i<sizeof($container_ids);$i++){   
                        $container_id = $container_ids[$i];
                        $data_hbl = array(); 
                        if (!empty($_POST['hbl_number']) && isset($_POST['hbl_number'])){ 
                            $data_hbl['hbl_number'] = $_POST['hbl_number'];
                        } 
                        if (isset($_POST['date_of_inspection'])){ 
                            $data_hbl['date_of_inspection'] = !empty($_POST['date_of_inspection']) ? date("Y-m-d", strtotime($_POST['date_of_inspection'])) : NULL;
                        } 
                        if (isset($_POST['place_of_inspection'])){ 
                            $data_hbl['place_of_inspection'] = $_POST['place_of_inspection'];
                        } 

                        $data_hbl['current_session_id'] = ''; 
                        $condition = " container_id = " . $container_id . " AND document_type_id =".$document_type_id." ";
                        $dynamic_data = $this->common->getData('tbl_custom_dynamic_data', 'container_id,document_type_id', $condition, 'custom_dynamic_data_id', 'desc');
                        if (!empty($dynamic_data)) {
                            $condition = " container_id = " . $dynamic_data[0]['container_id'] . " AND document_type_id =".$document_type_id." ";
                            $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                            $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $this->common->updateData("tbl_custom_dynamic_data", $data_hbl, $condition);
                        } else {
                            $data_hbl['container_id'] = $container_id;
                            $data_hbl['document_type_id'] = $document_type_id;
                            $data_hbl['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data_hbl['updated_on'] = date("Y-m-d H:i:s");
                            $data_hbl['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $data_hbl['created_on'] = date("Y-m-d H:i:s");
                            $this->common->insertData("tbl_custom_dynamic_data", $data_hbl, "1");
                        }
                    } 
                } 
                /*HBL Number, date of inspection and place of inspection insert/update code end here*/ 
            } 
            
            /*document reupload code start here*/
            if (!empty($_FILES["step3_document"]["name"]) && isset($_FILES["step3_document"]["name"]) && !empty($document_type_id) && isset($document_type_id)) {

                $this->load->library('upload');
                $result_reupload = array();
                $data = array();
                // this is for fr n ls document type 
                if ($document_type_id == '2' || $document_type_id == '3') {
                    if (isset($_FILES) && isset($_FILES["step3_document"]["name"])) {
                        // $this->upload->initialize($this->set_upload_options($document_type_id));
                        // if (!$this->upload->do_upload("step3_document")) {
                        //     $image_error = array('error' => $this->upload->display_errors());
                        //     echo json_encode(array("success" => false, "msg" => $image_error['error']));
                        //     exit;
                        // } else {
                            // $image_data = array('upload_data' => $this->upload->data());
                            //function for upload file in live bucket
                            // $full_path = $image_data['upload_data']['full_path'];                            
                            $resultUpload = set_s3_upload_file($_FILES["step3_document"]["tmp_name"],$document_type_id,$_FILES["step3_document"]["name"]); 
                            $step3_document =  basename($resultUpload);
                            // $step3_document = $image_data['upload_data']['file_name'];
                            $data['uploaded_document_file'] = $step3_document;
                            $upload = true;
                        // }
                    }

                    if ($upload) {
                        $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                        $data['updated_on'] = date("Y-m-d H:i:s"); 
                        for($i=0;$i<sizeof($container_ids);$i++){   
                            $container_id = $container_ids[$i];  
                            $condition = " container_id = " . $container_id . " AND document_type_id = " . $document_type_id . " ";
                            $result_reupload = $this->common->updateData("tbl_document_uploaded_files", $data, $condition);
                            $result = 1;
                        }
                    }
                } else {
                    $files = $_FILES;
                    foreach ($files['step3_document']['name'] as $key => $value) {
                        $upload = false;
                        if (!empty($files['step3_document']['name'][$key]) && isset($files['step3_document']['name'][$key])) {

                            $_FILES['step3_document']['name'] = $files['step3_document']['name'][$key];
                            $_FILES['step3_document']['type'] = $files['step3_document']['type'][$key];
                            $_FILES['step3_document']['tmp_name'] = $files['step3_document']['tmp_name'][$key];
                            $_FILES['step3_document']['error'] = $files['step3_document']['error'][$key];
                            $_FILES['step3_document']['size'] = $files['step3_document']['size'][$key];

                            // $this->upload->initialize($this->set_upload_options($document_type_id));
                            // if (!$this->upload->do_upload("step3_document")) {
                            //     $image_error = array('error' => $this->upload->display_errors());
                            //     echo json_encode(array("success" => false, "msg" => $image_error['error']));
                            //     exit;
                            // } else {
                            //     $image_data = array('upload_data' => $this->upload->data());
                            //     //function for upload file in live bucket
                            //     $full_path = $image_data['upload_data']['full_path'];                            
                                $resultUpload = set_s3_upload_file($_FILES['step3_document']['tmp_name'],$document_type_id,$_FILES['step3_document']['name']);
                                $step3_document =  basename($resultUpload);
                                // $step3_document = $image_data['upload_data']['file_name'];
                                if ($key == '0') {
                                    $uploaded_document_type_id['document_type_id'] = $key;
                                    $data['uploaded_document_file'] = $step3_document;
                                } else {
                                    $uploaded_document_file_name['file_name'][$key] = $step3_document;
                                    $uploaded_document_type_id['sub_document_type_id'][$key] = $key;
                                }
                                $upload = true;
                            // }
                        }
                    }

                    if ($upload) { 
                        foreach ($files['step3_document']['name'] as $key => $value) {
                            if ($key == 0) {
                                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['updated_on'] = date("Y-m-d H:i:s");
                                for($i=0;$i<sizeof($container_ids);$i++){   
                                    $container_id = $container_ids[$i]; 
                                    $condition = " container_id = " . $container_id . " AND document_type_id = " . $document_type_id . " ";
                                    $result_reupload = $this->common->updateData("tbl_document_ls_uploaded_files", $data, $condition);
                                    $result = 1;
                                }
                            } else {
                                $data['uploaded_document_file'] = $uploaded_document_file_name['file_name'][$key];
                                $data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $data['updated_on'] = date("Y-m-d H:i:s");
                                for($i=0;$i<sizeof($container_ids);$i++){   
                                    $container_id = $container_ids[$i]; 
                                    $condition = " container_id = " . $container_id . " AND document_type_id = " . $document_type_id . " AND sub_document_type_id = " . $uploaded_document_type_id['sub_document_type_id'][$key] . " ";
                                    $result_reupload = $this->common->updateData("tbl_document_ls_uploaded_files", $data, $condition);
                                    $result = 1;
                                }
                            }
                        }
                    }
                } 
            }
            /*document reupload code end here*/ 
            if($document_type_id != 9 ){
                if ($result) {
                    echo json_encode(array("success" => true, "msg" => "Document fields successfully updated in selected container!", "type" => "document"));
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
                    exit;
                }  
            }else{
                if ($result) {
                    echo json_encode(array("success" => true, "msg" => "Document uploaded successfully!", "type" => "document"));
                    exit;
                } else {
                    echo json_encode(array("success" => false, "msg" => "something went wrong!"));
                    exit;
                }  
            }
        }else{
            echo json_encode(array("success" => false, "msg" => "Select Atleast one container!"));
            exit;
        }   
    }
    
    function getcontainersSKUDataDetails($document_type_id = '', $document_number = '', $container_id = array()) {
        if ($document_type_id == '2' || $document_type_id == '3') {
            $main_table = array("tbl_document_uploaded_files as dluf", array("dluf.container_id,dluf.document_type_id"));
        } else {
            $main_table = array("tbl_document_ls_uploaded_files as dluf", array("dluf.container_id,dluf.document_type_id")); //dluf.container_id,
        }
        $condition = "1=1  AND dluf.uploaded_document_number = '" . $document_number . "' AND dluf.container_id IN (".$_POST['hold_containerIds'].") ";

        $join_tables = array(
            array("", "tbl_container_sku as csku", "csku.container_id = dluf.container_id", array('csku.*')),
            array("", "tbl_sku as sku", "sku.sku_id = csku.sku_id", array('sku.sku_number,sku.manufacturer_id')),
            array("", "tbl_container as c", "c.container_id = dluf.container_id", array('c.*')),
            array("", "tbl_order as o", "o.order_id = c.order_id", array()),
            array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array()),
            array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias,bp.business_name,bp.business_partner_id')),
            array("left", "tbl_business_partner as ff", "ff.business_partner_id = c.freight_forwarder_id", array('ff.business_partner_id as ff_id, ff.alias as ff_name')),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, '', 'csku.container_sku_id');
        $containerData = $this->common->MySqlFetchRow($rs, "array");
        // echo $this->db->last_query();exit;
        $resultArr = array();
        foreach ($containerData as $value) {
            if ($value['document_type_id'] == '1' || $value['document_type_id'] == '2' || $value['document_type_id'] == '3' || $value['document_type_id'] == '4' || $value['document_type_id'] == '5' || $value['document_type_id'] == '7' || $value['document_type_id'] == '10' || $value['document_type_id'] == '8') {
                $resultArr[$value['container_id']][$value['container_sku_id']] = $value;
            }
        }
        //echo '<pre>'; print_r($resultArr);die;
        return $resultArr;
    }
    
    function checkPreviousContainerStatus(){  
        $last_container_status_id = $_GET['last_container_status_id'];
        $container_status_id = $_GET['container_status_id']; 
        
         /* get status_squence of container */
        $condition = " container_status_id = '" . $container_status_id . "' ";
        $getContainerStatus = $this->common->getData('tbl_container_status', 'status_squence', $condition);  
        
        $condition = " container_status_id = '" . $last_container_status_id . "' ";
        $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status', 'status_squence', $condition);
        $lastContainerStatusSquence = 0;
        if (!empty($lastContainerStatusSquenceData)) {
            $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
        } 
        /* check previous status squence should less than from selected status squence*/
        if ($lastContainerStatusSquence > $getContainerStatus[0]['status_squence']) {
           echo json_encode(array('success' => false, 'msg' => "Can't set previous container status."));
           exit;
        }else{
           echo json_encode(array('success' => true, 'msg' => ''));
           exit;
        } 
    }

}

?>
