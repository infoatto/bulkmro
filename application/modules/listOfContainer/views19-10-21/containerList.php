    <?php  if(!empty($ListOfContainerData) &&  isset($ListOfContainerData)){
          foreach ($ListOfContainerData as $key => $value) { 
               //echo "<pre>";
               //print_r($value['documentType']);
              ?>
              <div class="container-single-wrapper">
                  <div class="cs-info">
                  <div class="cs-info-cta">
                      <?php if ($this->privilegeduser->hasPrivilege("ContainersDetailsView")) { ?>
                      <a href="<?= base_url()?>container_details?text='<?= rtrim(strtr(base64_encode("id=".$value['containerData']['container_id']), '+/', '-_'), '=')?>' ">
                      <img src="<?php echo base_url(); ?>assets/images/view-btn-icon.svg" alt=""></a>
                      <?php } ?>
                      <?php if ($this->privilegeduser->hasPrivilege("ContainersDetailsAddEdit")) { ?>
                          <!-- <a href="#/"><img src="assets/images/upload-btn-icon.svg" alt=""></a> -->
                      <?php } ?>
                     
                  </div>
                  <div class="cs-status-text"> 
                    <p class="container-number"><?= $value['containerData']['container_number'] ?></p>
                    <p class="container-status"><?= (!empty($value['containerStatus'])?$value['containerStatus']:"") ?></p>
                    <p class="last-update-title">Last Updated</p>
                    <p class="last-update-name"><?= $value['updatedBy'] ?></p>
                    <p class="last-update-date"><?= $value['updatedOn'] ?></p>
                  </div>
                  </div>
                  <div class="cs-status-wrapper">
                  <div class="connector"></div>
                      <?php if($value['documentType']){ 
                          foreach ($value['documentType'] as $dockey => $docval) { 
                          
                                //ravendra commit start
                                $colorClass = "css-grey";
                                $containerId = $value['containerData']['container_id'];
                                if(!empty($DocumentStatusData[$containerId][$docval['document_type_id']])){
                                    if($DocumentStatusData[$containerId][$docval['document_type_id']]["status"] == "Unchecked"){
                                        $colorClass = "css-yellow";
                                    }else if($DocumentStatusData[$containerId][$docval['document_type_id']]["status"] == "NotSet"){
                                        $colorClass = "css-red";
                                    }else{
                                        $colorClass = "css-green";
                                    }
                                }
                                if(!empty($containers[$containerId]['job_not_completed_document_type_id'])){
                                  if (in_array($docval['document_type_id'],$containers[$containerId]['job_not_completed_document_type_id'])){
                                    $colorClass = "css-red";
                                   }
                                }
                                ?>
                                <!-- //ravendra commit end -->
                          
                              
                            <div class="css-single">
                                <div class="css-flag <?=$colorClass?>"></div>
                                <div class="css-single-data">
                                <p class="css-single-data-title"><?= $docval['document_type_name'] ?></p>
                                <?php
                             
                                if(!empty($getuploaded_Document[$value['containerData']['container_id']][$docval['document_type_id']][0]['document_type_id'])){ 
                                  foreach ($getuploaded_Document[$value['containerData']['container_id']] as $uploadkey => $uploadval) { 
                                    
                                    if($docval['document_type_id'] == $uploadval[0]['document_type_id']){ ?>
                                    
                                      <p class="css-single-data-text"><?=  str_replace(',','<br/>',$uploadval[0]['uploaded_document_number']);?></p>
                                      <p class="css-single-data-date"><?= date('d-M-Y',strtotime($uploadval[0]['created_on']))?></p> 
                                  
                                    <?php } ?>

                                  <?php } ?> 
                                <?php } ?>
                              </div>
                          </div>
                          <?php  }
                          //  document type end here 
                      } ?>
                  </div>
              </div>
          <?php }

      }else{ ?>
                 <div class="container-single-wrapper">
                     <h4>Data not found !!</h4>
                 </div>
      <?php } ?>
          