<?php
if($formView){ ?>
    <div id="tabs">
        <ul>
            <?php  foreach ($formView as $key => $value) { ?>
                <li data-tab="tabs-<?=$key?>"><span><?= (!empty($value['sub_document_name'])?$value['sub_document_name']:$value['document_name'])?></a></li> 
            <?php  } ?>
        </ul>

        <?php  foreach ($formView as $key => $value) { ?>
            <div class="tabs-<?=$key?> tabs-modal">
                <iframe src="<?= base_url('container_document/').$value['uploaded_document_file']?>#zoom=FitH" style="width:100%;height:700px;"></iframe>
            </div> 
        <?php  } ?>
    </div>
<?php }else{ ?>
    <div>
        <p><h3>No Preview Available</h3></p>
    </div>
<?php } ?>

<script>

    // $("#tabs").tabs().hide();
</script>