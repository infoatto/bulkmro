<?PHP

class ListOfContainermodel extends CI_Model {

    function getRecords($get) {

        $table = "tbl_document_type";
        $default_sort_column = 'dt.document_type_squence';
        $default_sort_order = 'Asc';
        $condition = "1=1 ";

        $colArray = array('dt.document_type_name', 'dt.status');
        $sortArray = array('dt.document_type_name', 'dt.status');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

        for ($i = 0; $i < count($colArray); $i++) {
            if (isset($_GET['Searchkey_' . $i]) && $_GET['Searchkey_' . $i] != "") {
                $condition .= " AND " . $colArray[$i] . " LIKE '%" . $_GET['Searchkey_' . $i] . "%' ";
            }
        }

        $this->db->select('dt.*,group_concat(sdt.sub_document_type_name) as sub_document_type');
        $this->db->from("$table as dt");
        $this->db->join("tbl_sub_document_type as sdt","sdt.document_type_id = dt.document_type_id","left");
        $this->db->where("($condition)");
        $this->db->group_by('dt.document_type_id');
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page);
        $query = $this->db->get();
        // echo $this->db->last_query();exit; 

        $this->db->select('dt.*,group_concat(sdt.sub_document_type_name) as sub_document_type');
        $this->db->from("$table as dt");
        $this->db->join("tbl_sub_document_type as sdt","sdt.document_type_id = dt.document_type_id","left");
        $this->db->where("($condition)");
        $this->db->group_by('dt.document_type_id');
        $this->db->order_by($sort, $order);
        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    }
    
    function getContainers(){
        $condition = " c.status = 'Active' ";
        $this->db->select('c.*');
        $this->db->from("tbl_container as c"); 
        
        //condition for Customer and Supplier roles user
        if($_SESSION["mro_session"]['user_role'] =='Customer'){
            $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
            $condition .= " AND bp.business_partner_id = ".$bpID;  
            $this -> db -> join("tbl_order as o","o.order_id = c.order_id","left");
            $this -> db -> join("tbl_bp_order as bpo","bpo.bp_order_id = o.customer_contract_id","left");
            $this -> db -> join("tbl_business_partner as bp","bp.business_partner_id = bpo.business_partner_id","left");
        }else if($_SESSION["mro_session"]['user_role'] =='Supplier'){
            $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
            $condition .= " AND bp.business_partner_id = ".$bpID;  
            $this -> db -> join("tbl_order as o","o.order_id = c.order_id","left");
            $this -> db -> join("tbl_bp_order as bpo","bpo.bp_order_id = o.supplier_contract_id","left");
            $this -> db -> join("tbl_business_partner as bp","bp.business_partner_id = bpo.business_partner_id","left");
        }   
        
        $this->db->where("($condition)"); 
        $this->db->order_by("c.container_id DESC"); 
        $this->db->limit("50"); 
        $query = $this->db->get();
        
        //total records query
        $this->db->select('c.container_id');
        $this->db->from("tbl_container as c");  
        //condition for Customer and Supplier roles user
        if($_SESSION["mro_session"]['user_role'] =='Customer'){
            $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
            $condition .= " AND bp.business_partner_id = ".$bpID;  
            $this -> db -> join("tbl_order as o","o.order_id = c.order_id","left");
            $this -> db -> join("tbl_bp_order as bpo","bpo.bp_order_id = o.customer_contract_id","left");
            $this -> db -> join("tbl_business_partner as bp","bp.business_partner_id = bpo.business_partner_id","left");
        }else if($_SESSION["mro_session"]['user_role'] =='Supplier'){
            $bpID = (!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);
            $condition .= " AND bp.business_partner_id = ".$bpID;  
            $this -> db -> join("tbl_order as o","o.order_id = c.order_id","left");
            $this -> db -> join("tbl_bp_order as bpo","bpo.bp_order_id = o.supplier_contract_id","left");
            $this -> db -> join("tbl_business_partner as bp","bp.business_partner_id = bpo.business_partner_id","left");
        }    
        $this->db->where("($condition)"); 
        $query1 = $this->db->get();
        
        //echo $this->db->last_query();exit;  
        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result_array(), "totalRecords" => $totcount);
        } else {
            return array("query_result" => array(), "totalRecords" => 0);
        }
        
    }
    
    //ravendra commit start
    function getDocumentInContainer($container_id) {  
//        $condition = "cds.container_id=".$container_id." ";
//        $this->db->select('dt.document_type_id,IFNULL(cds.container_id, "0") as container_id,IFNULL(cds.status, "NotSet") as status,dt.document_type_squence,c.last_container_status_id');
//        $this->db->from("tbl_document_type as dt");
//        $this->db->join("tbl_container_uploaded_document_status as cds","dt.document_type_id = cds.document_type_id AND cds.container_id = $container_id ","left"); 
//        $this->db->join("tbl_container as c","c.container_id = cds.container_id","left");  
//        $this->db->where("($condition)");
//        $this->db->group_by('dt.document_type_id');
        
        $condition = "cds.container_id=".$container_id." ";
        $this->db->select('cds.document_type_id,IFNULL(cds.status, "NotSet") as status,cds.document_type_squence');
        $this->db->from("tbl_container_uploaded_document_status as cds");
        $this->db->where("($condition)");
        $this->db->group_by('cds.document_type_id');        
        $query = $this->db->get();
        //echo $this->db->last_query();exit; 
        if ($query->num_rows() > 0) { 
            return $query->result_array();
        } else {      
            return 0;
        } 
    }
    //ravendra commit end
    
    function getFRIandLS(){ 
        $this->db->select('container_id,uploaded_document_number,document_type_id');
        $this->db->from("tbl_document_uploaded_files"); 
        $this->db->group_by('uploaded_document_number');
        $query = $this->db->get(); 
        if ($query->num_rows() > 0) { 
            return $query->result_array();
        } else {      
            return 0;
        } 
    }
    
    function getContainerStatus($container_id) {
        $condition = " csl.container_id = " . $container_id . " "; 
        $this->db->select('cs.container_status_name,csl.status_log_id,csl.container_status_id');
        $this->db->from("tbl_container_status_log as csl");
        $this->db->join("tbl_container_status as cs", "cs.container_status_id = csl.container_status_id"); 
        $this->db->where("($condition)"); 
        $this->db->order_by("csl.status_log_id DESC"); 
        $this->db->limit("1"); 
        $query = $this->db->get();
        //echo $this->db->last_query();exit; 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getLinkContainers($documentNumberString = '',$type='') {
        $table = "tbl_document_ls_uploaded_files";
        if($type=='LS'){
            $table = "tbl_document_uploaded_files";
        }            
        $condition = " uploaded_document_number in (" . $documentNumberString . ") "; 
        $this->db->select('container_id');
        $this->db->from("$table"); 
        $this->db->where("($condition)"); 
        $this->db->group_by("container_id"); 
        $this->db->order_by("container_id");
        $query = $this->db->get();
        //echo $this->db->last_query();exit; 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    } 
    
    function getLastContainerStatus($container_id,$session_id='') {
        $condition = " log.container_id = " . $container_id . " AND log.current_session_id <> '".$session_id."' "; 
        $this->db->select('log.container_status_id,log.created_by,log.created_on');
        $this->db->from("tbl_container_status_log as log");
        $this->db->join("tbl_container_status as cs", "cs.container_status_id = log.container_status_id"); 
        $this->db->where("($condition)"); 
        $this->db->order_by("cs.status_squence DESC"); 
        $this->db->limit("1"); 
        $query = $this->db->get();
        //echo $this->db->last_query();exit; 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function filterCondition($post){
        $condition = "1=1 "; 
        $join = ""; 
        
        if(!empty($post['revised_eta'])){ 
            $condition .= " AND DATE_FORMAT(c.revised_eta, '%Y-%m-%d') = '".$post['revised_eta']."' ";
        }  
        
        if(!empty($post['container_id'])){ 
            $container_id = implode(",",$post['container_id']);
            $condition .= " AND c.container_id IN (".$container_id.") ";  
        } 
        
        if(!empty($post['fri']) && isset($post['ls'])){
            $join .= " left join tbl_document_uploaded_files as ls on (ls.container_id = c.container_id) ";
            $condition .= " AND ls.uploaded_document_number = '".$post['fri']."' ";
        }
        if(!empty($post['ls']) && isset($post['fri'])){
            $join .= " left join tbl_document_uploaded_files as fri on (fri.container_id = c.container_id) ";
            $condition .= " AND fri.uploaded_document_number = '".$post['ls']."' ";
        } 
        
        if(!empty($post['hbl']) && isset($post['hbl'])){
            $join .= " left join tbl_custom_dynamic_data  as cdd on (cdd.container_id = c.container_id) ";
            $condition .= " AND cdd.hbl_number = '".$post['hbl']."' ";
        }
        
        if(!empty($post['internal_hbl']) && isset($post['internal_hbl'])){
            $join .= " left join tbl_document_ls_uploaded_files as internal_hbl on (internal_hbl.container_id = c.container_id) ";
            $condition .= " AND internal_hbl.uploaded_document_number = '".$post['internal_hbl']."' ";
        }
        
        if(!empty($post['internal_fcr']) && isset($post['internal_fcr'])){
            $join .= " left join tbl_document_ls_uploaded_files as internal_fcr on (internal_fcr.container_id = c.container_id) ";
            $condition .= " AND internal_fcr.uploaded_document_number = '".$post['internal_fcr']."' ";
        }
         
        if(!empty($post['status']) && isset($post['status'])){ 
            $condition .= " AND  c.last_container_status_id  = ".$post['status']." ";
        }  
        
        $skuCnt = 0;
        if(!empty($post['product_id']) && !empty($post['brand_id'])){
            $join .= " left join tbl_container_sku as cs on (cs.container_id = c.container_id)  left join tbl_sku as s on (s.sku_id = cs.sku_id) ";
            $condition .= " AND s.sku_id = '".$post['product_id']."' AND s.brand_id = '".$post['brand_id']."' "; 
            $skuCnt++;
        }else if(!empty($post['product_id']) && empty($post['brand_id'])){
            $join .= " left join tbl_container_sku as cs on (cs.container_id = c.container_id)  left join tbl_sku as s on (s.sku_id = cs.sku_id) ";
            $condition .= " AND s.sku_id = '".$post['product_id']."' ";
            $skuCnt++;
        }else if(!empty($post['brand_id']) && empty($post['product_id'])){
            $join .= " left join tbl_container_sku as cs on (cs.container_id = c.container_id)  left join tbl_sku as s on (s.sku_id = cs.sku_id) ";
            $condition .= " AND s.brand_id = '".$post['brand_id']."' ";
            $skuCnt++;
        } 
        if($skuCnt > 0 && !empty($post['manufacturer']) && isset($post['manufacturer'])){
            $condition .= " AND s.manufacturer_id = '".$post['manufacturer']."' ";
        }else if ($skuCnt == 0 && !empty($post['manufacturer']) && isset($post['manufacturer'])){
            $join .= " left join tbl_container_sku as cs on (cs.container_id = c.container_id)  left join tbl_sku as s on (s.sku_id = cs.sku_id) ";
            $condition .= " AND s.manufacturer_id = '".$post['manufacturer']."' ";
        } 
        
        if(!empty($post['vessel_id']) && isset($post['vessel_id'])){
            $condition .= " AND c.vessel_name like '%".$post['vessel_id']."%' ";
        }
        if(!empty($post['freight_forwarder']) && isset($post['freight_forwarder'])){
            $condition .= " AND c.freight_forwarder_id like '%".$post['freight_forwarder']."%' ";
        } 
        
        //condition for Customer and Supplier roles user
        if($_SESSION["mro_session"]['user_role'] =='Customer'){            
            $join .= " left join tbl_order as o on (o.order_id = c.order_id) 
                       left join tbl_bp_order as bpo on (bpo.bp_order_id = o.customer_contract_id)                
                       left join tbl_business_partner as bp on (bp.business_partner_id = bpo.business_partner_id) ";  
            $condition .= " AND bp.business_partner_id = ".(!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);            
            
            if(!empty($post['bp_contract']) && isset($post['bp_contract'])){
                $condition .= " AND bpo.bp_order_id = ".$post['bp_contract']." ";
            } 
            if(!empty($post['contract']) && isset($post['contract'])){
                $condition .= " AND o.order_id = ".$post['contract']." ";
            }
            
        }else if($_SESSION["mro_session"]['user_role'] =='Supplier'){ 
            $join .= " left join tbl_order as o on (o.order_id = c.order_id) 
                       left join tbl_bp_order as bpo on (bpo.bp_order_id = o.supplier_contract_id)                
                       left join tbl_business_partner as bp on (bp.business_partner_id = bpo.business_partner_id) ";  
            $condition .= " AND bp.business_partner_id = ".(!empty($_SESSION["mro_session"][0]['business_partner_id'])?$_SESSION["mro_session"][0]['business_partner_id']:0);            
            if(!empty($post['bp_contract']) && isset($post['bp_contract'])){
                $condition .= " AND bpo.bp_order_id = ".$post['bp_contract']." ";
            } 
            if(!empty($post['contract']) && isset($post['contract'])){
                $condition .= " AND o.order_id = ".$post['contract']." ";
            }
        }else{
            $orderIdsStr = '';
            
            if(!empty($post['contract'])){     
                $orderIdsStr = $post['contract'];                
            }
            
            if(!empty($post['bp_contract']) && empty($orderIdsStr)){
                $orderIdsStr = getOrderIdsByBPContract($post['bp_contract']);
            } 
            
            if(!empty($post['bp']) && empty($orderIdsStr)){ 
                $orderIdsStr = getOrderIdsByBP($post['bp']);
            } 
            
            if($orderIdsStr != ''){
                $condition .= " AND c.order_id IN (".$orderIdsStr.") ";
            }
        }  
        
        return array("condition"=>$condition,"join"=>$join);
    }
    
    function getFilterData($post) {
        //echo '<pre>'; print_r($post);die;  
        $resultArr = $this->filterCondition($post); 
        $condition = $resultArr['condition'];
        $join = $resultArr['join'];
        
        $order_by = $post['order_by']; 
        
        $limit = '';
        if(!empty($post['f_clear']) || $condition == "1=1 " ){
            $limit = 'LIMIT 50';
        }
        
        $qry = "select c.container_id, c.container_number, c.last_container_status_id, c.last_status_by, c.last_status_date, container_status_name
                from tbl_container as c  
                ".$join." 
                left join tbl_container_status as status on (status.container_status_id = c.last_container_status_id)  
                where ".$condition."  
                group by c.container_id 
                order by c.container_number ".$order_by."  ".$limit." ";  
        //echo $qry;die;
        $query = $this->db->query($qry);
        
        //total records query
        $qry1 = "select c.container_id
                from tbl_container as c  
                ".$join." 
                left join tbl_container_status as status on (status.container_status_id = c.last_container_status_id)  
                where ".$condition." group by c.container_id  "; 
        $query1 = $this->db->query($qry1); 
         
        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result_array(), "totalRecords" => $totcount);
        } else {
            return array("query_result" => array(), "totalRecords" => 0);
        }
        
    } 
    
    function getAddressDetails($container_id = 0) {
        $condition = " c.container_id = " . $container_id . " ";
        $this->db->select('o.customer_contract_id,o.supplier_contract_id,o.customer_billing_details_id,o.customer_shipping_details_id,o.supplier_billing_details_id,o.supplier_shipping_details_id');
        $this->db->from("tbl_container as c");
        $this->db->join("tbl_order as o", "o.order_id = c.order_id"); 
        $this->db->where("($condition)");
        $query = $this->db->get(); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

}

?>
