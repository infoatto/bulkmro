<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("sku"); ?>">SKU List</a>
                                <span>></span>
                                <p>SKU Master</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">SKU Master</h1>
                            </div>
                        </div>
                        <button type="submit" class="btn-primary-mro">Save</button>
                    </div>
                    <div class="page-content-wrapper">
                        <input type="hidden" name="sku_id" id="sku_id" value="<?php echo(!empty($sku_details['sku_id'])) ? $sku_details['sku_id'] : ""; ?>">
                        <h3 class="form-group-title">Item Code</h3>
                        <div class="form-row form-row-3">
                            <div class="form-group">
                                <label for="">BM SKU Number<sup>*</sup></label>
                                <input type="text" name="sku_number" id="sku_number" value="<?php echo(!empty($sku_details['sku_number'])) ? $sku_details['sku_number'] : ""; ?>" placeholder="Enter BM SKU" class="input-form-mro">
                            </div>
                            <div class="form-group">
                                <label for="">Vendor SKU</label>
                                <input type="text" name="sku_vendor" id="sku_vendor" value="<?php echo(!empty($sku_details['sku_vendor'])) ? $sku_details['sku_vendor'] : ""; ?>" placeholder="Enter Vendor SKU" class="input-form-mro">
                            </div>
                            <div class="form-group">
                                <label for="Status">Status</label>
                                <select name="status" id="status" class="select-form-mro">
                                    <option value="Active" <?php echo(!empty($sku_details['status']) && $sku_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                    <option value="In-active" <?php echo(!empty($sku_details['status']) && $sku_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                </select>
                            </div>
                        </div>
                        <h3 class="form-group-title">Product Details</h3>
                        <div class="form-row form-row-3">
                            <div class="form-group">
                                <label for="">Category<sup>*</sup></label>
                                <div class="singleselect-dropdown-wrapper">
                                  <div class="sd-value category">
                                    Select Product Category
                                  </div>
                                  <div class="sd-list-wrapper">
                                    <input type="text" placeholder="Search" class="sd-search">
                                    <a href="<?php echo base_url("category/addEdit"); ?>" target="_blank" class="btn-secondary-mro sd-list-btn"><img src="<?php echo base_url(); ?>assets/images/add-icon-dark.svg" alt=""> Add New Category</a>                                            
                                    <div class="sd-list-items">
                                        <!--<div class="sdli-single">
                                            <label>Select Value</label>
                                        </div>-->
                                        <?php foreach ($categoryData as $value) { ?> 
                                            <div class="sdli-single" onclick="setCategoryId('<?= $value['category_id'] ?>')">
                                                <label id="category_<?= $value['category_id'] ?>"><?= $value['category_name'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>  
                                    </div>
                                  </div>
                                </div>
                                <input type="text" name="category_id"  id="category_id" value="" class="error_validation">
                            </div>
                            
                            <div class="form-group">
                                <label for="">Brand Name<sup>*</sup></label>
                                <div class="singleselect-dropdown-wrapper">
                                  <div class="sd-value brand">
                                    Select Brand Name
                                  </div>
                                  <div class="sd-list-wrapper">
                                    <input type="text" placeholder="Search" class="sd-search">
                                    <a href="<?php echo base_url("brand/addEdit"); ?>" target="_blank" class="btn-secondary-mro sd-list-btn"><img src="<?php echo base_url(); ?>assets/images/add-icon-dark.svg" alt=""> Add New Brand</a>                                            
                                    <div class="sd-list-items"> 
                                        <?php foreach ($brandData as $value) { ?> 
                                            <div class="sdli-single" onclick="setBrandId('<?= $value['brand_id'] ?>')">
                                                <label id="brand_<?= $value['brand_id'] ?>"><?= $value['brand_name'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>  
                                    </div>
                                  </div>
                                </div>
                                <input type="text" name="brand_id"  id="brand_id" value="" class="error_validation">
                            </div>
                            
                            <div class="form-group">
                                <label for="">Manufacturer<sup>*</sup></label>
                                <div class="singleselect-dropdown-wrapper">
                                  <div class="sd-value manufacturer">
                                    Select Manufacturer
                                  </div>
                                  <div class="sd-list-wrapper">
                                    <input type="text" placeholder="Search" class="sd-search">
                                    <a href="<?php echo base_url("business_partner/addEdit"); ?>" target="_blank" class="btn-secondary-mro sd-list-btn"><img src="<?php echo base_url(); ?>assets/images/add-icon-dark.svg" alt=""> Add New Manufacturer</a>                                            
                                    <div class="sd-list-items"> 
                                        <?php foreach ($businessPartnerData as $value) { ?> 
                                            <div class="sdli-single" onclick="setManufacturerId('<?= $value['business_partner_id'] ?>')">
                                                <label id="manufacturer_<?= $value['business_partner_id'] ?>"><?= $value['alias'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>  
                                    </div>
                                  </div>
                                </div>
                                <input type="text" name="manufacturer_id"  id="manufacturer_id" value="" class="error_validation">
                            </div> 
                        </div>
                        <div class="form-row form-row-3">
                            <div class="form-group">
                                <label for="">Product Name<sup>*</sup></label>
                                <input type="text" name="product_name" id="product_name" value="<?php echo(!empty($sku_details['product_name'])) ? $sku_details['product_name'] : ""; ?>" placeholder="Enter Product Name" class="input-form-mro">
                            </div>
                            <div class="form-group">
                                <label for="">Size</label>
                                <div class="singleselect-dropdown-wrapper">
                                  <div class="sd-value size">
                                    Select Size
                                  </div>
                                  <div class="sd-list-wrapper">
                                    <input type="text" placeholder="Search" class="sd-search">
                                    <a href="<?php echo base_url("size/addEdit"); ?>" target="_blank" class="btn-secondary-mro sd-list-btn"><img src="<?php echo base_url(); ?>assets/images/add-icon-dark.svg" alt=""> Add New Size</a>                                            
                                    <div class="sd-list-items"> 
                                        <?php foreach ($sizeData as $value) { ?> 
                                            <div class="sdli-single" onclick="setSizeId('<?= $value['size_id'] ?>')">
                                                <label id="size_<?= $value['size_id'] ?>"><?= $value['size_name'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>  
                                    </div>
                                  </div>
                                </div>
                                <input type="text" name="size_id"  id="size_id" value="" class="error_validation">
                            </div> 
                            
                            <div class="form-group">
                                <label for="">UoM</label>
                                <div class="singleselect-dropdown-wrapper">
                                  <div class="sd-value uom">
                                    Select UoM
                                  </div>
                                  <div class="sd-list-wrapper">
                                    <input type="text" placeholder="Search" class="sd-search">
                                    <a href="<?php echo base_url("uom/addEdit"); ?>" target="_blank" class="btn-secondary-mro sd-list-btn"><img src="<?php echo base_url(); ?>assets/images/add-icon-dark.svg" alt=""> Add New UoM</a>                                            
                                    <div class="sd-list-items"> 
                                        <?php foreach ($uomData as $value) { ?> 
                                            <div class="sdli-single" onclick="setUoMId('<?= $value['uom_id'] ?>')">
                                                <label id="uom_<?= $value['uom_id'] ?>"><?= $value['uom_name'] ?></label>
                                            </div>
                                            <?php
                                        }
                                        ?>  
                                    </div>
                                  </div>
                                </div>
                                <input type="text" name="uom_id"  id="uom_id" value="" class="error_validation">
                            </div>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>

<script>

    $(document).ready(function () {

        var vRules = {
            "sku_number": {required: true},
            "category_id": {required: true},
            "brand_id": {required: true},
            "manufacturer_id": {required: true},
            "product_name": {required: true}
        };
        var vMessages = {
            "sku_number": {required: "Please enter BM SKU number."},
            "category_id": {required: "Please select category."},
            "brand_id": {required: "Please select brand name."},
            "manufacturer_id": {required: "Please select manufacturer."},
            "product_name": {required: "Please enter product name."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>sku/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        console.log(response);
                        if (response.success) {
                            alert(response.msg);
                            setTimeout(function () {
                                window.location = "<?= base_url('sku') ?>";
                            }, 1000);
                        } else {
                            alert(response.msg);
                        }
                    }
                });
            }
        });

    });
    //set category id for save in table
    function setCategoryId(val){
        if (val) {
            $("#category_id").val(val);
            var payment_terms = $("#category_" + val).text();
            $('.category').html(payment_terms);
        } else {
            alert("select the value first");
        }
    }
    //set brand id for save in table
    function setBrandId(val){
        if (val) {
            $("#brand_id").val(val);
            var payment_terms = $("#brand_" + val).text();
            $('.brand').html(payment_terms);
        } else {
            alert("select the value first");
        }
    }
    //set manufacturer id for save in table
    function setManufacturerId(val){
        if (val) {
            $("#manufacturer_id").val(val);
            var payment_terms = $("#manufacturer_" + val).text();
            $('.manufacturer').html(payment_terms);
        } else {
            alert("select the value first");
        }
    }
    
    function setSizeId(val){
        if (val) {
            $("#size_id").val(val);
            var payment_terms = $("#size_" + val).text();
            $('.size').html(payment_terms);
        } else {
            alert("select the value first");
        }
    }
    
    function setUoMId(val){
        if (val) {
            $("#uom_id").val(val);
            var payment_terms = $("#uom_" + val).text();
            $('.uom').html(payment_terms);
        } else {
            alert("select the value first");
        }
    }
    
    <?php if (!empty($sku_details['sku_id'])) { ?>
            setCategoryId('<?= $sku_details['category_id'] ?>');
            setBrandId('<?= $sku_details['brand_id'] ?>');
            setManufacturerId('<?= $sku_details['manufacturer_id'] ?>');
            setSizeId('<?= $sku_details['size_id'] ?>');
            setUoMId('<?= $sku_details['uom_id'] ?>');
    <?php } ?>
</script>