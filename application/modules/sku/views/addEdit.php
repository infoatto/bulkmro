<?php
//start code of view option    
$dropDownCss = '';
$inputFieldCss = '';
if ($view == 1) {
    $dropDownCss = "disabled";
    $inputFieldCss = "readonly";
}
//end code of view option 
?>
<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("sku"); ?>">SKU List</a>
                                <span>></span>
                                <p>SKU Master</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">SKU Master</h1>
                            </div>
                        </div>
                        <div class="title-sec-right">
                            <?php if($view==0){ ?>
                                <button type="submit" class="btn-primary-mro">Save</button>
                                <a href="<?php echo base_url("sku"); ?>" class="btn-transparent-mro cancel">Cancel</a>
                            <?php } ?>
                        </div> 
                    </div>
                    <div class="page-content-wrapper">
                        <input type="hidden" name="sku_id" id="sku_id" value="<?php echo(!empty($sku_details['sku_id'])) ? $sku_details['sku_id'] : ""; ?>">
                        <h3 class="form-group-title">Item Code</h3>
                        <div class="form-row form-row-3">
                            <div class="form-group">
                                <label for="">BM SKU Number<sup>*</sup></label>
                                <input type="text" name="sku_number" id="sku_number" value="<?php echo(!empty($sku_details['sku_number'])) ? $sku_details['sku_number'] : ""; ?>" placeholder="Enter BM SKU" class="input-form-mro" <?= $inputFieldCss; ?>>
                            </div>
                            <div class="form-group">
                                <label for="">Vendor SKU</label>
                                <input type="text" name="sku_vendor" id="sku_vendor" value="<?php echo(!empty($sku_details['sku_vendor'])) ? $sku_details['sku_vendor'] : ""; ?>" placeholder="Enter Vendor SKU" class="input-form-mro" <?= $inputFieldCss; ?>>
                            </div>
                            <div class="form-group">
                                <label for="Status">Status</label>
                                <select name="status" id="status" class="select-form-mro" <?= $dropDownCss; ?>>
                                    <option value="Active" <?php echo(!empty($sku_details['status']) && $sku_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                    <option value="In-active" <?php echo(!empty($sku_details['status']) && $sku_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                </select>
                            </div>
                        </div>
                        <h3 class="form-group-title">Product Details</h3>
                        <div class="form-row form-row-3">
                            <div class="form-group">
                                <label for="category_id">Category<sup>*</sup></label> 
                                <select name="category_id" id="category_id" class="basic-single select-form-mro" onchange="removeErroMsg('category_id');" <?= $dropDownCss; ?>>
                                    <option value="" disabled selected hidden>Select Product Category</option>
                                    <?php
                                    foreach ($categoryData as $value) {
                                        $categoryID = (isset($sku_details['category_id']) ? $sku_details['category_id'] : 0);
                                        ?> 
                                        <option value="<?php echo $value['category_id']; ?>" <?php
                                        if ($value['category_id'] == $categoryID) {
                                            echo "selected";
                                        }
                                        ?>>
                                        <?php echo $value['category_name']; ?> 
                                        </option> 
                                        <?php
                                    }
                                    ?>
                                </select> 
                            </div>
                            <div class="form-group">
                                <label for="">Brand Name<sup>*</sup></label>
                                <select name="brand_id" id="brand_id" class="basic-single select-form-mro" onchange="removeErroMsg('brand_id');" <?= $dropDownCss; ?>>
                                    <option value="" disabled selected hidden>Select Brand Name</option>
                                    <?php
                                    foreach ($brandData as $value) {
                                        $brandID = (isset($sku_details['brand_id']) ? $sku_details['brand_id'] : 0);
                                        ?> 
                                        <option value="<?php echo $value['brand_id']; ?>" <?php
                                        if ($value['brand_id'] == $brandID) {
                                            echo "selected";
                                        }
                                        ?>>
                                        <?php echo $value['brand_name']; ?> 
                                        </option> 
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Manufacturer<sup>*</sup></label>
                                <select name="manufacturer_id" id="manufacturer_id" class="basic-single select-form-mro" onchange="removeErroMsg('manufacturer_id');" <?= $dropDownCss; ?>>
                                    <option value="" disabled selected hidden>Select Manufacturer</option>
                                    <?php
                                    foreach ($businessPartnerData as $value) {
                                        $businessPartnerID = (isset($sku_details['manufacturer_id']) ? $sku_details['manufacturer_id'] : 0);
                                        ?> 
                                        <option value="<?php echo $value['business_partner_id']; ?>" <?php
                                        if ($value['business_partner_id'] == $businessPartnerID) {
                                            echo "selected";
                                        }
                                        ?>>
                                        <?php echo $value['alias']; ?> 
                                        </option> 
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-row form-row-3">
                            <div class="form-group">
                                <label for="">Product Name<sup>*</sup></label>
                                <input type="text" name="product_name" id="product_name" value="<?php echo(!empty($sku_details['product_name'])) ? $sku_details['product_name'] : ""; ?>" placeholder="Enter Product Name" class="input-form-mro" <?= $inputFieldCss; ?>>
                            </div>
                            <div class="form-group">
                                <label for="">Size</label>
                                <select name="size_id" id="size_id" class="basic-single select-form-mro" <?= $dropDownCss; ?>> 
                                    <option value="" disabled selected hidden>Select Size</option>
                                    <?php
                                    foreach ($sizeData as $value) {
                                        $sizeID = (isset($sku_details['size_id']) ? $sku_details['size_id'] : 0);
                                        ?> 
                                        <option value="<?php echo $value['size_id']; ?>" <?php
                                        if ($value['size_id'] == $sizeID) {
                                            echo "selected";
                                        }
                                        ?>>
                                        <?php echo $value['size_name']; ?> 
                                        </option> 
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">UoM</label>
                                <select name="uom_id" id="uom_id" class="basic-single select-form-mro" <?= $dropDownCss; ?>> 
                                    <option value="" disabled selected hidden>Select UoM</option>
                                    <?php
                                    foreach ($uomData as $value) {
                                        $uomID = (isset($sku_details['uom_id']) ? $sku_details['uom_id'] : 0);
                                        ?> 
                                        <option value="<?php echo $value['uom_id']; ?>" <?php
                                        if ($value['uom_id'] == $uomID) {
                                            echo "selected";
                                        }
                                        ?>>
                                        <?php echo $value['uom_name']; ?> 
                                        </option> 
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>

<script>

    $(document).ready(function () {

        var vRules = {
            "sku_number": {required: true},
            "category_id": {required: true},
            "brand_id": {required: true},
            "manufacturer_id": {required: true},
            "product_name": {required: true},
//            "size_id": {required: true},
//            "uom_id": {required: true}
        };
        var vMessages = {
            "sku_number": {required: "Please enter BM SKU number."},
            "category_id": {required: "Please select category."},
            "brand_id": {required: "Please select brand name."},
            "manufacturer_id": {required: "Please select manufacturer."},
            "product_name": {required: "Please enter product name."},
//            "size_id": {required: "Please select size."},
//            "uom_id": {required: "Please select UoM."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            errorClass: 'error',
            errorElement: 'label',
            errorPlacement: function(error, e) {
              e.parents('.form-group').append(error);
            },
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>sku/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('sku') ?>";
                            }, 3000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });
</script>
