<?PHP

class Citymodel extends CI_Model {

    function getRecords($get) { 
        $table = "tbl_city";
        $default_sort_column = 'c.city_id';
        $default_sort_order = 'c.city_name desc';
        $condition = "1=1 ";
        $sortArray = array('c.city_name');

        $colArray = array('c.city_name', 's.state_id');
        $sortArray = array('c.city_name', 'ctry.country_name', 's.state_name');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

        for ($i = 0; $i < count($colArray); $i++) {
            if (isset($_GET['Searchkey_' . $i])) { //&& $_GET['Searchkey_'.$i] !=""
                $condition .= " AND " . $colArray[$i] . " LIKE '%" . $_GET['Searchkey_' . $i] . "%' ";
            }
        }

        $this->db->select('c.*,s.state_name,ctry.country_name');
        $this->db->from("$table as c");
        $this->db->join("tbl_state as s", "s.state_id = c.state_id");
        $this->db->join("tbl_country as ctry", "ctry.country_id = c.country_id");
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page);
        $query = $this->db->get();
        //echo $this->db->last_query();exit; 

        $this->db->select('c.*,s.state_name,ctry.country_name');
        $this->db->from("$table as c");
        $this->db->join("tbl_state as s", "s.state_id = c.state_id");
        $this->db->join("tbl_country as ctry", "ctry.country_id = c.country_id");
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    }

    function getData($get = array()) {
        $table = "tbl_city";
        $default_sort_column = 'c.city_id';
        $default_sort_order = 'desc';
        $condition = "1=1 ";

        if (isset($_GET['city_id'])) { //&& $_GET['city_id'] !=""
            $condition .= " AND c.city_id =" . $_GET['city_id'] . " ";
        }

        $this->db->select('c.*');
        $this->db->from("$table as c");
        $this->db->where("($condition)");
        $this->db->order_by('c.city_name', 'desc');
        //$this->db->limit(10,1); 
        $query = $this->db->get();
        //echo $this->db->last_query();exit;  

        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return 0;
        }
    }

}

?>
