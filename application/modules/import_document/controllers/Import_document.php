<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Import_document extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Import_documentmodel', 'import_documentmodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("DocumentImport")){
            redirect('dashboard');
        }
    }

    function index() {
        $brandData = array();
                 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('import_document/index', $brandData);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    }  
    
    function getContainerSKUQty($sku_id,$contract_id,$quantity=0,$isDuplicate=''){  
        $totalContainerQty = 0;
        $contractQty = 0;        
        $condition = "c.order_id=".$contract_id."  AND csku.sku_id = '".$sku_id."' ";
        $main_table = array("tbl_container_sku as csku", array('csku.quantity'));
        $join_tables = array(
            array("inner","tbl_container as c","c.container_id = csku.container_id", array("c.order_id")),
        );

        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,"",'');
        $containerSKUQty = $this->common->MySqlFetchRow($rs, "array"); // fetch result  
        //echo $this->db->last_query();exit;
        if(!empty($containerSKUQty)){
            foreach ($containerSKUQty as $value) {
                $totalContainerQty += $value['quantity'];
            } 
        }
        
        $contractSKUQty = $this->common->getData("tbl_order_sku", "quantity", array("order_id " => (int) $contract_id,"sku_id" => $sku_id)); 
        if(!empty($contractSKUQty)){
            foreach ($contractSKUQty as $value) {
                $contractQty += $value['quantity'];
            } 
        }
        
        if($isDuplicate=='Duplicate'){
            $qty = 0;
            $totalQty = $contractQty - $totalContainerQty;
            if($totalQty < 0){
                $qty = 0;
            }else{  
                if($totalQty < $quantity){ 
                    $qty = $totalQty;
                }else{ 
                    $qty = $quantity;
                } 
            }
            //echo $qty;die;
            return $qty; 
        }else{
            $totalQty = $contractQty - $totalContainerQty;
            if($totalQty < 0){
                $totalQty = 0;
            } 
            return $totalQty;   
        }   
    }
    
    function LSFRI_Import() {
        
        $docTypeID = $_GET['type'];
        $current_session_id =  round(microtime(true) * 1000);
        if(!$this->privilegeduser->hasPrivilege("DocumentImport")){
            redirect('dashboard');
        }
        require_once('application/libraries/SimpleXLSX.php');
        
        if($docTypeID == 2){
            $excelfile = "excelfile_ls";
        }else if($docTypeID == 3){
            $excelfile = "excelfile_fri";
        }else if($docTypeID == 1){
            $excelfile = "excelfile_ins";
        }else if($docTypeID == 4){
            $excelfile = "excelfile_fcr";
        }else if($docTypeID == 5){
            $excelfile = "excelfile_hbl";
        }else if($docTypeID == 10){
            $excelfile = "excelfile_an";
        }else if($docTypeID == 7){
            $excelfile = "excelfile_7501";
        }else if($docTypeID == 9){
            $excelfile = "excelfile_abi";
        }else if($docTypeID == 8){
            $excelfile = "excelfile_do";
        } 
        
        $statusData = array();
        $target_dir  = "assets/excel/";
        $basename = basename($_FILES[$excelfile]["name"]);
        $target_file = $target_dir . $basename;
        if (move_uploaded_file($_FILES[$excelfile]["tmp_name"], $target_file)) {
            if (($handle = SimpleXLSX::parse("./assets/excel/".$basename))) {
                $imported = 0; $notimported = 0; $valid = 0; $error  = "";
                //echo '<pre>'; print_r($handle->rows(2)); die;
                //check for LS and FRI document
                $xlsx = new SimpleXLSX($_FILES[$excelfile]['tmp_name']);
                //start FRI and LS import code
                if($docTypeID == 2 || $docTypeID == 3){
                    //sheet tab first import code start
                    foreach ($handle->rows() as $key => $data) {
                        //check for header column value
                        if ($key == 0) {  
                            if (trim($data[1]) != "Container Number") {
                                $error .= "Import sheet not valid.";
                                $valid++;
                            } 
                            if ($valid != 0) {
                                $statusData['success'] = false;
                                $statusData['msg'] = $error; 
                                print_r(json_encode($statusData));
                                exit;
                            }   
                        }else{
                            //check for blank value of all column value of row.
                            if(empty($data[1])){
                                continue;
                            } 

                            if (trim($data[1]) == "") {
                                $error .= "Container Number cannot be empty in row : " . ($key + 1) . "<br>";
                                $valid++;
                            } 
                        }  
                    }
                    if ($valid != 0) {
                        $statusData['success'] = false;
                        $statusData['msg'] = $error;
                        print_r(json_encode($statusData));
                        exit;
                    } else {
                        foreach ($handle->rows() as $key => $data) {
                            //check for header row
                            if ($key == 0) {
                                continue;
                            }

                            //check for blank value of all column value of row. 
                            if(empty($data[1])){
                                continue;
                            }
                            
                            $containerNumber = str_replace("'", "&#39;", trim($data[1]));  
                            
                            $condition = "container_number = '" . $containerNumber . "' "; 
                            $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,last_container_status_id", $condition);
                            $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                            
                             if (!empty($rs_container[0]['container_id'])) {
                                $container_id = $rs_container[0]['container_id']; 
                                
                                $condition = " container_status_id = '". $rs_container[0]['last_container_status_id']."' ";
                                $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status','status_squence',$condition);
                                $lastContainerStatusSquence = 0;
                                if(!empty($lastContainerStatusSquenceData)){
                                   $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                }

                                $mapping_data = array();
                                $mapping_data['container_id'] = $container_id;
                                $mapping_data['document_type_id'] = $docTypeID; 
                                $mapping_data['current_session_id'] =  $current_session_id;  
                                $mapping_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $mapping_data['updated_on'] = date("Y-m-d H:i:s");
                                
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " "; 
                                $chk_mapping_sql = $this->common->Fetch("tbl_document_container_mapping", "document_container_mapping_id", $condition);
                                $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                                if (!empty($rs_mapping[0]['document_container_mapping_id'])) {
                                     //update data 
                                    $result = $this->common->updateData("tbl_document_container_mapping", $mapping_data, array("document_container_mapping_id" => $rs_mapping[0]['document_container_mapping_id']));                             
                                    
                                }else{  
                                    $mapping_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $mapping_data['created_on'] = date("Y-m-d H:i:s");  
                                    //insert data 
                                    $result = $this->common->insertData("tbl_document_container_mapping", $mapping_data, "1");                                     
                                   
                                } 
                                
                                //document status insert
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . "  "; 
                                $this->common->deleteRecord('tbl_container_uploaded_document_status',$condition); 
                                
                                if($docTypeID == 2){
                                    $type_squence = 2;
                                    $status_id = 4;
                                }else{
                                    $type_squence = 1;
                                    $status_id = 3;
                                }
                                
                                $doc_status_data = array();
                                $doc_status_data['container_id'] = $container_id;
                                $doc_status_data['document_type_id'] = $docTypeID;
                                $doc_status_data['sub_document_type_id'] = 0;
                                $doc_status_data['document_type_squence'] = $type_squence;
                                $doc_status_data['status'] = 'Unchecked';
                                $doc_status_data['current_session_id'] =   $current_session_id;
                                $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['created_on'] = date("Y-m-d H:i:s"); 
                                $result =  $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1");
                                
                                //container status log insert 
                                $condition = "container_id = " . $container_id . " AND container_status_id=" . $status_id . "  ";
                                $this->common->deleteRecord('tbl_container_status_log',$condition); 
                                
                                $ContainerStatus =array();
                                $ContainerStatus['container_id'] = $container_id;
                                $ContainerStatus['container_status_id'] = $status_id;
                                $ContainerStatus['current_session_id'] =   $current_session_id;
                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                $ContainerStatus['document_date'] = date("Y-m-d");
                                $result =  $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1"); 
                                
                                
                                if($lastContainerStatusSquence < $type_squence){ 
                                    //last status in container table
                                    $containerStatusUpdate = array();
                                    $containerStatusUpdate['last_container_status_id'] = $status_id; 
                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $container_id));
                                } 
                                
                             } 
                        } 
                    }  
                    
                    //sheet tab second import code start
                    $imported = 0; $notimported = 0; $valid = 0; $error  = "";  
                    foreach ($handle->rows(1) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        }  

                        $containerNumber = str_replace("'", "&#39;", trim($data[1]));                             
                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2])); 

                        $condition = "container_number = '" . $containerNumber . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                         if (!empty($rs_container[0]['container_id'])) {
                            $container_id = $rs_container[0]['container_id']; 

                            $doc_data = array(); 
                            $doc_data['uploaded_document_file'] = str_replace("'", "&#39;", trim($data[0]));
                            $doc_data['container_id'] = $container_id;
                            $doc_data['document_type_id'] = $docTypeID; 
                            $doc_data['current_session_id'] =  $current_session_id;  
                            $doc_data['uploaded_document_number'] =   $uploaded_document_name;
                            $doc_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $doc_data['updated_on'] = date("Y-m-d H:i:s");

                            $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND uploaded_document_number ='".$uploaded_document_name."' "; 
                            $chk_mapping_sql = $this->common->Fetch("tbl_document_uploaded_files", "document_uploaded_files_id", $condition);
                            $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                            if (!empty($rs_mapping[0]['document_uploaded_files_id'])) {
                                 //update data 
                                $result = $this->common->updateData("tbl_document_uploaded_files", $doc_data, array("document_uploaded_files_id" => $rs_mapping[0]['document_uploaded_files_id']));                             
                                
                            }else{  
                                $doc_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_data['created_on'] = date("Y-m-d H:i:s");  
                                //insert data 
                                $result = $this->common->insertData("tbl_document_uploaded_files", $doc_data, "1");   
                            } 
                            
                            if ($result) {
                                $imported++;
                            } else {
                                $notimported++;
                            }  

                         } 
                    }              
                     
                    foreach ($handle->rows(2) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0])){
                            continue;
                        }  

                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[0]));   
                        $condition = " document_type_id=" . $docTypeID . " AND uploaded_document_number ='".$uploaded_document_name."' "; 
                        $this->common->deleteRecord('tbl_custom_field_data_submission',$condition);   

                        //get custom_field_structure_id according to document type
                        $custom_field = array();
                        $condition = " document_type_id=" . $docTypeID . " AND status='Active' "; 
                        $chk_fields_sql = $this->common->Fetch("tbl_custom_field_structure", "custom_field_structure_id,custom_field_title", $condition);
                        $rs_fields = $this->common->MySqlFetchRow($chk_fields_sql, "array");
                        if (!empty($rs_fields)) {
                            foreach ($rs_fields as $value) {
                                if(trim($value['custom_field_title'])=='Date of Document'){
                                    $DateofDocument = '';
                                    if(!empty(str_replace("'", "&#39;", trim($data[1])))){
                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[1]))); 
                                        $DateofDocument = date('Y-m-d',$DateVal); 
                                    }
                                    $custom_field[$value['custom_field_structure_id']] = $DateofDocument;
                                } 
                                
                                if(trim($value['custom_field_title'])=='Place of Inspection'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[2])); 
                                }

                                if(trim($value['custom_field_title'])=='Date of Inspection'){
                                    $DateofInspection = '';
                                    if(!empty(str_replace("'", "&#39;", trim($data[3])))){
                                        $DateVal1 = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[3]))); 
                                        $DateofInspection = date('Y-m-d',$DateVal1); 
                                    }
                                    $custom_field[$value['custom_field_structure_id']] = $DateofInspection;
                                }
                                
                                if(trim($value['custom_field_title'])=='Lot Number'){ 
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[8])); 
                                }
                            }
                        } 
                        
                        //echo '<pre>'; print_r($custom_field); die;
                        //insert custom fields data
                        if (!empty($custom_field)) {
                            foreach ($custom_field as $key => $value) { 
                                $custom_data = array();
                                $custom_data['custom_field_structure_id'] = $key;
                                $custom_data['custom_field_structure_value'] =  $value; 
                                $custom_data['uploaded_document_number'] =  $uploaded_document_name;                            
                                $custom_data['document_type_id'] =   $docTypeID;
                                $custom_data['sub_document_type_id'] =  0;
                                $custom_data['current_session_id'] = $current_session_id;
                                $custom_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['updated_on'] = date("Y-m-d H:i:s");
                                $custom_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['created_on'] = date("Y-m-d H:i:s"); 
                                //insert data 
                                $result = $this->common->insertData("tbl_custom_field_data_submission", $custom_data, "1");   
                            }
                        } 
                          
                        $supplierName = str_replace("'", "&#39;", trim($data[4]));
                        $contactpersonname = str_replace("'", "&#39;", trim($data[5]));
                        $contactpersonnumber = str_replace("'", "&#39;", trim($data[6]));

                        //get containers according to document number
                        $condition = "uploaded_document_number = '".$uploaded_document_name."' And document_type_id = '".$docTypeID."' ";
                        $chk_container_sql = $this->common->Fetch("tbl_document_uploaded_files", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                        if (!empty($rs_container)) {
                            foreach ($rs_container as $value) {
                                $container_id = $value['container_id'];

                                //update supplier information in dynamic table
                                if(!empty($supplierName)){  
                                   $condition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Distributor','Manufacturer') AND alias ='".$supplierName."'  ";
                                   $chk_supplier_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id,contact_person,phone_number", $condition);
                                   $rs_supplier = $this->common->MySqlFetchRow($chk_supplier_sql, "array");
                                   if (!empty($rs_supplier)) {   
                                       $data_supp =array();
                                       $data_supp['supplier_id '] = $rs_supplier[0]['business_partner_id']; 
                                       $data_supp['supplier_contactperson'] = !empty($contactpersonname)?$contactpersonname:$rs_supplier[0]['contact_person'];
                                       $data_supp['supplier_contactnumber'] = !empty($contactpersonnumber)?$contactpersonnumber:$rs_supplier[0]['phone_number'];
                                       $data_supp['current_session_id'] = $current_session_id;  
                                       $condition = " container_id = ".$container_id." AND document_type_id = ".$docTypeID." ";
                                       $dynamic_data = $this->common->getData('tbl_custom_dynamic_data','custom_dynamic_data_id',$condition); 
                                       if(!empty($dynamic_data[0]['custom_dynamic_data_id'])){
                                           $condition = " custom_dynamic_data_id = ".$dynamic_data[0]['custom_dynamic_data_id']." ";
                                           $data_supp['updated_on'] = date("Y-m-d H:i:s");
                                           $data_supp['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                           $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition); 
                                       }else{ 
                                           $data_supp['container_id'] = $container_id;
                                           $data_supp['document_type_id'] = $docTypeID;
                                           $data_supp['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                           $data_supp['updated_on'] = date("Y-m-d H:i:s");
                                           $data_supp['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                           $data_supp['created_on'] = date("Y-m-d H:i:s"); 
                                           $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                                       } 

                                   }  
                               }  
                                            
                                $DateofInspection = '';
                                if(!empty(str_replace("'", "&#39;", trim($data[3])))){
                                    $DateVal1 = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[3]))); 
                                    $DateofInspection = date('Y-m-d',$DateVal1); 
                                }
                                //insert update date of inspection and place of inspection
                                $data_supp = array(); 
                                $data_supp['date_of_inspection'] = $DateofInspection; 
                                $data_supp['place_of_inspection'] = str_replace("'", "&#39;", trim($data[2])); 

                                $condition = " container_id = ".$container_id." AND document_type_id = ".$docTypeID." ";
                                $dynamic_data = $this->common->getData('tbl_custom_dynamic_data','custom_dynamic_data_id',$condition); 
                                if(!empty($dynamic_data[0]['custom_dynamic_data_id'])){
                                    $condition = " custom_dynamic_data_id = ".$dynamic_data[0]['custom_dynamic_data_id']." "; 
                                    $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition); 
                                }else{ 
                                    $data_supp['container_id'] = $container_id;
                                    $data_supp['document_type_id'] = $docTypeID; 
                                    $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                                }        

                                //update container information
                                $condition = " container_id = ".$container_id." ";
                                $chk_containerdata_sql = $this->common->Fetch("tbl_container", "container_id,ff_seal,lot_number,sgs_seal,shipping_container", $condition);
                                $rs_containerdata = $this->common->MySqlFetchRow($chk_containerdata_sql, "array");

//                                if(!empty($rs_containerdata[0]['container_id'])){  
//                                    $data_container = array(); 
//                                    //$data_container['ff_seal'] = !empty(str_replace("'", "&#39;", trim($data[7])))?str_replace("'", "&#39;", trim($data[7])):$rs_containerdata[0]['ff_seal'];
//                                    $data_container['lot_number'] = !empty(str_replace("'", "&#39;", trim($data[8])))?str_replace("'", "&#39;", trim($data[8])):$rs_containerdata[0]['lot_number'];
//                                    //$data_container['sgs_seal'] = !empty(str_replace("'", "&#39;", trim($data[9])))?str_replace("'", "&#39;", trim($data[9])):$rs_containerdata[0]['sgs_seal'];
//                                    //$data_container['shipping_container'] = !empty(str_replace("'", "&#39;", trim($data[10])))?str_replace("'", "&#39;", trim($data[10])):$rs_containerdata[0]['shipping_container'];
//                                    $condition = " container_id = ".$rs_containerdata[0]['container_id']." ";
//                                    $this->common->updateData("tbl_container", $data_container, $condition);  
//                                } 

                            }
                        } 
                         
                        
                        //code update date of document in container status log  
                        $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND dluf.uploaded_document_number = '".$uploaded_document_name."' AND cfds.current_session_id = '".$current_session_id."' ";
                        $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));  
                        $join_tables = array(
                            array("","tbl_custom_field_structure as cfs","cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()), 
                            array("","tbl_document_uploaded_files as dluf","dluf.uploaded_document_number = cfds.uploaded_document_number", array('dluf.container_id')),  
                        );  
                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,'','dluf.container_id');
                        $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
                        //echo $this->db->last_query();exit;
                        if(!empty($dateOfDocumentData)){
                            foreach ($dateOfDocumentData as $value) {
                                $data_doc_date = array();
                                $condition = " current_session_id = '".$current_session_id."' AND  container_id=".$value['container_id']." ";
                                $data_doc_date['document_date'] = date("Y-m-d", strtotime($value['custom_field_structure_value']));
                                $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);  
                            } 
                        }

                    }
                          
                    //start sku tab import code
                    foreach ($handle->rows(3) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $container_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_number = str_replace("'", "&#39;", trim($data[1]));
                        $quantity = 0;
                        if(!empty(str_replace("'", "&#39;", trim($data[2])))){
                            $quantity = str_replace("'", "&#39;", trim($data[2]));  
                        } 
                        
                        //get order id
                        $condition_container = "container_number = '" . $container_number . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,order_id", $condition_container);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                        //echo $this->db->last_query(); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                        //condition for order and sku id blank
                        if (empty($rs_container[0]['container_id']) || empty($rs_sku[0]['sku_id'])) {
                            continue;
                        }else{                            
                            $container_id = $rs_container[0]['container_id'];
                            $order_id = $rs_container[0]['order_id'];
                            $sku_id = $rs_sku[0]['sku_id'];
                            
                            $condition_contract = "order_id = ".$order_id." AND sku_id = ".$sku_id." "; 
                            $chk_contract_sql = $this->common->Fetch("tbl_order_sku", "order_id", $condition_contract);
                            $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array");  
                            
                            if (!empty($rs_contract[0]['order_id'])) {
                                $data = array();
                                $data['container_id'] = $container_id;
                                $data['sku_id'] = $sku_id;  
                                
                                $this->common->deleteRecord('tbl_container_sku', array("container_id" => $container_id,"sku_id" => $sku_id)); 
                                
                                $qty = $this->getContainerSKUQty($sku_id,$order_id,(int)$quantity,'Duplicate');                                
                                $data['quantity'] = (int)$qty;

                                $result = $this->common->insertData("tbl_container_sku", $data, "1"); 
                            }   
                        }  
                    }
                    
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit; 
                    
                // start INS import code
                }else if($docTypeID == 1){ 
                     
                    //sheet tab first import code start
                    foreach ($handle->rows() as $key => $data) {
                        //check for header column value
                        if ($key == 0) {  
                             
                        }else{
                            //check for blank value of all column value of row.
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 

                            if (trim($data[1]) == "") {
                                $error .= "Container Number cannot be empty in row : " . ($key + 1) . "<br>";
                                $valid++;
                            } 
                        }  
                    }
                    if ($valid != 0) {
                        $statusData['success'] = false;
                        $statusData['msg'] = $error;
                        print_r(json_encode($statusData));
                        exit;
                    } else {
                        foreach ($handle->rows() as $key => $data) {
                            //check for header row
                            if ($key == 0) {
                                continue;
                            }

                            //check for blank value of all column value of row. 
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 
                            
                            $LSNumber = str_replace("'", "&#39;", trim($data[1])); 
                            $containerNumber = str_replace("'", "&#39;", trim($data[2]));  
                            
                            $condition = "container_number = '" . $containerNumber . "' "; 
                            $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,last_container_status_id", $condition);
                            $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                            
                             if (!empty($rs_container[0]['container_id'])) {
                                $container_id = $rs_container[0]['container_id']; 
                                
                                $condition = " container_status_id = '". $rs_container[0]['last_container_status_id']."' ";
                                $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status','status_squence',$condition);
                                $lastContainerStatusSquence = 0;
                                if(!empty($lastContainerStatusSquenceData)){
                                   $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                }

                                $mapping_data = array();
                                $mapping_data['container_id'] = $container_id;
                                $mapping_data['document_type_id'] = $docTypeID; 
                                $mapping_data['ls_number'] = $LSNumber; 
                                $mapping_data['current_session_id'] =  $current_session_id;  
                                $mapping_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $mapping_data['updated_on'] = date("Y-m-d H:i:s");
                                
                                 
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND ls_number='".$LSNumber."' "; 
                                $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_container_mapping", "document_container_mapping_id", $condition);
                                $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                                if (!empty($rs_mapping[0]['document_container_mapping_id'])) {
                                     //update data 
                                    $result = $this->common->updateData("tbl_document_ls_container_mapping", $mapping_data, array("document_container_mapping_id" => $rs_mapping[0]['document_container_mapping_id']));                             
                                    
                                }else{  
                                    $mapping_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $mapping_data['created_on'] = date("Y-m-d H:i:s");  
                                    //insert data 
                                    $result = $this->common->insertData("tbl_document_ls_container_mapping", $mapping_data, "1");   
                                }
                                
                                //document status insert
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . "  "; 
                                $this->common->deleteRecord('tbl_container_uploaded_document_status',$condition);  

                                $doc_status_data = array();
                                $doc_status_data['container_id'] = $container_id;
                                $doc_status_data['document_type_id'] = $docTypeID;
                                $doc_status_data['sub_document_type_id'] = 0;
                                $doc_status_data['document_type_squence'] = 3;
                                $doc_status_data['status'] = 'Unchecked';
                                $doc_status_data['current_session_id'] =   $current_session_id;
                                $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                $result =  $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1");  
                                
                                //container status log insert 
                                $condition = "container_id = " . $container_id . " AND container_status_id='14'  ";
                                $this->common->deleteRecord('tbl_container_status_log',$condition);  
                                
                                $ContainerStatus =array();
                                $ContainerStatus['container_id'] = $container_id;
                                $ContainerStatus['container_status_id'] = 14;
                                $ContainerStatus['current_session_id'] =   $current_session_id;
                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                $ContainerStatus['document_date'] = date("Y-m-d");
                                $result =    $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1"); 
                                
                                if($lastContainerStatusSquence < 3){ 
                                    //last status in container table
                                    $containerStatusUpdate = array();
                                    $containerStatusUpdate['last_container_status_id'] = 14; 
                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $container_id));
                                }  
                                
                             } 
                        } 
                    }  
                    
                    //sheet tab second import code start
                    $imported = 0; $notimported = 0; $valid = 0; $error  = "";  
                    foreach ($handle->rows(1) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2]) || empty($data[3]) || empty($data[4])){
                            continue;
                        }  

                        $containerNumber = str_replace("'", "&#39;", trim($data[1]));                             
                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2]));                        
                        $subDocType = str_replace("'", "&#39;", trim($data[4]));
                        if($subDocType =='Internal Insurance'){
                            $subDocTypeId = 16;
                        }else{
                            $subDocTypeId = 15;
                        }
                        

                        $condition = "container_number = '" . $containerNumber . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                         if (!empty($rs_container[0]['container_id'])) {
                            $container_id = $rs_container[0]['container_id']; 

                            $doc_data = array(); 
                            $doc_data['uploaded_document_file'] = str_replace("'", "&#39;", trim($data[0]));
                            $doc_data['container_id'] = $container_id;
                            $doc_data['document_type_id'] = $docTypeID; 
                            $doc_data['sub_document_type_id'] = $subDocTypeId; 
                            $doc_data['current_session_id'] =  $current_session_id;  
                            $doc_data['uploaded_document_number'] =   $uploaded_document_name; 
                            $doc_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $doc_data['updated_on'] = date("Y-m-d H:i:s");

                            $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND sub_document_type_id=".$subDocTypeId."  AND uploaded_document_number ='".$uploaded_document_name."' "; 
                            $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "document_uploaded_files_id", $condition);
                            $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                            if (!empty($rs_mapping[0]['document_uploaded_files_id'])) {
                                 //update data 
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $doc_data, array("document_uploaded_files_id" => $rs_mapping[0]['document_uploaded_files_id']));                             
                                
                            }else{  
                                $doc_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_data['created_on'] = date("Y-m-d H:i:s");  
                                //insert data 
                                $result = $this->common->insertData("tbl_document_ls_uploaded_files", $doc_data, "1");   
                            } 
                            
                            if ($result) {
                                $imported++;
                            } else {
                                $notimported++;
                            }  

                         } 
                    }  
                     
                    foreach ($handle->rows(2) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        }  
                        
                        $subDocType = str_replace("'", "&#39;", trim($data[1]));
                        if($subDocType =='Internal Insurance'){
                            $subDocTypeId = 16;
                        }else{
                            $subDocTypeId = 15;
                        }

                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2])); 
                        
                        $condition = " document_type_id=" . $docTypeID . " AND sub_document_type_id=".$subDocTypeId." AND uploaded_document_number ='".$uploaded_document_name."' "; 
                        $this->common->deleteRecord('tbl_custom_field_data_submission',$condition);   

                        //get custom_field_structure_id according to document type
                        $custom_field = array();
                        $condition = " document_type_id=" . $docTypeID . " AND status='Active' "; 
                        $chk_fields_sql = $this->common->Fetch("tbl_custom_field_structure", "custom_field_structure_id,custom_field_title", $condition);
                        $rs_fields = $this->common->MySqlFetchRow($chk_fields_sql, "array");
                        if (!empty($rs_fields)) {
                            foreach ($rs_fields as $value) {
                                if(trim($value['custom_field_title'])=='Date of Document'){
                                    $DateofDocument = '';
                                    if(!empty(str_replace("'", "&#39;", trim($data[3])))){
                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[3]))); 
                                        $DateofDocument = date('Y-m-d',$DateVal); 
                                    }
                                    $custom_field[$value['custom_field_structure_id']] = $DateofDocument;
                                } 
                                
                                if(trim($value['custom_field_title'])=='Amount Insured'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[4])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='Policy Number'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[5])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='HBL Number'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[6])); 
                                }  
                                
                            }
                        }
                        //echo '<pre>'; print_r($custom_field); die;
                        //insert custom fields data
                        if (!empty($custom_field)) {
                            foreach ($custom_field as $key => $value) { 
                                $custom_data = array();
                                $custom_data['custom_field_structure_id'] = $key;
                                $custom_data['custom_field_structure_value'] =  $value; 
                                $custom_data['uploaded_document_number'] =  $uploaded_document_name;                            
                                $custom_data['document_type_id'] =   $docTypeID;
                                $custom_data['sub_document_type_id'] =  $subDocTypeId;
                                $custom_data['current_session_id'] = $current_session_id;
                                $custom_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['updated_on'] = date("Y-m-d H:i:s");
                                $custom_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['created_on'] = date("Y-m-d H:i:s"); 
                                //insert data 
                                $result = $this->common->insertData("tbl_custom_field_data_submission", $custom_data, "1");   
                            }
                        }  

                        //get containers according to document number
                        $condition = "uploaded_document_number = '".$uploaded_document_name."' And document_type_id = '".$docTypeID."' AND sub_document_type_id='".$subDocTypeId."' ";
                        $chk_container_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                        if (!empty($rs_container)) {
                            foreach ($rs_container as $value) {
                                $container_id = $value['container_id']; 
                                $hbl_number = str_replace("'", "&#39;", trim($data[6])); 
                                //insert update date of inspection and place of inspection
                                
                                if(!empty($hbl_number)){
                                    $data_supp = array(); 
                                    $data_supp['hbl_number'] = $hbl_number;  

                                    $condition = " container_id = ".$container_id." AND document_type_id = ".$docTypeID." ";
                                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data','custom_dynamic_data_id',$condition); 
                                    if(!empty($dynamic_data[0]['custom_dynamic_data_id'])){
                                        $condition = " custom_dynamic_data_id = ".$dynamic_data[0]['custom_dynamic_data_id']." "; 
                                        $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition); 
                                    }else{ 
                                        $data_supp['container_id'] = $container_id;
                                        $data_supp['document_type_id'] = $docTypeID; 
                                        $data_supp['sub_document_type_id'] = $subDocTypeId; 
                                        $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                                    }   
                                }

                            }
                        } 
                        
                        //code update date of document in container status log  
                        $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND dluf.uploaded_document_number = '".$uploaded_document_name."' AND cfds.current_session_id = '".$current_session_id."' ";
                        $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));  
                        $join_tables = array(
                            array("","tbl_custom_field_structure as cfs","cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()), 
                            array("","tbl_document_ls_uploaded_files as dluf","dluf.uploaded_document_number = cfds.uploaded_document_number", array('dluf.container_id')), 
                        );  
                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,'','dluf.container_id');
                        $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
                        //echo $this->db->last_query();exit;
                        if(!empty($dateOfDocumentData)){
                            foreach ($dateOfDocumentData as $value) {
                                $data_doc_date = array();
                                $condition = " current_session_id = '".$current_session_id."' AND  container_id=".$value['container_id']." ";
                                $data_doc_date['document_date'] = date("Y-m-d", strtotime($value['custom_field_structure_value']));
                                $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);  
                            } 
                        }


                    }
                       
                    //start sku tab import code
                    foreach ($handle->rows(3) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $container_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_number = str_replace("'", "&#39;", trim($data[1]));
                        $quantity = 0;
                        if(!empty(str_replace("'", "&#39;", trim($data[2])))){
                            $quantity = str_replace("'", "&#39;", trim($data[2]));  
                        } 
                        
                        //get order id
                        $condition_container = "container_number = '" . $container_number . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,order_id", $condition_container);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                        //echo $this->db->last_query(); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                        //condition for order and sku id blank
                        if (empty($rs_container[0]['container_id']) || empty($rs_sku[0]['sku_id'])) {
                            continue;
                        }else{                            
                            $container_id = $rs_container[0]['container_id'];
                            $order_id = $rs_container[0]['order_id'];
                            $sku_id = $rs_sku[0]['sku_id'];
                            
                            $condition_contract = "order_id = ".$order_id." AND sku_id = ".$sku_id." "; 
                            $chk_contract_sql = $this->common->Fetch("tbl_order_sku", "order_id", $condition_contract);
                            $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array");  
                            
                            if (!empty($rs_contract[0]['order_id'])) {
                                $data = array();
                                $data['container_id'] = $container_id;
                                $data['sku_id'] = $sku_id;  
                                
                                $this->common->deleteRecord('tbl_container_sku', array("container_id" => $container_id,"sku_id" => $sku_id)); 
                                
                                $qty = $this->getContainerSKUQty($sku_id,$order_id,(int)$quantity,'Duplicate');                                
                                $data['quantity'] = (int)$qty;

                                $result = $this->common->insertData("tbl_container_sku", $data, "1"); 
                            }   
                        }  
                    }
                    
                    
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit; 
                     
                //start FCR import code    
                }else if($docTypeID == 4){ 
                     
                    //sheet tab first import code start
                    foreach ($handle->rows() as $key => $data) {
                        //check for header column value
                        if ($key == 0) {  
                             
                        }else{
                            //check for blank value of all column value of row.
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 

                            if (trim($data[1]) == "") {
                                $error .= "LS Number cannot be empty in row : " . ($key + 1) . "<br>";
                                $valid++;
                            } 
                        }  
                    }
                    if ($valid != 0) {
                        $statusData['success'] = false;
                        $statusData['msg'] = $error;
                        print_r(json_encode($statusData));
                        exit;
                    } else {
                        foreach ($handle->rows() as $key => $data) {
                            //check for header row
                            if ($key == 0) {
                                continue;
                            }

                            //check for blank value of all column value of row. 
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 
                            
                            $LSNumber = str_replace("'", "&#39;", trim($data[1])); 
                            $containerNumber = str_replace("'", "&#39;", trim($data[2]));  
                            
                            $condition = "container_number = '" . $containerNumber . "' "; 
                            $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,last_container_status_id", $condition);
                            $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                            
                             if (!empty($rs_container[0]['container_id'])) {
                                $container_id = $rs_container[0]['container_id']; 
                                
                                $condition = " container_status_id = '". $rs_container[0]['last_container_status_id']."' ";
                                $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status','status_squence',$condition);
                                $lastContainerStatusSquence = 0;
                                if(!empty($lastContainerStatusSquenceData)){
                                   $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                }
                                
                                $mapping_data = array();
                                $mapping_data['container_id'] = $container_id;
                                $mapping_data['document_type_id'] = $docTypeID; 
                                $mapping_data['ls_number'] = $LSNumber; 
                                $mapping_data['current_session_id'] =  $current_session_id;  
                                $mapping_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $mapping_data['updated_on'] = date("Y-m-d H:i:s");
                                
                                 
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND ls_number='".$LSNumber."' "; 
                                $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_container_mapping", "document_container_mapping_id", $condition);
                                $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                                if (!empty($rs_mapping[0]['document_container_mapping_id'])) {
                                     //update data 
                                    $result = $this->common->updateData("tbl_document_ls_container_mapping", $mapping_data, array("document_container_mapping_id" => $rs_mapping[0]['document_container_mapping_id']));                             
                                    
                                }else{  
                                    $mapping_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $mapping_data['created_on'] = date("Y-m-d H:i:s");  
                                    //insert data 
                                    $result = $this->common->insertData("tbl_document_ls_container_mapping", $mapping_data, "1");   
                                }
                                
                                
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . "  "; 
                                $this->common->deleteRecord('tbl_container_uploaded_document_status',$condition); 

                                $doc_status_data = array();
                                $doc_status_data['container_id'] = $container_id;
                                $doc_status_data['document_type_id'] = $docTypeID;
                                $doc_status_data['sub_document_type_id'] = 0;
                                $doc_status_data['document_type_squence'] = 4; 
                                $doc_status_data['status'] = 'Unchecked';
                                $doc_status_data['current_session_id'] =   $current_session_id;
                                $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                $result =  $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1"); 
                                 
                                //container status log insert 
                                $condition = "container_id = " . $container_id . " AND container_status_id='5'  ";
                                $this->common->deleteRecord('tbl_container_status_log',$condition);  
                                
                                $ContainerStatus =array();
                                $ContainerStatus['container_id'] = $container_id;
                                $ContainerStatus['container_status_id'] = 5;
                                $ContainerStatus['current_session_id'] =   $current_session_id;
                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                $ContainerStatus['document_date'] = date("Y-m-d");
                                $result = $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1"); 
                                
                                
                                if($lastContainerStatusSquence < 4){ 
                                    //last status in container table
                                    $containerStatusUpdate = array();
                                    $containerStatusUpdate['last_container_status_id'] = 5; 
                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $container_id));
                                }   
                                
                             } 
                        } 
                    }  
                    
                    //sheet tab second import code start
                    $imported = 0; $notimported = 0; $valid = 0; $error  = "";  
                    foreach ($handle->rows(1) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2]) || empty($data[3]) || empty($data[4])){
                            continue;
                        }  

                        $containerNumber = str_replace("'", "&#39;", trim($data[1]));                             
                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2]));                        
                        $subDocType = str_replace("'", "&#39;", trim($data[4]));
                        if($subDocType =='Internal FCR'){
                            $subDocTypeId = 18;
                        }else{
                            $subDocTypeId = 17;
                        }
                        

                        $condition = "container_number = '" . $containerNumber . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                         if (!empty($rs_container[0]['container_id'])) {
                            $container_id = $rs_container[0]['container_id']; 

                            $doc_data = array(); 
                            $doc_data['uploaded_document_file'] = str_replace("'", "&#39;", trim($data[0]));
                            $doc_data['container_id'] = $container_id;
                            $doc_data['document_type_id'] = $docTypeID; 
                            $doc_data['sub_document_type_id'] = $subDocTypeId; 
                            $doc_data['current_session_id'] =  $current_session_id;  
                            $doc_data['uploaded_document_number'] =   $uploaded_document_name; 
                            $doc_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $doc_data['updated_on'] = date("Y-m-d H:i:s");

                            $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND sub_document_type_id=".$subDocTypeId."  AND uploaded_document_number ='".$uploaded_document_name."' "; 
                            $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "document_uploaded_files_id", $condition);
                            $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                            if (!empty($rs_mapping[0]['document_uploaded_files_id'])) {
                                 //update data 
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $doc_data, array("document_uploaded_files_id" => $rs_mapping[0]['document_uploaded_files_id']));                             
                                
                            }else{  
                                $doc_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_data['created_on'] = date("Y-m-d H:i:s");  
                                //insert data 
                                $result = $this->common->insertData("tbl_document_ls_uploaded_files", $doc_data, "1");   
                            } 
                            
                            if ($result) {
                               $imported++;
                            } else {
                                $notimported++;
                            }  

                         } 
                    } 
                                 
                    foreach ($handle->rows(2) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2]) || empty($data[3])){
                            continue;
                        }  
                        
                        $subDocType = str_replace("'", "&#39;", trim($data[1]));
                        if($subDocType =='Internal FCR'){
                            $subDocTypeId = 18;
                        }else{
                            $subDocTypeId = 17;
                        }

                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2])); 
                        
                        $condition = " document_type_id=" . $docTypeID . " AND sub_document_type_id=".$subDocTypeId." AND uploaded_document_number ='".$uploaded_document_name."' "; 
                        $this->common->deleteRecord('tbl_custom_field_data_submission',$condition);   

                        //get custom_field_structure_id according to document type
                        $custom_field = array();
                        $condition = " document_type_id=" . $docTypeID . " AND status='Active' "; 
                        $chk_fields_sql = $this->common->Fetch("tbl_custom_field_structure", "custom_field_structure_id,custom_field_title", $condition);
                        $rs_fields = $this->common->MySqlFetchRow($chk_fields_sql, "array");
                        if (!empty($rs_fields)) {
                            foreach ($rs_fields as $value) {
                                if(trim($value['custom_field_title'])=='Date of Document'){
                                    $DateofDocument = '';
                                    if(!empty(str_replace("'", "&#39;", trim($data[3])))){
                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[3]))); 
                                        $DateofDocument = date('Y-m-d',$DateVal); 
                                    }
                                    $custom_field[$value['custom_field_structure_id']] = $DateofDocument;
                                } 
                                
                                if(trim($value['custom_field_title'])=='Shipper'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[4])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='Consignee'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[5])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='Notify Party'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[6])); 
                                } 
                                
                                if(trim($value['custom_field_title'])=='HBL Number'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[7])); 
                                }  
                                
                            }
                        }
                        //echo '<pre>'; print_r($custom_field); die;
                        //insert custom fields data
                        if (!empty($custom_field)) {
                            foreach ($custom_field as $key => $value) { 
                                $custom_data = array();
                                $custom_data['custom_field_structure_id'] = $key;
                                $custom_data['custom_field_structure_value'] =  $value; 
                                $custom_data['uploaded_document_number'] =  $uploaded_document_name;                            
                                $custom_data['document_type_id'] =   $docTypeID;
                                $custom_data['sub_document_type_id'] =  $subDocTypeId;
                                $custom_data['current_session_id'] = $current_session_id;
                                $custom_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['updated_on'] = date("Y-m-d H:i:s");
                                $custom_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['created_on'] = date("Y-m-d H:i:s"); 
                                //insert data 
                                $result = $this->common->insertData("tbl_custom_field_data_submission", $custom_data, "1");   
                            }
                        }  

                        //get containers according to document number
                        $condition = "uploaded_document_number = '".$uploaded_document_name."' And document_type_id = '".$docTypeID."' AND sub_document_type_id='".$subDocTypeId."' ";
                        $chk_container_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                        if (!empty($rs_container)) {
                            foreach ($rs_container as $value) {
                                $container_id = $value['container_id']; 
                                $hbl_number = str_replace("'", "&#39;", trim($data[7])); 
                                //insert update date of inspection and place of inspection 
                                if(!empty($hbl_number)){
                                    $data_supp = array(); 
                                    $data_supp['hbl_number'] = $hbl_number;  

                                    $condition = " container_id = ".$container_id." AND document_type_id = ".$docTypeID." ";
                                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data','custom_dynamic_data_id',$condition); 
                                    if(!empty($dynamic_data[0]['custom_dynamic_data_id'])){
                                        $condition = " custom_dynamic_data_id = ".$dynamic_data[0]['custom_dynamic_data_id']." "; 
                                        $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition); 
                                    }else{ 
                                        $data_supp['container_id'] = $container_id;
                                        $data_supp['document_type_id'] = $docTypeID; 
                                        $data_supp['sub_document_type_id'] = $subDocTypeId; 
                                        $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                                    }   
                                }
                                
                                //update container information
                                $condition = " container_id = ".$container_id." ";
                                $chk_containerdata_sql = $this->common->Fetch("tbl_container", "container_id,order_id,ff_seal,lot_number,sgs_seal,shipping_container,etd,eta", $condition);
                                $rs_containerdata = $this->common->MySqlFetchRow($chk_containerdata_sql, "array");

                                if(!empty($rs_containerdata[0]['container_id'])){  
                                    $data_container = array(); 
//                                    $data_container['sgs_seal'] = !empty(str_replace("'", "&#39;", trim($data[8])))?str_replace("'", "&#39;", trim($data[8])):$rs_containerdata[0]['sgs_seal'];
//                                    $data_container['ff_seal'] = !empty(str_replace("'", "&#39;", trim($data[9])))?str_replace("'", "&#39;", trim($data[9])):$rs_containerdata[0]['ff_seal'];  
//                                    $data_container['shipping_container'] = !empty(str_replace("'", "&#39;", trim($data[10])))?str_replace("'", "&#39;", trim($data[10])):$rs_containerdata[0]['shipping_container'];
//                                    //get ff name
//                                    if(!empty(str_replace("'", "&#39;", trim($data[11])))){
//                                        $ffcondition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Freight Forwarder') AND alias ='".str_replace("'", "&#39;", trim($data[11]))."'  ";
//                                        $chk_ff_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $ffcondition); 
//                                        $rs_ff = $this->common->MySqlFetchRow($chk_ff_sql, "array");
//                                        if(!empty($rs_ff[0]['business_partner_id'])){ 
//                                            //ff from master contract
//                                            $condition = "status = 'Active' AND order_id = ".$rs_containerdata[0]['order_id']." ";  
//                                            $orderData = $this->common->getData("tbl_order", "freight_forwarder_id",$condition);
//                                            if (!empty($orderData)) {  
//                                                $ffIdsArr = explode(",", $orderData[0]['freight_forwarder_id']); 
//                                                if (!empty($ffIdsArr)) {
//                                                    if(in_array($rs_ff[0]['business_partner_id'], $ffIdsArr)){ 
//                                                        $data_container['freight_forwarder_id'] = $rs_ff[0]['business_partner_id'];
//                                                    } 
//                                                }  
//                                            } 
//                                        }
//                                    }
//                                    // get liner name
//                                    if(!empty(str_replace("'", "&#39;", trim($data[12])))){
//                                        $lcondition = "status = 'Active' And liner_name ='".str_replace("'", "&#39;", trim($data[12]))."'  ";
//                                        $chk_liner_sql = $this->common->Fetch("tbl_liner", "liner_id", $lcondition); 
//                                        $rs_liner = $this->common->MySqlFetchRow($chk_liner_sql, "array");
//                                        if(!empty($rs_liner[0]['liner_id'])){
//                                            $data_container['liner_name'] = $rs_liner[0]['liner_id'];
//                                        }
//                                    }
//                                    
//                                    // get pol
//                                    if(!empty(str_replace("'", "&#39;", trim($data[13])))){
//                                        $polcondition = "status = 'Active' And pol_name ='".str_replace("'", "&#39;", trim($data[13]))."'  ";
//                                        $chk_pol_sql = $this->common->Fetch("tbl_pol", "pol_id", $polcondition); 
//                                        $rs_pol = $this->common->MySqlFetchRow($chk_pol_sql, "array");
//                                        if(!empty($rs_pol[0]['pol_id'])){
//                                            $data_container['pol'] = $rs_pol[0]['pol_id'];
//                                        }
//                                    }
//                                    
//                                    // get pod
//                                    if(!empty(str_replace("'", "&#39;", trim($data[14])))){
//                                        $podcondition = "status = 'Active' And pod_name ='".str_replace("'", "&#39;", trim($data[14]))."'  ";
//                                        $chk_pod_sql = $this->common->Fetch("tbl_pod", "pod_id", $podcondition); 
//                                        $rs_pod = $this->common->MySqlFetchRow($chk_pod_sql, "array");
//                                        if(!empty($rs_pod[0]['pod_id'])){
//                                            $data_container['pod'] = $rs_pod[0]['pod_id'];
//                                        }
//                                    } 
//                                     
//                                    if(!empty(str_replace("'", "&#39;", trim($data[15])))){
//                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[15]))); 
//                                        $data_container['etd'] = date('Y-m-d',$DateVal); 
//                                    } 
//                                     
//                                    if(!empty(str_replace("'", "&#39;", trim($data[16])))){
//                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[16]))); 
//                                        $data_container['revised_etd'] = date('Y-m-d',$DateVal); 
//                                    }
//                                    
//                                    if(!empty(str_replace("'", "&#39;", trim($data[17])))){ 
//                                        $data_container['ett'] = str_replace("'", "&#39;", trim($data[17])); 
//                                    }
//                                    
                                    if(!empty($rs_containerdata[0]['etd']) && !empty($rs_containerdata[0]['eta'])){
                                        $etd = date('d-m-Y', strtotime($rs_containerdata[0]['etd'])); 
                                        $eta = date('d-m-Y', strtotime($rs_containerdata[0]['eta']));  
                                        $days = DaysDiffBetweenTwoDays($eta,$etd);
                                        $data_container['ett'] = abs($days);  
                                        $data_container['revised_ett'] = abs($days); 
                                        
                                        $condition = " container_id = ".$rs_containerdata[0]['container_id']." ";
                                        $this->common->updateData("tbl_container", $data_container, $condition); 
                                    } 
                                }  
                                
                            }
                        } 
                         
                        
                        //code of update document date in container log table
                        $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND dluf.uploaded_document_number = '".$uploaded_document_name."' AND cfds.current_session_id = '".$current_session_id."' ";
                        $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));  
                        $join_tables = array(
                            array("","tbl_custom_field_structure as cfs","cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()), 
                            array("","tbl_document_ls_uploaded_files as dluf","dluf.uploaded_document_number = cfds.uploaded_document_number", array('dluf.container_id')), 
                        );  
                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,'','dluf.container_id');
                        $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
                        //echo $this->db->last_query();exit;
                        if(!empty($dateOfDocumentData)){
                            foreach ($dateOfDocumentData as $value) {
                                $data_doc_date = array();
                                $condition = " current_session_id = '".$current_session_id."' AND  container_id=".$value['container_id']." ";
                                $data_doc_date['document_date'] = date("Y-m-d", strtotime($value['custom_field_structure_value']));
                                $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);  
                            } 
                        }


                    }
                        
                    //start sku tab import code
                    foreach ($handle->rows(3) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $container_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_number = str_replace("'", "&#39;", trim($data[1]));
                        $quantity = 0;
                        if(!empty(str_replace("'", "&#39;", trim($data[2])))){
                            $quantity = str_replace("'", "&#39;", trim($data[2]));  
                        } 
                        
                        //get order id
                        $condition_container = "container_number = '" . $container_number . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,order_id", $condition_container);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                        //echo $this->db->last_query(); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                        //condition for order and sku id blank
                        if (empty($rs_container[0]['container_id']) || empty($rs_sku[0]['sku_id'])) {
                            continue;
                        }else{                            
                            $container_id = $rs_container[0]['container_id'];
                            $order_id = $rs_container[0]['order_id'];
                            $sku_id = $rs_sku[0]['sku_id'];
                            
                            $condition_contract = "order_id = ".$order_id." AND sku_id = ".$sku_id." "; 
                            $chk_contract_sql = $this->common->Fetch("tbl_order_sku", "order_id", $condition_contract);
                            $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array");  
                            
                            if (!empty($rs_contract[0]['order_id'])) {
                                $data = array();
                                $data['container_id'] = $container_id;
                                $data['sku_id'] = $sku_id;  
                                
                                $this->common->deleteRecord('tbl_container_sku', array("container_id" => $container_id,"sku_id" => $sku_id)); 
                                
                                $qty = $this->getContainerSKUQty($sku_id,$order_id,(int)$quantity,'Duplicate');                                
                                $data['quantity'] = (int)$qty;

                                $result = $this->common->insertData("tbl_container_sku", $data, "1"); 
                            }   
                        }  
                    }
                    
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit; 
                     
                // Start HBL import code    
                }else if($docTypeID == 5){ 
                     
                    //sheet tab first import code start
                    foreach ($handle->rows() as $key => $data) {
                        //check for header column value
                        if ($key == 0) {  
                             
                        }else{
                            //check for blank value of all column value of row.
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 

                            if (trim($data[1]) == "") {
                                $error .= "LS Number cannot be empty in row : " . ($key + 1) . "<br>";
                                $valid++;
                            } 
                        }  
                    }
                    if ($valid != 0) {
                        $statusData['success'] = false;
                        $statusData['msg'] = $error;
                        print_r(json_encode($statusData));
                        exit;
                    } else {
                        foreach ($handle->rows() as $key => $data) {
                            //check for header row
                            if ($key == 0) {
                                continue;
                            }

                            //check for blank value of all column value of row. 
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 
                            
                            $LSNumber = str_replace("'", "&#39;", trim($data[1])); 
                            $containerNumber = str_replace("'", "&#39;", trim($data[2]));  
                            
                            $condition = "container_number = '" . $containerNumber . "' "; 
                            $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,revised_etd,last_container_status_id", $condition);
                            $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                            
                             if (!empty($rs_container[0]['container_id'])) {
                                $container_id = $rs_container[0]['container_id']; 
                                
                                $condition = " container_status_id = '". $rs_container[0]['last_container_status_id']."' ";
                                $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status','status_squence',$condition);
                                $lastContainerStatusSquence = 0;
                                if(!empty($lastContainerStatusSquenceData)){
                                   $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                }

                                $mapping_data = array();
                                $mapping_data['container_id'] = $container_id;
                                $mapping_data['document_type_id'] = $docTypeID; 
                                $mapping_data['ls_number'] = $LSNumber; 
                                $mapping_data['current_session_id'] =  $current_session_id;  
                                $mapping_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $mapping_data['updated_on'] = date("Y-m-d H:i:s");
                                
                                 
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND ls_number='".$LSNumber."' "; 
                                $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_container_mapping", "document_container_mapping_id", $condition);
                                $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                                if (!empty($rs_mapping[0]['document_container_mapping_id'])) {
                                     //update data 
                                    $result = $this->common->updateData("tbl_document_ls_container_mapping", $mapping_data, array("document_container_mapping_id" => $rs_mapping[0]['document_container_mapping_id']));                             
                                    
                                }else{  
                                    $mapping_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $mapping_data['created_on'] = date("Y-m-d H:i:s");  
                                    //insert data 
                                    $result = $this->common->insertData("tbl_document_ls_container_mapping", $mapping_data, "1");  
                                }   
                                
                                // container status insert
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . "  "; 
                                $this->common->deleteRecord('tbl_container_uploaded_document_status',$condition); 

                                $doc_status_data = array();
                                $doc_status_data['container_id'] = $container_id;
                                $doc_status_data['document_type_id'] = $docTypeID;
                                $doc_status_data['sub_document_type_id'] = 0;
                                $doc_status_data['document_type_squence'] = 5; 
                                $doc_status_data['status'] = 'Unchecked';
                                $doc_status_data['current_session_id'] =   $current_session_id;
                                $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                $result =  $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1"); 
                                 
                                //container status log insert  
                                if(!empty($rs_container[0]['revised_etd'])){
                                    $condition = "container_id = " . $container_id . " AND container_status_id='6'  ";
                                    $this->common->deleteRecord('tbl_container_status_log',$condition);  

                                    $ContainerStatus =array();
                                    $ContainerStatus['container_id'] = $container_id;
                                    $ContainerStatus['container_status_id'] = 6;
                                    $ContainerStatus['current_session_id'] =   $current_session_id;
                                    $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                    $ContainerStatus['document_date'] = date("Y-m-d");
                                    $result = $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1"); 
                                    
                                    if($lastContainerStatusSquence < 5){ 
                                        //last status in container table
                                        $containerStatusUpdate = array();
                                        $containerStatusUpdate['last_container_status_id'] = 6; 
                                        $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                        $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                        $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $container_id));
                                    }  
                                    
                                }
                                
                             } 
                        } 
                    }  
                    
                    //sheet tab second import code start
                    $imported = 0; $notimported = 0; $valid = 0; $error  = "";  
                    foreach ($handle->rows(1) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2]) || empty($data[3]) || empty($data[4])){
                            continue;
                        }  

                        $containerNumber = str_replace("'", "&#39;", trim($data[1]));                             
                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2]));                        
                        $subDocType = str_replace("'", "&#39;", trim($data[4]));
                        if($subDocType =='Internal HBL'){
                            $subDocTypeId = 19;
                        }else{
                            $subDocTypeId = 20;
                        }

                        $condition = "container_number = '" . $containerNumber . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                         if (!empty($rs_container[0]['container_id'])) {
                            $container_id = $rs_container[0]['container_id']; 

                            $doc_data = array(); 
                            $doc_data['uploaded_document_file'] = str_replace("'", "&#39;", trim($data[0]));
                            $doc_data['container_id'] = $container_id;
                            $doc_data['document_type_id'] = $docTypeID; 
                            $doc_data['sub_document_type_id'] = $subDocTypeId; 
                            $doc_data['current_session_id'] = $current_session_id;  
                            $doc_data['uploaded_document_number'] = $uploaded_document_name; 
                            $doc_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $doc_data['updated_on'] = date("Y-m-d H:i:s");

                            $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND sub_document_type_id=".$subDocTypeId."  AND uploaded_document_number ='".$uploaded_document_name."' "; 
                            $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "document_uploaded_files_id", $condition);
                            $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                            if (!empty($rs_mapping[0]['document_uploaded_files_id'])) {
                                 //update data 
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $doc_data, array("document_uploaded_files_id" => $rs_mapping[0]['document_uploaded_files_id']));                             
                                
                            }else{  
                                $doc_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_data['created_on'] = date("Y-m-d H:i:s");  
                                //insert data 
                                $result = $this->common->insertData("tbl_document_ls_uploaded_files", $doc_data, "1");   
                            } 
                            
                            if ($result) {
                               $imported++;
                            } else {
                                $notimported++;
                            }  

                         } 
                    } 
                     
                    foreach ($handle->rows(2) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2]) || empty($data[3])){
                            continue;
                        }  
                        
                        $subDocType = str_replace("'", "&#39;", trim($data[1]));
                        if($subDocType =='Internal HBL'){
                            $subDocTypeId = 19;
                        }else{
                            $subDocTypeId = 20;
                        }

                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2])); 
                        
                        $condition = " document_type_id=" . $docTypeID . " AND sub_document_type_id=".$subDocTypeId." AND uploaded_document_number ='".$uploaded_document_name."' "; 
                        $this->common->deleteRecord('tbl_custom_field_data_submission',$condition);   

                        //get custom_field_structure_id according to document type
                        $custom_field = array();
                        $condition = " document_type_id=" . $docTypeID . " AND status='Active' "; 
                        $chk_fields_sql = $this->common->Fetch("tbl_custom_field_structure", "custom_field_structure_id,custom_field_title", $condition);
                        $rs_fields = $this->common->MySqlFetchRow($chk_fields_sql, "array");
                        if (!empty($rs_fields)) {
                            foreach ($rs_fields as $value) {
                                if(trim($value['custom_field_title'])=='Date of Document'){
                                    $DateofDocument = '';
                                    if(!empty(str_replace("'", "&#39;", trim($data[3])))){
                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[3]))); 
                                        $DateofDocument = date('Y-m-d',$DateVal); 
                                    }
                                    $custom_field[$value['custom_field_structure_id']] = $DateofDocument;
                                }  
                                
                                if(trim($value['custom_field_title'])=='HBL Number'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[4])); 
                                }  
                                
                            }
                        }
                        //echo '<pre>'; print_r($custom_field); die;
                        //insert custom fields data
                        if (!empty($custom_field)) {
                            foreach ($custom_field as $key => $value) { 
                                $custom_data = array();
                                $custom_data['custom_field_structure_id'] = $key;
                                $custom_data['custom_field_structure_value'] =  $value; 
                                $custom_data['uploaded_document_number'] =  $uploaded_document_name;                            
                                $custom_data['document_type_id'] =   $docTypeID;
                                $custom_data['sub_document_type_id'] =  $subDocTypeId;
                                $custom_data['current_session_id'] = $current_session_id;
                                $custom_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['updated_on'] = date("Y-m-d H:i:s");
                                $custom_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['created_on'] = date("Y-m-d H:i:s"); 
                                //insert data 
                                $result = $this->common->insertData("tbl_custom_field_data_submission", $custom_data, "1");   
                            }
                        }  

                        //get containers according to document number
                        $condition = "uploaded_document_number = '".$uploaded_document_name."' And document_type_id = '".$docTypeID."' AND sub_document_type_id='".$subDocTypeId."' ";
                        $chk_container_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                        if (!empty($rs_container)) {
                            foreach ($rs_container as $value) {
                                $container_id = $value['container_id']; 
                                $hbl_number = str_replace("'", "&#39;", trim($data[4])); 
                                //insert update date of inspection and place of inspection 
                                if(!empty($hbl_number)){
                                    $data_supp = array(); 
                                    $data_supp['hbl_number'] = $hbl_number;  

                                    $condition = " container_id = ".$container_id." AND document_type_id = ".$docTypeID." ";
                                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data','custom_dynamic_data_id',$condition); 
                                    if(!empty($dynamic_data[0]['custom_dynamic_data_id'])){
                                        $condition = " custom_dynamic_data_id = ".$dynamic_data[0]['custom_dynamic_data_id']." "; 
                                        $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition); 
                                    }else{ 
                                        $data_supp['container_id'] = $container_id;
                                        $data_supp['document_type_id'] = $docTypeID; 
                                        $data_supp['sub_document_type_id'] = $subDocTypeId; 
                                        $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                                    }   
                                }
                                
                                //update container information
                                $condition = " container_id = ".$container_id." ";
                                $chk_containerdata_sql = $this->common->Fetch("tbl_container", "container_id,order_id,ff_seal,lot_number,sgs_seal,shipping_container,eta,etd", $condition);
                                $rs_containerdata = $this->common->MySqlFetchRow($chk_containerdata_sql, "array");

                                if(!empty($rs_containerdata[0]['container_id'])){  
                                    $data_container = array();  
//                                    $data_container['ff_seal'] = !empty(str_replace("'", "&#39;", trim($data[5])))?str_replace("'", "&#39;", trim($data[5])):$rs_containerdata[0]['ff_seal'];  
//                                    $data_container['shipping_container'] = !empty(str_replace("'", "&#39;", trim($data[6])))?str_replace("'", "&#39;", trim($data[6])):$rs_containerdata[0]['shipping_container'];
//                                    
//                                    //get ff name
//                                    if(!empty(str_replace("'", "&#39;", trim($data[7])))){
//                                        $ffcondition = "status = 'Active' And business_type = 'Vendor' AND business_category IN ('Freight Forwarder') AND alias ='".str_replace("'", "&#39;", trim($data[7]))."'  ";
//                                        $chk_ff_sql = $this->common->Fetch("tbl_business_partner", "business_partner_id", $ffcondition); 
//                                        $rs_ff = $this->common->MySqlFetchRow($chk_ff_sql, "array");
//                                        if(!empty($rs_ff[0]['business_partner_id'])){ 
//                                            //ff from master contract
//                                            $condition = "status = 'Active' AND order_id = ".$rs_containerdata[0]['order_id']." ";  
//                                            $orderData = $this->common->getData("tbl_order", "freight_forwarder_id",$condition);
//                                            if (!empty($orderData)) {  
//                                                $ffIdsArr = explode(",", $orderData[0]['freight_forwarder_id']); 
//                                                if (!empty($ffIdsArr)) {
//                                                    if(in_array($rs_ff[0]['business_partner_id'], $ffIdsArr)){ 
//                                                        $data_container['freight_forwarder_id'] = $rs_ff[0]['business_partner_id'];
//                                                    } 
//                                                }  
//                                            }
//                                        }
//                                    }
//                                    // get liner name
//                                    if(!empty(str_replace("'", "&#39;", trim($data[8])))){
//                                        $lcondition = "status = 'Active' And liner_name ='".str_replace("'", "&#39;", trim($data[8]))."'  ";
//                                        $chk_liner_sql = $this->common->Fetch("tbl_liner", "liner_id", $lcondition); 
//                                        $rs_liner = $this->common->MySqlFetchRow($chk_liner_sql, "array");
//                                        if(!empty($rs_liner[0]['liner_id'])){
//                                            $data_container['liner_name'] = $rs_liner[0]['liner_id'];
//                                        }
//                                    }
//                                    
//                                    // get pol
//                                    if(!empty(str_replace("'", "&#39;", trim($data[9])))){
//                                        $polcondition = "status = 'Active' And pol_name ='".str_replace("'", "&#39;", trim($data[9]))."'  ";
//                                        $chk_pol_sql = $this->common->Fetch("tbl_pol", "pol_id", $polcondition); 
//                                        $rs_pol = $this->common->MySqlFetchRow($chk_pol_sql, "array");
//                                        if(!empty($rs_pol[0]['pol_id'])){
//                                            $data_container['pol'] = $rs_pol[0]['pol_id'];
//                                        }
//                                    }
//                                    
//                                    // get pod
//                                    if(!empty(str_replace("'", "&#39;", trim($data[10])))){
//                                        $podcondition = "status = 'Active' And pod_name ='".str_replace("'", "&#39;", trim($data[10]))."'  ";
//                                        $chk_pod_sql = $this->common->Fetch("tbl_pod", "pod_id", $podcondition); 
//                                        $rs_pod = $this->common->MySqlFetchRow($chk_pod_sql, "array");
//                                        if(!empty($rs_pod[0]['pod_id'])){
//                                            $data_container['pod'] = $rs_pod[0]['pod_id'];
//                                        }
//                                    } 
//                                     
//                                    if(!empty(str_replace("'", "&#39;", trim($data[11])))){
//                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[11]))); 
//                                        $data_container['etd'] = date('Y-m-d',$DateVal); 
//                                    } 
//                                     
//                                    if(!empty(str_replace("'", "&#39;", trim($data[12])))){
//                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[12]))); 
//                                        $data_container['revised_etd'] = date('Y-m-d',$DateVal); 
//                                    }
                                    
//                                    if(!empty(str_replace("'", "&#39;", trim($data[14])))){ 
//                                        $data_container['vessel_name'] = str_replace("'", "&#39;", trim($data[14])); 
//                                    }
                                    
                                    if(!empty($rs_containerdata[0]['etd']) && !empty($rs_containerdata[0]['eta'])){
                                        $etd = date('d-m-Y', strtotime($rs_containerdata[0]['etd'])); 
                                        $eta = date('d-m-Y', strtotime($rs_containerdata[0]['eta']));  
                                        $days = DaysDiffBetweenTwoDays($eta,$etd);
                                        $data_container['ett'] = abs($days);  
                                        $data_container['revised_ett'] = abs($days);
                                        
                                        $condition = " container_id = ".$rs_containerdata[0]['container_id']." ";
                                        $this->common->updateData("tbl_container", $data_container, $condition);  
                                    } 
                                   
                                }  
                                
                            }
                        } 

                        //code update date of document in container status log  
                        $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND dluf.uploaded_document_number = '".$uploaded_document_name."' AND cfds.current_session_id = '".$current_session_id."' ";
                        $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));  
                        $join_tables = array(
                            array("","tbl_custom_field_structure as cfs","cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()), 
                            array("","tbl_document_ls_uploaded_files as dluf","dluf.uploaded_document_number = cfds.uploaded_document_number", array('dluf.container_id')), 
                        );  
                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,'','dluf.container_id');
                        $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
                        //echo $this->db->last_query();exit;
                        if(!empty($dateOfDocumentData)){
                            foreach ($dateOfDocumentData as $value) {
                                $data_doc_date = array();
                                $condition = " current_session_id = '".$current_session_id."' AND  container_id=".$value['container_id']." ";
                                $data_doc_date['document_date'] = date("Y-m-d", strtotime($value['custom_field_structure_value']));
                                $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);  
                            } 
                        }
                        
                    }
                          
                    //start sku tab import code
                    foreach ($handle->rows(3) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $container_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_number = str_replace("'", "&#39;", trim($data[1]));
                        $quantity = 0;
                        if(!empty(str_replace("'", "&#39;", trim($data[2])))){
                            $quantity = str_replace("'", "&#39;", trim($data[2]));  
                        } 
                        
                        //get order id
                        $condition_container = "container_number = '" . $container_number . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,order_id", $condition_container);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                        //echo $this->db->last_query(); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                        //condition for order and sku id blank
                        if (empty($rs_container[0]['container_id']) || empty($rs_sku[0]['sku_id'])) {
                            continue;
                        }else{                            
                            $container_id = $rs_container[0]['container_id'];
                            $order_id = $rs_container[0]['order_id'];
                            $sku_id = $rs_sku[0]['sku_id'];
                            
                            $condition_contract = "order_id = ".$order_id." AND sku_id = ".$sku_id." "; 
                            $chk_contract_sql = $this->common->Fetch("tbl_order_sku", "order_id", $condition_contract);
                            $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array");  
                            
                            if (!empty($rs_contract[0]['order_id'])) {
                                $data = array();
                                $data['container_id'] = $container_id;
                                $data['sku_id'] = $sku_id;  
                                
                                $this->common->deleteRecord('tbl_container_sku', array("container_id" => $container_id,"sku_id" => $sku_id)); 
                                
                                $qty = $this->getContainerSKUQty($sku_id,$order_id,(int)$quantity,'Duplicate');                                
                                $data['quantity'] = (int)$qty;

                                $result = $this->common->insertData("tbl_container_sku", $data, "1"); 
                            }   
                        }  
                    }
                     
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit; 
                     
                //start Arrival Notice import code     
                }else if($docTypeID == 10){ 
                     
                    //sheet tab first import code start
                    foreach ($handle->rows() as $key => $data) {
                        //check for header column value
                        if ($key == 0) {  
                             
                        }else{
                            //check for blank value of all column value of row.
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 

                            if (trim($data[1]) == "") {
                                $error .= "LS Number cannot be empty in row : " . ($key + 1) . "<br>";
                                $valid++;
                            } 
                        }  
                    }
                    if ($valid != 0) {
                        $statusData['success'] = false;
                        $statusData['msg'] = $error;
                        print_r(json_encode($statusData));
                        exit;
                    } else {
                        foreach ($handle->rows() as $key => $data) {
                            //check for header row
                            if ($key == 0) {
                                continue;
                            }

                            //check for blank value of all column value of row. 
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 
                            
                            $LSNumber = str_replace("'", "&#39;", trim($data[1])); 
                            $containerNumber = str_replace("'", "&#39;", trim($data[2]));  
                            
                            $condition = "container_number = '" . $containerNumber . "' "; 
                            $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,last_container_status_id", $condition);
                            $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                            
                             if (!empty($rs_container[0]['container_id'])) {
                                $container_id = $rs_container[0]['container_id']; 
                                
                                $condition = " container_status_id = '". $rs_container[0]['last_container_status_id']."' ";
                                $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status','status_squence',$condition);
                                $lastContainerStatusSquence = 0;
                                if(!empty($lastContainerStatusSquenceData)){
                                   $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                }

                                $mapping_data = array();
                                $mapping_data['container_id'] = $container_id;
                                $mapping_data['document_type_id'] = $docTypeID; 
                                $mapping_data['ls_number'] = $LSNumber; 
                                $mapping_data['current_session_id'] =  $current_session_id;  
                                $mapping_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $mapping_data['updated_on'] = date("Y-m-d H:i:s");
                                
                                 
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND ls_number='".$LSNumber."' "; 
                                $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_container_mapping", "document_container_mapping_id", $condition);
                                $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                                if (!empty($rs_mapping[0]['document_container_mapping_id'])) {
                                     //update data 
                                    $result = $this->common->updateData("tbl_document_ls_container_mapping", $mapping_data, array("document_container_mapping_id" => $rs_mapping[0]['document_container_mapping_id']));                             
                                    
                                }else{  
                                    $mapping_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $mapping_data['created_on'] = date("Y-m-d H:i:s");  
                                    //insert data 
                                    $result = $this->common->insertData("tbl_document_ls_container_mapping", $mapping_data, "1");  
                                } 
                                
                                // container status insert
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . "  "; 
                                $this->common->deleteRecord('tbl_container_uploaded_document_status',$condition); 

                                $doc_status_data = array();
                                $doc_status_data['container_id'] = $container_id;
                                $doc_status_data['document_type_id'] = $docTypeID;
                                $doc_status_data['sub_document_type_id'] = 0;
                                $doc_status_data['document_type_squence'] = 7; 
                                $doc_status_data['status'] = 'Unchecked';
                                $doc_status_data['current_session_id'] =   $current_session_id;
                                $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                $result =  $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1"); 
                                
                                 //container status log insert  
                                $condition = "container_id = " . $container_id . " AND container_status_id='13'  ";
                                $this->common->deleteRecord('tbl_container_status_log',$condition);  
                                $ContainerStatus =array();
                                $ContainerStatus['container_id'] = $container_id;
                                $ContainerStatus['container_status_id'] = 13;
                                $ContainerStatus['current_session_id'] =   $current_session_id;
                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                $ContainerStatus['document_date'] = date("Y-m-d");
                                $result =    $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1"); 
                                
                                
                                if($lastContainerStatusSquence < 6){ 
                                    //last status in container table
                                    $containerStatusUpdate = array();
                                    $containerStatusUpdate['last_container_status_id'] = 13; 
                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $container_id));
                                }   
                                 
                             } 
                        } 
                    }  
                    
                    //sheet tab second import code start
                    $imported = 0; $notimported = 0; $valid = 0; $error  = "";  
                    foreach ($handle->rows(1) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2]) || empty($data[3]) || empty($data[4])){
                            continue;
                        }  

                        $containerNumber = str_replace("'", "&#39;", trim($data[1]));                             
                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2]));                        
                        $subDocType = str_replace("'", "&#39;", trim($data[4]));
                        if($subDocType =='Internal AN'){
                            $subDocTypeId = 22;
                        }else{
                            $subDocTypeId = 23;
                        }

                        $condition = "container_number = '" . $containerNumber . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                         if (!empty($rs_container[0]['container_id'])) {
                            $container_id = $rs_container[0]['container_id']; 

                            $doc_data = array(); 
                            $doc_data['uploaded_document_file'] = str_replace("'", "&#39;", trim($data[0]));
                            $doc_data['container_id'] = $container_id;
                            $doc_data['document_type_id'] = $docTypeID; 
                            $doc_data['sub_document_type_id'] = $subDocTypeId; 
                            $doc_data['current_session_id'] = $current_session_id;  
                            $doc_data['uploaded_document_number'] = $uploaded_document_name; 
                            $doc_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $doc_data['updated_on'] = date("Y-m-d H:i:s");

                            $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND sub_document_type_id=".$subDocTypeId."  AND uploaded_document_number ='".$uploaded_document_name."' "; 
                            $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "document_uploaded_files_id", $condition);
                            $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                            if (!empty($rs_mapping[0]['document_uploaded_files_id'])) {
                                 //update data 
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $doc_data, array("document_uploaded_files_id" => $rs_mapping[0]['document_uploaded_files_id']));                             
                                
                            }else{  
                                $doc_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_data['created_on'] = date("Y-m-d H:i:s");  
                                //insert data 
                                $result = $this->common->insertData("tbl_document_ls_uploaded_files", $doc_data, "1");   
                            }
                            
                            if ($result) {
                               $imported++;
                            } else {
                                $notimported++;
                            }  

                         } 
                    } 
                     
                    foreach ($handle->rows(2) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2]) || empty($data[3])){
                            continue;
                        }  
                        
                        $subDocType = str_replace("'", "&#39;", trim($data[1]));
                        if($subDocType =='Internal AN'){
                            $subDocTypeId = 22;
                        }else{
                            $subDocTypeId = 23;
                        }

                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2])); 
                        
                        $condition = " document_type_id=" . $docTypeID . " AND sub_document_type_id=".$subDocTypeId." AND uploaded_document_number ='".$uploaded_document_name."' "; 
                        $this->common->deleteRecord('tbl_custom_field_data_submission',$condition);   

                        //get custom_field_structure_id according to document type
                        $custom_field = array();
                        $condition = " document_type_id=" . $docTypeID . " AND status='Active' "; 
                        $chk_fields_sql = $this->common->Fetch("tbl_custom_field_structure", "custom_field_structure_id,custom_field_title", $condition);
                        $rs_fields = $this->common->MySqlFetchRow($chk_fields_sql, "array");
                        if (!empty($rs_fields)) {
                            foreach ($rs_fields as $value) {
                                if(trim($value['custom_field_title'])=='Date of Document'){
                                    $DateofDocument = '';
                                    if(!empty(str_replace("'", "&#39;", trim($data[3])))){
                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[3]))); 
                                        $DateofDocument = date('Y-m-d',$DateVal); 
                                    }
                                    $custom_field[$value['custom_field_structure_id']] = $DateofDocument;
                                }  
                                
                                if(trim($value['custom_field_title'])=='Ocean Freight'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[4])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='Trucking Charges'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[5])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='Local Charge'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[6])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='AMS'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[7])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='Destination Charge'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[8])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='Other Charge'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[9])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='HBL Number'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[10])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='MBL Number'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[11])); 
                                }
                                
                            }
                        }
                        //echo '<pre>'; print_r($custom_field); die;
                        //insert custom fields data
                        if (!empty($custom_field)) {
                            foreach ($custom_field as $key => $value) { 
                                $custom_data = array();
                                $custom_data['custom_field_structure_id'] = $key;
                                $custom_data['custom_field_structure_value'] =  $value; 
                                $custom_data['uploaded_document_number'] =  $uploaded_document_name;                            
                                $custom_data['document_type_id'] =   $docTypeID;
                                $custom_data['sub_document_type_id'] =  $subDocTypeId;
                                $custom_data['current_session_id'] = $current_session_id;
                                $custom_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['updated_on'] = date("Y-m-d H:i:s");
                                $custom_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['created_on'] = date("Y-m-d H:i:s"); 
                                //insert data 
                                $result = $this->common->insertData("tbl_custom_field_data_submission", $custom_data, "1");   
                            }
                        }  

                        //get containers according to document number
                        $condition = "uploaded_document_number = '".$uploaded_document_name."' And document_type_id = '".$docTypeID."' AND sub_document_type_id='".$subDocTypeId."' ";
                        $chk_container_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                        if (!empty($rs_container)) {
                            foreach ($rs_container as $value) {
                                $container_id = $value['container_id']; 
                                $hbl_number = str_replace("'", "&#39;", trim($data[10])); 
                                //insert update date of inspection and place of inspection 
                                if(!empty($hbl_number)){
                                    $data_supp = array(); 
                                    $data_supp['hbl_number'] = $hbl_number;  

                                    $condition = " container_id = ".$container_id." AND document_type_id = ".$docTypeID." ";
                                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data','custom_dynamic_data_id',$condition); 
                                    if(!empty($dynamic_data[0]['custom_dynamic_data_id'])){
                                        $condition = " custom_dynamic_data_id = ".$dynamic_data[0]['custom_dynamic_data_id']." "; 
                                        $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition); 
                                    }else{ 
                                        $data_supp['container_id'] = $container_id;
                                        $data_supp['document_type_id'] = $docTypeID; 
                                        $data_supp['sub_document_type_id'] = $subDocTypeId; 
                                        $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                                    }   
                                }

                                //update container information
//                                $condition = " container_id = ".$container_id." ";
//                                $chk_containerdata_sql = $this->common->Fetch("tbl_container", "container_id,email_subject", $condition);
//                                $rs_containerdata = $this->common->MySqlFetchRow($chk_containerdata_sql, "array");
//
//                                if(!empty($rs_containerdata[0]['container_id'])){  
//                                    $data_container = array();
//
//                                    $data_container['email_subject'] = $rs_containerdata[0]['email_subject']; 
//
//                                    if(!empty(str_replace("'", "&#39;", trim($data[12])))){
//                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[12]))); 
//                                        $data_container['etd'] = date('Y-m-d',$DateVal); 
//                                    } 
//
//                                    if(!empty(str_replace("'", "&#39;", trim($data[13])))){
//                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[13]))); 
//                                        $data_container['eta'] = date('Y-m-d',$DateVal); 
//                                    } 
//                                     
//                                    if(!empty(str_replace("'", "&#39;", trim($data[14])))){
//                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[14]))); 
//                                        $data_container['revised_eta'] = date('Y-m-d',$DateVal); 
//                                    } 
//                                    
//                                    $condition = " container_id = ".$rs_containerdata[0]['container_id']." ";
//                                    $this->common->updateData("tbl_container", $data_container, $condition);  
//                                } 
                                
                            }
                        }  
                        
                        //code update date of document in container status log  
                        $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND dluf.uploaded_document_number = '".$uploaded_document_name."' AND cfds.current_session_id = '".$current_session_id."' ";
                        $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));  
                        $join_tables = array(
                            array("","tbl_custom_field_structure as cfs","cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()), 
                            array("","tbl_document_ls_uploaded_files as dluf","dluf.uploaded_document_number = cfds.uploaded_document_number", array('dluf.container_id')), 
                        );  
                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,'','dluf.container_id');
                        $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
                        //echo $this->db->last_query();exit;
                        if(!empty($dateOfDocumentData)){
                            foreach ($dateOfDocumentData as $value) {
                                $data_doc_date = array();
                                $condition = " current_session_id = '".$current_session_id."' AND  container_id=".$value['container_id']." ";
                                $data_doc_date['document_date'] = date("Y-m-d", strtotime($value['custom_field_structure_value']));
                                $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);  
                            } 
                        }

                    }
                         
                    //start sku tab import code
                    foreach ($handle->rows(3) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $container_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_number = str_replace("'", "&#39;", trim($data[1]));
                        $quantity = 0;
                        if(!empty(str_replace("'", "&#39;", trim($data[2])))){
                            $quantity = str_replace("'", "&#39;", trim($data[2]));  
                        } 
                        
                        //get order id
                        $condition_container = "container_number = '" . $container_number . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,order_id", $condition_container);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                        //echo $this->db->last_query(); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                        //condition for order and sku id blank
                        if (empty($rs_container[0]['container_id']) || empty($rs_sku[0]['sku_id'])) {
                            continue;
                        }else{                            
                            $container_id = $rs_container[0]['container_id'];
                            $order_id = $rs_container[0]['order_id'];
                            $sku_id = $rs_sku[0]['sku_id'];
                            
                            $condition_contract = "order_id = ".$order_id." AND sku_id = ".$sku_id." "; 
                            $chk_contract_sql = $this->common->Fetch("tbl_order_sku", "order_id", $condition_contract);
                            $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array");  
                            
                            if (!empty($rs_contract[0]['order_id'])) {
                                $data = array();
                                $data['container_id'] = $container_id;
                                $data['sku_id'] = $sku_id;  
                                
                                $this->common->deleteRecord('tbl_container_sku', array("container_id" => $container_id,"sku_id" => $sku_id)); 
                                
                                $qty = $this->getContainerSKUQty($sku_id,$order_id,(int)$quantity,'Duplicate');                                
                                $data['quantity'] = (int)$qty;

                                $result = $this->common->insertData("tbl_container_sku", $data, "1"); 
                            }   
                        }  
                    }
                    
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit; 
                     
                //start 7501 import code    
                }else if($docTypeID == 7){ 
                     
                    //sheet tab first import code start
                    foreach ($handle->rows() as $key => $data) {
                        //check for header column value
                        if ($key == 0) {  
                             
                        }else{
                            //check for blank value of all column value of row.
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 

                            if (trim($data[1]) == "") {
                                $error .= "LS Number cannot be empty in row : " . ($key + 1) . "<br>";
                                $valid++;
                            } 
                        }  
                    }
                    if ($valid != 0) {
                        $statusData['success'] = false;
                        $statusData['msg'] = $error;
                        print_r(json_encode($statusData));
                        exit;
                    } else {
                        foreach ($handle->rows() as $key => $data) {
                            //check for header row
                            if ($key == 0) {
                                continue;
                            }

                            //check for blank value of all column value of row. 
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 
                            
                            $LSNumber = str_replace("'", "&#39;", trim($data[1])); 
                            $containerNumber = str_replace("'", "&#39;", trim($data[2]));  
                            
                            $condition = "container_number = '" . $containerNumber . "' "; 
                            $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
                            $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                            
                             if (!empty($rs_container[0]['container_id'])) {
                                $container_id = $rs_container[0]['container_id']; 

                                $mapping_data = array();
                                $mapping_data['container_id'] = $container_id;
                                $mapping_data['document_type_id'] = $docTypeID; 
                                $mapping_data['ls_number'] = $LSNumber; 
                                $mapping_data['current_session_id'] =  $current_session_id;  
                                $mapping_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $mapping_data['updated_on'] = date("Y-m-d H:i:s");
                                
                                 
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND ls_number='".$LSNumber."' "; 
                                $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_container_mapping", "document_container_mapping_id", $condition);
                                $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                                if (!empty($rs_mapping[0]['document_container_mapping_id'])) {
                                     //update data 
                                    $result = $this->common->updateData("tbl_document_ls_container_mapping", $mapping_data, array("document_container_mapping_id" => $rs_mapping[0]['document_container_mapping_id']));                             
                                    
                                }else{  
                                    $mapping_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $mapping_data['created_on'] = date("Y-m-d H:i:s");  
                                    //insert data 
                                    $result = $this->common->insertData("tbl_document_ls_container_mapping", $mapping_data, "1");  
                                } 
                                
                                // container status insert
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . "  "; 
                                $this->common->deleteRecord('tbl_container_uploaded_document_status',$condition); 

                                $doc_status_data = array();
                                $doc_status_data['container_id'] = $container_id;
                                $doc_status_data['document_type_id'] = $docTypeID;
                                $doc_status_data['sub_document_type_id'] = 0;
                                $doc_status_data['document_type_squence'] = 8;
                                $doc_status_data['status'] = 'Unchecked';
                                $doc_status_data['current_session_id'] =   $current_session_id;
                                $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                $result =  $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1"); 
                                
                             } 
                        } 
                    }  
                    
                    //sheet tab second import code start
                    $imported = 0; $notimported = 0; $valid = 0; $error  = "";  
                    foreach ($handle->rows(1) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        }  

                        $containerNumber = str_replace("'", "&#39;", trim($data[1]));                             
                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2]));                        
                         

                        $condition = "container_number = '" . $containerNumber . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                         if (!empty($rs_container[0]['container_id'])) {
                            $container_id = $rs_container[0]['container_id']; 

                            $doc_data = array(); 
                            $doc_data['uploaded_document_file'] = str_replace("'", "&#39;", trim($data[0]));
                            $doc_data['container_id'] = $container_id;
                            $doc_data['document_type_id'] = $docTypeID; 
                            $doc_data['sub_document_type_id'] = 0; 
                            $doc_data['current_session_id'] = $current_session_id;  
                            $doc_data['uploaded_document_number'] = $uploaded_document_name; 
                            $doc_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $doc_data['updated_on'] = date("Y-m-d H:i:s");

                            $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND uploaded_document_number ='".$uploaded_document_name."' "; 
                            $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "document_uploaded_files_id", $condition);
                            $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                            if (!empty($rs_mapping[0]['document_uploaded_files_id'])) {
                                 //update data 
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $doc_data, array("document_uploaded_files_id" => $rs_mapping[0]['document_uploaded_files_id']));                             
                                
                            }else{  
                                $doc_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_data['created_on'] = date("Y-m-d H:i:s");  
                                //insert data 
                                $result = $this->common->insertData("tbl_document_ls_uploaded_files", $doc_data, "1");   
                            } 
                            
                            if ($result) {
                               $imported++;
                            } else {
                               $notimported++;
                            }  

                         } 
                    }          
                     
                    foreach ($handle->rows(2) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        }  

                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[1])); 
                        
                        $condition = " document_type_id=" . $docTypeID . " AND uploaded_document_number ='".$uploaded_document_name."' "; 
                        $this->common->deleteRecord('tbl_custom_field_data_submission',$condition);   

                        //get custom_field_structure_id according to document type
                        $custom_field = array();
                        $condition = " document_type_id=" . $docTypeID . " AND status='Active' "; 
                        $chk_fields_sql = $this->common->Fetch("tbl_custom_field_structure", "custom_field_structure_id,custom_field_title", $condition);
                        $rs_fields = $this->common->MySqlFetchRow($chk_fields_sql, "array");
                        if (!empty($rs_fields)) {
                            foreach ($rs_fields as $value) {
                                if(trim($value['custom_field_title'])=='Date of Document'){
                                    $DateofDocument = '';
                                    if(!empty(str_replace("'", "&#39;", trim($data[2])))){
                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[2]))); 
                                        $DateofDocument = date('Y-m-d',$DateVal); 
                                    }
                                    $custom_field[$value['custom_field_structure_id']] = $DateofDocument;
                                }  
                                
                                if(trim($value['custom_field_title'])=='HBL Number'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[3])); 
                                } 
                                
                                if(trim($value['custom_field_title'])=='Merchandise Processing Fees'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[4])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='Harbor Maintenance Fee'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[5])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='Bulk MRO Container Ref Number'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[6])); 
                                } 
                                
                            }
                        }
                        //echo '<pre>'; print_r($custom_field); die;
                        //insert custom fields data
                        if (!empty($custom_field)) {
                            foreach ($custom_field as $key => $value) { 
                                $custom_data = array();
                                $custom_data['custom_field_structure_id'] = $key;
                                $custom_data['custom_field_structure_value'] =  $value; 
                                $custom_data['uploaded_document_number'] =  $uploaded_document_name;                            
                                $custom_data['document_type_id'] =   $docTypeID;
                                $custom_data['sub_document_type_id'] =  0;
                                $custom_data['current_session_id'] = $current_session_id;
                                $custom_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['updated_on'] = date("Y-m-d H:i:s");
                                $custom_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['created_on'] = date("Y-m-d H:i:s"); 
                                //insert data 
                                $result = $this->common->insertData("tbl_custom_field_data_submission", $custom_data, "1");   
                            }
                        }  

                        //get containers according to document number
                        $condition = "uploaded_document_number = '".$uploaded_document_name."' And document_type_id = '".$docTypeID."' ";
                        $chk_container_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                        if (!empty($rs_container)) {
                            foreach ($rs_container as $value) {
                                $container_id = $value['container_id']; 
                                $hbl_number = str_replace("'", "&#39;", trim($data[3])); 
                                //insert update date of inspection and place of inspection 
                                if(!empty($hbl_number)){
                                    $data_supp = array(); 
                                    $data_supp['hbl_number'] = $hbl_number;  

                                    $condition = " container_id = ".$container_id." AND document_type_id = ".$docTypeID." ";
                                    $dynamic_data = $this->common->getData('tbl_custom_dynamic_data','custom_dynamic_data_id',$condition); 
                                    if(!empty($dynamic_data[0]['custom_dynamic_data_id'])){
                                        $condition = " custom_dynamic_data_id = ".$dynamic_data[0]['custom_dynamic_data_id']." "; 
                                        $this->common->updateData("tbl_custom_dynamic_data", $data_supp, $condition); 
                                    }else{ 
                                        $data_supp['container_id'] = $container_id;
                                        $data_supp['document_type_id'] = $docTypeID;  
                                        $this->common->insertData("tbl_custom_dynamic_data", $data_supp, "1");
                                    }   
                                }
                                
                            }
                        } 
                        
                        //code update date of document in container status log  
                        $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND dluf.uploaded_document_number = '".$uploaded_document_name."' AND cfds.current_session_id = '".$current_session_id."' ";
                        $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));  
                        $join_tables = array(
                            array("","tbl_custom_field_structure as cfs","cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()), 
                            array("","tbl_document_ls_uploaded_files as dluf","dluf.uploaded_document_number = cfds.uploaded_document_number", array('dluf.container_id')), 
                        );  
                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,'','dluf.container_id');
                        $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
                        //echo $this->db->last_query();exit;
                        if(!empty($dateOfDocumentData)){
                            foreach ($dateOfDocumentData as $value) {
                                $data_doc_date = array();
                                $condition = " current_session_id = '".$current_session_id."' AND  container_id=".$value['container_id']." ";
                                $data_doc_date['document_date'] = date("Y-m-d", strtotime($value['custom_field_structure_value']));
                                $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);  
                            } 
                        }


                    }
                         
                    //start sku tab import code
                    foreach ($handle->rows(3) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $container_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_number = str_replace("'", "&#39;", trim($data[1]));
                        $quantity = 0;
                        if(!empty(str_replace("'", "&#39;", trim($data[2])))){
                            $quantity = str_replace("'", "&#39;", trim($data[2]));  
                        } 
                        
                        //get order id
                        $condition_container = "container_number = '" . $container_number . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,order_id", $condition_container);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                        //echo $this->db->last_query(); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                        //condition for order and sku id blank
                        if (empty($rs_container[0]['container_id']) || empty($rs_sku[0]['sku_id'])) {
                            continue;
                        }else{                            
                            $container_id = $rs_container[0]['container_id'];
                            $order_id = $rs_container[0]['order_id'];
                            $sku_id = $rs_sku[0]['sku_id'];
                            
                            $condition_contract = "order_id = ".$order_id." AND sku_id = ".$sku_id." "; 
                            $chk_contract_sql = $this->common->Fetch("tbl_order_sku", "order_id", $condition_contract);
                            $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array");  
                            
                            if (!empty($rs_contract[0]['order_id'])) {
                                $data = array();
                                $data['container_id'] = $container_id;
                                $data['sku_id'] = $sku_id;  
                                
                                $this->common->deleteRecord('tbl_container_sku', array("container_id" => $container_id,"sku_id" => $sku_id)); 
                                
                                $qty = $this->getContainerSKUQty($sku_id,$order_id,(int)$quantity,'Duplicate');                                
                                $data['quantity'] = (int)$qty;

                                $result = $this->common->insertData("tbl_container_sku", $data, "1"); 
                            }   
                        }  
                    }
                    
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit; 
                     
                //start ABI import code    
                }else if($docTypeID == 9){ 
                     
                    //sheet tab first import code start
                    foreach ($handle->rows() as $key => $data) {
                        //check for header column value
                        if ($key == 0) {  
                             
                        }else{
                            //check for blank value of all column value of row.
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 

                            if (trim($data[1]) == "") {
                                $error .= "LS Number cannot be empty in row : " . ($key + 1) . "<br>";
                                $valid++;
                            } 
                        }  
                    }
                    if ($valid != 0) {
                        $statusData['success'] = false;
                        $statusData['msg'] = $error;
                        print_r(json_encode($statusData));
                        exit;
                    } else {
                        foreach ($handle->rows() as $key => $data) {
                            //check for header row
                            if ($key == 0) {
                                continue;
                            }

                            //check for blank value of all column value of row. 
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 
                            
                            $LSNumber = str_replace("'", "&#39;", trim($data[1])); 
                            $containerNumber = str_replace("'", "&#39;", trim($data[2]));  
                            
                            $condition = "container_number = '" . $containerNumber . "' "; 
                            $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
                            $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                            
                             if (!empty($rs_container[0]['container_id'])) {
                                $container_id = $rs_container[0]['container_id']; 

                                $mapping_data = array();
                                $mapping_data['container_id'] = $container_id;
                                $mapping_data['document_type_id'] = $docTypeID; 
                                $mapping_data['ls_number'] = $LSNumber; 
                                $mapping_data['current_session_id'] =  $current_session_id;  
                                $mapping_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $mapping_data['updated_on'] = date("Y-m-d H:i:s");
                                
                                 
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND ls_number='".$LSNumber."' "; 
                                $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_container_mapping", "document_container_mapping_id", $condition);
                                $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                                if (!empty($rs_mapping[0]['document_container_mapping_id'])) {
                                     //update data 
                                    $result = $this->common->updateData("tbl_document_ls_container_mapping", $mapping_data, array("document_container_mapping_id" => $rs_mapping[0]['document_container_mapping_id']));                             
                                    
                                }else{  
                                    $mapping_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $mapping_data['created_on'] = date("Y-m-d H:i:s");  
                                    //insert data 
                                    $result = $this->common->insertData("tbl_document_ls_container_mapping", $mapping_data, "1");  
                                } 
                                
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . "  "; 
                                $this->common->deleteRecord('tbl_container_uploaded_document_status',$condition); 

                                $doc_status_data = array();
                                $doc_status_data['container_id'] = $container_id;
                                $doc_status_data['document_type_id'] = $docTypeID;
                                $doc_status_data['sub_document_type_id'] = 0;
                                $doc_status_data['document_type_squence'] = 10;
                                $doc_status_data['status'] = 'Unchecked';
                                $doc_status_data['current_session_id'] =   $current_session_id;
                                $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                $result =  $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1");
                                
                             } 
                        } 
                    }  
                    
                    //sheet tab second import code start
                    $imported = 0; $notimported = 0; $valid = 0; $error  = "";  
                    foreach ($handle->rows(1) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        }  

                        $containerNumber = str_replace("'", "&#39;", trim($data[1]));                             
                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2]));                        
                         

                        $condition = "container_number = '" . $containerNumber . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                         if (!empty($rs_container[0]['container_id'])) {
                            $container_id = $rs_container[0]['container_id']; 

                            $doc_data = array(); 
                            $doc_data['uploaded_document_file'] = str_replace("'", "&#39;", trim($data[0]));
                            $doc_data['container_id'] = $container_id;
                            $doc_data['document_type_id'] = $docTypeID; 
                            $doc_data['sub_document_type_id'] = 0; 
                            $doc_data['current_session_id'] = $current_session_id;  
                            $doc_data['uploaded_document_number'] = $uploaded_document_name; 
                            $doc_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $doc_data['updated_on'] = date("Y-m-d H:i:s");

                            $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND uploaded_document_number ='".$uploaded_document_name."' "; 
                            $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "document_uploaded_files_id", $condition);
                            $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                            if (!empty($rs_mapping[0]['document_uploaded_files_id'])) {
                                 //update data 
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $doc_data, array("document_uploaded_files_id" => $rs_mapping[0]['document_uploaded_files_id']));                             
                                
                            }else{  
                                $doc_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_data['created_on'] = date("Y-m-d H:i:s");  
                                //insert data 
                                $result = $this->common->insertData("tbl_document_ls_uploaded_files", $doc_data, "1");   
                            } 
                            
                            if ($result) {
                               $imported++;
                            } else {
                               $notimported++;
                            }  

                         } 
                    }   
                    
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit; 
                     
                //start DO import code    
                }else if($docTypeID == 8){ 
                     
                    //sheet tab first import code start
                    foreach ($handle->rows() as $key => $data) {
                        //check for header column value
                        if ($key == 0) {  
                             
                        }else{
                            //check for blank value of all column value of row.
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 

                            if (trim($data[1]) == "") {
                                $error .= "LS Number cannot be empty in row : " . ($key + 1) . "<br>";
                                $valid++;
                            } 
                        }  
                    }
                    if ($valid != 0) {
                        $statusData['success'] = false;
                        $statusData['msg'] = $error;
                        print_r(json_encode($statusData));
                        exit;
                    } else {
                        foreach ($handle->rows() as $key => $data) {
                            //check for header row
                            if ($key == 0) {
                                continue;
                            }

                            //check for blank value of all column value of row. 
                            if(empty($data[1]) || empty($data[2])){
                                continue;
                            } 
                            
                            $LSNumber = str_replace("'", "&#39;", trim($data[1])); 
                            $containerNumber = str_replace("'", "&#39;", trim($data[2]));  
                            
                            $condition = "container_number = '" . $containerNumber . "' "; 
                            $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,last_container_status_id", $condition);
                            $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                            
                             if (!empty($rs_container[0]['container_id'])) {
                                $container_id = $rs_container[0]['container_id']; 
                                
                                $condition = " container_status_id = '". $rs_container[0]['last_container_status_id']."' ";
                                $lastContainerStatusSquenceData = $this->common->getData('tbl_container_status','status_squence',$condition);
                                $lastContainerStatusSquence = 0;
                                if(!empty($lastContainerStatusSquenceData)){
                                   $lastContainerStatusSquence = $lastContainerStatusSquenceData[0]['status_squence'];
                                }

                                $mapping_data = array();
                                $mapping_data['container_id'] = $container_id;
                                $mapping_data['document_type_id'] = $docTypeID; 
                                $mapping_data['ls_number'] = $LSNumber; 
                                $mapping_data['current_session_id'] =  $current_session_id;  
                                $mapping_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $mapping_data['updated_on'] = date("Y-m-d H:i:s");
                                 
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND ls_number='".$LSNumber."' "; 
                                $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_container_mapping", "document_container_mapping_id", $condition);
                                $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                                if (!empty($rs_mapping[0]['document_container_mapping_id'])) {
                                     //update data 
                                    $result = $this->common->updateData("tbl_document_ls_container_mapping", $mapping_data, array("document_container_mapping_id" => $rs_mapping[0]['document_container_mapping_id']));                             
                                    
                                }else{  
                                    $mapping_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $mapping_data['created_on'] = date("Y-m-d H:i:s");  
                                    //insert data 
                                    $result = $this->common->insertData("tbl_document_ls_container_mapping", $mapping_data, "1");   
                                } 
                                
                                //container status insert
                                $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . "  "; 
                                $this->common->deleteRecord('tbl_container_uploaded_document_status',$condition); 

                                $doc_status_data = array();
                                $doc_status_data['container_id'] = $container_id;
                                $doc_status_data['document_type_id'] = $docTypeID;
                                $doc_status_data['sub_document_type_id'] = 0;
                                $doc_status_data['document_type_squence'] = 9;
                                $doc_status_data['status'] = 'Unchecked';
                                $doc_status_data['current_session_id'] =   $current_session_id;
                                $doc_status_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['updated_on'] = date("Y-m-d H:i:s");
                                $doc_status_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_status_data['created_on'] = date("Y-m-d H:i:s");
                                $result =  $this->common->insertData("tbl_container_uploaded_document_status", $doc_status_data, "1");  
                                
                                //container status log insert
                                $condition = "container_id = " . $container_id . " AND container_status_id='10'  ";
                                $this->common->deleteRecord('tbl_container_status_log',$condition);  
                                $ContainerStatus =array();
                                $ContainerStatus['container_id'] = $container_id;
                                $ContainerStatus['container_status_id'] = 10;
                                $ContainerStatus['current_session_id'] =   $current_session_id;
                                $ContainerStatus['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                                $ContainerStatus['document_date'] = date("Y-m-d");
                                $result =    $this->common->insertData("tbl_container_status_log", $ContainerStatus, "1"); 
                                
                                if($lastContainerStatusSquence < 8){ 
                                    //last status in container table
                                    $containerStatusUpdate = array();
                                    $containerStatusUpdate['last_container_status_id'] = 10; 
                                    $containerStatusUpdate['last_status_by'] = $_SESSION['mro_session'][0]['user_id'];
                                    $containerStatusUpdate['last_status_date'] = date('Y-m-d');
                                    $this->common->updateData("tbl_container", $containerStatusUpdate, array("container_id" => $container_id));
                                }   
                                
                             } 
                        } 
                    }  
                    
                    //sheet tab second import code start
                    $imported = 0; $notimported = 0; $valid = 0; $error  = "";  
                    foreach ($handle->rows(1) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        }  

                        $containerNumber = str_replace("'", "&#39;", trim($data[1]));                             
                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[2]));                        
                         

                        $condition = "container_number = '" . $containerNumber . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id", $condition);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");

                         if (!empty($rs_container[0]['container_id'])) {
                            $container_id = $rs_container[0]['container_id']; 

                            $doc_data = array(); 
                            $doc_data['uploaded_document_file'] = str_replace("'", "&#39;", trim($data[0]));
                            $doc_data['container_id'] = $container_id;
                            $doc_data['document_type_id'] = $docTypeID; 
                            $doc_data['sub_document_type_id'] = 0; 
                            $doc_data['current_session_id'] = $current_session_id;  
                            $doc_data['uploaded_document_number'] = $uploaded_document_name; 
                            $doc_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                            $doc_data['updated_on'] = date("Y-m-d H:i:s");

                            $condition = "container_id = " . $container_id . " AND document_type_id=" . $docTypeID . " AND uploaded_document_number ='".$uploaded_document_name."' "; 
                            $chk_mapping_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "document_uploaded_files_id", $condition);
                            $rs_mapping = $this->common->MySqlFetchRow($chk_mapping_sql, "array");

                            if (!empty($rs_mapping[0]['document_uploaded_files_id'])) {
                                 //update data 
                                $result = $this->common->updateData("tbl_document_ls_uploaded_files", $doc_data, array("document_uploaded_files_id" => $rs_mapping[0]['document_uploaded_files_id']));                             
                                
                            }else{  
                                $doc_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $doc_data['created_on'] = date("Y-m-d H:i:s");  
                                //insert data 
                                $result = $this->common->insertData("tbl_document_ls_uploaded_files", $doc_data, "1");   
                            } 
                            if ($result) {
                               $imported++;
                            } else {
                                $notimported++;
                            }  

                         } 
                    }  
                             
                     
                    foreach ($handle->rows(2) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        }

                        //check for blank value of all column value of row. 
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        }  

                        $uploaded_document_name = str_replace("'", "&#39;", trim($data[1])); 
                        
                        $condition = " document_type_id=" . $docTypeID . " AND uploaded_document_number ='".$uploaded_document_name."' "; 
                        $this->common->deleteRecord('tbl_custom_field_data_submission',$condition);   

                        //get custom_field_structure_id according to document type
                        $custom_field = array();
                        $condition = " document_type_id=" . $docTypeID . " AND status='Active' "; 
                        $chk_fields_sql = $this->common->Fetch("tbl_custom_field_structure", "custom_field_structure_id,custom_field_title", $condition);
                        $rs_fields = $this->common->MySqlFetchRow($chk_fields_sql, "array");
                        $DateofArrival = '';
                        if (!empty($rs_fields)) {
                            foreach ($rs_fields as $value) {
                                if(trim($value['custom_field_title'])=='Date of Document'){
                                    $DateofDocument = '';
                                    if(!empty(str_replace("'", "&#39;", trim($data[2])))){
                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[2]))); 
                                        $DateofDocument = date('Y-m-d',$DateVal); 
                                    }
                                    $custom_field[$value['custom_field_structure_id']] = $DateofDocument;
                                }  
                                
                                if(trim($value['custom_field_title'])=='BL Number'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[3])); 
                                } 
                                
                                if(trim($value['custom_field_title'])=='Issued By'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[4])); 
                                }
                                
                                if(trim($value['custom_field_title'])=='Actual date of Arrival'){  
                                    if(!empty(str_replace("'", "&#39;", trim($data[5])))){
                                        $DateVal = $xlsx->unixstamp(str_replace("'", "&#39;", trim($data[5]))); 
                                        $DateofArrival = date('Y-m-d',$DateVal); 
                                    }
                                    $custom_field[$value['custom_field_structure_id']] = $DateofArrival;                                    
                                }
                                
                                if(trim($value['custom_field_title'])=='Vessel'){
                                    $custom_field[$value['custom_field_structure_id']] = str_replace("'", "&#39;", trim($data[6])); 
                                } 
                                
                            }
                        }
                        //echo '<pre>'; print_r($custom_field); die;
                        //insert custom fields data
                        if (!empty($custom_field)) {
                            foreach ($custom_field as $key => $value) { 
                                $custom_data = array();
                                $custom_data['custom_field_structure_id'] = $key;
                                $custom_data['custom_field_structure_value'] =  $value; 
                                $custom_data['uploaded_document_number'] =  $uploaded_document_name;                            
                                $custom_data['document_type_id'] =   $docTypeID;
                                $custom_data['sub_document_type_id'] =  0;
                                $custom_data['current_session_id'] = $current_session_id;
                                $custom_data['updated_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['updated_on'] = date("Y-m-d H:i:s");
                                $custom_data['created_by'] = $_SESSION['mro_session'][0]['user_id'];
                                $custom_data['created_on'] = date("Y-m-d H:i:s"); 
                                //insert data 
                                $result = $this->common->insertData("tbl_custom_field_data_submission", $custom_data, "1");   
                            }
                        }
                        
                        //code update date of document in container status log  
                        $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND dluf.uploaded_document_number = '".$uploaded_document_name."' AND cfds.current_session_id = '".$current_session_id."' ";
                        $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));  
                        $join_tables = array(
                            array("","tbl_custom_field_structure as cfs","cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()), 
                            array("","tbl_document_ls_uploaded_files as dluf","dluf.uploaded_document_number = cfds.uploaded_document_number", array('dluf.container_id')), 
                        );  
                        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,'','dluf.container_id');
                        $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
                        //echo $this->db->last_query();exit;
                        if(!empty($dateOfDocumentData)){
                            foreach ($dateOfDocumentData as $value) {
                                $data_doc_date = array();
                                $condition = " current_session_id = '".$current_session_id."' AND  container_id=".$value['container_id']." ";
                                $data_doc_date['document_date'] = date("Y-m-d", strtotime($value['custom_field_structure_value']));
                                $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);  
                            } 
                        }
                        
                        //update actual date of arrival when upload DO then update container revised eta field
                        if(!empty($DateofArrival)){   
                            //get containers according to document number
                           $condition = "uploaded_document_number = '".$uploaded_document_name."' And document_type_id = '".$docTypeID."' ";
                           $chk_container_sql = $this->common->Fetch("tbl_document_ls_uploaded_files", "container_id", $condition);
                           $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array"); 
                           if (!empty($rs_container)) {
                               foreach ($rs_container as $value) { 
                                    $data_revised_eta = array(); 
                                    $data_revised_eta['revised_eta'] = !empty($DateofArrival) ? $DateofArrival : NULL; 
                                    $condition = " container_id = " . $value['container_id'] . " ";
                                    $this->common->updateData("tbl_container", $data_revised_eta, $condition); 
                               }
                           } 
                        }

                    }
                      
                    //start sku tab import code
                    foreach ($handle->rows(3) as $key => $data) {
                        //check for header row
                        if ($key == 0) {
                            continue;
                        } 
                        
                        //check for blank value of all column value of row.
                        if(empty($data[0]) || empty($data[1]) || empty($data[2])){
                            continue;
                        } 
                        
                        //bussiness partner details
                        $container_number = str_replace("'", "&#39;", trim($data[0]));
                        $sku_number = str_replace("'", "&#39;", trim($data[1]));
                        $quantity = 0;
                        if(!empty(str_replace("'", "&#39;", trim($data[2])))){
                            $quantity = str_replace("'", "&#39;", trim($data[2]));  
                        } 
                        
                        //get order id
                        $condition_container = "container_number = '" . $container_number . "' "; 
                        $chk_container_sql = $this->common->Fetch("tbl_container", "container_id,order_id", $condition_container);
                        $rs_container = $this->common->MySqlFetchRow($chk_container_sql, "array");
                        //echo $this->db->last_query(); 
                        
                        $condition_sku = "sku_number = '" . $sku_number . "' "; 
                        $chk_sku_sql = $this->common->Fetch("tbl_sku", "sku_id", $condition_sku);
                        $rs_sku = $this->common->MySqlFetchRow($chk_sku_sql, "array"); 
                        
                        //condition for order and sku id blank
                        if (empty($rs_container[0]['container_id']) || empty($rs_sku[0]['sku_id'])) {
                            continue;
                        }else{                            
                            $container_id = $rs_container[0]['container_id'];
                            $order_id = $rs_container[0]['order_id'];
                            $sku_id = $rs_sku[0]['sku_id'];
                            
                            $condition_contract = "order_id = ".$order_id." AND sku_id = ".$sku_id." "; 
                            $chk_contract_sql = $this->common->Fetch("tbl_order_sku", "order_id", $condition_contract);
                            $rs_contract = $this->common->MySqlFetchRow($chk_contract_sql, "array");  
                            
                            if (!empty($rs_contract[0]['order_id'])) {
                                $data = array();
                                $data['container_id'] = $container_id;
                                $data['sku_id'] = $sku_id;  
                                
                                $this->common->deleteRecord('tbl_container_sku', array("container_id" => $container_id,"sku_id" => $sku_id)); 
                                
                                $qty = $this->getContainerSKUQty($sku_id,$order_id,(int)$quantity,'Duplicate');                                
                                $data['quantity'] = (int)$qty;

                                $result = $this->common->insertData("tbl_container_sku", $data, "1"); 
                            }   
                        }  
                    }
                    
                    $statusData['success'] = true;
                    $statusData['msg'] = "Success";
                    $statusData['imported'] = $imported;
                    $statusData['notimported'] = $notimported;
                    print_r(json_encode($statusData));
                    exit; 
                     
                    
                }
                
                
                
                
            }
        }else{
            $statusData['success'] = false;
            $statusData['msg'] = 'Failed'; //'<div style="color:red">' . $error . "</div>";
            print_r(json_encode($statusData));
        }
    }

}

?>
