<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left1" >
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                            <span>></span>
                            <p>Document Import</p>
                        </div> 
                    </div>
                    <div class="title-sec-right1" > 
                        <a href="#" class="btn-primary-mro" id="importls">LS Import</a>&nbsp; 
                        <a href="#" class="btn-primary-mro" id="importfri">FRI Import</a>&nbsp; 
                        <a href="#" class="btn-primary-mro" id="importins">INS Import</a>&nbsp; 
                        <a href="#" class="btn-primary-mro" id="importfcr">FCR Import</a>&nbsp; 
                        <a href="#" class="btn-primary-mro" id="importhbl">HBL Import</a>&nbsp; 
                        <a href="#" class="btn-primary-mro" id="importan">Arrival Notice Import</a>&nbsp; 
                        <a href="#" class="btn-primary-mro" id="import7501">7501 Import</a>&nbsp; 
                        <a href="#" class="btn-primary-mro" id="importdo">DO Import</a>&nbsp; 
                        <a href="#" class="btn-primary-mro" id="importabi">ABI Import</a>  
                    </div>
                </div>
                <div id="docuement_loader" style="display: none;">
                    <img src="<?= base_url('assets/images/loading-bulkmro.gif') ?>" alt="">
                </div> 
                <div class="page-content-wrapper" id="efromls" style="display:none;">
                    <form id="excel_form_ls" name="excel_form_ls" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload LS Excel</h6> 
                            <input type="file" id="excelfile_ls" name="excelfile_ls" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel(2)" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample file for LS.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div> 
                <div class="page-content-wrapper" id="efromfri" style="display:none;">
                    <form id="excel_form_fri" name="excel_form_fri" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload FRI Excel</h6> 
                            <input type="file" id="excelfile_fri" name="excelfile_fri" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel(3)" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample file for FRI.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div>
                
                <div class="page-content-wrapper" id="efromins" style="display:none;">
                    <form id="excel_form_ins" name="excel_form_ins" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload INS Excel</h6> 
                            <input type="file" id="excelfile_ins" name="excelfile_ins" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel(1)" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample file for INS.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div>
                
                <div class="page-content-wrapper" id="efromfcr" style="display:none;">
                    <form id="excel_form_fcr" name="excel_form_fcr" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload FCR Excel</h6> 
                            <input type="file" id="excelfile_fcr" name="excelfile_fcr" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel(4)" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample file for FCR.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div>
                
                <div class="page-content-wrapper" id="efromhbl" style="display:none;">
                    <form id="excel_form_hbl" name="excel_form_hbl" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload HBL Excel</h6> 
                            <input type="file" id="excelfile_hbl" name="excelfile_hbl" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel(5)" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample file for HBL.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div>
                
                <div class="page-content-wrapper" id="efroman" style="display:none;">
                    <form id="excel_form_an" name="excel_form_an" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload Arrival Notice Excel</h6> 
                            <input type="file" id="excelfile_an" name="excelfile_an" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel(10)" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample file for AN Doc.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div>
                
                <div class="page-content-wrapper" id="efrom7501" style="display:none;">
                    <form id="excel_form_7501" name="excel_form_7501" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload 7501 Excel</h6> 
                            <input type="file" id="excelfile_7501" name="excelfile_7501" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel(7)" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample file for 7501.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div>
                
                <div class="page-content-wrapper" id="efromabi" style="display:none;">
                    <form id="excel_form_abi" name="excel_form_abi" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload ABI Excel</h6> 
                            <input type="file" id="excelfile_abi" name="excelfile_abi" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel(9)" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample file for ABI.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div>
                
                <div class="page-content-wrapper" id="efromdo" style="display:none;">
                    <form id="excel_form_do" name="excel_form_do" class="form-horizontal form-bordered" style="min-height: 70px;">
                        <div class="form-group">
                            <h6 class="mb8">Upload DO Excel</h6> 
                            <input type="file" id="excelfile_do" name="excelfile_do" accept=".xlsx" style="width:182px;">
                            <a href="javascript:void(0)" onclick="uploadExcel(8)" class="btn-primary-mro upload-button">
                              <i class="fa fa-upload"></i> Upload
                            </a>
                            <div class="btn-group">
                                <a class="btn-primary-mro" href="<?php echo base_url("assets/excel/sample/sample file for DO.xlsx"); ?>"> 
                                  Download Sample
                                </a>
                            </div>                               
                        </div> 
                    </form>
                </div>
                
                
                </br>  
            </div>
        </div>
    </div>
</section>
<script>

 function uploadExcel(type)
 {
    if(type==2){
        var excelfile = "excelfile_ls";
    }else if(type==3){
        var excelfile = "excelfile_fri";
    }else if(type==1){
        var excelfile = "excelfile_ins";
    }else if(type==4){
        var excelfile = "excelfile_fcr";
    }else if(type==5){
        var excelfile = "excelfile_hbl";
    }else if(type==10){
        var excelfile = "excelfile_an";
    }else if(type==7){
        var excelfile = "excelfile_7501";
    }else if(type==9){
        var excelfile = "excelfile_abi";
    }else if(type==8){
        var excelfile = "excelfile_do";
    }
        
    if( document.getElementById(excelfile).files.length == 0 ){ 
        showInsertUpdateMessage("Please Choose a xlsx file.",false);   
    }
    else
    {
        $(".upload-button").hide();
        $("#docuement_loader").show();
        if(type==2){
            var data = new FormData($('#excel_form_ls')[0]);
        }else if(type==3){
            var data = new FormData($('#excel_form_fri')[0]);
        }else if(type==1){
            var data = new FormData($('#excel_form_ins')[0]);
        }else if(type==4){
            var data = new FormData($('#excel_form_fcr')[0]);
        }else if(type==5){
            var data = new FormData($('#excel_form_hbl')[0]);
        }else if(type==10){
            var data = new FormData($('#excel_form_an')[0]);
        }else if(type==7){
            var data = new FormData($('#excel_form_7501')[0]);
        }else if(type==9){
            var data = new FormData($('#excel_form_abi')[0]);
        }else if(type==8){
            var data = new FormData($('#excel_form_do')[0]);
        }
        
        var url = '<?php echo base_url('import_document/LSFRI_Import?type=') ?>'+type; 
        $.ajax({
            url: url,
            type: 'POST',
            mimeType: "multipart/form-data",
            contentType: false,
            cache: false,
            processData: false,
            data: data, 
            success: function(data)
            {
                $("#docuement_loader").hide();
                var html="";
                var response = JSON.parse(data); 
                
                if (response.success) {  
                    showInsertUpdateMessage(response.msg,response);
                    showInsertUpdateMessage("Total entry done: "+response.imported,response); 
                    if(response.notimported > 0){
                        showInsertUpdateMessage("Total entry failed: "+response.notimported,false);   
                    } 
                    
                    if(type==2){
                        $('#excel_form_ls')[0].reset();
                    }else if(type==3){
                        $('#excel_form_fri')[0].reset();
                    }else if(type==1){
                        $('#excel_form_ins')[0].reset();
                    }else if(type==4){
                        $('#excel_form_fcr')[0].reset();
                    }else if(type==5){
                        $('#excel_form_hbl')[0].reset();
                    }else if(type==10){
                        $('#excel_form_an')[0].reset();
                    }else if(type==7){
                        $('#excel_form_7501')[0].reset();
                    }else if(type==9){
                        $('#excel_form_abi')[0].reset();
                    }else if(type==8){
                        $('#excel_form_do')[0].reset();
                    }
                    
                    setTimeout(function () {  
                        location.reload();
                    }, 3000);
                }else{
                    showInsertUpdateMessage(response.msg,response,false);
                }
                $(".upload-button").show();
            }
        });
    }
}

$("#importls").click(function(){
   $("#efromls").toggle();
   $("#efromfri").hide();
   $("#efromins").hide();
   $("#efromfcr").hide();
   $("#efromhbl").hide();
   $("#efroman").hide();
   $("#efrom7501").hide();
   $("#efromabi").hide();
   $("#efromdo").hide();
});

$("#importfri").click(function(){
   $("#efromfri").toggle();
   $("#efromls").hide();
   $("#efromins").hide();
   $("#efromfcr").hide();
   $("#efromhbl").hide();
   $("#efroman").hide();
   $("#efrom7501").hide();
   $("#efromabi").hide();
   $("#efromdo").hide();
});

$("#importins").click(function(){
   $("#efromins").toggle();
   $("#efromls").hide();
   $("#efromfri").hide();
   $("#efromfcr").hide();
   $("#efromhbl").hide();
   $("#efroman").hide();
   $("#efrom7501").hide();
   $("#efromabi").hide();
   $("#efromdo").hide();
});

$("#importfcr").click(function(){
   $("#efromfcr").toggle();
   $("#efromls").hide();
   $("#efromfri").hide();
   $("#efromins").hide();
   $("#efromhbl").hide();
   $("#efroman").hide();
   $("#efrom7501").hide();
   $("#efromabi").hide();
   $("#efromdo").hide();
});


$("#importhbl").click(function(){
   $("#efromhbl").toggle();
   $("#efromls").hide();
   $("#efromfri").hide();
   $("#efromins").hide();
   $("#efromfcr").hide();
   $("#efroman").hide();
   $("#efrom7501").hide();
   $("#efromabi").hide();
   $("#efromdo").hide();
});

$("#importan").click(function(){
   $("#efroman").toggle();
   $("#efromls").hide();
   $("#efromfri").hide();
   $("#efromins").hide();
   $("#efromfcr").hide();
   $("#efromhbl").hide();
   $("#efrom7501").hide();
   $("#efromabi").hide();
   $("#efromdo").hide();
});

$("#import7501").click(function(){
   $("#efrom7501").toggle();
   $("#efromls").hide();
   $("#efromfri").hide();
   $("#efromins").hide();
   $("#efromfcr").hide();
   $("#efromhbl").hide();
   $("#efroman").hide();
   $("#efromabi").hide();
   $("#efromdo").hide();
});

$("#importabi").click(function(){
   $("#efromabi").toggle();
   $("#efromls").hide();
   $("#efromfri").hide();
   $("#efromins").hide();
   $("#efromfcr").hide();
   $("#efromhbl").hide();
   $("#efroman").hide();
   $("#efrom7501").hide();
   $("#efromdo").hide();
});

$("#importdo").click(function(){
   $("#efromdo").toggle();
   $("#efromls").hide();
   $("#efromfri").hide();
   $("#efromins").hide();
   $("#efromfcr").hide();
   $("#efromhbl").hide();
   $("#efroman").hide();
   $("#efrom7501").hide();
   $("#efromabi").hide();
});



</script>