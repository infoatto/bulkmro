<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("permission"); ?>">Permission List</a>
                                <span>></span>
                                <p>Add Permission</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add Permission</h1>
                            </div>
                        </div>       
                        <div class="title-sec-right"> 
                            <button type="submit" class="btn-primary-mro">Save</button>
                            <a href="<?php echo base_url("permission"); ?>" class="btn-transparent-mro cancel">Cancel</a> 
                        </div>  
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="perm_id" id="perm_id" value="<?php echo(!empty($permission_details['perm_id'])) ? $permission_details['perm_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="PermDesc">Permission<sup>*</sup></label>
                                    <input type="text" name="perm_desc" id="perm_desc" class="input-form-mro" value="<?php echo(!empty($permission_details['perm_desc'])) ? $permission_details['perm_desc'] : ""; ?>">
                                </div>
                            </div>  
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {
        var vRules = {
            "perm_desc": {required: true}
        };
        var vMessages = {
            "perm_desc": {required: "Please enter permission."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>permission/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response){
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('permission') ?>";
                            }, 3000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });
</script>