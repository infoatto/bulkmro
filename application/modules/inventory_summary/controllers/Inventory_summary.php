<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Inventory_summary extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Inventory_summarymodel', 'inventory_summarymodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
        if(!$this->privilegeduser->hasPrivilege("ContainersDetailsView")){
            redirect('dashboard');
        }
    }

    function index() {
        $data['container_details'] = array(); 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');
        $this->load->view('inventory_summary/index', $data);
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php'); 
    }

    function getProductDetails() {
        $container_id = $_GET['container_id'];
        $productData = $this->inventory_summarymodel->getProductDetails($container_id);
        $strtr = '';
        $strtable = '';
        if (!empty($productData)) {
            $srNo = 0;
            foreach ($productData as $value) {
                $srNo++;
                $strtr .= '<tr>
                        <td>'.$srNo.'</td>
                        <td>'.$value['product_name'].'</td>
                        <td>'.$value['sku_number'].'</td>
                        <td>'.$value['alias'].'</td>
                        <td>'.$value['alias'].'</td>
                        <td>'.$value['brand_name'].'</td>
                        <td>'.$value['size_name'].'</td>
                        <td>'.$value['quantity'].'</td>
                        <td>'.$value['uom_name'].'</td>
                    </tr>';
            }
        }
        
        $strtable .= '<table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">Sr. No.</th>
                                <th scope="col">Product Description</th>
                                <th scope="col">BM SKU</th>
                                <th scope="col">Manufacturer</th>
                                <th scope="col">Manufacturer SKU</th>
                                <th scope="col">Brand</th>
                                <th scope="col">Size</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">UoM</th>
                            </tr>
                        </thead>
                        <tbody>'; 
        $strtable = $strtable.$strtr.'</tbody></table>';
        echo json_encode(array('productDetails' => $strtable));
        exit;
    } 
}

?>
