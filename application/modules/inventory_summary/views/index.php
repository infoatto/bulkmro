<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="#">Home</a>
                            <span>></span>
                            <p>Inventory Summary</p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title">Inventory Summary</h1>
                        </div>
                    </div>          
                </div>        
                <div class="bp-list-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th scope="col">Brand</th>
                                    <th scope="col">Contract</th>
                                    <th scope="col">Unit Price</th>
                                    <th scope="col">Freight</th>
                                    <th scope="col">SGS</th>
                                    <th scope="col">Total Unit<br/> Price</th>
                                    <th scope="col">Small</th>
                                    <th scope="col">Medium</th>
                                    <th scope="col">Large</th>
                                    <th scope="col">X-Large</th>
                                    <th scope="col">Total Volume <br/>In Stock</th>
                                    <th scope="col">Total Stock <br/>Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Brand 1</td>
                                    <td>100002</td>
                                    <td>$ 3.00</td>
                                    <td>$ 0.50</td>
                                    <td>$ 0.03</td>
                                    <td>$ 1.53</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>95,080.00</td>
                                    <td>$335,632.40</td>
                                </tr>
                                <tr>
                                    <td>Brand 1</td>
                                    <td>100002</td>
                                    <td>$ 3.00</td>
                                    <td>$ 0.50</td>
                                    <td>$ 0.03</td>
                                    <td>$ 1.53</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>95,080.00</td>
                                    <td>$335,632.40</td>
                                </tr>
                                <tr>
                                    <td>Brand 1</td>
                                    <td>100002</td>
                                    <td>$ 3.00</td>
                                    <td>$ 0.50</td>
                                    <td>$ 0.03</td>
                                    <td>$ 1.53</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>95,080.00</td>
                                    <td>$335,632.40</td>
                                </tr>
                                <tr>
                                    <td>Brand 1</td>
                                    <td>100002</td>
                                    <td>$ 3.00</td>
                                    <td>$ 0.50</td>
                                    <td>$ 0.03</td>
                                    <td>$ 1.53</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>95,080.00</td>
                                    <td>$335,632.40</td>
                                </tr>
                                <tr>
                                    <td>Brand 1</td>
                                    <td>100002</td>
                                    <td>$ 3.00</td>
                                    <td>$ 0.50</td>
                                    <td>$ 0.03</td>
                                    <td>$ 1.53</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>23,770</td>
                                    <td>95,080.00</td>
                                    <td>$335,632.40</td>
                                </tr>
                                <tr class="total">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>58,270</td>
                                    <td>58,270</td>
                                    <td>58,270</td>
                                    <td>58,270</td>
                                    <td>233,080.00</td>
                                    <td>$1,236,772.40</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>