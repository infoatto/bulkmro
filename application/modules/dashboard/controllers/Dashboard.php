<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Dashboardmodel', 'dashboardmodel', TRUE);
        $this->load->model('common_model/Common_model', 'common', TRUE);
        checklogin();
    }

    function index() {
        $data = array();
        if ($_SESSION["mro_session"]['user_role'] == 'Customer' || $_SESSION["mro_session"]['user_role'] == 'Supplier') {

            $main_table = array("tbl_order as o", array('o.order_id,o.contract_number,o.nick_name'));
            if ($_SESSION["mro_session"]['user_role'] == 'Customer') {
                $condition = " bp.business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
                $join_tables = array(
                    array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id", array()),
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array()),
                );
            } else {
                $condition = " bp.business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
                $join_tables = array(
                    array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array()),
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array()),
                );
            }

            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $orderData = $this->common->MySqlFetchRow($rs, "array");
            if (!empty($orderData)) {
                $data['orderData'] = $orderData;
            }

            $this->load->view('template/head.php');
            $this->load->view('template/navigation.php');
            $this->load->view('dashboard/bp-graph', $data);
            $this->load->view('template/footer.php');
            $this->load->view('template/footer-scripts.php');
        } else {
            $condition = " status = 'Active' ";
            $bpContractData = $this->common->getData("tbl_bp_order", "bp_order_id,contract_number,nick_name", $condition);
            if (!empty($bpContractData)) {
                $data['bpContractData'] = $bpContractData;
            }
            $this->load->view('template/head.php');
            $this->load->view('template/navigation.php');
            $this->load->view('dashboard/graph', $data);
            $this->load->view('template/footer.php');
            $this->load->view('template/footer-scripts.php');
        }
    }

    public function logOut() {
        $this->session->unset_userdata('mro_session');
        redirect(base_url());
    }

    public function getMasterContract() {
        $bp_contract_id = $_GET['bp_contract_id'];
        $str = '';
        $condition = " 1=1 AND (customer_contract_id=" . $bp_contract_id . " OR supplier_contract_id=" . $bp_contract_id . ") ";
        $data = $this->common->getData("tbl_order", "order_id,contract_number,nick_name", $condition);
        $str = '<select name="m_contract" id="m_contract" class="searchInput basic-single w100"  onchange="getGraphSection();" >'
                . '<option value="" disabled selected hidden>MContract NickName-MContract#</option>';
        if ($bp_contract_id > 0) {
            if (!empty($data)) {
                foreach ($data as $value) {
                    $contract = !empty($value['nick_name']) ? $value['nick_name'] . '-' . $value['contract_number'] : $value['contract_number'];
                    $str = $str . '<option value="' . $value['order_id'] . '"  >' . $contract . '</option>';
                }
            }
            $str = $str . '</select>';
        }
        echo json_encode(array('dropdown' => $str));
        exit;
    }

    public function getGraphSection() {

        //code of update document date in container log table
//        $condition = "1=1 AND cfs.custom_field_title = 'Date of Document' AND cfds.current_session_id = '1642074350713' ";
//        $main_table = array("tbl_custom_field_data_submission as cfds", array("cfds.custom_field_structure_value"));
//            $join_tables = array(
//                array("","tbl_custom_field_structure as cfs","cfs.custom_field_structure_id = cfds.custom_field_structure_id", array()),
//                //array("","tbl_document_ls_uploaded_files as dluf","dluf.uploaded_document_number = cfds.uploaded_document_number", array('dluf.container_id')),
//                array("","tbl_document_uploaded_files as dluf","dluf.uploaded_document_number = cfds.uploaded_document_number", array('dluf.container_id')),
//            );
//            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition,'','dluf.container_id');
//            $dateOfDocumentData = $this->common->MySqlFetchRow($rs, "array");
//            //echo $this->db->last_query();exit;
//            if(!empty($dateOfDocumentData)){
//                foreach ($dateOfDocumentData as $value) {
//                    $data_doc_date = array();
//                    $condition = " current_session_id = '1642074350713' AND  container_id=".$value['container_id']." ";
//                    $data_doc_date['document_date'] = date("Y-m-d", strtotime($value['custom_field_structure_value']));
//                    $this->common->updateData("tbl_container_status_log", $data_doc_date, $condition);
//                }
//            }

        $bp_contract_id = $_POST['bp_contract_id'];
        $m_contract_id = !empty($_POST['m_contract_id']) ? $_POST['m_contract_id'] : 0;
        $result = array();

        $condition = " bp_order_id=" . $bp_contract_id . "  ";
        $businessTypeData = $this->common->getData("tbl_bp_order", "business_type", $condition);

        //condition for show data at select of BP contract
        if ($m_contract_id == 0) {
            if ($businessTypeData[0]['business_type'] == 'Customer') {
                $condition = "customer_contract_id = " . $bp_contract_id . " ";
            } else {
                $condition = "supplier_contract_id = " . $bp_contract_id . " ";
            }
            $orderData = $this->common->getData("tbl_order", "order_id", $condition);
            $orderIds = array();
            if (!empty($orderData)) {
                foreach ($orderData as $value) {
                    $orderIds[] = $value['order_id'];
                }
            }
            $m_contract_id = implode(",", $orderIds);
        }

        if ($m_contract_id > 0) {

            //get container status ids based on BP incoterms
            $containerStatusIds = $this->getContainerStatusIds($m_contract_id, $bp_contract_id);

            //get document date wise actual containers
            $orderActualData = $this->dashboardmodel->getActualData($m_contract_id, $containerStatusIds);

            //get month wise delivery schedule
            $orderDeliveryScheduleData = $this->dashboardmodel->getDeliverySchedule($m_contract_id);

            //add previous one month date in Delivery Schedule date
            $previousDateArray = array();
            if(!empty($orderDeliveryScheduleData)){
                $date = date('Y-m-d',strtotime($orderDeliveryScheduleData[0]['week_date']));
                $previousDate = date('Y-m-d',(strtotime ( '-30 day' , strtotime ( $date) ) ));
                $previousDateArray = array(array('total'=>0,'week_date'=>$previousDate));
            }
            $orderDeliveryScheduleData = array_merge($previousDateArray,$orderDeliveryScheduleData);
            //echo '<pre>';print_r($orderDeliveryScheduleData);die;

            $holdMonthYear = array();
            $containerIdsArr = array();
            $delivedContainerIdsArr = array();
            $monthYearContainerIds = array();
            $monthWiseData = array();
            $projectedGrandTotal = 0;
            $actualGrandTotal = 0;

            $GrandTotalProjected = 0;
            $GrandTotalActual = 0;
            foreach ($orderDeliveryScheduleData as $value) {
                $monthYear = date('1-M-y', strtotime($value['week_date']));
                //get actual container ids
                $actualTotal = 0;
                if (!empty($orderActualData)) {
                    foreach ($orderActualData as $actValue) {
                        //get actual quantity of product
                        if (date('m-y', strtotime($value['week_date'])) == date('m-y', strtotime($actValue['document_date']))) {
                            $containerSKUData = $this->dashboardmodel->getContainerSKUData($actValue['container_id']);
                            if (!empty($containerSKUData)) {
                                $actualTotal += $containerSKUData[0]['totalQty'];
                                $actualGrandTotal += $containerSKUData[0]['totalQty'];
                            }
                            //hold actual container ids
                            $containerIdsArr[$actValue['container_id']] = $actValue['container_id'];
                        }
                        $delivedContainerIdsArr[$actValue['container_id']] = 1;
                    }
                }

                $GrandTotalProjected += $value['total'];
                $GrandTotalActual += $actualTotal;
                //hold atual projected qty
                $monthWiseData[strtotime($monthYear)] = array('monthYear' => $monthYear, 'projected' => $GrandTotalProjected, 'actual' => $GrandTotalActual);
                //hold projected actual total
                $projectedGrandTotal += $value['total'];
                $holdMonthYear[] = date('M-y', strtotime($value['week_date']));
            }

            $monthWiseHoldData = array();
            $actualTotal = 0;
            if (!empty($orderActualData)) {
                foreach ($orderActualData as $actValue) {
                    //get actual container ids
                    if (!in_array(date('M-y', strtotime($actValue['document_date'])), $holdMonthYear)) {
                        //get actual quantity of product
                        $containerSKUData = $this->dashboardmodel->getContainerSKUData($actValue['container_id']);
                        if (!empty($containerSKUData)) {
                            $actualTotal = $containerSKUData[0]['totalQty'];
                            $actualGrandTotal += $containerSKUData[0]['totalQty'];
                        }
                        //hold actual container ids
                        $containerIdsArr[$actValue['container_id']] = $actValue['container_id'];
                        $setActualQty = (isset($monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))]) ? $monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))] : 0);
                        $monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))] = $setActualQty + $actualTotal;
                    }
                }
            }

            if (!empty($monthWiseHoldData)) {
                $actualTotal = $GrandTotalActual;
                foreach ($monthWiseHoldData as $key => $value) {
                    $monthYear = date('1-M-y', strtotime($key));
                    $actualTotal += $value;
                    $monthWiseData[strtotime($monthYear)] = array('monthYear' => $monthYear, 'projected' => $GrandTotalProjected, 'actual' => $actualTotal);
                }
            }
            $containerIdsStr = implode(",", $containerIdsArr);

            //Containers Delivered concept
            $contaierDeliveredTotal = 0;
            if (!empty($delivedContainerIdsArr)) {
                $contaierDeliveredCnt = 0;
                foreach ($delivedContainerIdsArr as $key => $value) {
                    $contaierDeliveredCnt++;
                }
                $contaierDeliveredTotal += $contaierDeliveredCnt;
            }

            //Container in Transit concept
            $condition = "c.order_id IN (" . $m_contract_id . ") AND status_log.container_status_id ='4' ";
            $main_table = array("tbl_container as c", array('c.container_id'));
            $join_tables = array(
                array("", "tbl_container_status_log as status_log", "status_log.container_id = c.container_id", array()),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $transitResultData = $this->common->MySqlFetchRow($rs, "array");
            //echo $this->db->last_query();exit;
            //$transitResultData = $this->common->getData("tbl_container", "container_id", $condition);

            $contaierTransitTotal = 0;
            if (!empty($transitResultData)) {
                foreach ($transitResultData as $value) {
                    if (isset($delivedContainerIdsArr[$value['container_id']])) {
                        continue;
                    }
                    $contaierTransitTotal++;
                }
            }

            //get size wise projected qty
            $sizeWiseDeliveryScheduleData = $this->dashboardmodel->getSizwWiseDeliverySchedule($m_contract_id);
            $sizeWiseProjectedQty = array();
            if (!empty($sizeWiseDeliveryScheduleData)) {
                foreach ($sizeWiseDeliveryScheduleData as $sizeValue) {
                    $sizeWiseProjectedQty[$sizeValue['size_name']] = $sizeValue['total'];
                }
            }

            //get size wise actual qty
            $sizeWiseActualQty = array();
            $sizeWiseInprogressQty = array();
            if (!empty($containerIdsStr)) {
                $SizwWiseActualData = $this->dashboardmodel->getSizwWiseActual($containerIdsStr);
                if (!empty($SizwWiseActualData)) {
                    foreach ($SizwWiseActualData as $value) {
                        $sizeWiseActualQty[$value['size_name']] = $value['total'];
                    }
                }
            }

            //get brand wise projected qty
            $brandWiseProjectedData = $this->dashboardmodel->getBrandWiseProjected($m_contract_id);
            $brandWiseProjectedQty = array();
            $brandIdsArr = array();
            if (!empty($brandWiseProjectedData)) {
                foreach ($brandWiseProjectedData as $brandValue) {
                    $brandWiseProjectedQty[$brandValue['brand_id'] . '#' . $brandValue['brand_name']] = $brandValue['total'];
                    $brandIdsArr[] = $brandValue['brand_id'];
                }
            }
            $brandIdsStr = implode(",", $brandIdsArr);

            //get brand wise size projected qty
            $brandWiseSizeProjectedQty = array();
            if (!empty($brandIdsStr)) {
                $brandWiseSizeData = $this->dashboardmodel->getBrandWiseSizeProjected($m_contract_id, $brandIdsStr);
                if (!empty($brandWiseSizeData)) {
                    foreach ($brandWiseSizeData as $sizeValue) {
                        $brandWiseSizeProjectedQty[$sizeValue['brand_id']][$sizeValue['size_id'] . '#' . $sizeValue['size_name']] = $sizeValue['total'];
                    }
                }
            }

            //get brand wise actual qty
            $brandWiseActualQty = array();
            if (!empty($containerIdsStr) && !empty($brandIdsStr)) {
                $brandWiseActualData = $this->dashboardmodel->getBrandWiseActual($containerIdsStr, $brandIdsStr);
                if (!empty($brandWiseActualData)) {
                    foreach ($brandWiseActualData as $value) {
                        $brandWiseActualQty[$value['brand_id']] = $value['total'];
                    }
                }
            }

            //get brand wise size actual qty
            $brandWiseSizeActualQty = array();
            if (!empty($containerIdsStr) && !empty($brandIdsArr)) {
                foreach ($brandIdsArr as $brandId) {
                    $brandWiseSizeActualData = $this->dashboardmodel->getBrandWiseSizeActaul($containerIdsStr, $brandId);
                    if (!empty($brandWiseSizeActualData)) {
                        foreach ($brandWiseSizeActualData as $sizeValue) {
                            $brandWiseSizeActualQty[$brandId][$sizeValue['size_id']] = $sizeValue['total'];
                        }
                    }
                }
            }

            //logistics progress graph
            $containerStatus = array();
            $condition = "container_status_id IN (1,3,5,6,10,11)";
            $containerStatusData = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", $condition, "status_squence", "Asc");
            if (!empty($containerStatusData)) {
                foreach ($containerStatusData as $value) {
                    if($value['container_status_id'] == 1){
                        $containerStatus[$value['container_status_id']] = 'Projected';
                    }else if($value['container_status_id'] == 11){
                        $containerStatus[$value['container_status_id']] = 'Transloading / Customer WH';
                    }else{
                        $containerStatus[$value['container_status_id']] = $value['container_status_name'];
                    }
                }
            }

            //code of projected point in logistics progress graph
            //$logisticsContainerWithOutStatusData = $this->dashboardmodel->getLogisticsCotainerWithOutStatus($m_contract_id);
//        if(!empty($logisticsContainerWithOutStatusData)){
//            $previousValue = $logisticsContainerWithOutStatusData[0]['cnt'];
//            $logisticsContainerStatus['Projected'] = '0#'.$logisticsContainerWithOutStatusData[0]['cnt'];
//        }


            $logisticsContainerStatusIds = array();
            $logisticsContainerStatusData = $this->dashboardmodel->getLogisticsCotainerStatusIds($m_contract_id);
            if (!empty($logisticsContainerStatusData)) {
                foreach ($logisticsContainerStatusData as $value) {
                    $logisticsContainerStatusIds[$value['container_status_id']] = $value['cnt'];
                }
            }

            /* $logisticsContainerStatus = array();
            $previousValue = 0;
            $setCnt = 0;
            if (!empty($containerStatus)) {
                foreach ($containerStatus as $statusId => $statusName) {
                    if (isset($logisticsContainerStatusIds[$statusId])) {
                        if(((int) $previousValue > 0 ) || ((int) ($previousValue + $logisticsContainerStatusIds[$statusId]) > 0)){
                            $setCnt++;
                        }
                        $logisticsContainerStatus[$statusName] = (int) $previousValue . '#' . (int) ($previousValue + $logisticsContainerStatusIds[$statusId]). '#' .$logisticsContainerStatusIds[$statusId];
                        $previousValue += $logisticsContainerStatusIds[$statusId];

                    } else {
                        if((int) $previousValue > 0 ){
                            $setCnt++;
                        }
                        //$logisticsContainerStatus[$statusName] = '0#0';
                        $logisticsContainerStatus[$statusName] = (int) $previousValue . '#' . (int) ($previousValue). '#0';
                    }
                }
            } */

            $logisticsContainerStatus = array();
            $setCnt = 0;
            if (!empty($containerStatus)) {
                foreach ($containerStatus as $statusId => $statusName) {
                    $currentValue = 0;
                    if (isset($logisticsContainerStatusIds[$statusId])) {
                        if (((int) $currentValue > 0) || ((int) ($currentValue + $logisticsContainerStatusIds[$statusId]) > 0)) {
                            $setCnt++;
                        }
                        $logisticsContainerStatus[$statusName] = '0#' . (int) $logisticsContainerStatusIds[$statusId] . '#' . $logisticsContainerStatusIds[$statusId];
                    } else {
                        if ((int) $currentValue > 0) {
                            $setCnt++;
                        }
                        $logisticsContainerStatus[$statusName] = '0#0#0';
                    }
                }
            }

            if($setCnt==0){
                $logisticsContainerStatus = array();
            }

            //Delivery at port graph data
            $orderDeliveryScheduleByWeekData = $this->dashboardmodel->getDeliveryScheduleByWeek($m_contract_id);
            $weekWiseData = array();
            //$upcomingAtPortContainers = 0;
            if (!empty($orderDeliveryScheduleByWeekData)) {
                foreach ($orderDeliveryScheduleByWeekData as $value) {
                    $week_start_date = date('Y-m-d', strtotime($value['week_date']));
                    $week_end_date = date('Y-m-d', strtotime($week_start_date . ' + 6 days'));
                    $contaierCnt = 0;
                    if (!empty($orderActualData)) {
                        foreach ($orderActualData as $val) {
                            //$docDate = date('Y-m-d', strtotime($val['document_date']));
                            $eta = $val['eta'];
                            if (!empty($eta)) {
                                $etaDate = date('Y-m-d', strtotime($eta));
                                //condition of document date between week date
                                if (strtotime($etaDate) >= strtotime($week_start_date) && strtotime($etaDate) <= strtotime($week_end_date)) {
                                    $contaierCnt++;
                                }
                            }
                        }
                    }
                    $week_date = date('d M', strtotime($value['week_date']));
                    $weekWiseData[$week_date] = $contaierCnt;
                    //$upcomingAtPortContainers += $contaierCnt;
                }
            }
            if(!empty($weekWiseData)){
                foreach($weekWiseData as $wkey => $wVal){
                    if($wVal == 0){
                        $weekWiseData = array();
                    }
                }
            }

            //Voyage Completion graph data
            $voyageCompletionLinnerData = $this->dashboardmodel->getVoyageCompletionLinnerData($m_contract_id);
            // echo "<pre>";print_r($voyageCompletionLinnerData);exit;
            $linerIdsArr = array();
            $linerNameArr = array();
            $linerNameArrTwo = array();
            $voyageCompletionData = array();
            if (!empty($voyageCompletionLinnerData)) {
                foreach ($voyageCompletionLinnerData as $value) {
                    $curDate = date('Y-m-d');
                    $etdDate = date('Y-m-d', strtotime($value['etd']));
                    $etaDate = date('Y-m-d', strtotime($value['eta']));
                    if (!empty($value['etd']) && !empty($value['eta']) && $etdDate != '0000-00-00' && $etaDate != '0000-00-00') {
                        //$formulaOne = strtotime($curDate) - strtotime($etdDate);
                        //$formulaTwo = strtotime($etaDate) - strtotime($etdDate);
                        /* $formulaOne = (DaysDiffBetweenTwoDays($curDate, $etdDate) >= 0)?DaysDiffBetweenTwoDays($curDate, $etdDate):0;
                        $formulaTwo = (DaysDiffBetweenTwoDays($etaDate, $etdDate)>=0)?DaysDiffBetweenTwoDays($etaDate, $etdDate):0;
                        if($formulaOne != 0 && $formulaTwo != 0){
                            $formulaValue = (($formulaOne) / ($formulaTwo)) * 100;
                        }else{
                            $formulaValue = 0;
                        } */
                        $formulaValue = DaysDiffBetweenTwoDays($curDate, $etdDate) / DaysDiffBetweenTwoDays($etaDate, $etdDate) * 100;
                        // check if etaDate is greater than curDate return 0
                        $curEtaDiff = DaysDiffBetweenTwoDays($etaDate, $curDate);
                        // if($curEtaDiff > 0){
                        if($etaDate > $curDate){
                            $formulaValue = 0;
                        }else{
                            $formulaValue =  $formulaValue;
                        }
                        if (isset($linerNameArr[$value['vessel_name']])) {
                            $linerNameArr[$value['vessel_name']] = ($formulaValue) + ($linerNameArr[$value['vessel_name']]);
                        } else {
                            $linerNameArr[$value['vessel_name']] = $formulaValue;
                        }
                    }
                }
                // echo "<pre>";print_r($linerNameArr);exit;
                //container count by liner
                $containerCntLinnerData = $this->dashboardmodel->getcontainerCntLinnerData($m_contract_id);
                if (!empty($containerCntLinnerData)) {
                    foreach ($containerCntLinnerData as $value) {
                        $linerNameArrTwo[$value['vessel_name']] = $value['cnt'];
                    }
                }
            }

            //Hold Voyage Completion graph data according to liner
            if (!empty($linerNameArr)) {
                foreach ($linerNameArr as $key => $value) {
                    if (isset($linerNameArrTwo[$key])) {
                        // $dataVal = ($value) / ($linerNameArrTwo[$key]);
                        $dataVal = $value;
                        if ($dataVal > 100) {
                            $dataVal = 100;
                        }
                        $voyageCompletionData[$key] = round($dataVal);
                    }
                }
            }
            arsort($voyageCompletionData);

            //get BP details
            $result['contract'] = '';
            $result['bp_name'] = '';
            $condition = " o.order_id IN (" . $m_contract_id . ") ";
            $main_table = array("tbl_order as o", array());

            if ($businessTypeData[0]['business_type'] == 'Customer') {
                $join_tables1 = array(
                    array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id", array('bpo.contract_number')),
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias')),
                );
            } else {
                $join_tables1 = array(
                    array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array('bpo.contract_number')),
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias')),
                );
            }
            $join_tables2 = array(
                array("", "tbl_order_sku as o_sku", "o_sku.order_id = o.order_id", array()),
                array("", "tbl_sku as sku", "sku.sku_id = o_sku.sku_id", array()),
                array("", "tbl_brand as brand", "brand.brand_id = sku.brand_id", array('brand.brand_name')),
                array("", "tbl_category as category", "category.category_id = sku.category_id", array('category.category_name')),
                array("", "tbl_business_partner as manufacture", "manufacture.business_partner_id = sku.manufacturer_id", array('manufacture.alias as manufacture')),
            );
            $join_tables = array_merge($join_tables1, $join_tables2);
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $bpData = $this->common->MySqlFetchRow($rs, "array");
            //echo $this->db->last_query();exit;
            $brandArr = array();
            $categoryArr = array();
            $manufactureArr = array();
            if (!empty($bpData)) {
                foreach ($bpData as $value) {
                    $result['contract'] = $value['contract_number'];
                    $result['bp_name'] = $value['alias'];
                    $brandArr[$value['brand_name']] = $value['brand_name'];
                    $categoryArr[$value['category_name']] = $value['category_name'];
                    $manufactureArr[$value['manufacture']] = $value['manufacture'];
                }
            }

            $result['brand'] = implode(",", $brandArr);
            $result['category'] = implode(",", $categoryArr);
            $result['manufacture'] = implode(",", $manufactureArr);

            //line and contaract graph data
            ksort($monthWiseData);
            //echo '<pre>';print_r($monthWiseData);die;
            $result['projectedActual'] = $monthWiseData;
            $result['completedTotal'] = $actualGrandTotal;
            $result['inprogressTotal'] = ($projectedGrandTotal - $actualGrandTotal);
            //contract size graph data
            $result['contractSizeActual'] = $sizeWiseActualQty;
            $result['contractSizeProjected'] = $sizeWiseProjectedQty;
            //brand wise graph data
            //print_r($brandWiseProjectedQty);print_r($brandWiseActualQty);print_r($brandWiseSizeProjectedQty);print_r($brandWiseSizeActualQty);die;
            $result['brandLenght'] = sizeof($brandWiseProjectedQty);
            $result['brandWiseProjected'] = $brandWiseProjectedQty;
            $result['brandWiseActual'] = $brandWiseActualQty;
            $result['brandWiseSizeProjected'] = $brandWiseSizeProjectedQty;
            $result['brandWiseSizeActual'] = $brandWiseSizeActualQty;
            //logistics progress graph data
            $result['logisticsContainerStatus'] = $logisticsContainerStatus;
            //delivery at port graph data
            $result['deliveryAtPort'] = $weekWiseData;
            //Voyage Completion graph data
            $result['voyageCompletionData'] = $voyageCompletionData;
            //total sku in contract
            $result['projectedGrandTotal'] = $projectedGrandTotal;
            //total week
            $condition = " order_id IN (" . $m_contract_id . ") ";
            $weekCntData = $this->common->getData("tbl_order", "duration", $condition);
            $duration = 0;
            if (!empty($weekCntData)) {
                foreach ($weekCntData as $value) {
                    $duration += $value['duration'];
                }
            }
            $result['totalWeek'] = $duration;
            //total container in contract
            $totalContainer = $this->dashboardmodel->getTotalContainerInContract($m_contract_id);
            $result['totalContainer'] = !empty($totalContainer[0]['cnt']) ? $totalContainer[0]['cnt'] : 'N/A';
            //contaier delivered
            $result['contaierDelivered'] = $contaierDeliveredTotal;
            //contaier delivered
            $result['contaierTransit'] = $contaierTransitTotal;

            $result['bp_contract_id'] = $_POST['bp_contract_id'];
            $result['m_contract_id'] = !empty($_POST['m_contract_id']) ? $_POST['m_contract_id'] : 0;

            $graphSectionData = $this->load->view('graph-section', $result, true);

            if ($graphSectionData) {
                echo json_encode(array('success' => true, 'msg' => 'record Found', "graphSectionDiv" => $graphSectionData));
                exit;
            } else {
                echo json_encode(array('success' => false, 'msg' => 'record not Found', "graphSectionDiv" => $graphSectionData));
                exit;
            }
        } else {
            echo json_encode(array('success' => true, 'msg' => 'record not Found', "graphSectionDiv" => ''));
            exit;
        }
    }

    public function savePDF() {
        $bp_contract_id = 0;
        $m_contract_id = 0;
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $m_contract_id = $url_prams['m_contract_id'];
            if (isset($url_prams['bp_contract_id'])) {
                $bp_contract_id = $url_prams['bp_contract_id'];
            }
        }

        $condition = " bp_order_id=" . $bp_contract_id . "  ";
        $businessTypeData = $this->common->getData("tbl_bp_order", "business_type", $condition);

        //condition for show data at select of BP contract
        if ($m_contract_id == 0) {
            if ($businessTypeData[0]['business_type'] == 'Customer') {
                $condition = "customer_contract_id = " . $bp_contract_id . " ";
            } else {
                $condition = "supplier_contract_id = " . $bp_contract_id . " ";
            }
            $orderData = $this->common->getData("tbl_order", "order_id", $condition);
            $orderIds = array();
            if (!empty($orderData)) {
                foreach ($orderData as $value) {
                    $orderIds[] = $value['order_id'];
                }
            }
            $m_contract_id = implode(",", $orderIds);
        }

        if ($bp_contract_id > 0) {

            $result = array();
            //get container status ids based on BP incoterms
            $containerStatusIds = $this->getContainerStatusIds($m_contract_id, $bp_contract_id);

            //get document date wise actual containers
            $orderActualData = $this->dashboardmodel->getActualData($m_contract_id, $containerStatusIds);

            //get month wise delivery schedule
            $orderDeliveryScheduleData = $this->dashboardmodel->getDeliverySchedule($m_contract_id);

            //add previous one month date in Delivery Schedule date
            $previousDateArray = array();
            if(!empty($orderDeliveryScheduleData)){
                $date = date('Y-m-d',strtotime($orderDeliveryScheduleData[0]['week_date']));
                $previousDate = date('Y-m-d',(strtotime ( '-30 day' , strtotime ( $date) ) ));
                $previousDateArray = array(array('total'=>0,'week_date'=>$previousDate));
            }
            $orderDeliveryScheduleData = array_merge($previousDateArray,$orderDeliveryScheduleData);
            //echo '<pre>';print_r($orderDeliveryScheduleData);die;

            $holdMonthYear = array();
            $containerIdsArr = array();
            $delivedContainerIdsArr = array();
            $monthYearContainerIds = array();
            $monthWiseData = array();
            $projectedGrandTotal = 0;
            $actualGrandTotal = 0;

            $GrandTotalProjected = 0;
            $GrandTotalActual = 0;
            foreach ($orderDeliveryScheduleData as $value) {
                $monthYear = date('1-M-y', strtotime($value['week_date']));
                //get actual container ids
                $actualTotal = 0;
                if (!empty($orderActualData)) {
                    foreach ($orderActualData as $actValue) {
                        //get actual quantity of product
                        if (date('m-y', strtotime($value['week_date'])) == date('m-y', strtotime($actValue['document_date']))) {
                            $containerSKUData = $this->dashboardmodel->getContainerSKUData($actValue['container_id']);
                            if (!empty($containerSKUData)) {
                                $actualTotal += $containerSKUData[0]['totalQty'];
                                $actualGrandTotal += $containerSKUData[0]['totalQty'];
                            }
                            //hold actual container ids
                            $containerIdsArr[$actValue['container_id']] = $actValue['container_id'];
                        }
                        $delivedContainerIdsArr[$actValue['container_id']] = 1;
                    }
                }

                $GrandTotalProjected += $value['total'];
                $GrandTotalActual += $actualTotal;
                //hold atual projected qty
                $monthWiseData[strtotime($monthYear)] = array('monthYear' => $monthYear, 'projected' => $GrandTotalProjected, 'actual' => $GrandTotalActual);
                //hold projected actual total
                $projectedGrandTotal += $value['total'];

                $holdMonthYear[] = date('M-y', strtotime($value['week_date']));
            }

            $monthWiseHoldData = array();
            $actualTotal = 0;
            if (!empty($orderActualData)) {
                foreach ($orderActualData as $actValue) {
                    //get actual container ids
                    if (!in_array(date('M-y', strtotime($actValue['document_date'])), $holdMonthYear)) {
                        //get actual quantity of product
                        $containerSKUData = $this->dashboardmodel->getContainerSKUData($actValue['container_id']);
                        if (!empty($containerSKUData)) {
                            $actualTotal = $containerSKUData[0]['totalQty'];
                            $actualGrandTotal += $containerSKUData[0]['totalQty'];
                        }
                        //hold actual container ids
                        $containerIdsArr[$actValue['container_id']] = $actValue['container_id'];
                        $setActualQty = (isset($monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))]) ? $monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))] : 0);
                        $monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))] = $setActualQty + $actualTotal;
                    }
                }
            }

            if (!empty($monthWiseHoldData)) {
                $actualTotal = $GrandTotalActual;
                foreach ($monthWiseHoldData as $key => $value) {
                    $monthYear = date('1-M-y', strtotime($key));
                    $actualTotal += $value;
                    $monthWiseData[strtotime($monthYear)] = array('monthYear' => $monthYear, 'projected' => $GrandTotalProjected, 'actual' => $actualTotal);
                }
            }

            $containerIdsStr = implode(",", $containerIdsArr);

            //Containers Delivered concept
            $contaierDeliveredTotal = 0;
            if (!empty($delivedContainerIdsArr)) {
                $contaierDeliveredCnt = 0;
                foreach ($delivedContainerIdsArr as $key => $value) {
                    $contaierDeliveredCnt++;
                }
                $contaierDeliveredTotal += $contaierDeliveredCnt;
            }

            //Container in Transit concept
            $condition = "c.order_id IN (" . $m_contract_id . ") AND status_log.container_status_id ='4' ";
            $main_table = array("tbl_container as c", array('c.container_id'));
            $join_tables = array(
                array("", "tbl_container_status_log as status_log", "status_log.container_id = c.container_id", array()),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $transitResultData = $this->common->MySqlFetchRow($rs, "array");
            //echo $this->db->last_query();exit;
            //$transitResultData = $this->common->getData("tbl_container", "container_id", $condition);

            $contaierTransitTotal = 0;
            if (!empty($transitResultData)) {
                foreach ($transitResultData as $value) {
                    if (isset($delivedContainerIdsArr[$value['container_id']])) {
                        continue;
                    }
                    $contaierTransitTotal++;
                }
            }

            //get size wise projected qty
            $sizeWiseDeliveryScheduleData = $this->dashboardmodel->getSizwWiseDeliverySchedule($m_contract_id);
            $sizeWiseProjectedQty = array();
            if (!empty($sizeWiseDeliveryScheduleData)) {
                foreach ($sizeWiseDeliveryScheduleData as $sizeValue) {
                    $sizeWiseProjectedQty[$sizeValue['size_name']] = $sizeValue['total'];
                }
            }

            //get size wise actual qty
            $sizeWiseActualQty = array();
            $sizeWiseInprogressQty = array();
            if (!empty($containerIdsStr)) {
                $SizwWiseActualData = $this->dashboardmodel->getSizwWiseActual($containerIdsStr);
                if (!empty($SizwWiseActualData)) {
                    foreach ($SizwWiseActualData as $value) {
                        $sizeWiseActualQty[$value['size_name']] = $value['total'];
                    }
                }
            }

            //get brand wise projected qty
            $brandWiseProjectedData = $this->dashboardmodel->getBrandWiseProjected($m_contract_id);
            $brandWiseProjectedQty = array();
            $brandIdsArr = array();
            if (!empty($brandWiseProjectedData)) {
                foreach ($brandWiseProjectedData as $brandValue) {
                    $brandWiseProjectedQty[$brandValue['brand_id'] . '#' . $brandValue['brand_name']] = $brandValue['total'];
                    $brandIdsArr[] = $brandValue['brand_id'];
                }
            }
            $brandIdsStr = implode(",", $brandIdsArr);

            //get brand wise size projected qty
            $brandWiseSizeProjectedQty = array();
            if (!empty($brandIdsStr)) {
                $brandWiseSizeData = $this->dashboardmodel->getBrandWiseSizeProjected($m_contract_id, $brandIdsStr);
                if (!empty($brandWiseSizeData)) {
                    foreach ($brandWiseSizeData as $sizeValue) {
                        $brandWiseSizeProjectedQty[$sizeValue['brand_id']][$sizeValue['size_id'] . '#' . $sizeValue['size_name']] = $sizeValue['total'];
                    }
                }
            }

            //get brand wise actual qty
            $brandWiseActualQty = array();
            if (!empty($containerIdsStr) && !empty($brandIdsStr)) {
                $brandWiseActualData = $this->dashboardmodel->getBrandWiseActual($containerIdsStr, $brandIdsStr);
                if (!empty($brandWiseActualData)) {
                    foreach ($brandWiseActualData as $value) {
                        $brandWiseActualQty[$value['brand_id']] = $value['total'];
                    }
                }
            }

            //get brand wise size actual qty
            $brandWiseSizeActualQty = array();
            if (!empty($containerIdsStr) && !empty($brandIdsArr)) {
                foreach ($brandIdsArr as $brandId) {
                    $brandWiseSizeActualData = $this->dashboardmodel->getBrandWiseSizeActaul($containerIdsStr, $brandId);
                    if (!empty($brandWiseSizeActualData)) {
                        foreach ($brandWiseSizeActualData as $sizeValue) {
                            $brandWiseSizeActualQty[$brandId][$sizeValue['size_id']] = $sizeValue['total'];
                        }
                    }
                }
            }

            //logistics progress graph
            $containerStatus = array();
            $condition = "container_status_id IN (1,3,5,6,10,11)";
            $containerStatusData = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", $condition, "status_squence", "Asc");
            if (!empty($containerStatusData)) {
                foreach ($containerStatusData as $value) {
                    if($value['container_status_name'] == 'New Container'){
                        $containerStatus[$value['container_status_id']] = 'Projected';
                    }else{
                        $containerStatus[$value['container_status_id']] = $value['container_status_name'];
                    }
                }
            }

            //code of projected point in logistics progress graph
            $logisticsContainerStatusIds = array();
            $logisticsContainerStatusData = $this->dashboardmodel->getLogisticsCotainerStatusIds($m_contract_id);
            if (!empty($logisticsContainerStatusData)) {
                foreach ($logisticsContainerStatusData as $value) {
                    $logisticsContainerStatusIds[$value['container_status_id']] = $value['cnt'];
                }
            }

            $logisticsContainerStatus = array();
            $previousValue = 0;
            $setCnt = 0;
            if (!empty($containerStatus)) {
                foreach ($containerStatus as $statusId => $statusName) {
                    if (isset($logisticsContainerStatusIds[$statusId])) {
                        if(((int) $previousValue > 0 ) || ((int) ($previousValue + $logisticsContainerStatusIds[$statusId]) > 0)){
                            $setCnt++;
                        }
                        $logisticsContainerStatus[$statusName] = (int) $previousValue . '#' . (int) ($previousValue + $logisticsContainerStatusIds[$statusId]). '#' .$logisticsContainerStatusIds[$statusId];
                        $previousValue += $logisticsContainerStatusIds[$statusId];

                    } else {
                        if((int) $previousValue > 0 ){
                            $setCnt++;
                        }
                        //$logisticsContainerStatus[$statusName] = '0#0';
                        $logisticsContainerStatus[$statusName] = (int) $previousValue . '#' . (int) ($previousValue). '#0';
                    }
                }
            }

            if($setCnt==0){
                $logisticsContainerStatus = array();
            }

            //Delivery at port graph data
            $orderDeliveryScheduleByWeekData = $this->dashboardmodel->getDeliveryScheduleByWeek($m_contract_id);
            $weekWiseData = array();
            //$upcomingAtPortContainers = 0;
            if (!empty($orderDeliveryScheduleByWeekData)) {
                foreach ($orderDeliveryScheduleByWeekData as $value) {
                    $week_start_date = date('Y-m-d', strtotime($value['week_date']));
                    $week_end_date = date('Y-m-d', strtotime($week_start_date . ' + 6 days'));
                    $contaierCnt = 0;
                    if (!empty($orderActualData)) {
                        foreach ($orderActualData as $val) {
                            //$docDate = date('Y-m-d', strtotime($val['document_date']));
                            $eta = $val['eta'];
                            if (!empty($eta)) {
                                $etaDate = date('Y-m-d', strtotime($eta));
                                //condition of document date between week date
                                if (strtotime($etaDate) >= strtotime($week_start_date) && strtotime($etaDate) <= strtotime($week_end_date)) {
                                    $contaierCnt++;
                                }
                            }
                        }
                    }
                    $week_date = date('d M', strtotime($value['week_date']));
                    $weekWiseData['Week of ' . $week_date] = $contaierCnt;
                    //$upcomingAtPortContainers += $contaierCnt;
                }
            }

            //Voyage Completion graph data
            $voyageCompletionLinnerData = $this->dashboardmodel->getVoyageCompletionLinnerData($m_contract_id);
            $linerIdsArr = array();
            $linerNameArr = array();
            $linerNameArrTwo = array();
            $voyageCompletionData = array();
            if (!empty($voyageCompletionLinnerData)) {
                foreach ($voyageCompletionLinnerData as $value) {
                    $curDate = date('Y-m-d');
                    $etdDate = date('Y-m-d', strtotime($value['etd']));
                    $etaDate = date('Y-m-d', strtotime($value['eta']));
                    if (!empty($value['etd']) && !empty($value['eta']) && $etdDate != '0000-00-00' && $etaDate != '0000-00-00') {
                        //$formulaOne = strtotime($curDate) - strtotime($etdDate);
                        //$formulaTwo = strtotime($etaDate) - strtotime($etdDate);
                        $formulaOne = DaysDiffBetweenTwoDays($curDate, $etdDate);
                        $formulaTwo = DaysDiffBetweenTwoDays($etaDate, $etdDate);
                        $formulaValue = (($formulaOne) / ($formulaTwo)) * 100;
                        if (isset($linerNameArr[$value['liner_name']])) {
                            $linerNameArr[$value['liner_name']] = ($formulaValue) + ($linerNameArr[$value['liner_name']]);
                        } else {
                            $linerNameArr[$value['liner_name']] = $formulaValue;
                        }
                    }
                }
                //container count by liner
                $containerCntLinnerData = $this->dashboardmodel->getcontainerCntLinnerData($m_contract_id);
                if (!empty($containerCntLinnerData)) {
                    foreach ($containerCntLinnerData as $value) {
                        $linerNameArrTwo[$value['liner_name']] = $value['cnt'];
                    }
                }
            }

            //Hold Voyage Completion graph data according to liner
            if (!empty($linerNameArr)) {
                foreach ($linerNameArr as $key => $value) {
                    if (isset($linerNameArrTwo[$key])) {
                        $dataVal = ($value) / ($linerNameArrTwo[$key]);
                        if ($dataVal > 100) {
                            $dataVal = 100;
                        }
                        $voyageCompletionData[$key] = round($dataVal);
                    }
                }
            }
            arsort($voyageCompletionData);

            //get BP details
            $result['contract'] = '';
            $result['bp_name'] = '';
            $condition = " o.order_id IN (" . $m_contract_id . ") ";
            $main_table = array("tbl_order as o", array('o.contract_number as m_contract,o.nick_name as m_nick_name'));

            $condition = " bp_order_id=" . $bp_contract_id . "  ";
            $businessTypeData = $this->common->getData("tbl_bp_order", "business_type", $condition);

            if ($businessTypeData[0]['business_type'] == 'Customer') {
                $join_tables1 = array(
                    array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id", array('bpo.contract_number,bpo.nick_name as b_nick_name')),
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias')),
                );
            } else {
                $join_tables1 = array(
                    array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array('bpo.contract_number,bpo.nick_name as b_nick_name')),
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias')),
                );
            }
            $join_tables2 = array(
                array("", "tbl_order_sku as o_sku", "o_sku.order_id = o.order_id", array()),
                array("", "tbl_sku as sku", "sku.sku_id = o_sku.sku_id", array()),
                array("", "tbl_brand as brand", "brand.brand_id = sku.brand_id", array('brand.brand_name')),
                array("", "tbl_category as category", "category.category_id = sku.category_id", array('category.category_name')),
                array("", "tbl_business_partner as manufacture", "manufacture.business_partner_id = sku.manufacturer_id", array('manufacture.alias as manufacture')),
            );
            $join_tables = array_merge($join_tables1, $join_tables2);
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $bpData = $this->common->MySqlFetchRow($rs, "array");
            //echo $this->db->last_query();exit;
            $mContractArr = array();
            $brandArr = array();
            $categoryArr = array();
            $manufactureArr = array();
            if (!empty($bpData)) {
                foreach ($bpData as $value) {
                    $mContractName = !empty($value['m_nick_name']) ? $value['m_nick_name'] . '-' . $value['m_contract'] : $value['m_contract'];
                    $mContractArr[$mContractName] = $mContractName;
                    $result['m_contract'] = !empty($value['m_nick_name']) ? $value['m_nick_name'] . '-' . $value['m_contract'] : $value['m_contract'];
                    $result['b_contract'] = !empty($value['b_nick_name']) ? $value['b_nick_name'] . '-' . $value['contract_number'] : $value['contract_number'];
                    $result['contract'] = $value['contract_number'];
                    $result['bp_name'] = $value['alias'];
                    $brandArr[$value['brand_name']] = $value['brand_name'];
                    $categoryArr[$value['category_name']] = $value['category_name'];
                    $manufactureArr[$value['manufacture']] = $value['manufacture'];
                }
            }

            $result['m_contract'] = implode(",", $mContractArr);
            $result['brand'] = implode(",", $brandArr);
            $result['category'] = implode(",", $categoryArr);
            $result['manufacture'] = implode(",", $manufactureArr);

            //line and contaract graph data
            ksort($monthWiseData);
            $result['projectedActual'] = $monthWiseData;
            $result['completedTotal'] = $actualGrandTotal;
            $result['inprogressTotal'] = ($projectedGrandTotal - $actualGrandTotal);
            //contract size graph data
            $result['contractSizeActual'] = $sizeWiseActualQty;
            $result['contractSizeProjected'] = $sizeWiseProjectedQty;
            //brand wise graph data
            $result['brandLenght'] = sizeof($brandWiseProjectedQty);
            $result['brandWiseProjected'] = $brandWiseProjectedQty;
            $result['brandWiseActual'] = $brandWiseActualQty;
            $result['brandWiseSizeProjected'] = $brandWiseSizeProjectedQty;
            $result['brandWiseSizeActual'] = $brandWiseSizeActualQty;
            //logistics progress graph data
            $result['logisticsContainerStatus'] = $logisticsContainerStatus;
            //delivery at port graph data
            $result['deliveryAtPort'] = $weekWiseData;
            //Voyage Completion graph data
            $result['voyageCompletionData'] = $voyageCompletionData;
            //total sku in contract
            $result['projectedGrandTotal'] = $projectedGrandTotal;
            //total week
            $condition = " order_id IN (" . $m_contract_id . ") ";
            $weekCntData = $this->common->getData("tbl_order", "duration", $condition);
            $duration = 0;
            if (!empty($weekCntData)) {
                foreach ($weekCntData as $value) {
                    $duration += $value['duration'];
                }
            }
            $result['totalWeek'] = $duration;
            //total container in contract
            $totalContainer = $this->dashboardmodel->getTotalContainerInContract($m_contract_id);
            $result['totalContainer'] = !empty($totalContainer[0]['cnt']) ? $totalContainer[0]['cnt'] : 'N/A';
            //contaier delivered
            $result['contaierDelivered'] = $contaierDeliveredTotal;
            //contaier delivered
            $result['contaierTransit'] = $contaierTransitTotal;

            $head = $this->load->view('template/head.php', '', true);

            $graphSectionData = $this->load->view('test_pdf', $result, true);
            $resultData = $head . $graphSectionData;
            //echo $resultData;
            //echo '<script>window.print();</script>';
            //exit;

             //echo phpinfo(); die;
            //load mPDF library
            $this->load->library('M_pdf');

            $html = $this->load->view('test_pdf',$result, true);

            $this->m_pdf->pdf->WriteHTML($html);
            $filename = 'test.pdf';
            $this->m_pdf->pdf->Output();


//            $pdfFilePath ="webpreparations-".time().".pdf";
            //actually, you can pass mPDF parameter on this load() function


           //generate the PDF!
            //$stylesheet = '<style>'.file_get_contents('assets/css/bootstrap.min.css').'</style>';
           // apply external css

            //offer it to user via browser download! (The PDF won't be saved on your server HDD)
            //$pdf->Output($pdfFilePath, "D");
            exit;
        } else {
            exit;
        }
    }

    public function getGraphSection_bp() {
        $m_contract_id = $_POST['m_contract_id'];
        $result = array();

        //get container status ids based on incoterms
        $containerStatusIds = $this->getContainerStatusIds($m_contract_id);

        $orderActualData = $this->dashboardmodel->getActualData($m_contract_id, $containerStatusIds);

        //get month wise delivery schedule
        $orderDeliveryScheduleData = $this->dashboardmodel->getDeliverySchedule($m_contract_id);

        //add previous one month date in Delivery Schedule date
        $previousDateArray = array();
        if(!empty($orderDeliveryScheduleData)){
            $date = date('Y-m-d',strtotime($orderDeliveryScheduleData[0]['week_date']));
            $previousDate = date('Y-m-d',(strtotime ( '-30 day' , strtotime ( $date) ) ));
            $previousDateArray = array(array('total'=>0,'week_date'=>$previousDate));
        }
        $orderDeliveryScheduleData = array_merge($previousDateArray,$orderDeliveryScheduleData);
        //echo '<pre>';print_r($orderDeliveryScheduleData);die;

        $holdMonthYear = array();
        $containerIdsArr = array();
        $delivedContainerIdsArr = array();
        $monthYearContainerIds = array();
        $monthWiseData = array();
        $projectedGrandTotal = 0;
        $actualGrandTotal = 0;

        $GrandTotalProjected = 0;
        $GrandTotalActual = 0;
        foreach ($orderDeliveryScheduleData as $value) {
            $monthYear = date('1-M-y', strtotime($value['week_date']));
            //get actual container ids
            $actualTotal = 0;
            if (!empty($orderActualData)) {
                foreach ($orderActualData as $actValue) {
                    //get actual quantity of product
                    if (date('m-y', strtotime($value['week_date'])) == date('m-y', strtotime($actValue['document_date']))) {
                        $containerSKUData = $this->dashboardmodel->getContainerSKUData($actValue['container_id']);
                        if (!empty($containerSKUData)) {
                            $actualTotal += $containerSKUData[0]['totalQty'];
                            $actualGrandTotal += $containerSKUData[0]['totalQty'];
                        }
                        //hold actual container ids
                        $containerIdsArr[$actValue['container_id']] = $actValue['container_id'];
                    }
                    $delivedContainerIdsArr[$actValue['container_id']] = 1;
                }
            }

            $GrandTotalProjected += $value['total'];
            $GrandTotalActual += $actualTotal;
            //hold atual projected qty
            $monthWiseData[strtotime($monthYear)] = array('monthYear' => $monthYear, 'projected' => $GrandTotalProjected, 'actual' => $GrandTotalActual);
            //hold projected actual total
            $projectedGrandTotal += $value['total'];

            $holdMonthYear[] = date('M-y', strtotime($value['week_date']));
        }

        $monthWiseHoldData = array();
        $actualTotal = 0;
        if (!empty($orderActualData)) {
            foreach ($orderActualData as $actValue) {
                //get actual container ids
                if (!in_array(date('M-y', strtotime($actValue['document_date'])), $holdMonthYear)) {
                    //get actual quantity of product
                    $containerSKUData = $this->dashboardmodel->getContainerSKUData($actValue['container_id']);
                    if (!empty($containerSKUData)) {
                        $actualTotal = $containerSKUData[0]['totalQty'];
                        $actualGrandTotal += $containerSKUData[0]['totalQty'];
                    }
                    //hold actual container ids
                    $containerIdsArr[$actValue['container_id']] = $actValue['container_id'];
                    $setActualQty = (isset($monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))]) ? $monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))] : 0);
                    $monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))] = $setActualQty + $actualTotal;
                }
            }
        }

        if (!empty($monthWiseHoldData)) {
            $actualTotal = $GrandTotalActual;
            foreach ($monthWiseHoldData as $key => $value) {
                $monthYear = date('1-M-y', strtotime($key));
                $actualTotal += $value;
                $monthWiseData[strtotime($monthYear)] = array('monthYear' => $monthYear, 'projected' => $GrandTotalProjected, 'actual' => $actualTotal);
            }
        }

        $containerIdsStr = implode(",", $containerIdsArr);

        //Containers Delivered concept
        $contaierDeliveredTotal = 0;
        if (!empty($delivedContainerIdsArr)) {
            $contaierDeliveredCnt = 0;
            foreach ($delivedContainerIdsArr as $key => $value) {
                $contaierDeliveredCnt++;
            }
            $contaierDeliveredTotal += $contaierDeliveredCnt;
        }

        //Container in Transit concept
        $condition = "c.order_id = " . $m_contract_id . " AND status_log.container_status_id ='4' ";
        $main_table = array("tbl_container as c", array('c.container_id'));
        $join_tables = array(
            array("", "tbl_container_status_log as status_log", "status_log.container_id = c.container_id", array()),
        );
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
        $transitResultData = $this->common->MySqlFetchRow($rs, "array");
        //echo $this->db->last_query();exit;
        //$transitResultData = $this->common->getData("tbl_container", "container_id", $condition);

        $contaierTransitTotal = 0;
        if (!empty($transitResultData)) {
            foreach ($transitResultData as $value) {
                if (isset($delivedContainerIdsArr[$value['container_id']])) {
                    continue;
                }
                $contaierTransitTotal++;
            }
        }

        //get size wise projected qty
        $sizeWiseDeliveryScheduleData = $this->dashboardmodel->getSizwWiseDeliverySchedule($m_contract_id);
        $sizeWiseProjectedQty = array();
        if (!empty($sizeWiseDeliveryScheduleData)) {
            foreach ($sizeWiseDeliveryScheduleData as $sizeValue) {
                $sizeWiseProjectedQty[$sizeValue['size_name']] = $sizeValue['total'];
            }
        }

        //get size wise actual qty
        $sizeWiseActualQty = array();
        $sizeWiseInprogressQty = array();
        if (!empty($containerIdsStr)) {
            $SizwWiseActualData = $this->dashboardmodel->getSizwWiseActual($containerIdsStr);
            if (!empty($SizwWiseActualData)) {
                foreach ($SizwWiseActualData as $value) {
                    $sizeWiseActualQty[$value['size_name']] = $value['total'];
                }
            }
        }

        //get brand wise projected qty
        //get brand wise projected qty
        $brandWiseProjectedData = $this->dashboardmodel->getBrandWiseProjected($m_contract_id);
        $brandWiseProjectedQty = array();
        $brandIdsArr = array();
        if (!empty($brandWiseProjectedData)) {
            foreach ($brandWiseProjectedData as $brandValue) {
                $brandWiseProjectedQty[$brandValue['brand_id'] . '#' . $brandValue['brand_name']] = $brandValue['total'];
                $brandIdsArr[] = $brandValue['brand_id'];
            }
        }
        $brandIdsStr = implode(",", $brandIdsArr);
        //get brand wise size projected qty
        $brandWiseSizeProjectedQty = array();
        if (!empty($brandIdsStr)) {
            $brandWiseSizeData = $this->dashboardmodel->getBrandWiseSizeProjected($m_contract_id, $brandIdsStr);
            if (!empty($brandWiseSizeData)) {
                foreach ($brandWiseSizeData as $sizeValue) {
                    $brandWiseSizeProjectedQty[$sizeValue['brand_id']][$sizeValue['size_id'] . '#' . $sizeValue['size_name']] = $sizeValue['total'];
                }
            }
        }

        //get brand wise actual qty
        $brandWiseActualQty = array();
        if (!empty($containerIdsStr) && !empty($brandIdsStr)) {
            $brandWiseActualData = $this->dashboardmodel->getBrandWiseActual($containerIdsStr, $brandIdsStr);
            if (!empty($brandWiseActualData)) {
                foreach ($brandWiseActualData as $value) {
                    $brandWiseActualQty[$value['brand_id']] = $value['total'];
                }
            }
        }

        //get brand wise size actual qty
        $brandWiseSizeActualQty = array();
        if (!empty($containerIdsStr) && !empty($brandIdsArr)) {
            foreach ($brandIdsArr as $brandId) {
                $brandWiseSizeActualData = $this->dashboardmodel->getBrandWiseSizeActaul($containerIdsStr, $brandId);
                if (!empty($brandWiseSizeActualData)) {
                    foreach ($brandWiseSizeActualData as $sizeValue) {
                        $brandWiseSizeActualQty[$brandId][$sizeValue['size_id']] = $sizeValue['total'];
                    }
                }
            }
        }

        //logistics progress graph
        $containerStatus = array();
        $condition = "container_status_id IN (1,3,5,6,10,11)";
        $containerStatusData = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", $condition, "status_squence", "Asc");
        if (!empty($containerStatusData)) {
            foreach ($containerStatusData as $value) {
                if($value['container_status_name'] == 'New Container'){
                    $containerStatus[$value['container_status_id']] = 'Projected';
                }else{
                    $containerStatus[$value['container_status_id']] = $value['container_status_name'];
                }
            }
        }

        $logisticsContainerStatusIds = array();
        $logisticsContainerStatusData = $this->dashboardmodel->getLogisticsCotainerStatusIds($m_contract_id);
        if (!empty($logisticsContainerStatusData)) {
            foreach ($logisticsContainerStatusData as $value) {
                $logisticsContainerStatusIds[$value['container_status_id']] = $value['cnt'];
            }
        }

        $logisticsContainerStatus = array();
        $previousValue = 0;
        $setCnt = 0;
        if (!empty($containerStatus)) {
            foreach ($containerStatus as $statusId => $statusName) {
                if (isset($logisticsContainerStatusIds[$statusId])) {
                    if(((int) $previousValue > 0 ) || ((int) ($previousValue + $logisticsContainerStatusIds[$statusId]) > 0)){
                        $setCnt++;
                    }
                    $logisticsContainerStatus[$statusName] = (int) $previousValue . '#' . (int) ($previousValue + $logisticsContainerStatusIds[$statusId]). '#' .$logisticsContainerStatusIds[$statusId];
                    $previousValue += $logisticsContainerStatusIds[$statusId];

                } else {
                    if((int) $previousValue > 0 ){
                        $setCnt++;
                    }
                    //$logisticsContainerStatus[$statusName] = '0#0';
                    $logisticsContainerStatus[$statusName] = (int) $previousValue . '#' . (int) ($previousValue). '#0';
                }
            }
        }

        if($setCnt==0){
            $logisticsContainerStatus = array();
        }

        //Delivery at port graph data
        $orderDeliveryScheduleByWeekData = $this->dashboardmodel->getDeliveryScheduleByWeek($m_contract_id);
        $weekWiseData = array();
        $upcomingAtPortContainers = 0;
        if (!empty($orderDeliveryScheduleByWeekData)) {
            foreach ($orderDeliveryScheduleByWeekData as $value) {
                $week_start_date = date('Y-m-d', strtotime($value['week_date']));
                $week_end_date = date('Y-m-d', strtotime($week_start_date . ' + 6 days'));
                $contaierCnt = 0;
                if (!empty($orderActualData)) {
                    foreach ($orderActualData as $val) {
//                        $docDate = date('Y-m-d', strtotime($val['document_date']));
//                        if (strtotime($docDate) >= strtotime($week_start_date) && strtotime($docDate) <= strtotime($week_end_date)) {
//                            $contaierCnt++;
//                        }

                        $eta = $val['eta'];
                        if (!empty($eta)) {
                            $etaDate = date('Y-m-d', strtotime($eta));
                            //condition of document date between week date
                            if (strtotime($etaDate) >= strtotime($week_start_date) && strtotime($etaDate) <= strtotime($week_end_date)) {
                                $contaierCnt++;
                            }
                        }
                    }
                }
                $week_date = date('d M', strtotime($value['week_date']));
                $weekWiseData['Week of ' . $week_date] = $contaierCnt;
                $upcomingAtPortContainers += $contaierCnt;
            }
        }


        //Voyage Completion graph data
        $voyageCompletionLinnerData = $this->dashboardmodel->getVoyageCompletionLinnerData($m_contract_id);
        $linerIdsArr = array();
        $linerNameArr = array();
        $linerNameArrTwo = array();
        $voyageCompletionData = array();
        if (!empty($voyageCompletionLinnerData)) {
            foreach ($voyageCompletionLinnerData as $value) {
                $curDate = date('Y-m-d');
                $etdDate = date('Y-m-d', strtotime($value['etd']));
                $etaDate = date('Y-m-d', strtotime($value['eta']));
                if (!empty($value['etd']) && !empty($value['eta']) && $etdDate != '0000-00-00' && $etaDate != '0000-00-00') {
                    //$formulaOne = strtotime($curDate) - strtotime($etdDate);
                    //$formulaTwo = strtotime($etaDate) - strtotime($etdDate);
                    $formulaOne = DaysDiffBetweenTwoDays($curDate, $etdDate);
                    $formulaTwo = DaysDiffBetweenTwoDays($etaDate, $etdDate);
                    $formulaValue = (($formulaOne) / ($formulaTwo)) * 100;
                    if (isset($linerNameArr[$value['vessel_name']])) {
                        $linerNameArr[$value['vessel_name']] = ($formulaValue) + ($linerNameArr[$value['vessel_name']]);
                    } else {
                        $linerNameArr[$value['vessel_name']] = $formulaValue;
                    }
                }
            }
            //container count by liner
            $containerCntLinnerData = $this->dashboardmodel->getcontainerCntLinnerData($m_contract_id);
            if (!empty($containerCntLinnerData)) {
                foreach ($containerCntLinnerData as $value) {
                    $linerNameArrTwo[$value['vessel_name']] = $value['cnt'];
                }
            }
        }

        //Hold Voyage Completion graph data according to liner
        if (!empty($linerNameArr)) {
            foreach ($linerNameArr as $key => $value) {
                if (isset($linerNameArrTwo[$key])) {
                    $dataVal = ($value) / ($linerNameArrTwo[$key]);
                    if ($dataVal > 100) {
                        $dataVal = 100;
                    }
                    $voyageCompletionData[$key] = round($dataVal);
                }
            }
        }
        arsort($voyageCompletionData);

        //get BP details
        $result['contract'] = '';
        $result['bp_name'] = '';
        $condition = " o.order_id =" . $m_contract_id . " ";
        $main_table = array("tbl_order as o", array());
        if ($_SESSION["mro_session"]['user_role'] == 'Customer') {
            $join_tables1 = array(
                array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id", array('bpo.contract_number')),
                array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias')),
            );
        } else {
            $join_tables1 = array(
                array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array('bpo.contract_number')),
                array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias')),
            );
        }
        $join_tables2 = array(
            array("", "tbl_order_sku as o_sku", "o_sku.order_id = o.order_id", array()),
            array("", "tbl_sku as sku", "sku.sku_id = o_sku.sku_id", array()),
            array("", "tbl_brand as brand", "brand.brand_id = sku.brand_id", array('brand.brand_name')),
            array("", "tbl_category as category", "category.category_id = sku.category_id", array('category.category_name')),
            array("", "tbl_business_partner as manufacture", "manufacture.business_partner_id = sku.manufacturer_id", array('manufacture.alias as manufacture')),
        );
        $join_tables = array_merge($join_tables1, $join_tables2);
        $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
        $bpData = $this->common->MySqlFetchRow($rs, "array");
        //echo $this->db->last_query();exit;
        $brandArr = array();
        $categoryArr = array();
        $manufactureArr = array();
        if (!empty($bpData)) {
            foreach ($bpData as $value) {
                $result['contract'] = $value['contract_number'];
                $result['bp_name'] = $value['alias'];
                $brandArr[$value['brand_name']] = $value['brand_name'];
                $categoryArr[$value['category_name']] = $value['category_name'];
                $manufactureArr[$value['manufacture']] = $value['manufacture'];
            }
        }

        $result['brand'] = implode(",", $brandArr);
        $result['category'] = implode(",", $categoryArr);
        $result['manufacture'] = implode(",", $manufactureArr);
        //echo '<pre>'; print_r($result);die;
        //line and contaract graph data
        ksort($monthWiseData);
        $result['projectedActual'] = $monthWiseData;
        $result['completedTotal'] = $actualGrandTotal;
        $result['inprogressTotal'] = ($projectedGrandTotal - $actualGrandTotal);
        //contract size graph data
        $result['contractSizeActual'] = $sizeWiseActualQty;
        $result['contractSizeProjected'] = $sizeWiseProjectedQty;
        //brand wise graph data
        $result['brandLenght'] = sizeof($brandWiseProjectedQty);
        $result['brandWiseProjected'] = $brandWiseProjectedQty;
        $result['brandWiseActual'] = $brandWiseActualQty;
        $result['brandWiseSizeProjected'] = $brandWiseSizeProjectedQty;
        $result['brandWiseSizeActual'] = $brandWiseSizeActualQty;
        //logistics progress graph data
        $result['logisticsContainerStatus'] = $logisticsContainerStatus;
        //delivery at port graph data
        $result['deliveryAtPort'] = $weekWiseData;
        //Voyage Completion graph data
        $result['voyageCompletionData'] = $voyageCompletionData;
        //total sku in contract
        $result['projectedGrandTotal'] = $projectedGrandTotal;
        //total week
        $condition = " order_id=" . $m_contract_id . " ";
        $weekCntData = $this->common->getData("tbl_order", "duration", $condition);
        $result['totalWeek'] = !empty($weekCntData) ? $weekCntData[0]['duration'] : '0';
        //total container in contract
        $totalContainer = $this->dashboardmodel->getTotalContainerInContract($m_contract_id);
        $result['totalContainer'] = !empty($totalContainer[0]['cnt']) ? $totalContainer[0]['cnt'] : 'N/A';

        //contaier delivered
        $result['contaierDelivered'] = $contaierDeliveredTotal;
        //contaier delivered
        $result['contaierTransit'] = $contaierTransitTotal;

        $result['m_contract_id'] = $_POST['m_contract_id'];

        //echo '<pre>'; print_r($upcomingAtPortContainers);die;
        $graphSectionData = $this->load->view('bp-graph-section', $result, true);
        if ($graphSectionData) {
            echo json_encode(array('success' => true, 'msg' => 'record Found', "graphSectionDiv" => $graphSectionData));
            exit;
        } else {
            echo json_encode(array('success' => false, 'msg' => 'record not Found', "graphSectionDiv" => $graphSectionData));
            exit;
        }
    }

    public function savePDFBP() {
        $m_contract_id = 0;
        if (!empty($_GET['text']) && isset($_GET['text'])) {
            $varr = base64_decode(strtr($_GET['text'], '-_', '+/'));
            parse_str($varr, $url_prams);
            $m_contract_id = $url_prams['m_contract_id'];
        }
        if ($m_contract_id > 0) {

            $result = array();
            //get container status ids based on incoterms
            $containerStatusIds = $this->getContainerStatusIds($m_contract_id);

            $orderActualData = $this->dashboardmodel->getActualData($m_contract_id, $containerStatusIds);

            //get month wise delivery schedule
            $orderDeliveryScheduleData = $this->dashboardmodel->getDeliverySchedule($m_contract_id);

           //add previous one month date in Delivery Schedule date
            $previousDateArray = array();
            if(!empty($orderDeliveryScheduleData)){
                $date = date('Y-m-d',strtotime($orderDeliveryScheduleData[0]['week_date']));
                $previousDate = date('Y-m-d',(strtotime ( '-30 day' , strtotime ( $date) ) ));
                $previousDateArray = array(array('total'=>0,'week_date'=>$previousDate));
            }
            $orderDeliveryScheduleData = array_merge($previousDateArray,$orderDeliveryScheduleData);
            //echo '<pre>';print_r($orderDeliveryScheduleData);die;

            $holdMonthYear = array();
            $containerIdsArr = array();
            $delivedContainerIdsArr = array();
            $monthYearContainerIds = array();
            $monthWiseData = array();
            $projectedGrandTotal = 0;
            $actualGrandTotal = 0;

            $GrandTotalProjected = 0;
            $GrandTotalActual = 0;
            foreach ($orderDeliveryScheduleData as $value) {
                $monthYear = date('1-M-y', strtotime($value['week_date']));
                //get actual container ids
                $actualTotal = 0;
                if (!empty($orderActualData)) {
                    foreach ($orderActualData as $actValue) {
                        //get actual quantity of product
                        if (date('m-y', strtotime($value['week_date'])) == date('m-y', strtotime($actValue['document_date']))) {
                            $containerSKUData = $this->dashboardmodel->getContainerSKUData($actValue['container_id']);
                            if (!empty($containerSKUData)) {
                                $actualTotal += $containerSKUData[0]['totalQty'];
                                $actualGrandTotal += $containerSKUData[0]['totalQty'];
                            }
                            //hold actual container ids
                            $containerIdsArr[$actValue['container_id']] = $actValue['container_id'];
                        }
                        $delivedContainerIdsArr[$actValue['container_id']] = 1;
                    }
                }

                $GrandTotalProjected += $value['total'];
                $GrandTotalActual += $actualTotal;
                //hold atual projected qty
                $monthWiseData[strtotime($monthYear)] = array('monthYear' => $monthYear, 'projected' => $GrandTotalProjected, 'actual' => $GrandTotalActual);
                //hold projected actual total
                $projectedGrandTotal += $value['total'];

                $holdMonthYear[] = date('M-y', strtotime($value['week_date']));
            }

            $monthWiseHoldData = array();
            $actualTotal = 0;
            if (!empty($orderActualData)) {
                foreach ($orderActualData as $actValue) {
                    //get actual container ids
                    if (!in_array(date('M-y', strtotime($actValue['document_date'])), $holdMonthYear)) {
                        //get actual quantity of product
                        $containerSKUData = $this->dashboardmodel->getContainerSKUData($actValue['container_id']);
                        if (!empty($containerSKUData)) {
                            $actualTotal = $containerSKUData[0]['totalQty'];
                            $actualGrandTotal += $containerSKUData[0]['totalQty'];
                        }
                        //hold actual container ids
                        $containerIdsArr[$actValue['container_id']] = $actValue['container_id'];
                        $setActualQty = (isset($monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))]) ? $monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))] : 0);
                        $monthWiseHoldData[date('M-y', strtotime($actValue['document_date']))] = $setActualQty + $actualTotal;
                    }
                }
            }

            if (!empty($monthWiseHoldData)) {
                $actualTotal = $GrandTotalActual;
                foreach ($monthWiseHoldData as $key => $value) {
                    $monthYear = date('1-M-y', strtotime($key));
                    $actualTotal += $value;
                    $monthWiseData[strtotime($monthYear)] = array('monthYear' => $monthYear, 'projected' => $GrandTotalProjected, 'actual' => $actualTotal);
                }
            }

            $containerIdsStr = implode(",", $containerIdsArr);

            //Containers Delivered concept
            $contaierDeliveredTotal = 0;
            if (!empty($delivedContainerIdsArr)) {
                $contaierDeliveredCnt = 0;
                foreach ($delivedContainerIdsArr as $key => $value) {
                    $contaierDeliveredCnt++;
                }
                $contaierDeliveredTotal += $contaierDeliveredCnt;
            }

            //Container in Transit concept
            $condition = "c.order_id = " . $m_contract_id . " AND status_log.container_status_id ='4' ";
            $main_table = array("tbl_container as c", array('c.container_id'));
            $join_tables = array(
                array("", "tbl_container_status_log as status_log", "status_log.container_id = c.container_id", array()),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $transitResultData = $this->common->MySqlFetchRow($rs, "array");
            //echo $this->db->last_query();exit;
            //$transitResultData = $this->common->getData("tbl_container", "container_id", $condition);

            $contaierTransitTotal = 0;
            if (!empty($transitResultData)) {
                foreach ($transitResultData as $value) {
                    if (isset($delivedContainerIdsArr[$value['container_id']])) {
                        continue;
                    }
                    $contaierTransitTotal++;
                }
            }

            //get size wise projected qty
            $sizeWiseDeliveryScheduleData = $this->dashboardmodel->getSizwWiseDeliverySchedule($m_contract_id);
            $sizeWiseProjectedQty = array();
            if (!empty($sizeWiseDeliveryScheduleData)) {
                foreach ($sizeWiseDeliveryScheduleData as $sizeValue) {
                    $sizeWiseProjectedQty[$sizeValue['size_name']] = $sizeValue['total'];
                }
            }

            //get size wise actual qty
            $sizeWiseActualQty = array();
            $sizeWiseInprogressQty = array();
            if (!empty($containerIdsStr)) {
                $SizwWiseActualData = $this->dashboardmodel->getSizwWiseActual($containerIdsStr);
                if (!empty($SizwWiseActualData)) {
                    foreach ($SizwWiseActualData as $value) {
                        $sizeWiseActualQty[$value['size_name']] = $value['total'];
                    }
                }
            }

            //get brand wise projected qty
            //get brand wise projected qty
            $brandWiseProjectedData = $this->dashboardmodel->getBrandWiseProjected($m_contract_id);
            $brandWiseProjectedQty = array();
            $brandIdsArr = array();
            if (!empty($brandWiseProjectedData)) {
                foreach ($brandWiseProjectedData as $brandValue) {
                    $brandWiseProjectedQty[$brandValue['brand_id'] . '#' . $brandValue['brand_name']] = $brandValue['total'];
                    $brandIdsArr[] = $brandValue['brand_id'];
                }
            }
            $brandIdsStr = implode(",", $brandIdsArr);
            //get brand wise size projected qty
            $brandWiseSizeProjectedQty = array();
            if (!empty($brandIdsStr)) {
                $brandWiseSizeData = $this->dashboardmodel->getBrandWiseSizeProjected($m_contract_id, $brandIdsStr);
                if (!empty($brandWiseSizeData)) {
                    foreach ($brandWiseSizeData as $sizeValue) {
                        $brandWiseSizeProjectedQty[$sizeValue['brand_id']][$sizeValue['size_id'] . '#' . $sizeValue['size_name']] = $sizeValue['total'];
                    }
                }
            }

            //get brand wise actual qty
            $brandWiseActualQty = array();
            if (!empty($containerIdsStr) && !empty($brandIdsStr)) {
                $brandWiseActualData = $this->dashboardmodel->getBrandWiseActual($containerIdsStr, $brandIdsStr);
                if (!empty($brandWiseActualData)) {
                    foreach ($brandWiseActualData as $value) {
                        $brandWiseActualQty[$value['brand_id']] = $value['total'];
                    }
                }
            }

            //get brand wise size actual qty
            $brandWiseSizeActualQty = array();
            if (!empty($containerIdsStr) && !empty($brandIdsArr)) {
                foreach ($brandIdsArr as $brandId) {
                    $brandWiseSizeActualData = $this->dashboardmodel->getBrandWiseSizeActaul($containerIdsStr, $brandId);
                    if (!empty($brandWiseSizeActualData)) {
                        foreach ($brandWiseSizeActualData as $sizeValue) {
                            $brandWiseSizeActualQty[$brandId][$sizeValue['size_id']] = $sizeValue['total'];
                        }
                    }
                }
            }


            //logistics progress graph
            $containerStatus = array();
            $condition = "container_status_id IN (1,3,5,6,10,11)";
            $containerStatusData = $this->common->getData("tbl_container_status", "container_status_id,container_status_name", $condition, "status_squence", "Asc");
            if (!empty($containerStatusData)) {
                foreach ($containerStatusData as $value) {
                    if($value['container_status_name'] == 'New Container'){
                        $containerStatus[$value['container_status_id']] = 'Projected';
                    }else{
                        $containerStatus[$value['container_status_id']] = $value['container_status_name'];
                    }
                }
            }

            $logisticsContainerStatusIds = array();
            $logisticsContainerStatusData = $this->dashboardmodel->getLogisticsCotainerStatusIds($m_contract_id);
            if (!empty($logisticsContainerStatusData)) {
                foreach ($logisticsContainerStatusData as $value) {
                    $logisticsContainerStatusIds[$value['container_status_id']] = $value['cnt'];
                }
            }

            $logisticsContainerStatus = array();
            $previousValue = 0;
            $setCnt = 0;
            if (!empty($containerStatus)) {
                foreach ($containerStatus as $statusId => $statusName) {
                    if (isset($logisticsContainerStatusIds[$statusId])) {
                        if(((int) $previousValue > 0 ) || ((int) ($previousValue + $logisticsContainerStatusIds[$statusId]) > 0)){
                            $setCnt++;
                        }
                        $logisticsContainerStatus[$statusName] = (int) $previousValue . '#' . (int) ($previousValue + $logisticsContainerStatusIds[$statusId]). '#' .$logisticsContainerStatusIds[$statusId];
                        $previousValue += $logisticsContainerStatusIds[$statusId];

                    } else {
                        if((int) $previousValue > 0 ){
                            $setCnt++;
                        }
                        //$logisticsContainerStatus[$statusName] = '0#0';
                        $logisticsContainerStatus[$statusName] = (int) $previousValue . '#' . (int) ($previousValue). '#0';
                    }
                }
            }

            if($setCnt==0){
                $logisticsContainerStatus = array();
            }

            //Delivery at port graph data
            $orderDeliveryScheduleByWeekData = $this->dashboardmodel->getDeliveryScheduleByWeek($m_contract_id);
            $weekWiseData = array();
            $upcomingAtPortContainers = 0;
            if (!empty($orderDeliveryScheduleByWeekData)) {
                foreach ($orderDeliveryScheduleByWeekData as $value) {
                    $week_start_date = date('Y-m-d', strtotime($value['week_date']));
                    $week_end_date = date('Y-m-d', strtotime($week_start_date . ' + 6 days'));
                    $contaierCnt = 0;
                    if (!empty($orderActualData)) {
                        foreach ($orderActualData as $val) {
//                            $docDate = date('Y-m-d', strtotime($val['document_date']));
//                            if (strtotime($docDate) >= strtotime($week_start_date) && strtotime($docDate) <= strtotime($week_end_date)) {
//                                $contaierCnt++;
//                            }
                            $eta = $val['eta'];
                            if (!empty($eta)) {
                                $etaDate = date('Y-m-d', strtotime($eta));
                                //condition of document date between week date
                                if (strtotime($etaDate) >= strtotime($week_start_date) && strtotime($etaDate) <= strtotime($week_end_date)) {
                                    $contaierCnt++;
                                }
                            }
                        }
                    }
                    $week_date = date('d M', strtotime($value['week_date']));
                    $weekWiseData['Week of ' . $week_date] = $contaierCnt;
                    $upcomingAtPortContainers += $contaierCnt;
                }
            }


            //Voyage Completion graph data
            $voyageCompletionLinnerData = $this->dashboardmodel->getVoyageCompletionLinnerData($m_contract_id);
            $linerIdsArr = array();
            $linerNameArr = array();
            $linerNameArrTwo = array();
            $voyageCompletionData = array();
            if (!empty($voyageCompletionLinnerData)) {
                foreach ($voyageCompletionLinnerData as $value) {
                    $curDate = date('Y-m-d');
                    $etdDate = date('Y-m-d', strtotime($value['etd']));
                    $etaDate = date('Y-m-d', strtotime($value['eta']));
                    if (!empty($value['etd']) && !empty($value['eta']) && $etdDate != '0000-00-00' && $etaDate != '0000-00-00') {
                        //$formulaOne = strtotime($curDate) - strtotime($etdDate);
                        //$formulaTwo = strtotime($etaDate) - strtotime($etdDate);
                        $formulaOne = DaysDiffBetweenTwoDays($curDate, $etdDate);
                        $formulaTwo = DaysDiffBetweenTwoDays($etaDate, $etdDate);
                        $formulaValue = (($formulaOne) / ($formulaTwo)) * 100;
                        if (isset($linerNameArr[$value['liner_name']])) {
                            $linerNameArr[$value['liner_name']] = ($formulaValue) + ($linerNameArr[$value['liner_name']]);
                        } else {
                            $linerNameArr[$value['liner_name']] = $formulaValue;
                        }
                    }
                }
                //container count by liner
                $containerCntLinnerData = $this->dashboardmodel->getcontainerCntLinnerData($m_contract_id);
                if (!empty($containerCntLinnerData)) {
                    foreach ($containerCntLinnerData as $value) {
                        $linerNameArrTwo[$value['liner_name']] = $value['cnt'];
                    }
                }
            }

            //Hold Voyage Completion graph data according to liner
            if (!empty($linerNameArr)) {
                foreach ($linerNameArr as $key => $value) {
                    if (isset($linerNameArrTwo[$key])) {
                        $dataVal = ($value) / ($linerNameArrTwo[$key]);
                        if ($dataVal > 100) {
                            $dataVal = 100;
                        }
                        $voyageCompletionData[$key] = round($dataVal);
                    }
                }
            }
            arsort($voyageCompletionData);

            //get BP details
            $result['contract'] = '';
            $result['bp_name'] = '';
            $condition = " o.order_id =" . $m_contract_id . " ";
            $main_table = array("tbl_order as o", array('o.contract_number as m_contract,o.nick_name as m_nick_name'));
            if ($_SESSION["mro_session"]['user_role'] == 'Customer') {
                $join_tables1 = array(
                    array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id", array('bpo.contract_number')),
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias')),
                );
            } else {
                $join_tables1 = array(
                    array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array('bpo.contract_number')),
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array('bp.alias')),
                );
            }
            $join_tables2 = array(
                array("", "tbl_order_sku as o_sku", "o_sku.order_id = o.order_id", array()),
                array("", "tbl_sku as sku", "sku.sku_id = o_sku.sku_id", array()),
                array("", "tbl_brand as brand", "brand.brand_id = sku.brand_id", array('brand.brand_name')),
                array("", "tbl_category as category", "category.category_id = sku.category_id", array('category.category_name')),
                array("", "tbl_business_partner as manufacture", "manufacture.business_partner_id = sku.manufacturer_id", array('manufacture.alias as manufacture')),
            );
            $join_tables = array_merge($join_tables1, $join_tables2);
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $bpData = $this->common->MySqlFetchRow($rs, "array");
            //echo $this->db->last_query();exit;
            $brandArr = array();
            $categoryArr = array();
            $manufactureArr = array();
            if (!empty($bpData)) {
                foreach ($bpData as $value) {
                    $result['m_contract'] = !empty($value['m_nick_name']) ? $value['m_nick_name'] . '-' . $value['m_contract'] : $value['m_contract'];
                    $result['contract'] = $value['contract_number'];
                    $result['bp_name'] = $value['alias'];
                    $brandArr[$value['brand_name']] = $value['brand_name'];
                    $categoryArr[$value['category_name']] = $value['category_name'];
                    $manufactureArr[$value['manufacture']] = $value['manufacture'];
                }
            }

            $result['brand'] = implode(",", $brandArr);
            $result['category'] = implode(",", $categoryArr);
            $result['manufacture'] = implode(",", $manufactureArr);

            //echo '<pre>'; print_r($result);die;
            //line and contaract graph data
            ksort($monthWiseData);
            $result['projectedActual'] = $monthWiseData;
            $result['completedTotal'] = $actualGrandTotal;
            $result['inprogressTotal'] = ($projectedGrandTotal - $actualGrandTotal);
            //contract size graph data
            $result['contractSizeActual'] = $sizeWiseActualQty;
            $result['contractSizeProjected'] = $sizeWiseProjectedQty;
            //brand wise graph data
            $result['brandLenght'] = sizeof($brandWiseProjectedQty);
            $result['brandWiseProjected'] = $brandWiseProjectedQty;
            $result['brandWiseActual'] = $brandWiseActualQty;
            $result['brandWiseSizeProjected'] = $brandWiseSizeProjectedQty;
            $result['brandWiseSizeActual'] = $brandWiseSizeActualQty;
            //logistics progress graph data
            $result['logisticsContainerStatus'] = $logisticsContainerStatus;
            //delivery at port graph data
            $result['deliveryAtPort'] = $weekWiseData;
            //Voyage Completion graph data
            $result['voyageCompletionData'] = $voyageCompletionData;
            //total sku in contract
            $result['projectedGrandTotal'] = $projectedGrandTotal;
            //total week
            $condition = " order_id=" . $m_contract_id . " ";
            $weekCntData = $this->common->getData("tbl_order", "duration", $condition);
            $result['totalWeek'] = !empty($weekCntData) ? $weekCntData[0]['duration'] : '0';
            //total container in contract
            $totalContainer = $this->dashboardmodel->getTotalContainerInContract($m_contract_id);
            $result['totalContainer'] = !empty($totalContainer[0]['cnt']) ? $totalContainer[0]['cnt'] : 'N/A';
            //contaier delivered
            $result['contaierDelivered'] = $contaierDeliveredTotal;
            //contaier delivered
            $result['contaierTransit'] = $contaierTransitTotal;

            $head = $this->load->view('template/head.php', '', true);
            $graphSectionData = $this->load->view('save-pdf-bp', $result, true);
            $resultData = $head . $graphSectionData;

            echo $resultData;
            //echo '<script>window.print();</script>';
            exit;
        } else {
            exit;
        }
    }

    public function getContainerStatusIds($m_contract_id = 0, $bp_contract_id = 0) {
        $statusIds = array();
        $condition = " o.order_id = " . $m_contract_id . " ";
        if ($_SESSION["mro_session"]['user_role'] == 'Customer') {
            $main_table = array("tbl_order as o", array());
            $join_tables = array(
                array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id", array("bpo.incoterms")),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $result = $this->common->MySqlFetchRow($rs, "array");
            if (!empty($result)) {
                $customerIncoterms = $result[0]['incoterms'];
                if (!empty($customerIncoterms)) {
                    switch (trim($customerIncoterms)) {
                        case 'EXW':
                            // FCR status id
                            $statusIds[] = 5;
                            break;
                        case 'FOB':
                            // Goods on the Water status id
                            $statusIds[] = 6;
                            break;
                        case 'CIF':
                            // Arrival Notice Received status id
                            $statusIds[] = 13;
                            //Goods Received in Port status id
                            $statusIds[] = 7;
                            break;
                        case 'DDP Port':
                            // DO Issued status id
                            $statusIds[] = 10;
                            break;
                        case 'DDP Customer WH':
                            // FCR status id
                            $statusIds[] = 11;
                            break;
                        default:
                            $statusIds[] = 0;
                            break;
                    }
                }
            }
        } else if ($_SESSION["mro_session"]['user_role'] == 'Supplier') {
            $main_table = array("tbl_order as o", array());
            $join_tables = array(
                array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.supplier_contract_id", array("bpo.incoterms")),
            );
            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
            $result = $this->common->MySqlFetchRow($rs, "array");

            if (!empty($result)) {
                $supplierIncoterms = $result[0]['incoterms'];
                if (!empty($supplierIncoterms)) {
                    switch (trim($supplierIncoterms)) {
                        case 'EXW':
                            // FCR status id
                            $statusIds[] = 5;
                            break;
                        case 'FOB':
                            // Goods on the Water status id
                            $statusIds[] = 6;
                            break;
                        case 'CIF':
                            // Arrival Notice Received status id
                            $statusIds[] = 13;
                            //Goods Received in Port status id
                            $statusIds[] = 7;
                            break;
                        case 'DDP Port':
                            // DO Issued status id
                            $statusIds[] = 10;
                            break;
                        case 'DDP Customer WH':
                            // FCR status id
                            $statusIds[] = 11;
                            break;
                        default:
                            $statusIds[] = 0;
                            break;
                    }
                }
            }
        } else {

            if (!empty($bp_contract_id) && $bp_contract_id > 0) {
                $condition = "bp_order_id = " . $bp_contract_id . " ";
                $incotermsData = $this->common->getData("tbl_bp_order", "incoterms", $condition);
                if (!empty($incotermsData)) {
                    $incoterms = strtoupper(trim($incotermsData[0]['incoterms']));
                    if (!empty($incoterms)) {
                        switch ($incoterms) {
                            case 'EXW':
                                // FCR status id
                                $statusIds[] = 5;
                                break;
                            case 'FOB':
                                // Goods on the Water status id
                                $statusIds[] = 6;
                                break;
                            case 'CIF':
                                // Arrival Notice Received status id
                                $statusIds[] = 13;
                                //Goods Received in Port status id
                                $statusIds[] = 7;
                                break;
                            case 'DDP Port':
                                // DO Issued status id
                                $statusIds[] = 10;
                                break;
                            case 'DDP Customer WH':
                                // FCR status id
                                $statusIds[] = 11;
                                break;
                            default:
                                $statusIds[] = 0;
                                break;
                        }
                    }
                }
            }

//            $main_table = array("tbl_order as o", array());
//            $join_tables = array(
//                array("left", "tbl_bp_order as bpoc", "bpoc.bp_order_id = o.customer_contract_id", array("bpoc.incoterms as bpoc_incoterms")),
//                array("left", "tbl_bp_order as bpos", "bpos.bp_order_id = o.supplier_contract_id", array("bpos.incoterms as bpos_incoterms")),
//            );
//            $rs = $this->common->JoinFetch($main_table, $join_tables, $condition, "", '');
//            $result = $this->common->MySqlFetchRow($rs, "array");
//            if (!empty($result)) {
//                $customerIncoterms = $result[0]['bpoc_incoterms'];
//                $supplierIncoterms = $result[0]['bpos_incoterms'];
//                if (!empty($customerIncoterms)) {
//                    switch (trim($customerIncoterms)) {
//                        case 'EXW':
//                            // FCR status id
//                            $statusIds[] = 5;
//                            break;
//                        case 'FOB':
//                            // Goods on the Water status id
//                            $statusIds[] = 6;
//                            break;
//                        case 'CIF':
//                            // Arrival Notice Received status id
//                            $statusIds[] = 13;
//                            //Goods Received in Port status id
//                            $statusIds[] = 7;
//                            break;
//                        case 'DDP Port':
//                            // DO Issued status id
//                            $statusIds[] = 10;
//                            break;
//                        case 'DDP Customer WH':
//                            // FCR status id
//                            $statusIds[] = 11;
//                            break;
//                        default:
//                            $statusIds[] = 0;
//                            break;
//                    }
//                }
//
//                if (!empty($supplierIncoterms)) {
//                    switch (trim($supplierIncoterms)) {
//                        case 'EXW':
//                            // FCR status id
//                            $statusIds[] = 5;
//                            break;
//                        case 'FOB':
//                            // Goods on the Water status id
//                            $statusIds[] = 6;
//                            break;
//                        case 'CIF':
//                            // Arrival Notice Received status id
//                            $statusIds[] = 13;
//                            //Goods Received in Port status id
//                            $statusIds[] = 7;
//                            break;
//                        case 'DDP Port':
//                            // DO Issued status id
//                            $statusIds[] = 10;
//                            break;
//                        case 'DDP Customer WH':
//                            // FCR status id
//                            $statusIds[] = 11;
//                            break;
//                        default:
//                            $statusIds[] = 0;
//                            break;
//                    }
//                }
//            }
        }

        //print_r($statusIds);die;
        return $statusIds;
    }

    public function show_pdf() {
        $resultData = $this->input->post("hidden_html");
        echo $resultData;
        echo '<script>window.print();</script>';
        exit;
    }

}

?>
