<?PHP
class Dashboardmodel extends CI_Model {

    function getDeliverySchedule($orderId = 0) {
        $qry = "SELECT SUM(sku_qty) as total, week_date FROM tbl_order_delivery_schedule 
                WHERE order_id IN (".$orderId.")
                GROUP BY MONTH(week_date), YEAR(week_date) 
                ORDER BY week_date"; 
        //CAST(MONTH(week_date) AS VARCHAR(2)) + '-' + CAST(YEAR(week_date) AS VARCHAR(4))
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

    function getActualData_old($orderId = 0,$week_date='',$containerStatusIds=array()) {        
        //container status ids condition
        $containerStatusIdsStr = 0;
        if(!empty($containerStatusIds)){
            $containerStatusIdsStr = implode(",",$containerStatusIds);
        }
        $status_ids_cond = " AND c_log.container_status_id IN (".$containerStatusIdsStr.") "; 
        //c_log.created_on,
        $month = date('m', strtotime($week_date));
        $year = date('Y', strtotime($week_date));
        $qry = "SELECT c.container_id FROM tbl_container as c 
                INNER join tbl_container_status_log as c_log on c_log.container_id = c.container_id AND c_log.container_status_id = c.last_container_status_id
                WHERE c.order_id = ".$orderId." AND MONTH(c_log.document_date) = ".$month." AND YEAR(c_log.document_date) = ".$year." 
                ".$status_ids_cond."
                GROUP by c.container_id";
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    
    function getContainerSKUData($container_id = 0) {
        //c_log.created_on,
        $qry = "SELECT SUM(quantity) as totalQty FROM tbl_container_sku
                WHERE container_id = ".$container_id." ";
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getSizwWiseDeliverySchedule($orderId = 0) {
        $qry = "SELECT SUM(osku.quantity) as total,size.size_name FROM tbl_order_sku as osku
                INNER JOIN tbl_sku as sku ON sku.sku_id = osku.sku_id
                INNER JOIN tbl_size as size ON size.size_id = sku.size_id
                WHERE osku.order_id IN (".$orderId.")
                group by size.size_id ORDER by size.size_name desc";
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getSizwWiseActual($containerIdsStr = '') {
        $qry = "SELECT SUM(csku.quantity) as total,size.size_name FROM tbl_container_sku as csku
                INNER JOIN tbl_sku as sku ON sku.sku_id = csku.sku_id
                INNER JOIN tbl_size as size ON size.size_id = sku.size_id
                WHERE csku.container_id IN (".$containerIdsStr.")
                group by size.size_id
                ORDER by size.size_name desc";
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getBrandWiseProjected($orderId = 0) {
        $qry = "SELECT SUM(osku.quantity) as total,brand.brand_name,brand.brand_id FROM tbl_order_sku as osku
                INNER JOIN tbl_sku as sku ON sku.sku_id = osku.sku_id
                INNER JOIN tbl_brand as brand ON brand.brand_id = sku.brand_id
                WHERE osku.order_id IN (".$orderId.")
                group by sku.brand_id ORDER by brand.brand_name desc";
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getBrandWiseSizeProjected($orderId = 0,$brand_id='') {
        $qry = "SELECT SUM(osku.quantity) as total,size.size_name,size.size_id,brand.brand_id FROM tbl_order_sku as osku
                INNER JOIN tbl_sku as sku ON sku.sku_id = osku.sku_id
                INNER JOIN tbl_size as size ON size.size_id = sku.size_id
                INNER JOIN tbl_brand as brand ON brand.brand_id = sku.brand_id
                WHERE osku.order_id IN (".$orderId.") and brand.brand_id IN (".$brand_id.")
                group by size.size_id,brand.brand_id ORDER by size.size_name desc";
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getBrandWiseActual($containerIdsStr = '',$brandIdsStr='') {
        $qry = "SELECT SUM(csku.quantity) as total,brand.brand_name,brand.brand_id FROM tbl_container_sku as csku
                INNER JOIN tbl_sku as sku ON sku.sku_id = csku.sku_id
                INNER JOIN tbl_size as size ON size.size_id = sku.size_id
                INNER JOIN tbl_brand as brand ON brand.brand_id = sku.brand_id
                WHERE csku.container_id IN (".$containerIdsStr.") and brand.brand_id IN (".$brandIdsStr.")
                group by brand.brand_id ORDER by brand.brand_name desc";
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getBrandWiseSizeActaul($containerIdsStr = '',$brandIdsStr='') {
        $qry = "SELECT SUM(csku.quantity) as total,size.size_id FROM tbl_container_sku as csku
                INNER JOIN tbl_sku as sku ON sku.sku_id = csku.sku_id
                INNER JOIN tbl_size as size ON size.size_id = sku.size_id 
                WHERE csku.container_id IN (".$containerIdsStr.") and sku.brand_id IN (".$brandIdsStr.")
                group by size.size_name ORDER by size.size_name desc";
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    } 
    
    function getLogisticsCotainerWithOutStatus($m_contract_id) { 
        $qry = "SELECT  COUNT(DISTINCT container_id) as cnt
                FROM tbl_container  
                WHERE order_id = ".$m_contract_id."  
                AND container_id NOT IN (SELECT DISTINCT container_id FROM tbl_container_status_log) "; 
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    } 
    
    function getLogisticsCotainerStatusIds($orderId) {  
        $qry = "SELECT  COUNT(DISTINCT container_id) as cnt ,last_container_status_id as container_status_id
        FROM tbl_container  
        WHERE order_id IN (".$orderId.") 
        GROUP by last_container_status_id "; 
        $query = $this->db->query($qry); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    } 
    
    function getDeliveryScheduleByWeek($orderId = 0) { 
        $qry = "SELECT week_date FROM tbl_order_delivery_schedule 
                WHERE order_id IN (".$orderId.") AND DATE_FORMAT(week_date, '%Y-%m-%d') >= '".date('Y-m-d')."' 
                GROUP BY week_date ";  
        $query = $this->db->query($qry); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    } 
    
    function getActualByWeekData_old($orderId = 0,$week_date='',$containerStatusIds=array()) {
        
        //container status ids condition
        $containerStatusIdsStr = 0;
        if(!empty($containerStatusIds)){
            $containerStatusIdsStr = implode(",",$containerStatusIds);
        }
        $status_ids_cond = " AND c_log.container_status_id IN (".$containerStatusIdsStr.") "; 
        //AND c_log.container_status_id = c.last_container_status_id
        $week_start_date = date('Y-m-d', strtotime($week_date));
        $week_end_date = date('Y-m-d', strtotime($week_start_date. ' + 6 days'));   
        $qry = "SELECT COUNT(DISTINCT c.container_id) as cnt FROM tbl_container as c 
                INNER join tbl_container_status_log as c_log on c_log.container_id = c.container_id 
                WHERE c.order_id = ".$orderId." AND c_log.document_date BETWEEN '".$week_start_date."' AND '".$week_end_date."'
                ".$status_ids_cond." ";  //c_log.document_date >= '".$week_start_date."' AND c_log.document_date < '".$week_end_date."
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
     function getActualByWeekDataBP($orderId = 0,$week_date='',$containerStatusIds=array()) { 
        //container status ids condition
        $containerStatusIdsStr = 0;
        if(!empty($containerStatusIds)){
            $containerStatusIdsStr = implode(",",$containerStatusIds);
        }
        $status_ids_cond = " AND c_log.container_status_id IN (".$containerStatusIdsStr.") ";  
        $week_start_date = date('Y-m-d', strtotime($week_date));
        $week_end_date = date('Y-m-d', strtotime($week_start_date. ' + 6 days'));   
        $qry = "SELECT COUNT(DISTINCT c.container_id) as cnt FROM tbl_container as c 
                INNER join tbl_container_status_log as c_log on c_log.container_id = c.container_id 
                WHERE c.order_id = ".$orderId." AND c_log.document_date BETWEEN '".$week_start_date."' AND '".$week_end_date."'
                ".$status_ids_cond." ";   
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getTotalContainerInContract($m_contract_id) { 
        $qry = "SELECT  COUNT(DISTINCT container_id) as cnt
                FROM tbl_container  
                WHERE order_id IN (".$m_contract_id.") "; 
        $query = $this->db->query($qry); 
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    } 
    
    function getActualData($orderId = 0,$containerStatusIds=array()) {        
        //container status ids condition
        $containerStatusIdsStr = 0;
        if(!empty($containerStatusIds)){
            $containerStatusIdsStr = implode(",",$containerStatusIds);
        }
        //echo $containerStatusIdsStr;die;
        $status_ids_cond = " AND c_log.container_status_id IN (".$containerStatusIdsStr.") "; 
        //c_log.created_on,
        $qry = "SELECT DISTINCT c.container_id,c.eta, c_log.document_date FROM tbl_container as c 
                INNER join tbl_container_status_log as c_log on c_log.container_id = c.container_id 
                WHERE c.order_id IN (".$orderId.")  ".$status_ids_cond." 
                GROUP by c.container_id"; 
        $query = $this->db->query($qry);
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getVoyageCompletionLinnerData_old($m_contract_id) {
        $condition = " c.order_id IN (".$m_contract_id.") AND c.eta IS NOT NULL and c.etd IS NOT NULL and c.liner_name <> 0 ";  
        $qry = "SELECT c.container_id, c.eta, c.etd, liner.liner_name 
                FROM tbl_container as c 
                INNER join tbl_liner as liner on liner.liner_id = c.liner_name 
                WHERE $condition "; 
        $query = $this->db->query($qry); 
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getVoyageCompletionLinnerData($m_contract_id) {
        $condition = " c.order_id IN (".$m_contract_id.") AND c.eta IS NOT NULL and c.etd IS NOT NULL and c.vessel_name <> '' and c.vessel_name IS NOT NULL ";  
        $qry = "SELECT c.container_id, c.eta, c.etd, c.vessel_name 
                FROM tbl_container as c WHERE $condition "; 
        $query = $this->db->query($qry); 
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getcontainerCntLinnerData_old($m_contract_id) {  
        $condition = " c.order_id IN (".$m_contract_id.") AND c.eta IS NOT NULL and c.etd IS NOT NULL and c.liner_name <> 0 ";  
        $qry = "SELECT COUNT(DISTINCT c.container_id) as cnt, liner.liner_name 
                FROM tbl_container as c 
                INNER join tbl_liner as liner on liner.liner_id = c.liner_name 
                WHERE $condition  GROUP by liner.liner_name"; 
        $query = $this->db->query($qry); 
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }
    
    function getcontainerCntLinnerData($m_contract_id) {  
        $condition = " c.order_id IN (".$m_contract_id.") AND c.eta IS NOT NULL and c.etd IS NOT NULL and c.vessel_name <> '' and c.vessel_name IS NOT NULL ";  
        $qry = "SELECT COUNT(DISTINCT c.container_id) as cnt, c.vessel_name 
                FROM tbl_container as c WHERE $condition  GROUP by c.vessel_name"; 
        $query = $this->db->query($qry); 
        //echo $this->db->last_query();exit;
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return false;
        }
    }

}

?>
