    <div class="main-panel">
      <div class="container">
        <!--<div class="page-inner">
                <div class="page-header page-header-btn">
                    <div class="page-header-title">
                        <h1 class="page-title"><a href="<?php echo base_url("dashboard"); ?>"></a> Dashboard</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                    </div>
                </div>
            </div>-->
        <div class="dashboard-wrapper">
          <div class="dashboard-title-wrapper">
            <h1 class="dashboard-title">Choose Business Partner</h1>
            <h4 class="dashboard-subtitle">Reports will be visible once you have made your selection</h4>
          </div>
          <div class="dashboard-select-wrapper">
            <div class="form-group">
              <select name="bp_contract" id="bp_contract" class="searchInput basic-single w100">
                <option value="" disabled selected hidden>BPContract NickName-Contract#</option>
                <option value="">BPContract NickName-Contract1</option>
                <option value="">BPContract NickName-Contract2</option>
                <option value="">BPContract NickName-Contract3</option>
                <option value="">BPContract NickName-Contract4</option>
                <option value="">BPContract NickName-Contract5</option>
              </select>
            </div>
            <div class="form-group">
              <select name="m_contract" id="m_contract" class="searchInput basic-single w100">
                <option value="" disabled selected hidden>MContractNickname-MContract#</option>
                <option value="">MContractNickname-MContract1</option>
                <option value="">MContractNickname-MContract2</option>
                <option value="">MContractNickname-MContract3</option>
                <option value="">MContractNickname-MContract4</option>
                <option value="">MContractNickname-MContract5</option>
              </select>
            </div>
          </div>
          <div class="title-sec-wrapper">
            <div class="title-sec-left">
            </div>
            <div class="title-sec-right">
              <a href="javascript:void(0);" class="btn-grey-mro">View Container Bible</a>
              <a class="btn-primary-mro" href="javascript:void(0);"><img src="assets/images/add-icon-white.svg" alt=""> Save Report as PDF</a>
            </div>
          </div>
          <div class="contract-details-wrapper">
            <div class="contract-initials-block">
              <div class="initials-block">BH</div>
            </div>
            <div class="contract-info-wrapper">
              <div class="contract-name">
                <h3>BP Name</h3>
              </div>
              <div class="contract-details">
                <div class="cd-single">
                  <span>Contract#:</span>
                  12345
                </div>
                <div class="cd-single">
                  <span>Manufacturer:</span>
                  Manufacturer Name
                </div>
                <div class="cd-single">
                  <span>Brand:</span>
                  Brand Name
                </div>
                <div class="cd-single">
                  <span>Product:</span>
                  Product Type
                </div>
              </div>
            </div>
          </div>
          <div class="contract-timeline-wrapper">
            <div class="ct-single">
              <p>Total SKU in contract</p>
              <h3>100,000,000</h3>
            </div>
            <div class="ct-single">
              <p>delivery timeline</p>
              <h3>15 Weeks</h3>
            </div>
            <div class="ct-single">
              <p>total containers</p>
              <h3>N/A</h3>
            </div>
            <div class="ct-single">
              <p>upcoming deliveries at port</p>
              <h3>0</h3>
            </div>
            <div class="ct-single">
              <p>contract completion</p>
              <h3>0</h3>
            </div>
          </div>
          <!--<div class="dashboard-title-wrapper">
            <h1 class="dashboard-title">Nothing to check yet</h1>
            <h4 class="dashboard-subtitle">Reports will auto generate once the contract is in progress</h4>
          </div>-->
          <div class="chart-block-wrapper">
            <div class="chart-title">
              <h2>Actual vs. Projected Shipping</h2>
            </div>
            <div class="chart-content">
              <img src="<?php echo base_url(); ?>assets/images/graph-1.jpg" class="img-fluid"/> 
            </div>
          </div>
          <div class="chart-block-wrapper">
            <div class="chart-title">
              <h2>Contract Progress - All Boxes</h2>
            </div>
            <div class="chart-content">
              <img src="<?php echo base_url(); ?>assets/images/graph-2.jpg" class="img-fluid"/> 
            </div>
          </div>
          <div class="chart-block-wrapper">
            <div class="chart-title">
              <h2>Synguard Progress</h2>
            </div>
            <div class="chart-content">
              <img src="<?php echo base_url(); ?>assets/images/graph-3.jpg" class="img-fluid"/> 
            </div>
          </div>
          <div class="chart-block-wrapper">
            <div class="chart-title">
              <h2>Logistics Progress</h2>
            </div>
            <div class="chart-content">
              <img src="<?php echo base_url(); ?>assets/images/graph-4.jpg" class="img-fluid"/> 
            </div>
          </div>
          <div class="row">
            <div class="col-12 col-md-6">
              <div class="chart-block-wrapper">
                <div class="chart-title">
                  <h2>Upcoming Deliveries at Port</h2>
                </div>
                <div class="chart-content">
                  <img src="<?php echo base_url(); ?>assets/images/graph-5.jpg" class="img-fluid"/> 
                </div>
              </div>
            </div>
            <div class="col-12 col-md-6">
              <div class="chart-block-wrapper">
                <div class="chart-title">
                  <h2>Voyage Completion</h2>
                </div>
                <div class="chart-content">
                  <img src="<?php echo base_url(); ?>assets/images/graph-6.jpg" class="img-fluid"/> 
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>