<div class="main-panel" id="graph-section">   
    <div class="container" style="margin-top: 30px;"> 
        <div class="dashboard-wrapper"> 
            <div class="dashboard-select-wrapper"> 
                <div style="float: left;"><b>MContract : <?= $m_contract; ?></b> </div> 
            </div>
        </div>
        <div class="dashboard-wrapper" style="margin-top: 30px;">  
            <div class="contract-details-wrapper">
                <div class="contract-initials-block">
                    <div class="initials-block">BH</div>
                </div>
                <div class="contract-info-wrapper">
                    <div class="contract-name">
                        <h3><?= $bp_name; ?></h3>
                    </div>
                    <div class="contract-details">
                        <div class="cd-single">
                            <span>Contract#:</span>
                            <?= $contract; ?>
                        </div>
                        <div class="cd-single">
                            <span>Manufacturer:</span>
                            <?= $manufacture; ?>
                        </div>
                        <div class="cd-single">
                            <span>Brand:</span>
                            <?= $brand; ?>
                        </div>
                        <div class="cd-single">
                            <span>Product:</span>
                            <?= $category; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contract-timeline-wrapper">
                <div class="ct-single">
                    <p>TOTAL QUANTITY</p>
                    <h3><?= $projectedGrandTotal; ?></h3>
                </div>
                <div class="ct-single">
                    <p>DELIVERY Period</p>
                    <h3><?= $totalWeek ?> Weeks</h3>
                </div>
                <div class="ct-single">
                    <p>TOTAL # CONTAINERS</p>
                    <h3><?= $totalContainer ?></h3>
                </div>
                <div class="ct-single">
                    <p># Container in Transit</p>
                    <h3><?= $contaierTransit ?></h3>
                </div>
                <div class="ct-single">
                    <p># Containers Delivered</p>
                    <h3><?= $contaierDelivered ?></h3>
                </div>
            </div>
            
            <div class="row" style="margin-top: 30px;">
                <div class="col-12 col-md-6">
                    <div class="chart-block-wrapper">
                        <div class="row">
                            <div class="col-12">
                                <div class="chart-block-single">
                                    <div id="contract" class="g_chart g_chart_border"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <?php
                            $contract_cnt = 0;
                            if (isset($contractSizeProjected)) {
                                foreach ($contractSizeProjected as $valueDiv) {
                                    if (!empty($valueDiv)) {
                                        $contract_cnt++;
                                        ?>
                                        <?php if ($contract_cnt < 3) { ?>
                                            <div class="col-12 col-sm-6">
                                                <div class="chart-block-single">
                                                    <div id="contact_<?= $contract_cnt ?>" class="g_chart_small g_chart_border"></div>
                                                </div>
                                            </div>
                                        <?php } else if ($contract_cnt == 3) { ?>

                                            <div class="col-12 col-sm-6">
                                                <div class="chart-block-single">
                                                    <div id="contact_<?= $contract_cnt ?>" class="g_chart_small g_chart_border"></div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-12 col-sm-6">
                                                <div class="chart-block-single">
                                                    <div id="contact_<?= $contract_cnt ?>" class="g_chart_small g_chart_border"></div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
                $brandcntDiv = 0;
                foreach ($brandWiseProjected as $key => $value) {
                    $brandcntDiv++;
                    $keyArr = explode("#", $key);
                    $brand_id = $keyArr[0];
                    $brand_name = $keyArr[1];
                    ?>
                    <div class="col-12 col-md-6">
                        <div class="chart-block-wrapper">
                            <div class="row">
                                <div class="col-12">
                                    <div class="chart-block-single">
                                        <div id="brand_<?= $brandcntDiv ?>" class="g_chart g_chart_border"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">


                                <?php
                                $brandSizecntDiv = 0;
                                if (isset($brandWiseSizeProjected[$brand_id])) {
                                    foreach ($brandWiseSizeProjected[$brand_id] as $keySize => $valueSize) {
                                        if (!empty($valueSize)) {
                                            $brandSizecntDiv++;
                                            $idName = 'brand_' . $brandcntDiv . '_size_' . $brandSizecntDiv;
                                            ?>
                                            <?php if ($brandSizecntDiv < 3) { ?>
                                                <div class="col-12 col-sm-6">
                                                    <div class="chart-block-single">
                                                        <div id="<?= $idName ?>" class="g_chart_small g_chart_border"></div>
                                                    </div>
                                                </div>
                                            <?php } else if ($brandSizecntDiv == 3) { ?>


                                                <div class="col-12 col-sm-6">
                                                    <div class="chart-block-single">
                                                        <div id="<?= $idName ?>" class="g_chart_small g_chart_border"></div>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="col-12 col-sm-6">
                                                    <div class="chart-block-single">
                                                        <div id="<?= $idName ?>" class="g_chart_small g_chart_border"></div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <?php
                                        }
                                    }
                                }
                                ?> 
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="chart-block-wrapper">
                        <div class="chart-title">
                            <h2>Actual vs. Projected Shipping</h2>
                        </div>
                        <div id="lineChart" class="g_chart"></div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="chart-block-wrapper">
                        <div class="chart-title">
                            <h2>Upcoming Deliveries at Port </h2>
                        </div>
                        <?php if (!empty($deliveryAtPort)) { ?>
                            <div id="upcomingDeliPort" class="g_chart"></div>
                        <?php } else { ?>
                            <div> 
                              <div class="g_chart"><h4 class="dashboard-subtitle" style="padding: 30px;">No Upcoming Deliveries at Port</h4></div>
                            </div>
                        <?php } ?>  
                    </div>
                </div>
            </div>
            <!-- <?php
            $brandcntDiv = 0;
            foreach ($brandWiseProjected as $key => $value) {
                $brandcntDiv++;
                $keyArr = explode("#", $key);
                $brand_id = $keyArr[0];
                $brand_name = $keyArr[1];
                ?>
                      <div class="chart-block-wrapper">
                        <div class="chart-title">
                          <h2><?= $brand_name ?> Progress</h2>
                          <div class="row">
                            <div class="col-12 col-md-6">
                              <div id="brand_<?= $brandcntDiv ?>" style="height: 400px;width: auto; margin: 0 auto !important;"></div>
                            </div>
                            <div class="col-12 col-md-6">
                              <div class="row">
                <?php
                $brandSizecntDiv = 0;
                if (isset($brandWiseSizeProjected[$brand_id])) {
                    foreach ($brandWiseSizeProjected[$brand_id] as $keySize => $valueSize) {
                        if (!empty($valueSize)) {
                            $brandSizecntDiv++;
                            $idName = 'brand_' . $brandcntDiv . '_size_' . $brandSizecntDiv;
                            ?>
                            <?php if ($brandSizecntDiv < 3) { ?>
                                                                        <div class="col-12 col-sm-6">
                                                                          <div id="<?= $idName ?>" style="height: 200px;width: auto; margin: 0 auto !important;"></div>
                                                                        </div>
                            <?php } else if ($brandSizecntDiv == 3) { ?>
                                                              </div>
                                                              <div class="row">
                                                                <div class="col-12 col-sm-6">
                                                                  <div id="<?= $idName ?>" style="height: 200px;width: auto; margin: 0 auto !important;"></div>
                                                                </div>
                            <?php } else { ?>
                                                                <div class="col-12 col-sm-6">
                                                                  <div id="<?= $idName ?>" style="height: 200px;width: auto; margin: 0 auto !important;"></div>
                                                                </div>
                            <?php } ?>
                            <?php
                        }
                    }
                }
                ?>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
            <?php } ?> -->

            <div class="chart-block-wrapper">
                <div class="chart-title">
                    <h2>Logistics Progress</h2>
                </div>
                <div id="Logistics" class="g_chart"></div> 
            </div>
            <div class="chart-block-wrapper">
                <div class="chart-title">
                    <h2>Voyage Completion </h2>
                </div>
                <?php if (!empty($voyageCompletionData)) { ?>
                    <div id="voyageCompletion" class="g_chart"></div>
                <?php } else { ?>
                    <div id="voyageCompletion" class="g_chart">
                        <h4 class="dashboard-subtitle" style="padding: 30px;">No Voyage Completion</h4>
                    </div>
                <?php } ?> 
            </div> 
        </div>
    </div>
</div>

<!-- chart graph files -->
<!--<script src="<?php echo base_url(); ?>assets/js/plugin/chart.js/chart.min.js" type="text/javascript"></script> -->

<script src="<?php echo base_url(); ?>assets/js/plugin/chart.js/loader.js" type="text/javascript"></script>
<!--<script src="<?php echo base_url(); ?>assets/js/plugin/chart.js/fusioncharts.js?cacheBust=838"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/chart.js/fusioncharts.powercharts.js?cacheBust=838"></script> -->
<script type="text/javascript">
    //Line Charts 
    google.charts.load('current', {
        packages: ['corechart', 'line']
    });
    google.charts.setOnLoadCallback(drawLineStyles);

    function drawLineStyles() {
        var data = google.visualization.arrayToDataTable([
            ['Year', 'ACTUAL', 'PROJECTED', ],
<?php
if (!empty($projectedActual)) {
    foreach ($projectedActual as $value) {
        ?>['<?= $value['monthYear'] ?>', <?= $value['actual'] ?>, <?= $value['projected'] ?>],
    <?php
    }
}
?>
        ]);

        var options = {
            //            hAxis: {
            //                title: 'PROJECTED ACTUAL'
            //            },
            vAxis: {
                title: 'No. of Boxes (in Millions)'
            },
            colors: ['#8B929A', '#FFBD32'],

            pointsVisible: true,
            pointRadius: 50,
            series: {
                0: {
                    lineWidth: 5,
                    lineDashStyle: [8, 5, 8, 5],
                    //                    curveType: 'function'
                },
                1: {
                    lineWidth: 5,
                    lineDashStyle: [7, 2, 4, 3],
                    //                    curveType: 'function'
                }
            },
            //'width': 950,
            //'height': 400,
            legend: 'bottom'
        };


        var chart = new google.visualization.LineChart(document.getElementById('lineChart'));
        chart.draw(data, options);
    }


    //Donutchart Charts 
    google.charts.load("current", {
        packages: ["corechart"]
    });
    google.charts.setOnLoadCallback(drawDonutchart);

    function drawDonutchart() {
        var data = google.visualization.arrayToDataTable([
            ['Task', 'Hours per Day'],
            ['COMPLETED', <?= $completedTotal ?>],
            ['IN PROGRESS', <?= $inprogressTotal ?>]
        ]);

        var options = {
            annotations: {
                textStyle: {
//                    fontName: 'Calibri',
                    fontSize: 18,
                    bold: true,
                    italic: true,
                }
            },
            pieSliceTextStyle: {
                color: '#000',
            },
            title: 'Contract Progress',
            titleTextStyle: {
                color: '#2C4159', // any HTML string color ('red', '#cc00cc')
//                fontName: 'Calibri', // i.e. 'Times New Roman'
                fontSize: '20', // 12, 18 whatever you want (don't specify px)
                bold: true, // true or false
                italic: false // true of false
            },
            colors: ['#8B929A', '#FFBD32'],
            legend: 'bottom',
            //width: 400,
            //height: 400,
            pieHole: 0.5,
        };

        var chart = new google.visualization.PieChart(document.getElementById('contract'));
        chart.draw(data, options);

    }

    //Contract size chart
<?php
$cnt = 0;
if (!empty($contractSizeProjected)) {
    foreach ($contractSizeProjected as $key => $value) {
        if (!empty($value)) {
            $cnt++;
            $actual = 0;
            if (isset($contractSizeActual[$key])) {
                $actual = $contractSizeActual[$key];
            }
            $inprogress = $value - $actual;
            ?>
                google.charts.load("current", {
                    packages: ["corechart"]
                });
                google.charts.setOnLoadCallback(drawContractSize<?= $cnt ?>);

                function drawContractSize<?= $cnt ?>() {
                    var data = google.visualization.arrayToDataTable([
                        ['Task', 'Hours per Day'],
                        ['COMPLETED', <?= $actual ?>],
                        ['IN PROGRESS', <?= $inprogress ?>]
                    ]);

                    var options = {
                        annotations: {
                            textStyle: {
//                                fontName: 'Calibri',
                                fontSize: 18,
                                bold: true,
                                italic: true,
                            }
                        },
                        pieHole: 0.3,
                        pieSliceTextStyle: {
                            color: '#000',
                            fontSize: 10
                        },
                        title: '<?= strtoupper($key) ?>',
                        titleTextStyle: {
                            color: '#32475E', // any HTML string color ('red', '#cc00cc')
//                            fontName: 'Calibri', // i.e. 'Times New Roman'
                            fontSize: '14', // 12, 18 whatever you want (don't specify px)
                            bold: false, // true or false
                            italic: false // true of false
                        },
                        colors: ['#8B929A', '#FFBD32'],
                        legend: 'none',
                        //width: 200,
                        //height: 200,
                    };

                    var chart = new google.visualization.PieChart(document.getElementById('contact_<?= $cnt ?>'));
                    chart.draw(data, options);
                }
            <?php
        }
    }
}
?>


    //brand wise and brand size chart
<?php
$brandcnt = 0;
if (!empty($brandWiseProjected)) {
    foreach ($brandWiseProjected as $key => $value) {
        $brandcnt++;
        $keyArr = explode("#", $key);
        $brand_id = $keyArr[0];
        $brand_name = $keyArr[1];
        $brandActual = 0;
        if (isset($brandWiseActual[$brand_id])) {
            $brandActual = $brandWiseActual[$brand_id];
        }
        $BrandInprogress = $value - $brandActual;
        ?>
            google.charts.load("current", {
                packages: ["corechart"]
            });
            google.charts.setOnLoadCallback(drawBrandSize<?= $brandcnt ?>);

            function drawBrandSize<?= $brandcnt ?>() {
                var data = google.visualization.arrayToDataTable([
                    ['Task', 'Hours per Day'],
                    ['COMPLETED', <?= $brandActual ?>],
                    ['REMAINDER', <?= $BrandInprogress ?>]
                ]);

                var options = {
                    annotations: {
                        textStyle: {
//                            fontName: 'Calibri',
                            fontSize: 18,
                            bold: true,
                            italic: true,
                        }
                    },
                    pieSliceTextStyle: {
                        color: '#000',
                    },
                    title: '<?= $brand_name ?>',
                    titleTextStyle: {
                        color: '#2C4159', // any HTML string color ('red', '#cc00cc')
//                        fontName: 'Calibri', // i.e. 'Times New Roman'
                        fontSize: '20', // 12, 18 whatever you want (don't specify px)
                        bold: true, // true or false
                        italic: false // true of false
                    },
                    colors: ['#8B929A', '#FFBD32'],
                    legend: 'bottom',
                    //width: 400,
                    //height: 400,
                    pieHole: 0.5,
                };

                var chart = new google.visualization.PieChart(document.getElementById('brand_<?= $brandcnt ?>'));
                chart.draw(data, options);
            }
        <?php
        $brandSizecnt = 0;
        if (isset($brandWiseSizeProjected[$brand_id])) {
            foreach ($brandWiseSizeProjected[$brand_id] as $keySize => $valueSize) {
                if (!empty($valueSize)) {
                    $brandSizecnt++;
                    $keySizeArr = explode("#", $keySize);
                    $size_id = $keySizeArr[0];
                    $size_name = $keySizeArr[1];

                    $idNameSize = 'brand_' . $brandcnt . '_size_' . $brandSizecnt;
                    $brandSizeActual = 0;
                    if (isset($brandWiseSizeActual[$brand_id][$size_id])) {
                        $brandSizeActual = $brandWiseSizeActual[$brand_id][$size_id];
                    }
                    $BrandSizeInprogress = abs($valueSize - $brandSizeActual);
                    ?>
                        google.charts.load("current", {
                            packages: ["corechart"]
                        });
                        google.charts.setOnLoadCallback(drawBrandSize<?= $brandcnt . $brandSizecnt ?>);

                        function drawBrandSize<?= $brandcnt . $brandSizecnt ?>() {
                            var data = google.visualization.arrayToDataTable([
                                ['Task', 'Hours per Day'],
                                ['COMPLETED', <?= $brandSizeActual ?>],
                                ['REMAINDER', <?= $BrandSizeInprogress ?>]
                            ]);

                            var options = {
                                annotations: {
                                    textStyle: {
//                                        fontName: 'Calibri',
                                        fontSize: 18,
                                        bold: true,
                                        italic: true,
                                    }
                                },
                                pieHole: 0.3,
                                pieSliceTextStyle: {
                                    color: '#000',
                                    fontSize: 10
                                },
                                title: '<?= $size_name ?>',
                                titleTextStyle: {
                                    color: '#32475E', // any HTML string color ('red', '#cc00cc')
//                                    fontName: 'Calibri', // i.e. 'Times New Roman'
                                    fontSize: '14', // 12, 18 whatever you want (don't specify px)
                                    bold: false, // true or false
                                    italic: false // true of false
                                },
                                colors: ['#8B929A', '#FFBD32'],
                                legend: 'none',
                                //width: 200,
                                //height: 200,
                            };

                            var chart = new google.visualization.PieChart(document.getElementById('<?= $idNameSize ?>'));
                            chart.draw(data, options);
                        }

                    <?php
                }
            }
        }
        ?>

        <?php
    }
}
?>

    //Logistics progress graph
<?php if (!empty($logisticsContainerStatus)) { ?>
        google.charts.load('current', {
            'packages': ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart1);

        function drawChart1() {
            var data = google.visualization.arrayToDataTable([
                //['x','n1', 'n2', 'n3', 'n4'], //, {'type': 'string', 'role': 'annotationText'}
    <?php
    foreach ($logisticsContainerStatus as $key => $value) {
        $Lvalue = explode('#', $value);
        $previousCnt = $Lvalue[0];
        $actaulCnt = $Lvalue[1];
        //'{ size: 18; shape-type: star; fill-color: #bbedc9; }'
        ?>['<?= $key ?>', <?= $previousCnt ?>, <?= $previousCnt ?>, <?= $actaulCnt ?>, <?= $actaulCnt ?>],
        <?php
    }
    ?>
            ], true);

            var options = {
                legend: 'none',
                bar: {
                    groupWidth: '80%'
                }, // Remove space between bars.
                candlestick: {
                    fallingColor: {
                        strokeWidth: 0,
                        fill: '#FFBD32'
                    }, // red
                    risingColor: {
                        strokeWidth: 0,
                        fill: '#FFBD32'
                    } // green 
                },
                vAxis: {
                    title: 'No. of Containers'
                },
                //'width': 960,
                //'height': 400,
            };

            var chart = new google.visualization.CandlestickChart(document.getElementById('Logistics'));
            chart.draw(data, options);
        }

<?php } ?>

    //upcoming delivery at port graph 
<?php if (!empty($deliveryAtPort)) { ?>
        google.charts.load("current", {
            packages: ['corechart']
        });
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ["Week", "Containers", {
                        role: "style"
                    }],
    <?php
    foreach ($deliveryAtPort as $key => $value) {
        ?>['<?= $key ?>', <?= $value ?>, ""],
        <?php
    }
    ?>
            ]);

            var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
                {
                    calc: "stringify",
                    sourceColumn: 1,
                    type: "string",
                    role: "annotation"
                },
                2
            ]);

            var options = {
                colors: ['#FFBD32'],
                legend: {
                    position: 'none'
                },
                vAxis: {
                    title: 'No. of Containers'
                },
                width: 940,
                height: 400,
                bar: {
                    groupWidth: "50%"
                },

            };
            var chart = new google.visualization.ColumnChart(document.getElementById("upcomingDeliPort"));
            chart.draw(view, options);
        }

<?php } ?>

    //Voyage Completion graph
<?php if (!empty($voyageCompletionData)) { ?>
        google.charts.load("current", {
            packages: ['corechart', 'bar']
        }); //corechart
        google.charts.setOnLoadCallback(drawChartXLine);

        function drawChartXLine() {
            var data = google.visualization.arrayToDataTable([
                ['Opening Move', 'Percentage', {
                        role: 'style'
                    }],
    <?php
    foreach ($voyageCompletionData as $key => $value) {
        ?>['<?= $key ?>', <?= $value ?>, 'stroke-color: #8B929A; stroke-width: 1; fill-color: #8B929A; fill-opacity: 0.8;stroke-dasharray:5'],
        <?php
    }
    ?>
            ]);
            var view = new google.visualization.DataView(data);
            //        view.setColumns([0, 1,
            //            {calc: "stringify",
            //                sourceColumn: 1,
            //                type: "string",
            //                role: "annotation"},
            //            2]);
            var options = {
                //    title: "Density of Precious Metals, in g/cm^3",
                //            width: 600,
                //height: 400,
                bar: {
                    groupWidth: "30%"
                },
                legend: {
                    position: "none"
                },
                //isStacked: 'percent',
            };

            var chart = new google.visualization.BarChart(document.getElementById("voyageCompletion"));
            chart.draw(view, options);

        }

<?php } ?>
</script>    