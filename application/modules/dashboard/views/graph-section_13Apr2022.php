<?php
//echo '<pre>';print_r($logisticsContainerStatus);die;
$deliveryAtPortData = $deliveryAtPort;
if(empty($deliveryAtPort)){
    $cMonth = date('M');
    $cMonth1 = date('M', strtotime('+1 month'));
    $cMonth2 = date('M', strtotime('+2 month'));
    $cMonth3 = date('M', strtotime('+3 month'));
    $deliveryAtPortData = array($cMonth=>0,$cMonth1=>0,$cMonth2=>0,$cMonth3=>0);
}

//$logisticsContainerStatus = Array('Projected' => '0#10#10','SGS FRI' => '10#30#20','FCR' => '30#55#25','Goods on the Water' => '55#70#15','DO Issued' => '70#88#18','Transloading / Customer WH' => '88#118#30');

$logisticsContainerStatusData = $logisticsContainerStatus;
if(empty($logisticsContainerStatus)){ 
    $logisticsContainerStatusData = Array('Projected (0)' => 0,'SGS FRI (0)' => 0,'FCR (0)' => 0,'Goods on the Water (0)' => 0,'DO Issued (0)' => 0,'Transloading / Customer WH (0)' => 0);
} 

$logisticsContainerStatusDataTest = Array('Projected' => 10,'SGS FRI' => 20,'FCR' => 25,'Goods on the Water' => 15,'DO Issued' => 18,'Transloading / Customer WH' => 30);

$voyageCompletionDataData = $voyageCompletionData;
if(empty($voyageCompletionData)){ 
    $voyageCompletionDataData = Array('YML' => 0,'MSC' => 0,'COSCO' => 0,'CMA' => 0);
} 

?>
<div class="title-sec-wrapper">
  <div class="title-sec-left">
  </div>
  <div class="title-sec-right">
    <?php
    $containerBible = '#';
    if ($this->privilegeduser->hasPrivilege("ContainersDetailsList") && $m_contract_id > 0) {
      $containerBible = base_url() . "listOfContainer?text=" . rtrim(strtr(base64_encode("order_id=" . $m_contract_id . "&bp_order_id=" . $bp_contract_id), '+/', '-_'), '=');
    }
    $pdfURL = base_url() . "dashboard/savePDF?text=" . rtrim(strtr(base64_encode("m_contract_id=" . $m_contract_id . "&bp_contract_id=" . $bp_contract_id), '+/', '-_'), '=');
    ?>
    <a href="<?= $containerBible ?>" class="btn-grey-mro">View Container Bible</a>
    <!--<a class="btn-primary-mro" href="<?= $pdfURL ?>"><img src="assets/images/add-icon-white.svg" alt=""> Save Report as PDF</a>-->
  </div>
</div>
<div class="contract-details-wrapper">
  <div class="contract-initials-block">
    <div class="initials-block">BH</div>
  </div>
  <div class="contract-info-wrapper">
    <div class="contract-name">
      <h3><?= $bp_name; ?></h3>
    </div>
    <div class="contract-details">
      <div class="cd-single">
        <span>Contract#:</span>
        <?= $contract; ?>
      </div>
      <div class="cd-single">
        <span>Manufacturer:</span>
        <?= $manufacture; ?>
      </div>
      <div class="cd-single">
        <span>Brand:</span>
        <?= $brand; ?>
      </div>
      <div class="cd-single">
        <span>Product:</span>
        <?= $category; ?>
      </div>
    </div>
  </div>
</div>
<div class="contract-timeline-wrapper">
  <div class="ct-single">
    <p>TOTAL QUANTITY</p>
    <h3><?= number_format($projectedGrandTotal); ?></h3>
  </div>
  <div class="ct-single">
    <p>DELIVERY Period</p>
    <h3><?= $totalWeek ?> Weeks</h3>
  </div>
  <div class="ct-single">
    <p>TOTAL # CONTAINERS</p>
    <h3><?= $totalContainer ?></h3>
  </div>
  <div class="ct-single">
    <p># Container in Transit</p>
    <h3><?= $contaierTransit ?></h3>
  </div>
  <div class="ct-single">
    <p># Containers Delivered</p>
    <h3><?= $contaierDelivered ?></h3>
  </div>
</div>
<!--<div class="dashboard-title-wrapper">
    <h1 class="dashboard-title">Nothing to check yet</h1>
    <h4 class="dashboard-subtitle">Reports will auto generate once the contract is in progress</h4>
</div>-->

<div class="row">
    <div class="col-12 col-md-12">
        <div class="chart-block-wrapper">
          <div class="chart-title">
            <h2>Actual vs. Projected Shipping</h2>
          </div>
          <div id="lineChart" class="g_chart"></div>
        </div>
    </div> 
    
  <?php if($brandLenght > 1 || $brandLenght ==0 ){ ?>  
  <div class="col-12 col-md-12">
    <div class="chart-block-wrapper">
      <div class="row">
        <div class="col-12 col-md-6">
          <div class="chart-block-single">
            <h4>Total Progress</h4>  
             <div id="contract" class="g_chart g_chart_border"></div>
             <div id="contract_cnt" class="large-pie-chart"></div> 
          </div>
        </div>
        <div class="col-12 col-md-6">
          <div class="row">
            <?php
            $contract_cnt = 0;
            if (isset($contractSizeProjected)) {
              foreach ($contractSizeProjected as $key => $valueDiv) {
                if (!empty($valueDiv)) {
                  $contract_cnt++;
            ?>
                  <?php if ($contract_cnt < 3) { ?>
                    <div class="col-12 col-sm-6">
                      <div class="chart-block-single">
                        <p class="small-chart-title"><?= strtoupper($key) ?></p>
                        <div id="contact_<?= $contract_cnt ?>" class="g_chart_small g_chart_border"></div>
                        <div id="contract_cnt_<?= $contract_cnt ?>" class="small-pie-chart"></div>                         
                      </div>
                    </div>
                  <?php } else if ($contract_cnt == 3) { ?>
                    <div class="col-12 col-sm-6">
                      <div class="chart-block-single">
                        <p class="small-chart-title"><?= strtoupper($key) ?></p>
                        <div id="contact_<?= $contract_cnt ?>" class="g_chart_small g_chart_border"></div>
                        <div id="contract_cnt_<?= $contract_cnt ?>" class="small-pie-chart"></div>
                      </div>
                    </div>
                  <?php } else { ?>
                    <div class="col-12 col-sm-6">
                      <div class="chart-block-single">
                        <p class="small-chart-title"><?= strtoupper($key) ?></p>
                        <div id="contact_<?= $contract_cnt ?>" class="g_chart_small g_chart_border"></div>
                        <div id="contract_cnt_<?= $contract_cnt ?>" class="small-pie-chart"></div>
                      </div>
                    </div>
                  <?php } ?>   
            <?php }
              }
            } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php } ?> 
  <div class="col-12 col-md-12">
    <?php
    $brandcntDiv = 0;
    foreach ($brandWiseProjected as $key => $value) {
      $brandcntDiv++;
      $keyArr = explode("#", $key);
      $brand_id = $keyArr[0];
      $brand_name = $keyArr[1];
    ?>
      <div class="chart-block-wrapper">
        <div class="row">
          <div class="col-12 col-md-6">
            <div class="chart-block-single">
              <h4><?= strtoupper($brand_name) ?></h4>
              <div id="brand_<?= $brandcntDiv ?>" class="g_chart g_chart_border"></div>
              <div id="brand_cnt_<?= $brandcntDiv ?>" class="large-pie-chart"></div>                         
            </div>
          </div>
          <div class="col-12 col-md-6">
            <div class="row">
              <?php
              $brandSizecntDiv = 0;
              if (isset($brandWiseSizeProjected[$brand_id])) {
                foreach ($brandWiseSizeProjected[$brand_id] as $keySize => $valueSize) {
                  if (!empty($valueSize)) {
                    $brandSizecntDiv++;

                    $keySizeArr = explode("#", $keySize);
                    $size_id = $keySizeArr[0];
                    $size_name = $keySizeArr[1];

                    $idName = 'brand_' . $brandcntDiv . '_size_' . $brandSizecntDiv;
              ?>
                    <?php if ($brandSizecntDiv < 3) { ?>
                      <div class="col-12 col-sm-6">
                        <div class="chart-block-single">
                          <p class="small-chart-title"><?= strtoupper($size_name) ?></p>
                          <div id="<?= $idName ?>" class="g_chart_small g_chart_border"></div>
                          <div id="cnt_<?= $idName ?>" class="small-pie-chart"></div>                         
                        </div>
                      </div>
                    <?php } else if ($brandSizecntDiv == 3) { ?>
                      <div class="col-12 col-sm-6">
                        <div class="chart-block-single">
                          <p class="small-chart-title"><?= strtoupper($size_name) ?></p>
                          <div id="<?= $idName ?>" class="g_chart_small g_chart_border"></div>
                          <div id="cnt_<?= $idName ?>" class="small-pie-chart"></div>                         
                        </div>
                      </div>
                    <?php } else { ?>
                      <div class="col-12 col-sm-6">
                        <div class="chart-block-single">
                          <p class="small-chart-title"><?= strtoupper($size_name) ?></p>
                          <div id="<?= $idName ?>" class="g_chart_small g_chart_border"></div>
                          <div id="cnt_<?= $idName ?>" class="small-pie-chart"></div>                         
                        </div>
                      </div>
                    <?php } ?>
              <?php }
                }
              } ?>
            </div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
  
  <div class="col-12 col-md-12">
    <div class="chart-block-wrapper">
      <div class="chart-title">
        <h2>Upcoming Deliveries at Port </h2>
      </div> 
        <?php if(empty($deliveryAtPort)){ ?>
        <div class="nodata-delivery">
            There is no data available for these parameters.
        </div>   
        <script>
          $('#upcomingDeliPort').addClass('nodata-graph');   
        </script>
        <?php } ?>
        <div id="upcomingDeliPort" class="g_chart"></div>        
    </div>
  </div>
    
  <div class="col-12 col-md-12">
    <div class="chart-block-wrapper">
      <div class="chart-title">
        <h2>Logistics Progress</h2>
      </div>
        
       <?php if(empty($logisticsContainerStatus)){ ?>
        <div class="nodata-delivery">
            There is no data available for these parameters.
        </div>   
        <script>
          $('#Logistics').addClass('nodata-graph');   
        </script>
        <?php } ?>  
      <div id="Logistics" class="g_chart"></div>

    </div>
  </div>
  <div class="col-12 col-md-12">
    <div class="chart-block-wrapper">
      <div class="chart-title">
        <h2>Voyage Completion </h2>
      </div> 
         <?php if(empty($voyageCompletionData)){ ?>
        <div class="nodata-delivery">
            There is no data available for these parameters.
        </div>   
        <script>
          $('#voyageCompletion').addClass('nodata-graph');   
        </script>
        <?php } ?>  
        <div id="voyageCompletion" class="g_chart"></div> 
    </div>
  </div>
    
    
    <div class="col-12 col-md-12">
        <div class="chart-block-wrapper">
          <div class="chart-title">
            <h2>Logistics Progress</h2>
          </div>
            
            <figure class="highcharts-figure">
                <div id="logistics_highchart" class="g_chart"></div> 
            </figure> 
        </div>
    </div>  
    <style>
    .highcharts-figure, .highcharts-data-table table {
        min-width: 320px;
        max-width: 1050px;
        margin: 2em auto;
    }
    text.highcharts-credits {
        display: none;
    } 
    </style>  
<!-- chart graph files --> 
<script src="<?php echo base_url(); ?>assets/js/plugin/chart.js/loader.js" type="text/javascript"></script> 
<script src="<?php echo base_url(); ?>assets/js/plugin/chart.js/highcharts.js"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/chart.js/highcharts-more.js"></script> 

<script type="text/javascript">  
    
    //Logistics highchart
    Highcharts.chart('logistics_highchart', {
        chart: {
            type: 'waterfall'
        }, 
        title: {
            text: ''
        }, 
        xAxis: {
            type: 'category'
        }, 
        yAxis: {
            title: {
                text: 'No. of Containers'
            }
        }, 
        legend: {
            enabled: false
        }, 
        tooltip: {
            pointFormat: '<b>{point.y:,.0f}</b>'
        }, 
        series: [{
//            upColor: Highcharts.getOptions().colors[2],
//            color: Highcharts.getOptions().colors[3],
            data: [ 
                 <?php
                    foreach ($logisticsContainerStatusDataTest as $key => $value) {
//                      $Lvalue = explode('#', $value);
//                      $previousCnt = $Lvalue[0];
//                      $actaulCnt = $Lvalue[1];
//                      $container = $Lvalue[2];
                      if($key=='Projected'){
                          ?>                 
                          {
                              name: '<?= $key ?>',
                              y: <?= $value ?>,
                              color: '#B6D7A8'
                          },     
                          <?php
                      }else{
                          ?>
                          {
                              name: '<?= $key ?>',
                              y: <?= $value ?>,
                              color: '#FFE59A'
                          },   
                          <?php 
                      }
                    }
                ?>  
                ],
            dataLabels: {
                enabled: true,
//                formatter: function () {
//                    return Highcharts.numberFormat(this.y / 1000, 0, ',');
//                },
                style: { 
                    fontWeight: 400,
                    fontSize: '12px', 
                    color: '#2C4159'
                }
            },
            pointPadding: 0
        }]
    });
      
    //Line Charts 
    google.charts.load('current', {
      packages: ['corechart', 'line']
    });
    google.charts.setOnLoadCallback(drawLineStyles);

    function drawLineStyles() {
      var data = google.visualization.arrayToDataTable([
        ['Year', 'ACTUAL', 'PROJECTED', ],
        <?php
        if (!empty($projectedActual)) {
          foreach ($projectedActual as $value) { ?>
            ['<?= $value['monthYear'] ?>', <?= number_format((float)$value['actual']/1000000, 1, '.', '') ?>, <?= number_format((float)$value['projected']/1000000, 1, '.', '') ?>],
        <?php }
        } ?>
      ]);

      var options = {
        //            hAxis: {
        //                title: 'PROJECTED ACTUAL'
        //            },
        vAxis: {
            viewWindow: {min:0},
            title: 'No. of Boxes (in Millions)'
        },
        colors: ['#B6D7A8', '#FFE59A'], 
        pointsVisible: true,
        pointRadius: 50,
        series: {
          0: {
            lineWidth: 5,
            lineDashStyle: [8, 5, 8, 5],
            //curveType: 'function'
          },
          1: {
            lineWidth: 5,
            lineDashStyle: [7, 2, 4, 3],
            //curveType: 'function'
          }
        },
        //'width': 950,
        //'height': 400,
        legend: 'bottom'
      }; 
      
      var chart = new google.visualization.LineChart(document.getElementById('lineChart'));
      chart.draw(data, options); 
    }


    //if brand is one then not create total progress graph
    <?php if($brandLenght > 1 || $brandLenght ==0 ){ ?>
    //Donutchart Charts 
    google.charts.load("current", {
      packages: ["corechart"]
    });
    google.charts.setOnLoadCallback(drawDonutchart);

    function drawDonutchart() {
      var total = <?= $completedTotal ?> + <?= $inprogressTotal ?>;
      var hold_total = (<?= $completedTotal ?> / total)*100;
      var comp_per = hold_total.toFixed(1);
      if(hold_total=='100'){
          var comp_per = hold_total;
      } 
      var completed = 'COMPLETED'+' - '+comp_per+'%';  
      
      var hold_total_1 = (<?= $inprogressTotal ?> / total)*100;
      var inprog_per = hold_total_1.toFixed(1);
      if(hold_total_1=='100'){
          var inprog_per = hold_total_1; 
      }
      var inprogress = 'IN PROGRESS'+' - '+inprog_per+'%'; 
      
      var data = google.visualization.arrayToDataTable([
        ['Task', 'Hours per Day'],
        ['COMPLETED', <?= number_format((float)$completedTotal/1000000, 2, '.', '') ?>], //completed
        ['IN PROGRESS', <?= number_format((float)$inprogressTotal/1000000, 2, '.', '') ?>] //inprogress
      ]); 
      
      var options = { 
        annotations: {
          textStyle: {
            fontSize: 18,
            bold: true,
            italic: true,
          }
        },
        pieSliceTextStyle: {
          color: '#000',
        },
        title: 'Total Progress',
        titlePosition: 'none',
        titleTextStyle: {
          color: '#2C4159', // any HTML string color ('red', '#cc00cc')
          //        fontName: 'Calibri', // i.e. 'Times New Roman'
          fontSize: '20', // 12, 18 whatever you want (don't specify px)
          bold: true, // true or false
          italic: false // true of false
        },
        colors: ['#B6D7A8', '#FFE59A'],
        legend: 'bottom',
        //width: 400,
        height: 415,
        pieHole: 0.5, 
        tooltip : {
           // trigger: 'none',
            //text: 'value'
        },

      };

      var chart = new google.visualization.PieChart(document.getElementById('contract'));
      var total_cont = total/1000000;
      jQuery('#contract_cnt').text(total_cont.toFixed(2)+'M');
      chart.draw(data, options);
    }
    

    //Contract size chart
    <?php
    $cnt = 0;
    if (!empty($contractSizeProjected)) {
      foreach ($contractSizeProjected as $key => $value) {
        if (!empty($value)) {
          $cnt++;
          $actual = 0;
          if (isset($contractSizeActual[$key])) {
            $actual = $contractSizeActual[$key];
          }
          $inprogress = $value - $actual;
    ?>
          google.charts.load("current", {
            packages: ["corechart"]
          });
          google.charts.setOnLoadCallback(drawContractSize<?= $cnt ?>);
          
          function drawContractSize<?= $cnt ?>() { 
                var total = <?= $actual ?> + <?= $inprogress ?>;
                var hold_total = (<?= $actual ?> / total)*100;
                var comp_per = hold_total.toFixed(1);
                if(hold_total=='100'){
                    var comp_per = hold_total;
                } 
                var completed = 'COMPLETED'+' - '+comp_per+'%';  

                var hold_total_1 = (<?= $inprogress ?> / total)*100;
                var inprog_per = hold_total_1.toFixed(1);
                if(hold_total_1=='100'){
                    var inprog_per = hold_total_1; 
                }
                var inprogress = 'IN PROGRESS'+' - '+inprog_per+'%';  
                
                var data = google.visualization.arrayToDataTable([
                  ['Task', 'Hours per Day'],
                  ['COMPLETED', <?= number_format((float)$actual/1000000, 2, '.', '') ?>],
                  ['IN PROGRESS', <?= number_format((float)$inprogress/1000000, 2, '.', '') ?>]
                ]);  
                
            var options = {
              annotations: {
                textStyle: {
                  //                fontName: 'Calibri',
                  fontSize: 18,
                  bold: true,
                  italic: true,
                }
              },
              pieHole: 0.3,
              pieSliceTextStyle: {
                color: '#000',
                fontSize: 10
              },
              title: '<?= strtoupper($key) ?>',
              titlePosition: 'none',
              titleTextStyle: {
                color: '#32475E', // any HTML string color ('red', '#cc00cc')
                //              fontName: 'Calibri', // i.e. 'Times New Roman'
                fontSize: '14', // 12, 18 whatever you want (don't specify px)
                bold: false, // true or false
                italic: false // true of false
              },
              colors: ['#B6D7A8', '#FFE59A'],
              legend: 'bottom',
              //width: 200,
              //height: 200,
            };

            var chart = new google.visualization.PieChart(document.getElementById('contact_<?= $cnt ?>'));
            var total_cont = total/1000000;
            jQuery('#contract_cnt_<?= $cnt ?>').text(total_cont.toFixed(2)+'M');
            chart.draw(data, options);
          }
    <?php }
      }
    } ?>
   <?php } ?>

    //brand wise and brand size chart
    <?php
    $brandcnt = 0;
    if (!empty($brandWiseProjected)) {
      foreach ($brandWiseProjected as $key => $value) {
        $brandcnt++;
        $keyArr = explode("#", $key);
        $brand_id = $keyArr[0];
        $brand_name = $keyArr[1];
        $brandActual = 0;
        if (isset($brandWiseActual[$brand_id])) {
          $brandActual = $brandWiseActual[$brand_id];
        }
        $BrandInprogress = $value - $brandActual;
    ?>
        google.charts.load("current", {
          packages: ["corechart"]
        });
        google.charts.setOnLoadCallback(drawBrandSize<?= $brandcnt ?>);

        function drawBrandSize<?= $brandcnt ?>() {
            var total = <?= $brandActual ?> + <?= $BrandInprogress ?>;
            var hold_total = (<?= $brandActual ?> / total)*100;
            var comp_per = hold_total.toFixed(1);
            if(hold_total=='100'){
                var comp_per = hold_total;
            } 
            var completed = 'COMPLETED'+' - '+comp_per+'%';  

            var hold_total_1 = (<?= $BrandInprogress ?> / total)*100;
            var inprog_per = hold_total_1.toFixed(1);
            if(hold_total_1=='100'){
                var inprog_per = hold_total_1; 
            }
            var inprogress = 'IN PROGRESS'+' - '+inprog_per+'%';  
            
            var data = google.visualization.arrayToDataTable([
              ['Task', 'Hours per Day'],
              ['COMPLETED', <?= number_format((float)$brandActual/1000000, 2, '.', '') ?>],
              ['IN PROGRESS', <?= number_format((float)$BrandInprogress/1000000, 2, '.', '') ?>]
            ]);  

          var options = {
            annotations: {
              textStyle: {
                //              fontName: 'Calibri',
                fontSize: 18,
                bold: true,
                italic: true,
              }
            },
            pieSliceTextStyle: {
              color: '#000',
            },
            title: '<?php //echo strtoupper($brand_name) 
                    ?>',
            titlePosition: 'none',
            titleTextStyle: {
              color: '#2C4159', // any HTML string color ('red', '#cc00cc')
              //            fontName: 'Calibri', // i.e. 'Times New Roman'
              fontSize: '20', // 12, 18 whatever you want (don't specify px)
              bold: true, // true or false
              italic: false // true of false
            },
            colors: ['#B6D7A8', '#FFE59A'],
            legend: 'bottom',
            //width: 400,
            height: 415,
            pieHole: 0.5,
          };

          var chart = new google.visualization.PieChart(document.getElementById('brand_<?= $brandcnt ?>'));
          var total_cont = total/1000000;
          jQuery('#brand_cnt_<?= $brandcnt ?>').text(total_cont.toFixed(2)+'M');
          chart.draw(data, options);
        }
        <?php
        $brandSizecnt = 0;
        if (isset($brandWiseSizeProjected[$brand_id])) {
          foreach ($brandWiseSizeProjected[$brand_id] as $keySize => $valueSize) {
            if (!empty($valueSize)) {
              $brandSizecnt++;
              $keySizeArr = explode("#", $keySize);
              $size_id = $keySizeArr[0];
              $size_name = $keySizeArr[1];

              $idNameSize = 'brand_' . $brandcnt . '_size_' . $brandSizecnt;
              $brandSizeActual = 0;
              if (isset($brandWiseSizeActual[$brand_id][$size_id])) {
                $brandSizeActual = $brandWiseSizeActual[$brand_id][$size_id];
              }
              $BrandSizeInprogress = abs($valueSize - $brandSizeActual);
        ?>
              google.charts.load("current", {
                packages: ["corechart"]
              });
              google.charts.setOnLoadCallback(drawBrandSize<?= $brandcnt . $brandSizecnt ?>);

              function drawBrandSize<?= $brandcnt . $brandSizecnt ?>() {
                var total = <?= $brandSizeActual ?> + <?= $BrandSizeInprogress ?>;
                var hold_total = (<?= $brandSizeActual ?> / total)*100;
                var comp_per = hold_total.toFixed(1);
                if(hold_total=='100'){
                    var comp_per = hold_total;
                } 
                var completed = 'COMPLETED'+' - '+comp_per+'%';  

                var hold_total_1 = (<?= $BrandSizeInprogress ?> / total)*100;
                var inprog_per = hold_total_1.toFixed(1);
                if(hold_total_1=='100'){
                    var inprog_per = hold_total_1; 
                }
                var inprogress = 'IN PROGRESS'+' - '+inprog_per+'%';   
                
                var data = google.visualization.arrayToDataTable([
                  ['Task', 'Hours per Day'],
                  ['COMPLETED', <?= number_format((float)$brandSizeActual/1000000, 2, '.', '') ?>],
                  ['IN PROGRESS', <?= number_format((float)$BrandSizeInprogress/1000000, 2, '.', '') ?>]
                ]);  

                var options = {
                  annotations: {
                    textStyle: {
                      //                    fontName: 'Calibri',
                      fontSize: 18,
                      bold: true,
                      italic: true,
                    }
                  },
                  pieHole: 0.3,
                  pieSliceTextStyle: {
                    color: '#000',
                    fontSize: 10
                  },
                  title: '<?php //echo strtoupper($size_name) 
                          ?>',
                  titlePosition: 'none',
                  titleTextStyle: {
                    color: '#32475E', // any HTML string color ('red', '#cc00cc')
                    //                  fontName: 'Calibri', // i.e. 'Times New Roman'
                    fontSize: '14', // 12, 18 whatever you want (don't specify px)
                    bold: false, // true or false
                    italic: false // true of false
                  },
                  colors: ['#B6D7A8', '#FFE59A'],
                  legend: 'bottom',
                  //width: 200,
                  //height: 200,
                };

                var chart = new google.visualization.PieChart(document.getElementById('<?= $idNameSize ?>'));
                var total_cont = total/1000000;
                jQuery('#cnt_<?= $idNameSize ?>').text(total_cont.toFixed(2)+'M');
                chart.draw(data, options);
              }

        <?php }
          }
        } ?>

    <?php }
    }
    ?>

    //Logistics progress graph
    <?php if (!empty($logisticsContainerStatus)) { ?>
      google.charts.load('current', {
        'packages': ['corechart']
      });
      google.charts.setOnLoadCallback(drawChart1);

      function drawChart1() {
        var data = google.visualization.arrayToDataTable([
          ['x', '', '', '', '', {'role': 'style'},{'type':'string','role':'tooltip'}],
          <?php
          foreach ($logisticsContainerStatusData as $key => $value) {
            $Lvalue = explode('#', $value);
            $previousCnt = $Lvalue[0];
            $actaulCnt = $Lvalue[1];
            if($key=='Projected'){
                ?>
                ['<?= $key.' ('.($actaulCnt-$previousCnt).')' ?>', <?= $previousCnt ?>, <?= $previousCnt ?>, <?= $actaulCnt ?>, <?= $actaulCnt ?>, '{ fill-color: #B6D7A8; }','<?= $key.' ('.($actaulCnt-$previousCnt).')' ?>'],
                <?php
            }else{
                ?>
                ['<?= $key.' ('.($actaulCnt-$previousCnt).')' ?>', <?= $previousCnt ?>, <?= $previousCnt ?>, <?= $actaulCnt ?>, <?= $actaulCnt ?>, '{ fill-color: #FFE59A; }','<?= $key.' ('.($actaulCnt-$previousCnt).')' ?>'],
                <?php 
            }
          }
          ?>
        ], false); 

        var options = {
            legend: 'none',
            bar: {
                groupWidth: '70%'
            }, // Remove space between bars.
            candlestick: {
              fallingColor: {
                strokeWidth: 0,
                fill: '#FFE59A'
              }, // red
              risingColor: {
                strokeWidth: 0,
                fill: '#FFE59A'
              } // green 
            },
            vAxis: {
                title: 'No. of Containers',  
            },  
            
//            tooltip : {
//                trigger: 'none'
//            },
          
          //'width': 960,
          //'height': 400,
        }; 

        var chart = new google.visualization.CandlestickChart(document.getElementById('Logistics'));
        chart.draw(data, options);
      }

    <?php }else{ ?>
        google.charts.load("current", {
        packages: ['corechart']
      });
      google.charts.setOnLoadCallback(drawChart_l);

      function drawChart_l() {
        var data = google.visualization.arrayToDataTable([
          ["Week", "Containers", { role: "style" }],
          <?php
          foreach ($logisticsContainerStatusData as $key => $value) {
          ?>['<?= $key ?>', <?= $value ?>, ""],
          <?php
          }
          ?>
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
          {
            calc: "stringify",
            sourceColumn: 1,
            type: "string",
            role: "annotation"
          },
          2
        ]);
 
        var options = {
            colors: ['#FFE59A'],
            legend: {
              position: 'none'
            },  
            vAxis: { 
                title: 'No. of Containers', 
                viewWindow: {
                    min: 0,
                    max: 12
                },
                ticks: [0, 2, 4, 6, 8, 10, 12]   
            },    
            //          width: 940,
            //          height: 400,
            bar: {
              groupWidth: "50%"
            },

        };
        var chart = new google.visualization.ColumnChart(document.getElementById("Logistics"));
        chart.draw(view, options);
      }
    <?php } ?>
 

    //upcoming delivery at port graph 
    <?php if (!empty($deliveryAtPortData)) { ?>
      google.charts.load("current", {
        packages: ['corechart']
      });
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ["Week", "Containers", { role: "style" }],
          <?php
          foreach ($deliveryAtPortData as $key => $value) {
          ?>['<?= $key ?>', <?= $value ?>, ""],
          <?php
          }
          ?>
        ]);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
          {
            calc: "stringify",
            sourceColumn: 1,
            type: "string",
            role: "annotation"
          },
          2
        ]);
 
        var options = {
            colors: ['#FFE59A'],
            legend: {
              position: 'none'
            },  
            vAxis: { 
                title: 'No. of Containers',
                <?php if(empty($deliveryAtPort)){ ?>
                //minValue: 0, maxValue: 10
                viewWindow: {
                    min: 0,
                    max: 12
                },
                ticks: [0, 2, 4, 6, 8, 10, 12]  
                <?php } ?>
            },    
            //          width: 940,
            //          height: 400,
            bar: {
              groupWidth: "50%"
            },

        };
        var chart = new google.visualization.ColumnChart(document.getElementById("upcomingDeliPort"));
        chart.draw(view, options);
      }

    <?php } ?>

    //Voyage Completion graph
    <?php if (!empty($voyageCompletionDataData)) { ?>
      google.charts.load("current", {
        packages: ['corechart', 'bar']
      }); //corechart
      google.charts.setOnLoadCallback(drawChartXLine);

      function drawChartXLine() {
        var data = google.visualization.arrayToDataTable([
            ['Opening Move', '', {role: 'style'}],
            <?php
            foreach ($voyageCompletionDataData as $key => $value) {
            ?>
                ['<?= $key ?>', <?= $value ?>, 'stroke-color: #B6D7A8; stroke-width: 1; fill-color: #B6D7A8; fill-opacity: 0.9;stroke-dasharray:5'],
            <?php
            }
            ?> 
        ]);
        
//        var data = google.visualization.arrayToDataTable([
//		['City', '2010 Population',{role: 'style'}],
//		['New York City, NY', 100, 'stroke-color: #B6D7A8; stroke-width: 1; fill-color: #B6D7A8; fill-opacity: 0.8;stroke-dasharray:5'],
//		['Los Angeles, CA', 80, 'stroke-color: #B6D7A8; stroke-width: 1; fill-color: #B6D7A8; fill-opacity: 0.8;stroke-dasharray:5'],
//		['Chicago, IL', 65, 'stroke-color: #B6D7A8; stroke-width: 1; fill-color: #B6D7A8; fill-opacity: 0.8;stroke-dasharray:5'],
//		['Houston, TX', 40, 'stroke-color: #B6D7A8; stroke-width: 1; fill-color: #B6D7A8; fill-opacity: 0.8;stroke-dasharray:5'],
//		['Philadelphia, PA', 10, 'stroke-color: #B6D7A8; stroke-width: 1; fill-color: #B6D7A8; fill-opacity: 0.8;stroke-dasharray:5']
//	]);
        
        var formatter = new google.visualization.NumberFormat({pattern: '#\'%\''}); 
        formatter.format(data, 1);
        
        var view = new google.visualization.DataView(data);
            view.setColumns([0, 1,
            {
                calc: "stringify",
                sourceColumn: 1,
                type: "string",
                role: "annotation"
            },
            2]);
        var options = {
            //    title: "Density of Precious Metals, in g/cm^3",
            //            width: 600,
            //height: 400,
             bars: 'vertical', // Required for Material Bar Charts.
          axes: {
            x: {
              0: { side: 'bottom'} // Top x-axis.
            }
          },
            bar: {
              groupWidth: "30%"
            },
            legend: {
              position: "none"
            }, 
            //isStacked: 'percent',
            hAxis: {
                minValue: 0, maxValue: 100, format: '#\'%\''
            },
            vAxis: { 
                minValue: 0, maxValue: 100, format: '#\'%\''
            }
        };

        var chart = new google.visualization.BarChart(document.getElementById("voyageCompletion"));
        chart.draw(view, options);

      }

    <?php } ?>
  </script>