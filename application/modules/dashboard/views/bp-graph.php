<div class="main-panel">
    <div class="container">
        <!--<div class="page-inner">
                <div class="page-header page-header-btn">
                    <div class="page-header-title">
                        <h1 class="page-title"><a href="<?php echo base_url("dashboard"); ?>"></a> Dashboard</h1>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                    </div>
                </div>
            </div>-->
        <div class="dashboard-wrapper">
            <div class="dashboard-title-wrapper">
                <h1 class="dashboard-title">Choose Business Partner</h1>
                <h4 class="dashboard-subtitle">Reports will be visible once you have made your selection</h4>
            </div>
            <div class="dashboard-select-wrapper"> 
                <div class="form-group"> 
                    <select name="m_contract" id="m_contract" onchange="getGraphSection();" class="searchInput basic-single w100"> 
                        <option value="" disabled selected hidden>MContract NickName-MContract#</option>
                        <?php foreach ($orderData as $value) { ?>
                        <option value="<?=$value['order_id']?>"><?=!empty($value['nick_name'])?$value['nick_name'].'-'.$value['contract_number']:$value['contract_number']?></option>
                        <?php } ?>
                    </select>
                </div>
                 <div class="form-group"> 
                    <button type="reset"  onclick="refresh()"  class="select-large btn-primary-mro">
                        Clear Search
                    </button>
                </div> 
            </div> 
        </div>
        <div class="dashboard-wrapper" id="graph-section">

        </div>
    </div>
</div>

<script>  
    //get graph section data
    function getGraphSection() {  
        var m_contract_id = $("#m_contract").val(); 
        if(m_contract_id >0){
            $.ajax({
                url: "<?= base_url('dashboard/getGraphSection_bp') ?>",
                type: "POST",
                dataType: "json",
                data: {m_contract_id},
                success: function (response) {
                    if (response.success) {
                        $("#graph-section").html(response.graphSectionDiv);
                    } else {
                        $("#graph-section").html(response.graphSectionDiv);
                    }
                }
            });
        }else{
            $("#graph-section").html('');
        }
    }
    
    $( document ).ready(function() { 
        getGraphSection();
    }); 
    
    function refresh() {
        location.reload();
    }
</script>