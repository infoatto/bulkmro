<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Search extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Searchmodel', 'searchmodel', TRUE); 
    }

    function getSearchRecord() {
        $searchInput = trim($_GET['searchInput']);
        $str = ''; 
        //search containers
        if ($this->privilegeduser->hasPrivilege("ContainersDetailsView")) { 
            //condition for Customer and Supplier roles user 
            $containerIdsStr = ''; 
            if ($_SESSION["mro_session"]['user_role'] == 'Customer' || $_SESSION["mro_session"]['user_role'] == 'Supplier'){ 
                $condition = " bp.business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
                $main_table = array("tbl_container as c", array('c.container_id'));
                $join_tables = array(
                    array("left", "tbl_order as o", "o.order_id = c.order_id", array()),
                    array("left", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id OR bpo.bp_order_id = o.supplier_contract_id", array()), 
                    array("left", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array()), 
                );
                $rs = $this->searchmodel->JoinFetch($main_table, $join_tables, $condition);
                $containerIdsData = $this->searchmodel->MySqlFetchRow($rs, "array"); // fetch result  
                if (!empty($containerIdsData)) { 
                    //hold container ids
                    foreach ($containerIdsData as $value) {
                        if (!empty($containerIdsStr)) {
                            $containerIdsStr = $containerIdsStr . ',' . $value['container_id'];
                        } else {
                            $containerIdsStr = $value['container_id'];
                        } 
                    }
                } 
            } 
            $where = '';
            if(!empty($containerIdsStr)){
                $where = ' AND container_id IN ('.$containerIdsStr.') ';
            } 
            
            $condition = " status = 'Active' AND container_number LIKE '%$searchInput%'  $where ";
            $containerData = $this->searchmodel->getDataLimit('tbl_container', 'container_id,container_number', $condition, '', '', '10'); 
            if (!empty($containerData)) {
                foreach ($containerData as $value) {
                    $url = base_url() . "container_details?text=" . rtrim(strtr(base64_encode("id=" . $value['container_id']), "+/", "-_"), "=");
                    $str .= '<a href="' . $url . '"><span class="module-name">Container Detail : ' . $value['container_number'] . '</span></a>';
                }
            }
        }
        //search master contracts
        if ($this->privilegeduser->hasPrivilege("MasterContractView")) { 
            //condition for Customer and Supplier roles user 
            $contractIdsStr = ''; 
            if ($_SESSION["mro_session"]['user_role'] == 'Customer' || $_SESSION["mro_session"]['user_role'] == 'Supplier'){ 
                $main_table = array("tbl_order as o", array('o.order_id')); 
                $condition = " o.status = 'Active' AND bp.business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
                $join_tables = array(
                    array("", "tbl_bp_order as bpo", "bpo.bp_order_id = o.customer_contract_id OR bpo.bp_order_id = o.supplier_contract_id", array()),
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array()),
                );
                $rs = $this->searchmodel->JoinFetch($main_table, $join_tables, $condition);
                $orderIdsData = $this->searchmodel->MySqlFetchRow($rs, "array"); // fetch result   
                if (!empty($orderIdsData)) {  
                    foreach ($orderIdsData as $value) {
                        if (!empty($contractIdsStr)) {
                            $contractIdsStr = $contractIdsStr . ',' . $value['order_id'];
                        } else {
                            $contractIdsStr = $value['order_id'];
                        } 
                    }
                } 
            } 
            $where = '';
            if(!empty($contractIdsStr)){
                $where = ' AND order_id IN ('.$contractIdsStr.') ';
            } 
            
            $condition = " status = 'Active' AND contract_number LIKE '%$searchInput%'  $where ";
            $contractData = $this->searchmodel->getDataLimit('tbl_order', 'order_id,contract_number', $condition, '', '', '10'); 
            if (!empty($contractData)) {
                foreach ($contractData as $value) {
                    $url = base_url() . "order/addEdit?text=" . rtrim(strtr(base64_encode("id=" . $value['order_id']), "+/", "-_"), "=") . "&view=1";
                    $str .= '<a href="' . $url . '"><span class="module-name">Master Contract : ' . $value['contract_number'] . '</span></a>';
                }
            }
        }
        //search BP contract
        if ($this->privilegeduser->hasPrivilege("BusinessPartnersOrderView")) {
            //condition for Customer and Supplier roles user 
            $bpContractIdsStr = '';
            if ($_SESSION["mro_session"]['user_role'] == 'Customer' || $_SESSION["mro_session"]['user_role'] == 'Supplier'){  
                $condition = " bpo.status = 'Active' AND bp.business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
                $main_table = array("tbl_bp_order as bpo", array('bpo.bp_order_id'));
                $join_tables = array(
                    array("", "tbl_business_partner as bp", "bp.business_partner_id = bpo.business_partner_id", array()),
                );
                $rs = $this->searchmodel->JoinFetch($main_table, $join_tables, $condition); 
                $bpContractIdsData = $this->searchmodel->MySqlFetchRow($rs, "array"); // fetch result  
                if (!empty($bpContractIdsData)) {  
                    foreach ($bpContractIdsData as $value) {
                        if (!empty($bpContractIdsStr)) {
                            $bpContractIdsStr = $bpContractIdsStr . ',' . $value['bp_order_id'];
                        } else {
                            $bpContractIdsStr = $value['bp_order_id'];
                        } 
                    }
                }  
            } 
            $where = '';
            if(!empty($bpContractIdsStr)){
                $where = ' AND bp_order_id IN ('.$bpContractIdsStr.') ';
            }  
            
            $condition = " status = 'Active' AND contract_number LIKE '%$searchInput%'  $where ";
            $bpContractData = $this->searchmodel->getDataLimit('tbl_bp_order', 'bp_order_id,contract_number', $condition, '', '', '10'); 
            if (!empty($bpContractData)) {
                foreach ($bpContractData as $value) {
                    $url = base_url() . "bporder/addEdit?text=" . rtrim(strtr(base64_encode("id=" . $value['bp_order_id']), "+/", "-_"), "=") . "&view=1";
                    $str .= '<a href="' . $url . '"><span class="module-name">Business Partner Contract : ' . $value['contract_number'] . '</span></a>';
                }
            }
        } 
        //search BP
        if ($this->privilegeduser->hasPrivilege("BusinessPartnersView")) {
            //condition for Customer and Supplier roles user 
            $where = '';
            if ($_SESSION["mro_session"]['user_role'] == 'Customer' || $_SESSION["mro_session"]['user_role'] == 'Supplier'){                  
                $where = "  AND business_partner_id = " . (!empty($_SESSION["mro_session"][0]['business_partner_id']) ? $_SESSION["mro_session"][0]['business_partner_id'] : 0);
            }  
            
            $condition = " status = 'Active' AND alias LIKE '%$searchInput%'  $where ";
            $bpData = $this->searchmodel->getDataLimit('tbl_business_partner', 'business_partner_id,alias', $condition, '', '', '10');
            //echo $this->db->last_query();exit;
            if (!empty($bpData)) {
                foreach ($bpData as $value) {
                    $url = base_url() . "business_partner/addEdit?text=" . rtrim(strtr(base64_encode("id=" . $value['business_partner_id']), "+/", "-_"), "=") . "&view=1";
                    $str .= '<a href="' . $url . '"><span class="module-name">Business Partner : ' . $value['alias'] . '</span></a>';
                }
            }
        }
        echo json_encode(array('str' => $str));
        exit;
    }
    
    function updateContainerStatusCron() { 
        //container status to Goods Received in port.
        $result = 0;
        $container_id = '';
        $todayDate = date("Y-m-d");
        $condition = " DATE(eta) < '".$todayDate."' "; 
        $containerETAData = $this->searchmodel->getData('tbl_container','container_id,eta,last_container_status_id',$condition); 
        if (!empty($containerETAData)) { 
            foreach ($containerETAData as $value) {     
                $container_id = $value['container_id'];  
                $last_container_status_id = $value['last_container_status_id']; 
                if(!empty($last_container_status_id)){
                    $condition = " container_status_id = ".$last_container_status_id." "; 
                    $statusSquenceData = $this->searchmodel->getData('tbl_container_status','status_squence',$condition);  
                    $statusSquence = $statusSquenceData[0]['status_squence'];
                    //$containerStatusLogData = $this->searchmodel->getContainerStatusUpdate($container_id); 
                    //If ETA is before Today () then auto change Status to Goods Received in port. 
                    if($statusSquence < 7){
                        $ContainerStatus =array();
                        $ContainerStatus['container_id'] = $container_id;
                        $ContainerStatus['container_status_id'] = 7;
                        $ContainerStatus['current_session_id'] = 'System';
                        $ContainerStatus['created_on'] = date("Y-m-d H:i:s");
                        $ContainerStatus['document_date'] = date("Y-m-d"); 
                        $result = $this->searchmodel->insertData("tbl_container_status_log", $ContainerStatus, "1");
                        if($result){
                            $containerData = array();
                            $containerData['last_container_status_id'] = 7; 
                            $containerData['last_status_by'] = 0;
                            $containerData['last_status_date'] = date('Y-m-d');
                            $result1 = $this->searchmodel->updateData("tbl_container", $containerData, array("container_id" => $container_id)); 
                        } 
                    }
                } 
            } 
        }
        if($result>0){
            echo 'Status Updated';
        }else{
            echo 'Status Not Updated';
        }
         
    }

}

?>
