<?PHP

class Countrymodel extends CI_Model {

    function getRecords($get) {
        //get country record at list
        $table = "tbl_country";
        $default_sort_column = 'c.country_id';
        $default_sort_order = 'desc';
        $condition = "1=1 ";
        $sortArray = array('c.country_name');

        $page = $get['iDisplayStart']; // iDisplayStart starting offset of limit funciton
        $rows = $get['iDisplayLength']; // iDisplayLength no of records from the offset
        // sort order by column
        $sort = isset($get['iSortCol_0']) ? strval($sortArray[$get['iSortCol_0']]) : $default_sort_column;
        $order = isset($get['sSortDir_0']) ? strval($get['sSortDir_0']) : $default_sort_order;

        if (isset($_GET['Searchkey_0']) && $_GET['Searchkey_0'] != "") {
            $condition .= " AND c.country_name LIKE '%" . $_GET['Searchkey_0'] . "%' ";
        }

        $this->db->select('c.*');
        $this->db->from("$table as c"); 
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $this->db->limit($rows, $page);
        $query = $this->db->get();
        //echo $this->db->last_query();exit; 

        $this->db->select('c.*');
        $this->db->from("$table as c"); 
        $this->db->where("($condition)");
        $this->db->order_by($sort, $order);
        $query1 = $this->db->get();

        if ($query->num_rows() > 0) {
            $totcount = $query1->num_rows();
            return array("query_result" => $query->result(), "totalRecords" => $totcount);
        } else {
            return array("totalRecords" => 0);
        }
    }

}

?>
