<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left"> 
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("country"); ?>">Country List</a>
                                <span>></span>
                                <p>Add Country</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add Country</h1>
                            </div>
                        </div> 
                        <button class="btn-primary-mro">Save</button>
                    </div>

                    <div class="page-content-wrapper">  
                        <input type="hidden" name="country_id" id="country_id" value="<?php echo(!empty($country_details['country_id'])) ? $country_details['country_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="Country Name">Country Name</label>
                                    <input type="text" name="country_name" id="country_name" class="input-form-mro" value="<?php echo(!empty($country_details['country_name'])) ? $country_details['country_name'] : ""; ?>">
                                </div> 
                            </div>
                            <div class="form-row form-row-4">
                                <div class="form-group">
                                    <label for="">Multiselect List</label>
                                    <div class="multiselect-dropdown-wrapper">
                                        <div class="md-value">
                                            Select value
                                        </div>
                                        <div class="md-list-wrapper">
                                            <input type="text" placeholder="Search" class="md-search">
                                            <div class="md-list-items">
                                                <?php
                                                foreach ($details as $key => $value) {
                                                    ?>
                                                    <div class="mdli-single">
                                                        <label class="container-checkbox"><?= $value['country_name'] ?>
                                                            <input type="checkbox" name="ids[]" value="<?= $value['country_id'] ?>">
                                                            <span class="checkmark-checkbox"></span>
                                                        </label>
                                                    </div>
                                                    <?php
                                                }
                                                ?> 
                                            </div>
                                            <div class="md-cta">
                                                <a href="#/" class="btn-primary-mro md-done">Done</a>
                                                <a href="#/" class="btn-secondary-mro md-clear">Clear All</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Multiselect List with Add New</label>
                                    <div class="multiselect-dropdown-wrapper">
                                        <div class="md-value">
                                            Select value
                                        </div>
                                        <div class="md-list-wrapper">
                                            <input type="text" placeholder="Search" class="md-search">
                                            <a href="#/" class="btn-secondary-mro md-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
                                            <div class="md-list-items">
                                                <div class="mdli-single">
                                                    <label class="container-checkbox">Listed Item 1
                                                        <input type="checkbox">
                                                        <span class="checkmark-checkbox"></span>
                                                    </label>
                                                </div>
                                                <div class="mdli-single">
                                                    <label class="container-checkbox">Listed Item 2
                                                        <input type="checkbox">
                                                        <span class="checkmark-checkbox"></span>
                                                    </label>
                                                </div>
                                                <div class="mdli-single">
                                                    <label class="container-checkbox">Listed Item 3
                                                        <input type="checkbox">
                                                        <span class="checkmark-checkbox"></span>
                                                    </label>
                                                </div>
                                                <div class="mdli-single">
                                                    <label class="container-checkbox">Listed Item 4
                                                        <input type="checkbox">
                                                        <span class="checkmark-checkbox"></span>
                                                    </label>
                                                </div>
                                                <div class="mdli-single">
                                                    <label class="container-checkbox">Listed Item 5
                                                        <input type="checkbox">
                                                        <span class="checkmark-checkbox"></span>
                                                    </label>
                                                </div>
                                                <div class="mdli-single">
                                                    <label class="container-checkbox">Listed Item 6
                                                        <input type="checkbox">
                                                        <span class="checkmark-checkbox"></span>
                                                    </label>
                                                </div>
                                                <div class="mdli-single">
                                                    <label class="container-checkbox">Listed Item 7
                                                        <input type="checkbox">
                                                        <span class="checkmark-checkbox"></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="md-cta">
                                                <a href="#/" class="btn-primary-mro md-done">Done</a>
                                                <a href="#/" class="btn-secondary-mro md-clear">Clear All</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Single Select List</label>
                                    <div class="singleselect-dropdown-wrapper">
                                        <div class="sd-value">
                                            Select value
                                        </div>
                                        <div class="sd-list-wrapper">
                                            <input type="text" placeholder="Search" class="sd-search">
                                            <div class="sd-list-items">
                                                <div class="sdli-single">
                                                    <label>Select Value</label>
                                                </div>
                                                <input type="hidden" name="single_id"  id="single_id" value="">
                                                <?php foreach ($details as $key => $value) { ?> 
                                                    <div class="sdli-single" onclick="getvalue('<?= $value['country_id'] ?>')">
                                                        <label id="country_<?= $value['country_id'] ?>"><?= $value['country_name'] ?></label>
                                                    </div>
                                                    <?php
                                                }
                                                ?>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">Single Select List with Add New</label>
                                    <div class="singleselect-dropdown-wrapper">
                                        <div class="sd-value">
                                            Select value
                                        </div>
                                        <div class="sd-list-wrapper">
                                            <input type="text" placeholder="Search" class="sd-search">
                                            <div class="sd-list-items">
                                                <a href="#/" class="btn-secondary-mro sd-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
                                                <div class="sdli-single">
                                                    <label>Select Value</label>
                                                </div>
                                                <div class="sdli-single">
                                                    <label>Listed Item 1</label>
                                                </div>
                                                <div class="sdli-single">
                                                    <label>Listed Item 2 </label>
                                                </div>
                                                <div class="sdli-single">
                                                    <label>Listed Item 3 </label>
                                                </div>
                                                <div class="sdli-single">
                                                    <label>Listed Item 4 </label>
                                                </div>
                                                <div class="sdli-single">
                                                    <label>Listed Item 5 </label>
                                                </div>
                                                <div class="sdli-single">
                                                    <label>Listed Item 6 </label>
                                                </div>
                                                <div class="sdli-single">
                                                    <label>Listed Item 7 </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {

        var vRules = {
            "country_name": {required: true}
        };
        var vMessages = {
            "country_name": {required: "Please enter Country Name."}
        };
        //check and save country
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>country/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {
                        $(".btn-primary-mro").show();
                        console.log(response);
                        if (response.success) {
                            alert(response.msg);
                            setTimeout(function () {
                                window.location = "<?= base_url('country') ?>";
                            }, 1000);
                        } else {
                            alert(response.msg);
                        }
                    }
                });
            }
        });

    });

    function getvalue(val) {
        if (val) {
            $("#single_id").val(val);
            var test = $("#country_" + val).text();
            $('.sd-value').html(test);
        } else {
            alert("select the value first");
        }
    }
    <?php if ($country_details['country_id']) { ?>
            getvalue('<?= $country_details['country_id'] ?>');
    <?php } ?>

</script>