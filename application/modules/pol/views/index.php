<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                            <span>></span>
                            <p>All POL</p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title"><a href="<?php echo base_url("pol"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All POL</h1>
                        </div>
                    </div>
                    <div class="title-sec-right">
                        <?php if ($this->privilegeduser->hasPrivilege("POLAddEdit")) { ?>
                            <a href="<?php echo base_url("pol/addEdit"); ?>" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> Add POL</a>                            
                        <?php } ?> 
                    </div>
                </div>
                 
                <div class="page-content-wrapper1">
                    <div id="serchfilter" class="filter-sec-wrapper">
                        <div class="form-group filter-search dataTables_filter searchFilterClass">
                            <input type="text" id="sSearch_0" name="sSearch_0" class="searchInput filter-search-input" placeholder="POL">
                        </div>
                        <div class="form-group clear-search-filter">
                            <button class="btn-primary-mro" onclick="clearSearchFilters();">Clear Search</button>
                        </div>
                    </div>
                </div>
                <div class="bp-list-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>POL</th>
                                    <th>Status</th>
                                    <th class="table-action-cls" style="width:50px !important;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
 