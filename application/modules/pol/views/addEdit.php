<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("pol"); ?>">POL List</a>
                                <span>></span>
                                <p>Add POL</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add POL</h1>
                            </div>
                        </div> 
                        <div class="title-sec-right"> 
                            <button type="submit" class="btn-primary-mro">Save</button>
                            <a href="<?php echo base_url("pol"); ?>" class="btn-transparent-mro cancel">Cancel</a> 
                        </div> 
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="pol_id" id="pol_id" value="<?php echo(!empty($pol_details['pol_id'])) ? $pol_details['pol_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="CategoryName">POL<sup>*</sup></label>
                                    <input type="text" name="pol_name" id="pol_name" class="input-form-mro" value="<?php echo(!empty($pol_details['pol_name'])) ? $pol_details['pol_name'] : ""; ?>">
                                </div> 
                                <div class="form-group">
                                    <label for="Status">Status</label>
                                    <select name="status" id="status" class="select-form-mro">
                                        <option value="Active" <?php echo(!empty($pol_details['status']) && $pol_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                        <option value="In-active" <?php echo(!empty($pol_details['status']) && $pol_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                    </select>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {
        var vRules = {
            "pol_name": {required: true}
        };
        var vMessages = {
            "pol_name": {required: "Please enter pol name."}
        };
        //check and save pol
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>pol/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('pol') ?>";
                            }, 1000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });
</script>