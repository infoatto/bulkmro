<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

//session_start(); //we need to call PHP's session object to access it through CI
class Dashboard_old extends CI_Controller {

    function __construct() {
        parent::__construct(); 
        checklogin();
    }

    function index() {
        $data = array(); 
        $this->load->view('template/head.php');
        $this->load->view('template/navigation.php');  
        $this->load->view('dashboard_old/index', $data); 
        $this->load->view('template/footer.php');
        $this->load->view('template/footer-scripts.php');
    } 
    
     public function logOut() {
        $this->session->unset_userdata('mro_session');
        redirect(base_url());
    }
}

?>
