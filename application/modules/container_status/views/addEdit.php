<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("container_status"); ?>">Container Status List</a>
                                <span>></span>
                                <p>Add Container Status</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add Container Status</h1>
                            </div>
                        </div>    
                        <div class="title-sec-right"> 
                            <button type="submit" class="btn-primary-mro">Save</button>
                            <a href="<?php echo base_url("container_status"); ?>" class="btn-transparent-mro cancel">Cancel</a> 
                        </div>  
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="container_status_id" id="container_status_id" value="<?php echo(!empty($container_status_details['container_status_id'])) ? $container_status_details['container_status_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="SizeName">Container Status Name<sup>*</sup></label>
                                    <input type="text" name="container_status_name" id="container_status_name" class="input-form-mro" value="<?php echo(!empty($container_status_details['container_status_name'])) ? $container_status_details['container_status_name'] : ""; ?>">
                                </div> 
                                <div class="form-group">
                                    <label for="Status">Status</label>
                                    <select name="status" id="status" class="select-form-mro">
                                        <option value="Active" <?php echo(!empty($container_status_details['status']) && $container_status_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                        <option value="In-active" <?php echo(!empty($container_status_details['status']) && $container_status_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="document_type">Document Type</label>
                                    <select name="document_type_id" id="document_type_id" class="basic-single select-form-mro"> 
                                        <option value="0">Select Document Type</option>
                                        <?php
                                        foreach ($documentType as $value) {
                                            $documentTypeID = (isset($container_status_details['document_type_id']) ? $container_status_details['document_type_id'] : 0);
                                            ?> 
                                            <option value="<?php echo $value['document_type_id']; ?>"  <?php
                                                if ($value['document_type_id'] == $documentTypeID) {
                                                    echo "selected";
                                                }
                                                ?>>
                                                <?php echo $value['document_type_name']; ?> 
                                            </option> 
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div> 
                            </div>  
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {
        var vRules = {
            "container_status_name": {required: true}
        };
        var vMessages = {
            "container_status_name": {required: "Please enter container status name."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>container_status/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {  
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('container_status') ?>";
                            }, 3000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });
</script>