<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="title-sec-wrapper">
                    <div class="title-sec-left">
                        <div class="breadcrumb">
                            <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                            <span>></span>
                            <p>All Users</p>
                        </div>
                        <div class="page-title-wrapper">
                            <h1 class="page-title"><a href="<?php echo base_url("users"); ?>" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Users</h1>
                        </div>
                    </div>
                    <div class="title-sec-right">
                        <?php if ($this->privilegeduser->hasPrivilege("UsersAddEdit")) { ?>
                            <a href="<?php echo base_url("users/addEdit"); ?>" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> Add Users</a>
                        <?php } ?>
                    </div>
                </div>
                <div class="page-content-wrapper1">
                    <div id="serchfilter" class="filter-sec-wrapper filter-users">
                        <div class="form-row form-row-4">
                            <div class="form-group dataTables_filter searchFilterClass">
                                <input type="text" id="sSearch_0" name="sSearch_0" class="searchInput filter-search-field" placeholder="First Name">
                            </div>
                            <div class="form-group dataTables_filter searchFilterClass">
                                <input type="text" id="sSearch_1" name="sSearch_1" class="searchInput filter-search-field" placeholder="Last Name">
                            </div>
                            <div class="form-group dataTables_filter searchFilterClass"> 
                                <select name="sSearch_2" id="sSearch_2" class="searchInput basic-single select-mro" style="width: 100%">
                                    <option value="">All Roles</option>
                                    <?php if(!empty($roles_details)){  ?>
                                        <?php foreach($roles_details as $val){  
                                            ?>
                                            <option value="<?php echo $val['role_name'];?>"><?php echo $val['role_name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>  
                            </div>
                            <div class="form-group dataTables_filter searchFilterClass">
                                <input type="text" id="sSearch_3" name="sSearch_3" class="searchInput filter-search-field" placeholder="Email Id">
                            </div>
                        </div>
                        <div class="form-row form-row-4">
                            <div class="form-group dataTables_filter searchFilterClass">
                                <input type="text" id="sSearch_4" name="sSearch_4" class="searchInput filter-search-field" placeholder="Mobile No.">
                            </div>
                            <div class="form-group dataTables_filter searchFilterClass">
                                <select name="sSearch_5" id="sSearch_5" class="searchInput filter-search-select">
                                    <option value="">All Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                            <div class="form-group dataTables_filter searchFilterClass">
                                <select name="sSearch_6" id="sSearch_6" class="searchInput filter-search-select">
                                    <option value="">All Status</option>
                                    <option value="Active">Active</option>
                                    <option value="In-active">In-active</option>
                                </select>
                            </div>
                            <div class="form-group clear-search-filter">
                                <button class="btn-primary-mro" onclick="clearSearchFilters();">Clear Search</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bp-list-wrapper">
                    <div class="table-responsive">
                        <table class="table table-striped basic-datatables dynamicTable text-left" cellpadding="0" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>User Role</th>
                                    <th>Business Partner</th>
                                    <th>Email Id</th>
                                    <th>Mobile No.</th>
                                    <th>Status</th>
                                    <th class="table-action-cls" style="width:50px !important;">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    function changeStatusNew() {
        return false;
    }
</script>