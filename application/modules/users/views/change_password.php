<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span> 
                                <p>Change Password</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Change Your Password</h1>
                            </div>
                        </div> 
                        <div class="title-sec-right"> 
                            <button type="submit" class="btn-primary-mro">Save</button>
                            <a href="<?php echo base_url("dashboard"); ?>" class="btn-transparent-mro cancel">Cancel</a> 
                        </div>   
                    </div>
                    <div class="page-content-wrapper">  
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="Password">New Password<sup>*</sup></label>
                                    <input type="text" name="password" id="password" placeholder="Enter New Password" class="input-form-mro" required>
                                </div> 
                                <div class="form-group">
                                    <label for="Confirm Password">Confirm Password<sup>*</sup></label>
                                    <input type="password" name="confirm_password" placeholder="Enter Confirm Password" id="confirm_password" class="input-form-mro">
                                </div> 
                            </div>  
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {
        var vRules = {
            "confirm_password": {required: true, equalTo: "#password"}
        };
        var vMessages = {
            "confirm_password": {required: "Please enter Confirm Password", equalTo: "New Password and Confirm Password should same."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>users/submitPassword";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) {                         
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('dashboard') ?>";
                            }, 3000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });
</script>