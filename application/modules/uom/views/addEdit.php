<section class="main-sec">
    <div class="container">
        <div class="row">
            <div class="col-12">         
                <form action="" id="addEditForm" method="post" enctype="multipart/form-data">
                    <div class="title-sec-wrapper">
                        <div class="title-sec-left">
                            <div class="breadcrumb">
                                <a href="<?php echo base_url("dashboard"); ?>">Dashboard</a>
                                <span>></span>
                                <a href="<?php echo base_url("uom"); ?>">UoM List</a>
                                <span>></span>
                                <p>Add UoM</p>
                            </div>
                            <div class="page-title-wrapper">
                                <h1 class="page-title">Add UoM</h1>
                            </div>
                        </div>   
                        <div class="title-sec-right"> 
                            <button type="submit" class="btn-primary-mro">Save</button>
                            <a href="<?php echo base_url("uom"); ?>" class="btn-transparent-mro cancel">Cancel</a> 
                        </div>  
                    </div>
                    <div class="page-content-wrapper">  
                        <input type="hidden" name="uom_id" id="uom_id" value="<?php echo(!empty($uom_details['uom_id'])) ? $uom_details['uom_id'] : ""; ?>">
                        <div class="col-sm-12">
                            <div class="form-row form-row-3">
                                <div class="form-group">
                                    <label for="SizeName">UoM Name<sup>*</sup></label>
                                    <input type="text" name="uom_name" id="uom_name" class="input-form-mro" value="<?php echo(!empty($uom_details['uom_name'])) ? $uom_details['uom_name'] : ""; ?>">
                                </div> 
                                <div class="form-group">
                                    <label for="Status">Status</label>
                                    <select name="status" id="status" class="select-form-mro">
                                        <option value="Active" <?php echo(!empty($uom_details['status']) && $uom_details['status'] == "Active") ? "selected" : ""; ?>>Active</option>
                                        <option value="In-active" <?php echo(!empty($uom_details['status']) && $uom_details['status'] == "In-active") ? "selected" : ""; ?>>In-active</option>
                                    </select>
                                </div>
                            </div>  
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div> 
</section>     

<script>

    $(document).ready(function () {
        var vRules = {
            "uom_name": {required: true}
        };
        var vMessages = {
            "uom_name": {required: "Please enter UoM name."}
        };
        //check and save data
        $("#addEditForm").validate({
            rules: vRules,
            messages: vMessages,
            submitHandler: function (form)
            {
                var act = "<?php echo base_url(); ?>uom/submitForm";
                $("#addEditForm").ajaxSubmit({
                    url: act,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    clearForm: false,
                    beforeSubmit: function (arr, $form, options) {
                        $(".btn-primary-mro").hide();
                    },
                    success: function (response) { 
                        showInsertUpdateMessage(response.msg,response);                        
                        if (response.success) { 
                            setTimeout(function () {                                
                                window.location = "<?= base_url('uom') ?>";
                            }, 3000);
                        }  
                        $(".btn-primary-mro").show(); 
                    }
                });
            }
        });

    });
</script>