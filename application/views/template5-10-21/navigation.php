<?php //echo '<pre>'; print_r($_SESSION);die;  ?>
<header class="bulkmro-nav-container">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="nav-wrapper">
                    <div class="nav-wrapper-left">
                        <a href="<?php echo base_url("dashboard") ?>" class="nav-logo"><img src="<?php echo base_url(); ?>assets/images/BulkMRO_logo.svg" alt=""></a>
                        <div class="nav-divider"></div>
                        <div class="menu-dropdown-wrapper">
                            <a href="#" class="menu-link">
                                Menu
                                <img src="<?php echo base_url(); ?>assets/images/down-menu-home.svg" alt="" class="menu-dropdown-icon">
                            </a>

                            <div class="menu-dropdown">
                                <div class="dropdown-content">
                                    <div class="dropdown-col-single">
                                        <h3>View</h3>
                                        <ul class="menu-links">
                                            <li><a href="<?php echo base_url("dashboard") ?>">Dashboard</a></li>
                                            <?php if ($this->privilegeduser->hasPrivilege("PermissionList")) { ?>
                                                <li><a href="<?php echo base_url("permission") ?>">All Permissions</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("RolesList")) { ?>
                                                <li><a href="<?php echo base_url("roles") ?>">All Roles</a></li> 
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("UsersList")) { ?>
                                                <li><a href="<?php echo base_url("users") ?>">All Users</a></li> 
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("CountryList")) { ?>
                                                <li><a href="<?php echo base_url("country") ?>">All Country</a></li> 
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("StateList")) { ?>
                                                <li><a href="<?php echo base_url("state") ?>">All State</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("CityList")) { ?>
                                                <li><a href="<?php echo base_url("city") ?>">All City</a></li> 
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("CategoryList")) { ?>
                                                <li><a href="<?php echo base_url("category") ?>">All Category</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("BrandList")) { ?>
                                                <li><a href="<?php echo base_url("brand") ?>">All Brand</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("SizeList")) { ?>
                                                <li><a href="<?php echo base_url("size") ?>">All Size</a></li>
                                            <?php } ?>   
                                            <?php if ($this->privilegeduser->hasPrivilege("UoMList")) { ?>
                                                <li><a href="<?php echo base_url("uom") ?>">All UoM</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("PaymentTermsList")) { ?>
                                                <li><a href="<?php echo base_url("payment_term") ?>">All Payment Term</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("IncotermsList")) { ?>
                                                <li><a href="<?php echo base_url("incoterms") ?>">All Incoterms</a></li>
                                            <?php } ?> 
                                            
                                            <li><a href="<?php echo base_url("inventory_summary") ?>">Inventory Summary</a></li>
                                            <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersList")) { ?>
                                                <li><a href="<?php echo base_url("business_partner") ?>">All Business Partners</a></li> 
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("SKUList")) { ?>
                                                <li><a href="<?php echo base_url("sku") ?>">All SKUs</a></li>
                                            <?php } ?> 
                                        </ul>
                                    </div>
                                    <div class="dropdown-col-single">
                                        <h3>Create New</h3>
                                        <ul class="menu-links">
                                            <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersAddEdit")) { ?>
                                                <li><a href="<?php echo base_url("business_partner/addEdit") ?>">Business Partner</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersOrderList")) { ?>
                                                <li><a href="<?php echo base_url("bporder") ?>">Business Partner Order</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("DocumentTypeList")) { ?>
                                                <li><a href="<?php echo base_url("listOfDocument") ?>">Type Of Document </a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("DocumentMasterList")) { ?>
                                                <li><a href="<?php echo base_url("documentMaster") ?>">Document Master</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("MasterContractList")) { ?>
                                                <li><a href="<?php echo base_url("order") ?>">Master Contract</a></li>
                                            <?php } ?> 
                                        </ul>
                                    </div>
                                    <div class="dropdown-col-single">
                                        <h3>View</h3>
                                        <ul class="menu-links">
                                            <li><a href="#">Dashboard</a></li>
                                            <?php if ($this->privilegeduser->hasPrivilege("ContainerStatusList")) { ?>
                                                <li><a href="<?php echo base_url("container_status") ?>">All Container Status</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("ContainersList")) { ?>
                                                <li><a href="<?php echo base_url("container") ?>">All Containers</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("ContainerStatusLog")) { ?>
                                                <li><a href="<?php echo base_url("container_status_log") ?>">All Containers Status Log</a></li> 
                                            <?php } ?>
                                            <li><a href="#">Inventory Summary</a></li> 
                                            <?php if ($this->privilegeduser->hasPrivilege("ContainersDetailsList")) { ?>
                                                <li><a href="<?= base_url('listOfContainer')?>">List of Containers </a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="nav-divider"></div>
                        <div class="menu-search">
                            <form>
                                <input type="text" id="search-admin" placeholder="Search">
                            </form>
                        </div>
                    </div>
                    <div class="nav-wrapper-right">
                        <a href="#"><img src="<?php echo base_url(); ?>assets/images/notification-icon.svg" alt=""></a>
                        <div class="nav-divider"></div>
                        <a href="#" class="btn-secondary-mro"><img src="<?php echo base_url(); ?>assets/images/add-icon-dark.svg" alt="">  New Container</a>
                        <div class="menu-dropdown-wrapper">
                            <a href="#" class="profile-link"> 
                                <?php if (!empty($_SESSION['mro_session'][0]['profile_picture'])) { ?>
                                    <img src="<?php echo FRONT_URL . '/uploads/client_images/' . $_SESSION['mro_session'][0]['profile_picture']; ?>" alt="..." class="avatar-profile">
                                <?php } else { ?>
                                    <img src="<?php echo base_url("assets/images/user-avatar.png") ?>" alt="..." class="avatar-profile">
                                <?php } ?>
                                Profile
                                <img src="<?php echo base_url(); ?>assets/images/down-menu-home.svg" alt="" class="menu-dropdown-icon" style="width:8px;"> 
                            </a>
                            <div class="profile-dropdown">
                                <div class="dropdown-content profile-dropdown-content">
                                    <div class="dropdown-col-single">
                                        <h3>My Profile</h3>
                                        <ul class="menu-links">
                                            <?php $myAccount = 'users/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $_SESSION['mro_session'][0]['user_id']), '+/', '-_'), '=') . ''; ?>                
                                            <li><a href="<?php echo base_url($myAccount) ?>">My Account</a></li>
                                            <li><a href="#">Change Password</a></li>
                                            <li><a href="<?php echo base_url("dashboard/logOut") ?>">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>