<footer>
  <img src="assets/images/graph-pattern.svg" alt="" class="footer-bg img-fluid">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="footer-top-wrapper no-image">          
          <div class="footer-top-portrait">
            <img src="assets/images/portrait-footer.png" alt="">
          </div>
          <div class="footer-top-content">
            <div class="ftc-text-wrapper">
              <h2 class="title-2 title-dark bold">Prepare to profit with GEPL Capital</h2>
              <div class="ftc-text txtm ">
                <p>Your investments are specific to your needs and financial goals. We can help you find the most suitable solution for you.</p>
              </div>
            </div>
            <div class="ftc-cta">
              <a href="#/" class="btn-gepl btn-large btn-gepl-primary">
                <img src="assets/images/whatsapp-icon-white.svg" alt="" class="icon-left-primary"> Message on Whatsapp
              </a>
              <a href="#" class="btn-gepl btn-large btn-gepl-secondary">
                <img src="assets/images/callback-icon-footer.svg" alt="" class="icon-left-secondary">
                Request a call back
              </a>
            </div>
          </div>
        </div>        
      </div>
    </div>
  </div>
</footer>
<!-- <footer>
  <img src="assets/images/graph-pattern.svg" alt="" class="footer-bg img-fluid">
  <div class="footer-top">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="footer-top-wrapper">
            <div class="footer-top-portrait ">
              <img src="assets/images/portrait-footer.png" alt="">
            </div>
            <div class="footer-top-content">
              <div class="ftc-text-wrapper">
                <h2 class="title-2 title-dark bold">Prepare to profit with GEPL Capital</h2>
                <div class="ftc-text txtm ">
                  <p>Your investments are specific to your needs and financial goals. We can help you find the most suitable solution for you.</p>
                </div>
              </div>
              <div class="ftc-cta">
                <a href="#/" class="btn-gepl btn-large btn-gepl-primary">
                  <img src="assets/images/whatsapp-icon-white.svg" alt="" class="icon-left-primary"> Message on Whatsapp
                </a>
                <a href="#" class="btn-gepl btn-large btn-gepl-secondary">
                  <img src="assets/images/callback-icon-footer.svg" alt="" class="icon-left-secondary">
                  Request a call back
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </footer> -->
  <div class="footer-bottom">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 pl0 pr0">
          <div class="attention-section-wrapper">
            <div class="container">
              <div class="row">
                <div class="col-12">
                  <div class="attention-section">
                    <div class="attention-title">
                      <p class="txtm text-light"> <img src="assets/images/attention-icon.svg" alt=""> Attention users</p>
                    </div>
                    <div class="attention-scroller">
                      <div class="scroll-left">
                        <p class="marquee txts text-light">
                          <span>
                            1) Stock Brokers can accept securities as margin from clients only by way of pledge in the depository system w.e.f. September 1, 2020. 2) Update your mobile number & email Id wit
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <div class="footer-links-wrapper">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="footer-links-container">
            <div class="footer-single footer-logo-gepl">
              <a href="index.php"><img src="assets/images/logo-white.png" alt="" class="footer-logo"></a>
              <div class="footer-short-desc txtm">
                <p>We offer comfortable spaces, cozy cafe, high-speed internet, spacious parking area and many more for your best workspaces and meetings</p>
              </div>
              <div class="footer-social-links">
                <a href="#/"><img src="assets/images/twitter-white.svg" alt=""></a>
                <a href="#/"><img src="assets/images/facebook-white.svg" alt=""></a>
                <a href="#/"><img src="assets/images/linkedin-white.svg" alt=""></a>
              </div>
            </div>
            <div class="footer-single footer-about-gepl footer-links-section">
              <h4 class="txtm">
                About
                <img src="assets/images/footer-arrow.svg" class="footer-link-arrow">
              </h4>
              <ul class="footer-links-list">
                <li><a href="about-gepl.php">Philisophy</a></li>
                <li><a href="about-gepl.php">Profile of company</a></li>
                <li><a href="about-gepl.php">Servicing principle</a></li>
                <li><a href="about-gepl.php">Key term</a></li>
                <li><a href="contact.php">Location</a></li>
                <li><a href="about-gepl.php">Group Companies</a></li>
              </ul>
            </div>
            <div class="footer-single footer-resources-gepl footer-links-section">
              <h4 class="txtm">
                Resources
                <img src="assets/images/footer-arrow.svg" class="footer-link-arrow">
              </h4>
              <ul class="footer-links-list">
                <li><a href="resources.php">Margin Files</a></li>
                <li><a href="resources.php">Form Centre</a></li>
                <li><a href="resources.php">FAQs</a></li>
                <li><a href="#/">Key term</a></li>
              </ul>
            </div>
            <div class="footer-single footer-careers-gepl footer-links-section">
              <h4 class="txtm">
                Careers
                <img src="assets/images/footer-arrow.svg" class="footer-link-arrow">
              </h4>
              <ul class="footer-links-list">
                <li><a href="#/">Life at GEPL</a></li>
                <li><a href="#/">Opportunities</a></li>
              </ul>
            </div>
            <div class="footer-single footer-newsletter-gepl">
              <h4 class="txtm">Stay up to date</h4>
              <p class="txtm">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
              <form class="newsletter-footer">
                <input type="text" placeholder="Your email address" class="newsletter-email" />
                <input type="submit" class="submit-newsletter" value="">
              </form>
              <a href="#/" class="btn-gepl btn-gepl-tertiary-medium text-light">Create a Ticket</a>
            </div>
          </div>
          <div class="footer-disclaimer txts ">
            <h4 class="txtm disclaimer-link-mobile">
              Disclaimer
              <img src="assets/images/footer-arrow.svg" class="footer-link-arrow">
            </h4>
            <div class="disclaimer-content">
              <p><strong>GEPL CAPITAL PRIVATE LIMITED</strong>: Member of NSE, BSE (INZ000168137) - CIN No.- U67120MH1997PTC110941 Registered office address- D-21, Dhanraj Mahal, CSM Marg, Colaba, Mumbai-400001;Tel No.-022 66182400, SEBI Registration No’s- GEPL Capital Private Limited- INZ000168137(NSE and BSE); CDSL- IN-DP-CDSL-27-99;Research Analyst Certificate No.:-INH000000081; AMFI-ARN-27210; GEPL Investment Advisors Private Limited-Investment Advisor Certificate no.: INA000002744; GEPL Finance Private Limited-RBI registration no.: N-13.01928; GEPL Insurance Broking Private Limited-No- 267.</p>
              <p>Please read risk disclosure documents prescribed by the Stock Exchange carefully before investing. There is no assurance or guarantee of the returns. Investment in securities market is subject to market risk, read all the related documents carefully before investing. GEPL COMMODITIES PRIVATE LIMITED- Member of MCX, NCDEX (INZ000017536) - CIN No.- U67190MH1998PTC115790</p>
              <p><strong>DISCLAIMER</strong>: You agree that GEPL Capital Pvt. Ltd. can modify or alter the terms and conditions of the use of this service without any liability. GEPL Capital Pvt. Ltd. has launched e-broking services. It reserves the right to decide the criteria based on which customers would be allowed to avail of these services.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
