<?php //echo '<pre>'; print_r($_SESSION);die;     ?>
<header class="bulkmro-nav-container">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="nav-wrapper">
                    <div class="nav-wrapper-left">
                        <a href="<?php echo base_url("dashboard") ?>" class="nav-logo"><img src="<?php echo base_url(); ?>assets/images/BulkMRO_logo.svg" alt=""></a>
                        <div class="nav-divider"></div>
                        <div class="menu-dropdown-wrapper">
                            <a href="#" class="menu-link">
                                Menu
                                <img src="<?php echo base_url(); ?>assets/images/down-menu-home.svg" alt="" class="menu-dropdown-icon">
                            </a> 
                            <div class="menu-dropdown">
                                <div class="dropdown-content">
                                    <div class="dropdown-col-single">
                                        <h3>View</h3>
                                        <ul class="menu-links">
                                            <!-- <li><a href="<?php echo base_url("dashboard") ?>">Dashboard</a></li> -->

                                            <?php if ($this->privilegeduser->hasPrivilege("ContainersDetailsList")) { ?>
                                                <li><a href="<?= base_url('listOfContainer') ?>">Container Bible</a></li>
                                            <?php } ?>

                                            <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersList")) { ?>
                                                <li><a href="<?php echo base_url("business_partner") ?>">All Business Partners</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersOrderList")) { ?>
                                                <li><a href="<?php echo base_url("bporder") ?>">All Business Partner Contracts</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("MasterContractList")) { ?>
                                                <li><a href="<?php echo base_url("order") ?>">All Master Contracts</a></li>
                                            <?php } ?> 

<!--                                            <li><a href="<?php echo base_url("inventory_summary") ?>">Inventory Summary</a></li>--> 

                                            <?php if ($this->privilegeduser->hasPrivilege("ContainersList")) { ?>
                                                <li><a href="<?php echo base_url("container") ?>">All Containers</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("AllDocuments")) { ?>
                                                <li><a href="<?php echo base_url("allDocuments") ?>">All Documents</a></li>
                                            <?php } ?>  
                                        </ul>
                                    </div>
                                    <?php if($_SESSION["mro_session"]['user_role'] !='Customer' && $_SESSION["mro_session"]['user_role'] !='Supplier'){  ?>
                                    <div class="dropdown-col-single">
                                        <h3>Create New</h3>
                                        <ul class="menu-links">  
                                            <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersAddEdit")) { ?>
                                                <li><a href="<?php echo base_url("business_partner/addEdit") ?>">Business Partner</a></li> 
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersOrderAddEdit")) { ?>
                                                <li><a href="<?php echo base_url("bporder/addEdit") ?>">Business Partner Contract</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("MasterContractAddEdit")) { ?>
                                                <li><a href="<?php echo base_url("order/AddEdit") ?>">Master Contract</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) { ?>
                                                <li><a href="<?php echo base_url("container/AddEdit") ?>">Container</a></li>
                                            <?php } ?>   
                                        </ul>
                                    </div>
                                    <?php } ?>
                                    <div class="dropdown-col-single">
                                        <h3>Masters</h3>
                                        <ul class="menu-links"> 
                                            <?php if ($this->privilegeduser->hasPrivilege("CategoryList")) { ?>
                                                <li><a href="<?php echo base_url("category") ?>">All Sku Category</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("BrandList")) { ?>
                                                <li><a href="<?php echo base_url("brand") ?>">All Brand</a></li>
                                            <?php } ?>                                             
                                            <?php if ($this->privilegeduser->hasPrivilege("BusinessPartnersList")) { ?>
                                                <li><a href="<?php echo base_url("business_partner") ?>">All Manufacture</a></li>
                                            <?php } ?>  
                                            <?php if ($this->privilegeduser->hasPrivilege("SizeList")) { ?>
                                                <li><a href="<?php echo base_url("size") ?>">All Size</a></li>
                                            <?php } ?>   
                                            <?php if ($this->privilegeduser->hasPrivilege("UoMList")) { ?>
                                                <li><a href="<?php echo base_url("uom") ?>">All UoM</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("SKUList")) { ?>
                                                <li><a href="<?php echo base_url("sku") ?>">All SKUs</a></li>
                                            <?php } ?>   
                                           
                                            <?php if ($this->privilegeduser->hasPrivilege("ContainerStatusList")) { ?>
                                                <li><a href="<?php echo base_url("container_status") ?>">All Container Status</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("UsersList")) { ?>
                                                <li><a href="<?php echo base_url("users") ?>">All Users</a></li> 
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("ContainerStatusLog")) { ?>
                                                <li><a href="<?php echo base_url("container_status_log") ?>">All Containers Status Log</a></li> 
                                            <?php } ?>  
                                            <?php if ($this->privilegeduser->hasPrivilege("DocumentTypeList")) { ?>
                                                <li><a href="<?php echo base_url("listOfDocument") ?>">Type Of Document </a></li>
                                            <?php } ?>  
                                            <?php if ($this->privilegeduser->hasPrivilege("DocumentMasterList")) { ?>
                                                <li><a href="<?php echo base_url("documentMaster") ?>">Document Master</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("DocumentImport")) { ?>
                                                <li><a href="<?php echo base_url("import_document") ?>">Document Import</a></li>
                                            <?php } ?>  
                                            <?php if ($this->privilegeduser->hasPrivilege("PermissionList")) { ?>
                                                <li><a href="<?php echo base_url("permission") ?>">All Permissions</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("RolesList")) { ?>
                                                <li><a href="<?php echo base_url("roles") ?>">All Roles</a></li> 
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("CountryList")) { ?>
                                                <li><a href="<?php echo base_url("country") ?>">All Country</a></li> 
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("StateList")) { ?>
                                                <li><a href="<?php echo base_url("state") ?>">All State</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("CityList")) { ?>
                                                <li><a href="<?php echo base_url("city") ?>">All City</a></li> 
                                            <?php } ?> 
                                           
                                            <?php if ($this->privilegeduser->hasPrivilege("PaymentTermsList")) { ?>
                                                <li><a href="<?php echo base_url("payment_term") ?>">All Payment Term</a></li>
                                            <?php } ?>
                                            <?php if ($this->privilegeduser->hasPrivilege("IncotermsList")) { ?>
                                                <li><a href="<?php echo base_url("incoterms") ?>">All Incoterms</a></li>
                                            <?php } ?>  
                                            <?php if ($this->privilegeduser->hasPrivilege("LinerList")) { ?>
                                                <li><a href="<?php echo base_url("liner") ?>">All Liner</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("POLList")) { ?>
                                                <li><a href="<?php echo base_url("pol") ?>">All POL</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("PODList")) { ?>
                                                <li><a href="<?php echo base_url("pod") ?>">All POD</a></li>
                                            <?php } ?> 
                                            <?php if ($this->privilegeduser->hasPrivilege("FlagList")) { ?>
                                                <li><a href="<?php echo base_url("flag") ?>">All Flag</a></li>
                                            <?php } ?>  
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="nav-divider"></div>
                        <div class="menu-search header-search"> 
                            <span class="menu-search-div" style="position: relative;">
                                <input type="text" id="search-admin" placeholder="Search" value="">
                                <div class="search-menu" style="position: absolute;top: 143%;left: 0px;z-index: 100;display: none;"> 
                                    <div class="search-result" id="search-result"></div>  
                                </div> 
                            </span>  
                        </div>
                    </div>
                    <div class="nav-wrapper-right">
                        <a href="#"><img src="<?php echo base_url(); ?>assets/images/notification-icon.svg" alt=""></a>
                        <div class="nav-divider"></div>
                        <?php if ($this->privilegeduser->hasPrivilege("ContainersAddEdit")) { ?>
                            <a href="<?php echo base_url("container/addEdit") ?>" class="btn-secondary-mro"><img src="<?php echo base_url(); ?>assets/images/add-icon-dark.svg" alt="">  New Container</a>
                        <?php } ?>  
                        <div class="menu-dropdown-wrapper">
                            <a href="#" class="profile-link"> 
                                <?php
                                $firstname = $_SESSION['mro_session'][0]['firstname'];
                                $lastname = $_SESSION['mro_session'][0]['lastname'];
                                ?>
                                <span class="client-initials-logo"><?= $firstname[0] . $lastname[0]; ?></span><?= $firstname; ?>
                                <!-- <?php if (!empty($_SESSION['mro_session'][0]['profile_picture'])) { ?>
                                                <img src="<?php echo FRONT_URL . '/uploads/client_images/' . $_SESSION['mro_session'][0]['profile_picture']; ?>" alt="..." class="avatar-profile">
                                <?php } else { ?>
                                                <img src="<?php echo base_url("assets/images/user-avatar.png") ?>" alt="..." class="avatar-profile">
                                <?php } ?> -->
                                <!-- Profile -->
                                <img src="<?php echo base_url(); ?>assets/images/down-menu-home.svg" alt="" class="menu-dropdown-icon" style="width:8px;"> 
                            </a>
                            <div class="profile-dropdown">
                                <div class="dropdown-content profile-dropdown-content">
                                    <div class="dropdown-col-single">
                                        <h3>My Profile</h3>
                                        <ul class="menu-links">
                                            <?php $myAccount = 'users/addEdit?text=' . rtrim(strtr(base64_encode("id=" . $_SESSION['mro_session'][0]['user_id']), '+/', '-_'), '=') . ''; ?>                
                                            <li><a href="<?php echo base_url($myAccount) ?>">My Account</a></li>
                                            <li><a href="<?php echo base_url("users/changePassword") ?>">Change Password</a></li>
                                            <li><a href="<?php echo base_url("dashboard/logOut") ?>">Logout</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
 
<style>
/* Search record */ 
.search-menu {
    width: 100%;
}
.search-menu > div {
    max-height: 350px;
    overflow-y: auto;
    background-color: white;
    margin-left: auto;
}
.search-result > a {
    padding: 5px 10px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    border-width: 1px 0 0 0;
    color: #687686;
    font-size: 14px;
    border: 1px solid #e7e9eb;
} 
 
</style>

<script>
//get search records
$('#search-admin').keyup(function () {
    var searchInput = $('#search-admin').val().trim();
    if (searchInput.length > 1) {
        $('.search-menu').show();
        var act = "<?php echo base_url(); ?>search/getSearchRecord";
        $.ajax({
            type: 'GET',
            url: act,
            data: {
                searchInput: searchInput
            },
            dataType: "json",
            success: function (data) {
                $('#search-result').html(data.str);
            }
        });
    } else {
        $('.search-menu').hide();
    }
})
</script>