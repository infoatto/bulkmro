<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="#/">Dashboard</a>
              <span>></span>
              <p>New Master Order</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title">Master Order</h1>
            </div>
          </div>
          <button type="submit" class="btn-primary-mro">Save</button>
        </div>
        <div class="page-content-wrapper master-order-wrapper">
          <div class="scroll-content-wrapper master-order">
            <div class="scroll-content-left master-order-left" id="contract-details">
              <form>
                <h3 class="form-group-title">Order Details</h3>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Customer Contract Number <sup>*</sup></label>
                    <input type="text" placeholder="Enter Contract Number" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Supplier Contract Number <sup>*</sup></label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Supplier Contract Number</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                  </div>
                  <div class="form-group input-edit">
                    <label for="">Master Contract Number <sup>*</sup></label>
                    <input type="text" placeholder="Customer-Supplier Contract #" class="input-form-mro">
                    <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                  </div>
                </div>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">CHA</label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select CHA</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Freight Forwarder</label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Freight Forwarder</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                  </div>
                </div>
                <hr class="separator mt0" id="shipment-dates">
                <h3 class="form-group-title">Shipment Dates</h3>
                <div class="form-row form-row-3">
                  <div class="form-group input-edit">
                    <label for="">Shipment Start Date<sup>*</sup></label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Start Date</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                    <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                  </div>
                  <div class="form-group input-edit">
                    <label for="">Duration in Weeks<sup>*</sup></label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Number of Weeks</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                    <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                  </div>
                </div>
                <hr class="separator mt0" id="pickup-details">
                <h3 class="form-group-title">Billing and Shipping Addresses</h3>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Shipping Address</label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Shipping Address</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                    <div class="ba-single ba-select">
                      <p class="bas-title">Billing Address</p>
                      <p class="bas-text">
                        Panasonic India Ltd.
                        502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                      </p>
                      <p class="bas-title">Shipping Contact</p>
                      <p class="bas-text">Eric Campbell</p>
                      <p class="bas-title">Shipping Email</p>
                      <p class="bas-text">johndoe@domain.com</p>
                      <p class="bas-title">Shipping Phone</p>
                      <p class="bas-text">+1-912-695-3507</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Ship To or From Address</label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Shipping Address</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                    <div class="ba-single ba-select">
                      <p class="bas-title">Shipping Address</p>
                      <p class="bas-text">
                        Panasonic India Ltd.
                        502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                      </p>
                      <p class="bas-title">Shipping Contact</p>
                      <p class="bas-text">Eric Campbell</p>
                      <p class="bas-title">Shipping Email</p>
                      <p class="bas-text">johndoe@domain.com</p>
                      <p class="bas-title">Shipping Phone</p>
                      <p class="bas-text">+1-912-695-3507</p>
                    </div>
                  </div>
                </div>
                <hr class="separator mt0" id="deliver-skus">
                <h3 class="form-group-title">SKUs to Deliver</h3>
                <div class="form-row form-row-3">
                  <div class="sku-list-wrapper add-sku-wrapper">
                    <div class="sit-row">
                      <div class="sit-single sku-item-code">
                        <p class="sit-single-title">Item Code</p>
                        <p class="sit-single-value">BM12345</p>
                      </div>
                      <div class="sit-single sku-pd">
                        <p class="sit-single-title">Product Description</p>
                        <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                      </div>
                      <div class="sit-single sku-size">
                        <p class="sit-single-title">Size</p>
                        <p class="sit-single-value">Small</p>
                      </div>
                      <div class="sit-single sku-qty">
                        <p class="sit-single-title">Quantity<sup>*</sup></p>
                        <input type="text" class="input-form-mro" placeholder="Enter quantity">
                      </div>
                    </div>
                    <div class="sit-row">
                      <div class="sit-single sku-item-code">
                        <p class="sit-single-title">Item Code</p>
                        <p class="sit-single-value">BM12345</p>
                      </div>
                      <div class="sit-single sku-pd">
                        <p class="sit-single-title">Product Description</p>
                        <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                      </div>
                      <div class="sit-single sku-size">
                        <p class="sit-single-title">Size</p>
                        <p class="sit-single-value">Small</p>
                      </div>
                      <div class="sit-single sku-qty">
                        <p class="sit-single-title">Quantity<sup>*</sup></p>
                        <input type="text" class="input-form-mro" placeholder="Enter quantity">
                      </div>
                    </div>
                    <div class="sit-row">
                      <div class="sit-single sku-item-code">
                        <p class="sit-single-title">Item Code</p>
                        <p class="sit-single-value">BM12345</p>
                      </div>
                      <div class="sit-single sku-pd">
                        <p class="sit-single-title">Product Description</p>
                        <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                      </div>
                      <div class="sit-single sku-size">
                        <p class="sit-single-title">Size</p>
                        <p class="sit-single-value">Small</p>
                      </div>
                      <div class="sit-single sku-qty">
                        <p class="sit-single-title">Quantity<sup>*</sup></p>
                        <input type="text" class="input-form-mro" placeholder="Enter quantity">
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="separator mt0" id="delivery-schedule">
                <div class="load-delivery-schedule">
                  <h3 class="form-group-title">Delivery Schedule</h3>
                  <p><strong>Note:</strong> Add All SKUs before Loading Delivery Schedule</p>
                  <a href="#/" class="btn-primary-mro load-ds">Load Delivery Schedule </a>
                </div>
                <div class="delivery-schedule-data">
                  <div class="title-sec-wrapper ds-title-wrapper">
                    <h3 class="form-group-title">Delivery Schedule</h3>
                    <a href="#/" class="btn-primary-mro"><img src="assets/images/redo-white.svg" alt=""> Reload </a>
                  </div>
                  <div class="delivery-schedule-wrapper">
                    <div class="ds-row ds-row-top">
                      <div class="ds-month">Month</div>
                      <div class="ds-weeks"></div>
                      <div class="ds-sku">BM12345</div>
                      <div class="ds-sku">BM12345</div>
                      <div class="ds-sku">BM12345</div>
                      <div class="ds-sku">BM12345</div>
                      <div class="ds-sku">BM12345</div>
                      <div class="ds-sku">BM12345</div>
                    </div>
                    <!-- Single month wrapper -->
                    <div class="ds-single-wrapper">
                      <!-- Single month header -->
                      <div class="ds-row ds-row-header">
                        <div class="ds-month">
                          <a href="#/" class="ds-toggle-icon"></a> <input type="text" class="input-form-mro" value="July 2021">
                        </div>
                        <div class="ds-weeks"></div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                      </div>
                      <!-- Single month header -->
                      <!-- Single month content -->
                      <div class="ds-expand-content">
                        <div class="ds-row ds-row-week ds-row-week-header">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Weeks</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="19/07/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="26/07/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-total">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Recalculated Total</div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                        </div>
                      </div>
                      <!-- Single month content -->
                    </div>
                    <!-- Single month wrapper -->
                    <!-- Single month wrapper -->
                    <div class="ds-single-wrapper">
                      <!-- Single month header -->
                      <div class="ds-row ds-row-header">
                        <div class="ds-month">
                          <a href="#/" class="ds-toggle-icon"></a> <input type="text" class="input-form-mro" value="Aug 2021">
                        </div>
                        <div class="ds-weeks"></div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                      </div>
                      <!-- Single month header -->
                      <!-- Single month content -->
                      <div class="ds-expand-content">
                        <div class="ds-row ds-row-week ds-row-week-header">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Weeks</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="02/08/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="09/08/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="16/08/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="23/08/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="30/08/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-total">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Recalculated Total</div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                        </div>
                      </div>
                      <!-- Single month content -->
                    </div>
                    <!-- Single month wrapper -->
                    <!-- Single month wrapper -->
                    <div class="ds-single-wrapper">
                      <!-- Single month header -->
                      <div class="ds-row ds-row-header">
                        <div class="ds-month">
                          <a href="#/" class="ds-toggle-icon"></a> <input type="text" class="input-form-mro" value="Sept 2021">
                        </div>
                        <div class="ds-weeks"></div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                      </div>
                      <!-- Single month header -->
                      <!-- Single month content -->
                      <div class="ds-expand-content">
                        <div class="ds-row ds-row-week ds-row-week-header">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Weeks</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="19/09/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="26/09/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-total">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Recalculated Total</div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                        </div>
                      </div>
                      <!-- Single month content -->
                    </div>
                    <!-- Single month wrapper -->
                    <!-- Single month wrapper -->
                    <div class="ds-single-wrapper">
                      <!-- Single month header -->
                      <div class="ds-row ds-row-header">
                        <div class="ds-month">
                          <a href="#/" class="ds-toggle-icon"></a> <input type="text" class="input-form-mro" value="Oct 2021">
                        </div>
                        <div class="ds-weeks"></div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                        <div class="ds-sku">
                          <input type="text" class="input-form-mro" value="2000">
                        </div>
                      </div>
                      <!-- Single month header -->
                      <!-- Single month content -->
                      <div class="ds-expand-content">
                        <div class="ds-row ds-row-week ds-row-week-header">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Weeks</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="02/10/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <input type="text" class="input-form-mro" value="09/10/21">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                          <div class="ds-sku">
                            <input type="text" class="input-form-mro" value="2000">
                          </div>
                        </div>
                        <div class="ds-row ds-row-total">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Recalculated Total</div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                        </div>
                      </div>
                      <!-- Single month content -->
                    </div>
                    <!-- Single month wrapper -->

                  </div>
                </div>
              </form>
            </div>
            <div class="scroll-content-right master-order-right">
              <div class="vertical-scroll-links">
                <a href="#contract-details" class="active">Contract Details</a>
                <a href="#shipment-dates">Shipment Dates</a>
                <a href="#pickup-details">Billing and Shipping Addresses</a>
                <a href="#deliver-skus">SKUs to Deliver</a>
                <a href="#delivery-schedule">Delivery Schedule</a>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>