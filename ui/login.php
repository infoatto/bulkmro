<?php require_once('include/head.php') ?>
<div class="login-wrapper">
  <!-- <div class="container">
    <div class="row">
      <div class="col-12">
          
      </div>
    </div>
  </div> -->
  <div class="login-content">
    <div class="login-logo"><img src="assets/images/BulkMRO_logo.svg" alt=""></div>
    <div class="login-form">
      <form>
        <h3 class="form-group-title">Login</h3>
        <div class="form-group">
          <label for="">Username<sup>*</sup></label>
          <input type="text" placeholder="Enter username" class="input-form-mro">
        </div>
        <div class="form-group">
          <label for="">Password<sup>*</sup></label>
          <input type="text" placeholder="Enter password" class="input-form-mro">
        </div>
        <div>
          <button type="submit" class="btn-primary-mro w100">Login</button>
        </div>
        <div class="login-bottom-links">
          <a href="#/" class="fp-link link-mro">Forgot Password?</a>
        </div>
      </form>
    </div>
    <div class="fp-form">
      <form>
      <h3 class="form-group-title">Forgot Password?</h3>
        <div class="form-group">
          <label for="">Enter Email ID<sup>*</sup></label>
          <input type="text" placeholder="Enter email ID" class="input-form-mro">
        </div>        
        <div>
          <button type="submit" class="btn-primary-mro w100">Reset Password</button>
        </div>
        <div class="login-bottom-links">
          <a href="#/" class="login-link link-mro">Back to Login</a>
        </div>
      </form>
    </div>
  </div>

</div>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>