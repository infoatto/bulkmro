<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="#/">Dashboard</a>
              <span>></span>
              <a href="#/">All Containers</a>
              <span>></span>
              <p># 600007</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title">Container # 600007</h1>
              <div class="nav-divider"></div>
              <div class="cs-status-text">
                <p class="container-status">Goods in BM Warehouse</p>
              </div>
              <div class="cs-status-text">
                <p class="last-update-title">Last Updated</p>
                <p class="last-update-name">Nitin N, 19-Mar-21</p>
              </div>
              <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
            </div>
          </div>
          <div class="title-sec-right">
            <a href="#/" class="btn-primary-mro"><img src="assets/images/upload-icon-white.svg" alt=""> Upload File</a>
            <a href="#/" class="btn-transparent-mro"><img src="assets/images/duplicate-icon.svg" alt=""> Duplicate</a>
          </div>
        </div>
        <div class="shipping-info-top">
          <h2 class="sec-title">Shipping Information</h2>
          <hr class="separator">
          <div class="sit-wrapper">
            <div class="sit-row">
              <div class="sit-single">
                <p class="sit-single-title">Freight Forwarder</p>
                <p class="sit-single-value">DB Schenker</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">CHA</p>
                <p class="sit-single-value">Seven Seas</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">POL</p>
                <p class="sit-single-value">Shanghai</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">POD</p>
                <p class="sit-single-value">Los Angeles</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Liner Name</p>
                <p class="sit-single-value">CMA GMA</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Vessel Name</p>
                <p class="sit-single-value">CMA CGM FLORIDA V.</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Flag</p>
                <p class="sit-single-value">USA</p>
              </div>
            </div>
            <div class="sit-row">
              <div class="sit-single">
                <p class="sit-single-title">ETD</p>
                <p class="sit-single-value">01-May-21</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Revised ETD</p>
                <p class="sit-single-value">01-May-21</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">ETA</p>
                <p class="sit-single-value">01-Jun-21</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Revised ETA</p>
                <p class="sit-single-value">01-May-21</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">ETT</p>
                <p class="sit-single-value">51 Days</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Revised ETT</p>
                <p class="sit-single-value">51 Days</p>
              </div>
            </div>
            <div class="sit-row">
              <div class="sit-single">
                <p class="sit-single-title">Customer Alias</p>
                <p class="sit-single-value">REM</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Vendor Alias</p>
                <p class="sit-single-value">HON</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Shipping Container #</p>
                <p class="sit-single-value">Waiting for FCR</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Shipping Container Seal #</p>
                <p class="sit-single-value">Waiting for FCR</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">FF Seal #</p>
                <p class="sit-single-value">Waiting for FCR</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Email Sent On</p>
                <p class="sit-single-value">30-Jun-21</p>
              </div>
              <div class="sit-single">
                <p class="sit-single-title">Email Subject</p>
                <p class="sit-single-value">Lorem ipsum dolor si amet...</p>
              </div>
            </div>
          </div>
        </div>
        <div class="shipping-tabs-wrapper">
          <div class="mro-tabs-container">
            <nav>
              <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-documents-tab" data-toggle="tab" href="#nav-documents" role="tab" aria-controls="nav-documents" aria-selected="true">Documents</a>
                <a class="nav-item nav-link" id="nav-linked-containers-tab" data-toggle="tab" href="#nav-linked-containers" role="tab" aria-controls="nav-linked-containers" aria-selected="true">Linked Containers</a>
                <a class="nav-item nav-link " id="nav-products-tab" data-toggle="tab" href="#nav-products" role="tab" aria-controls="nav-products" aria-selected="false">Products</a>
                <a class="nav-item nav-link " id="nav-invoices-tab" data-toggle="tab" href="#nav-invoices" role="tab" aria-controls="nav-invoices" aria-selected="false">Invoices</a>
                <a class="nav-item nav-link" id="nav-addresses-tab" data-toggle="tab" href="#nav-addresses" role="tab" aria-controls="nav-addresses" aria-selected="false">Addresses</a>
              </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active" id="nav-documents" role="tabpanel" aria-labelledby="nav-documents-tab">
                <div class="tab-content-wrapper container-details-doc-tab">
                  <div class="container-empty-msg">
                    <p>No documents have been added to this container</p>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="nav-linked-containers" role="tabpanel" aria-labelledby="nav-linked-containers-tab">
                <div class="tab-content-wrapper">
                  <div class="table-responsive">
                    <table class="table list-table linked-containers-table">
                      <thead>
                        <tr>
                          <th scope="col">Linked Containers</th>
                          <th scope="col">Status</th>
                          <th scope="col"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>600008</td>
                          <td>
                            <div class="cs-status-text">
                              <p class="container-status">Goods in Transit to Customer</p>
                            </div>
                          </td>
                          <td>
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>600008</td>
                          <td>
                            <div class="cs-status-text">
                              <p class="container-status">Goods Received in Port</p>
                            </div>
                          </td>
                          <td>
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>600008</td>
                          <td>
                            <div class="cs-status-text">
                              <p class="container-status">Goods Received in Port</p>
                            </div>
                          </td>
                          <td>
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>600008</td>
                          <td>
                            <div class="cs-status-text">
                              <p class="container-status">Goods in BM Warehouse</p>
                            </div>
                          </td>
                          <td>
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>600008</td>
                          <td>
                            <div class="cs-status-text">
                              <p class="container-status">Goods in Transit to Customer</p>
                            </div>
                          </td>
                          <td>
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="nav-products" role="tabpanel" aria-labelledby="nav-products-tab">
                <div class="tab-content-wrapper">
                  <h3 class="form-group-title">Product Details <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a></h3>
                  <div class="bp-list-wrapper">
                    <div class="table-responsive">
                      <table class="table table-striped">
                        <thead>
                          <tr>
                            <th scope="col">Sr. No.</th>
                            <th scope="col">Product Description</th>
                            <th scope="col">BM SKU</th>
                            <th scope="col">Manufacturer</th>
                            <th scope="col">Manufacturer SKU</th>
                            <th scope="col">Brand</th>
                            <th scope="col">Size</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">UoM</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</td>
                            <td>BM123456</td>
                            <td>Hongray</td>
                            <td>SBMPF300bx-PAR</td>
                            <td>Synmax</td>
                            <td>Small</td>
                            <td>100,000</td>
                            <td>Box of 100 Gloves</td>
                          </tr>
                          <tr>
                            <td>2</td>
                            <td>Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</td>
                            <td>BM123456</td>
                            <td>Hongray</td>
                            <td>SBMPF300bx-PAR</td>
                            <td>Synmax</td>
                            <td>Small</td>
                            <td>100,000</td>
                            <td>Box of 100 Gloves</td>
                          </tr>
                          <tr>
                            <td>3</td>
                            <td>Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</td>
                            <td>BM123456</td>
                            <td>Hongray</td>
                            <td>SBMPF300bx-PAR</td>
                            <td>Synmax</td>
                            <td>Small</td>
                            <td>100,000</td>
                            <td>Box of 100 Gloves</td>
                          </tr>
                          <tr>
                            <td>4</td>
                            <td>Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</td>
                            <td>BM123456</td>
                            <td>Hongray</td>
                            <td>SBMPF300bx-PAR</td>
                            <td>Synmax</td>
                            <td>Small</td>
                            <td>100,000</td>
                            <td>Box of 100 Gloves</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="nav-invoices" role="tabpanel" aria-labelledby="nav-invoices-tab">
                <div class="tab-content-wrapper">
                  <h3 class="form-group-title">Uploaded Invoices</h3>
                  <div class="table-responsive">
                    <table class="table list-table uploaded-invoices-table">
                      <tbody>
                        <tr>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">22-06-21</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:250px">
                            <div class="form-group">
                              <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="Auto Populated File Name ">
                            </div>
                          </td>
                          <td style="width:150px">
                            Name of Business Partner
                          </td>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">Paid</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">22-06-21</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:150px">
                            <div class="form-group">
                              <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="200,000,000">
                            </div>
                          </td>
                          <td style="width:150px">
                            <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                            <a href="#/" class="btn-grey-mro">View</a>
                          </td>
                          <td style="width:150px">
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">22-06-21</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:250px">
                            <div class="form-group">
                              <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="Auto Populated File Name ">
                            </div>
                          </td>
                          <td style="width:150px">
                            Name of Business Partner
                          </td>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">Paid</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">22-06-21</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:150px">
                            <div class="form-group">
                              <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="200,000,000">
                            </div>
                          </td>
                          <td style="width:150px">
                            <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                            <a href="#/" class="btn-grey-mro">View</a>
                          </td>
                          <td style="width:150px">
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">22-06-21</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:250px">
                            <div class="form-group">
                              <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="Auto Populated File Name ">
                            </div>
                          </td>
                          <td style="width:150px">
                            Name of Business Partner
                          </td>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">Paid</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">22-06-21</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:150px">
                            <div class="form-group">
                              <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="200,000,000">
                            </div>
                          </td>
                          <td style="width:150px">
                            <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                            <a href="#/" class="btn-grey-mro">View</a>
                          </td>
                          <td style="width:150px">
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">22-06-21</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:250px">
                            <div class="form-group">
                              <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="Auto Populated File Name ">
                            </div>
                          </td>
                          <td style="width:150px">
                            Name of Business Partner
                          </td>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">Paid</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:150px">
                            <div class="form-group ">
                              <select name="" id="" class="select-form-mro ">
                                <option value="">22-06-21</option>
                                <option value="">Option 1</option>
                                <option value="">Option 2</option>
                              </select>
                            </div>
                          </td>
                          <td style="width:150px">
                            <div class="form-group">
                              <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="200,000,000">
                            </div>
                          </td>
                          <td style="width:150px">
                            <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                            <a href="#/" class="btn-grey-mro">View</a>
                          </td>
                          <td style="width:150px">
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="nav-addresses" role="tabpanel" aria-labelledby="nav-addresses-tab">
                <div class="tab-content-wrapper">
                  <div class="sit-wrapper nat-content">
                    <div class="sit-row">
                      <div class="sit-single">
                        <p class="sit-single-title">Business Alias</p>
                        <p class="sit-single-value">Remcoda</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Business Partner Type</p>
                        <p class="sit-single-value">Customer, Corporate</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Contact Name</p>
                        <p class="sit-single-value">Mohd. Sha Navas</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Email</p>
                        <p class="sit-single-value">n.mohd@ril.com</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Phone</p>
                        <p class="sit-single-value">7896541239</p>
                      </div>
                    </div>
                    <div class="sit-row">
                      <div class="sit-single">
                        <p class="sit-single-title">Billing Address</p>
                        <p class="sit-single-value">Panasonic India Ltd.
                          502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Billing Contact</p>
                        <p class="sit-single-value">Eric Campbell</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Shipping Address</p>
                        <p class="sit-single-value">Address 1:
                          Village Meghpar, Padana, Gagva, Taluka Lalpur,</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Shipping Contact</p>
                        <p class="sit-single-value">Eric Campbell</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Customer Contract</p>
                        <p class="sit-single-value">View Customer Contract</p>
                      </div>                      
                    </div>                    
                  </div>
                  <hr class="separator">
                  <div class="sit-wrapper nat-content">
                    <div class="sit-row">
                      <div class="sit-single">
                        <p class="sit-single-title">Vendor Alias</p>
                        <p class="sit-single-value">Remcoda</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Business Partner Type</p>
                        <p class="sit-single-value">Customer, Corporate</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Contact Name</p>
                        <p class="sit-single-value">Mohd. Sha Navas</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Email</p>
                        <p class="sit-single-value">n.mohd@ril.com</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Phone</p>
                        <p class="sit-single-value">7896541239</p>
                      </div>
                    </div>
                    <div class="sit-row">
                      <div class="sit-single">
                        <p class="sit-single-title">Billing Address</p>
                        <p class="sit-single-value">Panasonic India Ltd.
                          502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Billing Contact</p>
                        <p class="sit-single-value">Eric Campbell</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Shipping Address</p>
                        <p class="sit-single-value">Address 1:
                          Village Meghpar, Padana, Gagva, Taluka Lalpur,</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Shipping Contact</p>
                        <p class="sit-single-value">Eric Campbell</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Customer Contract</p>
                        <p class="sit-single-value">View Customer Contract</p>
                      </div>                      
                    </div>                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>