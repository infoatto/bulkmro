<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="#/">Home</a>
              <span>></span>
              <p>Dashboard</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title"><a href="#/" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> Containers</h1>
            </div>
          </div>
          <div class="title-sec-right">
            <a href="#/" class="btn-primary-mro upload-document" data-fancybox="dialog" data-src="#upload-doc-content"><img src="assets/images/upload-icon-white.svg" alt=""> Upload File</a>
          </div>
        </div>
        <div class="notification-wrapper">
          <div class="notification-text">New container created sucessfully</div>
          <a href="#/" class="notification-close"><img src="assets/images/close-dark.svg" alt=""></a>
        </div>
        <div class="filter-sec-wrapper">
          <div class="filter-search">
            <input type="text" class="filter-search-input" placeholder="Search by Container No., Document No....">
          </div>
          <div class="form-group mt20">
            <!-- <div class="singleselect-dropdown-wrapper">
              <div class="sd-value">
                Contaner #
              </div>
              <div class="sd-list-wrapper">
                <input type="text" placeholder="Search" class="sd-search">
                <div class="sd-list-items">
                  <div class="sdli-single">
                    <label>Listed Item 1</label>
                  </div>
                  <div class="sdli-single">
                    <label>Listed Item 2 </label>
                  </div>
                  <div class="sdli-single">
                    <label>Listed Item 3 </label>
                  </div>
                  <div class="sdli-single">
                    <label>Listed Item 4 </label>
                  </div>
                  <div class="sdli-single">
                    <label>Listed Item 5 </label>
                  </div>
                  <div class="sdli-single">
                    <label>Listed Item 6 </label>
                  </div>
                  <div class="sdli-single">
                    <label>Listed Item 7 </label>
                  </div>
                </div>
              </div>
            </div> -->
            <select class="basic-single select-large" name="state">
              <option value="">Container #</option>
              <option value="">List Item 1</option>
              <option value="">List Item 2</option>
              <option value="">List Item 3</option>
              <option value="">List Item 4</option>
              <option value="">List Item 5</option>
              <option value="">List Item 6</option>
              <option value="">List Item 7</option>
              <option value="">List Item 8</option>
              <option value="">List Item 9</option>
              <option value="">List Item 10</option>
            </select>
          </div>
          <div class="form-group  mt20">
            <select class="basic-single select-medium" name="state">
              <option value="">Status</option>
              <option value="">List Item 1</option>
              <option value="">List Item 2</option>
              <option value="">List Item 3</option>
              <option value="">List Item 4</option>
              <option value="">List Item 5</option>
              <option value="">List Item 6</option>
              <option value="">List Item 7</option>
              <option value="">List Item 8</option>
              <option value="">List Item 9</option>
              <option value="">List Item 10</option>
            </select>
          </div>
          <div class="form-group mt20">
            <select class="basic-single select-medium" name="state">
              <option value="">FRI</option>
              <option value="">List Item 1</option>
              <option value="">List Item 2</option>
              <option value="">List Item 3</option>
              <option value="">List Item 4</option>
              <option value="">List Item 5</option>
              <option value="">List Item 6</option>
              <option value="">List Item 7</option>
              <option value="">List Item 8</option>
              <option value="">List Item 9</option>
              <option value="">List Item 10</option>
            </select>
          </div>
          <div class="form-group mt20">
            <select class="basic-single select-medium" name="state">
              <option value="">LS</option>
              <option value="">List Item 1</option>
              <option value="">List Item 2</option>
              <option value="">List Item 3</option>
              <option value="">List Item 4</option>
              <option value="">List Item 5</option>
              <option value="">List Item 6</option>
              <option value="">List Item 7</option>
              <option value="">List Item 8</option>
              <option value="">List Item 9</option>
              <option value="">List Item 10</option>
            </select>
          </div>
          <div class="form-group mt20">
            <select class="basic-single select-medium" name="state">
              <option value="">Product</option>
              <option value="">List Item 1</option>
              <option value="">List Item 2</option>
              <option value="">List Item 3</option>
              <option value="">List Item 4</option>
              <option value="">List Item 5</option>
              <option value="">List Item 6</option>
              <option value="">List Item 7</option>
              <option value="">List Item 8</option>
              <option value="">List Item 9</option>
              <option value="">List Item 10</option>
            </select>
          </div>
          <div class="form-group mt20">
            <select class="basic-single select-medium" name="state">
              <option value="">Brand</option>
              <option value="">List Item 1</option>
              <option value="">List Item 2</option>
              <option value="">List Item 3</option>
              <option value="">List Item 4</option>
              <option value="">List Item 5</option>
              <option value="">List Item 6</option>
              <option value="">List Item 7</option>
              <option value="">List Item 8</option>
              <option value="">List Item 9</option>
              <option value="">List Item 10</option>
            </select>
          </div>
          <div class="form-group mt20">
            <select class="basic-single select-large" name="state">
              <option value="">Vessel</option>
              <option value="">List Item 1</option>
              <option value="">List Item 2</option>
              <option value="">List Item 3</option>
              <option value="">List Item 4</option>
              <option value="">List Item 5</option>
              <option value="">List Item 6</option>
              <option value="">List Item 7</option>
              <option value="">List Item 8</option>
              <option value="">List Item 9</option>
              <option value="">List Item 10</option>
            </select>
          </div>
          <div class="form-group mt20">
            <select class="basic-single select-large" name="state">
              <option value="">Freight Forwarder</option>
              <option value="">List Item 1</option>
              <option value="">List Item 2</option>
              <option value="">List Item 3</option>
              <option value="">List Item 4</option>
              <option value="">List Item 5</option>
              <option value="">List Item 6</option>
              <option value="">List Item 7</option>
              <option value="">List Item 8</option>
              <option value="">List Item 9</option>
              <option value="">List Item 10</option>
            </select>
          </div>
        </div>
        <!-- <div class="filter-sec-wrapper">
          <div class="filter-search">
            <input type="text" class="filter-search-input" placeholder="Search by Container No., Document No....">
          </div>
          <select name="" id="" class="select-mro select-large">
            <option value="">Container #</option>
            <option value="">1</option>
            <option value="">2</option>
          </select>
          <select name="" id="" class="select-mro select-medium">
            <option value="">Status</option>
            <option value="">1</option>
            <option value="">2</option>
          </select>
          <select name="" id="" class="select-mro select-small">
            <option value="">FRI</option>
            <option value="">1</option>
            <option value="">2</option>
          </select>
          <select name="" id="" class="select-mro select-small">
            <option value="">LS</option>
            <option value="">1</option>
            <option value="">2</option>
          </select>
          <select name="" id="" class="select-mro select-medium">
            <option value="">Product</option>
            <option value="">1</option>
            <option value="">2</option>
          </select>
          <select name="" id="" class="select-mro select-medium">
            <option value="">Brand</option>
            <option value="">1</option>
            <option value="">2</option>
          </select>
          <select name="" id="" class="select-mro select-large">
            <option value="">Vessel</option>
            <option value="">1</option>
            <option value="">2</option>
          </select>
          <select name="" id="" class="select-mro select-large">
            <option value="">Freight Forwarder</option>
            <option value="">1</option>
            <option value="">2</option>
          </select>
        </div> -->
        <div class="container-list-wrapper">
          <div class="container-single-wrapper">
            <div class="cs-info">
              <div class="cs-info-cta">
                <a href="#/"><img src="assets/images/view-btn-icon.svg" alt=""></a>
                <a href="#/"><img src="assets/images/upload-btn-icon.svg" alt=""></a>
              </div>
              <div class="cs-status-text">
                <p class="container-number">600006</p>
                <p class="container-status">Arrival Notice Received</p>
                <p class="last-update-title">Last Updated</p>
                <p class="last-update-name">Nitin N</p>
                <p class="last-update-date">19-Mar-21</p>
              </div>
            </div>
            <div class="cs-status-wrapper">
              <div class="connector"></div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">FRI</p>
                  <p class="css-single-data-text">FRI000005</p>
                  <p class="css-single-data-date">1-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">LS</p>
                  <p class="css-single-data-text">LS000005</p>
                  <p class="css-single-data-date">19-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">INS</p>
                  <p class="css-single-data-text">INS600006 </p>
                  <p class="css-single-data-text">TIP600006 </p>
                  <p class="css-single-data-date">30-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-red"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">FCR</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">HBL</p>
                  <p class="css-single-data-text">HBL600006</p>
                  <p class="css-single-data-text">EHBL600006</p>
                  <p class="css-single-data-date">31-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">MBL</p>
                  <p class="css-single-data-text">MBL600006</p>
                  <p class="css-single-data-text">MED3R5R68868</p>
                  <p class="css-single-data-date">31-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-red"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">7501</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">ANI</p>
                  <p class="css-single-data-text">ANI600006</p>
                  <p class="css-single-data-date">31-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-yellow"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">DO</p>
                  <p class="css-single-data-text">DO600006</p>
                  <p class="css-single-data-date">01-Jun-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-yellow"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">ABI</p>
                  <p class="css-single-data-text">ABI600006</p>
                  <p class="css-single-data-date">01-Jun-21</p>
                </div>
              </div>
            </div>
          </div>
          <div class="container-single-wrapper">
            <div class="cs-info">
              <div class="cs-info-cta">
                <a href="#/"><img src="assets/images/view-btn-icon.svg" alt=""></a>
                <a href="#/"><img src="assets/images/upload-btn-icon.svg" alt=""></a>
              </div>
              <div class="cs-status-text">
                <p class="container-number">600006</p>
                <p class="container-status">Goods in Transit to Customer</p>
                <p class="last-update-title">Last Updated</p>
                <p class="last-update-name">Nitin N</p>
                <p class="last-update-date">19-Mar-21</p>
              </div>
            </div>
            <div class="cs-status-wrapper">
              <div class="connector"></div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">FRI</p>
                  <p class="css-single-data-text">FRI000005</p>
                  <p class="css-single-data-date">1-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">LS</p>
                  <p class="css-single-data-text">LS000005</p>
                  <p class="css-single-data-date">19-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">INS</p>
                  <p class="css-single-data-text">INS600006 </p>
                  <p class="css-single-data-text">TIP600006 </p>
                  <p class="css-single-data-date">30-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-red"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">FCR</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">HBL</p>
                  <p class="css-single-data-text">HBL600006</p>
                  <p class="css-single-data-text">EHBL600006</p>
                  <p class="css-single-data-date">31-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">MBL</p>
                  <p class="css-single-data-text">MBL600006</p>
                  <p class="css-single-data-text">MED3R5R68868</p>
                  <p class="css-single-data-date">31-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-red"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">7501</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-green"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">ANI</p>
                  <p class="css-single-data-text">ANI600006</p>
                  <p class="css-single-data-date">31-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-yellow"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">DO</p>
                  <p class="css-single-data-text">DO600006</p>
                  <p class="css-single-data-date">01-Jun-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-yellow"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">ABI</p>
                  <p class="css-single-data-text">ABI600006</p>
                  <p class="css-single-data-date">01-Jun-21</p>
                </div>
              </div>
            </div>
          </div>
          <div class="container-single-wrapper">
            <div class="cs-info">
              <div class="cs-info-cta">
                <a href="#/"><img src="assets/images/view-btn-icon.svg" alt=""></a>
                <a href="#/"><img src="assets/images/upload-btn-icon.svg" alt=""></a>
              </div>
              <div class="cs-status-text">
                <p class="container-number">600006</p>
                <p class="container-status">Goods Delivered to Customer</p>
                <p class="last-update-title">Last Updated</p>
                <p class="last-update-name">Nitin N</p>
                <p class="last-update-date">19-Mar-21</p>
              </div>
            </div>
            <div class="cs-status-wrapper">
              <div class="connector"></div>
              <div class="css-single">
                <div class="css-flag css-grey"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">FRI</p>
                  <p class="css-single-data-text">FRI000005</p>
                  <p class="css-single-data-date">1-Mar-21</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-grey"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">LS</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-grey"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">INS</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-grey"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">FCR</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-grey"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">HBL</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-grey"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">MBL</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-grey"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">7501</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-grey"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">ANI</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-grey"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">DO</p>
                </div>
              </div>
              <div class="css-single">
                <div class="css-flag css-grey"></div>
                <div class="css-single-data">
                  <p class="css-single-data-title">ABI</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- Upload Document-->
<div id="upload-doc-content" style="display:none;min-width:400px;max-width:972px;">
  <!-- Select document type -->
  <div class="doc-type-wrapper upload-fri-step-1">
    <h3 class="form-group-title">Upload Document</h3>
    <div class="form-radio-wrapper">
      <div class="form-radio">
        <label class="container-radio">Document
          <input type="radio" checked="checked" name="radio" value="upload-document">
          <span class="checkmark"></span>
        </label>
      </div>
      <div class="form-radio">
        <label class="container-radio">Invoice
          <input type="radio" name="radio" value="upload-invoice">
          <span class="checkmark"></span>
        </label>
      </div>
    </div>
    <div class="form-cta">
      <button class="btn-primary-mro fri-step-1">Next</button>
    </div>
  </div>
  <!-- Select document type -->
  <!-- Upload FRI/LS Step 2 -->
  <div class="ud-step upload-fri-step-2">
    <div class="upload-doc-title">
      <div class="ud-title-left">
        <a href="#/" class="fri-step-2-back"><img src="assets/images/ud-back.svg" alt=""></a>
        <h3>Container Listing</h3>
      </div>
      <!-- <div class="ud-title-right">
        <a href="#/" class="btn-primary-mro">Back</a>
      </div> -->
    </div>
    <div class="select-doc-wrapper">
      <div class="sd-left">
        <h4 class="sd-subtitle">Document Type</h4>
        <select name="" id="" class="select-doc-type select-form-mro mb20">
          <option value="FRI">FRI</option>
          <option value="LS">LS</option>
          <option value="HBL">HBL</option>
        </select>
        <h4 class="sd-subtitle">Choose Containers</h4>
        <input type="text" class="search-doc" placeholder="Search">
        <div class="ud-list">
          <div class="ud-list-single">
            <label class="container-checkbox">6000001
              <input type="checkbox">
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000002
              <input type="checkbox">
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000003
              <input type="checkbox">
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000004
              <input type="checkbox">
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000005
              <input type="checkbox">
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000006
              <input type="checkbox">
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000007
              <input type="checkbox">
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000008
              <input type="checkbox">
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
        </div>
      </div>
      <div class="sd-right">
        <p class="select-message">Choose containers to upload documents</p>
      </div>
    </div>
    <div class="upload-doc-next">
      <button class="btn-primary-mro fri-step-2">Next</button>
    </div>
  </div>
  <!-- Upload FRI/LS Step 2 -->
  <!-- Upload FRI/LS Step 3 -->
  <div class="ud-step upload-fri-step-3">
    <div class="upload-doc-title">
      <div class="ud-title-left">
        <a href="#/" class="fri-step-3-back"><img src="assets/images/ud-back.svg" alt=""></a>
        <h3>Choose Containers</h3>
      </div>
      <!-- <div class="ud-title-right">
        <a href="#/" class="btn-primary-mro">Back</a>
      </div> -->
    </div>
    <div class="select-doc-wrapper">
      <div class="sd-left">
        <h4 class="sd-subtitle">Selected Containers</h4>
        <div class="ud-list mt20">
          <div class="ud-list-single">
            <label class="container-checkbox">6000001
              <input type="checkbox" checked>
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000002
              <input type="checkbox" checked>
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000003
              <input type="checkbox" checked>
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000004
              <input type="checkbox" checked>
              <span class="checkmark-checkbox"></span>
            </label>
          </div>
        </div>
      </div>
      <div class="sd-right">
        <h4 class="sd-subtitle mb20">Upload Document</h4>
        <input type="file">
      </div>
    </div>
    <div class="upload-doc-next">
      <button class="btn-primary-mro fri-step-3">Next</button>
    </div>
  </div>
  <!-- Upload FRI/LS Step 3 -->
  <!-- Upload FRI/LS Step 4 -->
  <div class="ud-step upload-fri-step-4">
    <div class="upload-doc-title">
      <div class="ud-title-left">
        <a href="#/" class="fri-step-4-back"><img src="assets/images/ud-back.svg" alt=""></a>
        <h3>Upload Document</h3>
      </div>
      <!-- <div class="ud-title-right">
        <a href="#/" class="btn-primary-mro">Back</a>
      </div> -->
    </div>
    <div class="select-doc-wrapper">
      <div class="sd-left">
        <h4 class="sd-subtitle">Containers</h4>
        <div class="ud-list ud-list-doc-names mt20">
          <div class="ud-list-single">
            <label class="container-checkbox">6000001
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000002
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000003
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">6000004
            </label>
          </div>
        </div>
      </div>
      <div class="sd-right">
        <h4 class="sd-subtitle">FRI Title</h4>
        <div class="ud-list ud-list-doc-names mt20">
          <div class="ud-list-single">
            <label class="container-checkbox">FRI00000018
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">FRI00000018
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">FRI00000018
            </label>
          </div>
          <div class="ud-list-single">
            <label class="container-checkbox">FRI00000018
            </label>
          </div>
        </div>
      </div>
    </div>
    <div class="upload-doc-next">
      <button class="btn-primary-mro fri-step-4">Next</button>
    </div>
  </div>
  <!-- Upload FRI/LS Step 4 -->
  <!-- Upload FRI/LS Step 5 -->
  <div class="ud-step upload-fri-step-5">
    <div class="upload-doc-title">
      <div class="ud-title-left">
        <a href="#/" class="fri-step-5-back"><img src="assets/images/ud-back.svg" alt=""></a>
        <h3>Review Document Titles</h3>
      </div>
      <div class="ud-title-right">
        <button class="btn-grey-mro">Clear</button>
        <button class="btn-primary-mro">Save</button>
      </div>
    </div>
    <div class="select-doc-wrapper">
      <div class="sd-left">
        <h4 class="sd-subtitle mb20">Enter Custom Data</h4>
        <div class="form-group ">
          <label for="">Custom Date Field</label>
          <input type="date" placeholder="Choose Date" class="input-form-mro">
        </div>
        <div class="form-group ">
          <label for="">Custom Type Field</label>
          <input type="date" placeholder="Enter Quantity" class="input-form-mro">
        </div>
        <div class="form-group ">
          <label for="">Custom Type Field</label>
          <input type="date" placeholder="Enter SKU" class="input-form-mro">
        </div>
        <div class="form-group ">
          <label for="">SKU</label>
          <input type="date" placeholder="Enter SKU" class="input-form-mro">
        </div>
        <div class="form-group">
          <a href="#/" class="add-remove-link"><img src="assets/images/add-more-dark.svg" alt=""> Add SKU</a>
        </div>
        <div class="form-group ">
          <label for="">Custom Type Field</label>
          <input type="date" placeholder="Enter Lot Number" class="input-form-mro">
        </div>
        <div class="form-group">
          <div class="form-group">
            <label for="">Multiselect List</label>
            <div class="multiselect-dropdown-wrapper">
              <div class="md-value">
                Select value
              </div>
              <div class="md-list-wrapper">
                <input type="text" placeholder="Search" class="md-search">
                <div class="md-list-items">
                  <div class="mdli-single">
                    <label class="container-checkbox">Listed Item 1
                      <input type="checkbox">
                      <span class="checkmark-checkbox"></span>
                    </label>
                  </div>
                  <div class="mdli-single">
                    <label class="container-checkbox">Listed Item 2
                      <input type="checkbox">
                      <span class="checkmark-checkbox"></span>
                    </label>
                  </div>
                  <div class="mdli-single">
                    <label class="container-checkbox">Listed Item 3
                      <input type="checkbox">
                      <span class="checkmark-checkbox"></span>
                    </label>
                  </div>
                  <div class="mdli-single">
                    <label class="container-checkbox">Listed Item 4
                      <input type="checkbox">
                      <span class="checkmark-checkbox"></span>
                    </label>
                  </div>
                  <div class="mdli-single">
                    <label class="container-checkbox">Listed Item 5
                      <input type="checkbox">
                      <span class="checkmark-checkbox"></span>
                    </label>
                  </div>
                  <div class="mdli-single">
                    <label class="container-checkbox">Listed Item 6
                      <input type="checkbox">
                      <span class="checkmark-checkbox"></span>
                    </label>
                  </div>
                  <div class="mdli-single">
                    <label class="container-checkbox">Listed Item 7
                      <input type="checkbox">
                      <span class="checkmark-checkbox"></span>
                    </label>
                  </div>
                </div>
                <div class="md-cta">
                  <a href="#/" class="btn-primary-mro md-done">Done</a>
                  <a href="#/" class="btn-secondary-mro md-clear">Clear All</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="">Multiselect List with Add New</label>
          <div class="multiselect-dropdown-wrapper">
            <div class="md-value">
              Select value
            </div>
            <div class="md-list-wrapper">
              <input type="text" placeholder="Search" class="md-search">
              <a href="#/" class="btn-secondary-mro md-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
              <div class="md-list-items">
                <div class="mdli-single">
                  <label class="container-checkbox">Listed Item 1
                    <input type="checkbox">
                    <span class="checkmark-checkbox"></span>
                  </label>
                </div>
                <div class="mdli-single">
                  <label class="container-checkbox">Listed Item 2
                    <input type="checkbox">
                    <span class="checkmark-checkbox"></span>
                  </label>
                </div>
                <div class="mdli-single">
                  <label class="container-checkbox">Listed Item 3
                    <input type="checkbox">
                    <span class="checkmark-checkbox"></span>
                  </label>
                </div>
                <div class="mdli-single">
                  <label class="container-checkbox">Listed Item 4
                    <input type="checkbox">
                    <span class="checkmark-checkbox"></span>
                  </label>
                </div>
                <div class="mdli-single">
                  <label class="container-checkbox">Listed Item 5
                    <input type="checkbox">
                    <span class="checkmark-checkbox"></span>
                  </label>
                </div>
                <div class="mdli-single">
                  <label class="container-checkbox">Listed Item 6
                    <input type="checkbox">
                    <span class="checkmark-checkbox"></span>
                  </label>
                </div>
                <div class="mdli-single">
                  <label class="container-checkbox">Listed Item 7
                    <input type="checkbox">
                    <span class="checkmark-checkbox"></span>
                  </label>
                </div>
              </div>
              <div class="md-cta">
                <a href="#/" class="btn-primary-mro md-done">Done</a>
                <a href="#/" class="btn-secondary-mro md-clear">Clear All</a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="sd-right">
        <h4 class="sd-subtitle">Form View</h4>

      </div>
    </div>
    <div class="upload-doc-next">
      <button class="btn-grey-mro">Clear</button>
      <button class="btn-primary-mro">Save</button>
    </div>
  </div>
  <!-- Upload FRI/LS Step 5 -->
</div>
<!-- Upload Document-->
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>