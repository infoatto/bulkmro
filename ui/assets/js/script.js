$(document).ready(function () {
  /*Toggle contact popup*/
  const $menu = $('.menu-dropdown-wrapper');
  $(document).mouseup(e => {
    if (!$menu.is(e.target) // if the target of the click isn't the container...
      && $menu.has(e.target).length === 0) // ... nor a descendant of the container
    {
      $menu.children('.menu-dropdown').removeClass('is-active');
      $menu.children('.profile-dropdown').removeClass('is-active');
      $('.menu-dropdown-icon').removeClass('rotate')
    }
  });
  $('.menu-link').on('click', () => {
    $menu.children('.menu-dropdown').toggleClass('is-active');
    $('.menu-link').children('.menu-dropdown-icon').toggleClass('rotate')
  });
  $('.profile-link').on('click', () => {
    $menu.children('.profile-dropdown').toggleClass('is-active');
    $('.profile-link').children('.menu-dropdown-icon').toggleClass('rotate')
  });
  /*Toggle contact popup*/
  /*Form scrollto link state*/
  $('.vertical-scroll-links a').click(function () {
    $('.vertical-scroll-links a').removeClass('active');
    $(this).addClass('active');
  })
  /*Form scrollto link state*/
  /*View order toggle*/
  $('.view-order-details').click(function () {
    $('tr.row-expand').hide();
    $(this).parent('td').parent('tr').next('tr.row-expand').show();
  })
  /*View order toggle*/
  /*Login toggle*/
  $('.fp-link').click(function () {
    $('.fp-form').show();
    $('.login-form').hide();
  })
  $('.login-link').click(function () {
    $('.login-form').show();
    $('.fp-form').hide();
  })
  /*Login toggle*/
  /*Delivery schedule toggle*/
  $('.load-ds').click(function () {
    $('.load-delivery-schedule').hide();
    $('.delivery-schedule-data').show();
  })

  $('.ds-toggle-icon').click(function () {

    if ($(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-expand-content').is(':visible')) {
      $(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-expand-content').hide();
      $('.ds-toggle-icon').removeClass('toggle');
    } else {
      $('.ds-expand-content').hide();
      $(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-expand-content').show();
      $('.ds-toggle-icon').removeClass('toggle');
      $(this).addClass('toggle');
    }

  })
  /*Delivery schedule toggle*/
  /*New Container toggle*/
  $('.select-master-contract').change(function () {
    $('.new-container-details').show();
  });
  /*New Container toggle*/
  /*Notification close*/
  $('.notification-close').click(function () {
    $('.notification-wrapper').hide();
  })
  /*Notification close*/
  /*Upload Documents*/
  $('.fri-step-1').on('click', function () {
    $('.upload-fri-step-2').show();
    $('.upload-fri-step-1').hide();
  });
  $('.fri-step-2').on('click', function () {
    $('.upload-fri-step-3').show();
    $('.upload-fri-step-2').hide();
  });
  $('.fri-step-3').on('click', function () {
    $('.upload-fri-step-4').show();
    $('.upload-fri-step-3').hide();
  });
  $('.fri-step-4').on('click', function () {
    $('.upload-fri-step-5').show();
    $('.upload-fri-step-4').hide();
  });
  /*Upload Documents*/
  /*Custom Dropdowns */
  $('.md-value').on('click', function () {
    if ($(this).siblings('.md-list-wrapper').hasClass('is-active')) {
      $('.md-list-wrapper').removeClass('is-active')
      $(this).siblings('.md-list-wrapper').removeClass('is-active')
    } else {
      $('.md-list-wrapper').removeClass('is-active')
      $(this).siblings('.md-list-wrapper').addClass('is-active')
    }
  });
  $('.sd-value').on('click', function () {
    if ($(this).siblings('.sd-list-wrapper').hasClass('is-active')) {
      $('.sd-list-wrapper').removeClass('is-active')
      $(this).siblings('.sd-list-wrapper').removeClass('is-active')
    } else {
      $('.sd-list-wrapper').removeClass('is-active')
      $(this).siblings('.sd-list-wrapper').addClass('is-active')
    }
  });
  $('.md-search').on('keyup', function () {
    var value = $(this).val().toLowerCase();
    $(this).siblings('.md-list-items').children('.mdli-single').children('label.container-checkbox').filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $('.sd-search').on('keyup', function () {
    var value = $(this).val().toLowerCase();
    $(this).siblings('.sd-list-items').children('.sdli-single').children('label').filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  /*Custom Dropdowns */
  /*Show selected checkbox count*/
  $('.md-list-items .mdli-single label  input[type="checkbox"]').on('click', function () {
    var countCheckedCheckboxes = $(this).parent('label').parent('.mdli-single').parent('.md-list-items').parent('.md-list-wrapper').find("input:checked").length;

    if (countCheckedCheckboxes === 0) {
      $(this).parent('label').parent('.mdli-single').parent('.md-list-items').parent('.md-list-wrapper').siblings('.md-value').text('Select value');
    } else {
      $(this).parent('label').parent('.mdli-single').parent('.md-list-items').parent('.md-list-wrapper').siblings('.md-value').text(countCheckedCheckboxes + ' selected');
    }
  })
  /*Show selected checkbox count*/
  $('.sd-list-items .sdli-single label').on('click', function () {
    var singleSelectValue = $(this).text();
    $(this).parent('.sdli-single').parent('.sd-list-items').parent('.sd-list-wrapper').siblings('.sd-value').text(singleSelectValue);
    $('.sd-list-wrapper').removeClass('is-active')
  });
  /*Multiselect Done*/
  $('.md-done').on('click', function () {
    $('.md-list-wrapper').removeClass('is-active')
  })
  /*Multiselect Done*/
  /*Multiselect Clear All*/
  $('.md-clear').on('click', function () {
    $(this).parent('.md-cta').siblings('.md-list-items').children('.mdli-single').children('label').children('input[type="checkbox"]').prop('checked', false);
    $(this).parent('.md-cta').parent('.md-list-wrapper').siblings('.md-value').text('Select value')
  })
  /*Multiselect Clear All*/

  /*Toggle dropdown*/
  const $menu2 = $('.multiselect-dropdown-wrapper');
  $(document).mouseup(e => {
    if (!$menu2.is(e.target) // if the target of the click isn't the container...
      && $menu2.has(e.target).length === 0) // ... nor a descendant of the container
    {
      $menu2.children('.md-list-wrapper').removeClass('is-active');
    }
  });
  const $menu3 = $('.singleselect-dropdown-wrapper');
  $(document).mouseup(e => {
    if (!$menu3.is(e.target) // if the target of the click isn't the container...
      && $menu3.has(e.target).length === 0) // ... nor a descendant of the container
    {
      $menu3.children('.sd-list-wrapper').removeClass('is-active');
    }
  });

  /*Toggle dropdown*/


  // In your Javascript (external .js resource or <script> tag)
  $(document).ready(function () {
    $('.basic-single').select2();
  });
});
