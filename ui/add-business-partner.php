<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="business-partner-listing.php">Business Partners</a>
              <span>></span>
              <p>Add New Business Partner</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title">Add New Business Partner</h1>
            </div>
          </div>
          <button type="submit" class="btn-primary-mro">Save</button>
        </div>
        <div class="page-content-wrapper">
          <div class="scroll-content-wrapper">

            <div class="scroll-content-left" id="bp-details">
              <form>

                <h3 class="form-group-title">Custom Dropdowns Demo Section</h3>
                <div class="form-row form-row-4">
                  <div class="form-group">
                    <label for="">Multiselect List</label>
                    <div class="multiselect-dropdown-wrapper">
                      <div class="md-value">
                        Select value
                      </div>
                      <div class="md-list-wrapper">
                        <input type="text" placeholder="Search" class="md-search">
                        <div class="md-list-items">
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 1
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 2
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 3
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 4
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 5
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 6
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 7
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                        </div>
                        <div class="md-cta">
                          <a href="#/" class="btn-primary-mro md-done">Done</a>
                          <a href="#/" class="btn-secondary-mro md-clear">Clear All</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Multiselect List with Add New</label>
                    <div class="multiselect-dropdown-wrapper">
                      <div class="md-value">
                        Select value
                      </div>
                      <div class="md-list-wrapper">
                        <input type="text" placeholder="Search" class="md-search">
                        <a href="#/" class="btn-secondary-mro md-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
                        <div class="md-list-items">
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 1
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 2
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 3
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 4
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 5
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 6
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 7
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                        </div>
                        <div class="md-cta">
                          <a href="#/" class="btn-primary-mro md-done">Done</a>
                          <a href="#/" class="btn-secondary-mro md-clear">Clear All</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Single Select List</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select value
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Select Value</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Single Select List with Add New</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select value
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <a href="#/" class="btn-secondary-mro sd-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
                        <div class="sd-list-items">

                          <div class="sdli-single">
                            <label>Select Value</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <hr class="separator mt0">
                <!-- <h3 class="form-group-title">Item Code</h3> -->
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Business Name<sup>*</sup></label>
                    <input type="text" placeholder="Enter Business Name" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Business Partner Type<sup>*</sup></label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select Partner Type
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Business Partner Category<sup>*</sup></label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select Partner Category
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Contact Person</label>
                    <input type="text" placeholder="Enter Contact Person Name" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Email Address</label>
                    <input type="text" placeholder="Enter Email Address" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Phone Number</label>
                    <input type="text" placeholder="Enter Phone Number" class="input-form-mro">
                  </div>
                </div>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Business Alias<sup>*</sup></label>
                    <input type="text" placeholder="Enter Business Alias" class="input-form-mro">
                  </div>
                </div>
                <hr class="separator mt0" id="billing-details">
                <h3 class="form-group-title">Billing Details</h3>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Contact Person</label>
                    <input type="text" placeholder="Enter Contact Person" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Contact Phone</label>
                    <input type="text" placeholder="Enter Contact Phone" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Contact Email</label>
                    <input type="text" placeholder="Enter Contact Email" class="input-form-mro">
                  </div>
                </div>
                <div class="form-row form-row-2">
                  <div class="form-group">
                    <label for="">Address Line 1</label>
                    <input type="text" placeholder="Enter Contact Person" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Address Line 2</label>
                    <input type="text" placeholder="Enter Contact Phone" class="input-form-mro">
                  </div>
                </div>
                <!-- <div class="form-row form-row-4">
                  <div class="form-group">
                    <label for="">Multiselect List</label>
                    <div class="multiselect-dropdown-wrapper">
                      <div class="md-value">
                        Select value
                      </div>
                      <div class="md-list-wrapper">
                        <input type="text" placeholder="Search" class="md-search">
                        <div class="md-list-items">
                          <div class="mdli-single">
                            <label class="container-checkbox">Vinayak
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 2
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 3
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 4
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 5
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 6
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 7
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                        </div>
                        <div class="md-cta">
                          <a href="#/" class="btn-primary-mro md-done">Done</a>
                          <a href="#/" class="btn-secondary-mro md-clear">Clear All</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Multiselect List with Add New</label>
                    <div class="multiselect-dropdown-wrapper">
                      <div class="md-value">
                        Select value
                      </div>
                      <div class="md-list-wrapper">
                        <input type="text" placeholder="Search" class="md-search">
                        <a href="#/" class="btn-secondary-mro md-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
                        <div class="md-list-items">
                          <div class="mdli-single">
                            <label class="container-checkbox">Vinayak
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 2
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 3
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 4
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 5
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 6
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                          <div class="mdli-single">
                            <label class="container-checkbox">Listed Item 7
                              <input type="checkbox">
                              <span class="checkmark-checkbox"></span>
                            </label>
                          </div>
                        </div>
                        <div class="md-cta">
                          <a href="#/" class="btn-primary-mro md-done">Done</a>
                          <a href="#/" class="btn-secondary-mro md-clear">Clear All</a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Single Select List</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select value
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Select Value</label>
                          </div>
                          <div class="sdli-single">
                            <label>Vinayak</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Single Select List with Add Newt</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select value
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <a href="#/" class="btn-secondary-mro sd-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
                          <div class="sdli-single">
                            <label>Select Value</label>
                          </div>
                          <div class="sdli-single">
                            <label>Vinayak</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
                <div class="form-row form-row-4">
                <div class="form-group">
                    <label for="">Country</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select Country
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">State</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select State
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">City</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select City
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Zip Code</label>
                    <input type="text" placeholder="Enter Zip Code" class="input-form-mro">
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group">
                    <a href="#" class="add-more-link"><img src="assets/images/add-more-dark.svg" alt=""> Add </a>
                  </div>
                </div>
                <hr class="separator mt0" id="pickup-details">
                <h3 class="form-group-title">Shipping or Pick Up Details</h3>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Contact Person</label>
                    <input type="text" placeholder="Enter Contact Person" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Contact Phone</label>
                    <input type="text" placeholder="Enter Contact Phone" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Contact Email</label>
                    <input type="text" placeholder="Enter Contact Email" class="input-form-mro">
                  </div>
                </div>
                <div class="form-row form-row-2">
                  <div class="form-group">
                    <label for="">Address Line 1</label>
                    <input type="text" placeholder="Enter Contact Person" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Address Line 2</label>
                    <input type="text" placeholder="Enter Contact Phone" class="input-form-mro">
                  </div>
                </div>
                <div class="form-row form-row-4">
                <div class="form-group">
                    <label for="">Country</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select Country
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">State</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select State
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">City</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select City
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Zip Code</label>
                    <input type="text" placeholder="Enter Zip Code" class="input-form-mro">
                  </div>
                </div>
                <h3 class="form-group-title">Shipping or Pick Up Details 2</h3>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Contact Person</label>
                    <input type="text" placeholder="Enter Contact Person" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Contact Phone</label>
                    <input type="text" placeholder="Enter Contact Phone" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Contact Email</label>
                    <input type="text" placeholder="Enter Contact Email" class="input-form-mro">
                  </div>
                </div>
                <div class="form-row form-row-2">
                  <div class="form-group">
                    <label for="">Address Line 1</label>
                    <input type="text" placeholder="Enter Contact Person" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Address Line 2</label>
                    <input type="text" placeholder="Enter Contact Phone" class="input-form-mro">
                  </div>
                </div>
                <div class="form-row form-row-4">
                  <div class="form-group">
                    <label for="">Country</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select Country
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">State</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select State
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">City</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select City
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Zip Code</label>
                    <input type="text" placeholder="Enter Zip Code" class="input-form-mro">
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-group">
                    <a href="#" class="add-more-link"><img src="assets/images/add-more-dark.svg" alt=""> Add </a>
                  </div>
                </div>
                <hr class="separator mt0" id="scroll-terms">
                <h3 class="form-group-title">Terms</h3>
                <div class="form-row form-row-2">
                  <div class="form-group">
                    <label for="">Payment Terms</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select Payment Terms
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <a href="#/" class="btn-secondary-mro sd-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Incoterm</label>
                    <div class="singleselect-dropdown-wrapper">
                      <div class="sd-value">
                        Select Incoterm
                      </div>
                      <div class="sd-list-wrapper">
                        <input type="text" placeholder="Search" class="sd-search">
                        <a href="#/" class="btn-secondary-mro sd-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
                        <div class="sd-list-items">
                          <div class="sdli-single">
                            <label>Listed Item 1</label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 2 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 3 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 4 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 5 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 6 </label>
                          </div>
                          <div class="sdli-single">
                            <label>Listed Item 7 </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="scroll-content-right">
              <div class="vertical-scroll-links">
                <a href="#bp-details" class="active">Business Partner Detail</a>
                <a href="#billing-details">Billing Address</a>
                <a href="#pickup-details">Shipping / Pick Up Address</a>
                <a href="#scroll-terms">Terms</a>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>