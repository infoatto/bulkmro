<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="business-partner-listing.php">Business Partners</a>
              <span>></span>
              <p>New Business Partner Contract</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title">Business Partner Contract</h1>
            </div>
          </div>
          <button type="submit" class="btn-primary-mro">Save</button>
        </div>
        <div class="page-content-wrapper">
          <div class="scroll-content-wrapper">
            <div class="scroll-content-left" id="contract-details">
              <form>
                <h3 class="form-group-title">Contract Details</h3>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Contract Number<sup>*</sup></label>
                    <input type="text" placeholder="Enter Contract Number" class="input-form-mro">
                  </div>
                  <div class="form-group">
                    <label for="">Business Partner<sup>*</sup></label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Business Partner</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                  </div>
                </div>
                <hr class="separator mt0" id="shipment-dates">
                <h3 class="form-group-title">Shipment Dates</h3>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Shipment Start Date<sup>*</sup></label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Start Date</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="">Duration in Weeks<sup>*</sup></label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Number of Weeks</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                  </div>
                </div>
                <hr class="separator mt0" id="pickup-details">
                <h3 class="form-group-title">Billing and Shipping Addresses</h3>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">Shipping Address</label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Shipping Address</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                    <div class="ba-single ba-select">
                      <p class="bas-title">Billing Address</p>
                      <p class="bas-text">
                        Panasonic India Ltd.
                        502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                      </p>
                      <p class="bas-title">Shipping Contact</p>
                      <p class="bas-text">Eric Campbell</p>
                      <p class="bas-title">Shipping Email</p>
                      <p class="bas-text">johndoe@domain.com</p>
                      <p class="bas-title">Shipping Phone</p>
                      <p class="bas-text">+1-912-695-3507</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="">Ship To or From Address</label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select Shipping Address</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                    <div class="ba-single ba-select">
                      <p class="bas-title">Shipping Address</p>
                      <p class="bas-text">
                        Panasonic India Ltd.
                        502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                      </p>
                      <p class="bas-title">Shipping Contact</p>
                      <p class="bas-text">Eric Campbell</p>
                      <p class="bas-title">Shipping Email</p>
                      <p class="bas-text">johndoe@domain.com</p>
                      <p class="bas-title">Shipping Phone</p>
                      <p class="bas-text">+1-912-695-3507</p>
                    </div>
                  </div>
                </div>
                <hr class="separator mt0" id="scroll-terms">
                <h3 class="form-group-title">Terms</h3>
                <div class="form-row form-row-3">
                  <div class="form-group input-edit">
                    <label for="">Payment Terms</label>
                    <input type="text" placeholder="Typed Response" class="input-form-mro" value="Payment Upon Delivery">
                    <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                  </div>
                  <div class="form-group input-edit">
                    <label for="">Incoterm</label>
                    <input type="text" placeholder="Typed Response" class="input-form-mro" value="120 days from date of invoice">
                    <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                  </div>
                </div>
                <hr class="separator mt0" id="add-skus">
                <h3 class="form-group-title">Add SKUs to Order</h3>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <label for="">SKU</label>
                    <select name="" id="" class="select-form-mro">
                      <option value="">Select SKU</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                  </div>
                  <div class="sku-list-wrapper add-sku-wrapper">
                    <div class="sit-row">
                      <div class="sit-single sku-item-code">
                        <p class="sit-single-title">Item Code</p>
                        <p class="sit-single-value">BM12345</p>
                      </div>
                      <div class="sit-single sku-pd">
                        <p class="sit-single-title">Product Description</p>
                        <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                      </div>
                      <div class="sit-single sku-size">
                        <p class="sit-single-title">Size</p>
                        <p class="sit-single-value">Small</p>
                      </div>
                      <div class="sit-single sku-qty">
                        <p class="sit-single-title">Quantity<sup>*</sup></p>
                        <input type="text" class="input-form-mro" placeholder="Enter quantity">
                      </div>
                      <div class="sit-single sku-price">
                        <p class="sit-single-title">Price per unit in USD<sup>*</sup></p>
                        <input type="text" class="input-form-mro" placeholder="Enter Price">
                      </div>
                      <div class="sit-single sku-remove">
                        <a href="#/" class="remove-sku"><img src="assets/images/remove-dark.svg" alt=""> Remove</a>
                      </div>
                    </div>
                    <div class="sit-row">
                      <div class="sit-single sku-item-code">
                        <p class="sit-single-title">Item Code</p>
                        <p class="sit-single-value">BM12345</p>
                      </div>
                      <div class="sit-single sku-pd">
                        <p class="sit-single-title">Product Description</p>
                        <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                      </div>
                      <div class="sit-single sku-size">
                        <p class="sit-single-title">Size</p>
                        <p class="sit-single-value">Small</p>
                      </div>
                      <div class="sit-single sku-qty">
                        <p class="sit-single-title">Quantity<sup>*</sup></p>
                        <input type="text" class="input-form-mro" placeholder="Enter quantity">
                      </div>
                      <div class="sit-single sku-price">
                        <p class="sit-single-title">Price per unit in USD<sup>*</sup></p>
                        <input type="text" class="input-form-mro" placeholder="Enter Price">
                      </div>
                      <div class="sit-single sku-remove">
                        <a href="#/" class="remove-sku"><img src="assets/images/remove-dark.svg" alt=""> Remove</a>
                      </div>
                    </div>
                    <div class="sit-row">
                      <div class="sit-single sku-item-code">
                        <p class="sit-single-title">Item Code</p>
                        <p class="sit-single-value">BM12345</p>
                      </div>
                      <div class="sit-single sku-pd">
                        <p class="sit-single-title">Product Description</p>
                        <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                      </div>
                      <div class="sit-single sku-size">
                        <p class="sit-single-title">Size</p>
                        <p class="sit-single-value">Small</p>
                      </div>
                      <div class="sit-single sku-qty">
                        <p class="sit-single-title">Quantity<sup>*</sup></p>
                        <input type="text" class="input-form-mro" placeholder="Enter quantity">
                      </div>
                      <div class="sit-single sku-price">
                        <p class="sit-single-title">Price per unit in USD<sup>*</sup></p>
                        <input type="text" class="input-form-mro" placeholder="Enter Price">
                      </div>
                      <div class="sit-single sku-remove">
                        <a href="#/" class="remove-sku"><img src="assets/images/remove-dark.svg" alt=""> Remove</a> 
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="scroll-content-right">
              <div class="vertical-scroll-links">
                <a href="#contract-details" class="active">Contract Details</a>
                <a href="#shipment-dates">Shipment Dates</a>
                <a href="#pickup-details">Billing and Shipping Addresses</a>
                <a href="#scroll-terms">Terms</a>
                <a href="#add-skus">Add SKUs to Order</a>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>