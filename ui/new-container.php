<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="#/">Dashboard</a>
              <span>></span>
              <a href="#/">All Containers</a>
              <span>></span>
              <p>New Container Details</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title">New Container Details</h1>
            </div>
          </div>
          <button type="submit" class="btn-primary-mro">Save</button>
        </div>
        <div class="page-content-wrapper master-order-wrapper">
          <div class="scroll-content-wrapper master-order">
            <div class="scroll-content-left master-order-left" id="container-details">
              <form>
                <h3 class="form-group-title">Container Details</h3>
                <div class="form-row form-row-3">
                  <div class="form-group input-edit">
                    <label for="">Container Number <sup>*</sup></label>
                    <input type="text" placeholder="Enter Contract Number" class="input-form-mro" value="60000022">
                    <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                  </div>
                  <div class="form-group">
                    <label for="">Master Contract<sup>*</sup></label>
                    <select name="" id="" class="select-form-mro select-master-contract">
                      <option value="">Select Master Contract</option>
                      <option value="">Option 1</option>
                      <option value="">Option 2</option>
                    </select>
                  </div>
                </div>
                <div class="new-container-details">
                  <hr class="separator mt0">
                  <div class="view-mo-wrapper">
                    <div class="sit-row">
                      <div class="sit-single">
                        <p class="sit-single-title">Customer Name</p>
                        <p class="sit-single-value">REM</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Customer Order</p>
                        <p class="sit-single-value"><a href="#/"><u>View Customer Order</u></a></p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Vendor Name</p>
                        <p class="sit-single-value">Hongray</p>
                      </div>
                      <div class="sit-single">
                        <p class="sit-single-title">Customer Order</p>
                        <p class="sit-single-value"><a href="#/"><u>View Vendor Order</u></a></p>
                      </div>
                    </div>
                  </div>
                  <hr class="separator mt0" id="sku-qty">
                  <h3 class="form-group-title">SKUs in Container</h3>
                  <div class="form-row form-row-3">
                    <div class="sku-list-wrapper add-sku-wrapper">
                      <div class="sit-row">
                        <div class="sit-single sku-item-code">
                          <p class="sit-single-title">Item Code</p>
                          <p class="sit-single-value">BM12345</p>
                        </div>
                        <div class="sit-single sku-pd">
                          <p class="sit-single-title">Product Description</p>
                          <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                        </div>
                        <div class="sit-single sku-size">
                          <p class="sit-single-title">Size</p>
                          <p class="sit-single-value">Small</p>
                        </div>
                        <div class="sit-single sku-qty">
                          <p class="sit-single-title">Quantity<sup>*</sup></p>
                          <input type="text" class="input-form-mro" placeholder="Enter quantity" value="110,000">
                        </div>
                      </div>
                      <div class="sit-row">
                        <div class="sit-single sku-item-code">
                          <p class="sit-single-title">Item Code</p>
                          <p class="sit-single-value">BM12345</p>
                        </div>
                        <div class="sit-single sku-pd">
                          <p class="sit-single-title">Product Description</p>
                          <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                        </div>
                        <div class="sit-single sku-size">
                          <p class="sit-single-title">Size</p>
                          <p class="sit-single-value">Small</p>
                        </div>
                        <div class="sit-single sku-qty">
                          <p class="sit-single-title">Quantity<sup>*</sup></p>
                          <input type="text" class="input-form-mro" placeholder="Enter quantity" value="110,000">
                        </div>
                      </div>
                      <div class="sit-row">
                        <div class="sit-single sku-item-code">
                          <p class="sit-single-title">Item Code</p>
                          <p class="sit-single-value">BM12345</p>
                        </div>
                        <div class="sit-single sku-pd">
                          <p class="sit-single-title">Product Description</p>
                          <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                        </div>
                        <div class="sit-single sku-size">
                          <p class="sit-single-title">Size</p>
                          <p class="sit-single-value">Small</p>
                        </div>
                        <div class="sit-single sku-qty">
                          <p class="sit-single-title">Quantity<sup>*</sup></p>
                          <input type="text" class="input-form-mro" placeholder="Enter quantity" value="110,000">
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr class="separator mt0" id="pickup-details">
                  <div class="form-row form-row-2">
                    <div class="form-group input-edit">
                      <label for="">Freight Forwarder</label>
                      <select name="" id="" class="select-form-mro">
                        <option value="">DB Schenker</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                      </select>
                      <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                    </div>
                    <div class="form-group input-edit">
                      <label for="">CHA</label>
                      <select name="" id="" class="select-form-mro">
                        <option value="">Seven Seas</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                      </select>
                      <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                    </div>
                  </div>
                  <div class="form-row form-row-4">
                    <div class="form-group">
                      <label for="">ETA</label>
                      <select name="" id="" class="select-form-mro">
                        <option value="">Select Date</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Revised ETA</label>
                      <select name="" id="" class="select-form-mro">
                        <option value="">Select Date</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">ETD</label>
                      <select name="" id="" class="select-form-mro">
                        <option value="">Select Date</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="">Revised ETD</label>
                      <select name="" id="" class="select-form-mro">
                        <option value="">Select Date</option>
                        <option value="">Option 1</option>
                        <option value="">Option 2</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-row form-row-2">
                    <div class="form-group input-edit">
                      <label for="">ETT</label>
                      <input type="text" placeholder="No ETA or ETD Entered" class="input-form-mro">
                      <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                    </div>
                    <div class="form-group input-edit">
                      <label for="">Revised ETT</label>
                      <input type="text" placeholder="No ETA or ETD Entered" class="input-form-mro">
                      <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                    </div>
                  </div>
                  <div class="form-row form-row-2">
                    <div class="form-group">
                      <label for="">Liner Name</label>
                      <input type="text" placeholder="Enter Liner Name" class="input-form-mro">
                    </div>
                    <div class="form-group ">
                      <label for="">Liner Tracker URL</label>
                      <input type="text" placeholder="Enter Liner Tracker URL" class="input-form-mro">
                    </div>
                  </div>
                  <div class="form-row form-row-2">
                    <div class="form-group">
                      <label for="">POL</label>
                      <input type="text" placeholder="Enter POL" class="input-form-mro">
                    </div>
                    <div class="form-group ">
                      <label for="">POD</label>
                      <input type="text" placeholder="Enter POD" class="input-form-mro">
                    </div>
                  </div>
                  <div class="form-row form-row-2">
                    <div class="form-group">
                      <label for="">Vessel Name</label>
                      <input type="text" placeholder="Enter Vessel Name" class="input-form-mro">
                    </div>
                    <div class="form-group">
                      <label for="">Vessel Tracker URL</label>
                      <input type="text" placeholder="Enter Vessel Tracker URL" class="input-form-mro">
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div class="scroll-content-right master-order-right">
              <div class="vertical-scroll-links">
                <a href="#container-details" class="active">Container</a>
                <a href="#sku-qty">SKU Quantities</a>
                <a href="#pickup-details">Shipping Details</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>