<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="#/">Dashboard</a>
              <span>></span>
              <p>SKU Master</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title">SKU Master</h1>
            </div>
          </div>
          <button type="submit" class="btn-primary-mro">Save</button>
        </div>
        <div class="page-content-wrapper">
          <form>
            <h3 class="form-group-title">Item Code</h3>
            <div class="form-row form-row-4">
              <div class="form-group">
                <label for="">BM SKU Number<sup>*</sup></label>
                <input type="text" placeholder="Enter BM SKU" class="input-form-mro">
              </div>
              <div class="form-group">
                <label for="">Vendor SKU</label>
                <input type="text" placeholder="Enter Vendor SKU" class="input-form-mro">
              </div>
            </div>
            <h3 class="form-group-title">Product Details</h3>
            <div class="form-row form-row-3">
              <div class="form-group">
                <label for="">Category<sup>*</sup></label>
                <div class="singleselect-dropdown-wrapper">
                  <div class="sd-value">
                    Select Product Category
                  </div>
                  <div class="sd-list-wrapper">
                    <input type="text" placeholder="Search" class="sd-search">
                    <div class="sd-list-items">
                      <div class="sdli-single">
                        <label>Listed Item 1</label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 2 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 3 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 4 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 5 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 6 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 7 </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="">Brand Name<sup>*</sup></label>
                <div class="singleselect-dropdown-wrapper">
                  <div class="sd-value">
                    Select Brand Name
                  </div>
                  <div class="sd-list-wrapper">
                    <input type="text" placeholder="Search" class="sd-search">
                    <a href="#/" class="btn-secondary-mro sd-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
                    <div class="sd-list-items">

                      <div class="sdli-single">
                        <label>Select Value</label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 1</label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 2 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 3 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 4 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 5 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 6 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 7 </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="">Manufacturer<sup>*</sup></label>
                <div class="singleselect-dropdown-wrapper">
                  <div class="sd-value">
                    Select Manufacturer
                  </div>
                  <div class="sd-list-wrapper">
                    <input type="text" placeholder="Search" class="sd-search">
                    <a href="#/" class="btn-secondary-mro sd-list-btn"><img src="assets/images/add-icon-dark.svg" alt=""> Add New [List Name]</a>
                    <div class="sd-list-items">

                      <div class="sdli-single">
                        <label>Select Value</label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 1</label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 2 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 3 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 4 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 5 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 6 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 7 </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-row form-row-3">
              <div class="form-group">
                <label for="">Product Name<sup>*</sup></label>
                <input type="text" placeholder="Enter Product Name" class="input-form-mro">
              </div>
              <div class="form-group">
                <label for="">Size</label>
                <div class="singleselect-dropdown-wrapper">
                  <div class="sd-value">
                    Select Size
                  </div>
                  <div class="sd-list-wrapper">
                    <input type="text" placeholder="Search" class="sd-search">
                    <div class="sd-list-items">
                      <div class="sdli-single">
                        <label>Listed Item 1</label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 2 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 3 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 4 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 5 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 6 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 7 </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="">UoM</label>
                <div class="singleselect-dropdown-wrapper">
                  <div class="sd-value">
                    Select UoM
                  </div>
                  <div class="sd-list-wrapper">
                    <input type="text" placeholder="Search" class="sd-search">
                    <div class="sd-list-items">
                      <div class="sdli-single">
                        <label>Listed Item 1</label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 2 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 3 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 4 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 5 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 6 </label>
                      </div>
                      <div class="sdli-single">
                        <label>Listed Item 7 </label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>