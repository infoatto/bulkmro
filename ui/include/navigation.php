<header class="bulkmro-nav-container">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="nav-wrapper">
          <div class="nav-wrapper-left">
            <a href="index.php" class="nav-logo"><img src="assets/images/BulkMRO_logo.svg" alt=""></a>
            <div class="nav-divider"></div>
            <div class="menu-dropdown-wrapper">
              <a href="#/" class="menu-link">
                Menu
                <img src="assets/images/down-menu-home.svg" alt="" class="menu-dropdown-icon">
              </a>

              <div class="menu-dropdown">
                <div class="dropdown-content">
                  <div class="dropdown-col-single">
                    <h3>View</h3>
                    <ul class="menu-links">
                      <li><a href="#/">Dashboard</a></li>
                      <li><a href="index.php">All Containers</a></li>
                      <li><a href="inventory-summary.php">Inventory Summary</a></li>
                      <li><a href="business-partner-listing.php">All Business Partners</a></li>
                      <li><a href="#/">All SKUs</a></li>
                    </ul>
                  </div>
                  <div class="dropdown-col-single">
                    <h3>Create New</h3>
                    <ul class="menu-links">
                      <li><a href="add-business-partner.php">Business Partner</a></li>
                      <li><a href="new-bp-order.php">Business Partner Order</a></li>
                      <li><a href="new-master-order.php">Master Order</a></li>
                      <li><a href="new-container.php">Container</a></li>
                    </ul>
                  </div>
                  <div class="dropdown-col-single">
                    <h3>View</h3>
                    <ul class="menu-links">
                      <li><a href="#/">Dashboard</a></li>
                      <li><a href="index.php">All Containers</a></li>
                      <li><a href="inventory-summary.php">Inventory Summary</a></li>
                      <li><a href="business-partner-listing.php">All Business Partners</a></li>
                      <li><a href="#/">All SKUs</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
            <div class="nav-divider"></div>
            <div class="menu-search">
              <form>
                <input type="text" id="search-admin" placeholder="Search">
              </form>
            </div>
          </div>
          <div class="nav-wrapper-right">
            <a href="#/"><img src="assets/images/notification-icon.svg" alt=""></a>
            <div class="nav-divider"></div>
            <a href="new-container.php" class="btn-secondary-mro"><img src="assets/images/add-icon-dark.svg" alt="">  New Container</a>
            <div class="menu-dropdown-wrapper">
              <a href="#/" class="profile-link">
                <img src="assets/images/avatar.png" alt="" class="avatar-profile">
                Profile
                <img src="assets/images/down-menu-home.svg" alt="" class="menu-dropdown-icon" style="width:8px;"> 
              </a>
              <div class="profile-dropdown">
                <div class="dropdown-content profile-dropdown-content">
                  <div class="dropdown-col-single">
                    <h3>My Profile</h3>
                    <ul class="menu-links">
                      <li><a href="#/">My Account</a></li>
                      <li><a href="#/">Change Password</a></li>
                      <li><a href="#/">Logout</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</header>