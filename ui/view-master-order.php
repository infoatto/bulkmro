<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="#/">Dashboard</a>
              <span>></span>
              <p>New Master Order</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title">Master Order</h1>
            </div>
          </div>
          <a href="#/" class="btn-primary-mro"><img src="assets/images/edit-icon-white.svg" alt=""> Update</a>
        </div>
        <div class="page-content-wrapper">
          <div class="scroll-content-wrapper">
            <div class="scroll-content-left" id="contract-details">
              <form>
                <h3 class="form-group-title">Order Details</h3>
                <div class="view-mo-wrapper">
                  <div class="sit-row">
                    <div class="sit-single">
                      <p class="sit-single-title">Customer</p>
                      <p class="sit-single-value">Remcoda</p>
                    </div>
                    <div class="sit-single">
                      <p class="sit-single-title">Vendor</p>
                      <p class="sit-single-value">Hongray</p>
                    </div>
                    <div class="sit-single">
                      <p class="sit-single-title">Master Order Number</p>
                      <p class="sit-single-value">123456-456780</p>
                    </div>
                  </div>
                  <div class="sit-row">
                    <div class="sit-single">
                      <p class="sit-single-title">CHA</p>
                      <p class="sit-single-value">Seven Seas</p>
                    </div>
                    <div class="sit-single">
                      <p class="sit-single-title">Freight Forwarder</p>
                      <p class="sit-single-value">N/A</p>
                    </div>
                  </div>
                </div>
                <hr class="separator mt0" id="shipment-dates">
                <h3 class="form-group-title">Shipment Dates</h3>
                <div class="view-mo-wrapper">
                  <div class="sit-row">
                    <div class="sit-single">
                      <p class="sit-single-title">Shipment Start Date</p>
                      <p class="sit-single-value">01-Jul-21</p>
                    </div>
                    <div class="sit-single">
                      <p class="sit-single-title">Duration in Weeks</p>
                      <p class="sit-single-value">15 Weeks</p>
                    </div>
                  </div>
                </div>
                <hr class="separator mt0" id="pickup-details">
                <h3 class="form-group-title">Billing and Shipping Addresses</h3>
                <div class="form-row form-row-3">
                  <div class="form-group">
                    <div class="ba-single ba-select">
                      <p class="bas-title">Billing Address</p>
                      <p class="bas-text">
                        Panasonic India Ltd.
                        502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                      </p>
                      <p class="bas-title">Shipping Contact</p>
                      <p class="bas-text">Eric Campbell</p>
                      <p class="bas-title">Shipping Email</p>
                      <p class="bas-text">johndoe@domain.com</p>
                      <p class="bas-title">Shipping Phone</p>
                      <p class="bas-text">+1-912-695-3507</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="ba-single ba-select">
                      <p class="bas-title">Shipping Address</p>
                      <p class="bas-text">
                        Panasonic India Ltd.
                        502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                      </p>
                      <p class="bas-title">Shipping Contact</p>
                      <p class="bas-text">Eric Campbell</p>
                      <p class="bas-title">Shipping Email</p>
                      <p class="bas-text">johndoe@domain.com</p>
                      <p class="bas-title">Shipping Phone</p>
                      <p class="bas-text">+1-912-695-3507</p>
                    </div>
                  </div>
                </div>
                <hr class="separator mt0" id="deliver-skus">
                <h3 class="form-group-title">SKUs to Deliver</h3>
                <div class="sku-list-wrapper">
                  <div class="sit-row">
                    <div class="sit-single sku-item-code">
                      <p class="sit-single-title">Item Code</p>
                      <p class="sit-single-value">BM12345</p>
                    </div>
                    <div class="sit-single sku-pd">
                      <p class="sit-single-title">Product Description</p>
                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                    </div>
                    <div class="sit-single sku-size">
                      <p class="sit-single-title">Size</p>
                      <p class="sit-single-value">Small</p>
                    </div>
                    <div class="sit-single sku-qty">
                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                      <p class="sit-single-value">100,000</p>
                    </div>
                  </div>
                  <div class="sit-row">
                    <div class="sit-single sku-item-code">
                      <p class="sit-single-title">Item Code</p>
                      <p class="sit-single-value">BM12345</p>
                    </div>
                    <div class="sit-single sku-pd">
                      <p class="sit-single-title">Product Description</p>
                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                    </div>
                    <div class="sit-single sku-size">
                      <p class="sit-single-title">Size</p>
                      <p class="sit-single-value">Small</p>
                    </div>
                    <div class="sit-single sku-qty">
                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                      <p class="sit-single-value">100,000</p>
                    </div>
                  </div>
                  <div class="sit-row">
                    <div class="sit-single sku-item-code">
                      <p class="sit-single-title">Item Code</p>
                      <p class="sit-single-value">BM12345</p>
                    </div>
                    <div class="sit-single sku-pd">
                      <p class="sit-single-title">Product Description</p>
                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                    </div>
                    <div class="sit-single sku-size">
                      <p class="sit-single-title">Size</p>
                      <p class="sit-single-value">Small</p>
                    </div>
                    <div class="sit-single sku-qty">
                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                      <p class="sit-single-value">100,000</p>
                    </div>
                  </div>
                </div>
                <hr class="separator mt0" id="delivery-schedule">                
                <div class="delivery-schedule-data view-delivery-data" style="display:block">
                  <div class="title-sec-wrapper ds-title-wrapper">
                    <h3 class="form-group-title">Delivery Schedule</h3>
                    <a href="#/" class="btn-primary-mro"><img src="assets/images/redo-white.svg" alt=""> Reload </a>
                  </div>
                  <div class="delivery-schedule-wrapper">
                    <div class="ds-row ds-row-top">
                      <div class="ds-month">Month</div>
                      <div class="ds-weeks"></div>
                      <div class="ds-sku">BM12345</div>
                      <div class="ds-sku">BM12345</div>
                      <div class="ds-sku">BM12345</div>
                      <div class="ds-sku">BM12345</div>
                      <div class="ds-sku">BM12345</div>
                      <div class="ds-sku">BM12345</div>
                    </div>
                    <!-- Single month wrapper -->
                    <div class="ds-single-wrapper">
                      <!-- Single month header -->
                      <div class="ds-row ds-row-header">
                        <div class="ds-month">
                          <a href="#/" class="ds-toggle-icon"></a> <span>July 2021</span>
                        </div>
                        <div class="ds-weeks"></div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                      </div>
                      <!-- Single month header -->
                      <!-- Single month content -->
                      <div class="ds-expand-content">
                        <div class="ds-row ds-row-week ds-row-week-header">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Weeks</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>19/07/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>26/07/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-total">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Recalculated Total</div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                        </div>
                      </div>
                      <!-- Single month content -->
                    </div>
                    <!-- Single month wrapper -->
                    <!-- Single month wrapper -->
                    <div class="ds-single-wrapper">
                      <!-- Single month header -->
                      <div class="ds-row ds-row-header">
                        <div class="ds-month">
                          <a href="#/" class="ds-toggle-icon"></a> <span>Aug 2021</span>
                        </div>
                        <div class="ds-weeks"></div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                      </div>
                      <!-- Single month header -->
                      <!-- Single month content -->
                      <div class="ds-expand-content">
                        <div class="ds-row ds-row-week ds-row-week-header">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Weeks</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>02/08/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>09/08/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>16/08/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>23/08/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>30/08/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-total">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Recalculated Total</div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                        </div>
                      </div>
                      <!-- Single month content -->
                    </div>
                    <!-- Single month wrapper -->
                    <!-- Single month wrapper -->
                    <div class="ds-single-wrapper">
                      <!-- Single month header -->
                      <div class="ds-row ds-row-header">
                        <div class="ds-month">
                          <a href="#/" class="ds-toggle-icon"></a> <span>Sep 2021</span>
                        </div>
                        <div class="ds-weeks"></div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                      </div>
                      <!-- Single month header -->
                      <!-- Single month content -->
                      <div class="ds-expand-content">
                        <div class="ds-row ds-row-week ds-row-week-header">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Weeks</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>19/09/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>26/09/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-total">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Recalculated Total</div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                        </div>
                      </div>
                      <!-- Single month content -->
                    </div>
                    <!-- Single month wrapper -->
                    <!-- Single month wrapper -->
                    <div class="ds-single-wrapper">
                      <!-- Single month header -->
                      <div class="ds-row ds-row-header">
                        <div class="ds-month">
                          <a href="#/" class="ds-toggle-icon"></a> <span>Oct 2021</span>
                        </div>
                        <div class="ds-weeks"></div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                        <div class="ds-sku">
                          <span>2000</span>
                        </div>
                      </div>
                      <!-- Single month header -->
                      <!-- Single month content -->
                      <div class="ds-expand-content">
                        <div class="ds-row ds-row-week ds-row-week-header">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Weeks</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                          <div class="ds-sku">BM12345</div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>19/10/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-week">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">
                            <span>26/10/21</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                          <div class="ds-sku">
                            <span>2000</span>
                          </div>
                        </div>
                        <div class="ds-row ds-row-total">
                          <div class="ds-month"></div>
                          <div class="ds-weeks">Recalculated Total</div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                          <div class="ds-sku">
                            100,000
                          </div>
                        </div>
                      </div>
                      <!-- Single month content -->
                    </div>
                    <!-- Single month wrapper -->
                  </div>
                </div>
              </form>
            </div>
            <div class="scroll-content-right">
              <div class="vertical-scroll-links">
                <a href="#contract-details" class="active">Contract Details</a>
                <a href="#shipment-dates">Shipment Dates</a>
                <a href="#pickup-details">Billing and Shipping Addresses</a>
                <a href="#deliver-skus">SKUs to Deliver</a>
                <a href="#delivery-schedule">Delivery Schedule</a>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
  </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>