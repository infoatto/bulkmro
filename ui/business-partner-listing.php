<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="#/">Home</a>
              <span>></span>
              <p>Dashboard</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title"><a href="#/" class="title-icon"><img src="assets/images/download-icon-dark.svg" alt=""></a> All Business Partners</h1>
            </div>
          </div>
          <div class="title-sec-right">
            <a href="#/" class="btn-secondary-mro"><img src="assets/images/add-icon-dark.svg" alt=""> New Business Partner Order</a>
            <a href="add-business-partner.php" class="btn-primary-mro"><img src="assets/images/add-icon-white.svg" alt=""> Add New Business Partner</a>
          </div>
        </div>
        <div class="filter-sec-wrapper">
          <div class="filter-search">
            <input type="text" class="filter-search-input" placeholder="Search by Container No., Document No....">
          </div>
          <select name="" id="" class="select-mro select-xl">
            <option value="">Business Partner Type</option>
            <option value="">1</option>
            <option value="">2</option>
          </select>
          <select name="" id="" class="select-mro select-xl">
            <option value="">Invoice Status</option>
            <option value="">1</option>
            <option value="">2</option>
          </select>
        </div>
        <div class="bp-list-wrapper">
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Sr. No.</th>
                  <th scope="col">Business Partner Name</th>
                  <th scope="col">Partner Type</th>
                  <th scope="col">Partner Category</th>
                  <th scope="col">Contact Person</th>
                  <th scope="col">Phone Number</th>
                  <th scope="col">Email Address</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>Sunder Electronics Instrumentation Engineers</td>
                  <td>Customer</td>
                  <td>Government</td>
                  <td>Robert Powell</td>
                  <td>+1-XXX-XXX-XXXX</td>
                  <td>info@companyname.com</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>Arwani Trading Company L.L.C.</td>
                  <td>Customer</td>
                  <td>Corporate</td>
                  <td>N/A</td>
                  <td>N/A</td>
                  <td>N/A</td>
                </tr>
                <tr>
                  <td>3</td>
                  <td>Sunder Electronics Instrumentation Engineers</td>
                  <td>Customer</td>
                  <td>Distributor</td>
                  <td>Robert Powell</td>
                  <td>+1-XXX-XXX-XXXX</td>
                  <td>info@companyname.com</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>Sunder Electronics Instrumentation Engineers</td>
                  <td>Customer</td>
                  <td>Government</td>
                  <td>Robert Powell</td>
                  <td>+1-XXX-XXX-XXXX</td>
                  <td>info@companyname.com</td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>Arwani Trading Company L.L.C.</td>
                  <td>Customer</td>
                  <td>Corporate</td>
                  <td>N/A</td>
                  <td>N/A</td>
                  <td>N/A</td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>Sunder Electronics Instrumentation Engineers</td>
                  <td>Customer</td>
                  <td>Distributor</td>
                  <td>Robert Powell</td>
                  <td>+1-XXX-XXX-XXXX</td>
                  <td>info@companyname.com</td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>