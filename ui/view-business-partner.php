<?php require_once('include/head.php') ?>
<?php require_once('include/navigation.php') ?>
<section class="main-sec">
  <div class="container">
    <div class="row">
      <div class="col-12">
        <div class="title-sec-wrapper">
          <div class="title-sec-left">
            <div class="breadcrumb">
              <a href="#/">Dashboard</a>
              <span>></span>
              <p>Business Partners</p>
            </div>
            <div class="page-title-wrapper">
              <h1 class="page-title">Business Partner Name</h1>
            </div>
          </div>
          <div class="title-sec-right">
            <a href="#/" class="btn-secondary-mro"><img src="assets/images/add-icon-dark.svg" alt=""> Create Business Partner Order</a>
            <a href="#/" class="btn-primary-mro"><img src="assets/images/edit-icon-white.svg" alt=""> Update</a>
          </div>
        </div>
        <div class="bp-tabs-wrapper">
          <div class="mro-tabs-container">
            <nav>
              <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-main-tab" data-toggle="tab" href="#nav-main" role="tab" aria-controls="nav-main " aria-selected="true">Main Details</a>
                <a class="nav-item nav-link" id="nav-orders-tab" data-toggle="tab" href="#nav-orders" role="tab" aria-controls="nav-orders" aria-selected="true">Orders</a>
              </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
              <div class="tab-pane fade show active" id="nav-main" role="tabpanel" aria-labelledby="nav-main-tab">
                <div class="tab-content-wrapper scroll-content-wrapper">
                  <div class="scroll-content-left" id="bp-details">
                    <h3 class="form-group-title">Main Contact Details</h3>
                    <div class="bp-view-details">
                      <div class="sit-wrapper">
                        <div class="sit-row">
                          <div class="sit-single">
                            <p class="sit-single-title">Business Alias</p>
                            <p class="sit-single-value">REM</p>
                          </div>
                          <div class="sit-single">
                            <p class="sit-single-title">Business Partner Type</p>
                            <p class="sit-single-value">Customer, Corporate</p>
                          </div>
                          <div class="sit-single">
                            <p class="sit-single-title">Contact Name</p>
                            <p class="sit-single-value">Mohd. Sha Navas</p>
                          </div>
                          <div class="sit-single">
                            <p class="sit-single-title">Email</p>
                            <p class="sit-single-value">n.mohd@ril.com</p>
                          </div>
                          <div class="sit-single">
                            <p class="sit-single-title">Phone</p>
                            <p class="sit-single-value">+7896541239</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <hr class="separator" id="billing-details">
                    <h3 class="form-group-title">Billing Details</h3>
                    <div class="billing-address-wrapper">
                      <div class="ba-single">
                        <p class="bas-title">Billing Address 1</p>
                        <p class="bas-text">
                          Panasonic India Ltd.
                          502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                        </p>
                        <p class="bas-title">Billing Contact</p>
                        <p class="bas-text">Eric Campbell</p>
                        <p class="bas-title">Billing Email</p>
                        <p class="bas-text">johndoe@domain.com</p>
                        <p class="bas-title">Billing Phone</p>
                        <p class="bas-text">+1-912-695-3507</p>
                      </div>
                      <div class="ba-single">
                        <p class="bas-title">Billing Address 2</p>
                        <p class="bas-text">
                          Panasonic India Ltd.
                          502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                        </p>
                        <p class="bas-title">Billing Contact</p>
                        <p class="bas-text">Eric Campbell</p>
                        <p class="bas-title">Billing Email</p>
                        <p class="bas-text">johndoe@domain.com</p>
                        <p class="bas-title">Billing Phone</p>
                        <p class="bas-text">+1-912-695-3507</p>
                      </div>
                      <div class="ba-single">
                        <p class="bas-title">Billing Address 3</p>
                        <p class="bas-text">
                          Panasonic India Ltd.
                          502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                        </p>
                        <p class="bas-title">Billing Contact</p>
                        <p class="bas-text">Eric Campbell</p>
                        <p class="bas-title">Billing Email</p>
                        <p class="bas-text">johndoe@domain.com</p>
                        <p class="bas-title">Billing Phone</p>
                        <p class="bas-text">+1-912-695-3507</p>
                      </div>
                      <div class="ba-single">
                        <p class="bas-title">Billing Address 4</p>
                        <p class="bas-text">
                          Panasonic India Ltd.
                          502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                        </p>
                        <p class="bas-title">Billing Contact</p>
                        <p class="bas-text">Eric Campbell</p>
                        <p class="bas-title">Billing Email</p>
                        <p class="bas-text">johndoe@domain.com</p>
                        <p class="bas-title">Billing Phone</p>
                        <p class="bas-text">+1-912-695-3507</p>
                      </div>

                    </div>
                    <hr class="separator mt0" id="pickup-details">
                    <h3 class="form-group-title">Shipping or Pickup Details</h3>
                    <div class="billing-address-wrapper">
                      <div class="ba-single">
                        <p class="bas-title">Shipping Address 1</p>
                        <p class="bas-text">
                          Panasonic India Ltd.
                          502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                        </p>
                        <p class="bas-title">Shipping Contact</p>
                        <p class="bas-text">Eric Campbell</p>
                        <p class="bas-title">Shipping Email</p>
                        <p class="bas-text">johndoe@domain.com</p>
                        <p class="bas-title">Shipping Phone</p>
                        <p class="bas-text">+1-912-695-3507</p>
                      </div>
                      <div class="ba-single">
                        <p class="bas-title">Shipping Address 1</p>
                        <p class="bas-text">
                          Panasonic India Ltd.
                          502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                        </p>
                        <p class="bas-title">Shipping Contact</p>
                        <p class="bas-text">Eric Campbell</p>
                        <p class="bas-title">Shipping Email</p>
                        <p class="bas-text">johndoe@domain.com</p>
                        <p class="bas-title">Shipping Phone</p>
                        <p class="bas-text">+1-912-695-3507</p>
                      </div>
                      <div class="ba-single">
                        <p class="bas-title">Shipping Address 1</p>
                        <p class="bas-text">
                          Panasonic India Ltd.
                          502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                        </p>
                        <p class="bas-title">Shipping Contact</p>
                        <p class="bas-text">Eric Campbell</p>
                        <p class="bas-title">Shipping Email</p>
                        <p class="bas-text">johndoe@domain.com</p>
                        <p class="bas-title">Shipping Phone</p>
                        <p class="bas-text">+1-912-695-3507</p>
                      </div>
                      <div class="ba-single">
                        <p class="bas-title">Shipping Address 1</p>
                        <p class="bas-text">
                          Panasonic India Ltd.
                          502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                        </p>
                        <p class="bas-title">Shipping Contact</p>
                        <p class="bas-text">Eric Campbell</p>
                        <p class="bas-title">Shipping Email</p>
                        <p class="bas-text">johndoe@domain.com</p>
                        <p class="bas-title">Shipping Phone</p>
                        <p class="bas-text">+1-912-695-3507</p>
                      </div>
                    </div>
                    <hr class="separator mt0" id="scroll-terms">
                    <h3 class="form-group-title">Terms</h3>
                    <div class="bp-view-details">
                      <div class="sit-wrapper">
                        <div class="sit-row">
                          <div class="sit-single">
                            <p class="sit-single-title">Payment Terms</p>
                            <p class="sit-single-value">Payment Upon Delivery</p>
                          </div>
                          <div class="sit-single">
                            <p class="sit-single-title">Incoterm</p>
                            <p class="sit-single-value">DDP</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="scroll-content-right">
                    <div class="vertical-scroll-links">
                      <a href="#bp-details" class="active">Business Partner Detail</a>
                      <a href="#billing-details">Billing Address</a>
                      <a href="#pickup-details">Shipping / Pick Up Address</a>
                      <a href="#scroll-terms">Terms</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane fade" id="nav-orders" role="tabpanel" aria-labelledby="nav-orders-tab">
                <div class="tab-content-wrapper">
                  <div class="table-responsive">
                    <table class="table list-table">
                      <thead>
                        <tr>
                          <th scope="col">Contract Number</th>
                          <th scope="col">Shipment Start Date</th>
                          <th scope="col">Duration in Weeks</th>
                          <th scope="col">Product Category</th>
                          <th scope="col">Linked Master Order</th>
                          <th></th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>123456</td>
                          <td>01-Jun-21</td>
                          <td>15 Weeks</td>
                          <td>Gloves</td>
                          <td>No Master Order Yet</td>
                          <td>
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                          <td><a href="#/" class="view-order-details"><img src="assets/images/expand-icon.svg" alt=""> </a></td>
                        </tr>
                        <tr class="row-expand">
                          <td colspan="7">
                            <div class="order-details-wrapper">
                              <h3 class="form-group-title">Contract Details - 1</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group input-edit">
                                  <label for="">Business Partner Contract Number</label>
                                  <input type="text" placeholder="Typed Response" class="input-form-mro">
                                  <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                                </div>
                                <div class="form-group">
                                  <label for="">Business Partner<sup>*</sup></label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Business Partner</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Shipment Dates</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">Shipment Start Date<sup>*</sup></label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Start Date</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="">Duration in Weeks<sup>*</sup></label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Number of Weeks</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Billing and Shipping Addresses</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">Shipping Address</label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Shipping Address</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                  <div class="ba-single ba-select">
                                    <p class="bas-title">Billing Address</p>
                                    <p class="bas-text">
                                      Panasonic India Ltd.
                                      502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                                    </p>
                                    <p class="bas-title">Shipping Contact</p>
                                    <p class="bas-text">Eric Campbell</p>
                                    <p class="bas-title">Shipping Email</p>
                                    <p class="bas-text">johndoe@domain.com</p>
                                    <p class="bas-title">Shipping Phone</p>
                                    <p class="bas-text">+1-912-695-3507</p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="">Ship To or From Address</label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Shipping Address</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                  <div class="ba-single ba-select">
                                    <p class="bas-title">Shipping Address</p>
                                    <p class="bas-text">
                                      Panasonic India Ltd.
                                      502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                                    </p>
                                    <p class="bas-title">Shipping Contact</p>
                                    <p class="bas-text">Eric Campbell</p>
                                    <p class="bas-title">Shipping Email</p>
                                    <p class="bas-text">johndoe@domain.com</p>
                                    <p class="bas-title">Shipping Phone</p>
                                    <p class="bas-text">+1-912-695-3507</p>
                                  </div>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Terms</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group input-edit">
                                  <label for="">Payment Terms</label>
                                  <input type="text" placeholder="Typed Response" class="input-form-mro" value="Payment Upon Delivery">
                                  <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                                </div>
                                <div class="form-group input-edit">
                                  <label for="">Incoterm</label>
                                  <input type="text" placeholder="Typed Response" class="input-form-mro" value="120 days from date of invoice">
                                  <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Add SKUs to Order</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">SKU</label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select SKU</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                                <div class="sku-list-wrapper">
                                  <div class="sit-row">
                                    <div class="sit-single sku-item-code">
                                      <p class="sit-single-title">Item Code</p>
                                      <p class="sit-single-value">BM12345</p>
                                    </div>
                                    <div class="sit-single sku-pd">
                                      <p class="sit-single-title">Product Description</p>
                                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                                    </div>
                                    <div class="sit-single sku-size">
                                      <p class="sit-single-title">Size</p>
                                      <p class="sit-single-value">Small</p>
                                    </div>
                                    <div class="sit-single sku-qty">
                                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                                      <p class="sit-single-value">100,000</p>
                                    </div>
                                    <div class="sit-single sku-price">
                                      <p class="sit-single-title">Price per unit in USD</p>
                                      <p class="sit-single-value">$1.00</p>
                                    </div>                                    
                                  </div> 
                                  <div class="sit-row">
                                    <div class="sit-single sku-item-code">
                                      <p class="sit-single-title">Item Code</p>
                                      <p class="sit-single-value">BM12345</p>
                                    </div>
                                    <div class="sit-single sku-pd">
                                      <p class="sit-single-title">Product Description</p>
                                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                                    </div>
                                    <div class="sit-single sku-size">
                                      <p class="sit-single-title">Size</p>
                                      <p class="sit-single-value">Small</p>
                                    </div>
                                    <div class="sit-single sku-qty">
                                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                                      <p class="sit-single-value">100,000</p>
                                    </div>
                                    <div class="sit-single sku-price">
                                      <p class="sit-single-title">Price per unit in USD</p>
                                      <p class="sit-single-value">$1.00</p>
                                    </div>                                    
                                  </div>
                                  <div class="sit-row">
                                    <div class="sit-single sku-item-code">
                                      <p class="sit-single-title">Item Code</p>
                                      <p class="sit-single-value">BM12345</p>
                                    </div>
                                    <div class="sit-single sku-pd">
                                      <p class="sit-single-title">Product Description</p>
                                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                                    </div>
                                    <div class="sit-single sku-size">
                                      <p class="sit-single-title">Size</p>
                                      <p class="sit-single-value">Small</p>
                                    </div>
                                    <div class="sit-single sku-qty">
                                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                                      <p class="sit-single-value">100,000</p>
                                    </div>
                                    <div class="sit-single sku-price">
                                      <p class="sit-single-title">Price per unit in USD</p>
                                      <p class="sit-single-value">$1.00</p>
                                    </div>                                    
                                  </div>                                 
                                </div>                              
                              </div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>123456</td>
                          <td>01-Jun-21</td>
                          <td>15 Weeks</td>
                          <td>Gloves</td>
                          <td>No Master Order Yet</td>
                          <td>
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                          <td><a href="#/" class="view-order-details"><img src="assets/images/expand-icon.svg" alt=""> </a></td>
                        </tr>
                        <tr class="row-expand">
                          <td colspan="7">
                            <div class="order-details-wrapper">
                              <h3 class="form-group-title">Contract Details - 2</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group input-edit">
                                  <label for="">Business Partner Contract Number</label>
                                  <input type="text" placeholder="Typed Response" class="input-form-mro">
                                  <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                                </div>
                                <div class="form-group">
                                  <label for="">Business Partner<sup>*</sup></label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Business Partner</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Shipment Dates</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">Shipment Start Date<sup>*</sup></label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Start Date</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="">Duration in Weeks<sup>*</sup></label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Number of Weeks</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Billing and Shipping Addresses</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">Shipping Address</label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Shipping Address</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                  <div class="ba-single ba-select">
                                    <p class="bas-title">Billing Address</p>
                                    <p class="bas-text">
                                      Panasonic India Ltd.
                                      502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                                    </p>
                                    <p class="bas-title">Shipping Contact</p>
                                    <p class="bas-text">Eric Campbell</p>
                                    <p class="bas-title">Shipping Email</p>
                                    <p class="bas-text">johndoe@domain.com</p>
                                    <p class="bas-title">Shipping Phone</p>
                                    <p class="bas-text">+1-912-695-3507</p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="">Ship To or From Address</label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Shipping Address</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                  <div class="ba-single ba-select">
                                    <p class="bas-title">Shipping Address</p>
                                    <p class="bas-text">
                                      Panasonic India Ltd.
                                      502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                                    </p>
                                    <p class="bas-title">Shipping Contact</p>
                                    <p class="bas-text">Eric Campbell</p>
                                    <p class="bas-title">Shipping Email</p>
                                    <p class="bas-text">johndoe@domain.com</p>
                                    <p class="bas-title">Shipping Phone</p>
                                    <p class="bas-text">+1-912-695-3507</p>
                                  </div>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Terms</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group input-edit">
                                  <label for="">Payment Terms</label>
                                  <input type="text" placeholder="Typed Response" class="input-form-mro" value="Payment Upon Delivery">
                                  <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                                </div>
                                <div class="form-group input-edit">
                                  <label for="">Incoterm</label>
                                  <input type="text" placeholder="Typed Response" class="input-form-mro" value="120 days from date of invoice">
                                  <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Add SKUs to Order</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">SKU</label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select SKU</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                                <div class="sku-list-wrapper">
                                  <div class="sit-row">
                                    <div class="sit-single sku-item-code">
                                      <p class="sit-single-title">Item Code</p>
                                      <p class="sit-single-value">BM12345</p>
                                    </div>
                                    <div class="sit-single sku-pd">
                                      <p class="sit-single-title">Product Description</p>
                                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                                    </div>
                                    <div class="sit-single sku-size">
                                      <p class="sit-single-title">Size</p>
                                      <p class="sit-single-value">Small</p>
                                    </div>
                                    <div class="sit-single sku-qty">
                                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                                      <p class="sit-single-value">100,000</p>
                                    </div>
                                    <div class="sit-single sku-price">
                                      <p class="sit-single-title">Price per unit in USD</p>
                                      <p class="sit-single-value">$1.00</p>
                                    </div>                                    
                                  </div> 
                                  <div class="sit-row">
                                    <div class="sit-single sku-item-code">
                                      <p class="sit-single-title">Item Code</p>
                                      <p class="sit-single-value">BM12345</p>
                                    </div>
                                    <div class="sit-single sku-pd">
                                      <p class="sit-single-title">Product Description</p>
                                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                                    </div>
                                    <div class="sit-single sku-size">
                                      <p class="sit-single-title">Size</p>
                                      <p class="sit-single-value">Small</p>
                                    </div>
                                    <div class="sit-single sku-qty">
                                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                                      <p class="sit-single-value">100,000</p>
                                    </div>
                                    <div class="sit-single sku-price">
                                      <p class="sit-single-title">Price per unit in USD</p>
                                      <p class="sit-single-value">$1.00</p>
                                    </div>                                    
                                  </div>
                                  <div class="sit-row">
                                    <div class="sit-single sku-item-code">
                                      <p class="sit-single-title">Item Code</p>
                                      <p class="sit-single-value">BM12345</p>
                                    </div>
                                    <div class="sit-single sku-pd">
                                      <p class="sit-single-title">Product Description</p>
                                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                                    </div>
                                    <div class="sit-single sku-size">
                                      <p class="sit-single-title">Size</p>
                                      <p class="sit-single-value">Small</p>
                                    </div>
                                    <div class="sit-single sku-qty">
                                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                                      <p class="sit-single-value">100,000</p>
                                    </div>
                                    <div class="sit-single sku-price">
                                      <p class="sit-single-title">Price per unit in USD</p>
                                      <p class="sit-single-value">$1.00</p>
                                    </div>                                    
                                  </div>                                 
                                </div>                              
                              </div>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>123456</td>
                          <td>01-Jun-21</td>
                          <td>15 Weeks</td>
                          <td>Gloves</td>
                          <td>No Master Order Yet</td>
                          <td>
                            <div class="cs-status-text">
                              <p class="last-update-title">Last Updated</p>
                              <p class="last-update-name">Nitin N, 19-Mar-21</p>
                            </div>
                          </td>
                          <td><a href="#/" class="view-order-details"><img src="assets/images/expand-icon.svg" alt=""> </a></td>
                        </tr>
                        <tr class="row-expand">
                          <td colspan="7">
                            <div class="order-details-wrapper">
                              <h3 class="form-group-title">Contract Details - 3</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group input-edit">
                                  <label for="">Business Partner Contract Number</label>
                                  <input type="text" placeholder="Typed Response" class="input-form-mro">
                                  <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                                </div>
                                <div class="form-group">
                                  <label for="">Business Partner<sup>*</sup></label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Business Partner</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Shipment Dates</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">Shipment Start Date<sup>*</sup></label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Start Date</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <label for="">Duration in Weeks<sup>*</sup></label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Number of Weeks</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Billing and Shipping Addresses</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">Shipping Address</label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Shipping Address</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                  <div class="ba-single ba-select">
                                    <p class="bas-title">Billing Address</p>
                                    <p class="bas-text">
                                      Panasonic India Ltd.
                                      502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                                    </p>
                                    <p class="bas-title">Shipping Contact</p>
                                    <p class="bas-text">Eric Campbell</p>
                                    <p class="bas-title">Shipping Email</p>
                                    <p class="bas-text">johndoe@domain.com</p>
                                    <p class="bas-title">Shipping Phone</p>
                                    <p class="bas-text">+1-912-695-3507</p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="">Ship To or From Address</label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select Shipping Address</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                  <div class="ba-single ba-select">
                                    <p class="bas-title">Shipping Address</p>
                                    <p class="bas-text">
                                      Panasonic India Ltd.
                                      502-503 Windfall Building Sahar Plaza Complex, J B Nagar, Andheri East, Mumbai, Maharashtra 400059
                                    </p>
                                    <p class="bas-title">Shipping Contact</p>
                                    <p class="bas-text">Eric Campbell</p>
                                    <p class="bas-title">Shipping Email</p>
                                    <p class="bas-text">johndoe@domain.com</p>
                                    <p class="bas-title">Shipping Phone</p>
                                    <p class="bas-text">+1-912-695-3507</p>
                                  </div>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Terms</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group input-edit">
                                  <label for="">Payment Terms</label>
                                  <input type="text" placeholder="Typed Response" class="input-form-mro" value="Payment Upon Delivery">
                                  <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                                </div>
                                <div class="form-group input-edit">
                                  <label for="">Incoterm</label>
                                  <input type="text" placeholder="Typed Response" class="input-form-mro" value="120 days from date of invoice">
                                  <a href="#/"><img src="assets/images/edit-icon.svg" alt=""></a>
                                </div>
                              </div>
                              <hr class="separator mt0">
                              <h3 class="form-group-title">Add SKUs to Order</h3>
                              <div class="form-row form-row-3">
                                <div class="form-group">
                                  <label for="">SKU</label>
                                  <select name="" id="" class="select-form-mro">
                                    <option value="">Select SKU</option>
                                    <option value="">Option 1</option>
                                    <option value="">Option 2</option>
                                  </select>
                                </div>
                                <div class="sku-list-wrapper">
                                  <div class="sit-row">
                                    <div class="sit-single sku-item-code">
                                      <p class="sit-single-title">Item Code</p>
                                      <p class="sit-single-value">BM12345</p>
                                    </div>
                                    <div class="sit-single sku-pd">
                                      <p class="sit-single-title">Product Description</p>
                                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                                    </div>
                                    <div class="sit-single sku-size">
                                      <p class="sit-single-title">Size</p>
                                      <p class="sit-single-value">Small</p>
                                    </div>
                                    <div class="sit-single sku-qty">
                                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                                      <p class="sit-single-value">100,000</p>
                                    </div>
                                    <div class="sit-single sku-price">
                                      <p class="sit-single-title">Price per unit in USD</p>
                                      <p class="sit-single-value">$1.00</p>
                                    </div>                                    
                                  </div> 
                                  <div class="sit-row">
                                    <div class="sit-single sku-item-code">
                                      <p class="sit-single-title">Item Code</p>
                                      <p class="sit-single-value">BM12345</p>
                                    </div>
                                    <div class="sit-single sku-pd">
                                      <p class="sit-single-title">Product Description</p>
                                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                                    </div>
                                    <div class="sit-single sku-size">
                                      <p class="sit-single-title">Size</p>
                                      <p class="sit-single-value">Small</p>
                                    </div>
                                    <div class="sit-single sku-qty">
                                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                                      <p class="sit-single-value">100,000</p>
                                    </div>
                                    <div class="sit-single sku-price">
                                      <p class="sit-single-title">Price per unit in USD</p>
                                      <p class="sit-single-value">$1.00</p>
                                    </div>                                    
                                  </div>
                                  <div class="sit-row">
                                    <div class="sit-single sku-item-code">
                                      <p class="sit-single-title">Item Code</p>
                                      <p class="sit-single-value">BM12345</p>
                                    </div>
                                    <div class="sit-single sku-pd">
                                      <p class="sit-single-title">Product Description</p>
                                      <p class="sit-single-value">Synman Basic Vinyl Exam Gloves, Blue, Box 0f 100</p>
                                    </div>
                                    <div class="sit-single sku-size">
                                      <p class="sit-single-title">Size</p>
                                      <p class="sit-single-value">Small</p>
                                    </div>
                                    <div class="sit-single sku-qty">
                                      <p class="sit-single-title">Quantity<sup>*</sup></p>
                                      <p class="sit-single-value">100,000</p>
                                    </div>
                                    <div class="sit-single sku-price">
                                      <p class="sit-single-title">Price per unit in USD</p>
                                      <p class="sit-single-value">$1.00</p>
                                    </div>                                    
                                  </div>                                 
                                </div>                              
                              </div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php require_once('include/footer.php') ?>
<?php require_once('include/footer-scripts.php') ?>
</body>

</html>