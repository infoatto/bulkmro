$(document).ready(function () {
  /*Toggle contact popup*/
  const $menu = $('.menu-dropdown-wrapper');
  $(document).mouseup(e => {
    if (!$menu.is(e.target) // if the target of the click isn't the container...
      && $menu.has(e.target).length === 0) // ... nor a descendant of the container
    {
      $menu.children('.menu-dropdown').removeClass('is-active');
      $menu.children('.profile-dropdown').removeClass('is-active');
      $('.menu-dropdown-icon').removeClass('rotate')
    }
  });
  $('.menu-link').on('click', () => {
    $menu.children('.menu-dropdown').toggleClass('is-active');
    $('.menu-link').children('.menu-dropdown-icon').toggleClass('rotate')
  });
  $('.profile-link').on('click', () => {
    $menu.children('.profile-dropdown').toggleClass('is-active');
    $('.profile-link').children('.menu-dropdown-icon').toggleClass('rotate')
  });
  /*Toggle contact popup*/
  /*Form scrollto link state*/
  $('.vertical-scroll-links a').click(function () {
    $('.vertical-scroll-links a').removeClass('active');
    $(this).addClass('active');
  })
  /*Form scrollto link state*/
  /*View order toggle*/
  $('.view-order-details').click(function () {
    $('tr.row-expand').hide();
    $(this).parent('td').parent('tr').next('tr.row-expand').show();
  })
  /*View order toggle*/
  /*Login toggle*/
  $('.fp-link').click(function () {
    $('.fp-form').show();
    $('.login-form').hide();
  })
  $('.login-link').click(function () {
    $('.login-form').show();
    $('.fp-form').hide();
  })
  /*Login toggle*/
  /*Delivery schedule toggle*/
  $('.load-ds').click(function () {
    $('.load-delivery-schedule').hide();
    $('.delivery-schedule-data').show();
  })

  /* $(document).on("click", ".ds-toggle-icon", function () {

    if ($(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-expand-content').is(':visible')) {
      $(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-expand-content,.ds-row-total').hide();
      $('.ds-toggle-icon').removeClass('toggle');
    } else {
      $('.ds-expand-content,.ds-row-total').hide();
      $(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-expand-content').show();
      $(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-row-total').css({ 'display': 'flex' });
      $('.ds-toggle-icon').removeClass('toggle');
      $(this).addClass('toggle');
    }

  }) */
  $(document).on("click", ".ds-toggle-icon", function () {

    if ($(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-expand-content').is(':visible')) {
      $(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-expand-content,.ds-row-total').hide();
      //$('.ds-toggle-icon').removeClass('toggle');
      $(this).removeClass('toggle');
    } else {
      //$('.ds-expand-content,.ds-row-total').hide();
      $(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-expand-content').show();
      $(this).parent('.ds-month').parent('.ds-row-header').siblings('.ds-row-total').css({ 'display': 'flex' });
      //$('.ds-toggle-icon').removeClass('toggle');
      $(this).addClass('toggle');
    }

  })
  /*  var totalWidth = 0;  
   $('.ds-row-week div').each(function(index) {
       totalWidth += Math.abs(parseInt($(this).width(), 10));   
   });
   $('.ds-row-header').css({'width':totalWidth-800}); */

  /*Delivery schedule toggle*/
  /*New Container toggle*/
  $('.select-master-contract').change(function () {
    $('.new-container-details').show();
  });
  /*New Container toggle*/
  /*Notification close*/
  $('.notification-close').click(function () {
    $('.notification-wrapper').hide();
  })
  /*Notification close*/
  /*Upload Documents*/
  // $('.fri-step-1').on('click', function () {
  //   $('.upload-fri-step-2').show();
  //   $('.upload-fri-step-1').hide();
  // });
  $('.fri-step-1').on('click', function () {
    var upload_type = $('input[name="upload_type"]:checked').val();
    // alert(upload_type);
    // return false;
    if (upload_type == 'upload-invoice') {
      $("#document_type_div").hide();
    } else {
      $("#business_partner_div").hide();
    }
    $('.upload-fri-step-2').show();
    $('.upload-fri-step-1').hide();
  });

  $('.fri-step-2').on('click', function () {
    // $('.upload-fri-step-3').show();
    // $('.upload-fri-step-2').hide();
  });
  $('.fri-step-3').on('click', function () {
    // $('.upload-fri-step-4').show();
    // $('.upload-fri-step-3').hide();
  });
  $('.fri-step-4').on('click', function () {
    $('.upload-fri-step-5').show();
    $('.upload-fri-step-4').hide();
  });

  /*Upload Documents*/
  /*Custom Dropdowns */
  $('body').on('click', '.md-value', function () {
    if ($(this).siblings('.md-list-wrapper').hasClass('is-active')) {
      $('.md-list-wrapper').removeClass('is-active')
      $(this).siblings('.md-list-wrapper').removeClass('is-active')
    } else {
      $('.md-list-wrapper').removeClass('is-active')
      $(this).siblings('.md-list-wrapper').addClass('is-active')
    }
  });
  /*$('.md-value').on('click', function () {	  
    if ($(this).siblings('.md-list-wrapper').hasClass('is-active')) {
      $('.md-list-wrapper').removeClass('is-active')
      $(this).siblings('.md-list-wrapper').removeClass('is-active')
    } else {
      $('.md-list-wrapper').removeClass('is-active')
      $(this).siblings('.md-list-wrapper').addClass('is-active')
    }
  });*/
  // $('.sd-value').on('click', function () {
  $('body').on('click', '.sd-value', function () {
    if ($(this).siblings('.sd-list-wrapper').hasClass('is-active')) {
      $('.sd-list-wrapper').removeClass('is-active')
      $(this).siblings('.sd-list-wrapper').removeClass('is-active')
    } else {
      $('.sd-list-wrapper').removeClass('is-active')
      $(this).siblings('.sd-list-wrapper').addClass('is-active')
    }
  });
  $('body').on('keyup', '.md-search', function () {
    var value = $(this).val().toLowerCase();
    //console.log(value);
    $(this).siblings('.md-list-items').children('.mdli-single').children('label.container-checkbox').filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
  $('body').on('keyup', '.sd-search', function () {
    var value = $(this).val().toLowerCase();
    $(this).siblings('.sd-list-items').children('.sdli-single').children('label').filter(function () {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  /*Custom Dropdowns */
  /*Show selected checkbox count*/
  $('body').on('click', '.md-list-items .mdli-single label  input[type="checkbox"]', function () {
    var countCheckedCheckboxes = $(this).parent('label').parent('.mdli-single').parent('.md-list-items').parent('.md-list-wrapper').find("input:checked").length;

    if (countCheckedCheckboxes === 0) {
      $(this).parent('label').parent('.mdli-single').parent('.md-list-items').parent('.md-list-wrapper').siblings('.md-value').text('Select value');
    } else {
      $(this).parent('label').parent('.mdli-single').parent('.md-list-items').parent('.md-list-wrapper').siblings('.md-value').text(countCheckedCheckboxes + ' selected');
    }
  });
  /*$('.md-list-items .mdli-single label  input[type="checkbox"]').on('click', function () {
    var countCheckedCheckboxes = $(this).parent('label').parent('.mdli-single').parent('.md-list-items').parent('.md-list-wrapper').find("input:checked").length;

    if (countCheckedCheckboxes === 0) {
      $(this).parent('label').parent('.mdli-single').parent('.md-list-items').parent('.md-list-wrapper').siblings('.md-value').text('Select value');
    } else {
      $(this).parent('label').parent('.mdli-single').parent('.md-list-items').parent('.md-list-wrapper').siblings('.md-value').text(countCheckedCheckboxes + ' selected');
    }
  })*/
  $('body').on('click', '#tabs ul li', function () {
    const tabValue = $(this).attr('data-tab');
    $('.tabs-modal').hide();
    $('.tabs-modal.' + tabValue).show();
    $('#tabs ul li').removeClass('active');
    $(this).addClass('active');
  })
  /*Show selected checkbox count*/
  $('body').on('click', '.sd-list-items .sdli-single label', function () {
    var singleSelectValue = $(this).text();
    $(this).parent('.sdli-single').parent('.sd-list-items').parent('.sd-list-wrapper').siblings('.sd-value').text(singleSelectValue);
    $('.sd-list-wrapper').toggleClass('is-active');
  });

  /*Multiselect Done*/
  $('body').on('click', '.md-done', function () {
    $('.md-list-wrapper').removeClass('is-active')
  });
  /*$('.md-done').on('click', function () {
    $('.md-list-wrapper').removeClass('is-active')
  })*/
  /*Multiselect Done*/
  /*Multiselect Clear All*/
  $('body').on('click', '.md-clear', function () {
    $(this).parent('.md-cta').siblings('.md-list-items').children('.mdli-single').children('label').children('input[type="checkbox"]').prop('checked', false);
    $(this).parent('.md-cta').parent('.md-list-wrapper').siblings('.md-value').text('Select value')
  });
  /*$('.md-clear').on('click', function () {
    $(this).parent('.md-cta').siblings('.md-list-items').children('.mdli-single').children('label').children('input[type="checkbox"]').prop('checked', false);
    $(this).parent('.md-cta').parent('.md-list-wrapper').siblings('.md-value').text('Select value')
  })*/
  /*Multiselect Clear All*/

  /*Toggle dropdown*/
  const $menu2 = $('.multiselect-dropdown-wrapper');
  $(document).mouseup(e => {
    if (!$menu2.is(e.target) // if the target of the click isn't the container...
      && $menu2.has(e.target).length === 0) // ... nor a descendant of the container
    {
      $menu2.children('.md-list-wrapper').removeClass('is-active');
    }
  });

  const $menu3 = $('.singleselect-dropdown-wrapper');
  $(document).mouseup(e => {

    if (!$menu3.is(e.target) // if the target of the click isn't the container...
      && $menu3.has(e.target).length === 0) // ... nor a descendant of the container
    {
      $menu3.children('.sd-list-wrapper').removeClass('is-active');
    }
  });

  /*Toggle dropdown*/


  // In your Javascript (external .js resource or <script> tag)
  /*  $(document).ready(function () {
     $('.basic-single').select2();
   }); */
  /*Select2 placeholder*/
  var Defaults = $.fn.select2.amd.require('select2/defaults');

  $.extend(Defaults.defaults, {
    searchInputPlaceholder: ''
  });

  var SearchDropdown = $.fn.select2.amd.require('select2/dropdown/search');

  var _renderSearchDropdown = SearchDropdown.prototype.render;

  SearchDropdown.prototype.render = function (decorated) {

    // invoke parent method
    var $rendered = _renderSearchDropdown.apply(this, Array.prototype.slice.apply(arguments));

    this.$search.attr('placeholder', this.options.get('searchInputPlaceholder'));

    return $rendered;
  };

  /*Select2 placeholder*/
  $('.basic-single').select2({
    searchInputPlaceholder: 'Search'
  });

  /*Open select2 onfocus*/
  // on first focus (bubbles up to document), open the menu
  $(document).on('focus', '.select2-selection.select2-selection--single', function (e) {
    $(this).closest(".select2-container").siblings('select:enabled').select2('open');
  });

  // steal focus during close - only capture once and stop propogation
  $('select.select2').on('select2:closing', function (e) {
    $(e.target).data("select2").$selection.one('focus focusin', function (e) {
      e.stopPropagation();
    });
  });
  /*Open select2 onfocus*/

  /*Remove tabindex from anchor tags*/
  $('input').removeAttr("tabindex");
  /*Remove tabindex from anchor tags*/
  $('body').on('click', '.overlay,.overlay-document,.mro-close-modal', function () {
    $('.overlay,.mro-modal-wrapper,.overlay-document,.mro-modal-wrapper-document').hide();
  });

  /*scroll highlight*/

  var nav = $('.vertical-scroll-links a');
  console.log(nav.length);
  if (nav.length == 5) {
    var eTop1 = $('#section-mro-1').offset().top;
    var eTop2 = $('#section-mro-2').offset().top;
    var eTop3 = $('#section-mro-3').offset().top;
    var eTop4 = $('#section-mro-4').offset().top;
    var eTop5 = $('#section-mro-5').offset().top;
  } else if  (nav.length == 4) {
    var eTop1 = $('#section-mro-1').offset().top;
    var eTop2 = $('#section-mro-2').offset().top;
    var eTop3 = $('#section-mro-3').offset().top;
    var eTop4 = $('#section-mro-4').offset().top;

  }
  $(window).scroll(function () {
    var changelink1 = eTop1 - $(window).scrollTop();
    if (changelink1 < 100) {
      $('.vertical-scroll-links a').removeClass('active');
      $('.vertical-scroll-links a[href="#section-mro-1"]').addClass('active');
    }
    var changelink2 = eTop2 - $(window).scrollTop();
    if (changelink2 < 100) {
      $('.vertical-scroll-links a').removeClass('active');
      $('.vertical-scroll-links a[href="#section-mro-2"]').addClass('active');
    }
    var changelink3 = eTop3 - $(window).scrollTop();
    if (changelink3 < 100) {
      $('.vertical-scroll-links a').removeClass('active');
      $('.vertical-scroll-links a[href="#section-mro-3"]').addClass('active');
    }
    var changelink4 = eTop4 - $(window).scrollTop();
    if (changelink4 < 100) {
      $('.vertical-scroll-links a').removeClass('active');
      $('.vertical-scroll-links a[href="#section-mro-4"]').addClass('active');
    }
    var changelink5 = eTop5 - $(window).scrollTop();
    if (changelink5 < 100) {
      $('.vertical-scroll-links a').removeClass('active');
      $('.vertical-scroll-links a[href="#section-mro-5"]').addClass('active');
    }

  });
  /* $(window).scroll(function () {
    var spaceFromTop = $('.vertical-scroll-links a').offset().top - $(window).scrollTop();
    if($('.vertical-scroll-links a:nth-child(1)').offset().top - $(window).scrollTop() < 20){
      console.log('first element is at top')
    }
    if($('.vertical-scroll-links a:nth-child(2)').offset().top - $(window).scrollTop() < 20){
      console.log('second element is at top')
    }
    if($('.vertical-scroll-links a:nth-child(3)').offset().top - $(window).scrollTop() < 20){
      console.log('third element is at top')
    }
    if($('.vertical-scroll-links a:nth-child(4)').offset().top - $(window).scrollTop() < 20){
      console.log('forth element is at top')
    }
    if($('.vertical-scroll-links a:nth-child(5)').offset().top - $(window).scrollTop() < 20){
      console.log('fifth element is at top')
    }

  }); */
  /*scroll highlight*/

});

function removeErroMsg(id) {
  if ($('#' + id + '-error').length) {
    if ($('#' + id).val() != '0' && $('#' + id).val() != '') {
      $('#' + id).removeClass('error');
      $('#' + id + '-error').hide();
    } else {
      $('#' + id).addClass('error');
      $('#' + id + '-error').show();
    }
  }
}

function showInsertUpdateMessage(msg = '', response = false, second = 2800) {
  var isSuccess = false;
  if (response) {
    var isSuccess = response.success;
  }
  if (msg) {
    if (isSuccess) { 
      $.toast({
        text: msg,
        loader: false,
        showHideTransition: 'slide',  // It can be plain, fade or slide
        bgColor: '#0d8530',              // Background color for toast
        textColor: '#fff',            // text color
        allowToastClose: true,       // Show the close button or not
        hideAfter: second,              // `false` to make it sticky or time in miliseconds to hide after
        stack: 10,                     // `fakse` to show one stack at a time count showing the number of toasts that can be shown at once
        textAlign: 'left',            // Alignment of text i.e. left, right, center
        position: 'top-center'       // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values to position the toast on page
      });
    } else {
      $.toast({
        text: msg,
        loader: false,
        showHideTransition: 'slide',  // It can be plain, fade or slide
        bgColor: '#f00',              // Background color for toast
        textColor: '#fff',            // text color
        allowToastClose: true,       // Show the close button or not
        hideAfter: second,              // `false` to make it sticky or time in miliseconds to hide after
        stack: 10,                     // `fakse` to show one stack at a time count showing the number of toasts that can be shown at once
        textAlign: 'left',            // Alignment of text i.e. left, right, center
        position: 'top-center'       // bottom-left or bottom-right or bottom-center or top-left or top-right or top-center or mid-center or an object representing the left, right, top, bottom values to position the toast on page
      });
    }
  }
}
